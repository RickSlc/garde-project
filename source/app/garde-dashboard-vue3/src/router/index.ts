import {createRouter, createWebHistory} from 'vue-router'

const routes = [
  {
    path: '/garde',
    alias: ['/dashboard'],
    component: () => import('@/views/Dashboard.vue'),
    name: 'Dashboard'
  },
  {
    path: '/evaluator',
    component: () => import('@/views/Evaluator.vue'),
    name: 'Population Evaluator'
  },
  {
    path: '/evaluatorRules',
    component: () => import('@/components/EvaluatorRules.vue'),
    name: 'Evaluator Rules'
  },
  {
    path: '/evaluatorStatesViewer/:instanceId',
    component: () => import('@/views/EvaluatorStatesViewer.vue'),
    name: 'Evaluator States'
  },
  {
    path: '/resultsViewer/:outputType/:instanceId',
    component: () => import('@/views/ResultsViewer.vue'),
    name: 'Results Viewer'
  },
  {
    path: '/graph',
    component: () => import('@/views/GraphData.vue'),
    name: 'Results Graph'
  },
  {
    path: '/codeSetBrowser/:codeSetCd?',
    component: () => import('@/views/CodeSetBrowser.vue'),
    name: 'Code Set Browser'
  },
  {
    path: '/codeBrowser/:bindingCd?',
    component: () => import('@/views/CodeBrowser.vue'),
    name: 'Code Mappings'
  },
  {
    path: '/terminologyLoader',
    component: () => import('@/views/TerminologyImporter.vue'),
    name: 'Terminology'
  },
  {
    path: '/scannedCodes',
    component: () => import('@/views/ScannedCodesDataTable.vue'),
    name: 'Scanned Codes'
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router

import {defineStore} from "pinia";
import {Evaluator, EvaluatorInstance, EvaluatorState} from "@/model/evaluatorModels";
import axios from "axios";
import {useAppStore} from "@/store/appStore";
import {formatTime, handleAxiosError} from "@/store/sharedHelpers";

export interface EvaluatorInstruction {
    instanceId: string
    evaluatorId: string
    outputType: string
    patientDemographicsFile: File
    patientFamilyHistoryFile: File
    patientLdlObservationsFile: File | undefined
}

export type SortOrderType = 'asc' | 'desc'

const appStore = useAppStore()
export const useEvaluatorStore = defineStore("evaluators", {

    state: () => ({
        evaluatorInstructions: [] as EvaluatorInstruction[],
        evaluatorPickList: [] as Evaluator[],
        evaluatorInstanceMap: new Map<String, EvaluatorInstance>,
        currentEvaluatorInstance: {} as EvaluatorInstance,
        evaluatorInstanceSortOrder: 'desc' as SortOrderType,
        excludeNegMetCriteria: false as boolean,
        currentEvaluator: {} as Evaluator,
        dummy: 0
    }),

    actions: {

        async fetchEvaluatorRules(evaluatorId: string) {
            const endpoint = `${appStore.serverEndpoint}/getEvaluatorRuleAndCodes/${evaluatorId}`
            console.log(`FETCH EVALUATOR RULES: ${endpoint}`)

            try {
                const resp = await axios.get(endpoint)
                this.currentEvaluator = resp.data
            } catch (error) {
                handleAxiosError(error, endpoint);
            }
        },

        async fetchEvaluator(evaluatorId: string, lookBackId: number) {
            const endpoint = `${appStore.serverEndpoint}/getEvaluationStates/${evaluatorId}/${lookBackId}`
            console.log(`FETCH EVALUATOR: ${endpoint}`)

            try {
                const resp = await axios.get(endpoint)
                this.currentEvaluator = resp.data
                for (let inst of resp.data.instances) {
                    inst.startTime = formatTime(inst.startTime)
                    inst.completionTime = formatTime(inst.completionTime)
                    this.evaluatorInstanceMap.set(inst.instanceId, inst)
                }
            } catch (error) {
                handleAxiosError(error, endpoint);
            }
        },

        async fetchEvaluatorInstance(instanceId: string) {

            let ei = this.evaluatorInstanceMap.get(instanceId)
            console.log('FETCH EVALUATOR MAP INSTANCE')

            if (ei) {
                this.currentEvaluatorInstance = ei
            }

            const url = appStore.serverEndpoint + '/getEvaluatorInstance/' + instanceId
            console.log('NO MAP - FETCH EVALUATOR DB INSTANCE: ' + url)
            try {
                const resp = await axios.get(url)
                this.currentEvaluator = resp.data
                for (let inst of resp.data.instances) {
                    inst.startTime = formatTime(inst.startTime)
                    inst.completionTime = formatTime(inst.completionTime)
                    this.evaluatorInstanceMap.set(inst.instanceId, inst)
                    this.currentEvaluatorInstance = inst
                }

            } catch (error) {
                handleAxiosError(error, url);
            }
        },

        async runEvaluation(instruction: EvaluatorInstruction) {

            console.log("Instruction", instruction)

            this.evaluatorInstructions.push(instruction)
            let endpoint = `${appStore.serverEndpoint}/runEvaluation`
            console.log(`RUN EVALUATION URL: ${endpoint}`)

            const formData = new FormData()
            formData.append('instanceId', instruction.instanceId)
            formData.append('serviceId', instruction.evaluatorId)
            formData.append('outputType', instruction.outputType)
            formData.append('patDemogFile', instruction.patientDemographicsFile)
            formData.append('patFHxFile', instruction.patientFamilyHistoryFile)

            if (instruction.patientLdlObservationsFile !== undefined) {
                formData.append('patLdlObsFile', instruction.patientLdlObservationsFile)
            }

            try {
                const maxSize = 1024 * 1024 * 1024 * 2 // 2 GB
                const response = axios.post(endpoint, formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    },
                    timeout: 180000, // 3 min timeout
                    maxContentLength: maxSize,
                    maxBodyLength: maxSize,
                })
                console.log(response)
                console.log('Evaluation ' + instruction.evaluatorId + ':' + instruction.instanceId + ' completed.')

            } catch (error) {
                handleAxiosError(error, endpoint);
            }
        },

        toggleEvaluationInstanceSortOrder() {
            if (this.evaluatorInstanceSortOrder === 'desc') {
                this.evaluatorInstanceSortOrder = 'asc'
            } else {
                this.evaluatorInstanceSortOrder = 'desc'
            }
        },

        toggleExcludeNegMetCriteria() {
            this.excludeNegMetCriteria = ! this.excludeNegMetCriteria
        }

    },

    getters: {

        getEvaluatorInstanceResultsFile: (state) => {
            return (instanceId: string): string | undefined => {

                if (state.currentEvaluator.instances) {
                    for (const inst of state.currentEvaluator.instances) {
                        if (inst.instanceId === instanceId) {
                            const evaluatorStates = inst.states as EvaluatorState[]
                            for (const idx in evaluatorStates) {
                                if (evaluatorStates[idx].resultsFile) {
                                    return evaluatorStates[idx].resultsFile?.replace(':', '/')
                                }
                            }
                        }
                    }
                }
                return undefined
            }
        },

        getEvaluatorInstanceFinalState: (state) => {
            return (instanceId: string): string | undefined => {
                let inst = state.evaluatorInstanceMap.get(instanceId)
                if (inst?.states) {
                    const evaluatorStates = inst.states as EvaluatorState[]
                    return evaluatorStates[evaluatorStates.length - 1].status
                }
                return undefined
            }
        },

        getEvaluatorInstancesSorted: (state) => {

                let instances = state.currentEvaluator?.instances || [];

                if (state.excludeNegMetCriteria === true) {
                    instances = instances.filter(instance => instance.peopleMetCriteria !== -1);
                }

                return instances.sort((a: EvaluatorInstance, b: EvaluatorInstance) => {
                    if (!a.startTime) return 1;
                    if (!b.startTime) return -1;
                    if (state.evaluatorInstanceSortOrder === 'asc') {
                        return new Date(a.startTime).getTime() - new Date(b.startTime).getTime();
                    } // else desc
                    return new Date(b.startTime).getTime() - new Date(a.startTime).getTime();
                });
        },

        getEvaluatorInstanceById: (state) => {
            return (id: string) => state.currentEvaluator?.instances?.find((inst) => inst.instanceId === id)
        },

        getEvaluatorById: (state) => {
            return (eId: string) => state.evaluatorPickList.find((ev) => ev.evaluatorId === eId)
        },

    }

})


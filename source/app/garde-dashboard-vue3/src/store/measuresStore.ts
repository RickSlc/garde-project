import {defineStore} from "pinia";
import axios from "axios";
import {Evaluator, Measure, MeasureCategory} from "@/model/evaluatorModels";
import {useAppStore} from "@/store/appStore";
import {handleAxiosError} from "@/store/sharedHelpers";
import {CategoricalCodeAnalytics} from "@/model/terminologyModels";

const appStore = useAppStore()
export const useMeasuresStore = defineStore("measuresStore", {

    state: () => ({
        evaluator: {} as Evaluator,
        fhxFilename: '',
        fhxError: '',
        demogFilename: '',
        patientDataMeasures: [] as MeasureCategory[],
        patientDataCodeAnalyticsArray: [] as CategoricalCodeAnalytics[]
    }),

    actions: {

        async fetchMeasures(evaluatorId: string) {
            const endpoint = `${appStore.serverEndpoint }/terminologySummary/${evaluatorId}`
            console.log(`FETCH TERMINOLOGY SUMMARY: ${endpoint}`)
            try {
                const response = await axios.get(endpoint);
                this.evaluator = response.data

            } catch (error) {
                handleAxiosError(error, endpoint);
            }
        },

        analyzePatientData: async function (dataType: string, patientDataFile: File) {

            let gardeRestServiceUrl = appStore.serverEndpoint + '/analyzePatientDataCodes'
            console.log('ANALYZE PATIENT DATA URL: ' + gardeRestServiceUrl)

            const formData = new FormData()
            formData.append('dataType', dataType)
            formData.append('patientDataFile', patientDataFile)

            try {
                const resp = await axios.post(gardeRestServiceUrl, formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    }
                })
                this.fhxError = ''
                this.patientDataCodeAnalyticsArray = resp.data

                if (this.patientDataCodeAnalyticsArray.length > 0) {
                    if (this.patientDataCodeAnalyticsArray[0].category === 'ERROR'
                        && this.patientDataCodeAnalyticsArray[0].errorMessage
                    ) {
                        this.fhxError = this.patientDataCodeAnalyticsArray[0].errorMessage
                        return
                    }
                }

                if (dataType == 'Patient_Family_Hx') {
                    this.fhxFilename = patientDataFile.name
                }

                this.patientDataCodeAnalyticsArray.forEach( (cat) => {
                        const meas: Measure = {
                            measureLabel: 'Distinct ' + cat.category + " Code Count",
                            measureValue: "" + cat.codeAnalytics.length,
                        }
                        const mCategory: MeasureCategory = {
                            measureCategory: patientDataFile.name + ' ' + cat.category + ' Summary',
                            measures: [meas]
                        }
                        this.patientDataMeasures.push(mCategory)

                        // if (this.evaluator.measureCategories) {
                        //     this.evaluator.measureCategories.push(mCategory)
                        // } else {
                        //     this.evaluator.measureCategories = [mCategory]
                        // }
                    })

            } catch (error) {
                throw new Error(`Failed to post data file: ${error}`);
            }
        },



    },

    getters: {

    }

})

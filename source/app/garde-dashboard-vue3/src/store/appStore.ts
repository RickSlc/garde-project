// Utilities
import {defineStore} from 'pinia'
import axios from "axios";
import {Evaluator} from "@/model/evaluatorModels";
import {handleAxiosError} from "@/store/sharedHelpers";

export interface SystemState {
    systemCd: string
    systemLabel: string
    endpoint: string
    status: string
}

export interface ServiceResponse {
    status: 'SUCCESS' | 'ERROR'
    message: string
}

const devServer = 'http://localhost:3000'
const endpointRegEx = /^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n?]+):?(\d+)?/ig
const urlMatch = endpointRegEx.exec(window.location.href)

function getServerUrl() {
    console.log("GET SERVER ", urlMatch)
    if (urlMatch) {
        let server = ''
        if (urlMatch[0] === devServer) { //need to change the port
            server = urlMatch[0].replace("3000", "8090")
        } else {
            server = urlMatch[0]
        }
        console.log("SERVER " + server)
        return server
    }
    return ''
}

function getWebsocketEndpoint() {
    let protocol = 'ws://'
    if (urlMatch) {
        if (urlMatch[0].includes('https')) {
            protocol = 'wss://'
        }

        let websocketServer = ''
        if (urlMatch[2]) { // has a port
            websocketServer = protocol + urlMatch[1] + ':8090/channel/evaluators'
        } else {
            websocketServer = protocol + urlMatch[1] + '/channel/evaluators'
        }
        console.log("WEBSOCKET SERVER " + websocketServer)
        return websocketServer
    }
    return ''
}

const systemStartStates: SystemState[] = [
    {
        systemCd: 'POPCOORD',
        systemLabel: 'Popuation Coordinator',
        endpoint: '/popcoord',
        status: 'down'
    },
    {
        systemCd: 'CDSHOOKS',
        systemLabel: 'CDS Hooks',
        endpoint: '/cdshooks',
        status: 'down'
    },
    // {
    //   systemCd: 'CHATBOT',
    //   systemLabel: 'U Author Boot',
    //   endpoint: '/chatbot',
    //   status: 'down'
    // },
    // {
    //   systemCd: 'EPICSDE',
    //   systemLabel: 'Epic SDE',
    //   endpoint: '/dse',
    //   status: 'down'
    // },
]

export const nccn2019 = 'genetic-testing-recommender-r4'
export const nccn2023 = 'genetic-testing-recommender-r4-2023'
export const fhgt2018 = 'fh-genetic-testing-recommender-r4-2018'

export interface CdsHooksService {
    hook: string
    title: string
    description: string
    id: string
    prefetch: Object
    extension: string | null
}

interface AppState {
    showStatesDialogMap: Map<string,boolean>
    showEvalDialog: boolean
    showImportDialog: boolean
    systemStates?: SystemState[]
    cdsHooksServices: CdsHooksService[]
    serverEndpoint: string
    websocketEndpoint: string
    currentEvaluatorId: string
    currentGuideline: string
    evaluatorPickList: Evaluator[]
    breadcrumbs: [{}]
    majorBuildVersion: string
    buildVersion: string
}

export const useAppStore = defineStore('app', {
    state: (): AppState => (
        {
        showStatesDialogMap: new Map<string,boolean>,
        showEvalDialog: false,
        showImportDialog: false,
        systemStates: systemStartStates,
        cdsHooksServices: [],
        serverEndpoint: getServerUrl(),
        websocketEndpoint: getWebsocketEndpoint(),
        currentEvaluatorId: nccn2023,
        currentGuideline: nccn2023,
        evaluatorPickList: [] as Evaluator[],
        // breadcrumbs: [{title: 'Dashboard', disabled: false, href:'dashboard'}]
        breadcrumbs: [{}],
        majorBuildVersion: '3.0.7',
        buildVersion: ''
    }),

    actions: {

        async downloadFile(url: string, fileName: string) {
            console.log("DOWNLOAD RESULTS URL: " + url)
            const anchor = document.createElement('a');
            anchor.style.display = 'none';
            document.body.appendChild(anchor);

            // Set the href attribute of the anchor to the file URL
            anchor.href = url;

            // Set the download attribute with the desired file name
            anchor.download = fileName;

            // Trigger a click event on the anchor to initiate the download
            anchor.click();

            // Remove the anchor element from the DOM
            document.body.removeChild(anchor);
        },

        async fetchEvaluatorsPickList() {
            const endpoint = `${this.serverEndpoint}/data/evaluators`;
            console.log(`FETCH EVALUATOR PL: ${endpoint}`)
            try {
                const response = await axios.get(endpoint);
                const rawArray = response.data._embedded.evaluators
                let respArray: Evaluator[] = []
                for (let e of rawArray) {
                    respArray.push({
                        evaluatorId: e.serviceId,
                        title: e.title,
                        guideline: e.guideline
                    })
                }
                this.evaluatorPickList = respArray
            } catch (error) {
                handleAxiosError(error, endpoint);
            }
        },

        async fetchSystemStates() {
            const endpoint = `${this.serverEndpoint}/systemStatus`;
            console.log(`CHECK SYSTEM STATUS URL: ${endpoint}`);
            try {
                const response = await axios.get(endpoint);
                this.systemStates = response.data
            } catch (error) {
                handleAxiosError(error, endpoint);
            }
        },

        async fetchBuildInfo() {
            const endpoint = `${this.serverEndpoint}/build/info`;
            console.log(`BUILD INFO URL: ${endpoint}`);
            try {
                const response = await axios.get(endpoint);
                this.buildVersion = this.majorBuildVersion + ' | ' + response.data.build.time.split('T')[0]
            } catch (error) {
                this.buildVersion = this.majorBuildVersion
                handleAxiosError(error, endpoint);
            }
        },

        async fetchCsdHooksServices() {
            const endpoint = `${this.serverEndpoint}/cds-services`;
            console.log(`FETCH CDS HOOKS SERVICES URL: ${endpoint}`);
            try {
                const response = await axios.get(endpoint);
                if (response.data.services !== undefined) {
                    this.cdsHooksServices = response.data.services;
                }
            } catch (error) {
                handleAxiosError(error, endpoint);
            }
        },

    },

    getters: {

        isReadyToRun: (state) => {

            if (state.cdsHooksServices === undefined || state.cdsHooksServices.length === 0) {
                return false
            }
            for(let i = 0; i < state.cdsHooksServices.length; i++) {
                if (state.cdsHooksServices[i].id === state.currentEvaluatorId) {
                    return true
                }
            }
        },

        getSystemStates: (state): SystemState[] => {
            if (state.systemStates === undefined) {
                return []
            }
            return state.systemStates
        },

        getCdsHooksServices: (state): CdsHooksService[] => {
            if (state.cdsHooksServices === undefined) {
                return []
            }
            return state.cdsHooksServices
        }
    }

})

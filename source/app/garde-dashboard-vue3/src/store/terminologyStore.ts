import {defineStore} from "pinia";
import axios from "axios";
import {ServiceResponse, useAppStore} from "@/store/appStore";
import {BindingCodeMapping, CodeSetDefinition, MappedCode} from "@/model/terminologyModels";
import {handleAxiosError} from "@/store/sharedHelpers";

const appStore = useAppStore()

export interface ImportInstruction {
    evaluatorId: string
    importStrategy: string
    importCodeType: string
    terminologyFile: File
}

const allCodeSetDef: CodeSetDefinition = {
    codeSystem: 'none',
    codeSetBinding: 'ALL',
    definition: 'Code Sets',
}

interface TerminologyState {
    codeMappings: MappedCode[]
    bindingCodeMapFilter: string
    filterEhrCodes: 0 | 1
    filterStandardCodes: 0 | 1
    filterNotMappedCodes: 0 | 1
    codeMapDefinitions: CodeSetDefinition[]
    codeMapDefinition: CodeSetDefinition

    codeSetBindingFilter: string
    filterCodeSetEhrCodes: 0 | 1
    filterCodeSetStandardCodes: 0 | 1
    filterCodeSetNotMappedCodes: 0 | 1
    codeSetDefinitions: CodeSetDefinition[]
    codeSetDefinition: CodeSetDefinition
    codeSetMembers: MappedCode[]

    importResponse?: ServiceResponse
}

export const useTerminologyStore = defineStore("terminology", {

    state: (): TerminologyState => ({
        codeMappings: [],
        bindingCodeMapFilter: 'ALL',
        filterEhrCodes: 0,
        filterStandardCodes: 0,
        filterNotMappedCodes: 0,
        codeMapDefinitions: [],
        codeMapDefinition: allCodeSetDef,

        codeSetMembers: [],
        codeSetBindingFilter: 'ALL',
        filterCodeSetEhrCodes: 0,
        filterCodeSetStandardCodes: 0,
        filterCodeSetNotMappedCodes: 0,
        codeSetDefinitions: [],
        codeSetDefinition: allCodeSetDef,

        importResponse: undefined
    }),

    actions: {

        toggleEhrFilter() {
            if (this.filterEhrCodes === 0) {
                this.filterEhrCodes = 1
            } else {
                this.filterEhrCodes = 0
            }
        },

        toggleStandardFilter() {
            if (this.filterStandardCodes === 0) {
                this.filterStandardCodes = 1
            } else {
                this.filterStandardCodes = 0
            }
        },

        toggleNotMappedFilter() {
            if (this.filterNotMappedCodes === 0) {
                this.filterNotMappedCodes = 1
            } else {
                this.filterNotMappedCodes = 0
            }
        },

        importTerminology: async function (instruction: ImportInstruction) {

            let gardeRestServiceUrl = appStore.serverEndpoint + '/importTerminology'
            console.log('IMPORT TERMINOLOGY URL: ' + gardeRestServiceUrl)

            const formData = new FormData()
            formData.append('serviceId', instruction.evaluatorId)
            formData.append('importStrategy', instruction.importStrategy)
            formData.append('importCodeType', instruction.importCodeType)
            formData.append('terminologyFile', instruction.terminologyFile)

            try {
                const resp = await axios.post(gardeRestServiceUrl, formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    }
                })
                return resp.data
            } catch (error) {
                // return {status: 'ERROR', message: error.toString()}
                throw new Error(`Failed to post terminology: ${error}`);
            }
        },

        async fetchCodeMappings(codeBinding: string) {
            const endpoint = `${appStore.serverEndpoint}/bindingCodeMappings/${appStore.currentEvaluatorId}`
            try {
                console.log(`FETCH CODE MAPPINGS: ${endpoint}`)
                const response = await axios.get(endpoint)
                const bindings: BindingCodeMapping[] = response.data
                const members: MappedCode[] = []
                const bindingCodeSetDef: CodeSetDefinition[] = []

                const allCodesDef: CodeSetDefinition =
                    {
                        codeSystem: 'http://utah.edu/rher/nccn'
                        , codeSetBinding: 'ALL'
                        , definition: 'ALL'
                        , isMappedToStandard: 0
                        , isMappedToLocal: 0
                        , isCodeSet: 0
                    }
                bindingCodeSetDef.push(allCodesDef)

                for (const b in bindings) {

                    const bindingCodeDef: CodeSetDefinition =
                        {
                            codeSystem: bindings[b].bindingCode.codeSystem
                            , codeSetBinding: bindings[b].bindingCode.code
                            , definition: bindings[b].bindingCode.code
                            , isMappedToStandard: 0
                            , isMappedToLocal: 0
                            , isCodeSet: 0
                        }

                    for (const m in bindings[b].mappings) {
                        const code: MappedCode = {
                            gid: bindings[b].mappings[m].code.gid,
                            bindingCode: bindings[b].bindingCode.code,
                            codeSystem: bindings[b].mappings[m].code.codeSystem,
                            code: bindings[b].mappings[m].code.code,
                            codeLabel: bindings[b].mappings[m].code.codeLabel,
                            isMappedToStandard: !bindings[b].mappings[m].code.codeSystem.includes('utah.edu/rehr/nccn') && bindings[b].mappings[m].association === 'STANDARD_CODE_IS' ? 1 : 0,
                            isMappedToLocal: bindings[b].mappings[m].association === 'EHR_CODE_IS' ? 1 : 0
                        }
                        members.push(code)

                        if (code.isMappedToStandard == 1 && !code.codeSystem.includes('utah.edu/rehr/nccn')) { bindingCodeDef.isMappedToStandard = 1}
                        if (code.isMappedToLocal == 1) {bindingCodeDef.isMappedToLocal = 1}
                    }
                    bindingCodeSetDef.push(bindingCodeDef)
                }
                this.codeMappings = members
                this.codeMapDefinitions = bindingCodeSetDef

            } catch (error) {
                handleAxiosError(error, endpoint);
            }
        },

        toggleCodeSetEhrFilter() {
            if (this.filterCodeSetEhrCodes === 0) {
                this.filterCodeSetEhrCodes = 1
            } else {
                this.filterCodeSetEhrCodes = 0
            }
        },

        toggleCodeSetStandardFilter() {
            if (this.filterCodeSetStandardCodes === 0) {
                this.filterCodeSetStandardCodes = 1
            } else {
                this.filterCodeSetStandardCodes = 0
            }
        },

        toggleCodeSetNotMappedFilter() {
            if (this.filterCodeSetNotMappedCodes === 0) {
                this.filterCodeSetNotMappedCodes = 1
            } else {
                this.filterCodeSetNotMappedCodes = 0
            }
        },

        fetchCodeSet: async function (codeSetCode: string) {
            let endpoint = appStore.serverEndpoint
            if (codeSetCode && codeSetCode !== 'ALL') {
                endpoint += `/data/valueSets/search/findByVsCode?valueSetCode=${codeSetCode}&size=10000`
            } else {
                // endpoint += '/data/valueSets?size=10000'
                endpoint += `/data/valueSets/search/findValueSetsByServiceId?serviceId=${appStore.currentEvaluatorId}`
            }
            console.log(`FETCH CODE SET: ${endpoint}`)

            let defs = new Map<string, CodeSetDefinition>

            try {
                const response = await axios.get(endpoint)
                const rawCodes = response.data._embedded.valueSets
                if (!rawCodes) {
                    console.log('ERROR - NO CODE SETS FOUND FOR ' + codeSetCode)
                    return
                }

                const members: MappedCode[] = []
                for (const c in rawCodes) {

                    if (!defs.has(rawCodes[c].vsCode)) {
                        defs.set(rawCodes[c].vsCode,
                            {
                                codeSystem: rawCodes[c].vsCodeSystem
                                , codeSetBinding: rawCodes[c].vsCode
                                , definition: rawCodes[c].vsCodeLabel
                                , isMappedToStandard: 0
                                , isMappedToLocal: 0
                                , isNotMapped: 0
                                , isCodeSet: 0
                            }
                        )
                    }

                    const code: MappedCode = {
                        gid: rawCodes[c].codeId
                        , bindingCode: rawCodes[c].memberBindingCode
                        , codeSetBindingCode: rawCodes[c].vsCode
                        , codeSystem: rawCodes[c].codeSystem
                        , code: rawCodes[c].code
                        , codeLabel: rawCodes[c].codeLabel
                        , isMappedToStandard: !rawCodes[c].codeSystem.includes('utah.edu/rehr/nccn')
                        && rawCodes[c].association === 'STANDARD_CODE_IS' ? 1 : 0
                        , isMappedToLocal: rawCodes[c].association === 'EHR_CODE_IS' ? 1 : 0
                        , isNotMapped: 0
                    }
                    let csDef = defs.get(rawCodes[c].vsCode)
                    if (code.isMappedToLocal == 1 && csDef) {
                        csDef.isMappedToLocal = 1
                    }
                    if (code.isMappedToStandard == 1 && csDef) {
                        csDef.isMappedToStandard = 1
                    }
                    if (code.isMappedToStandard == 0
                        && code.isMappedToLocal == 0
                        && csDef) {
                        csDef.isNotMapped = 1
                        code.isNotMapped = 1
                    }
                    members.push(code)
                }

                this.codeSetDefinitions = [allCodeSetDef]
                for (let key of defs.keys()) {
                    let def = defs.get(key)
                    if (def) {
                        this.codeSetDefinitions.push(def)
                    }
                }

                this.codeSetMembers = members

            } catch (error) {
                handleAxiosError(error, endpoint);
            }

        },

        //     async fetchCodeSetDefs() {
        //         const url = appStore.serverEndpoint + '/codeSetDefinitions'
        //         console.log("FETCH CODE SET DEFINITIONS: " + url)
        //
        //         await axios.get(url)
        //             .then(resp => {
        //                 this.codeSetDefinitions = resp.data as CodeSetDefinition[]
        //             })
        //             .catch(function (error) {
        //                 let errorCode = 'ERROR'
        //                 if (error.response) {
        //                     console.log(errorCode, error.response.data)
        //                     console.log(errorCode, error.response.headers)
        //                 } else if (error.request) {
        //                     // The request was made but no response was received
        //                     errorCode = 'NO_RESPONSE'
        //                     console.log(errorCode, error.request);
        //                 } else {
        //                     // Something happened in setting up the request that triggered an Error
        //                     errorCode = 'INVALID_REQUEST_ERROR'
        //                     console.log(errorCode, error.message);
        //                 }
        //             })
        //     },
    },

    getters: {

        isMappedToLocal: (state) => {
            return (bCode: string) => {
                const def = state.codeMapDefinitions.filter((d) => d.codeSetBinding === bCode)
                if (def[0]) {
                    return def[0].isMappedToLocal === 1
                }
            }
        },

        isMappedToStandard: (state) => {
            return (bCode: string) => {
                const def =
                    state.codeMapDefinitions.filter((d) =>
                        d.codeSetBinding === bCode)
                if (def[0]) {
                    return def[0].isMappedToStandard === 1
                }
            }
        },

        isNotMapped: (state) => {
            return (bCode: string) => {
                const def = state.codeMapDefinitions.filter((d) => d.codeSetBinding === bCode)
                if (def[0]) {
                    return def[0].isMappedToStandard === 0 && def[0].isMappedToLocal === 0
                }
            }
        },

        getFilteredCodeSetMembers: (state) => {

            console.log('CS FILTER >>> ' + state.codeSetBindingFilter
                + ' ehr:' + state.filterCodeSetEhrCodes
                + ' std:' + state.filterCodeSetStandardCodes
                + ' not:' + state.filterCodeSetNotMappedCodes)

            if (!state.codeSetBindingFilter) {
                state.codeSetBindingFilter = 'ALL'
            }

            return state.codeSetMembers.filter((cd) => {

                if (state.codeSetBindingFilter === 'ALL') {
                    if (state.filterCodeSetEhrCodes == 0 && cd.isMappedToLocal == 1) {
                        return true
                    }
                    if (state.filterCodeSetStandardCodes == 0
                        && cd.isMappedToStandard == 1
                    ) {
                        return true
                    }
                    if (state.filterCodeSetNotMappedCodes == 0
                        && cd.isMappedToLocal == 0
                        && cd.isMappedToStandard == 0
                    ) {
                        return true
                    }
                }

                if (cd.codeSetBindingCode === state.codeSetBindingFilter) {
                    if (state.filterCodeSetEhrCodes == 0 && cd.isMappedToLocal == 1) {
                        return true
                    }
                    if (state.filterCodeSetStandardCodes == 0
                        && cd.isMappedToStandard == 1
                    ) {
                        return true
                    }
                    if (state.filterCodeSetNotMappedCodes == 0
                        && cd.isMappedToLocal == 0
                        && cd.isMappedToStandard == 0
                    ) {
                        return true
                    }
                }

            })
        },

        isCodeSetMappedToLocal: (state) => {
            return (bCode: string) => {
                const def = state.codeSetDefinitions.filter(
                    (d) => d.codeSetBinding === bCode)
                // console.log(bCode, def[0])
                if (def[0]) {
                    return def[0].isMappedToLocal === 1
                }
            }
        },

        isCodeSetMappedToStandard: (state) => {
            return (bCode: string) => {
                const def =
                    state.codeSetDefinitions.filter(
                        (d) => d.codeSetBinding === bCode)
                if (def[0]) {
                    return def[0].isMappedToStandard === 1
                }
            }
        },

        isCodeSetNotMapped: (state) => {
            return (bCode: string) => {
                const def =
                    state.codeSetDefinitions.filter(
                        (d) => d.codeSetBinding === bCode)
                if (def[0]) {
                    return def[0].isNotMapped === 1
                }
            }
        },

        getFilteredCodeMappings: (state) => {

            console.log('FILTER >>> ' + state.bindingCodeMapFilter + ' ehr:' + state.filterEhrCodes + ' std:' + state.filterStandardCodes + ' not:' + state.filterNotMappedCodes)

            if (!state.bindingCodeMapFilter) {
                state.bindingCodeMapFilter = 'ALL'
            }

            return state.codeMappings.filter((cd) => {

                if (state.bindingCodeMapFilter === 'ALL') {
                    if (state.filterEhrCodes == 0 && cd.isMappedToLocal == 1) {
                        return true
                    }
                    if (state.filterStandardCodes == 0
                        && cd.isMappedToStandard == 1
                    ) {
                        return true
                    }
                    if (state.filterNotMappedCodes == 0
                        && cd.isMappedToLocal == 0
                        && cd.isMappedToStandard == 0
                    ) {
                        return true
                    }
                }

                if (cd.bindingCode === state.bindingCodeMapFilter) {
                    if (state.filterEhrCodes == 0 && cd.isMappedToLocal == 1) {
                        return true
                    }
                    if (state.filterStandardCodes == 0
                        && cd.isMappedToStandard == 1
                    ) {
                        return true
                    }
                    if (state.filterNotMappedCodes == 0
                        && cd.isMappedToLocal == 0
                        && cd.isMappedToStandard == 0
                    ) {
                        return true
                    }
                }

            })
        },

        getCodeSetDefinition: (state) => {
            return (binding: string) => {
                const codeSetDef = state.codeSetDefinitions.filter(
                    (def) => def.codeSetBinding === binding
                )
                // console.log('getCodeSetDefinition for ' + state.codeSetBindingFilter + ':' + codeSetDef.length)
                return codeSetDef[0]?.definition
            }
        }

    }
})

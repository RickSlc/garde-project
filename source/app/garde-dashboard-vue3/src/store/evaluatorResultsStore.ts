import {defineStore} from "pinia";
import {useAppStore} from "@/store/appStore";
import {EvaluatorRegistryInstructionResult, EvaluatorResult} from "@/model/evaluatorModels";
import axios from "axios";
import {handleAxiosError} from "@/store/sharedHelpers";
import {integer} from "@volar/language-service";
import {useEvaluatorStore} from "@/store/evaluatorStore";
import {GoogleDataTable} from "vue-google-charts/dist/types";

const appStore = useAppStore()
const evaluatorStore = useEvaluatorStore()

export const useEvaluatorResultsStore= defineStore("evaluatorResultsStore", {

    state: () => ({
        evaluatorId: '',
        instanceId: '',
        page: 0,
        pageSize: 0,
        resultsPage: [] as EvaluatorResult[],
        registryInstructionsResultsPage: [] as EvaluatorRegistryInstructionResult[],
        patientCount: 0,
        chartData: [[]] as GoogleDataTable | Record<string, any> | unknown[][] | null,
    }),

    actions: {

        async downloadResultsFile(fileName: string) {
            const url = appStore.serverEndpoint + '/downloadResults/' + fileName
            console.log('DOWNLOAD: ' + fileName)
            await appStore.downloadFile(url, fileName)
        },

        async fetchResultsPage(outputType: string, instanceId: string) {
            const restUrl = appStore.serverEndpoint + '/resultsReviewPage/' + outputType + '/' + instanceId + '/' + this.page + '/' + this.pageSize
            console.log("FETCH RESULTS: " + restUrl)
            this.instanceId = instanceId
            try {
                const response = await axios.get(restUrl)
                if (outputType === 'MET_CRITERIA_OUTPUT_TYPE') {
                    this.resultsPage = response.data
                    if (this.resultsPage.length > 0) {
                        this.evaluatorId = this.resultsPage[0].cohort.cdsHooksServiceCode
                    }
                }

                if (outputType === 'REGISTRY_INSTRUCTION_OUTPUT_TYPE') {
                    this.registryInstructionsResultsPage = response.data
                }

            } catch (error) {
                handleAxiosError(error, restUrl);
            }
        },

        async fetchRuleSummaryGraphData() {
            if (this.resultsPage.length === 0) {
                console.log('NO RESULTS TO GRAPH')
                return [['Rule','Count']]
            }
            console.log('CREATING GRAPH DATA')
            let dataSet = []
            dataSet.push(['Rule', 'Count'])

            let ruleMap = new Map<string, number>
            let rules = evaluatorStore.currentEvaluator.rules
            if (rules === undefined) {
                await evaluatorStore.fetchEvaluatorRules(appStore.currentEvaluatorId)
                rules = evaluatorStore.currentEvaluator.rules
                if (rules !== undefined) {
                    for (let rule of rules) {
                        if (rule.ruleCd.includes('NO_LONGER_MEETS_CRITERIA')) {continue}
                        let shortCode = rule.ruleBadge + ':' + rule.ruleCd.replace('_FHX_CRIT','').replace('_2023','').replace('CO_', '').replace('BC_','').replaceAll('_',' ')
                        ruleMap.set(shortCode, 0)
                    }
                    console.log('RULES TO GRAPH: ' + ruleMap.size)
                }
            }

            for (let res of this.resultsPage) {
                for (let rule of res.metRuleCriteria) {
                    let shortCode = rule.ruleBadge + ':' + rule.ruleCd.replace('_FHX_CRIT','').replace('_2023','').replace('CO_', '').replace('BC_','').replaceAll('_',' ')

                    let count = ruleMap.get(shortCode)
                    if (count === undefined) {
                        count = 0
                    }
                    count++
                    ruleMap.set(shortCode, count)
                }
            }

            for (let [ruleCd, count] of ruleMap.entries()) {
                dataSet.push([ruleCd, count])
            }
            this.chartData = dataSet
        }

    },

    getters: {

        getPatientCount(state) : integer {
            if (this.resultsPage.length === 0) {
                return 0
            }
            let patIdSet = new Set<String>()
            for (let res of state.resultsPage) {
                patIdSet.add(res.patientId)
            }
            return patIdSet.size
        },

    }

})

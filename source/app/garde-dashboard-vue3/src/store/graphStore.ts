import {defineStore} from "pinia";
import {GoogleChartOptions, GoogleChartWrapperChartType, GoogleDataTable} from "vue-google-charts/dist/types";


export const useGraphStore = defineStore("graphsStore", {

    state: () => ({
        chartType: 'Bar' as GoogleChartWrapperChartType,
        chartData: [[]] as GoogleDataTable | Record<string, any> | unknown[][] | null,
        chartOptions: {} as GoogleChartOptions,
    })

})
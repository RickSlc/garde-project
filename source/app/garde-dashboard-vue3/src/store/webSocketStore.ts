import {useAppStore} from "@/store/appStore";
import {defineStore} from "pinia";
import {EvaluatorState} from "@/model/evaluatorModels";

export const useWebSocketStore = defineStore('webSocketStore', {

    state: () => ({
        evaluationStates: [] as EvaluatorState[],
        websock: undefined as undefined | WebSocket
    }),

    actions: {

        reset: function () {
            this.evaluationStates = []
        },

        closeWebSocket: function () {
            if (this.websock !== undefined) {
                this.websock.close()
            }
        },

        connect: async function () {
            this.reset()
            const appStore = useAppStore()
            console.log("Start connection to WebSocket Server:" + appStore.websocketEndpoint)

            this.websock = new WebSocket(appStore.websocketEndpoint)
            this.websock.onopen = this.websocketOnOpen;
            this.websock.onerror = this.websocketOnError;
            this.websock.onmessage = this.websocketOnMessage;
            this.websock.onclose = this.disconnect;
        },

        websocketOnOpen: function () {
            console.log("WEBSOCKET OPENED");
        },

        websocketOnError: function (e: Event): any {
            console.log("WEBSOCKET ERROR", e );
        },

        websocketOnMessage: function (msg: MessageEvent<string>) {

            const st = JSON.parse(msg.data)
            const evalState = {
                stateId: st.stateId,
                state: st.state,
                status: st.status,
                startTime: st.startTime,
                completionTime: st.completionTime,
                message: st.message
            } as EvaluatorState
            // console.log("WEBSOCKET MESSAGE", msg )
            if (st.resultsFile && st.resultsFile !== "null") {
                evalState.resultsFile = st.resultsFile
            }
            this.addState(evalState)
        },

        disconnect: function (e: CloseEvent): any {
            console.log("WEBSOCKET DISCONNECTED", e );
            this.websock = undefined
        },

        addState(state: EvaluatorState) {
            // if this new state is an error, need to set end time for all previous states
            if (state.status === 'FAILED') {
                for (let i = 0; i < this.evaluationStates.length; i++) {
                    if (this.evaluationStates[i].status === 'PROCESSING') {
                        this.evaluationStates[i].status = 'FAILED';
                    }
                    if (!this.evaluationStates[i].completionTime) {
                        this.evaluationStates[i].completionTime = state.completionTime;
                    }
                }
            }

            const stateIndex = this.evaluationStates.findIndex(
                (s)=> s.stateId === state.stateId
            )
            if (stateIndex > -1) { // update existing state with new
                this.evaluationStates[stateIndex] = state
            } else {
                this.evaluationStates.push(state)
            }
        },

    },

    getters: {

        isWebSocketOpen: (state): boolean => {
          if (state.websock) {
              return true
          }
          return false
        },

        getLastEvaluationState: (state): EvaluatorState | undefined => {
            if (state.evaluationStates.length === 0) {
                return undefined
            }
            const lastState = state.evaluationStates[state.evaluationStates.length - 1];
            console.log(lastState.resultsFile)
            return lastState;
        }
    }

})

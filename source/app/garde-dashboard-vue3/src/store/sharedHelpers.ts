import {DateTime} from "ts-luxon";

export function handleAxiosError(error: any, endpoint: string) {
    let errorCode = 'Error';
    if (error.response) {
        errorCode = 'ERROR_DURING_FETCH';
        console.log(errorCode, error.response.data, error.response.headers);
    } else if (error.request) {
        errorCode = 'NO_RESPONSE_FROM_SERVER';
        console.log(errorCode, error.request);
    } else {
        errorCode = 'INVALID_REQUEST_ERROR';
        console.log(errorCode, `FAILED: ${endpoint}`);
    }

}

export function formatTime(dateTimeString?: string): string {
    let dtmString = new String(dateTimeString)
    if (dtmString) {
        return dtmString.replace('T',' ').split('.')[0].toString();
    }
    const formatted = DateTime.now().toFormat('yyyy-LL-dd HH:mm:ss');
    return formatted;
}
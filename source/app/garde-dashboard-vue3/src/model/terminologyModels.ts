import {integer} from "@volar/language-service";

export interface Term {
    id: string;
    name: string;
    definition: string;
}

export interface Code {
    gid: number
    codeSystem: string
    code: string
    codeLabel: string
    codeSetBindingCode?: string
    bindingCode?: string
    version?: number
}
export interface MappedCode extends Code {
    isMappedToStandard?: 0 | 1
    isMappedToLocal?: 0 | 1
    isNotMapped?: 0 | 1
    isCodeSet?: 0 | 1
}

export interface CodeAssociation {
    codeAssociationGid: string,
    association: string,
    code: Code
}
export interface BindingCodeMapping {
    bindingCode: Code,
    mappings: CodeAssociation[]
}
export interface CodeSetDefinition {
    codeSystem: string
    codeSetBinding: string
    definition: string
    isMappedToStandard?: 0 | 1
    isMappedToLocal?: 0 | 1
    isNotMapped?: 0 | 1
    isCodeSet?: 0 | 1
}

export interface CodeAnalytics {
    count: integer
    isMappedAsStandardCode: integer
    isMappedAsEhrCode: integer
    code: Code
}

export interface CategoricalCodeAnalytics {
    category: string,
    errorMessage?: string,
    codeAnalytics: CodeAnalytics[]
}
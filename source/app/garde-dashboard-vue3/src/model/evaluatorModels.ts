import {MappedCode} from "@/model/terminologyModels";
import {integer} from "@volar/language-service";

export interface Rule {
    ruleId: number
    ruleCd: string
    ruleLabel: string
    ruleBadge: string
    codes?: MappedCode[]
}

export interface Cohort {
    code: string
    cdsHooksServiceCode: string
    label: string
    description: string
}

export interface EvaluatorResult {
    patientId: string
    cohort: Cohort
    metRuleCriteria: Rule[]
}

export interface EvaluatorRegistryInstructionResult {
    patientId: string
    instruction: string
    dataElementType: string
    dataElementName: string
    dataElementId: string
    dataElementValue: string
}

export interface EvaluatorState {
    stateId: string
    state: string
    status: string
    startTime: string
    completionTime?: string
    message: string
    resultsFile?: string
    variables?: any
}
export interface EvaluatorInstance {
    evaluatorId: string
    instanceId: string
    outputType: string
    startTime?: string
    completionTime?: string
    populationSize?: integer
    peopleMetCriteria?: integer
    resultsFile?: string
    states?: EvaluatorState[]
}

export interface MeasureProperty {
    isMappedToLocal?: number | undefined
    , isNotMappedToLocal?: number | undefined
    , isMappedToStandard?: number | undefined
    , isNotMappedToStandard?: number | undefined
}
export interface Measure {
    measureLabel: string
    measureValue: string
    properties?: MeasureProperty
}

export interface MeasureCategory {
    measureCategory: string
    measures: Measure[]
    icon?: string
    iconColor?: string
}

export interface Evaluator {
    evaluatorId: string
    title: string
    guideline: string
    instances?: EvaluatorInstance[]
    rules?: Rule[]
    measureCategories?: MeasureCategory[]
}



// type DataTableHeader = {
//   key?: 'data-table-group' | 'data-table-select' | 'data-table-expand' | (string & {})
//   value?: SelectItemKey
//   title?: string
//
//   fixed?: boolean
//   align?: 'start' | 'end' | 'center'
//
//   width?: number | string
//   minWidth?: string
//   maxWidth?: string
//
//   headerProps?: Record<string, any>
//   cellProps?: HeaderCellProps
//
//   sortable?: boolean
//   sort?: DataTableCompareFunction
//   sortRaw?: DataTableCompareFunction
//   filter?: FilterFunction
//
//   children?: DataTableHeader[]
// }

export type MyTableHeader = {
    title: string;
    align?: 'start' | 'end' | 'center' | undefined;
    key: string;
    width?: string, sortable?: boolean | undefined;
    class?: string;
}

export type MySortItem = { key: string, order?: boolean | 'asc' | 'desc' }

export const lookBackOptions = [
    { id: 0, label: 'Today'},
    { id: 1, label: 'Yesterday'},
    { id: 2, label: '2 Days ago'},
    { id: 3, label: '3 Days ago'},
    { id: 7, label: '1 Week ago'},
    { id: 14, label: '2 Weeks ago'},
    { id: 30, label: '1 Month ago'},
    { id: -1, label: 'All'},
]

export const runEvaluationOptions = [
    {
        serviceId: "",
        dataFiles: [],
        outputOptions: [],
    },
]

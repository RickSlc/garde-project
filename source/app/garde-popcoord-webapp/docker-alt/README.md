# docker-alt

Using the resources in this directory provides the ability to run a script on the GARDE garde-popcoord-webapp docker server
before garde starts. This is useful for setting cloud-specific environment variables.

## Instructions

1. Download the garde-popcoord-webapp-spring-boot.jar file from https://bitbucket.org/RickSlc/garde-project/downloads/
and save it to /shared/garde/app.

2. Build the docker image

### Docker image for intel linux
```
docker buildx build --platform linux/amd64 -t rickslc/garde-popcoord-webapp-alt .
```

### Docker image for arm64
#### when built on on osx/mac with an arm64 chip
```
docker build -t rickslc/garde-popcoord-webapp-alt-arm64 .
```

3. To run setup commands on the docker server, add your setup commands to /shared/garde/app/run-before-pop-coord-webapp.sh script before or after the java command that starts GARDE.

4. In the docker-compose.yml file, change the image name accordingly

```yaml
  garde-popcoord-webapp:
    image: rickslc/garde-popcoord-webapp-alt:latest
```

5. Run the docker-compose.sh command. Must be in the same directory as docker-compose.yml

```bash
docker compose up -d garde-popcoord-webapp
```

7. Check to see that your new container is running

```bash
docker container ls
```
 
8. Validate your updates by opening a bash shell on the garde-popcoord-webapp container.

```bash
docker exec -it garde-popcoord-webapp bash

bash-4.2# echo $MYVAR
theValue

```
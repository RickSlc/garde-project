#!/bin/bash

/shared/garde/app/start-popcoord-webapp.sh

# BEFORE GARDE exe - add your commands here

# Example of how to run bash script before gare starts - put your script in the shared directory and reference it here.
# /shared/garde/security/run-before-garde.sh

# Or just run a few commands here. This adds a key from a file (secretKey) to the bash env
# echo "export CLOUD_KEY=\$(cat /shared/garde/security/secretKey)" > /root/.bashrc

# This is the garde-popcoord-webapp server.
# java -Xmx8g -jar app.jar --spring.config.location=file:///shared/garde/config/ | tee /shared/garde/appLogs/garde-popcoord-webapp-$(date +'%Y%m%d_%H%M%S').log

# AFTER GARDE exe - add your command here
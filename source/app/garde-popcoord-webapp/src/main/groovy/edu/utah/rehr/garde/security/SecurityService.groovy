package edu.utah.rehr.garde.security

import edu.utah.rehr.garde.security.repository.AuthorityRepository
import edu.utah.rehr.garde.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service

@Service
@Profile("dbsecurity")
class SecurityService {

    @Autowired
    UserRepository userRepository

    @Autowired
    AuthorityRepository authorityRepository

    void makeAdmin(String username) {
        Optional<User> u = userRepository.findByUsernameIgnoreCaseEquals(username)
        if (u.isEmpty()) {
            throw new IllegalArgumentException("User not found.")
        }

        Authority admin = getAuthority("ROLE_ADMIN").get()
        if (u.get().authorities.contains(admin)) {
            return
        }
        User user = u.get()
        user.authorities.add(admin)
        userRepository.save(user)
    }

    void removeAdmin(String username) {
        Optional<User> u = userRepository.findByUsernameIgnoreCaseEquals(username)
        if (u.isEmpty()) {
            throw new IllegalArgumentException("User not found.")
        }

        Authority admin = getAuthority("ROLE_ADMIN").get()
        if (!u.get().authorities.contains(admin)) {
            return
        }
        User user = u.get()
        user.authorities.remove(admin)
        userRepository.save(user)
    }

    List<User> findUsersByAuthority(String authorityName) {
        Optional<Authority> optional = getAuthority(authorityName)
        if (optional.isEmpty()) {
            throw new IllegalArgumentException("Could not find authority: ${authorityName}")
        }
        return userRepository.findAllByAuthorities(optional.get())
    }

    Optional<Authority> getAuthority(String auth) {
        return authorityRepository.findByName(auth)
    }

    Authority addAuthority(Authority authority) {
        return authorityRepository.save(authority)
    }

    User currentUser() {
        String username = currentUsername()
        if (!username) {
            return null
        }
        return userRepository.findByUsernameIgnoreCaseEquals(username).get()
    }

    String currentUsername()  {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal()

        if (principal instanceof UserDetails) {
            return ((UserDetails)principal).getUsername()
        } else {
            return principal.toString()
        }
    }
}


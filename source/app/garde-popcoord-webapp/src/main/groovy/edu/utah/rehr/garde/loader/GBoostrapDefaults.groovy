package edu.utah.rehr.garde.loader

import groovy.util.logging.Log
import org.springframework.beans.factory.InitializingBean
import org.springframework.stereotype.Component

@Component
@Log
class GBoostrapDefaults implements InitializingBean {


    @Override // place holder - pick and set profiles here
    void afterPropertiesSet() throws Exception {
        log.info("INITIALIZE - BOOTSTRAP AFTER PROPERTIES SET")
    }
}

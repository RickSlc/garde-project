package edu.utah.rehr.garde.config

import edu.utah.rehr.garde.security.SessionListener
import jakarta.servlet.http.HttpSessionListener
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class SessionConfig {

    @Value('${server.servlet.session.timeout:900}')
    int timeoutSeconds

    @Bean
    HttpSessionListener httpSessionListener() {
        return new SessionListener(timeoutSeconds)
    }

}

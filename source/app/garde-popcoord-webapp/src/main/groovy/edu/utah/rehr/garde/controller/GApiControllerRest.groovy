package edu.utah.rehr.garde.controller

import edu.utah.rehr.garde.GardePopulationCoordinatorApplication
import edu.utah.rehr.garde.db.model.EvaluatorDTO
import edu.utah.rehr.garde.db.model.EvaluatorInstanceState
import edu.utah.rehr.garde.db.model.PatientMetRuleCriteriaDTO
import edu.utah.rehr.garde.domain.core.accessory.UResult
import edu.utah.rehr.garde.domain.core.model.dto.RegistryInstructionDTO
import edu.utah.rehr.garde.domain.cdshooks.ICdsHooksEvaluator
import edu.utah.rehr.garde.impl.cdshooks.NccnCdsHooksRequestFactory
import edu.utah.rehr.garde.model.SystemStatus
import edu.utah.rehr.garde.service.GEvaluatorService
import edu.utah.rehr.garde.service.GFileService
import edu.utah.rehr.garde.service.GSystemCheckService
import groovy.util.logging.Log
import jakarta.servlet.http.HttpServletResponse
import org.json.simple.JSONObject
import org.opencds.hooks.model.discovery.Services
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.env.Environment
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.StopWatch
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

import java.time.LocalDateTime


@RestController
@Log
class GApiControllerRest {

    @Autowired
    private Environment env

    @Autowired
    private GEvaluatorService evaluatorService

    @Autowired
    private ICdsHooksEvaluator cdsHooksEvaluator

    @Autowired
    private GSystemCheckService systemCheckService

    @Autowired
    private NccnCdsHooksRequestFactory nccnCdsHooksRequestFactory

    @Value('${garde.popcoord.endpoint}')
    private String populationCoordinatorEndpoint

    @Value('${garde.websocket.endpoint}')
    private String websocketEndpoint

    @Value('${rehr.nccn.cdshooks.server.url}')
    private String cdsHooksEndpoint

    @Value('${garde.dataProvider.files.root.directory}')
    private String rootDirectory

    @Value('${garde.dataProvider.files.input.directory}')
    private String inputDirectory

    @Value('${garde.dataProvider.files.output.directory}')
    private String outputDirectory

    @Value('${garde.data.page.size:10000}')
    private String bulkPageSize

    GApiControllerRest() {}

    @GetMapping("/sanity")
    @CrossOrigin(origins = "*")
    String hello() {
        log.info("SANITY CHECK")
        return "Garde is alive."
    }

    @GetMapping("/settings")
    @CrossOrigin(origins = "*")
    Map<String, String> settings() {
        var map = new HashMap<String, String>()
        map.put("populationCoordinatorEndpoint", populationCoordinatorEndpoint)
        map.put("websocketEndpoint", websocketEndpoint)
        map.put("cdsHooksEndpoint", cdsHooksEndpoint)

        return map
    }

    @GetMapping("/cds-services")
    @CrossOrigin(origins = "*")
    ResponseEntity<Services> cdsServices() {
        log.info("CHECK CDS SERVICES")
        UResult<Services> result = cdsHooksEvaluator.getServices()
        if (!result.success()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND)
        }
        return new ResponseEntity<>(result.getResult(), HttpStatus.OK)
    }

//    @GetMapping("/cdsServices")
//    @CrossOrigin(origins = "*")
//    Services cdsServices2() {
//        return cdsServices()
//    }

    @GetMapping("/getEvaluatorRuleAndCodes/{serviceId}")
    @CrossOrigin(origins = "*")
    String getEvaluatorRuleCode(@PathVariable("serviceId") String serviceId) {
        log.info("FETCH EVALUATOR RULE CODES")
        JSONObject dataObj = evaluatorService.getEvaluatorRuleCodesByServiceId(serviceId)
        return dataObj.toJSONString()
    }

    @GetMapping("/getEvaluationStates/{serviceId}/{lookBackId}")
    @CrossOrigin(origins = "*")
    EvaluatorDTO getServiceInstanceStates(@PathVariable("serviceId") String serviceId, @PathVariable("lookBackId") Integer lookBackId) {
        return evaluatorService.getEvaluatorInstances(serviceId, lookBackId)
    }

    @GetMapping("/getEvaluatorInstance/{instanceId}")
    @CrossOrigin(origins = "*")
    EvaluatorDTO getServiceInstanceState(@PathVariable("instanceId") String instanceId) {
        return evaluatorService.getEvaluatorInstance(instanceId)
    }

    @GetMapping("/restart")
    @CrossOrigin(origins = "*")
    String restart() {
        GardePopulationCoordinatorApplication.restart()
        return "RESTARTING"
    }

    @GetMapping("/resultsReview/MET_CRITERIA_OUTPUT_TYPE/{instanceId}")
    @CrossOrigin(origins = "*")
    List<PatientMetRuleCriteriaDTO> resultsReview(@PathVariable("instanceId") String instanceId) {
        return evaluatorService.getPatientMetCriteriaByInstanceId(instanceId)
    }

    @GetMapping("/resultsReview/REGISTRY_INSTRUCTION_OUTPUT_TYPE/{instanceId}")
    @CrossOrigin(origins = "*")
    List<RegistryInstructionDTO> registryInstructionsResultsReview(@PathVariable("instanceId") String instanceId) {
        return evaluatorService.getRegistryInstructionsByInstanceId(instanceId)
    }

    @GetMapping("/downloadResults/{fileName}")
    @ResponseBody
    @CrossOrigin(origins = "*")
    void downloadResults(@PathVariable("fileName") String fileName, HttpServletResponse response) {
        String filePath = rootDirectory + outputDirectory + "/" + fileName
        log.info("DOWNLOAD: " + filePath)
        GFileService.writeToFileResponse(response, filePath)
    }

    @GetMapping("/resultsReviewPage/MET_CRITERIA_OUTPUT_TYPE/{instanceId}/{page}/{pageSize}")
    @CrossOrigin(origins = "*")
    List<PatientMetRuleCriteriaDTO> resultsReviewPage(
            @PathVariable("instanceId") String instanceId
            , @PathVariable("page") Integer page
            , @PathVariable("pageSize") Integer pageSize) {
        return evaluatorService.getPatientMetCriteriaByInstanceId(instanceId)
    }

    @GetMapping("/resultsReviewPage/REGISTRY_INSTRUCTION_OUTPUT_TYPE/{instanceId}/{page}/{pageSize}")
    @CrossOrigin(origins = "*")
    List<RegistryInstructionDTO> registryInstructionsResultsReviewPage(
            @PathVariable("instanceId") String instanceId
            , @PathVariable("page") Integer page
            , @PathVariable("pageSize") Integer pageSize) {
        return evaluatorService.getRegistryInstructionsByInstanceId(instanceId)
    }

    @GetMapping("/systemStatus")
    @CrossOrigin(origins = "*")
    List<SystemStatus> systemStatus() {
        return systemCheckService.systemStatusTests()
    }

    @PostMapping("/runEvaluation")
    @CrossOrigin(origins = "*")
    String runEvaluation(
            @RequestParam("serviceId") String serviceId
            , @RequestParam("instanceId") String instanceId
            , @RequestParam("outputType") String outputType
            , @RequestParam("patDemogFile") MultipartFile patDemogFile
            , @RequestParam("patFHxFile") MultipartFile patFHxFile
            , @RequestParam(value = "patLdlObsFile", required = false) MultipartFile patLdlObsFile

    ) {
        log.info("RUNNING EVALUATION")

        EvaluatorInstanceState evaluationStatus = new EvaluatorInstanceState()
        evaluationStatus.instanceId = instanceId
        evaluationStatus.serviceId = serviceId
        evaluationStatus.state = "Evaluation initiated"
        evaluationStatus.status = EvaluatorInstanceState.COMPLETED
        evaluationStatus.startTime = LocalDateTime.now()
        evaluationStatus.completionTime = LocalDateTime.now()
        evaluationStatus.message = "Data files: " + patDemogFile.originalFilename + ", " + patFHxFile.originalFilename
        evaluationStatus.message += "; Output type " + outputType

        String inputDirectory = getInputDirectory()
        String patientDTO_filePath = inputDirectory + patDemogFile.originalFilename
        String familyHxDTO_filePath = inputDirectory + patFHxFile.originalFilename

        File storedDemographicsFile = new File(patientDTO_filePath)
        patDemogFile.transferTo(storedDemographicsFile)

        File storedFamilyHxFile = new File(familyHxDTO_filePath)
        patFHxFile.transferTo(storedFamilyHxFile)

        Map<String, String> params = new HashMap<>()
        params.put("instanceId", instanceId)
        params.put("outputType", outputType)
        params.put("fhirServiceEndpoint", env.getProperty("rehr.nccn.fhir.server.url"))
        params.put("cdsServiceId", serviceId)
        params.put("patientDTO_filePath", patientDTO_filePath)
        params.put("familyHxDTO_filePath", familyHxDTO_filePath)
        params.put("bulkPageSize", bulkPageSize)

        evaluationStatus.setVariable("outputType", outputType)
        evaluationStatus.setVariable("inputDemographicsFilename", patDemogFile.originalFilename)
        evaluationStatus.setVariable("inputPatientFamilyHxFilename", patFHxFile.originalFilename)
        evaluationStatus.setVariable("storedPatientDemographicsFilePath", patientDTO_filePath)
        evaluationStatus.setVariable("storedPatientFamilyHxFilePath", familyHxDTO_filePath)
        evaluationStatus.setVariable("bulkPageSize", bulkPageSize)

        if (patLdlObsFile != null) {

            String patientLdlObsDTO_filePath = inputDirectory + patLdlObsFile.originalFilename
            File storedObsFile = new File(patientLdlObsDTO_filePath)
            patLdlObsFile.transferTo(storedObsFile)

            evaluationStatus.message += "," + patLdlObsFile.originalFilename
            evaluationStatus.setVariable("inputLdlObservationsFilename", patLdlObsFile.originalFilename)
            evaluationStatus.setVariable("storedLdlObservationsFilePath", patientLdlObsDTO_filePath)

            params.put("patientLdlObsDTO_filePath", patientLdlObsDTO_filePath) // add file path to method call params
        }

        evaluatorService.saveStateMessage(evaluationStatus)
        evaluationStatus.jsonVariables = null

        StopWatch stopWatch = new StopWatch()
        stopWatch.start()

        String returnMessage = "NO MESSAGE"

        try {
            UResult<String> evaluationResult = evaluatorService.evaluate(params)

            stopWatch.stop()

            if (evaluationResult.success() == true) {

                evaluationStatus.state = "Evaluation completed"
                evaluationStatus.status = EvaluatorInstanceState.COMPLETED
                evaluationStatus.completionTime = LocalDateTime.now()
                evaluationStatus.message = String.format("Completed in %.2f sec or %.2f min. Results in file %s", stopWatch.getTotalTimeSeconds(), (stopWatch.getTotalTimeSeconds() / 60), evaluationResult.getResult())
                evaluationStatus.setVariable("resultsFile", evaluationResult.getResult())

                evaluatorService.saveStateMessage(evaluationStatus)

                return "{ \"resultsFile\": \"${evaluationResult.getResult()}\", \"message\": \"Evaluation for " + serviceId + " completed in " + stopWatch.getTotalTimeSeconds() + " seconds\" }"

            } else {
                // set for below
                returnMessage = evaluationResult.getMessage()
            }
        } catch (Exception e) {
            log.warning("EVAL FAILED")
            e.printStackTrace()
            returnMessage = e.message
        }

        nccnCdsHooksRequestFactory.flushInstance(serviceId, instanceId)

        if (returnMessage.length() >= 255) returnMessage = returnMessage.substring(0, 254)

        if (stopWatch.isRunning()) {
            stopWatch.stop()
        }

        evaluationStatus.state = "Evaluation failed"
        evaluationStatus.status = EvaluatorInstanceState.FAILED
        evaluationStatus.completionTime = LocalDateTime.now()
        evaluationStatus.message = returnMessage.replace("\"","")
        evaluationStatus.setVariable("outputType", outputType)

        evaluatorService.saveStateMessage(evaluationStatus)

        return "{ \"resultsFile\": \"NONE\", \"message\": \"Evaluation for " + serviceId + " FAILED after " + stopWatch.getTotalTimeSeconds() + " seconds.\" }"
    }

    private String getInputDirectory() {
        String inputDirectory = rootDirectory + inputDirectory + "/"
        inputDirectory
    }

}

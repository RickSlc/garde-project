package edu.utah.rehr.garde.security

import edu.utah.rehr.garde.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional


@Service("userDetailsService")
@Profile("dbsecurity")
class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository

    @Transactional(readOnly = true)
    @Override
    UserDetails loadUserByUsername(final String username)
            throws UsernameNotFoundException {

        Optional<User> user = userRepository.findByUsernameIgnoreCaseEquals(username)
        if (user.isPresent()) {
            return user.get()
        }
        throw new UsernameNotFoundException("Username not found.")
    }
}

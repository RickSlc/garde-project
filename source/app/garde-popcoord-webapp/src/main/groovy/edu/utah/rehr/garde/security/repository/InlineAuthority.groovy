package edu.utah.rehr.garde.security.repository

import edu.utah.rehr.garde.security.Authority
import edu.utah.rehr.garde.security.User
import org.springframework.data.rest.core.config.Projection

@Projection(name = "inlineAuthority", types = [ User.class ])
interface InlineAuthority {
    String getUsername();

    String getEmail();

    String getFirstName();

    String getLastName();

    boolean getEnabled();

    boolean getAdmin();

    Set<Authority> getAuthorities();
}

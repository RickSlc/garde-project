package edu.utah.rehr.garde.config

import edu.utah.rehr.garde.channel.GardeChannel
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.socket.server.standard.ServerEndpointExporter

@Configuration
class WebSocketConfig {

    @Bean
    ServerEndpointExporter serverEndpointExporter() {
        ServerEndpointExporter serverEndpointExporter = new ServerEndpointExporter()
        serverEndpointExporter.setAnnotatedEndpointClasses(GardeChannel.class)
        return serverEndpointExporter
    }

}

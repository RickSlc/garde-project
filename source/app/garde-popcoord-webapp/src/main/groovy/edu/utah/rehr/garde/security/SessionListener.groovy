package edu.utah.rehr.garde.security

import edu.utah.rehr.garde.channel.GardeChannel
import groovy.util.logging.Log
import jakarta.servlet.http.HttpSessionEvent
import jakarta.servlet.http.HttpSessionListener

@Log
class SessionListener implements HttpSessionListener {

    private int timeoutSeconds

    SessionListener(int timeoutSeconds) {
        this.timeoutSeconds = timeoutSeconds
    }

    @Override
    void sessionCreated(HttpSessionEvent se) {
        se.getSession().setMaxInactiveInterval(timeoutSeconds)
    }

    @Override
    void sessionDestroyed(HttpSessionEvent se) {
        log.info("LOGGING OUT")
        GardeChannel.push("SESSION_EXPIRED")
    }
}

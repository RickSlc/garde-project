package edu.utah.rehr.garde.config

import edu.utah.rehr.garde.db.GardeDbConfig
import edu.utah.rehr.garde.domain.cdshooks.factory.GCdsHooksRequestFactory
import edu.utah.rehr.garde.domain.core.accessory.UResultStatus
import edu.utah.rehr.garde.domain.core.model.GEvaluator
import edu.utah.rehr.garde.domain.core.model.fhir.r4.FamilyMemberHistoryHelperR4
import edu.utah.rehr.garde.domain.core.model.fhir.r4.PatientHelperR4
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService
import edu.utah.rehr.garde.domain.etl.provider.IFamilyHxDTOProvider
import edu.utah.rehr.garde.domain.etl.provider.IPatientDTOProvider
import edu.utah.rehr.garde.domain.etl.service.ICsvService
import edu.utah.rehr.garde.domain.cdshooks.ICdsHooksRequestFactory
import edu.utah.rehr.garde.domain.cdshooks.utils.FhirR4JsonUtil
import edu.utah.rehr.garde.impl.cdshooks.NccnCdsHooksRequestFactory
import edu.utah.rehr.garde.impl.cdshooks.NccnCdsHooksRequestFactoryR4
import edu.utah.rehr.garde.impl.ehr.config.EhrClientConfig
import edu.utah.rehr.garde.impl.etl.provider.FamilyHxDTOProviderCsv
import edu.utah.rehr.garde.impl.etl.provider.PatientDTOProviderCsv
import edu.utah.rehr.garde.impl.etl.service.CsvService
import org.opencds.hooks.lib.json.JsonUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.core.env.Environment
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@Configuration
@EntityScan(basePackages=[
        "edu.utah.rehr.garde.db.model"
        , "edu.utah.rehr.garde.terminology.db.model"
        , "edu.utah.rehr.garde.security"
        , "edu.utah.rehr.garde.model"])
@EnableJpaRepositories(basePackages = [
        "edu.utah.rehr.garde.db.repository"
        , "edu.utah.rehr.garde.terminology.db.repository"
        , "edu.utah.rehr.garde.security.repository"
        ,"edu.utah.rehr.garde.repository"])
@Import([EhrClientConfig.class, GardeDbConfig.class])
class GardePopulationCoordinatorAppConfig {

    @Autowired
    private Environment env

    @Autowired
    private ICodeService codeService

    @Bean
    @Qualifier("cdsHooksServerUrl")
    String cdsHooksServerUrl() {
        return env.getProperty("rehr.nccn.cdshooks.server.url")
    }

    @Bean
    ICsvService csvService() {
        return new CsvService()
    }

    @Bean
    IPatientDTOProvider patientDTOProvider() {
        return new PatientDTOProviderCsv()
    }

    @Bean
    NccnCdsHooksRequestFactory nccnCdsHooksRequestFactory() {

        ICsvService csvService = new CsvService()
        IFamilyHxDTOProvider familyHxDTOProvider = new FamilyHxDTOProviderCsv(csvService)

        Map<String, ICdsHooksRequestFactory> factories = new HashMap<>()

        NccnCdsHooksRequestFactoryR4 nccnCdsHooksRequestFactoryR4 = new NccnCdsHooksRequestFactoryR4(
                patientHelperR4()
                , familyMemberHistoryHelperR4()
                , patientDTOProvider()
                , familyHxDTOProvider
        )

        GCdsHooksRequestFactory cdsHooksRequestFactory = new GCdsHooksRequestFactory(codeService)
        factories.put(GEvaluator.GENETIC_TESTING_RECOMMENDER_R4_2023.serviceId, nccnCdsHooksRequestFactoryR4)
        factories.put(GEvaluator.GENETIC_TESTING_RECOMMENDER_R4.serviceId, nccnCdsHooksRequestFactoryR4)
        factories.put(GEvaluator.FH_GENETIC_TESTING_RECOMMENDER_R4_2018.serviceId, cdsHooksRequestFactory)

        return new NccnCdsHooksRequestFactory(factories)
    }

    @Bean
    UResultStatus uResultStatus() {
        return UResultStatus.WARNING
    }

    @Bean
    JsonUtil jsonUtilR4() {
        return new FhirR4JsonUtil()
    }

    @Bean
    IFamilyHxDTOProvider familyHxDTOProvider() {
        return new FamilyHxDTOProviderCsv(csvService())
    }

    @Bean
    FamilyMemberHistoryHelperR4 familyMemberHistoryHelperR4() {
        return new FamilyMemberHistoryHelperR4(codeService)
    }

    @Bean
    PatientHelperR4 patientHelperR4() {
        return new PatientHelperR4(codeService)
    }

}

package edu.utah.rehr.garde.service

import edu.utah.rehr.garde.domain.core.model.dto.CategoryCodeAnalyticsDTO
import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxDTO
import edu.utah.rehr.garde.domain.core.model.dto.MeasureCategoryDTO
import edu.utah.rehr.garde.domain.core.terminology.CategoricalCodeAnalytics
import edu.utah.rehr.garde.domain.core.terminology.Code
import edu.utah.rehr.garde.domain.core.terminology.CodeAnalytics
import edu.utah.rehr.garde.impl.etl.provider.FamilyHxDTOProviderCsv
import edu.utah.rehr.garde.impl.etl.service.CsvService
import edu.utah.rehr.garde.terminology.db.model.Association
import edu.utah.rehr.garde.terminology.db.model.TCodeAssociation
import edu.utah.rehr.garde.terminology.db.repository.TCodeAssociationRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class GPatientDataAnalysisService {

    private TCodeAssociationRepository codeAssociationRepository

    @Autowired
    GPatientDataAnalysisService(TCodeAssociationRepository codeAssociationRepository) {
        this.codeAssociationRepository = codeAssociationRepository
    }

    List<MeasureCategoryDTO> getFamilyHxCodeMeasures(String filePath) {

        CategoricalCodeAnalytics codeSummary = getFamilyHxCodeSummary(filePath)

        return codeSummary.toMeasureCategoryDTO()
    }


    List<CategoryCodeAnalyticsDTO> getFamilyHxCategoryCodeAnalytics(String filePath) {

        CategoricalCodeAnalytics categoricalCodeAnalytics = getFamilyHxCodeSummary(filePath)
        var categoryCodeAnalyticsDTOList = categoricalCodeAnalytics.toCategoryCodeAnalyticsDTO()

        for (CategoryCodeAnalyticsDTO categoryCodeAnalyticsDTO:categoryCodeAnalyticsDTOList) {

            for (CodeAnalytics codeAnalytics:categoryCodeAnalyticsDTO.codeAnalytics) {

                List<TCodeAssociation> ehrAssoc = codeAssociationRepository.findByAssociationCdAndRsCode_codeSystemAndRsCode_code(
                        Association.EHR_CODE_IS.toString()
                        , codeAnalytics.code.codeSystem
                        , codeAnalytics.code.code
                )

                codeAnalytics.isMappedAsEhrCode = ehrAssoc.size()
                if (ehrAssoc.size() > 0) {
                    codeAnalytics.code.bindingCode = ehrAssoc.get(0).lsCode.code
                }

                List<TCodeAssociation> standardAssoc = codeAssociationRepository.findByAssociationCdAndRsCode_codeSystemAndRsCode_code(
                        Association.STANDARD_CODE_IS.toString()
                        , codeAnalytics.code.codeSystem
                        , codeAnalytics.code.code
                )

                codeAnalytics.isMappedAsStandardCode = standardAssoc.size()
                if (standardAssoc.size() > 0) {
                    codeAnalytics.code.bindingCode = standardAssoc.get(0).lsCode.code
                }
            }

        }

        return categoryCodeAnalyticsDTOList
    }

    private CategoricalCodeAnalytics getFamilyHxCodeSummary(String filePath) {

        FamilyHxDTOProviderCsv familyHxDTOProvider = new FamilyHxDTOProviderCsv(new CsvService())
        familyHxDTOProvider.setFamilyHxFilePath(filePath)
        List<FamilyHxDTO> familyHxDTOList = familyHxDTOProvider.getFamilyHxDTOs()
        CategoricalCodeAnalytics codeSummary = new CategoricalCodeAnalytics()

        for (FamilyHxDTO fHx : familyHxDTOList) {
            codeSummary.add("Condition", new Code(fHx.conditionCodeSystem, fHx.conditionCode, fHx.conditionLabel))
            codeSummary.add("Relationship", new Code(fHx.relationshipCodeSystem, fHx.relationshipCode, fHx.relationshipLabel))
        }

        return codeSummary
    }

}

package edu.utah.rehr.garde.model

class SystemStatus {

    static String SYSTEM_STATUS_UP = 'up'
    static String SYSTEM_STATUS_DOWN = 'down'

    String systemCd
    String systemLabel
    String status
    String endpoint

}

package edu.utah.rehr.garde.channel

import groovy.util.logging.Log
import jakarta.websocket.*
import jakarta.websocket.CloseReason.CloseCodes
import jakarta.websocket.server.ServerEndpoint

import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap

@ServerEndpoint(value = "/channel/evaluators")
@Log
class GardeChannel {

    static final ConcurrentMap<String, GardeChannel> CHANNELS = new ConcurrentHashMap<>()

    private Session session

    @OnMessage
    void onMessage(String message) {

        log.info("WEBSOCKET MESSAGE RECEIVED:" + message)

        try {
            this.session.close(new CloseReason(CloseCodes.TOO_BIG, "This endpoint does not accept client messages"))
        } catch (IOException e) {
            log.severe("Connection close error:,id=" + this.session.getId() + " err=" +  e.getMessage())
        }
    }

    @OnOpen
    void onOpen(Session session, EndpointConfig endpointConfig) {
        this.session = session
        this.session.setMaxIdleTimeout(0)
        CHANNELS.put(this.session.getId(), this)

        log.info("NEW WEBSOCKET CLIENT CONNECTION: ID = " + this.session.getId())
    }

    @OnClose
    void onClose(CloseReason closeReason) {
        log.info("WEBSOCKET CLOSED: ID=" + this.session.getId()+ " REASON=" + closeReason)
        CHANNELS.remove(this.session.getId())
    }

    @OnError
    void onError(Throwable throwable) throws IOException {
        log.info("WEBSOCKET ERROR: ID=" + this.session.getId() + " ERR=" + throwable)
        this.session.close(new CloseReason(CloseCodes.UNEXPECTED_CONDITION, throwable.getMessage()))
    }

    /**
     * Push messages to all clients
     *
     * @param message
     */
    static void push(Object message) {

        CHANNELS.values().stream().forEach(channel -> {
            synchronized (channel) {
                try {
                    if (channel.session.isOpen()) {
                        channel.session.getBasicRemote().sendText(message.toString())
                    }
                } catch (IOException io) {
                    io.printStackTrace()
                }
            }
        })

    }

}

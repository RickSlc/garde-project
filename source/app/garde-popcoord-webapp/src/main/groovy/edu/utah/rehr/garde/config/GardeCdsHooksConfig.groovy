package edu.utah.rehr.garde.config

import edu.utah.rehr.cdshooks.server.config.OpenCdsHooksConfig
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.Profile

@Configuration
@Import(OpenCdsHooksConfig.class)
@ComponentScan(basePackages = [
        "edu.utah.rehr.cdshooks.server.controller"
])
@Profile("cdsingarde")
class GardeCdsHooksConfig {
}

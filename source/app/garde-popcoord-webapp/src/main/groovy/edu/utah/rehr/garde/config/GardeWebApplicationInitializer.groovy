package edu.utah.rehr.garde.config

import groovy.util.logging.Log
import jakarta.servlet.ServletContext
import jakarta.servlet.ServletException
import org.springframework.web.WebApplicationInitializer

//@Configuration
@Log
class GardeWebApplicationInitializer implements WebApplicationInitializer {

    @Override
    void onStartup(ServletContext servletContext) throws ServletException {
        log.info("STARTUP GardeWebApplicationInitializer")
    }

}

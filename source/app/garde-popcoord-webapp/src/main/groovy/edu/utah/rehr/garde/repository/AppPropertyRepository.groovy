package edu.utah.rehr.garde.repository

import edu.utah.rehr.garde.model.AppProperty
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.CrossOrigin

@Repository
@RepositoryRestResource(collectionResourceRel = "property", path = "property")
@CrossOrigin(origins = "*")
interface AppPropertyRepository extends JpaRepository<AppProperty, String> {}

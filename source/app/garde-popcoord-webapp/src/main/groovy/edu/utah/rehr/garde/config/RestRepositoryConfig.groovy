package edu.utah.rehr.garde.config

import edu.utah.rehr.garde.db.model.EvaluatorInstanceState
import edu.utah.rehr.garde.db.model.RuleCodeView
import edu.utah.rehr.garde.model.AppProperty
import edu.utah.rehr.garde.security.User
import edu.utah.rehr.garde.terminology.db.model.TCode
import edu.utah.rehr.garde.terminology.db.model.TCodeAssociation
import edu.utah.rehr.garde.terminology.db.model.TCodeAssociationFlat
import edu.utah.rehr.garde.terminology.db.model.TValueSetFlat
import org.springframework.context.annotation.Configuration
import org.springframework.data.rest.core.config.RepositoryRestConfiguration
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer
import org.springframework.web.servlet.config.annotation.CorsRegistry

@Configuration
class RestRepositoryConfig implements RepositoryRestConfigurer {

    @Override
    void configureRepositoryRestConfiguration(RepositoryRestConfiguration config, CorsRegistry cors) {

        cors.addMapping("/*")
                .allowedOrigins("*")
                .allowedMethods("GET", "PUT", "DELETE", "POST")
                .allowCredentials(false).maxAge(3600)

        config.exposeIdsFor(AppProperty.class
                , RuleCodeView.class
                , EvaluatorInstanceState.class
                , TCodeAssociationFlat.class
                , TCodeAssociation.class
                , TValueSetFlat.class
                , TCode.class
                , User.class
        )
    }
}

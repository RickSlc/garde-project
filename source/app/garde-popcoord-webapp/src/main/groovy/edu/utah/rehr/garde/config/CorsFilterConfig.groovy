package edu.utah.rehr.garde.config

import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component

import jakarta.servlet.*
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse

@Component
@Profile('local')
class CorsFilterConfig implements Filter {

    @Override
    void init(FilterConfig filterConfig) throws ServletException {
        // Initialization logic if required
    }

    @Override
    void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req
        HttpServletResponse response = (HttpServletResponse) res

        response.setHeader("Access-Control-Allow-Origin", "http://localhost:3000")
        response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
        response.setHeader("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
        response.setHeader("Access-Control-Allow-Credentials", "true")

        // Handle preflight requests immediately
        if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
            response.setStatus(HttpServletResponse.SC_OK)
            return
        }

        chain.doFilter(req, res)
    }

    @Override
    void destroy() {
        // Cleanup logic if required
    }
}
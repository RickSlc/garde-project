package edu.utah.rehr.garde.security

import jakarta.persistence.*
import org.springframework.context.annotation.Profile
import org.springframework.security.core.GrantedAuthority

@Entity
@Table(name="g_authority")
@Profile("dbsecurity")
class Authority  implements  GrantedAuthority{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    Long id

    String name

    @ManyToMany(mappedBy = "authorities")
    Collection<User> users

    @Override
    String getAuthority() {
        return name
    }

    @Override
    String toString() {
        return new StringJoiner(", ", Authority.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .toString()
    }
}

package edu.utah.rehr.garde.security

import jakarta.persistence.*
import org.springframework.context.annotation.Profile
import org.springframework.security.core.userdetails.UserDetails

@Entity
@Table(name="g_user")
@Inheritance(strategy = InheritanceType.JOINED)
@Profile("dbsecurity")
class User implements UserDetails {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id

    @Column(nullable = false, unique = true)
    private String username

    @Column(nullable = false)
    private String password

    @Column(nullable= false, unique = true)
    private String email

    @Column(nullable=true, length = 500, name="first_name")
    String firstName

    @Column(nullable=true, length = 500, name="last_name")
    String lastName

    @Column
    private boolean enabled


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "g_security_user_authority",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "authority_id", referencedColumnName = "id"))
    private Set<Authority> authorities


    int getId() {
        return id
    }

    void setId(int id) {
        this.id = id
    }

    String getPassword() {
        return password
    }

    void setPassword(String password) {
        this.password = password
    }

    String getUsername() {
        return username
    }

    void setUsername(String username) {
        this.username = username
    }

    String getEmail() {
        return email
    }

    void setEmail(String email) {
        this.email = email
    }

    boolean getEnabled() {
        return enabled
    }

    void setEnabled(boolean enabled) {
        this.enabled = enabled
    }


    Collection<Authority> getAuthorities() {
        return authorities
    }

    void setAuthorities(Collection<Authority> authorities) {
        this.authorities = authorities
    }



    boolean isAdmin() {
        return authorities?.find{ it.name == "ROLE_ADMIN"} ? true : false
    }

    @Override
    boolean isAccountNonExpired() {
        return true
    }

    @Override
    boolean isAccountNonLocked() {
        return true
    }

    @Override
    boolean isCredentialsNonExpired() {
        return true
    }

    @Override
    boolean isEnabled() {
        return enabled
    }


    @Override
    String toString() {
        return new StringJoiner(", ", User.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("username='" + username + "'")
                .add("password='" + password + "'")
                .add("email='" + email + "'")
                .add("firstName='" + firstName + "'")
                .add("lastName='" + lastName + "'")
                .add("enabled=" + enabled)
                .add("authorities=" + authorities)
                .toString()
    }
}

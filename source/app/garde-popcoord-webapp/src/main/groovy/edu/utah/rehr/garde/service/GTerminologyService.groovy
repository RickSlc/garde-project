package edu.utah.rehr.garde.service

import edu.utah.rehr.garde.db.model.EvaluatorDTO
import edu.utah.rehr.garde.db.repository.EvaluatorEntityRepository
import edu.utah.rehr.garde.db.repository.MeasureCodeRepository
import edu.utah.rehr.garde.domain.core.accessory.UResult
import edu.utah.rehr.garde.domain.core.model.GEvaluator
import edu.utah.rehr.garde.domain.core.model.dto.MeasureCategoryDTO
import edu.utah.rehr.garde.domain.core.model.dto.MeasureDTO
import edu.utah.rehr.garde.domain.core.terminology.CodeSetDefinition
import edu.utah.rehr.garde.domain.core.terminology.provider.csv.TerminologyReaderCsv
import edu.utah.rehr.garde.domain.core.terminology.service.CodeServiceByMaps
import edu.utah.rehr.garde.domain.cdshooks.ICdsHooksEvaluator
import edu.utah.rehr.garde.terminology.db.loader.TerminologyLoader
import edu.utah.rehr.garde.terminology.db.model.*
import edu.utah.rehr.garde.terminology.db.provider.CodeServiceJpaMapsProvider
import edu.utah.rehr.garde.terminology.db.repository.*
import edu.utah.rehr.garde.terminology.db.service.TerminologyExportCsv
import groovy.util.logging.Log
import jakarta.transaction.Transactional
import lombok.NonNull
import org.json.simple.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service

import java.util.stream.Collectors

@Service
@Log
class GTerminologyService {

    @Autowired
    Environment environment

    @Autowired
    TUriBindingRepository uriBindingRepository

    @Autowired
    private TCodeRepository codeRepository

    @Autowired
    private TCodeAssociationRepository codeAssociationRepository

    @Autowired
    private TCodeAssociationFlatRepository codeAssociationFlatRepository

    @Autowired
    private TValueSetFlatRepository valueSetFlatRepository

    @Autowired
    private CodeServiceByMaps codeServiceByMaps

    @Autowired
    private TerminologyExportCsv terminologyCsvProvider

    @Autowired
    private TerminologyLoader terminologyLoader

    @Autowired
    private ICdsHooksEvaluator cdsHooksEvaluator

    @Autowired
    private CodeServiceJpaMapsProvider codeServiceJpaMapsProvider

    @Autowired
    private EvaluatorEntityRepository evaluatorEntityRepository

    @Autowired
    private MeasureCodeRepository measureCodeRepository

    @Value("garde.dataProvider.files.root.directory")
    String gardeRootDir

    String gardeTerminologyDir() {
        return gardeRootDir + "/terminology/"
    }

    @Transactional
    void deleteCodesByAssociation(Association associationCode) {
        // identify local codes by their associations
        List<TCodeAssociation> ehrCodeAssociations = codeAssociationRepository.findByAssociationCd(associationCode.toString())
        var codesToDelete = new HashSet<TCode>()
        for (TCodeAssociation association: ehrCodeAssociations) {
            codesToDelete.add(association.rsCode)
            codeAssociationRepository.delete(association)
        }
        codeRepository.deleteAll(codesToDelete)
    }

    EvaluatorDTO getEvaluatorTerminologySummary(String serviceId) {

        var evaluator = evaluatorEntityRepository.findByServiceId(serviceId).toEvaluatorDTO()
        var categories = new ArrayList<MeasureCategoryDTO>()

        categories.add(getCodeMeasureCategory(serviceId))
//        categories.add(getCodeSetsMeasureCategory(serviceId))
//        categories.add(getAssociationsMeasureCategory(serviceId))

        evaluator.measureCategories = categories
        return evaluator
    }

    MeasureCategoryDTO getCodeMeasureCategory(String serviceId) {

        var codesMeasureCategory = new MeasureCategoryDTO(measureCategory: "Code Mapping Summary")
        var codeMeasures = new ArrayList<MeasureDTO>()

        var evaluatorCodes = measureCodeRepository.findEvaluatorCodesByServiceId(serviceId)
        var evaluatorCodeMeasure = new MeasureDTO(measureLabel: "GARDE codes", measureValue: "" + evaluatorCodes.size())
        codeMeasures.add(evaluatorCodeMeasure)

        var evaluatorGardeCodesMappedToEhrCodes = measureCodeRepository.findEvaluatorGardeCodesMappedToEhrCodesByServiceId(serviceId)
        var evaluatorGardeCodesMappedToEhrCodeMeasure = new MeasureDTO(measureLabel: "GARDE codes -> EHR codes", measureValue: "" + evaluatorGardeCodesMappedToEhrCodes.size())
        var evaluatorGardeCodesMappedToEhrCodeMeasureProps = new Properties()
        evaluatorGardeCodesMappedToEhrCodeMeasureProps.setProperty("isMappedToLocal","1")
        evaluatorGardeCodesMappedToEhrCodeMeasure.properties = evaluatorGardeCodesMappedToEhrCodeMeasureProps
        codeMeasures.add(evaluatorGardeCodesMappedToEhrCodeMeasure)

        var evaluatorEhrCodes = measureCodeRepository.findEvaluatorEhrCodesByServiceId(serviceId)
        var evaluatorEhrCodeMeasure = new MeasureDTO(measureLabel: "EHR codes -> GARDE codes", measureValue: "" + evaluatorEhrCodes.size())
        var evaluatorEhrCodeMeasureProps = new Properties()
        evaluatorEhrCodeMeasureProps.setProperty("isMappedToLocal","1")
        evaluatorEhrCodeMeasure.properties = evaluatorEhrCodeMeasureProps
        codeMeasures.add(evaluatorEhrCodeMeasure)

        var evaluatorNotMappedToEhrCodes = measureCodeRepository.findEvaluatorCodesNotMappedToEhrCodesByServiceId(serviceId)
        var evaluatorNotMappedToEhrCodeMeasure = new MeasureDTO(measureLabel: "GARDE codes NOT mapped to EHR codes", measureValue: "" + evaluatorNotMappedToEhrCodes.size())
        var evaluatorNotMappedToEhrCodeMeasureProps = new Properties()
        evaluatorNotMappedToEhrCodeMeasureProps.setProperty("isNotMappedToLocal","1")
        evaluatorNotMappedToEhrCodeMeasure.properties = evaluatorNotMappedToEhrCodeMeasureProps
        codeMeasures.add(evaluatorNotMappedToEhrCodeMeasure)

        var evaluatorGardeCodesMappedToStandardCodes = measureCodeRepository.findEvaluatorGardeCodesMappedToStandardCodesByServiceId(serviceId)
        var evaluatorGardeCodesMappedToStandardCodeMeasure = new MeasureDTO(measureLabel: "GARDE codes -> Standard codes", measureValue: "" + evaluatorGardeCodesMappedToStandardCodes.size())
        var evaluatorGardeCodesMappedToStandardCodeMeasureProps = new Properties()
        evaluatorGardeCodesMappedToStandardCodeMeasureProps.setProperty("isMappedToStandard","1")
        evaluatorGardeCodesMappedToStandardCodeMeasure.properties = evaluatorGardeCodesMappedToStandardCodeMeasureProps
        codeMeasures.add(evaluatorGardeCodesMappedToStandardCodeMeasure)

        var evaluatorStandardCodes = measureCodeRepository.findEvaluatorStandardCodesByServiceId(serviceId)
        var evaluatorStandardCodeMeasure = new MeasureDTO(measureLabel: "Standard codes -> GARDE codes", measureValue: "" + evaluatorStandardCodes.size())
        var evaluatorStandardCodeMeasureProps = new Properties()
        evaluatorStandardCodeMeasureProps.setProperty("isMappedToStandard","1")
        evaluatorStandardCodeMeasure.properties = evaluatorStandardCodeMeasureProps
        codeMeasures.add(evaluatorStandardCodeMeasure)

        var evaluatorNotMappedToStandardCodes = measureCodeRepository.findEvaluatorCodesNotMappedToStandardCodesByServiceId(serviceId)
        var evaluatorNotMappedToStandardCodeMeasure = new MeasureDTO(measureLabel: "GARDE codes NOT mapped to Standard codes", measureValue: "" + evaluatorNotMappedToStandardCodes.size())
        var evaluatorNotMappedToStandardCodeMeasureProps = new Properties()
        evaluatorNotMappedToStandardCodeMeasureProps.setProperty("isNotMappedToStandard","1")
        evaluatorNotMappedToStandardCodeMeasure.properties = evaluatorNotMappedToStandardCodeMeasureProps
        codeMeasures.add(evaluatorNotMappedToStandardCodeMeasure)

        var codeSets = measureCodeRepository.findEvaluatorCodeSetsByServiceId(serviceId)
        var measureDTO = new MeasureDTO(measureLabel: "GARDE code sets", measureValue: codeSets.size().toString(), )
        codeMeasures.add(measureDTO)

        codesMeasureCategory.measures = codeMeasures

        return codesMeasureCategory
    }

    MeasureCategoryDTO getCodeSetsMeasureCategory(String serviceId) {
        var codeSetsMeasureCategory = new MeasureCategoryDTO(measureCategory: "Code Sets")
        var measures = new ArrayList<MeasureDTO>()
        var codeSets = measureCodeRepository.findEvaluatorCodeSetsByServiceId(serviceId)
        var measureDTO = new MeasureDTO(measureLabel: "GARDE code sets", measureValue: codeSets.size().toString(), )
        measures.add(measureDTO)

        codeSetsMeasureCategory.measures = measures
        return codeSetsMeasureCategory
    }

    static MeasureCategoryDTO getAssociationsMeasureCategory(String serviceId) {
        var associationsMeasureCategory = new MeasureCategoryDTO(measureCategory: "Associations")

        return associationsMeasureCategory
    }

    List<CodeSetDTO> getCodeSets() {

        Map<TCode, List<CodeBindingDTO>> codeSetMap = new HashMap<>()
        List<TValueSetFlat> valueSetFlatList = valueSetFlatRepository.findAllByOrderByVsCodeAscMemberBindingCodeAsc()

        for (TValueSetFlat valueSetFlat: valueSetFlatList) {

            Optional<TCode> oCodeSetCode = codeRepository.findById(valueSetFlat.vsCodeId)

            if (oCodeSetCode.isEmpty()) {
                log.warning("CODE SET LOOKUP FAIL: " + valueSetFlat.vsCodeId)
                continue
            }

            List<CodeBindingDTO> codeBindingDTOList = codeSetMap.get(oCodeSetCode.get())

            if (!codeBindingDTOList) { // create the set
                codeBindingDTOList = new ArrayList<>()
            }

            Optional<TCode> oCode = codeRepository.findById(valueSetFlat.codeId)

            CodeBindingDTO newCodeBindingDTO = new CodeBindingDTO(
                    memberAssociation: valueSetFlat.association
                    , codeBinding: valueSetFlat.memberBindingCode
                    , code: oCode.get()
            )

            codeBindingDTOList.add(newCodeBindingDTO)
            codeSetMap.put(oCodeSetCode.get(), codeBindingDTOList)

        }

        return toCodeSetDTO(codeSetMap)
    }

    static List<CodeSetDTO> toCodeSetDTO(Map<TCode, List<CodeBindingDTO>> codeSetMap) {

        List<CodeSetDTO> codeSetDTOList = new ArrayList<>()
        List<TCode> codeSetList = codeSetMap.keySet().stream().sorted((vs1,vs2) ->
            vs1.code.compareTo(vs2.code)).collect(Collectors.toList())

        for (TCode csCode: codeSetList) {
            CodeSetDTO codeSetDTO = new CodeSetDTO(
                codeSetGid: csCode.gid
                    , codeSystem: csCode.codeSystem
                    , code: csCode.code
                    , codeSetLabel: csCode.codeLabel
                    , members: codeSetMap.get(csCode)
            )
            codeSetDTOList.add(codeSetDTO)
        }

        return codeSetDTOList
    }

    static List<CodeSetDefinition> getCodeSetDefinitions() {

        List<CodeSetDefinition> codeSetDefinitions = new ArrayList<>()
        for (String[] row: TerminologyReaderCsv.getCodeSetDefinitions("classpath:/terminology/garde-code-set-definitions.csv")) {
            codeSetDefinitions.add(new CodeSetDefinition(row[0], row[1], row[2]))
        }
        return codeSetDefinitions
    }

    Boolean refreshTerminologyCache() {
        log.info("REFRESH POPULATION COORDINATOR TERMINOLOGY")
        codeServiceByMaps.setStandardCodeBindingsMap(codeServiceJpaMapsProvider.getStandardCodeBindingsMap().getResult())
        codeServiceByMaps.setEhrCodeBindingsMap(codeServiceJpaMapsProvider.getEhrCodeBindingsMap().getResult())
        codeServiceByMaps.setUriBindingsMap(codeServiceJpaMapsProvider.getUriBindingsMap().getResult())

        // Need to refresh on cds hooks serve too if it is running
        UResult<Boolean> cdsHooksIsAliveResult = cdsHooksEvaluator.isAlive()
        if (cdsHooksIsAliveResult.success() == true) {
            cdsHooksEvaluator.refreshTerminology()
        } else {
            log.info("CDS HOOKS NOT RUNNING - NO TERMINOLOGY REFRESH REQUIRED.")
        }
        return cdsHooksIsAliveResult.result
    }

    String deleteCodeAssociation(Long id) {

        Optional<TCodeAssociation> oCodeAssoc = codeAssociationRepository.findById(id)

        if (oCodeAssoc.isPresent()) {
            TCodeAssociation codeAssoc = oCodeAssoc.get()
            TCode rsCode = codeAssoc.rsCode
            codeAssociationRepository.delete(codeAssoc)
            codeRepository.delete(rsCode)
            return "CodeAssociation " + id + " DELETED"
        }

        return "CodeAssociation " + id + " NOT FOUND"
    }

    TBindingCodeAssociationDTO saveBindingCodeAssociation(TBindingCodeAssociationDTO bindingCodeAssociationDTO) {

        Optional<TCode> oBindingCode = codeRepository.findByCodeSystemAndCode("http://utah.edu/rehr/nccn", bindingCodeAssociationDTO.bindingCode)
        TCode bindingCode = oBindingCode.get()
        Optional<TCode> oCode = codeRepository.findByCodeSystemAndCode(bindingCodeAssociationDTO.codeSystem, bindingCodeAssociationDTO.code)

        TCode persistedCode
        if (oCode.isPresent()) {
            persistedCode = oCode.get()
            persistedCode.codeLabel = bindingCodeAssociationDTO.codeLabel
            persistedCode = codeRepository.save(persistedCode)
        }
        else { // persistedCode is null or not present, create new
            TCode newCode = new TCode(
                    codeSystem: bindingCodeAssociationDTO.codeSystem
                    , code: bindingCodeAssociationDTO.code
                    , codeLabel: bindingCodeAssociationDTO.codeLabel
            )
            persistedCode = codeRepository.save(newCode)
        }

        Long codeAssocId = (bindingCodeAssociationDTO.codeAssociationId != null) ? bindingCodeAssociationDTO.codeAssociationId : -1
        Optional<TCodeAssociation> oCodeAssociation = codeAssociationRepository.findById(codeAssocId)

        if (oCodeAssociation.isEmpty()) {

            TCodeAssociation newAssoc = new TCodeAssociation(
                    lsCode: bindingCode
                    , rsCode: persistedCode
                    , associationCd: bindingCodeAssociationDTO.association
            )

            codeAssociationRepository.save(newAssoc)
        } else { // check association
            TCodeAssociation codeAssociation = oCodeAssociation.get()
            if (bindingCodeAssociationDTO.association != codeAssociation.associationCd) {
                codeAssociation.associationCd = bindingCodeAssociationDTO.association
                codeAssociationRepository.save(codeAssociation)
            }
        }

        TBindingCodeAssociationDTO persistedDTO = new TBindingCodeAssociationDTO(
                bindingCode: bindingCode.code
                , codeId: persistedCode.gid
                , codeSystem: persistedCode.codeSystem
                , code: persistedCode.code
                , codeLabel: persistedCode.codeLabel
                , association: bindingCodeAssociationDTO.association
        )

        return persistedDTO
    }

    UResult<List<TBindingCodeMappings>> getBindingCodeMappings(String serviceId) {

        try {
            List<TBindingCodeMappings> bindingCodeMappingsList = new ArrayList<>()
            List<TCode> bindingCodes = codeRepository.findRuleCodesByServiceId(serviceId)

            for (TCode bindingCode : bindingCodes) {

                List<TCodeAssociationFlat> associatedCodes = codeAssociationFlatRepository.findByLsCodeSystemAndLsCode(bindingCode.codeSystem, bindingCode.code)
                TBindingCodeMappings newMappings = new TBindingCodeMappings(bindingCode: bindingCode)
                List<TAssociatedCode> mappings = new ArrayList<>()

                for (TCodeAssociationFlat assoc : associatedCodes) {
                    Optional<TCode> oMappedCode = codeRepository.findById(assoc.rsCodeId)
                    mappings.add(new TAssociatedCode(codeAssociationGid: assoc.codeAssociationId, association: assoc.associationCd, code:oMappedCode.get()))
                }
                newMappings.mappings = mappings
                bindingCodeMappingsList.add(newMappings)
            }

            return UResult.successResult(bindingCodeMappingsList)
        } catch (Exception e) {
            return UResult.errorResult(e)
        }

    }

    UResult<Boolean> loadUpdatesTerminologyFile(Association association, String filePath) {

        try {
            UResult<Boolean> isValidHeaderResult = TerminologyReaderCsv.isValidHeader(TerminologyReaderCsv.CODE_BINDING_HEADER, filePath)

            if (isValidHeaderResult.success() == true) {
                var loadResult = terminologyLoader.loadBindingCodeAssociationUpdates(association, filePath)
                refreshTerminologyCache()
                return loadResult
            } else {
                if (isValidHeaderResult.getMessage()) {
                    return isValidHeaderResult
                }
                return UResult.errorResult("Error - the terminology file header should contain ${TerminologyReaderCsv.CODE_BINDING_HEADER}")
            }
        } catch (Exception e) {
            return UResult.errorResult(e)
        }
    }

    UResult<Boolean> loadNewTerminologyFile(@NonNull String serviceId, Association association, @NonNull String filePath) {

        GEvaluator evaluator = GEvaluator.findByServiceId(serviceId)

        try {
            UResult<Boolean> isValidHeaderResult = TerminologyReaderCsv.isValidHeader(TerminologyReaderCsv.CODE_BINDING_HEADER, filePath)

            if (isValidHeaderResult.getResult() == true) {
                var loadResult = terminologyLoader.loadBindingCodeAssociations(evaluator, association, filePath)
                refreshTerminologyCache()
                return loadResult
            } else {
                if (isValidHeaderResult.getMessage()) {
                    return isValidHeaderResult
                }
                return UResult.errorResult("Error - the terminology file header should contain ${TerminologyReaderCsv.CODE_BINDING_HEADER}")
            }
        } catch (Exception e) {
            return UResult.errorResult(e)
        }
    }

    UResult<Boolean> exportTerminology() {

        Optional<TUriBinding> oUriBinding = uriBindingRepository.findById("REHR_NCCN_CODE_SYSTEM_URI")
        if (oUriBinding.isEmpty()) {
            return UResult.errorResult("URI key REHR_NCCN_CODE_SYSTEM_URI MISSING IN URI REPO.")
        }

        String codeSetUri = oUriBinding.get().uri

        if (true) {
            List<TCodeAssociationFlat> ehrAssociations = codeAssociationFlatRepository.findByAssociationCd(Association.EHR_CODE_IS.toString())
            terminologyCsvProvider.exportBindingCodeMappings(ehrAssociations, gardeTerminologyDir() + "ehr-code-bindings.csv")
        }

        if (true) {
            List<TCodeAssociationFlat> standardAssociations = codeAssociationFlatRepository.findByAssociationCd(Association.STANDARD_CODE_IS.toString())
            terminologyCsvProvider.exportBindingCodeMappings(standardAssociations, gardeTerminologyDir() + "standard-code-bindings.csv")
        }

        if (true) {
            List<TCodeAssociationFlat> codeSetAssociations = codeAssociationFlatRepository.findByLsCodeSystemAndAssociationCdAndRsCodeSystemOrderByLsCode(
                    codeSetUri
                    , Association.HAS_MEMBER.toString()
                    , codeSetUri
            )
            terminologyCsvProvider.exportCodeSets(codeSetAssociations, gardeTerminologyDir() + "code-sets.csv")
        }

        return UResult.successResult(true)
    }

    List<TCodeAssociation> getCodeSet(TCode code) {
        List<TCodeAssociation> codeAssociationList = codeAssociationRepository.findByLsCode_codeSystemAndLsCode_codeAndAssociationCd(
                code.codeSystem
                , code.code
                , "HAS_MEMBER"
        )
        return codeAssociationList
    }

    JSONObject toMappedCodeJson(TCode code) {
        JSONObject codeObj = new JSONObject()
        codeObj.put("codeId", code.gid)
        codeObj.put("codeSystem", code.codeSystem)
        codeObj.put("code", code.code)
        codeObj.put("codeLabel", code.codeLabel)

        List<TCodeAssociationFlat> standardCodeAssociations = codeAssociationFlatRepository.findByLsCodeSystemAndLsCodeAndAssociationCd(
                code.codeSystem
                , code.code
                , Association.STANDARD_CODE_IS.code
        )

        List<TCodeAssociationFlat> localCodeAssociations = codeAssociationFlatRepository.findByLsCodeSystemAndLsCodeAndAssociationCd(
                code.codeSystem
                , code.code
                , Association.EHR_CODE_IS.code
        )

        List<TCodeAssociationFlat> codeSetAssociations = codeAssociationFlatRepository.findByLsCodeSystemAndLsCodeAndAssociationCd(
                code.codeSystem
                , code.code
                , Association.HAS_MEMBER.code
        )

        codeObj.put("isMappedToStandard", standardCodeAssociations.size() > 0)
        codeObj.put("isMappedToLocal", localCodeAssociations.size() > 0)
        codeObj.put("isCodeSet", codeSetAssociations.size() > 0)

        return codeObj
    }

}

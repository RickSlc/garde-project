package edu.utah.rehr.garde


import org.springframework.boot.ApplicationArguments
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.ConfigurableApplicationContext

@SpringBootApplication()
//@EnableConfigurationProperties(SmartDataElementProperties.class)
class GardePopulationCoordinatorApplication {

    private static ConfigurableApplicationContext context

    static void main(String[] args) {
        context = SpringApplication.run(GardePopulationCoordinatorApplication, args)
    }

    static void restart() {
        ApplicationArguments args = context.getBean(ApplicationArguments.class)

        Thread thread = new Thread(() -> {
            context.close()
            context = SpringApplication.run(GardePopulationCoordinatorApplication, args.getSourceArgs())
        })

        thread.setDaemon(false)
        thread.start()
    }


}

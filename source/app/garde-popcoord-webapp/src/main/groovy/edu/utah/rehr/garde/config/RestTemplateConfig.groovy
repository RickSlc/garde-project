package edu.utah.rehr.garde.config

import org.apache.http.conn.ssl.SSLConnectionSocketFactory
import org.apache.http.impl.client.HttpClients
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.web.client.RestTemplate

import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager
import java.security.SecureRandom
import java.security.cert.X509Certificate

@Configuration
class RestTemplateConfig {

    @Value('${garde.security.selfsignedcertificate:false}')
    private boolean selfSignedCertificate

    @Bean
    @Qualifier("cdsHooksRestTemplate")
    RestTemplate cdsHooksRestTemplate() throws Exception {

        if (selfSignedCertificate == false) {
            return new RestTemplate()
        }

        // Configure trust manager to accept self-signed certificates (for development only)
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            void checkClientTrusted(X509Certificate[] x509Certificates, String authType) {}

            @Override
            void checkServerTrusted(X509Certificate[] x509Certificates, String authType) {}

            @Override
            X509Certificate[] getAcceptedIssuers() {
                return null
            }
        }}

        SSLContext sslContext = SSLContext.getInstance("TLS")
        sslContext.init(null, trustAllCerts, new SecureRandom())

        // Create RestTemplate with HttpComponentsClientHttpRequestFactory
        HttpComponentsClientHttpRequestFactory requestFactory =
                new HttpComponentsClientHttpRequestFactory()
        requestFactory.setHttpClient(HttpClients.custom()
                .setSSLSocketFactory(new SSLConnectionSocketFactory(sslContext))
                .build())

        return new RestTemplate(requestFactory)
    }
}

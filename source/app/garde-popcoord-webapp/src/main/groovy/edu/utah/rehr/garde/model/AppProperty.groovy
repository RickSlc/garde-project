package edu.utah.rehr.garde.model

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table

@Entity
@Table(name="g_app_property")
class AppProperty {

    @Id
    @Column(name="property_cd", nullable = false)
    String propertyName

    @Column(name="property_value")
    String propertyValue

    @Column(name="instructions")
    String instructions

}

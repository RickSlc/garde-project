package edu.utah.rehr.garde.loader

import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component

import java.util.logging.Logger

@Component
class GPropertiesLoader implements ApplicationListener<ApplicationEnvironmentPreparedEvent> {

    private Logger log = Logger.getLogger(this.getClass().getName())

    @Override
    void onApplicationEvent(ApplicationEnvironmentPreparedEvent event) {
        log.info("PREPARING AND SETTING PROPERTIES")

//        ConfigurableEnvironment environment = event.getEnvironment()
//        Properties props = new Properties()
//        props.put("spring.datasource.url", "<my value>");
//        environment.getPropertySources().addFirst(new PropertiesPropertySource("myProps", props));

    }
}

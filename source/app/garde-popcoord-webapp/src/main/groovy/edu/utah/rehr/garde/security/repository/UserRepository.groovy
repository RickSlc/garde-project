package edu.utah.rehr.garde.security.repository

import edu.utah.rehr.garde.security.Authority
import edu.utah.rehr.garde.security.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository

@Repository
@RepositoryRestResource(collectionResourceRel = "users", path = "user", excerptProjection = InlineAuthority.class)
interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> findByUsernameIgnoreCaseEqualsOrEmailIgnoreCaseEquals(String usernameOrEmail, String usernameOrEmail2)

    Optional<User> findByUsernameIgnoreCaseEquals(String usernameOrEmail)

    List<User> findAllByAuthorities(Authority authority)
}

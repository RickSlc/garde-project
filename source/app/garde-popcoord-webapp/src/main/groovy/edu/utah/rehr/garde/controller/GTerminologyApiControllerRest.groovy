package edu.utah.rehr.garde.controller

import edu.utah.rehr.garde.db.model.EvaluatorDTO
import edu.utah.rehr.garde.domain.core.accessory.UResult
import edu.utah.rehr.garde.domain.core.accessory.UResultStatus
import edu.utah.rehr.garde.domain.core.model.dto.CategoryCodeAnalyticsDTO
import edu.utah.rehr.garde.domain.core.model.dto.MeasureCategoryDTO
import edu.utah.rehr.garde.domain.core.terminology.CodeSetDefinition
import edu.utah.rehr.garde.model.GardeServiceResponseDTO
import edu.utah.rehr.garde.service.GFileService
import edu.utah.rehr.garde.service.GPatientDataAnalysisService
import edu.utah.rehr.garde.service.GTerminologyService
import edu.utah.rehr.garde.terminology.db.model.Association
import edu.utah.rehr.garde.terminology.db.model.CodeSetDTO
import edu.utah.rehr.garde.terminology.db.model.TBindingCodeAssociationDTO
import edu.utah.rehr.garde.terminology.db.model.TBindingCodeMappings
import groovy.util.logging.Log
import org.apache.commons.lang3.exception.ExceptionUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
@Log
class GTerminologyApiControllerRest {

    @Autowired
    private GTerminologyService terminologyService

    @Autowired
    private GPatientDataAnalysisService patientDataAnalysisService

    @Value('${garde.dataProvider.files.root.directory}')
    private String rootDirectory

    @Value('${garde.dataProvider.files.input.directory}')
    private String inputDirectory

    @Value('${garde.dataProvider.files.output.directory}')
    private String outputDirectory

    String getInputDirectory() {
        return "${rootDirectory}${inputDirectory}/"
    }

    @GetMapping("/refreshTerminology")
    @CrossOrigin(origins = "*")
    String refreshTerminology() {
        terminologyService.refreshTerminologyCache()
        return "SUCCESS"
    }

    @GetMapping("/exportTerminology")
    @CrossOrigin(origins = "*")
    String exportTerminology() {
        terminologyService.exportTerminology()
        return "SUCCESS"
    }

    @GetMapping("/codeSetDefinitions")
    @CrossOrigin(origins = "*")
    List<CodeSetDefinition> getCodeSetDefinitions() {
        return terminologyService.getCodeSetDefinitions()
    }

    @GetMapping("/codeAssociations/delete/{id}")
    @CrossOrigin(origins = "*")
    String deleteCodeAssociation(@PathVariable("id") Long id) {
        return terminologyService.deleteCodeAssociation(id)
    }

    @PostMapping("/codeAssociations/update")
    @CrossOrigin(origins = "*")
    TBindingCodeAssociationDTO saveCodeAssociation(@RequestBody TBindingCodeAssociationDTO bindingCodeAssociationDTO) {

        TBindingCodeAssociationDTO updatedBindingCodeAssociationDTO = terminologyService.saveBindingCodeAssociation(bindingCodeAssociationDTO)
        return updatedBindingCodeAssociationDTO
    }

    @GetMapping("/bindingCodeMappings/{serviceId}")
    @CrossOrigin(origins = "*")
    List<TBindingCodeMappings> getBindingCodeMappings(@PathVariable("serviceId") String serviceId) {

        UResult<List<TBindingCodeMappings>> result = terminologyService.getBindingCodeMappings(serviceId)

        if (result.success()) {
            return result.getResult()
        } else {
            throw result.getException()
        }

    }

    @GetMapping("/codeSets")
    @CrossOrigin(origins = "*")
    Set<CodeSetDTO> getCodeSets() {
        return terminologyService.getCodeSets()
    }

    @GetMapping("/terminologySummary/{serviceId}")
    @CrossOrigin(origins = "*")
    EvaluatorDTO terminologySummary(@PathVariable("serviceId") String serviceId) {
        return terminologyService.getEvaluatorTerminologySummary(serviceId)
    }

    @PostMapping("/analyzePatientDataCodes")
    @CrossOrigin(origins = "*")
    List<CategoryCodeAnalyticsDTO> analyzePatientDataCodes(
            @RequestParam("dataType") String dataType
            , @RequestParam("patientDataFile") MultipartFile patientDataFile) {

        log.info("ANALYZE PATIENT DATA CODES: " + dataType + ":" + patientDataFile.originalFilename)

        try {
            if (dataType == "Patient_Family_Hx") {
                String filePath = GFileService.getDatedSystemFilePath(getInputDirectory(), dataType)
                GFileService.saveFile(filePath, patientDataFile)
                return patientDataAnalysisService.getFamilyHxCategoryCodeAnalytics(filePath)
            }
        } catch (Exception e){
            CategoryCodeAnalyticsDTO categoryCodeAnalyticsDTO = new CategoryCodeAnalyticsDTO()
            categoryCodeAnalyticsDTO.errorMessage = ExceptionUtils.getMessage(e)
            categoryCodeAnalyticsDTO.category = "ERROR"
            var array = new ArrayList<CategoryCodeAnalyticsDTO>()
            array.add(categoryCodeAnalyticsDTO)
            return array
        }

        return new ArrayList<CategoryCodeAnalyticsDTO>()
    }

    @PostMapping("/analyzePatientData")
    @CrossOrigin(origins = "*")
    List<MeasureCategoryDTO> analyzePatientData(
            @RequestParam("dataType") String dataType
            , @RequestParam("patientDataFile") MultipartFile patientDataFile) {

        log.info("ANALYZE PATIENT DATA: " + dataType + ":" + patientDataFile.originalFilename)

        if (dataType == "patientFamilyHx") {
            String filePath = GFileService.getDatedSystemFilePath(getInputDirectory(), "PatientFamilyHx")
            GFileService.saveFile(filePath, patientDataFile)
            return patientDataAnalysisService.getFamilyHxCodeMeasures(filePath)
        }

        return new ArrayList<MeasureCategoryDTO>()
    }

    @PostMapping("/importTerminology")
    @CrossOrigin(origins = "*")
    GardeServiceResponseDTO importTerminology(
            @RequestParam("serviceId") String serviceId
            , @RequestParam("importStrategy") String importStrategy
            , @RequestParam("importCodeType") String importCodeType
            , @RequestParam("terminologyFile") MultipartFile terminologyFile) {

        log.info("IMPORT TERMINOLOGY: " + importCodeType + ":" + importStrategy + ":" + terminologyFile.originalFilename)

        Association association = Association.EHR_CODE_IS // default - reset below otherwise

        if (importStrategy == "delete") {

            String filePath = null
            if (importCodeType == "importLocal") {
                filePath = GFileService.getDatedSystemFilePath(getInputDirectory(), "Imported-EHR-Codes")
            } else if (importCodeType == "importStandards") {
                filePath = GFileService.getDatedSystemFilePath(getInputDirectory(), "Imported-Standard-Codes")
                association = Association.STANDARD_CODE_IS
            }

            GFileService.saveFile(filePath, terminologyFile)
            terminologyService.deleteCodesByAssociation(association)
            UResult<Boolean> loadTerminologyResult = terminologyService.loadNewTerminologyFile(serviceId, association, filePath)

            String responseMessage = "Terminology loaded" // default - reset below otherwise

            if (loadTerminologyResult.resultStatus == UResultStatus.WARNING
                    || loadTerminologyResult.resultStatus == UResultStatus.ERROR
            ) {
                responseMessage = loadTerminologyResult.message
            }

            return new GardeServiceResponseDTO(status: loadTerminologyResult.resultStatus.toString(), message: responseMessage)
        }

        if (importStrategy == "update") {

            String filePath = null
            if (importCodeType == "importLocal") {
                filePath = GFileService.getDatedSystemFilePath(getInputDirectory(),"Imported-Local-Code-Updates")
                terminologyService.loadUpdatesTerminologyFile(Association.EHR_CODE_IS, filePath)
            } else if (importCodeType == "importStandards") {
                filePath = GFileService.getDatedSystemFilePath(getInputDirectory(), "Imported-Standard-Code-Updates")
                association = Association.STANDARD_CODE_IS
            }

            GFileService.saveFile(filePath, terminologyFile)

            UResult<Boolean> loadTerminologyResult = terminologyService.loadUpdatesTerminologyFile(association, filePath)
            String responseMessage = "Terminology loaded"

            if (loadTerminologyResult.resultStatus == UResultStatus.WARNING
                    || loadTerminologyResult.resultStatus == UResultStatus.ERROR
            ) {
                responseMessage = loadTerminologyResult.message
            }

            return new GardeServiceResponseDTO(status: loadTerminologyResult.resultStatus.toString(), message: responseMessage)
        }

        return new GardeServiceResponseDTO(status: "SUCCESS", message: "Terminology ${importStrategy} successful.")
    }


}

package edu.utah.rehr.garde.service

import jakarta.servlet.http.HttpServletResponse
import org.springframework.web.multipart.MultipartFile

import java.nio.file.Files
import java.nio.file.Path
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class GFileService {

    static void writeToFileResponse(HttpServletResponse response, String filePath) throws IOException {
        Path path = Path.of(filePath)

        if (Files.exists(path)) {
            response.setContentType("application/octet-stream")
            response.setContentLength(Files.size(path) as int)
            response.setHeader("Content-Disposition", "attachment filename=" + path.getFileName())

            try (InputStream inputStream = Files.newInputStream(path)
                 OutputStream outputStream = response.getOutputStream()) {
                byte[] buffer = new byte[4096]
                int bytesRead
                while ((bytesRead = inputStream.read(buffer)) > -1) {
                    outputStream.write(buffer, 0, bytesRead)
                }
            }
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND)
        }
    }

    static String getDatedSystemFilePath(String inputDirectory, String fileName) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss")
        String formattedDateTime = LocalDateTime.now().format(formatter)
        StringBuilder filePath = new StringBuilder()
        filePath.append(inputDirectory).append(fileName).append("_").append(formattedDateTime).append(".csv")
        return filePath.toString()
    }

    static void saveFile(String filePath, MultipartFile multipartFile) {
        File storedTerminologyFile = new File(filePath)
        multipartFile.transferTo(storedTerminologyFile)
    }


}

package edu.utah.rehr.garde.model

enum AppPropertyKey {

    GardeServerPort("server.port", "8090", null)
    , CdsHooksEndpoint("rehr.nncn.cdshooks.server.url","http://localhost:8091/opencds-hooks-service/r4/hooks/cds-services/"
            , "The CDS Hooks endpoint.")
    , LocalRootDirectory("local.dataProvider.root.directory","/temp"
    , "Temporary file exchange directory root. The input and output directories are in this directory - /temp/input and /temp/output are the defaults.")
    , LocalInputDirectory("local.dataProvider.input.directory","/input")
    , LocalOutputDirectory("local.dataProvider.output.directory","/output")
    , s3_Profile("s3.profile",null)
    , s3_Region("s3.region","us-east-1")
    , s3_BucketName("s3.dataProvider.bucket","s3-population-coordinator-prod")
    , s3_BucketRootDirectory("s3.dataProvider.root.directory","test")
    , s3_InputDirectory("s3.dataProvider.input.directory","/input")
    , s3_PatientFactsSearchPattern("s3.dataProvider.patientFacts.searchPattern","/PATIENT_2")
    , s3_PatientFamilyHxFactsSearchPattern("s3.dataProvider.patientFacts.searchPattern","/PATIENT_FHX_2")

    String key
    String value
    String instructions

    AppPropertyKey(String key, String value) {
        this.key = key
        this.value = value
    }

    AppPropertyKey(String key, String value, String instructions) {
        this.key = key
        this.value = value
        this.instructions = instructions
    }

}

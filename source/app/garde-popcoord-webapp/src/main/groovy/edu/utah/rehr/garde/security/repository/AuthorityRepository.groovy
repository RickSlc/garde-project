package edu.utah.rehr.garde.security.repository

import edu.utah.rehr.garde.security.Authority
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository

@Repository
@RepositoryRestResource(collectionResourceRel = "authorities", path = "authority")
interface AuthorityRepository extends JpaRepository<Authority, Long> {

    Optional<Authority> findByName(String authority)

}

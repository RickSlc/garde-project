package edu.utah.rehr.garde.service

import edu.utah.rehr.garde.domain.core.accessory.UResult
import edu.utah.rehr.garde.domain.ehr.service.IEhrClientService
import edu.utah.rehr.garde.domain.cdshooks.ICdsHooksEvaluator
import edu.utah.rehr.garde.impl.cdshooks.NccnCdsHooksRequestFactory
import edu.utah.rehr.garde.impl.ehr.service.epic.InterconnectServiceClient
import edu.utah.rehr.garde.model.SystemStatus
import edu.utah.rehr.garde.repository.AppPropertyRepository
import groovy.util.logging.Log
import org.opencds.hooks.model.discovery.Services
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service


@Log
@Service
class GSystemCheckService {

    @Autowired
    private final Environment env

    @Value('${rehr.nccn.cdshooks.server.url}')
    private String cdsHooksEndpoint

    @Value('${garde.popcoord.endpoint}')
    private String popcoordEndpoint

    @Autowired
    private final ICdsHooksEvaluator cdsHooksEvaluator

    @Autowired
    private final AppPropertyRepository appPropertyRepository

    @Autowired
    private final IEhrClientService ehrClientService

    @Autowired
    private NccnCdsHooksRequestFactory nccnCdsHooksRequestFactory

    @Autowired
    private InterconnectServiceClient interconnectServiceClient

//    private String cdsHooksEndpoint() {
//        return env.getProperty("rehr.nncn.cdshooks.server.url")
//    }

    @Value('${epic.interconnectServer:NO_INTERCONNECT_ENDPOINT}')
    private String interconnectEndpoint

    @Value('${spring.datasource.url}')
    private String intermediateDbEndpoint

    private String chatbotEndpoint() {
        return "https://chatbot.bmi.utah.edu:8080/mrRoboto"
    }

//    @Value('${server.port}')
//    private String serverPort
//
//    @Value('${local.dataProvider.root.directory}')
//    String rootDirectory
//
//    @Value('${local.dataProvider.input.directory}')
//    String inputDirectory
//
//    @Value('${local.dataProvider.input.directory}')
//    String outputDirectory
//
//    private String inputDirectory() {
//        String inputDir = rootDirectory + inputDirectory
//        return inputDir
//    }
//
//    private String outputDirectory() {
//        String outputDir = rootDirectory + outputDirectory
//        return outputDir
//    }

    private Boolean interconnectWorking() {
        String userId = ""

        return true
    }

    Services getCdsHooksServices() {

        UResult<Services> servicesResult = cdsHooksEvaluator.services

        if (!servicesResult.success()) {
            log.warning("CDS HOOKS SERVICES ARE NOT CONFIGURED CORRECTLY OR ARE UNAVAILABLE")
            return
        } else {
            log.info("CDS HOOKS SERVER: ALIVE")
        }

        return servicesResult.getResult()
    }

    List<SystemStatus> systemStatusTests() {
        log.info("POPULATION COORDINATOR: ALIVE")

        List<SystemStatus> systemStatusList = new ArrayList<>()
        String cdsHooksStatus = SystemStatus.SYSTEM_STATUS_DOWN
        String dbStatus = SystemStatus.SYSTEM_STATUS_DOWN
        String ehrStatus= SystemStatus.SYSTEM_STATUS_DOWN
        String chatbotStatus = SystemStatus.SYSTEM_STATUS_DOWN

        if (cdsHooksEvaluator?.isAlive()?.getResult()) {
            log.info("CDS HOOKS SERVER: ALIVE")
            cdsHooksStatus = SystemStatus.SYSTEM_STATUS_UP
        } else {
            log.info("CDS HOOKS SERVER: DOWN")
        }

        try {
            if (appPropertyRepository?.findAll()?.size() > 0) {
                log.info("INTERMEDIATE DB SERVER: ALIVE")
                dbStatus = SystemStatus.SYSTEM_STATUS_UP
            } else {
                log.info("INTERMEDIATE DB SERVER: DOWN")
            }
        } catch (Exception e) {
            log.warning("INTERMEDIATE DB NOT RESPONDING")
        }

        // @Todo - enable this when the Epic SDE interface is configured
//        try {
//            String sdeValue = interconnectServiceClient.getSmartDataElementValue("289891", SmartDataElement.NCCN_GENETIC_COUNSELING_CRITERIA_MET_DATE.id);
//            if (sdeValue != null) {
//                ehrStatus= SystemStatus.SYSTEM_STATUS_UP
//            }
//
//            if (interconnectServiceClient.isInterconnectAlive()) {
//                ehrStatus= SystemStatus.SYSTEM_STATUS_UP
//            }
//        } catch (Exception e) {
//            log.warning("EHR CLIENT CONFIG ERROR")
//            log.warning(e.toString())
//        }

        systemStatusList.add(new SystemStatus(
                systemCd: "POPULATION_COORDINATOR"
                , systemLabel: "Population Coordinator"
                , status: SystemStatus.SYSTEM_STATUS_UP
                , endpoint: popcoordEndpoint
        ))

//        systemStatusList.add(new SystemStatus(
//                systemCd: "INT_DB"
//                , systemLabel: "Intermediate DB"
//                , status: dbStatus
//                , endpoint: intermediateDbEndpoint
//        ))

        systemStatusList.add(new SystemStatus(
                systemCd: "CDSHOOKS_SERVER"
                , systemLabel: "CDS Hooks Server"
                , status: cdsHooksStatus
                , endpoint: cdsHooksEndpoint
        ))

//        systemStatusList.add(new SystemStatus(
//                systemCd: "EHR_DATA_SERVICE"
//                , systemLabel: "EHR Data Service"
//                , status: ehrStatus
//                , endpoint: interconnectEndpoint
//        ))
//
//        systemStatusList.add(new SystemStatus(
//                systemCd: "CHATBOT_SERVICES"
//                , systemLabel: "Chatbot Services"
//                , status: chatbotStatus
//                , endpoint: chatbotEndpoint()
//        ))

        return systemStatusList
    }



}

package edu.utah.rehr.garde.loader

import edu.utah.rehr.garde.domain.core.utils.SqlProcessorUtil
import org.apache.commons.lang3.StringUtils
import org.springframework.boot.SpringApplication
import org.springframework.boot.env.EnvironmentPostProcessor
import org.springframework.core.env.ConfigurableEnvironment
import org.springframework.core.env.MapPropertySource
import org.springframework.stereotype.Component

@Component
class GPostProcessorPropertyLoader implements EnvironmentPostProcessor {

    public static String PROPERTY_SOURCE_NAME = "gardeStoredProperties"

    @Override
    void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {

        if (StringUtils.isEmpty(environment.getProperty("spring.datasource.url"))
                || StringUtils.isEmpty(environment.getProperty("spring.datasource.username"))
                || StringUtils.isEmpty(environment.getProperty("spring.datasource.password"))) {
            // loggers not configured yet
            println("NO DB PROPERTIES - CAN'T SET DB-MANAGED PROPERTIES")
            return
        }

        SqlProcessorUtil sqlProcessor = new SqlProcessorUtil(
                environment.getProperty("spring.datasource.url")
                , environment.getProperty("spring.datasource.username")
                , environment.getProperty("spring.datasource.password")
        )

        if (!environment.getProperty("spring.datasource.url").contains("jdbc:sqlserver")) {

            if (sqlProcessor.objectExists("g_app_property", "TABLE")) {
                println("g_app_property TABLE FOUND - PROCESSING GARDE STORED PROPERTIES.")
                // Get props from db
                Map<String, Object> storedProps = sqlProcessor.getStoredAppProperties()
                // Create a custom property source with the highest precedence and add it to Spring Environment
                environment.getPropertySources().addFirst(new MapPropertySource(PROPERTY_SOURCE_NAME, storedProps))
            }
        }

    }

}

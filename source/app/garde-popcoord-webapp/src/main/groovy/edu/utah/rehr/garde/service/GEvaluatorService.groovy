package edu.utah.rehr.garde.service

import edu.utah.rehr.garde.channel.GardeChannel
import edu.utah.rehr.garde.db.model.*
import edu.utah.rehr.garde.db.repository.*
import edu.utah.rehr.garde.domain.core.accessory.UResult
import edu.utah.rehr.garde.domain.core.model.GEvaluator
import edu.utah.rehr.garde.domain.core.model.dto.MetCriteriaDTO
import edu.utah.rehr.garde.domain.core.model.dto.PatientDTO
import edu.utah.rehr.garde.domain.core.model.dto.RegistryInstructionDTO
import edu.utah.rehr.garde.domain.core.model.dto.RuleDTO
import edu.utah.rehr.garde.domain.cdshooks.service.CdsHooksEvaluator
import edu.utah.rehr.garde.domain.cdshooks.service.CdsHooksResponseMetCriteriaInterpreterR4
import edu.utah.rehr.garde.domain.cdshooks.service.CdsHooksResponseRegistryInstructionInterpreter
import edu.utah.rehr.garde.domain.core.provider.DtoProvider
import edu.utah.rehr.garde.impl.cdshooks.NccnCdsHooksRequestFactory
import edu.utah.rehr.garde.impl.etl.provider.MetCriteriaDTOProviderCsv
import edu.utah.rehr.garde.impl.etl.provider.PatientDTOProviderCsv
import edu.utah.rehr.garde.impl.etl.provider.RegistryInstructionDTOProviderCsv
import edu.utah.rehr.garde.impl.etl.service.CsvService
import edu.utah.rehr.garde.terminology.db.model.TCodeAssociation
import edu.utah.rehr.garde.terminology.db.repository.TCodeAssociationFlatRepository
import edu.utah.rehr.garde.terminology.db.repository.TCodeAssociationRepository
import groovy.util.logging.Log
import org.apache.commons.lang3.StringUtils
import org.json.simple.JSONArray
import org.json.simple.JSONObject
import org.opencds.hooks.model.request.CdsRequest
import org.opencds.hooks.model.response.CdsResponse
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.util.StopWatch

import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter

@Service
@Log
class GEvaluatorService {

    private Environment environment

    private final CohortRepository cohortRepository
    private final EvaluatorEntityRepository evaluatorRepository
    private final EvaluatorRuleRepository evaluatorRuleRepository
    private final RuleCodeRepository ruleCodeRepository
    private TCodeAssociationRepository codeAssociationRepository
    private EvaluatorInstanceStateRepository serviceInstanceStateRepository
    private NccnCdsHooksRequestFactory nccnCdsHooksRequestFactory
    private CdsHooksEvaluator cdsHooksEvaluator
    private CdsHooksResponseMetCriteriaInterpreterR4 cdsHooksResponseInterpreter
    private CdsHooksResponseRegistryInstructionInterpreter cdsHooksResponseRegistryInstructionInterpreter
    private TCodeAssociationFlatRepository codeAssociationFlatRepository
    private GTerminologyService gardeTerminologyService

    GEvaluatorService(
            CohortRepository cohortRepository
            , EvaluatorEntityRepository evaluatorRepository
            , EvaluatorRuleRepository evaluatorRuleRepository
            , RuleCodeRepository ruleCodeRepository
            , TCodeAssociationRepository codeAssociationRepository
            , EvaluatorInstanceStateRepository serviceInstanceStateRepository
            , Environment environment
            , NccnCdsHooksRequestFactory nccnCdsHooksRequestFactory
            , CdsHooksEvaluator cdsHooksEvaluator
            , CdsHooksResponseMetCriteriaInterpreterR4 cdsHooksResponseInterpreter
            , CdsHooksResponseRegistryInstructionInterpreter cdsHooksResponseRegistryInstructionInterpreter
            , TCodeAssociationFlatRepository codeAssociationFlatRepository
            , GTerminologyService gardeTerminologyService
    ) {

        this.cohortRepository = cohortRepository
        this.evaluatorRepository = evaluatorRepository
        this.evaluatorRuleRepository = evaluatorRuleRepository
        this.ruleCodeRepository = ruleCodeRepository
        this.codeAssociationRepository = codeAssociationRepository
        this.serviceInstanceStateRepository = serviceInstanceStateRepository
        this.environment = environment
        this.nccnCdsHooksRequestFactory = nccnCdsHooksRequestFactory
        this.cdsHooksResponseInterpreter = cdsHooksResponseInterpreter
        this.cdsHooksResponseRegistryInstructionInterpreter = cdsHooksResponseRegistryInstructionInterpreter
        this.cdsHooksEvaluator = cdsHooksEvaluator
        this.codeAssociationFlatRepository = codeAssociationFlatRepository
        this.gardeTerminologyService = gardeTerminologyService
    }



    JSONObject getEvaluatorRuleCodesByServiceId(String serviceId) {

        EvaluatorEntity evaluator = evaluatorRepository.findByServiceId(serviceId)

        JSONObject root = new JSONObject()

        root.put("evaluatorId", evaluator.serviceId)
        root.put("title", evaluator.title)
        root.put("guideline", evaluator.guideline)

        JSONArray ruleArray = new JSONArray()

        List<EvaluatorRule> evaluatorRules = evaluatorRuleRepository.findByEvaluator_serviceId(serviceId)

        for (EvaluatorRule evaluatorRule: evaluatorRules) {

            JSONObject ruleObj = new JSONObject()
            ruleObj.put("ruleId", evaluatorRule.rule.gid)
            ruleObj.put("ruleCd", evaluatorRule.rule.ruleCd)
            ruleObj.put("ruleLabel", evaluatorRule.rule.ruleLabel)
            ruleObj.put("ruleBadge", evaluatorRule.rule.badgeCd)

            List<RuleCode> ruleCodes = ruleCodeRepository.findByRule_ruleCd(evaluatorRule.rule.ruleCd)
            JSONArray codeArray = new JSONArray()

            for (RuleCode ruleCode:ruleCodes) {

                List<TCodeAssociation> associations = gardeTerminologyService.getCodeSet(ruleCode.code)
                if (!associations.isEmpty() && ruleCode.autoExpand == 1) {

                    for(TCodeAssociation assoc: associations) {
                        JSONObject codeObj = gardeTerminologyService.toMappedCodeJson(assoc.rsCode)
                        codeArray.add(codeObj)
                    }

                } else {
                    JSONObject codeObj = gardeTerminologyService.toMappedCodeJson(ruleCode.code)
                    codeArray.add(codeObj)
                }

            }
            ruleObj.put('codes', codeArray)
            ruleArray.add(ruleObj)
        }
        root.put("rules", ruleArray)
        return root
    }

    EvaluatorDTO getEvaluatorInstance(String instanceId) {

        List<EvaluatorInstanceState> states = serviceInstanceStateRepository.findByInstanceIdOrderByStateId(instanceId)
        Map<String, List<EvaluatorInstanceStateDTO>> instanceStatesMap = getEvaluatorStatesMap(states)

        String serviceId = "none"
        if (!states.isEmpty()) {
            serviceId = states.get(0).serviceId
        }

        List<EvaluatorInstanceDTO> evaluatorInstanceDTOList = getEvaluatorInstanceDTOList(instanceStatesMap, serviceId)
        EvaluatorDTO evaluatorDTO = getEvaluatorDTO(serviceId, evaluatorInstanceDTOList)

        return evaluatorDTO
    }

    EvaluatorDTO getEvaluatorInstances(String serviceId, Integer lookBackCode) {

        List<EvaluatorInstanceState> states = serviceInstanceStateRepository.findAllForServiceWithStartTimeAfter(serviceId, getLookBackDate(lookBackCode))
        Map<String, List<EvaluatorInstanceStateDTO>> instanceStatesMap = getEvaluatorStatesMap(states)
        List<EvaluatorInstanceDTO> evaluatorInstanceDTOList = getEvaluatorInstanceDTOList(instanceStatesMap, serviceId)
        EvaluatorDTO evaluatorDTO = getEvaluatorDTO(serviceId, evaluatorInstanceDTOList)

        return evaluatorDTO
    }

    private EvaluatorDTO getEvaluatorDTO(String serviceId, ArrayList<EvaluatorInstanceDTO> evaluatorInstanceDTOList) {
        EvaluatorEntity evaluator = evaluatorRepository.findByServiceId(serviceId)

        EvaluatorDTO evaluatorDTO = new EvaluatorDTO(
                evaluatorId: serviceId
                , title: evaluator.title
                , guideline: evaluator.guideline
                , instances: evaluatorInstanceDTOList
        )
        evaluatorDTO
    }

    private ArrayList<EvaluatorInstanceDTO> getEvaluatorInstanceDTOList(Map<String, List<EvaluatorInstanceStateDTO>> instanceStatesMap, String serviceId) {
        List<EvaluatorInstanceDTO> evaluatorInstanceDTOList = new ArrayList<>()

        for (String instanceKey : instanceStatesMap.keySet()) {
            List<EvaluatorInstanceStateDTO> instanceStates = instanceStatesMap.get(instanceKey)
            EvaluatorInstanceDTO evaluatorInstanceDTO = getEvaluatorInstanceDTO(serviceId, instanceKey, instanceStates)
            evaluatorInstanceDTOList.add(evaluatorInstanceDTO)
        }
        evaluatorInstanceDTOList
    }

    private Map<String, List<EvaluatorInstanceStateDTO>> getEvaluatorStatesMap(List<EvaluatorInstanceState> states) {
        Map<String, List<EvaluatorInstanceStateDTO>> instanceStatesMap = new HashMap<>()
        for (EvaluatorInstanceState state : states) {

            if (instanceStatesMap.containsKey(state.instanceId)) {
                List<EvaluatorInstanceStateDTO> instanceStates = instanceStatesMap.get(state.instanceId)
                instanceStates.add(state.toServiceInstanceStateDTO())
            } else {
                List<EvaluatorInstanceStateDTO> instanceStates = new ArrayList<>()
                instanceStates.add(state.toServiceInstanceStateDTO())
                instanceStatesMap.put(state.instanceId, instanceStates)
            }
        }
        return instanceStatesMap
    }

    private EvaluatorInstanceDTO getEvaluatorInstanceDTO(String serviceId, String instanceId, List<EvaluatorInstanceStateDTO> instanceStates) {

        LocalDateTime startTime = instanceStates.get(0).startTime
        LocalDateTime completionTime = instanceStates.last().completionTime

        int populationSize = getFirstIntValueFromList(instanceStates, "populationSize")
        int peopleMetCriteria = getLastIntValueFromList(instanceStates, "cumulativePeopleMetCriteria")

        EvaluatorInstanceDTO newEvaluatorInstanceDTO = new EvaluatorInstanceDTO(
                        evaluatorId: serviceId
                        , instanceId: instanceId
                        , outputType: getFirstVariableValueFromStates(instanceStates,"outputType")
                        , startTime: startTime
                        , completionTime: completionTime
                        , populationSize: populationSize
                        , peopleMetCriteria: peopleMetCriteria
                        , resultsFile: getFirstVariableValueFromStates(instanceStates,"resultsFile")
                        , states: instanceStates
        )

        return newEvaluatorInstanceDTO
    }

    private int getFirstIntValueFromList(List<EvaluatorInstanceStateDTO> instanceStates, String varName) {
        String valueString = getFirstVariableValueFromStates(instanceStates, varName)
        int intValue = -1
        if (valueString) {
            intValue = Integer.parseInt(valueString)
        }
        return intValue
    }

    private int getLastIntValueFromList(List<EvaluatorInstanceStateDTO> instanceStates, String varName) {
        String valueString = getLastVariableValueFromStates(instanceStates, varName)
        int intValue = -1
        if (valueString) {
            intValue = Integer.parseInt(valueString)
        }
        return intValue
    }

    private String getFirstVariableValueFromStates(List<EvaluatorInstanceStateDTO> instanceStates, String varName) {

        for (EvaluatorInstanceStateDTO state: instanceStates) {
            if (state.getVariableValue(varName) != null) {
                return state.getVariableValue(varName)
            }
        }
        return null
    }

    private String getLastVariableValueFromStates(List<EvaluatorInstanceStateDTO> instanceStates, String varName) {

        String returnValue = null
        Long maxStateId = null

        for (EvaluatorInstanceStateDTO state : instanceStates) {
            if (state.getVariableValue(varName) != null && (maxStateId == null || maxStateId < state.stateId)) {
                maxStateId = state.stateId
                returnValue = state.getVariableValue(varName)
            }
        }
        return returnValue
    }

    UResult<String> evaluate(Map<String,String> params) {

        StopWatch wholeEvalStopWatch = new StopWatch()
        wholeEvalStopWatch.start()

        if (StringUtils.isEmpty(params.get("cdsServiceId"))) {
            String errorMessage = "REQUEST STOPPED - REQUEST TO EVALUATE POPULATION REQUIRES A cdsServiceId"
            log.severe(errorMessage)
            return UResult.errorResult(errorMessage)
        }

        String serviceId = params.get("cdsServiceId")
        String outputType = params.get("outputType")

        // Set default and check for bulk page size
        Integer bulkPageSize = 10000

        if (StringUtils.isNotEmpty(params.get("bulkPageSize"))) {
            bulkPageSize = Integer.parseInt(params.get("bulkPageSize"))
        }

        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME
        String dateTimeString = LocalDateTime.now().format(formatter)

        String resultFileName = serviceId + "-RESULTS-" + dateTimeString.replace(":","_") + ".csv"
        if ("REGISTRY_INSTRUCTION_OUTPUT_TYPE".equals(outputType)) {
            resultFileName = serviceId + "-REGISTRY-INSTRUCTIONS-" + dateTimeString.replace(":","_") + ".csv"
        }

        String filePath = getMetCriteriaFilePath() + "/" + resultFileName

        EvaluatorInstanceState evaluationStatus = new EvaluatorInstanceState()
        evaluationStatus.instanceId = params.get("instanceId")
        evaluationStatus.serviceId = serviceId

        log.info("RETRIEVING PATIENT DTOs")
        PatientDTOProviderCsv patientDTOProvider = new PatientDTOProviderCsv()
        String patientDTOFilePath = params.get("patientDTO_filePath")
//        List<PatientDTO> patientDTOs = patientDTOProvider.getPatientDTOs(patientDTOFilePath)
        List<PatientDTO> patientDTOs = DtoProvider.csvToDto(patientDTOFilePath, PatientDTO.class)

        Float pagesToFinishFloat = patientDTOs.size()/bulkPageSize
        Integer pagesToFinishInt = (patientDTOs.size()/bulkPageSize)
        if ((pagesToFinishFloat - pagesToFinishInt) > 0.0) { //add a page when there is a fraction left over
            pagesToFinishInt++
        }

        evaluationStatus.setVariable("populationSize",""+ patientDTOs.size())
        evaluationStatus.setVariable("pageSize",""+ bulkPageSize)
        evaluationStatus.setVariable("pagesToEvaluate",""+ pagesToFinishInt)

        Integer page = 0
        int totalPeopleMeCriteria = 0
        while(page < pagesToFinishInt) {

            String pageLabel = "P" + ++page + "/" + pagesToFinishInt + ": "

            evaluationStatus.setVariable("page",""+ page)

            evaluationStatus.state = pageLabel + "Create Requests"
            evaluationStatus.status = EvaluatorInstanceState.PROCESSING
            evaluationStatus.startTime = LocalDateTime.now()
            evaluationStatus.completionTime = null

            if (page == 1) {
                evaluationStatus.message = patientDTOs.size() + " people to evaluate; page size " + bulkPageSize + "; "

                if (pagesToFinishInt > 1) {
                    evaluationStatus.message += pagesToFinishInt + " pages to complete request; "
                } else {
                    evaluationStatus.message += pagesToFinishInt + " page to complete request; "
                }

            } else {
                evaluationStatus.message = ""
            }

            saveStateMessage(evaluationStatus)
            evaluationStatus.jsonVariables = null

            UResult<List<CdsRequest>> bulkCdsRequestResult = nccnCdsHooksRequestFactory.getCdsHooksRequests(serviceId, page, bulkPageSize, params, patientDTOs)

            if (bulkCdsRequestResult.error()) {
                throw new RuntimeException(bulkCdsRequestResult.getException())
            }

            List<CdsRequest> cdsRequestList = bulkCdsRequestResult.getResult()
            Integer cdsRequestListSize = cdsRequestList.size()

            evaluationStatus.status = EvaluatorInstanceState.COMPLETED
            evaluationStatus.completionTime = LocalDateTime.now()
            Duration dur1  = Duration.between(evaluationStatus.startTime , evaluationStatus.completionTime )

            if (cdsRequestListSize > 0) {
                evaluationStatus.message += String.format("%d requests created in %d sec", cdsRequestListSize, dur1.getSeconds())

                int noFHx = 0
                // if the number of people to evaluate is less than the page size
                if (patientDTOs.size() <= bulkPageSize) {
                    noFHx = patientDTOs.size() - cdsRequestListSize
                } else if (cdsRequestListSize < bulkPageSize) {
                    noFHx = bulkPageSize - cdsRequestListSize
                }

                if (noFHx > 0) {
                    evaluationStatus.message += String.format("; %d people did not have family history records", noFHx)
                }

                evaluationStatus.setVariable("peopleWithFhx", String.format("%d",cdsRequestListSize))
                evaluationStatus.setVariable("peopleWithoutFhx", String.format("%d",noFHx))

            } else {
                evaluationStatus.message = 'Done. No more records.'
                saveStateMessage(evaluationStatus)
                continue
            }

            saveStateMessage(evaluationStatus)
            evaluationStatus.jsonVariables = null

            if (cdsRequestListSize > 0) {
                StopWatch evalStopWatch = new StopWatch()
                evalStopWatch.start()

                evaluationStatus.state = pageLabel + "Evaluate Requests"
                evaluationStatus.status = EvaluatorInstanceState.PROCESSING
                evaluationStatus.startTime = LocalDateTime.now()
                evaluationStatus.completionTime = null
                evaluationStatus.message = cdsRequestListSize.toString()

                evaluationStatus.setVariable("page", ""+page)
                evaluationStatus.setVariable("cdsRequestsToProcess", ""+cdsRequestListSize)

                saveStateMessage(evaluationStatus)
                evaluationStatus.jsonVariables=null

                log.info("BULK EVALUATE PAGE " + page)
                UResult<CdsResponse[]> cdsResponseArrayResult = cdsHooksEvaluator.bulkEvaluate(cdsRequestList.toArray(new CdsRequest[0]), serviceId)

                if (cdsResponseArrayResult.error() == true) {
                    log.severe("CDS HOOK RESPONSE ERROR: " + cdsResponseArrayResult.message)
                    return UResult.errorResult(cdsResponseArrayResult.message)
                }

                List<CdsResponse> cdsResponses = Arrays.asList(cdsResponseArrayResult.getResult())
                evalStopWatch.stop()
                String message = "BULK EVALUATE PAGE " + page + " RETURNED " + cdsResponses.size() + " RESPONSES IN " + evalStopWatch.getTotalTimeSeconds() + " (SEC)"
                log.info(message)

                evaluationStatus.status = EvaluatorInstanceState.COMPLETED
                evaluationStatus.completionTime = LocalDateTime.now()
                evaluationStatus.message += String.format(" evaluated in %.2f sec", evalStopWatch.getTotalTimeSeconds())

                saveStateMessage(evaluationStatus)

                evaluationStatus.state = pageLabel + "Interpret Responses"
                evaluationStatus.status = EvaluatorInstanceState.PROCESSING
                evaluationStatus.startTime = LocalDateTime.now()
                evaluationStatus.completionTime = null
                evaluationStatus.message = cdsResponses.size().toString()

                evaluationStatus.setVariable("page", ""+page)
                evaluationStatus.setVariable("cdsResponses",""+cdsResponses.size())
                saveStateMessage(evaluationStatus)
                evaluationStatus.jsonVariables = null

                StopWatch interpretStopWatch = new StopWatch()
                interpretStopWatch.start()

                if ("MET_CRITERIA_OUTPUT_TYPE" == outputType) {

                    UResult<List<MetCriteriaDTO>> metCriteriaResult = cdsHooksResponseInterpreter.interpret(serviceId, cdsResponses)

                    interpretStopWatch.stop()
                    String intMessage = "CDS REQUESTS INTERPRETED IN " + interpretStopWatch.getTotalTimeSeconds() + " (SEC)"
                    log.info(intMessage)

                    evaluationStatus.status = EvaluatorInstanceState.COMPLETED
                    evaluationStatus.completionTime = LocalDateTime.now()
                    evaluationStatus.message += String.format(" interpreted in %.2f sec", interpretStopWatch.getTotalTimeSeconds())

                    saveStateMessage(evaluationStatus)

                    evaluationStatus.state = pageLabel + "Export Results"
                    evaluationStatus.status = EvaluatorInstanceState.PROCESSING
                    evaluationStatus.startTime = LocalDateTime.now()
                    evaluationStatus.completionTime = null
                    int peopleMetCriteria = peopleMetCriteria(metCriteriaResult.getResult())
                    evaluationStatus.message = peopleMetCriteria + " people met criteria from page ${page};"
                    evaluationStatus.setVariable("page", "" + page)
                    evaluationStatus.setVariable("peopleMetCriteria", "" + peopleMetCriteria)

                    totalPeopleMeCriteria += peopleMetCriteria
                    evaluationStatus.setVariable("cumulativePeopleMetCriteria", "" + totalPeopleMeCriteria)
                    evaluationStatus.message += " " + totalPeopleMeCriteria + " cumulative people have met criteria;"

                    saveStateMessage(evaluationStatus)
                    evaluationStatus.jsonVariables = null

                    String expMessage = "WRITING PAGE " + page + " RESULTS TO " + resultFileName
                    log.info(expMessage)

                    exportMetCriteria(filePath, metCriteriaResult.getResult(), page != 1)

                    evaluationStatus.status = EvaluatorInstanceState.COMPLETED
                    evaluationStatus.completionTime = LocalDateTime.now()
                    Duration between  = Duration.between(evaluationStatus.startTime , evaluationStatus.completionTime )
                    evaluationStatus.message += String.format(" exported in %d sec", between.getSeconds())
                    saveStateMessage(evaluationStatus)
                }

                if ("REGISTRY_INSTRUCTION_OUTPUT_TYPE" == outputType) {

                    UResult<List<RegistryInstructionDTO>> registryUpdatesResult = cdsHooksResponseRegistryInstructionInterpreter.interpret(cdsResponses)

                    interpretStopWatch.stop()
                    String intMessage = "CDS REQUESTS INTERPRETED IN " + interpretStopWatch.getTotalTimeSeconds() + " (SEC)"
                    log.info(intMessage)

                    evaluationStatus.status = EvaluatorInstanceState.COMPLETED
                    evaluationStatus.completionTime = LocalDateTime.now()
                    evaluationStatus.message += String.format(" interpreted in %.2f sec", interpretStopWatch.getTotalTimeSeconds())

                    saveStateMessage(evaluationStatus)

                    if (registryUpdatesResult.success()) {

                        List<RegistryInstructionDTO> registryInstructionDTOList = registryUpdatesResult.getResult()

                        evaluationStatus.state = pageLabel + "Export Results"
                        evaluationStatus.status = EvaluatorInstanceState.PROCESSING
                        evaluationStatus.startTime = LocalDateTime.now()
                        evaluationStatus.completionTime = null
                        evaluationStatus.message = "GARDE discovered " + numberOfPeopleToEdit(registryInstructionDTOList, "CREATE") + " new people and " + numberOfPeopleToEdit(registryInstructionDTOList, "UPDATE") + " people who needed updates."

                        saveStateMessage(evaluationStatus)

                        String expMessage = "WRITING PAGE " + page + " RESULTS TO " + resultFileName
                        log.info(expMessage)

                        exportRegistryInstructions(filePath, registryInstructionDTOList, page != 1)

                        evaluationStatus.status = EvaluatorInstanceState.COMPLETED
                        evaluationStatus.completionTime = LocalDateTime.now()
                        Duration between = Duration.between(evaluationStatus.startTime, evaluationStatus.completionTime)
                        evaluationStatus.message += String.format(" exported in %d sec", between.getSeconds())
                        saveStateMessage(evaluationStatus)
                    }

                }

            }
        }

        wholeEvalStopWatch.stop()
        log.info("TOTAL EVAL TIME: " + wholeEvalStopWatch.getTotalTimeSeconds() + " (SEC) " + " = " + wholeEvalStopWatch.getTotalTimeSeconds()/60 + " (MIN)")

        return UResult.successResult(resultFileName)
    }

    List<RegistryInstructionDTO> getRegistryInstructionsByInstanceId(String instanceId) {

        Optional<EvaluatorInstanceState> oCompletionState = serviceInstanceStateRepository.findOneByInstanceIdAndStateAndStatus(instanceId, "Evaluation completed", EvaluatorInstanceState.COMPLETED)
        if (oCompletionState.isEmpty()) {
            log.warning("NO VALID COMPLETION STATE FOUND FOR INSTANCE " + instanceId)
            return new ArrayList<RegistryInstructionDTO>()
        }

        if (!oCompletionState.get().jsonVariables) {
            log.warning("RESULTS FILE NOT FOUND FOR SERVICE INSTANCE " + instanceId)
            return new ArrayList<RegistryInstructionDTO>()
        }

        String resultsFile = oCompletionState.get().getVariable("resultsFile")

        return RegistryInstructionDTOProviderCsv.readFromCsv(getMetCriteriaFilePath() + "/" + resultsFile, new CsvService())
    }

    List<PatientMetRuleCriteriaDTO> getPatientMetCriteriaByInstanceId(String instanceId) {

        Map<String, Set<RuleDTO>> patientMetCriteriaMap = new HashMap<>()

        // get the file name from the instance
        Optional<EvaluatorInstanceState> oCompletionState = serviceInstanceStateRepository.findOneByInstanceIdAndStateAndStatus(instanceId, "Evaluation completed", EvaluatorInstanceState.COMPLETED)

        if (oCompletionState.isEmpty()) {
            log.warning("NO VALID COMPLETION STATE FOUND FOR SERVICE INSTANCE " + instanceId)
            return new ArrayList<PatientMetRuleCriteriaDTO>()
        }

        if (!oCompletionState.get().jsonVariables) {
            log.warning("MET CRITERIA RESULTS FILE NOT FOUND FOR SERVICE INSTANCE " + instanceId)
            return new ArrayList<PatientMetRuleCriteriaDTO>()
        }

        String resultsFile = oCompletionState.get().getVariable("resultsFile")

        MetCriteriaDTOProviderCsv metCriteriaDTOProvider = new MetCriteriaDTOProviderCsv(new CsvService())
        List<MetCriteriaDTO> metCriteriaDTOList = metCriteriaDTOProvider.readFromCsv(getMetCriteriaFilePath() + "/" + resultsFile)

        for (MetCriteriaDTO mc: metCriteriaDTOList) {

            Set<RuleDTO> personsMetCriteriaSet = patientMetCriteriaMap.get(mc.ehrPatientId)

            if (!personsMetCriteriaSet) {
                personsMetCriteriaSet = new HashSet<>()
            }
            RuleDTO ruleDTO = mc.toRuleDTO()
            personsMetCriteriaSet.add(ruleDTO)
            patientMetCriteriaMap.put(mc.ehrPatientId, personsMetCriteriaSet)
        }

        String serviceId = oCompletionState.get().serviceId
        GEvaluator evaluator = GEvaluator.findByServiceId(serviceId)

        List<PatientMetRuleCriteriaDTO> patientMetCriteriaDTOList = new ArrayList<>()
        for (String patientId:patientMetCriteriaMap.keySet()) {
            patientMetCriteriaDTOList.add(new PatientMetRuleCriteriaDTO(
                    patientId: patientId
                    , metRuleCriteria: patientMetCriteriaMap.get(patientId)
                    , cohort: evaluator.generatedCohorts[0]))
        }

        return patientMetCriteriaDTOList
    }


    private getMetCriteriaFilePath() {

        if (environment.getProperty("garde.dataProvider.files.root.directory") && environment.getProperty("garde.dataProvider.files.output.directory")) {
            return environment.getProperty("garde.dataProvider.files.root.directory") + environment.getProperty("garde.dataProvider.files.output.directory")
        }

        log.severe("PROPERTIES NOT SET -> garde.dataProvider.files.root.directory and/or garde.dataProvider.files.output.directory")

    }

    static UResult<Boolean> exportMetCriteria(String fileName, List<MetCriteriaDTO> metCriteriaDTOList, Boolean appendFile) {

        try {
            MetCriteriaDTOProviderCsv metCriteriaDTOProvider = new MetCriteriaDTOProviderCsv(new CsvService())
            metCriteriaDTOProvider.write(fileName, metCriteriaDTOList, appendFile)
        } catch (Exception e) {
            return UResult.errorResult(e)
        }

        return UResult.successResult(true)
    }

    static UResult<Boolean> exportRegistryInstructions(String fileName, List<RegistryInstructionDTO> registryInstructionDTOs, Boolean appendFile) {

        try {
            RegistryInstructionDTOProviderCsv.writeToFile(fileName, registryInstructionDTOs, ",", appendFile)
        } catch (Exception e) {
            return UResult.errorResult(e)
        }

        return UResult.successResult(true)
    }

    static Integer numberOfPeopleToEdit(List<RegistryInstructionDTO> instructions, String editType) {
        Set<String> patientIdSet = new HashSet<>()
        for (RegistryInstructionDTO regDTO: instructions) {
            if ("CREATE".equals(editType) && "CREATE".equals(regDTO.instruction)
            ) {
                patientIdSet.add(regDTO.ehrPatientId)
            }
            if ("UPDATE".equals(editType) && "UPDATE".equals(regDTO.instruction)
            ) {
                patientIdSet.add(regDTO.ehrPatientId)
            }
        }
        return patientIdSet.size()
    }

    static Integer peopleMetCriteria(List<MetCriteriaDTO> metCriteriaList) {
        Set<String> patientIdSet = new HashSet<>()
        for (MetCriteriaDTO metCriteriaDTO: metCriteriaList) {
            patientIdSet.add(metCriteriaDTO.ehrPatientId)
        }
        return patientIdSet.size()
    }

    @Transactional
    saveStateMessage(EvaluatorInstanceState state) {
        state.stateId = null

        Optional<EvaluatorInstanceState> optionalServiceInstanceState = serviceInstanceStateRepository.findOneByInstanceIdAndStateAndCompletionTimeIsNull(state.instanceId, state.state)
        EvaluatorInstanceState uServiceInstanceState = null

        if (optionalServiceInstanceState.isPresent()) {
            EvaluatorInstanceState serviceInstanceState = optionalServiceInstanceState.get()
            serviceInstanceState.completionTime = state.completionTime
            serviceInstanceState.status = state.status
            serviceInstanceState.message = state.message
            serviceInstanceState.mergeVariables(state.jsonVariables)
            uServiceInstanceState = serviceInstanceStateRepository.save(serviceInstanceState)
        } else {
            EvaluatorInstanceState pState = new EvaluatorInstanceState(state)
            uServiceInstanceState = serviceInstanceStateRepository.save(pState)
        }

        GardeChannel.push(uServiceInstanceState)
    }

//    { id: 0, label: 'Today'},
//    { id: 1, label: 'Yesterday'},
//    { id: 2, label: '2 Days ago'},
//    { id: 3, label: '3 Days ago'},
//    { id: 7, label: '1 Week ago'},
//    { id: 14, label: '2 Weeks ago'},
//    { id: 30, label: '1 Month ago'},
//    { id: -1, label: 'All'},
    //
    // when we get a 1, the query should be startTime >= yesterday at 00:00:00

    static LocalDateTime getLookBackDate(Integer lookBackId) {

        LocalDate today = LocalDate.now()
        switch (lookBackId) {
            case 0:
                return LocalDateTime.of(today, LocalTime.of(00, 00, 01))
            case 1:
                return LocalDateTime.of(today.minusDays(1), LocalTime.of(00, 00, 01))
            case 2:
                return LocalDateTime.of(today.minusDays(2), LocalTime.of(00, 00, 01))
            case 3:
                return LocalDateTime.of(today.minusDays(3), LocalTime.of(00, 00, 01))
            case 7:
                return LocalDateTime.of(today.minusWeeks(1), LocalTime.of(00, 00, 01))
            case 14:
                return LocalDateTime.of(today.minusWeeks(2), LocalTime.of(00, 00, 01))
            case 30:
                return LocalDateTime.of(today.minusMonths(1), LocalTime.of(00, 00, 01))
            case -1: // all -> capability didn't exist before 2023
                return LocalDateTime.of(LocalDate.of(2023,1,1), LocalTime.of(00, 00, 01))
            default:
                throw new IllegalArgumentException("Invalid lookBackId: " + lookBackId)
        }

    }

}

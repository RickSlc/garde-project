-- Runs after jpa sets up tables - changes tables inadvertently created by JPA to their intended views
drop table g_code_association_v;

create or replace view g_code_association_v as
select a1.code_association_gid
     , a1.ls_code_gid
     , ls.code_system     ls_code_system
     , ls.code            ls_code
     , ls.code_label      ls_code_label
     , ls.is_binding_code ls_is_binding_code
     , a1.association_cd  association_cd
     , a1.rs_code_gid
     , rs.code_system     rs_code_system
     , rs.code            rs_code
     , rs.code_label      rs_code_label
     , rs.is_binding_code rs_is_binding_code
from g_code_association a1
         join g_code ls on ls.code_gid = a1.ls_code_gid
         join g_code rs on rs.code_gid = a1.rs_code_gid
;

drop table g_value_set_v;

create or replace view g_value_set_v as
with vs_bindings as (select assoc.*
                     from g_code_association_v assoc
                     where assoc.association_cd = 'HAS_MEMBER')
select concat(b1.ls_code_gid, '|', assoc.association_cd, '|', assoc.rs_code_gid) vs_code_association_gid
     , b1.ls_code_gid                                                            vs_code_gid
     , b1.ls_code_system                                                         vs_code_system
     , b1.ls_code                                                                vs_code
     , b1.ls_code_label                                                          vs_code_label
     , b1.ls_is_binding_code                                                     vs_is_binding_code
     , b1.rs_code_gid                                                            m_code_gid
     , b1.rs_code_system                                                         m_code_system
     , b1.rs_code                                                                m_code
     , b1.rs_code_label                                                          m_code_label
     , b1.rs_is_binding_code                                                     m_is_binding_code
     , assoc.association_cd                                                      association
     , assoc.rs_code_gid                                                         code_gid
     , assoc.rs_code_system                                                      code_system
     , assoc.rs_code                                                             code
     , assoc.rs_code_label                                                       code_label
     , assoc.rs_is_binding_code                                                  is_binding_code
from vs_bindings b1
         join g_code_association_v assoc on b1.rs_code_system = assoc.ls_code_system
    and b1.rs_code = assoc.ls_code
order by b1.ls_code, b1.rs_code
;

drop table g_rule_code_v;

create or replace view g_rule_code_v as
with garde_code_set as (select ls_code code_set_binding, count(1) cnt
                        from g_code_association_v
                        where association_cd = 'HAS_MEMBER'
                          and ls_is_binding_code = 1
                        group by ls_code),
     rule_code_set as (select rc.rule_code_gid
                            , rl.rule_gid
                            , rl.rule_cd
                            , rl.rule_label
                            , rc.auto_expand
                            , cd.code_gid
                            , cd.code
                       from g_rule_code rc
                                join g_code cd on cd.code_gid = rc.code_gid
                                join g_rule rl on rl.rule_gid = rc.rule_gid
                       where cd.is_binding_code = 1
                         and cd.code in (select nc.code_set_binding from garde_code_set nc)),
     rule_codes as (select rc.rule_code_gid
                         , rl.rule_gid
                         , rl.rule_cd
                         , rl.rule_label
                         , null code_set
                         , rc.auto_expand
                         , cd.code_gid
                         , cd.code_system
                         , cd.code
                         , cd.code_label
                         , cd.is_binding_code
                    from g_rule_code rc
                             join g_code cd on cd.code_gid = rc.code_gid
                             join g_rule rl on rl.rule_gid = rc.rule_gid
                    where cd.code not in (select nc.code_set_binding from garde_code_set nc)
                       or rc.auto_expand = 0),
     rule_code_union as (select *
                         from rule_codes
                         union
                         select rc.rule_code_gid
                              , rc.rule_gid
                              , rc.rule_cd
                              , rc.rule_label
                              , rc.code                code_set
                              , rc.auto_expand
                              , mem.rs_code_gid        code_gid
                              , mem.rs_code_system     code_system
                              , mem.rs_code            code
                              , mem.rs_code_label      code_label
                              , mem.rs_is_binding_code is_binding_code
                         from rule_code_set rc
                                  join g_code_association_v mem on mem.ls_code = rc.code and rc.auto_expand = 1)

select concat(un.rule_gid, ':', un.code_gid) rule_code_view_gcd
     , ev.evaluator_gid
     , ev.service_id
     , un.*
from rule_code_union un
         join g_evaluator_rule er on er.rule_gid = un.rule_gid
         join g_evaluator ev on ev.evaluator_gid = er.evaluator_gid
order by rule_cd, code_set, code
;

drop table g_rule_code_expand_v;

create or replace view g_rule_code_expand_v as
with binding_code_set as (select ls_code code_set_binding, count(1) cnt
                          from g_code_association_v
                          where association_cd = 'HAS_MEMBER'
                            and ls_is_binding_code = 1
                          group by ls_code),
     rule_code_set as (select rc.rule_code_gid
                            , rl.rule_gid
                            , rl.rule_cd
                            , rl.rule_label
                            , rc.auto_expand
                            , cd.code_gid
                            , cd.code
                       from g_rule_code rc
                                join g_code cd on cd.code_gid = rc.code_gid
                                join g_rule rl on rl.rule_gid = rc.rule_gid
                       where cd.is_binding_code = 1
                         and cd.code in (select nc.code_set_binding from binding_code_set nc)),
     nccn_rule_code as (select rc.rule_code_gid
                             , rl.rule_gid
                             , rl.rule_cd
                             , rl.rule_label
                             , null code_set
                             , rc.auto_expand
                             , cd.code_gid
                             , cd.code_system
                             , cd.code
                             , cd.code_label
                             , cd.is_binding_code
                        from g_rule_code rc
                                 join g_code cd on cd.code_gid = rc.code_gid
                                 join g_rule rl on rl.rule_gid = rc.rule_gid
                        where cd.code not in (select nc.code_set_binding from binding_code_set nc)
                           or rc.auto_expand = 0),
     rule_code_union as (select *
                         from nccn_rule_code
                         union
                         select rc.rule_code_gid
                              , rc.rule_gid
                              , rc.rule_cd
                              , rc.rule_label
                              , rc.code                code_set
                              , rc.auto_expand
                              , mem.rs_code_gid        code_gid
                              , mem.rs_code_system     code_system
                              , mem.rs_code            code
                              , mem.rs_code_label      code_label
                              , mem.rs_is_binding_code is_binding_code
                         from rule_code_set rc
                                  join g_code_association_v mem on mem.ls_code = rc.code -- and rc.auto_expand = 1

     )

select concat(un.rule_gid, ':', un.code_gid)                   rule_code_view_gcd
     , ev.evaluator_gid
     , ev.service_id
     , case when vs1.vs_code_gid is not null then 1 else 0 end is_code_set
     , un.*
from rule_code_union un
         join g_evaluator_rule er on er.rule_gid = un.rule_gid
         join g_evaluator ev on ev.evaluator_gid = er.evaluator_gid
         left join (select distinct vs.vs_code_gid from g_value_set_v vs) vs1 on vs1.vs_code_gid = un.code_gid
order by rule_cd, code_set, code
;

create or replace view g_binding_code_error_v as
with binding_code_cnt as (

  select code, count(1) binding_code_count
  from g_code
  where is_binding_code = 1
  group by code

)
select *
from binding_code_cnt
where binding_code_count > 1
;
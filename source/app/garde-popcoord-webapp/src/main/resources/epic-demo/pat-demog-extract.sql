select 'http://epic.com/YOUR_INSTITUTION/clarity/patient.pat_id' ehr_pat_id_code_system
     , pt.pat_id                                                 ehr_pat_id
     , pt.birth_date
     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_sex'         ehr_sex_code_system
     , sx.rcpt_mem_sex_c                                         ehr_sex_code
     , sx.name                                                   ehr_sex_label
     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_religion'    ehr_religion_code_system
     , rl.religion_c                                             ehr_religion_code
     , rl.name                                                   ehr_religion_label
from CLARITY_RAW.patient pt
         join YOUR_TABLE pat_denom on pat_denom.pat_id = pt.pat_id -- table with distinct pat_id set to evaluate
         left outer join CLARITY.zc_sex sx on (sx.rcpt_mem_sex_c = pt.sex_c)
         left outer join CLARITY.zc_religion rl on (rl.religion_c = pt.religion_c)
;

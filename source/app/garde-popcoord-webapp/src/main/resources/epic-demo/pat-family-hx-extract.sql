WITH max_fhx_contact AS (

    SELECT
        fhx.pat_id,
        MAX(fhx.pat_enc_csn_id) max_enc_id
    FROM clarity.family_hx fhx
             JOIN n_test_pat tst ON tst.ehr_pat_id = fhx.pat_id
    GROUP BY
        fhx.pat_id

)

SELECT
    'http://epic.com/YOUR_INSTITUTION/clarity/family_hx/pat_id__pat_enc_csn_id__line'  famil_hx_id_code_system
     , fhx.pat_id || '_' || fhx.pat_enc_csn_id || '_' || fhx.line family_hx_id
     , 'http://epic.com/YOUR_INSTITUTION/clarity/patient.pat_id'  ehr_pat_id_code_system
     , fhx.pat_id           ehr_pat_id
     , 'http://epic.com/YOUR_INSTITUTION/clarity/pat_enc.pat_enc_csn_id'  ehr_enc_id_code_system
     , fhx.pat_enc_csn_id   ehr_enc_id
     , fhx.line             ehr_rec_id
     , fhx.contact_date     ehr_rec_dt
     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_medical_hx'  ehr_cond_code_system
     , fhx.medical_hx_c     ehr_cond_code
     , zmhx.title           ehr_cond_label
     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_relation'  ehr_relation_code_system
     , fhx.relation_c       ehr_relation_code
     , zrel.title           ehr_relation_label
     , fhx.age_of_onset     ehr_cond_onset_age
     , fhx.comments         ehr_comments
FROM
    CLARITY.family_hx       fhx
        JOIN max_fhx_contact             mx ON ( mx.pat_id = fhx.pat_id AND mx.max_enc_id = fhx.pat_enc_csn_id )
        JOIN YOUR_TABLE pat_denom on pat_denom.pat_id = fhx.pat_id -- table with patients to evaluate
        LEFT JOIN clarity.zc_medical_hx   zmhx ON ( zmhx.medical_hx_c = fhx.medical_hx_c )
        LEFT JOIN clarity.zc_relation     zrel ON ( zrel.relation_c = fhx.relation_c )
;

/* Run in personal edw schema */
create table n_test_pat as
with pats as (
    select
        'http://epic.com/uhealth/clarity/patient.pat_id' ehr_pat_id_code_system
         , pt.pat_id                              ehr_pat_id
         , pt.pat_last_name
         , pt.pat_first_name
         , pt.pat_middle_name
         , pt.birth_date
         , 'http://epic.com/uhealth/clarity/zc_sex' ehr_sex_code_system
         , sx.rcpt_mem_sex_c                        ehr_sex_code
         , sx.name                                  ehr_sex_label
         , 'http://epic.com/uhealth/clarity/zc_marital_status' ehr_marital_status_code_system
         , ms.marital_status_c                                 ehr_marital_status_code
         , ms.name                                             ehr_marital_status_label
         , 'http://epic.com/uhealth/clarity/zc_religion' ehr_religion_code_system
         , rl.religion_c                                 ehr_religion_code
         , rl.name                                       ehr_religion_label
         , 'http://epic.com/uhealth/clarity/zc_language' ehr_lang_code_system
         , lng.language_c                                ehr_lang_code
         , lng.name                                      ehr_lang_label
    from CLARITY_RAW.patient pt
             left outer join CLARITY_ORG.zc_sex sx on (sx.rcpt_mem_sex_c = pt.sex_c)
             left outer join CLARITY_ORG.zc_marital_status ms on (ms.marital_status_c = pt.marital_status_c)
             left outer join CLARITY_ORG.zc_religion rl on (rl.religion_c = pt.religion_c)
             left outer join CLARITY_ORG.zc_language lng on (lng.language_c = pt.language_c)

), max_screened_pats as (

    select ehr_pat_id, max(vrsn) max_vrsn
    from kmm_nci.n_pat_screened
    where cohort_cd = 'MET_NCCN_SCREENING_CRITERIA'
    group by ehr_pat_id

), cur_screened_managed as (

    select s1.ehr_pat_id
    from kmm_nci.n_pat_screened s1
      inner join max_screened_pats mx on mx.ehr_pat_id = s1.ehr_pat_id and mx.max_vrsn = s1.vrsn
    where s1.change_cd <> 'DELETE'
    union
    select distinct coh.ehr_pat_id from kmm_nci.n_pat_cohort coh where coh.cohort_cd = 'MET_NCCN_GENETIC_TESTING_CRITERIA' -- anyone who has ever been added to the registry

)

select pts.*
from pats pts
inner join cur_screened_managed sc on sc.ehr_pat_id = pts.ehr_pat_id -- will lose a few patients in this join
;

create unique index N_TEST_PAT_EHR_PAT_ID_UINDEX
    on N_TEST_PAT (EHR_PAT_ID)
;

-- to create the csv for import/eval
select
    EHR_PAT_ID_CODE_SYSTEM
     ,EHR_PAT_ID
     ,BIRTH_DATE
     ,EHR_SEX_CODE_SYSTEM
     ,EHR_SEX_CODE
     ,EHR_SEX_LABEL
     ,EHR_RELIGION_CODE_SYSTEM
     ,EHR_RELIGION_CODE
     ,EHR_RELIGION_LABEL
from N_TEST_PAT
;

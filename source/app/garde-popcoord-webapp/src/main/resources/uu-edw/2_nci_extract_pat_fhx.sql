/* ran in personal edw schema - fetch all fhx in n_test_pat */
create table n_test_fhx as
WITH max_fhx_contact AS (

    SELECT
        fhx.pat_id,
        MAX(fhx.pat_enc_csn_id) max_enc_id
    FROM clarity_raw.family_hx fhx
    JOIN n_test_pat tst ON tst.ehr_pat_id = fhx.pat_id
    GROUP BY
        fhx.pat_id

)

SELECT
    'http://epic.com/uhealth/clarity/family_hx/pat_id__pat_enc_csn_id__line'  famil_hx_id_code_system
     , fhx.pat_id || '_' || fhx.pat_enc_csn_id || '_' || fhx.line family_hx_id
     , 'http://epic.com/uhealth/clarity/patient.pat_id'  ehr_pat_id_code_system
     , fhx.pat_id           ehr_pat_id
     , 'http://epic.com/uhealth/clarity/pat_enc.pat_enc_csn_id'  ehr_enc_id_code_system
     , fhx.pat_enc_csn_id   ehr_enc_id
     , fhx.line             ehr_rec_id
     , fhx.contact_date     ehr_rec_dt
     , 'http://epic.com/uhealth/clarity/zc_medical_hx'  ehr_cond_code_system
     , fhx.medical_hx_c     ehr_cond_code
     , zmhx.title           ehr_cond_label
     , 'http://epic.com/uhealth/clarity/zc_relation'  ehr_relation_code_system
     , fhx.relation_c       ehr_relation_code
     , zrel.title           ehr_relation_label
     , fhx.age_of_onset     ehr_cond_onset_age
     , fhx.comments         ehr_comments
FROM
    clarity_raw.family_hx       fhx
        INNER JOIN max_fhx_contact             mx ON ( mx.pat_id = fhx.pat_id
        AND mx.max_enc_id = fhx.pat_enc_csn_id )
        LEFT OUTER JOIN clarity_org.zc_medical_hx   zmhx ON ( zmhx.medical_hx_c = fhx.medical_hx_c )
        LEFT OUTER JOIN clarity_org.zc_relation     zrel ON ( zrel.relation_c = fhx.relation_c )
;

alter table N_TEST_FHX
    add constraint N_TEST_FHX_PK
        primary key (FAMILY_HX_ID)
;

create index N_TEST_FHX_EHR_PAT_ID_INDEX
    on N_TEST_FHX (EHR_PAT_ID)
;

create index N_TEST_FHX_EHR_ENC_ID_INDEX
    on N_TEST_FHX (EHR_ENC_ID)
;


/* ran in personal edw schema */
/* Prerequisite scripts:
   1_nci_extract_pat_demog.sql
   2_nci_extract_pat_fhx.sql
   */

create table n_test_last_nm as
select rownum rank, last_name, cnt
from (select upper(PAT_LAST_NAME) last_name, count(1) cnt
      from clarity_org.patient pt
      group by upper(PAT_LAST_NAME)
      order by cnt desc)
;

-- select random name
select last_name
from (select last_name
      from n_test_last_nm
      where cnt between 100 and 1000
      order by DBMS_RANDOM.RANDOM)
where rownum < 2
;

create table n_test_first_nm as
select rownum rank, first_name, sex_c, cnt
from (select upper(PAT_FIRST_NAME) first_name, SEX_C, count(1) cnt
      from clarity_org.patient pt
      group by upper(PAT_FIRST_NAME), SEX_C
      order by cnt desc)
;

create or replace function get_random_last_nm return varchar2 is
    v_last_name varchar2(100);
begin
    select last_name into v_last_name
    from (select ll.last_name
          from n_test_last_nm ll
          where cnt > 1000
          order by dbms_random.value)
    where rownum < 2;

    return v_last_name;

end;


create or replace function get_random_first_nm (p_sex_c number) return varchar2 is
    v_first_name varchar2(100);
begin
    select first_name into v_first_name
    from (select ll.first_name
          from n_test_first_nm ll
          where ll.sex_c = p_sex_c and cnt > 1000
          order by dbms_random.value)
    where rownum < 2;

    return v_first_name;

end;

drop table n_test_pat_1000_pat_de_id_key;

/* key for ALL pats with fhx */
create table n_test_pat_de_id_key as
select '' || sys_guid() pat_de_id
     , pp.EHR_PAT_ID
     , round(dbms_random.value(30, 90),2) date_offset
from (select distinct pt.EHR_PAT_ID
      from N_TEST_PAT pt
               join n_test_fhx fhx
                    on fhx.EHR_PAT_ID = pt.EHR_PAT_ID and fhx.EHR_RELATION_CODE <> '15' -- relative 15 is "nobody in my family"
      order by DBMS_RANDOM.RANDOM) pp
;

create unique index n_test_pat_de_id_key_idx
    on n_test_pat_de_id_key (EHR_PAT_ID)
;


/* key for 1000 rand pats with fhx */
create table n_test_pat_1000_pat_de_id_key as
select '' || sys_guid() pat_de_id
     , pp.EHR_PAT_ID
     , round(dbms_random.value(30, 90),2) date_offset
from (select distinct pt.EHR_PAT_ID
      from N_TEST_PAT pt
               join n_test_fhx fhx
                    on fhx.EHR_PAT_ID = pt.EHR_PAT_ID and fhx.EHR_RELATION_CODE <> '15' -- relative 15 is "nobody in my family"
      order by DBMS_RANDOM.RANDOM) pp
where rownum <= 1000
;

create unique index n_test_pat_1000_pat_de_id_key_idx
    on n_test_pat_1000_pat_de_id_key (EHR_PAT_ID)
;

drop table n_test_pat_1000_enc_de_id_key;

create table n_test_pat_1000_enc_de_id_key as
    select ehr_pat_id
         , '' || sys_guid() enc_de_id
         , ehr_enc_id
         , round(dbms_random.value(30, 90),2) date_offset
    from (select distinct fhx.ehr_pat_id, ehr_enc_id
          from n_test_fhx fhx
          join n_test_pat_1000_pat_de_id_key pt on pt.EHR_PAT_ID = fhx.EHR_PAT_ID
          )
;

create unique index n_test_pat_1000_enc_de_id_key_idx
    on n_test_pat_1000_enc_de_id_key (ehr_enc_id)
;

drop table n_test_pat_1000_fhx_de_id_key;

create table n_test_pat_1000_fhx_de_id_key as
select
fhx.FAMILY_HX_ID
, ''||sys_guid() family_hx_de_id
, fhx.EHR_PAT_ID
, rnd.pat_de_id
, fhx.EHR_ENC_ID
, enc.enc_de_id
, enc.date_offset
from n_test_fhx fhx
join n_test_pat_1000_pat_de_id_key rnd on rnd.EHR_PAT_ID = fhx.EHR_PAT_ID and fhx.EHR_RELATION_CODE <> '15'
join n_test_pat_1000_enc_de_id_key enc on enc.EHR_PAT_ID = fhx.EHR_PAT_ID and enc.EHR_ENC_ID = fhx.EHR_ENC_ID
;

create index n_test_pat_1000_fhx_de_id_key_idx1
    on n_test_pat_1000_fhx_de_id_key (EHR_PAT_ID)
;

create index n_test_pat_1000_fhx_de_id_key_idx2
    on n_test_pat_1000_fhx_de_id_key (EHR_ENC_ID)
;

create index n_test_pat_1000_fhx_de_id_key_idx3
    on n_test_pat_1000_fhx_de_id_key (FAMILY_HX_ID)
;

drop table n_test_pat_1000;

-- query to extract the 1000 pat records
create table n_test_pat_1000 as
select EHR_PAT_ID_CODE_SYSTEM
     , pat_de_id as                                  EHR_PAT_ID
     , get_random_last_nm() PAT_LAST_NAME
     , get_random_first_nm(EHR_SEX_CODE) PAT_FIRST_NAME
     , dbms_random.string('U', 1) PAT_MIDDLE_NAME
     , BIRTH_DATE + rnd.date_offset BIRTH_DATE
     , EHR_SEX_CODE_SYSTEM
     , EHR_SEX_CODE
     , EHR_SEX_LABEL
     , EHR_MARITAL_STATUS_CODE_SYSTEM
     , EHR_MARITAL_STATUS_CODE
     , EHR_MARITAL_STATUS_LABEL
     , EHR_RELIGION_CODE_SYSTEM
     , EHR_RELIGION_CODE
     , EHR_RELIGION_LABEL
     , EHR_LANG_CODE_SYSTEM
     , EHR_LANG_CODE
     , EHR_LANG_LABEL
from n_test_pat pt
         join n_test_pat_1000_pat_de_id_key rnd on rnd.EHR_PAT_ID = pt.EHR_PAT_ID
;

create unique index N_TEST_PAT_1000_PAT_DE_ID_uidx
    on N_TEST_PAT_1000 (EHR_PAT_ID)
;

select * from V$VERSION;

-- query to extract the 1000 pats FHx records (all recs except for "no fhx" for 1000 pats)
create table n_test_pat_1000_fhx as
select FAMIL_HX_ID_CODE_SYSTEM
     , rnd.FAMILY_HX_DE_ID
     , EHR_PAT_ID_CODE_SYSTEM
     , rnd.pat_de_id EHR_PAT_ID
     , EHR_ENC_ID_CODE_SYSTEM
     , rnd.ENC_DE_ID ehr_enc_id
     , EHR_REC_ID
     , (EHR_REC_DT + rnd.date_offset) EHR_REC_DT
     , EHR_COND_CODE_SYSTEM
     , EHR_COND_CODE
     , EHR_COND_LABEL
     , EHR_RELATION_CODE_SYSTEM
     , EHR_RELATION_CODE
     , EHR_RELATION_LABEL
     , trunc(EHR_COND_ONSET_AGE) EHR_COND_ONSET_AGE
     , EHR_COMMENTS
from n_test_fhx fhx
         join n_test_pat_1000_fhx_de_id_key rnd
              on rnd.FAMILY_HX_ID = fhx.FAMILY_HX_ID
                     and fhx.EHR_RELATION_CODE <> '15' -- where the "no family hx" flag is not set
order by fhx.ehr_pat_id, ehr_rec_id
;

select count(distinct fhx.EHR_PAT_ID) pat_cnt, count(1) fhx_cnt
from n_test_pat_1000_fhx fhx
join n_test_pat_1000_pat_de_id_key pt on pt.PAT_DE_ID = fhx.EHR_PAT_ID
;

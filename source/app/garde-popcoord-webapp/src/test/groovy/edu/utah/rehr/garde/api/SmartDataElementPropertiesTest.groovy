package edu.utah.rehr.garde.api

import edu.utah.rehr.garde.domain.ehr.model.SmartDataElementProperties
import org.junit.jupiter.api.Test

import static org.junit.jupiter.api.Assertions.assertFalse
import static org.junit.jupiter.api.Assertions.assertTrue

//@SpringBootTest
//@SpringBootApplication(exclude = [DataSourceAutoConfiguration.class])
//@EnableConfigurationProperties(SmartDataElementProperties.class)
class SmartDataElementPropertiesTest {

//    @Autowired
    SmartDataElementProperties sdeProps

    @Test
    void testTestConfig() {
        assertTrue(1==1)
    }

    @Test
    void sdeReadingIn() {
        assertFalse(sdeProps.id.isEmpty())
    }

    @Test
    void isGettingCorrectSdeIdValue() {
        assertTrue(sdeProps.getIdValue("NCCN_GENETIC_COUNSELING_CRITERIA_MET_DATE") == "REHR#1590010011")
    }

}

package edu.utah.rehr.garde.api

import edu.utah.rehr.garde.domain.ehr.model.SmartDataElementMetaList
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.test.context.SpringBootTest

import static org.junit.jupiter.api.Assertions.assertFalse
import static org.junit.jupiter.api.Assertions.assertTrue

@SpringBootTest
@SpringBootApplication(exclude = [DataSourceAutoConfiguration.class])
@EnableConfigurationProperties(SmartDataElementMetaList.class)
class SmartDataElementMetaListTest {

    @Autowired
    SmartDataElementMetaList sdes

    @Test
    void testTestConfig() {
        assertTrue(1==1)
    }

    @Test
    void sdeMetaReadingIn() {
        assertFalse(sdes.getSdes().isEmpty())
    }

    @Test
    void testGetSdeValues() {
        Optional<SmartDataElementMetaList.SmartDataElementMeta> oSdeMata = sdes.findSmartDataElementMetaByKey("KEY1")
        assertTrue(oSdeMata.isPresent())
        assertTrue(oSdeMata.get().id == "SDE#1")
        assertTrue(oSdeMata.get().type == "DATE")
    }

    @Test
    void testGetNextSdeValues() {
        Optional<SmartDataElementMetaList.SmartDataElementMeta> oSdeMata = sdes.findSmartDataElementMetaByKey("KEY2")
        assertTrue(oSdeMata.isPresent())
        assertTrue(oSdeMata.get().id == "SDE#2")
        assertTrue(oSdeMata.get().type == "STRING")
    }

    @Test
    void testGetSdeValuesMissing() {
        Optional<SmartDataElementMetaList.SmartDataElementMeta> oSdeMata = sdes.findSmartDataElementMetaByKey("KK")
        assertFalse(oSdeMata.isPresent())
    }

}

#!/bin/bash

date

# stop running container
docker container stop garde-popcoord-webapp
docker container stop garde-popcoord-webapp-arm64
# docker container stop garde-popcoord-webapp-win-amd64

# remove the container anticipating rebuild
docker container rm garde-popcoord-webapp
docker container rm garde-popcoord-webapp-arm64
# docker container rm garde-popcoord-webapp-win-amd64

# Remove image of the container
docker image rm rickslc/garde-popcoord-webapp
docker image rm rickslc/garde-popcoord-webapp-arm64
# docker image rm rickslc/garde-popcoord-webapp-win-amd64

# This one is for intel linux/amd64
docker buildx build --platform linux/amd64 -t rickslc/garde-popcoord-webapp .

# This one is for arm64 - macOS arm chips or similar
docker buildx build --platform linux/arm64 -t rickslc/garde-popcoord-webapp-arm64 .

# This one is for windows/amd64
# docker buildx build --platform windows/amd64 -t rickslc/garde-popcoord-webapp-win-amd64 .

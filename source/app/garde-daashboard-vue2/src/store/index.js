import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        loggedIn: false,
        // popCoordEndpoint: 'https://192.168.86.249:8090/',
        // websocketEndpoint: 'wss://192.168.86.249:8090/channel/evaluators',
        // popCoordEndpoint: 'http://localhost:8090',
        // websocketEndpoint: 'ws://localhost:8090/channel/evaluators',
        popCoordEndpoint: '',
        websocketEndpoint: '',
        cdsHooksEndpoint: '',
        appProps: [],
        codes: [],
        valueSets: [],
        bindingCodeMappings: [],
        searchFilterTypes: ['STARTS_WITH', 'EXACT', 'CONTAINS'],
        bindingCodeMappingsFilter: null,
        bindingCodeMappingsFilterType: 'STARTS_WITH',
        valueSetFilter: null,
        valueSetFilterType: 'STARTS_WITH',
        nextTempStateId: 0,
        associations: [
            {text: '(None)', value: ''},
            {text: 'HAS_MEMBER', value: 'HAS_MEMBER'},
            {text: 'EHR_CODE_IS', value: 'EHR_CODE_IS'},
            {text: 'STANDARD_CODE_IS', value: 'STANDARD_CODE_IS'},
        ],
        cdsHooksService: {id: null, title: null, instances: []},
        serviceInstanceStates: [],
        systemStatus: [],
        codebindingCode: '/NCCN',
        codebindingCodeType: 'STARTS_WITH',
        bindingCodeFilter: null,
        associationFilter: null,
        codeSystemFilter: null,
        codeSetDefinitions: [],
        serviceInstanceMetRuleCriteria: [],
        nccnRules: [
            {
                id: 'header1',
                header: 'Genetic testing for familial breast, colon, pancreatic, or ovarian cancer is recommended if at least ONE of the following is true:'
            },
            {
                id: 'BC 0',
                title: 'BRCA1/2, CHEK2, ATM, PALB2, TP53, PTEN, CDH1, Cowden Syndrome, Li-Fraumeni Syndrome discovered in the coded family history.',
                codes: [
                    {
                        codeLabel: 'BRCA-1',
                        bindingCode: 'BRCA_1_GENE_MUTATION',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'BRCA-2',
                        bindingCode: 'BRCA_2_GENE_MUTATION',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'CHEK2',
                        bindingCode: 'CHEK2_GENE_MUTATION',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'ATM',
                        bindingCode: 'ATM_GENE_MUTATION',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'PALB',
                        bindingCode: 'PALB2_GENE_MUTATION',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'TP53',
                        bindingCode: 'TP53_GENE_MUTATION',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'PTEN',
                        bindingCode: 'PTEN_GENE_MUTATION',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'CDH1',
                        bindingCode: 'CDH1_GENE_MUTATION',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'Cowden syndrome',
                        bindingCode: 'COWDEN_SYNDROME',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'Li-Fraumeni syndrome',
                        bindingCode: 'LI_FRAUMENI_SYNDROME',
                        codeType: 'code',
                    },
                ]
            },
            {
                id: 'BC 1',
                title: 'First or second degree relative with beast cancer age <= 45.',
                codes: [
                    {
                        codeLabel: 'first degree relative',
                        bindingCode: 'FIRST_DEG_RELATIVE',
                        codeType: 'valueSet',
                    },
                    {
                        codeLabel: 'second degree relative',
                        bindingCode: 'SECOND_DEG_RELATIVE',
                        codeType: 'valueSet',
                    },
                    {
                        codeLabel: 'breast cancer',
                        bindingCode: 'CANCER_BREAST',
                        codeType: 'code',
                    },
                ]
            },
            {
                id: 'BC 2',
                title: 'First or second degree relative with ovarian cancer.',
                codes: [
                    {
                        codeLabel: 'first degree relative',
                        bindingCode: 'FIRST_DEG_RELATIVE',
                        codeType: 'valueSet',
                    },
                    {
                        codeLabel: 'second degree relative',
                        bindingCode: 'SECOND_DEG_RELATIVE',
                        codeType: 'valueSet',
                    },
                    {
                        codeLabel: 'ovarian cancer',
                        bindingCode: 'CANCER_OVARIAN',
                        codeType: 'code',
                    },
                ]
            },
            {
                id: 'BC 3',
                title: 'First or second degree relative with pancreatic cancer.',
                codes: [
                    {
                        codeLabel: 'first degree relative',
                        bindingCode: 'FIRST_DEG_RELATIVE',
                        codeType: 'valueSet',
                    },
                    {
                        codeLabel: 'second degree relative',
                        bindingCode: 'SECOND_DEG_RELATIVE',
                        codeType: 'valueSet',
                    },
                    {
                        codeLabel: 'pancreatic cancer',
                        bindingCode: 'CANCER_PANCREAS',
                        codeType: 'code',
                    },
                ]
            },
            {
                id: 'BC 4',
                title: 'Three or more first or second degree relatives with breast, prostate, or pancreas cancer on the same side of the family.',
                codes: [
                    {
                        codeLabel: 'first degree relative',
                        bindingCode: 'FIRST_DEG_RELATIVE',
                        codeType: 'valueSet',
                    },
                    {
                        codeLabel: 'second degree maternal relative',
                        bindingCode: 'MATERNAL_SECOND_DEG_RELATIVE',
                        codeType: 'valueSet',
                    },
                    {
                        codeLabel: 'second degree paternal relative',
                        bindingCode: 'PATERNAL_SECOND_DEG_RELATIVE',
                        codeType: 'valueSet',
                    },
                    {
                        codeLabel: 'breast cancer',
                        bindingCode: 'CANCER_BREAST',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'prostate cancer',
                        bindingCode: 'CANCER_PROSTATE',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'pancreatic cancer',
                        bindingCode: 'CANCER_PANCREAS',
                        codeType: 'code',
                    },
                ]
            },
            {
                id: 'BC 5',
                title: 'Breast cancer in a male relative - brother, father, uncle, son, grandfather.',
                codes: [
                    {
                        codeLabel: 'male relative',
                        bindingCode: 'MALE_RELATIVE',
                        codeType: 'valueSet',
                    },
                    {
                        codeLabel: 'breast cancer',
                        bindingCode: 'CANCER_BREAST',
                        codeType: 'code',
                    },
                ]
            },
            {
                id: 'BC 7',
                title: 'Jewish and any breast, ovarian, prostate, pancreas anywhere in the family at any age.',
                codes: [
                    {
                        codeLabel: 'Jewish',
                        bindingCode: 'JEWISH',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'breast cancer',
                        bindingCode: 'CANCER_BREAST',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'ovarian cancer',
                        bindingCode: 'CANCER_OVARIAN',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'pancreatic cancer',
                        bindingCode: 'CANCER_PANCREAS',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'prostate cancer',
                        bindingCode: 'CANCER_PROSTATE',
                        codeType: 'code',
                    },
                ]
            },
            {
                id: 'CO 1',
                title: 'MLH1, MSH2, PMS2, MSH6, EPCAM, Lynch syndrome, hereditary non-polyposis colorectal cancer (HNPCC), familial adenomatous polyposis (FAP), APC, MYH, MUTYH, serrated polyposis, or polyposis discovered in the coded family history.',
                codes: [
                    {
                        codeLabel: 'MLH1',
                        bindingCode: 'MLH1_GENE_MUTATION',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'MSH2',
                        bindingCode: 'MSH2_GENE_MUTATION',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'PMS2',
                        bindingCode: 'PMS2_GENE_MUTATION',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'MSH6',
                        bindingCode: 'MSH6_GENE_MUTATION',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'EPCAM',
                        bindingCode: 'EPCAM_GENE_MUTATION',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'Lynch Syndrome',
                        bindingCode: 'LYNCH_SYNDROME',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'HNPCC',
                        bindingCode: 'HNPCC_GENE_MUTATION',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'FAP',
                        bindingCode: 'FAMILIAL_ADENOMATOUS_POLYPOSIS',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'APC',
                        bindingCode: 'APC_GENE_MUTATION',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'MYH',
                        bindingCode: 'MYH_GENE_MUTATION',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'MUTYH',
                        bindingCode: 'MUTYH_GENE_MUTATION',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'serrated polyposis',
                        bindingCode: 'SERRATED_POLYPOSIS',
                        codeType: 'code',
                    },
                ]
            },
            {
                id: 'CO 2',
                title: 'First or second degree relative with colon cancer <= 50.',
                codes: [
                    {
                        codeLabel: 'first degree relative',
                        bindingCode: 'FIRST_DEG_RELATIVE',
                        codeType: 'valueSet',
                    },
                    {
                        codeLabel: 'second degree relative',
                        bindingCode: 'SECOND_DEG_RELATIVE',
                        codeType: 'valueSet',
                    },
                    {
                        codeLabel: 'colon cancer',
                        bindingCode: 'CANCER_COLON',
                        codeType: 'code',
                    },
                ]
            },
            {
                id: 'CO 3',
                title: 'First or second degree relative with endometrial cancer <= 50.',
                codes: [
                    {
                        codeLabel: 'first degree relative',
                        bindingCode: 'FIRST_DEG_RELATIVE',
                        codeType: 'valueSet',
                    },
                    {
                        codeLabel: 'second degree relative',
                        bindingCode: 'SECOND_DEG_RELATIVE',
                        codeType: 'valueSet',
                    },
                    {
                        codeLabel: 'uterine cancer',
                        bindingCode: 'CANCER_UTERINE',
                        codeType: 'code',
                    },
                ]
            },
            {
                id: 'CO 4',
                title: 'Three or more first or second degree relatives with Lynch syndrome, hereditary non-polyposis colorectal cancer (HNPCC)' +
                    ', colon cancer, endometrial cancer, uterine cancer, ovarian cancer, stomach cancer, gastric cancer, small bowel cancer' +
                    ', small intestine cancer, Kidney cancer, ureteral cancer, bladder cancer, urethra cancer, brain cancer, pancreas cancer ' +
                    'AND at least one is colon or endometrial cancer ' +
                    'AND all on the same side of the family.',
                codes: [
                    {
                        codeLabel: 'first degree relative',
                        bindingCode: 'FIRST_DEG_RELATIVE',
                        codeType: 'valueSet',
                    },
                    {
                        codeLabel: 'second degree maternal relative',
                        bindingCode: 'MATERNAL_SECOND_DEG_RELATIVE',
                        codeType: 'valueSet',
                    },
                    {
                        codeLabel: 'second degree paternal relative',
                        bindingCode: 'PATERNAL_SECOND_DEG_RELATIVE',
                        codeType: 'valueSet',
                    },
                    {
                        codeLabel: 'Lynch Syndrome',
                        bindingCode: 'LYNCH_SYNDROME',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'HNPCC',
                        bindingCode: 'HNPCC_GENE_MUTATION',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'colon cancer',
                        bindingCode: 'CANCER_COLON',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'uterine cancer',
                        bindingCode: 'CANCER_UTERINE',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'ovarian cancer',
                        bindingCode: 'CANCER_OVARIAN',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'stomach cancer',
                        bindingCode: 'CANCER_STOMACH',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'gastric cancer',
                        bindingCode: 'CANCER_GASTRIC',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'small bowel cancer',
                        bindingCode: 'CANCER_SMALL_BOWEL',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'small intestine cancer',
                        bindingCode: 'CANCER_SMALL_INTESTINE',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'kidney cancer',
                        bindingCode: 'CANCER_KIDNEY',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'ureteral cancer',
                        bindingCode: 'CANCER_URETERAL',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'bladder cancer',
                        bindingCode: 'CANCER_BLADDER',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'urethral cancer',
                        bindingCode: 'CANCER_URETHRAL',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'brain cancer',
                        bindingCode: 'CANCER_BRAIN',
                        codeType: 'code',
                    },
                    {
                        codeLabel: 'pancreas cancer',
                        bindingCode: 'CANCER_PANCREAS',
                        codeType: 'code',
                    },
                ]
            },
        ]
    },
    getters: {

        getServiceInstanceMetRuleCriteria(state) {
            return state.serviceInstanceMetRuleCriteria
        },

        getCodeSetDefinition: (state) => (codeSetBinding) => {
            const codeSetDef = state.codeSetDefinitions.find(csd => csd.codeSetBinding === codeSetBinding)
            if (codeSetDef) {
                return codeSetDef.definition
            }
            return '(No definition)'
        },

        getSystemsReady(state) {

            let popCoord = false
            let cdsHooks = false

            for (let i = 0, l = state.systemStatus.length; i < l; i++) {
                let system = state.systemStatus[i]
                if (system.systemCd === 'POPULATION_COORDINATOR'
                    && system.status === 'up'
                ) {
                    popCoord = true
                } else if (system.systemCd === 'CDSHOOKS_SERVER'
                    && system.status === 'up'
                ) {
                    cdsHooks = true
                }
            }
            return popCoord && cdsHooks
        },

        getAppProps(state) {
            return state.appProps
        },

        getValueSets(state) {
            return state.valueSets
        },

        getCodes(state) {
            return state.codes
        },

        getSystemState(state) {
            return state.systemStatus.filter((sys) => {
                return sys.systemCd === 'POPULATION_COORDINATOR' || sys.systemCd === 'CDSHOOKS_SERVER'
            })
        },

        getCdsSystemState(state) {
            const cdsServerStatus = state.systemStatus.filter((sys) => {
                return sys.systemCd === 'CDSHOOKS_SERVER'
            })
            console.log("CDS SYSTEM SERVER - ", cdsServerStatus[0].endpoint)

            return cdsServerStatus[0]
        },

        getAssociations(state) {
            return state.associations
        },

        getAssociationFilter(state) {
            return state.associationFilter
        },

        getCdsHooksService(state) {
            return state.cdsHooksService
        },

        getServiceInstanceStates(state) {
            return state.serviceInstanceStates
        },

        getMappedCodes(state) {
            return state.bindingCodeMappings
        },

        getMappedCodesFiltered(state) {

            return state.bindingCodeMappings.filter((bm) => {
                if (state.valueSetFilterType === 'CONTAINS') {
                    return !state.bindingCodeMappingsFilter || bm.bindingCode.code.toUpperCase().includes(state.bindingCodeMappingsFilter.toUpperCase())
                } else if (state.valueSetFilterType === 'STARTS_WITH') {
                    return !state.bindingCodeMappingsFilter || bm.bindingCode.code.toUpperCase().startsWith(state.bindingCodeMappingsFilter.toUpperCase())
                } // equals
                return !state.bindingCodeMappingsFilter || bm.bindingCode.code.toUpperCase() === state.bindingCodeMappingsFilter.toUpperCase()
            })
        },

        getValueSetsFiltered(state) {

            return state.valueSets.filter((vs) => {
                if (state.valueSetFilterType === 'CONTAINS') {
                    return !state.valueSetFilter || vs.code.toUpperCase().includes(state.valueSetFilter.toUpperCase())
                } else if (state.valueSetFilterType === 'STARTS_WITH') {
                    return !state.valueSetFilter || vs.code.toUpperCase().startsWith(state.valueSetFilter.toUpperCase())
                } // equals
                return !state.valueSetFilter || vs.code.toUpperCase() === state.valueSetFilter.toUpperCase()
            })
        },

        getCodesFiltered(state) {

            console
                .log("GET_CODES_FILTERED: <" + state.codebindingCode + "> <" + state.bindingCodeFilter + "> <" + state.associationFilter + "> <" + state.codeSystemFilter + ">")
            return state.codes.filter((code) => {

                if (state.codebindingCodeType === 'CONTAINS') {

                    return (!state.associationFilter || code.association === state.associationFilter)
                        && (!state.bindingCodeFilter || code.bindingCode.includes(state.bindingCodeFilter))
                        && (!state.codeSystemFilter || code.codeSystem.includes(state.codeSystemFilter))
                        && (!state.codebindingCode || code.path.includes(state.codebindingCode))
                }

                return (!state.associationFilter || code.association === state.associationFilter)
                    && (!state.bindingCodeFilter || code.bindingCode.includes(state.bindingCodeFilter))
                    && (!state.codeSystemFilter || code.codeSystem.includes(state.codeSystemFilter))
                    && (!state.codebindingCode || code.path.startsWith(state.codebindingCode))
            })
        }

    },
    mutations: {

        updateSettings(state, payload) {
            state.popCoordEndpoint = payload.popCoordEndpoint
            state.popCoordWebSockEndpoint = payload.websocketEndpoint
        },

        setAssociationFilter(state, payload) {
            state.associationFilter = payload
        },

        setAppProps(state, payload) {
            state.appProps = payload._embedded.property
        },

        saveCode(state, payload) {
            state.codes.push(payload)
        },

        setCodes(state, payload) {
            state.codes = payload
        },

        saveProperty(state, payload) {

        },

        saveSystemStates(state, payload) {
            state.systemStatus = payload
        },

        saveServiceInstance(state, instanceId) {
            Vue.set(state.cdsHooksService.instances, state.cdsHooksService.instances.length, {
                instanceId: instanceId,
                states: []
            })
        },

        saveServiceInstanceState(state, payload) {

            if (payload.status === 'FAILED' // set completion date to now on all unfinished states and add error message
                && state.serviceInstanceStates.length > 0
                && state.serviceInstanceStates.find(existingState => existingState.instanceId === payload.instanceId)) {

                const updatedStates = state.serviceInstanceStates.map(st => {
                    if (st.instanceId === payload.instanceId && !st.completionTime) {
                        return {
                            ...st,
                            status: payload.status,
                            completionTime: (new Date()).toISOString(),
                            message: payload.message
                        }
                    }
                    return st
                })

                state.serviceInstanceStates = updatedStates
                return
            }

            if (state.serviceInstanceStates.length > 0
                && state.serviceInstanceStates.find(existingState => existingState.instanceId === payload.instanceId && existingState.state === payload.state)) {

                const updatedStates = state.serviceInstanceStates.map(st => {
                    if (st.instanceId === payload.instanceId && st.state === payload.state) {
                        return {
                            ...st,
                            status: payload.status,
                            completionTime: payload.completionTime,
                            message: payload.message
                        }
                    }
                    return st
                })

                state.serviceInstanceStates = updatedStates

            } else {
                if (!payload.stateId) {
                    payload.stateId = 'T' + state.nextTempStateId
                    state.nextTempStateId++
                }

                state.serviceInstanceStates.push(payload)
            }
        },

        saveCdsHooksServices(state, payload) {
            state.cdsHooksService = payload.services[0]
            state.cdsHooksService.instances = []
        },

        saveServiceInstanceStates(state, payload) {
            state.serviceInstanceStates = payload
        },

        setValueSets(state, payload) {
            state.valueSets = payload
        },

        setBindingCodeMappings(state, payload) {
            state.bindingCodeMappings = payload
        },

        setCodeSetDefinitions(state, payload) {
            state.codeSetDefinitions = payload
        },

        setServiceInstanceMetRuleCriteria(state, payload) {
            state.serviceInstanceMetRuleCriteria = payload
        },

        setThisEndpoint(state, endpoint) {
            state.popCoordEndpoint = endpoint
        },

        setSystemSettings(state, payload) {
            state.websocketEndpoint = payload.websocketEndpoint
            state.popCoordEndpoint = payload.populationCoordinatorEndpoint
            state.cdsHooksEndpoint = payload.cdsHooksEndpoint
        }
    },

    actions: {

        fetchSystemSettings({state, commit}) {
            let url = state.popCoordEndpoint + '/settings'
            console.log('FETCH: ' + url)

            axios.get(url)
                .then(response => {
                    console.log("FETCHED SETTINGS: " + response.data)
                    commit('setSystemSettings', response.data)
                })
        },

        fetchServiceInstanceMetRuleCriteria({state, commit}, instanceId) {
            const url = state.popCoordEndpoint + '/resultsReview/' + instanceId
            axios.get(url)
                .then(response => {
                    console.log("FETCHED EVALUATION RESULTS - " + response.data.length + " RECORDS")
                    commit('setServiceInstanceMetRuleCriteria', response.data)
                })
        },

        fetchCodeSetDefinitions({commit, state}) {
            axios.get(state.popCoordEndpoint + '/codeSetDefinitions')
                .then(response => {
                    console.log("FETCHED CODE SET DEFINITIONS - " + response.data.length + " RECORDS")
                    commit('setCodeSetDefinitions', response.data)
                })
        },

        deleteCodeAssociation({commit, state, dispatch}, id) {
            const url = state.popCoordEndpoint + '/codeAssociations/delete/' + id
            axios.get(url)
                .then(response => {
                    dispatch('fetchBindingCodeMappings')
                })
        },

        saveCodeAssociation({commit, state, dispatch}, payload) {

            axios.post(state.popCoordEndpoint + '/codeAssociations/update', payload)
                .then(response => {
                    console.log(response.data)
                    dispatch('fetchBindingCodeMappings')
                })
        },

        fetchValueSets({commit, state}) {
            const url = state.popCoordEndpoint + '/codeSets'
            axios.get(url)
                .then(response => {
                    console.log('Fetch value sets: ' + url)
                    commit('setValueSets', response.data)
                })
        },

        addServiceInstanceState({commit}, payload) {
            commit('saveServiceInstanceState', payload)
        },

        saveProperty({commit, state}, property) {

            axios.post(state.popCoordEndpoint + '/data/property/', property)
                .then(response => {
                    commit('saveProperty', response.data)
                })

        },

        fetchAppProps({commit, state}) {
            axios.get(state.popCoordEndpoint + '/data/property/')
                .then(response => {
                    commit('setAppProps', response.data)
                })
        },

        saveCode({commit, state}, code) {
            axios.post(state.popCoordEndpoint + '/data/codes/', code)
                .then(response => {
                    commit('saveCode', response.data)
                })
        },

        fetchCodes({commit, state}) {
            const urlPath = state.popCoordEndpoint + '/pathCodes/NCCN' //+ this.$state.codebindingCode.replace('/', '|')
            axios.get(encodeURI(urlPath))
                .then(response => {
                    commit('setCodes', response.data)
                })
        },

        fetchBindingCodeMappings({commit, state}) {
            const urlPath = state.popCoordEndpoint + '/bindingCodeMappings' //+ this.$state.codebindingCode.replace('/', '|')
            axios.get(encodeURI(urlPath))
                .then(response => {
                    commit('setBindingCodeMappings', response.data)
                })
        },

        fetchSystemStatus({state, commit}) {

            const gardeRestServiceUrl = state.popCoordEndpoint + '/systemStatus/'
            console.log("FETCHING " + gardeRestServiceUrl)
            axios.get(gardeRestServiceUrl)
                .then(response => {
                    commit('saveSystemStates', response.data)
                    this.dispatch('fetchCsdHooksServices')
                    console.log('System status checked and saved.')
                })
                .catch(function (error) {
                    let errorCode = 'Error'
                    if (error.response) {
                        // The request was made and the server responded with a status code
                        // out of the range 2xx
                        console.log(error.response.data)
                        console.log(error.response.status)
                        console.log(error.response.headers)
                        errorCode = 'SERVER_RESPONDED_WITH_ERROR'
                    } else if (error.request) {
                        // The request was made but no response was received
                        console.log(error.request);
                        errorCode = 'NO_RESPONSE_FROM_SERVER'
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        console.log('Error', error.message);
                        errorCode = 'INVALID_REQUEST_ERROR'
                    }
                    commit('saveSystemStates', [{
                            systemCd: 'POPULATION_COORDINATOR'
                            , systemLabel: 'Population Coordinator'
                            , status: errorCode
                        }]
                    )
                })
        },

        fetchServiceInstanceStates({commit, state}, fromDate) {

            const dt = new Date()
            let isoDate = dt.toISOString().substring(0, 10)
            if (fromDate) {
                isoDate = fromDate
            }

            const restUrl = state.popCoordEndpoint + '/data/states/search/findAllWithStartTimeAfter?startTime=' + isoDate
            console.log("Fetch Service instance states: " + restUrl)
            axios.get(restUrl)
                .then(response => {
                    commit('saveServiceInstanceStates', response.data._embedded.states)
                    console.log('Cds Hooks service list saved.')
                })
        },

        fetchCsdHooksServices({commit, state}) {
            const endpoint = state.popCoordEndpoint + '/cds-services'
            console.log("FETCH CDS HOOKS SERVICES URL: " + endpoint)
            axios.get(endpoint)
                .then(response => {
                    commit('saveCdsHooksServices', response.data)
                    console.log('Cds Hooks service list saved.')
                })
        },

        async runEvaluation({commit, state}, payload) {
            commit('saveServiceInstance', payload.instanceId)
            let gardeRestServiceUrl = state.popCoordEndpoint + '/runEvaluation'

            console.log('RUN EVALUATION URL: ' + gardeRestServiceUrl)

            const formData = new FormData()
            formData.append('instanceId', payload.instanceId)
            formData.append('serviceId', payload.serviceId)
            formData.append('patDemogFile', payload.patDemogDataFile)
            formData.append('patFHxFile', payload.patFHxDataFile)

            axios.post(gardeRestServiceUrl, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                }
            })
                .then(response => {
                    console.log(response)
                    console.log('Evaluation ' + payload.serviceId + ':' + payload.instanceId + ' completed.')
                })
                .catch(function (error) {
                    let errorCode = 'Error'
                    if (error.response) {
                        // The request was made and the server responded with a status code
                        // out of the range 2xx
                        console.log(error.response.data)
                        console.log(error.response.headers)
                        errorCode = 'ERROR_DURING_EVALUATION'
                    } else if (error.request) {
                        // The request was made but no response was received
                        console.log(error.request);
                        errorCode = 'NO_RESPONSE_FROM_EVALUATION_SERVER'
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        console.log('Error', error.message);
                        errorCode = 'INVALID_REQUEST_ERROR'
                    }

                    commit('saveServiceInstanceState',
                        {
                            instanceId: payload.instanceId
                            , serviceId: payload.serviceId
                            , status: 'FAILED'
                            , message: 'Processing error: ' + errorCode
                        })
                })
        }
    } // actions
})

import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '@/components/Dashboard.vue'
import PropertyEditor from "@/components/PropertyEditor.vue"
import TerminologyBrowser from "@/components/TerminologyBrowser.vue";
import TerminologyValueSets from "@/components/TerminologyValueSets.vue";
import TerminologyMap from "@/components/TerminologyMap.vue";
import Populations from "@/components/Populations.vue";
import CodeLookerUpper from "@/components/CodeLookerUpper.vue";
import ResultsReview from "@/components/ResultsReview.vue";
import Settings from "@/components/Settings.vue";

Vue.use(VueRouter)

const routes = [
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard
  },
  {
    path: '/settings',
    name: 'GARDE Settings',
    component: Settings
  },
  {
    path: '/populations',
    name: 'Recommender Rules',
    component: Populations,
    alias: '/'
  },
  // {
  //   path: '/',
  //   name: 'Recommender Rules',
  //   component: Populations
  // },
  {
    path: '/resultsReview/:instanceId/:startTime',
    name: 'Results Review',
    component: ResultsReview,
    props: true
  },
  {
    path: '/properties',
    name: 'Application Properties',
    component: PropertyEditor
  },
  {
    path: '/codeBrowser',
    name: 'Terminology Browser',
    component: TerminologyBrowser
  },
  {
    path: '/valueSets',
    name: 'Terminology Value Sets',
    component: TerminologyValueSets
  },
  {
    path: '/codeMappings',
    name: 'Code Mappings',
    component: TerminologyMap
  },
  {
    path: '/codeLookup',
    name: 'Terminology Code Search',
    component: CodeLookerUpper
  },
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {

  // This accesses the index doc title and sets it to the rout name (see above).
  document.title= `${process.env.VUE_APP_TITLE} - ${to.name}`
  // need to call next to complete the routing
  next()
})

export default router

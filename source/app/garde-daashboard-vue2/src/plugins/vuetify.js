import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
            light: {
                primary: '#65AFAF',
                // secondary: colors.red.base,
                // accent: colors.yellow.base,
                // error: colors.pink.base,
                // warning: colors.orange.base,
                // info: colors.lightBlue.base,
                // success: colors.green.base
            },
        },
    },
});

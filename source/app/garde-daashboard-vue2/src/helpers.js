import axios from "axios";

export default {
    // getThisEndpoint() {
    //     const urlStr = window.location.toString()
    //     let startIndex = 6
    //     if (urlStr.includes('https')) {
    //         startIndex = 7
    //     }
    //     const endIndex = urlStr.indexOf('/', (startIndex + 1))
    //     return urlStr.substring(0, endIndex)
    // },
    //
    // getServerAddress() {
    //     const urlStr = window.location.toString()
    //     let startIndex = 7
    //     if (urlStr.includes('https')) {
    //         startIndex = 8
    //     }
    //     const endIndex = urlStr.indexOf('/', (startIndex + 1))
    //     return urlStr.substring(startIndex, endIndex)
    // }

    async getCsrfTicket(url) {
        try {
            console.log("FETCH CSRF URL: " + url)
            const response = await axios.get(url)
            console.log('CSRF RESPONSE: ', response.data)
            return response.data
        } catch (error) {
            console.log(error)
        }
    }

}

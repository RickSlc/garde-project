#!/bin/bash

date

# run attached to console - see logs in terms and exit via ctrl-C
# docker run -p 8092:80 --network garde-network --name garde-webapp --rm rehr/garde-webapp

# run detached
docker run -p 8092:80 --network garde-network --name garde-webapp -d rehr/garde-webapp

# connect to instance interactively
# docker start garde-webapp -i /bin/bash

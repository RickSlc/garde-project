#!/bin/bash

date

# stop running container
docker container stop garde-webapp

# remove the container anticipating rebuild
docker container rm garde-webapp

# Remove image of the container
docker image rm rehr/garde-webapp

# Rebuild the image of the container using Dockerfile
docker build -t rehr/garde-webapp .

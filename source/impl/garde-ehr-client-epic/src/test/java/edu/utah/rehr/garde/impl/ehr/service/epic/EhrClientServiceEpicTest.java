package edu.utah.rehr.garde.impl.ehr.service.epic;

import edu.utah.rehr.garde.domain.ehr.service.IEhrClientService;
import edu.utah.rehr.garde.impl.ehr.config.EhrClientServiceTestConfig;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.assertTrue;

@SpringBootTest
@ContextConfiguration(classes = {EhrClientServiceTestConfig.class})
public class EhrClientServiceEpicTest {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @Autowired
    IEhrClientService ehrService;

    @Autowired
    InterconnectServiceClient interconnectClient;

    @Test
    public void testTest() {
        logger.log(Level.INFO, "Testing Spring configuration. Won't run unless wiring is correct.");
        assertTrue(true);
    }

    // Todo: These are integratrion tests - move to integration test project.

//    @Test
//    public void testEhrServiceWriteSde() {
//
//        String patientId = "1798212";
//        String elementId = "REHR#1590010007"; // transcript
//
//        UResult<Boolean> result = ehrService.writePatientDataElement(patientId, elementId, "GIA: bla bla bla \n PATIENT_NAME: yada yada yada.\n");
//        Boolean shouldBeFalse = result.getResult();
//
//        assertFalse(shouldBeFalse); //value exists already - no error but no write either
//    }
//
//    @Test
//    public void testEhrServiceReadSde() {
//
//        String patientId = "235732";
//        String patId = "840716";
//        String elementId = SmartDataElement.GENETIC_TEST_NEGATIVE_RESULT_OUTREACH_LINK.getId(); // transcript
//
//        UResult<String> result = ehrService.readPatientDataElement(patId, elementId);
//        String shi = result.getResult();
//
//        assertTrue(StringUtils.isNotEmpty(shi));
//    }

//    @Test
//    public void testEhrServiceReadSdeNoValue() {
//
//        String patientId = "03756046";
//        String elementId = SmartDataElement.GENETIC_COUNSELING_PRE_TESTING_OUTREACH_RESPONSE_TRANSCRIPT.getId(); // transcript
//
//        UResult<String> result = ehrService.readPatientDataElement(patientId, elementId);
//        String shouldBeEmpty = result.getResult();
//
//        assertTrue(StringUtils.isEmpty(shouldBeEmpty));
//    }
//
//    @Test
//    public void setSdeDate() throws Exception {
//
//        String patientId = "1798212";
//        String elementId = "REHR#002"; //"".GENETIC_TEST_NEGATIVE_RESULT_OUTREACH_RESPONSE_DATE.getId();
//
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); // Z for time offset
//        String dateString = "2000-01-15";
//        Date date = df.parse(dateString);
//
//        long DTE = EpicDateUtility.dateToEpicDte(date);
//
//        UResult<String> strResult = interconnectClient.getSmartDataElementValue(patientId,elementId);
//        assertEquals((DTE + ""), strResult.getResult());
//
//    }
//
//    @Test
//    public void setUtcDateTime() throws Exception {
//
//        String patientId = "1798212";
//        String elementId = "REHE#002"; //"".GENETIC_TEST_NEGATIVE_RESULT_OUTREACH_RESPONSE_DATE.getId();
//
//        TimeZone tz = TimeZone.getTimeZone("UTC");
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX"); // Z for time offset
//        df.setTimeZone(tz);
//        String dateString = "2000-01-15T13:45:33-0700";
//        Date isoDate = df.parse(dateString);
//        String isoDateString = df.format(isoDate);
//
//        UResult<Boolean> result = interconnectClient.setSmartDataElementValue(patientId, elementId, isoDateString);
//        assertTrue(result.success());
//
//        UResult<String> strResult = interconnectClient.getSmartDataElementValue(patientId,elementId);
//        assertTrue(dateString.equals(strResult.getResult()));
//
//    }


}

package edu.utah.rehr.garde.impl.ehr;

import edu.utah.rehr.garde.impl.ehr.config.EhrClientServiceTestConfig;
import edu.utah.rehr.garde.impl.ehr.utils.EpicDateUtility;
import org.json.simple.JSONObject;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import static org.junit.Assert.*;

//@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {EhrClientServiceTestConfig.class})
public class SanityCheckTests {

    @Test
    public void testTest() { // test tsst config
        assertTrue(true);
    }

    @Test
    public void testJson8601DateTime() throws Exception {

//        TimeZone tz = TimeZone.getTimeZone("UTC");
//        df.setTimeZone(tz);
        String dateString = "2000-01-15T13:45:33-0700";
//        String dateString = "2000-01-15T13:45:33-00:00"; // found this pattern in Epic. still doesn't work
//        String dateString = "2000-01-15T13:45:33Z";

        String utcPattern = "yyyy-MM-dd'T'HH:mm:ssZ";
        DateFormat utcFormat = new SimpleDateFormat(utcPattern);
        Date utcDate = utcFormat.parse(dateString);
        String utcDateString = utcFormat.format(utcDate);

        assertEquals(utcDateString,dateString);

        JSONObject root = new JSONObject();
        root.put("date",dateString);

        String dateFromJson = (String)root.get("date");
        Date dateParsedFromJson =utcFormat.parse(dateFromJson);

        assertNotNull(dateParsedFromJson);
    }

    @Test
    public void testTimeZoneOffsetFormat() throws Exception {

        String dateString = "2010-03-01T13:20:57-07";
        String pattern = "yyyy-MM-dd'T'HH:mm:ssX";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        Date date = sdf.parse(dateString);
        String compareDateString = sdf.format(date);

        assertEquals(compareDateString, dateString);
    }

    @Test
    public void testEpicTimeTranslationToAndFrom() throws Exception {

        long answer = 65451;

        String dateString = "2020-03-13";
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        Date testDate = sdf.parse(dateString);

        long epicDte = EpicDateUtility.dateToEpicDte(testDate);

        assertEquals(epicDte, answer);

        LocalDate localDate = EpicDateUtility.epicDteToLocalDate(epicDte);

        assertTrue(
                localDate.getYear() == 2020
                && localDate.getMonthValue() == 3
                && localDate.getDayOfMonth() == 13
        );
    }
}

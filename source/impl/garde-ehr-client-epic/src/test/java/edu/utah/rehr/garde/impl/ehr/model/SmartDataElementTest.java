package edu.utah.rehr.garde.impl.ehr.model;


import org.junit.Test;

import static org.junit.Assert.assertTrue;

enum Sde1 { // implements ISmartDataElementMethods {

    SDE_A("CODE_1A")
    , SDE_B("CODE_1B") ;

    private final String code;

    Sde1(String cd) {
        this.code = cd;
    }

//    @Override
    public String getCode() {
        return code;
    }
}

enum Sde2 { // implements ISmartDataElementMethods {

    SDE_A("CODE_2A")
    , SDE_B("CODE_2B") ;

    private final String code;

    Sde2(String cd) {
        this.code = cd;
    }

//    @Override
    public String getCode() {
        return code;
    }
}

//class NccnSde {
//
//    public ISmartDataElementMethods SDE_A(){
//        return Sde1.SDE_A;
//    }
//
//    public ISmartDataElementMethods SDE_B(){
//        return Sde1.SDE_B;
//    }
//}

public class SmartDataElementTest {

    @Test
    public void SdeTest() {

        assertTrue(true);

    }


}

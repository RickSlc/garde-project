package edu.utah.rehr.garde.impl.ehr.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages = {"edu.utah.rehr.garde.impl.ehr"})
@PropertySource("classpath:application-ehrtest.properties")
@Import({EhrClientConfig.class})
public class EhrClientServiceTestConfig {

}

package edu.utah.rehr.garde.impl.ehr.service.epic;

import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.model.PatientFact;
import edu.utah.rehr.garde.domain.ehr.service.IEhrClientService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.StringUtils;
import org.hl7.fhir.dstu3.model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class EhrClientServiceEpic implements IEhrClientService {

    Logger logger =  Logger.getLogger(this.getClass().getName());

    @Autowired
    InterconnectServiceClient interconnectServiceClient;

    @Override
    public UResult<String> readPatientDataElement(String patientId, String dataElementId) {
        return interconnectServiceClient.getSmartDataElementValue(patientId, dataElementId);
    }

    /**
     * Write a SDE when no value exists already - no overwrites.
     * Patient-level SDE
     * Single value
     * String type (need to add/consider dates)
     * @param patientId
     * @param dataElementId
     * @param dataElementValue
     * @return successResult true when new value is written; Returns successResult false when a value exists already. errorResult false when there is an error.
     */
    @Override
    public UResult<Boolean> writePatientDataElement(String patientId, String dataElementId, String dataElementValue) {

        // Check for existing value
        UResult<String> sdeReadValueResult = interconnectServiceClient.getSmartDataElementValue(patientId,dataElementId);
        String sdeValue = null;

        if (sdeReadValueResult.success()) { // no errors

            if (StringUtils.isNotEmpty(sdeReadValueResult.getResult())) { // and found a value. Exit successfully with false (not written)
                sdeValue = sdeReadValueResult.getResult();
                return UResult.successResult(false);
            }

        } else { // there was an error
            UResult.errorResult(false);
        }

        UResult<Boolean> sdeWriteResult = interconnectServiceClient.setSmartDataElementValue(patientId,dataElementId,dataElementValue);

        return sdeWriteResult;
    }

    @Override
    public UResult<Integer> createCohortFile(String filePathAndName, String cohortId, List<PatientFact> patients) {

        try (
                BufferedWriter writer = Files.newBufferedWriter(Paths.get(filePathAndName));
                CSVPrinter csvPrinter = new CSVPrinter(
                        writer, CSVFormat.DEFAULT.withHeader("MRN", "PAT_LAST_NAME", "PAT_FIRST_NAME", "PAT_MIDDLE_NAME", "DOB", "SEX")
                )) {

            for (PatientFact patientFact:patients) {

                String sex = null;
                if (StringUtils.isNotEmpty(patientFact.getInternalSexCode())) {
                    sex = patientFact.getInternalSexCode().substring(0,1);
                } else {
                    sex = "U";
                }

                csvPrinter.printRecord(
                        patientFact.getOrgPatientId(),
                        patientFact.getLastName(),
                        patientFact.getFirstName(),
                        patientFact.getMiddleName(),
                        patientFact.getDob(),
                        sex
                );

            }

            csvPrinter.flush();

        } catch (Exception e) {
            logger.log(Level.SEVERE, "CSV generation method crashed. - STOP.", e);
            UResult.errorResult(e);
        }

        return UResult.successResult(patients.size());
    }

    @Override
    public UResult<Integer> addPatientsToCohort(String cohortId, List<Patient> patients) {
        return UResult.errorResult(0);
    }

}

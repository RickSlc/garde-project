package edu.utah.rehr.garde.impl.ehr.config.epic;

import org.apache.commons.lang3.StringUtils;

public class InterconnectConfig {

    private String interconnectServer;
    private String writeSdeService;
    private String readSdeService;
    private String fhirMetadataService;
    private String userId;
    private String clientId;
    private String interconnectUser;
    private String interconnectPassword;

    public void setFhirMetadataService(String fhirMetadataService) {
        this.fhirMetadataService = fhirMetadataService;
    }

    public String getFhirMetadataService() {
        return fhirMetadataService;
    }

    public String getInterconnectServer() {
        return interconnectServer;
    }

    public void setInterconnectServer(String interconnectServer) {
        this.interconnectServer = interconnectServer;
    }

    public String getWriteSdeService() {
        return writeSdeService;
    }

    public void setWriteSdeService(String writeSdeService) {
        this.writeSdeService = writeSdeService;
    }

    public String getReadSdeService() {
        return readSdeService;
    }

    public void setReadSdeService(String readSdeService) {
        this.readSdeService = readSdeService;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getInterconnectUser() {
        return interconnectUser;
    }

    public void setInterconnectUser(String interconnectUser) {
        this.interconnectUser = interconnectUser;
    }

    public String getInterconnectPassword() {
        return interconnectPassword;
    }

    public void setInterconnectPassword(String interconnectPassword) {
        this.interconnectPassword = interconnectPassword;
    }

    public Boolean isValid() {
        return StringUtils.isNotEmpty(interconnectServer)
                && StringUtils.isNotEmpty(interconnectUser)
                && StringUtils.isNotEmpty(interconnectPassword)
                && StringUtils.isNotEmpty(writeSdeService)
                && StringUtils.isNotEmpty(readSdeService)
                && StringUtils.isNotEmpty(userId)
                && StringUtils.isNotEmpty(clientId);
    }

}

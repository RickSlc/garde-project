package edu.utah.rehr.garde.impl.ehr.config;

import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.utils.SecurityUtils;
import edu.utah.rehr.garde.impl.ehr.config.epic.InterconnectConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Logger;

@Configuration
public class EhrClientConfig {

    private final Logger log = Logger.getLogger(this.getClass().getName());

    @Autowired
    private Environment env;

    /**
     * Sets up Interconnect settings using the application.properties - see required properties in the method
     * Encrypted fields require a master key is set in the system's environment OPENCDS_MASTER
     * The master key must be the secret key used to encrypt the fields.
     * In Linux OPENCDS_MASTER can be set in the app user's $HOME/.bash_profile
     * Example: export OPENCDS_MASTER=hcj3ue9rbv4yfjdf09*=zkxo
     *
     * @return
     */
    @Bean
    InterconnectConfig interconnectConfig() {

        InterconnectConfig config = new InterconnectConfig();
        config.setClientId(env.getProperty("epic.clientId"));
        config.setUserId(env.getProperty("epic.userId"));
        config.setInterconnectServer(env.getProperty("epic.interconnectServer"));
        config.setReadSdeService(env.getProperty("epic.readSdeService"));
        config.setWriteSdeService(env.getProperty("epic.writeSdeService"));
        config.setFhirMetadataService(env.getProperty("epic.fhirMetadataService"));

        UResult<String> interconnectUserResult = SecurityUtils.decrypt(env.getProperty("epic.interconnectUser.encrypted"));

        if (interconnectUserResult.success()) {
            String u = interconnectUserResult.getResult();
            config.setInterconnectUser(u);
        } else {
            log.warning("INTERCONNECT USER DECRYPTION FAILED: " + env.getProperty("epic.interconnectUser.encrypted"));
        }

        UResult<String> interconnectPasswordResult = SecurityUtils.decrypt(env.getProperty("epic.interconnectPassword.encrypted"));

        if (interconnectPasswordResult.success()) {
            String p = interconnectPasswordResult.getResult();
            config.setInterconnectPassword(p);
        } else {
            log.warning("INTERCONNECT PASSWORD DECRYPTION FAILED: " + env.getProperty("epic.interconnectPassword.encrypted"));
        }

        return config;
    }

    /**
     * Setup Spring's RestTemplate with the authentication settings
     * @return
     */
    @Bean
    @Qualifier("interconnectRestTemplate")
    RestTemplate interconnectRestTemplate() {

        InterconnectConfig interconnectConfig = interconnectConfig();

        RestTemplateBuilder builder = new RestTemplateBuilder();

        if (!interconnectConfig.isValid()) {
            log.warning("REST TEMPLATE CONFIG INCOMPLETE DUE TO INVALID INTERCONNECT PROPERTIES");
            return builder.build();
        }

        RestTemplate restTemplate = builder
                .basicAuthentication(
                        interconnectConfig.getInterconnectUser()
                        , interconnectConfig.getInterconnectPassword()
                )
                .build();

        return restTemplate;
    }

}

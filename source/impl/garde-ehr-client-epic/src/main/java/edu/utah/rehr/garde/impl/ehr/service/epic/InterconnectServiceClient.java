package edu.utah.rehr.garde.impl.ehr.service.epic;

import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.impl.ehr.config.epic.InterconnectConfig;
import org.apache.http.HttpStatus;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class InterconnectServiceClient {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    private final RestTemplate interconectRestTemplate;
    private final InterconnectConfig interconnectConfig;

    @Autowired
    public InterconnectServiceClient(
            @Qualifier("interconnectRestTemplate") RestTemplate interconectRestTemplate
            , InterconnectConfig interconnectConfig) {

        this.interconectRestTemplate = interconectRestTemplate;
        this.interconnectConfig = interconnectConfig;
    }

    public UResult<Boolean> isInterconnectAlive() {

        String restUrl = interconnectConfig.getInterconnectServer() + interconnectConfig.getFhirMetadataService();
        RestTemplate restTemp = new RestTemplate();
        try {

            ResponseEntity<String> response = restTemp.getForEntity(restUrl, String.class);
            if (response.getStatusCode().value() == HttpStatus.SC_OK) {
                return UResult.successResult(true);
            } else {
                return UResult.errorResult(false);
            }
        } catch (HttpClientErrorException httpClientErrorException) {
            return UResult.errorResult(httpClientErrorException);
        }
    }

    /**
     * Create the JSON request for Epic's smart data element write interconnect service
     *
     * @param patientId    - pat_id (ehr_pat_id in the intermediate database) from Clarity is the "ExternalID" in the Interconnect services
     * @param elementId    - SDI value of the smart data element (example REHR#1590010004)
     * @param elementValue - value to store in the smart data element
     * @return the request in JSON
     */
    private String getSdeWriteRequest(String patientId, String elementId, String elementValue) {

        JSONObject sdeObj = new JSONObject();

        sdeObj.put("SmartDataID", elementId);
        sdeObj.put("SmartDataIDType", "SDI");

        JSONArray sdeValueArray = new JSONArray();
        sdeValueArray.add(elementValue);

        sdeObj.put("Values", sdeValueArray);

        JSONArray sdeCommentArray = new JSONArray();
        sdeCommentArray.add("Value set on " + new Date());

        sdeObj.put("Comments", sdeCommentArray);

        JSONArray sdeObjValueArray = new JSONArray();
        sdeObjValueArray.add(sdeObj);

        JSONObject rootObj = new JSONObject();

        rootObj.put("ContextName", "PATIENT");
        rootObj.put("EntityID", patientId);
        rootObj.put("EntityIDType", "External");
        rootObj.put("ContactID", "");
        rootObj.put("ContactIDType", "");
        rootObj.put("UserID", interconnectConfig.getUserId()); // user on record for writing the SDE
        rootObj.put("UserIDType", "External");
        rootObj.put("SmartDataValues", sdeObjValueArray);

        return rootObj.toJSONString();
    }


    /**
     * Create the JSON request for Epic's smart data element read interconnect service
     *
     * @param patientId - pat_id (ehr_pat_id in the intermediate database) from Clarity is the "ExternalID" in the Interconnect services
     * @param elementId - SDI value of the smart data element (example REHR#1590010004)
     * @return
     */
    private String getSdeReadRequest(String patientId, String elementId) {

        JSONObject idObj = new JSONObject();
        idObj.put("ID", elementId);
        idObj.put("Type", "SDI");

        JSONArray sdeIDArray = new JSONArray();
        sdeIDArray.add(idObj);

        JSONObject rootObj = new JSONObject();

        rootObj.put("ContextName", "PATIENT");
        rootObj.put("EntityID", patientId);
        rootObj.put("EntityIDType", "External");
        rootObj.put("ContactID", "");
        rootObj.put("ContactIDType", "");
        rootObj.put("UserID", interconnectConfig.getUserId());
        rootObj.put("UserIDType", "External");
        rootObj.put("SmartDataIDs", sdeIDArray);

        return rootObj.toJSONString();
    }

    public UResult<Boolean> setSmartDataElementValue(String patientId, String elementId, String elementValue) {

        String requestJson = getSdeWriteRequest(patientId, elementId, elementValue);

        HttpEntity httpEntity = getPreparedHttpEntity(requestJson);
        String restUrl = interconnectConfig.getInterconnectServer() + interconnectConfig.getWriteSdeService();
        ResponseEntity<String> result;
        try {
            result = interconectRestTemplate.exchange(
                    restUrl
                    , HttpMethod.PUT
                    , httpEntity
                    , String.class
            );
        } catch (Exception e) {
            return UResult.errorResult(e);
        }

        if (HttpStatus.SC_OK != result.getStatusCode().value() || !wasSuccessfulRequest(result.getBody())) {
            logger.log(Level.WARNING, "RESPONSE ENTITY:\n" + result);
            return UResult.errorResult(false);
        }

        return UResult.successResult(true);
    }

    private Boolean wasSuccessfulRequest(String serviceResponseJson) {

        JSONParser parser = new JSONParser();
        JSONObject root = null;
        try {
            root = (JSONObject) parser.parse(serviceResponseJson);
        } catch (ParseException e) {
            logger.log(Level.WARNING, "Error parsing Epic service response.", e);
            return false;
        }
        Boolean success = (Boolean)root.get("Success");

        return success;
    }

    /**
     * Retrieves a smart data element (SDE) value. Constraints:
     * SDE is PATIENT-level
     * SDE has one value
     * @param patientId
     * @param elementId
     * @return
     */
    public UResult<String> getSmartDataElementValue(String patientId, String elementId) {

        String requestJson = getSdeReadRequest(patientId, elementId);
        HttpEntity httpEntity = getPreparedHttpEntity(requestJson);
        String restUrl = interconnectConfig.getInterconnectServer() + interconnectConfig.getReadSdeService();

        ResponseEntity<String> result = null;

        try {

            result = interconectRestTemplate.exchange(
                    restUrl
                    , HttpMethod.POST
                    , httpEntity
                    , String.class
            );

        } catch (HttpClientErrorException httpClientErrorException) {
            return UResult.errorResult(httpClientErrorException);
        }

        if (HttpStatus.SC_OK != result.getStatusCode().value()) {
            logger.log(Level.WARNING, "RESPONSE ENTITY STATUS NOT 200:\n" + result);
            return UResult.errorResult(result.toString());
        }

        String returnValue = null;

        try {
            returnValue = getSmartDataElementValueFromJson(result.getBody());
        } catch (Exception e) {
            logger.log(Level.SEVERE, "SMART DATA ELEMENT PARSE FAILURE. Service response: " + result);
            return UResult.errorResult(e);
        }

        return UResult.successResult(returnValue);
    }

    /**
     * Extract return value from JSON response. When value > 3000 chars, Epic splits it into 3000 char chunks.
     * This method concatenates the chunks back into a single String.
     * @param json Http response in JSON
     * @return the string value of the smart data element
     * @throws Exception
     */
    private String getSmartDataElementValueFromJson(String json) throws Exception {

        JSONParser parser = new JSONParser();
        JSONObject root = (JSONObject) parser.parse(json);
        JSONArray sdeArray = (JSONArray) root.get("SmartDataValues");
        JSONObject sdeValues = (JSONObject) sdeArray.get(0);

        JSONArray valuesArray = (JSONArray) sdeValues.get("Values"); // creates multiple values when string > 3000

        StringBuffer value = new StringBuffer();
        for (Object piece : valuesArray) {
            value.append(piece);
        }

        return value.toString();
    }

    /**
     * Setup HttpHeaders for service calls
     * @param requestJson service request in JSON according to Epic's specification.
     * @return
     */
    private HttpEntity getPreparedHttpEntity(String requestJson) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Epic-Client-ID", interconnectConfig.getClientId());
        headers.add("Content-Type", "application/json");
        headers.add("Content-Length", "" + requestJson.length());
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));

        HttpEntity<String> httpEntity = new HttpEntity<>(requestJson, headers);

        return httpEntity;
    }

}

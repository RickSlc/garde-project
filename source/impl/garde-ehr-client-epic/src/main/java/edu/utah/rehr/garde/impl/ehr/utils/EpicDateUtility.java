package edu.utah.rehr.garde.impl.ehr.utils;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class EpicDateUtility {

    public static long dateToEpicDte(Date date) {
        LocalDate localDate = toLocalDate(date);
        return localDateToEpicDte(localDate);
    }

    public static LocalDate epicDteToLocalDate(long epicDte) {
        return ChronoUnit.DAYS
                .addTo(getEpicDteBeginLocalDate(), epicDte)
                .atStartOfDay()
                .toLocalDate();
    }

    public static LocalDate getEpicDteBeginLocalDate() {
        return LocalDate.of(1840, 12, 31);
    }

    public static long localDateToEpicDte(LocalDate localDate) {
        return ChronoUnit.DAYS.between(getEpicDteBeginLocalDate(), localDate);
    }

    public static LocalDate toLocalDate(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime()
                .toLocalDate();
    }

}

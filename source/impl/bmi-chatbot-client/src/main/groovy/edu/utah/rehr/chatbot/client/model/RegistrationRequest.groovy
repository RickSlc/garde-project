package edu.utah.rehr.chatbot.client.model

class RegistrationRequest {

    /**     * Required. Represents the Id of the chatbot for what the session is being registered.     */
    Long chatbotId

    /**     * Optional. Represents a session ID for the session being registered. If not informed the server will generate a     * sessionId automatically.     */
    String sessionId

    /**     * Optional. Indicates that the chatbot session can only start or have interaction after the start date. If no start     * date is indicated, the chatbot session can start at anytime.     */
    Date startBy

    /**     * Optional. Indicates that the chatbot session will not carry conversations after the finish date.     * If no finish date is indicated, the chatbot session can can proceed indefinitely.     */
    Date finishBy

    /**     * Optional. Indicates what is the initial intent, so the chatbot client (e.g. chatwidget) can trigger the initial     * intent on the user's behalf for a more precise conversation     */
    String initialIntent

    /**     * Optional. Indicates the callback rest service that should be called when the chatbot session is finished     */
    String callbackUrl

    /**     * Optional. Indicates what is the user display avatar. If not informed a default will be displayed on the     * chatbot widget. The format can be a base64 encoded image or an image URL.     */
    String userDisplayAvatar

    /**     * Optional. Indicates what is the chat display avatar. If not informed a default will be displayed on the     * chatbot widget. The format can be a base64 encoded image or an image URL.     */
    String chatbotDisplayAvatar

    /**     * Optional. Indicates what is the user display name (e.g. first name, or nickname).     * If not informed a default "You" will be displayed on the chatbot widget.     */
    String userDisplayName

    /**     * Optional. Indicates what is the chatbot display name (e.g. GIA - Genetic Information Assistant.).     * If not informed a default "Chatbot" will be displayed on the chatbot widget.     */
    String chatbotDisplayName
    String createSessionSecret
    String label

    /**     * Optional. Profile indicates what profile the chatbot session will run, some option buttons may only appear     * under certain profiles. */
    String profile

//    /**     * Optional. Indicates the initial value for the chatbot. So the chatbot can know some information about the user     * e.g. (name, date of birth, address)     */
//    Map<String, Object> initialData

}

package edu.utah.rehr.chatbot.client.service

import com.google.gson.Gson
import edu.utah.rehr.chatbot.client.model.RegistrationRequest
import edu.utah.rehr.chatbot.client.model.RegistrationResponse
import edu.utah.rehr.garde.domain.core.accessory.UResult
import groovy.util.logging.Log
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.*
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
@Log
class ChatbotClientService {

    @Value('${chatbot.endpoint:http://127.0.0.1:8092/ubot/}')
    String chatbotEndpoint

    Gson gson = new Gson()

    Boolean isAlive(String url) {
        return true
    }

    UResult<RegistrationResponse> registerSession(RegistrationRequest registrationRequest) {

        String jsonRequest = null
        ResponseEntity<String> response = null
        String restRequestUrl = chatbotEndpoint + "coordinator/registerSession"

        try {
            RestTemplate restRequest = new RestTemplate()
            HttpHeaders headers = new HttpHeaders()
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON))
            headers.setContentType(MediaType.APPLICATION_JSON)
            jsonRequest = gson.toJson(registrationRequest)
            HttpEntity<String> httpEntity = new HttpEntity<>(jsonRequest, headers)

            response = restRequest.exchange(restRequestUrl, HttpMethod.POST, httpEntity, String.class)
            RegistrationResponse registrationResponse = gson.fromJson(response.getBody(), RegistrationResponse.class)

            if (!HttpStatus.OK.equals(response.getStatusCode())) {
                String error = "ERROR CREATING CHATBOT SESSION: " + jsonRequest + "\nRESPONSE STATUS" + response.getStatusCode() + "\n" + response.getBody()
                log.severe(error)
                return UResult.errorResult(registrationResponse, error)
            }

            return UResult.successResult(registrationResponse)

        } catch (Exception e) {
            String error = "ERROR CREATING CHATBOT SESSION: " + jsonRequest + "\nRESPONSE STATUS " + response.getStatusCode() + "\n" + response.getBody()
            log.severe(error)
            RegistrationResponse registrationResponse = new RegistrationResponse()
            registrationResponse.errorMessage = error
            return UResult.errorResult(registrationResponse, e)
        }

    }

    String getSessionLink(String sessionId) {
        return chatbotEndpoint + "chatapp/" + sessionId
    }

}

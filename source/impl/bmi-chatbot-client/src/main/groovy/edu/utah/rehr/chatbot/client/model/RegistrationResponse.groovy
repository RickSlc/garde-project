package edu.utah.rehr.chatbot.client.model

class RegistrationResponse {

    String sessionId
    String errorMessage

}

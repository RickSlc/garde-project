package edu.utah.rehr.garde.impl.sftp;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.provider.IRemoteFileProvider;
import edu.utah.rehr.garde.impl.provider.SftpProperties;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Logger;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@SpringBootTest
@ContextConfiguration(classes = {AppConfigSftpTest.class})
public class SftpApiTest {

    private final Logger log = Logger.getLogger(this.getClass().getName());

    @Autowired
    IRemoteFileProvider sftpProvider;

    @Autowired
    SftpProperties sftpProperties;

    String localTestFileDirectory = "tmp-test-data";
    String remoteTestFileInDirectory;
    String remoteTestFileOutDirectory;

    Boolean stopTests = true;

    @Before
    public void cleanup() {

        if (stopTests) return;

        remoteTestFileInDirectory = sftpProperties.getRemoteRootDirectory() + "/in/integtest";
        remoteTestFileOutDirectory = sftpProperties.getRemoteRootDirectory() + "/out/integtest";
        sftpProvider.deleteRemoteFiles(remoteTestFileInDirectory + "/*");
        sftpProvider.deleteRemoteFiles(remoteTestFileOutDirectory + "/*");
    }

    @Test
    public void listDirectory() {

        if (stopTests) { //getChannelSftp()==null) {
            log.warning("Unable to connect to sftp server - sftp tests not performed.");
            return;
        }

        UResult<List<String>> files = sftpProvider.lsRemoteDirectory(sftpProperties.getRemoteRootDirectory() + "/*.data");

        for (String fileName:files.getResult()) {
            log.info("LS FILE: " + fileName);
        }

        assertTrue(files.getResult().size() > 0);
    }

    @Test
    public void pushGetDeleteLocalAndRemoteFile() {

        if (stopTests) { // getChannelSftp()==null) {
            log.warning("Unable to connect to sftp server - sftp tests not performed.");
            return;
        }

        String testFileName = createLocalTestFile(localTestFileDirectory + "/out");
        String localTestFilePath = localTestFileDirectory + "/out/" + testFileName;
        String remoteTestFilePath = remoteTestFileInDirectory + "/" + testFileName;

        UResult<Boolean> res1 = sftpProvider.putOnRemoteFileSystem(localTestFilePath, remoteTestFileInDirectory);
        assertTrue(res1.success());

        UResult<Boolean> res2 = sftpProvider.getRemoteFile(remoteTestFilePath, localTestFileDirectory + "/in");
        assertTrue(res2.success());

        UResult<Boolean> res3 = sftpProvider.deleteRemoteFiles(remoteTestFilePath);
        assertTrue(res3.success());

        UResult<Boolean> res31 = sftpProvider.remoteFileExists(remoteTestFilePath);
        assertFalse(res31.getResult());

        UResult<Boolean> res4 = sftpProvider.deleteLocalFile(localTestFilePath);
        assertTrue(res4.success());

        UResult<Boolean> res41 = sftpProvider.localFileExists(localTestFilePath);
        assertFalse(res41.getResult());
    }

    @Test
    public void pushMoveDeleteRemoteFile() {

        if (stopTests) { //getChannelSftp()==null) {
            log.warning("Unable to connect to sftp server - sftp tests not performed.");
            return;
        }

        String testFileName = createLocalTestFile(localTestFileDirectory + "/out");
        String localTestFilePath = localTestFileDirectory + "/out/" + testFileName;
        String remoteTestFilePath = remoteTestFileInDirectory + "/" + testFileName;
        String movedRemoteTestFilePath = remoteTestFileOutDirectory + "/" + testFileName;

        UResult<Boolean> res1 = sftpProvider.putOnRemoteFileSystem(localTestFilePath, remoteTestFileInDirectory);
        assertTrue(res1.success());

        UResult<Boolean> res11 = sftpProvider.moveRemoteFile(remoteTestFilePath, movedRemoteTestFilePath);
        assertTrue(res11.success());

        UResult<Boolean> res12 = sftpProvider.remoteFileExists(movedRemoteTestFilePath);
        assertTrue(res12.getResult());

        UResult<Boolean> res120 = sftpProvider.remoteFileExists(remoteTestFilePath);
        assertFalse(res120.getResult());

        UResult<Boolean> res13 = sftpProvider.deleteLocalFile(localTestFilePath);
        assertTrue(res13.getResult());

        UResult<Boolean> res14 = sftpProvider.deleteRemoteFiles(movedRemoteTestFilePath);
        assertTrue(res14.getResult());

    }

    @Test
    public void pushRemoteFilesDelete() {

        if (stopTests) { //getChannelSftp()==null) {
            log.warning("Unable to connect to sftp server - sftp tests not performed.");
            return;
        }

        String testFileName = createLocalTestFile(localTestFileDirectory + "/out");
        String testFileName2 = createLocalTestFile(localTestFileDirectory + "/out");
        String localTestFilesPath = localTestFileDirectory + "/out/test-*";

        // Can use wildcard to move multiple files
        UResult<Boolean> res1 = sftpProvider.putOnRemoteFileSystem(localTestFilesPath, remoteTestFileInDirectory);
        assertTrue(res1.success());

        // Check if they exist
        UResult<Boolean> res12 = sftpProvider.remoteFileExists(remoteTestFileInDirectory + "/" + testFileName);
        assertTrue(res12.getResult());

        UResult<Boolean> res13 = sftpProvider.remoteFileExists(remoteTestFileInDirectory + "/" + testFileName2);
        assertTrue(res13.getResult());

        // Cleanup
        UResult<Boolean> res14 = sftpProvider.deleteLocalFile(localTestFileDirectory + "/out/" + testFileName);
        assertTrue(res14.getResult());

        UResult<Boolean> res140 = sftpProvider.deleteLocalFile(localTestFileDirectory + "/out/" + testFileName2);
        assertTrue(res140.getResult());

        UResult<Boolean> res15 = sftpProvider.deleteRemoteFiles(remoteTestFileInDirectory + "/test-*");
        assertTrue(res15.getResult());

        // Check to make sure they no longer exist
        UResult<Boolean> res16 = sftpProvider.remoteFileExists(remoteTestFileInDirectory + "/" + testFileName);
        assertFalse(res16.getResult());

        UResult<Boolean> res17 = sftpProvider.remoteFileExists(remoteTestFileInDirectory + "/" + testFileName2);
        assertFalse(res17.getResult());

    }

    private String createLocalTestFile(String localTestFilePath) {

        long timeNow = System.currentTimeMillis();
        String fileName = "test-" + timeNow + ".txt";
        String fileContent = "Test file " + fileName;

        try {
            Files.write(Paths.get(localTestFilePath + "/" + fileName), fileContent.getBytes());

            return fileName;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ChannelSftp getChannelSftp() {

        try {
            JSch jsch = new JSch();
            String knownHostsPath = System.getProperty("user.dir") + "/.ssh/known_hosts";
            jsch.setKnownHosts(knownHostsPath);
            Session jschSession = jsch.getSession(sftpProperties.getUsername(), sftpProperties.getServerAddress());
            jschSession.setPassword(sftpProperties.getPassword());

            java.util.Properties prop = new java.util.Properties();
            prop.put("StrictHostKeyChecking", "no");

            jschSession.setConfig(prop);
            int timeoutSeconds = 5 * 1000;
            jschSession.setTimeout(timeoutSeconds);
            jschSession.connect();

            return (ChannelSftp) jschSession.openChannel("sftp");

        } catch (Exception e) {
//            e.printStackTrace();
            return null;
        }

    }


}

package edu.utah.rehr.garde.impl.sftp;

import edu.utah.rehr.garde.domain.core.provider.IRemoteFileProvider;
import edu.utah.rehr.garde.domain.core.utils.SecurityUtils;
import edu.utah.rehr.garde.impl.provider.RemoteFileProviderSftp;
import edu.utah.rehr.garde.impl.provider.SftpProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:application-sftpTest.properties")
public class AppConfigSftpTest {

    @Autowired
    private Environment env;

    @Bean
    public SftpProperties sftpProperties() {

        SftpProperties props = new SftpProperties();

        props.setUsername(
                SecurityUtils.unencrypt(
                        env.getProperty("sftp.username.encrypted")
                )
        );

        props.setPassword(
                SecurityUtils.unencrypt(
                env.getProperty("sftp.password.encrypted"))
        );

        props.setServerAddress(env.getProperty("sftp.server"));
        props.setRemoteRootDirectory(env.getProperty("sftp.folder.data"));

        return props;
    }

    @Bean
    IRemoteFileProvider sftpProvider() {
        return new RemoteFileProviderSftp(sftpProperties());
    }

}

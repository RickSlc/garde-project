package edu.utah.rehr.garde.impl.provider;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.provider.IRemoteFileProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class RemoteFileProviderSftp implements IRemoteFileProvider {

    private final SftpProperties sftpProperties;

    public RemoteFileProviderSftp(SftpProperties sftpProperties) {
        assert sftpProperties != null;
        this.sftpProperties = sftpProperties;
    }

    @Override
    public UResult<List<String>> lsRemoteDirectory(String remoteDirectorySearchPattern) {

        List<String> fileNames = new ArrayList<>();

        try {
            ChannelSftp sftp = getChannelSftp();
            sftp.connect();
            Vector<ChannelSftp.LsEntry> vector = new Vector<>();
            vector.addAll(sftp.ls(remoteDirectorySearchPattern)); // load all the txt file transcript metadata into the vector
            vector.forEach(
                    (fileEntry) -> {
                        fileNames.add(fileEntry.getFilename());
                    }
            );

        } catch (Exception e) {
            return UResult.errorResult(fileNames, e);
        }

        return UResult.successResult(fileNames);
    }

    @Override
    public UResult<Boolean> deleteRemoteFiles(String remoteFilePath) {

        try {
            ChannelSftp sftp = getChannelSftp();
            sftp.connect();
            sftp.rm(remoteFilePath);
            sftp.disconnect();

            return UResult.successResult(true);

        } catch(Exception e){
            return UResult.errorResult(false, e);
        }

    }

    @Override
    public UResult<Boolean> remoteFileExists(String remoteFilePath) {

        try {
            ChannelSftp sftp = getChannelSftp();
            sftp.connect();
            Vector fileList = new Vector();
            fileList.addAll(sftp.ls(remoteFilePath + "*"));
            sftp.disconnect();

            return UResult.successResult(fileList.size() > 0);

        } catch(Exception e){
            return UResult.errorResult(false, e);
        }
    }

    @Override
    public UResult<Boolean> getRemoteFile(String remoteFilePath, String localFilePath) {

        try {
            ChannelSftp sftp = getChannelSftp();
            sftp.connect();
            sftp.get(remoteFilePath, localFilePath);
            sftp.disconnect();

            return UResult.successResult(true);

        } catch(Exception e){
            return UResult.errorResult(false, e);
        }
    }

    @Override
    public UResult<Boolean> putOnRemoteFileSystem(String localFilePath, String remoteFilePath) {

        try {
            ChannelSftp sftp = getChannelSftp();
            sftp.connect();
            sftp.put(localFilePath, remoteFilePath);
            sftp.disconnect();

            return UResult.successResult(true);

        } catch(Exception e){
            return UResult.errorResult(false, e);
        }
    }

    @Override
    public UResult<Boolean> moveRemoteFile(String remoteSourceFilePath, String remoteDestinationFilePath) {

        try {
            ChannelSftp sftp = getChannelSftp();
            sftp.connect();
            sftp.rename(remoteSourceFilePath, remoteDestinationFilePath);
            sftp.disconnect();

            return UResult.successResult(true);

        } catch(Exception e){
            return UResult.errorResult(false, e);
        }
    }

    private ChannelSftp getChannelSftp() throws Exception {

        JSch jsch = new JSch();
        String knownHostsPath = System.getProperty("user.dir") + "/.ssh/known_hosts";
        jsch.setKnownHosts(knownHostsPath);
        Session jschSession = jsch.getSession(sftpProperties.getUsername(), sftpProperties.getServerAddress());
        jschSession.setPassword(sftpProperties.getPassword());

        java.util.Properties prop = new java.util.Properties();
        prop.put("StrictHostKeyChecking", "no");

        jschSession.setConfig(prop);
        jschSession.connect();

        return (ChannelSftp) jschSession.openChannel("sftp");
    }

}

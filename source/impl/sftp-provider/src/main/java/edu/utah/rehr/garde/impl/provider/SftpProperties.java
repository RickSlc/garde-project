package edu.utah.rehr.garde.impl.provider;

public class SftpProperties {

    private String username;
    private String password;
    private String serverAddress;
    private String remoteRootDirectory;
    private String localRootDirectory;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getServerAddress() {
        return serverAddress;
    }

    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    public String getRemoteRootDirectory() {
        return remoteRootDirectory;
    }

    public void setRemoteRootDirectory(String remoteRootDirectory) {
        this.remoteRootDirectory = remoteRootDirectory;
    }

    public String getLocalRootDirectory() {
        return localRootDirectory;
    }

    public void setLocalRootDirectory(String localRootDirectory) {
        this.localRootDirectory = localRootDirectory;
    }

}

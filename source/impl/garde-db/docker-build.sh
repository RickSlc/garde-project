#/bin/bash

# stop running container
docker container stop intdb

# remove the container anticpating rebuild
docker container rm intdb

# Remove image of the container
docker image rm rehr/mysql-intdb

# Rebuild the image of the container using Dockerfile
docker build -t rehr/mysql-intdb .

# Create and run new container from the image - this is for local testing.
docker run -p 3306:3306 --name intdb -d rehr/mysql-intdb

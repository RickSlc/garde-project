package edu.utah.rehr.garde.db.model

import jakarta.persistence.*

@Entity
@Table(name="g_evaluator_fhir_resource")
class EvaluatorFhirResource {

    @Id
    @GeneratedValue
    @Column(name="fhir_resource_gid")
    Long gid

    @Column(name="prefetch_name", nullable = false)
    String prefetchName

    @Column(name="fhir_resource", nullable = true)
    String fhirResource

    @Column(name="fhir_query", nullable = false)
    String fhirQuery

    @Column(name="cardinality", nullable = false)
    String cardinality

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "evaluator_gid")
    EvaluatorEntity evaluator

}

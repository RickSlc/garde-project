package edu.utah.rehr.garde.db.model

import jakarta.persistence.*

@Entity
@Table(name="g_cohort")
class CohortEntity implements Serializable {

    @Id
    @GeneratedValue
    @Column(name="cohort_gid")
    Long gid

    @Column(name="cohort_cd", nullable=false, unique = true)
    String cohortCd

    @Column(name="cohort_type_cd")
    String typeCd

    @Column(name="cohort_label")
    String label

    @Column(name="cohort_dsc")
    String description

    @OneToMany(mappedBy = "cohort")
    List<PatientCohort> patients

    @Version
    Long version
}

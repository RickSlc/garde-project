package edu.utah.rehr.garde.db.model

import jakarta.persistence.*

import java.time.LocalDateTime

@Entity
@Table(name="g_import_log")
class ImportLog {

    @Id
    @GeneratedValue
    Long logId

    @Column(name="file_name", unique = true)
    String fileName

    @Column(name="created_dts")
    LocalDateTime fileCreatedDateTime

    @Column(name="imported_dts")
    LocalDateTime importDateTime

    @Column(name="status")
    String status

}

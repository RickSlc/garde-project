package edu.utah.rehr.garde.db.repository

import edu.utah.rehr.garde.db.model.RuleCodeView
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.CrossOrigin

@Repository
@RepositoryRestResource(collectionResourceRel = "ruleCodesView", path = "ruleCodesView")
@CrossOrigin(origins = "*")
interface RuleCodeViewRepository  extends JpaRepository<RuleCodeView, Long> {

    List<RuleCodeView> findByServiceId(@Param("serviceId") String serviceId)

}

package edu.utah.rehr.garde.db.repository

import edu.utah.rehr.garde.db.model.CohortEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.CrossOrigin

@Repository
@RepositoryRestResource(collectionResourceRel = "cohorts", path = "cohorts")
@CrossOrigin(origins = "*")
interface CohortRepository extends JpaRepository<CohortEntity, Long> {
    Optional<CohortEntity> findOneByCohortCd(String cohortCd)
}

package edu.utah.rehr.garde.db.model

import jakarta.persistence.*

@Entity
@Table(name="g_pat_met_rule_criteria")
class PatientMetRuleCriteria {

    @Id
    @Column(name="pat_met_rule_criteria_gid")
    Long gid

    @ManyToOne
    @JoinColumn(name = "ehr_pat_id", referencedColumnName = "ehr_pat_id")
    PatientEntity patient

    @ManyToOne
    @JoinColumn(name = "rule_cd", referencedColumnName = "rule_cd")
    Rule rule

    @ManyToMany
    @JoinTable(
            name = "g_met_rule_criteria_fhx",
            joinColumns = @JoinColumn(
                    name = "pat_met_rule_criteria_gid", referencedColumnName = "pat_met_rule_criteria_gid"),
            inverseJoinColumns = @JoinColumn(
                    name = "pat_fhx_hx_gid", referencedColumnName = "pat_fhx_gid"))
    Set<PatientFamilyHx> patientFamilyHxList

    @Column(name="batch_id")
    Long batchId

    @Version
    Long version

}

package edu.utah.rehr.garde.db.model

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode
class PatientCohortId implements Serializable {
    String patientId
    String cohortCd
}

package edu.utah.rehr.garde.db.provider

import edu.utah.rehr.garde.db.repository.ProcessBatchRepository
import jakarta.transaction.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import java.time.LocalDate

@Component
class RuntimeVersionProvider {

    @Autowired
    ProcessBatchRepository runtimeVersionRepository

    @Transactional
    Integer nextVersion() {

        if (runtimeVersionRepository.findAll().size() == 0) {
            edu.utah.rehr.garde.db.model.ProcessBatch runtimeVersion = new edu.utah.rehr.garde.db.model.ProcessBatch(
                    runVersion: 0
                    , batchDate: LocalDate.now()
                    , purposeCode: "APP BOOTSTRAP"
                    , description: "Load app data."
            )
            runtimeVersionRepository.save(runtimeVersion)
        }
        return runtimeVersionRepository.nextVersion()
    }

    @Transactional
    Integer getNextVersion(String purposeCode, String description) {

        Integer version = nextVersion()
        edu.utah.rehr.garde.db.model.ProcessBatch runtimeVersion = new edu.utah.rehr.garde.db.model.ProcessBatch(
                runVersion: version
                , batchDate: LocalDate.now()
                , purposeCode: purposeCode
                , description: description
        )
        runtimeVersionRepository.save(runtimeVersion)

        return version
    }

    @Transactional
    LocalDate getVersionLocalDate(Integer version) {
        Optional<edu.utah.rehr.garde.db.model.ProcessBatch> optionalRuntimeVersion = runtimeVersionRepository.findById(version)
        edu.utah.rehr.garde.db.model.ProcessBatch runtimeVersion = optionalRuntimeVersion.get()
        return runtimeVersion.batchDate
    }


}

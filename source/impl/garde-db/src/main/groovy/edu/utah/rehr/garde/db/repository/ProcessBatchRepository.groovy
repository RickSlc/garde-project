package edu.utah.rehr.garde.db.repository

import edu.utah.rehr.garde.db.model.ProcessBatch
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface ProcessBatchRepository extends JpaRepository<ProcessBatch, Integer> {

    @Query(value="SELECT max(rv.id) FROM ProcessBatch rv")
    Integer maxVersion()

}

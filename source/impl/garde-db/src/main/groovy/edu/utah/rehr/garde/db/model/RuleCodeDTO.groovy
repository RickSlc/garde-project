package edu.utah.rehr.garde.db.model

class RuleCodeDTO {

    String ruleCodeSystem
    String ruleCode
    String codeSystem
    String code
    Integer autoExpand

}

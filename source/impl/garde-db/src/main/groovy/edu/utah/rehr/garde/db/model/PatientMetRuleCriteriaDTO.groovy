package edu.utah.rehr.garde.db.model

import edu.utah.rehr.garde.domain.core.model.GCohort
import edu.utah.rehr.garde.domain.core.model.dto.RuleDTO

class PatientMetRuleCriteriaDTO {

    String patientId
    GCohort cohort
    Set<RuleDTO> metRuleCriteria

}

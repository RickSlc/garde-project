package edu.utah.rehr.garde.db.repository

import edu.utah.rehr.garde.db.model.RuleCode
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.CrossOrigin

@Repository
@RepositoryRestResource(collectionResourceRel = "ruleCodes", path = "ruleCodes")
@CrossOrigin(origins = "*")
interface RuleCodeRepository  extends JpaRepository<RuleCode, Long> {

    List<RuleCode> findByRule_ruleCd(@Param("ruleCode") String ruleCode)
}

package edu.utah.rehr.garde.db.repository

import edu.utah.rehr.garde.db.model.Rule
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.CrossOrigin

@Repository
@RepositoryRestResource(collectionResourceRel = "rules", path = "rules")
@CrossOrigin(origins = "*")
interface RuleRepository extends JpaRepository<Rule, Long> {

    Optional<Rule> findOneByRuleCd(String ruleCd)

    @Query(value="SELECT r FROM Rule r where r.ruleCd not like '%_2023'")
    List<Rule> find2019Rules()

    @Query(value="SELECT r FROM Rule r where r.ruleCd like '%_2023'")
    List<Rule> find2023Rules()

}

package edu.utah.rehr.garde.db.model

import java.time.LocalDateTime

class EvaluatorInstanceDTO {

    String evaluatorId
    String instanceId
    String outputType
    LocalDateTime startTime
    LocalDateTime completionTime
    int populationSize
    int peopleMetCriteria
    String resultsFile
    List<EvaluatorInstanceStateDTO> states

}

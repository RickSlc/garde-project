package edu.utah.rehr.garde.db.model

import jakarta.persistence.*

@Entity
@Table(
        name="g_pat_cohort",
        uniqueConstraints=@UniqueConstraint(columnNames = ["ehr_pat_id","cohort_cd"])
)
class PatientCohort {

    @Id
    @GeneratedValue
    @Column(name="patient_cohort_gid")
    Long gid

    @Column(name="ehr_pat_id", nullable = false)
    String patientId

    @Column(name="cohort_cd", nullable = false)
    String cohortCd

    @ManyToOne
    @JoinColumn(name = "ehr_pat_id", updatable = false, insertable = false, referencedColumnName = "ehr_pat_id")
    PatientEntity patient

    @ManyToOne
    @JoinColumn(name = "cohort_cd", updatable = false, insertable = false, referencedColumnName = "cohort_cd")
    CohortEntity cohort

    @Column(name="batch_id")
    Long batchId

    @Version
    Long version

}

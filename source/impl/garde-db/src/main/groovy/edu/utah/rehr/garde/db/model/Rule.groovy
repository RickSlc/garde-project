package edu.utah.rehr.garde.db.model

import groovy.transform.EqualsAndHashCode

import jakarta.persistence.*

@Entity
@Table(name="g_rule")
@EqualsAndHashCode(includeFields=true, includes=['gid'] )
class Rule implements Serializable {

    @Id
    @GeneratedValue
    @Column(name="rule_gid")
    Long gid

    @Column(name="rule_code_system", nullable = false)
    String ruleCodeSystem

    @Column(name="rule_cd", nullable = false, unique = true)
    String ruleCd

    @Column(name="badge_cd")
    String badgeCd

    @Column(name="rule_label", nullable = false, length = 1000)
    String ruleLabel

    @Version
    Long version

}

package edu.utah.rehr.garde.db.model

import groovy.util.logging.Log
import jakarta.persistence.*
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Log
@Entity
@Table(name="g_service_instance_state")
class EvaluatorInstanceState {

    static String NOT_STARTED = "NOT_STARTED"
    static String PROCESSING = "PROCESSING"
    static String FAILED = "FAILED"
    static String COMPLETED = "COMPLETED"

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "state_id", nullable = false)
    Integer stateId

    @Column(name = "instance_id", nullable = false)
    String instanceId

    @Column(name = "service_id", nullable = false)
    String serviceId

    @Column(name = "state_cd", nullable = false)
    String state

    @Column(name = "status_cd", nullable = false)
    String status

    @Column(name = "started_dts", nullable = false)
    LocalDateTime startTime

    @Column(name = "completed_dts")
    LocalDateTime completionTime

    @Column(name = "json_variables", length=4096)
    String jsonVariables

    @Column(name = "message")
    String message

    JSONObject getVariables() {
        if (jsonVariables) {
            JSONParser parser = new JSONParser()
            Object jsonObject = parser.parse(jsonVariables)
            return (JSONObject) jsonObject
        }
        return new JSONObject()
    }

    void setVariable(String varName, String varValue) {
        JSONObject jsonObject = getVariables()
        jsonObject.put(varName, varValue)
        this.jsonVariables = jsonObject.toString()
    }

    void mergeVariables(String newJasonVariables) {

        if (!newJasonVariables) { // no new vars, bye
            return
        }

        JSONParser parser = new JSONParser()

        if (!this.jsonVariables) { // no existing vars, no merge needed
            JSONObject jsonObject = (JSONObject) parser.parse(newJasonVariables)
            this.jsonVariables = jsonObject.toString()
            return
        }

        // merge
        JSONObject jsonObject1 = (JSONObject) parser.parse(jsonVariables)
        JSONObject jsonObject2 = (JSONObject) parser.parse(newJasonVariables)
        jsonObject1.putAll(jsonObject2)
        this.jsonVariables = jsonObject1.toString()
    }

    String getVariable(String varName) {
        JSONObject json = getVariables()
        if (json) {
            return (String)json.get(varName)
        }
        return null
    }

    EvaluatorInstanceState(){}

    EvaluatorInstanceState(EvaluatorInstanceState copy) {
        this.instanceId = copy.instanceId
        this.serviceId = copy.serviceId
        this.state = copy.state
        this.status = copy.status
        this.startTime = copy.startTime
        this.completionTime = copy.completionTime
        this.message = copy.message
        this.jsonVariables = copy.jsonVariables
    }

    EvaluatorInstanceStateDTO toServiceInstanceStateDTO() {
        EvaluatorInstanceStateDTO dto = new EvaluatorInstanceStateDTO(
                stateId: this.stateId
                , state: this.state
                , status: this.status
                , startTime: this.startTime
                , completionTime: this.completionTime
                , message: this.message
                , resultsFile: getVariable("resultsFile")
                , jsonVariables: this.jsonVariables
        )
        return dto
    }

    @Override
    String toString() {

        StringBuilder str = new StringBuilder()
        str.append("{\n \"stateId\": \"").append(stateId).append("\"\n")
        str.append(", \"instanceId\": \"").append(instanceId).append("\"\n")
        str.append(", \"serviceId\": \"").append(serviceId).append("\"\n")
        str.append(", \"state\": \"").append(state).append("\"\n")
        str.append(", \"status\": \"").append(status).append("\"\n")
        if (startTime != null)
        str.append(", \"startTime\": \"").append(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(startTime)).append("\"\n")
        if (completionTime != null)
        str.append(", \"completionTime\": \"").append(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(completionTime)).append("\"\n")
        str.append(", \"message\": \"").append(message).append("\"\n")
        if (jsonVariables) {
            str.append(", \"jsonVariables\": ").append(jsonVariables)
        } else {
            str.append(", \"jsonVariables\": {}")
        }
        str.append(", \"resultsFile\": \"").append(getVariable("resultsFile")).append("\"\n}")

        return str.toString()
    }
}

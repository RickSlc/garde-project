package edu.utah.rehr.garde.db.repository

import edu.utah.rehr.garde.db.model.ImportLog
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.CrossOrigin

@Repository
@RepositoryRestResource(collectionResourceRel = "importLogs", path = "importLogs")
@CrossOrigin(origins = "*")
interface ImportLogRepository extends JpaRepository<ImportLog, Long> {

    ImportLog findByFileName(String fileName)

}

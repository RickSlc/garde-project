package edu.utah.rehr.garde.db.repository

import edu.utah.rehr.garde.db.model.PatientCohort
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PatientCohortRepository  extends JpaRepository<PatientCohort, Long> {

    Optional<PatientCohort> findByPatientIdAndCohortCd(String patentId, String cohortCd)

}

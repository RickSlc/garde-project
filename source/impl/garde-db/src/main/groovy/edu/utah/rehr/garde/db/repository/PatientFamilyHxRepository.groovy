package edu.utah.rehr.garde.db.repository

import edu.utah.rehr.garde.db.model.PatientFamilyHx
import edu.utah.rehr.garde.db.model.PatientFamilyHxId
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PatientFamilyHxRepository extends JpaRepository<PatientFamilyHx, PatientFamilyHxId> {
}

package edu.utah.rehr.garde.db.model

import jakarta.persistence.*

import java.time.LocalDateTime

@Entity
@Table(
        name="g_pat_chat"
        , uniqueConstraints=@UniqueConstraint(columnNames = ["ehr_pat_id","chat_script_id"])
)
class PatientChatEntity {

    @Id
    @GeneratedValue
    @Column(name="PAT_CHAT_ID")
    Long gid

    @Column(name="EHR_PAT_ID", nullable = false)
    String ehrPatientId

    @ManyToOne
    @JoinColumn(name = "EHR_PAT_ID", updatable = false, insertable = false, referencedColumnName = "EHR_PAT_ID")
    PatientEntity patient

    @Column(name="CHAT_SCRIPT_ID", nullable = false)
    String chatScriptId

    @Column(name="CHAT_SCRIPT_URL", nullable = false)
    String chatServiceUrl

    @Column(name="CHAT_URL_CREATE_DTS")
    LocalDateTime urlCreateDate

    @Column(name="CHAT_COMPLETED_DTS")
    LocalDateTime chatCompleteDate

    @Column(name="CHAT_RECEIVED_DTS")
    LocalDateTime chatReceivedDate

    @Column(name="CHAT_LOAD_DTS")
    LocalDateTime chatLoadDate

    @Column(name="CHAT_TRANSCRIPT_ID")
    Integer transcriptId

    @Lob
    @Column(name="CHAT_TRANSCRIPT_TXT", length=1000000) // 1 MB max
    String transcriptText

    @Lob
    @Column(name="CHAT_TRANSCRIPT_PDF", length=10000000) // 10 MB max
    byte[] transcriptPdf

    @Column(name="FILENAME")
    String fileName

}

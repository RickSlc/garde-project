package edu.utah.rehr.garde.db.model


import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table

import java.time.LocalDate

@Entity
@Table(name="g_batch")
class ProcessBatch {

    @Id
    @Column(name="batch_id")
    Long id

    @Column(name="batch_dts")
    LocalDate batchDate

    @Column(name="batch_purpose_cd")
    String purposeCode

    @Column(name="batch_desc")
    String description

}

package edu.utah.rehr.garde.db.loader

import edu.utah.rehr.garde.db.model.CohortEntity
import edu.utah.rehr.garde.db.model.EvaluatorEntity
import edu.utah.rehr.garde.db.model.Rule
import edu.utah.rehr.garde.db.model.RuleCodeDTO
import edu.utah.rehr.garde.domain.core.model.GCohort
import edu.utah.rehr.garde.domain.core.model.GEvaluator
import edu.utah.rehr.garde.domain.core.model.GlueRule
import edu.utah.rehr.garde.domain.core.model.Nccn2023Rule
import edu.utah.rehr.garde.domain.core.model.NccnRule
import edu.utah.rehr.garde.domain.core.utils.FileReaderUtil
import groovy.util.logging.Log
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVRecord

@Log
class StarterContentProvider {


    static Iterable<CSVRecord> getRecords(String[] header, String csvFilePath) {

        Reader reader = FileReaderUtil.getReader(csvFilePath)

        try {
            def csvFormat = CSVFormat.DEFAULT.builder()
                    .setHeader(header)
                    .setSkipHeaderRecord(true)
                    .setCommentMarker('#' as Character)
                    .build()

            def records = csvFormat.parse(reader)

            return records
        } catch (IOException e) {
            throw new RuntimeException(e)
        }

    }

    static List<RuleCodeDTO> getRuleCodes(String csvFilename) {
        String[] RULE_CODE_HEADER = ["RULE_CODE_SYSTEM","RULE_CODE","CODE_SET","CODE","AUTO_EXPAND"]
        List<RuleCodeDTO> ruleCodeList = new ArrayList<>()
        Iterable<CSVRecord> records = getRecords(RULE_CODE_HEADER, csvFilename)

        for (CSVRecord rec: records) {
            ruleCodeList.add(
                    new RuleCodeDTO(ruleCodeSystem: rec[0]
                            , ruleCode: rec[1]
                            , codeSystem: rec[2]
                            , code: rec[3]
                            , autoExpand: Integer.parseInt(rec[4]) ))
        }

        return ruleCodeList
    }

    static List<CohortEntity> getCohorts() {

        List<CohortEntity> cohorts = new ArrayList<>()

        for (GCohort coh: GCohort.values()) {

            CohortEntity cohort = new CohortEntity(
                    cohortCd: coh.code
                    , label: coh.label
                    , description: coh.description
            )

            cohorts.add(cohort)
        }

        return cohorts
    }

    static List<EvaluatorEntity> getEvaluators() {

        List<EvaluatorEntity> evaluators = new ArrayList<>()

        for (GEvaluator evaluator: GEvaluator.values()) {
            evaluators.add(new EvaluatorEntity(
                    serviceId: evaluator.serviceId
                    , title: evaluator.title
                    , guideline: evaluator.guideline
                    , codeSystem: evaluator.bindingCodeSystem
                )
            )
        }

        return evaluators
    }

    static List<Rule> getNccnRules() {

        List<Rule> rules = new ArrayList<>()

        for (NccnRule nccnRule: NccnRule.values()) {
            Rule rule = new Rule(
                    ruleCodeSystem: GEvaluator.GENETIC_TESTING_RECOMMENDER_R4.getBindingCodeSystem()
                    , ruleCd: nccnRule.code
                    , badgeCd: nccnRule.shortCode
                    , ruleLabel: nccnRule.label
            )
            rules.add(rule)
        }

        return rules
    }

    static List<Rule> getNccn2023Rules() {

        List<Rule> rules = new ArrayList<>()

        for (Nccn2023Rule nccnRule: Nccn2023Rule.values()) {
            Rule rule = new Rule(
                    ruleCodeSystem: GEvaluator.GENETIC_TESTING_RECOMMENDER_R4_2023.getBindingCodeSystem()
                    , ruleCd: nccnRule.code
                    , badgeCd: nccnRule.shortCode
                    , ruleLabel: nccnRule.label
            )
            rules.add(rule)
        }

        return rules
    }

    static List<Rule> getGlueRules() {

        List<Rule> rules = new ArrayList<>()

        for (GlueRule gRule: GlueRule.values()) {
            Rule rule = new Rule(
                    ruleCodeSystem: GEvaluator.GENETIC_TESTING_RECOMMENDER_R4_2023.getBindingCodeSystem()
                    , ruleCd: gRule.code
                    , badgeCd: gRule.shortCode
                    , ruleLabel: gRule.label
            )
            rules.add(rule)
        }

        return rules
    }

}

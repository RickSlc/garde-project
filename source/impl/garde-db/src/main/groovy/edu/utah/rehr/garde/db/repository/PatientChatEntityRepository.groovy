package edu.utah.rehr.garde.db.repository

import edu.utah.rehr.garde.db.model.PatientChatEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

import java.time.LocalDateTime

@Repository
interface PatientChatEntityRepository extends JpaRepository<PatientChatEntity, Long> {

    PatientChatEntity findByEhrPatientIdAndChatScriptId(String ehrPatientId, String chatScriptId)
    List<PatientChatEntity> findByChatLoadDateAfter(LocalDateTime loadDateTime);

}
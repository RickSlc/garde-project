package edu.utah.rehr.garde.db.model

class PatientFamilyHxId implements Serializable {

    private String patientId
    private Long encounterId
    private Long recordId

    PatientFamilyHxId() {}

    PatientFamilyHxId(String patientId, Long encounterId, Long recordId) {
        this.patientId = patientId
        this.encounterId = encounterId
        this.recordId = recordId
    }

    @Override
    boolean equals(Object o) {
        if (o == null || this.getClass() != o.getClass()) return false

        PatientFamilyHxId that = (PatientFamilyHxId) o

        return (
                Objects.equals(patientId,   that.patientId)
            &&  Objects.equals(encounterId, that.encounterId)
            &&  Objects.equals(recordId,    that.recordId)
        )
    }

    @Override
    int hashCode() {
        return Objects.hash(patientId, encounterId, recordId)
    }

}

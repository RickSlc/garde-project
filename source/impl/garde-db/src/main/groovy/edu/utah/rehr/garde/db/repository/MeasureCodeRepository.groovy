package edu.utah.rehr.garde.db.repository

import edu.utah.rehr.garde.terminology.db.model.TCode
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.CrossOrigin

@Repository
@RepositoryRestResource(collectionResourceRel = "measureCodes", path = "measureCodes")
@CrossOrigin(origins = "*")
interface MeasureCodeRepository  extends JpaRepository<TCode, Long> {

    @Query(value="select distinct cd from TCode cd where cd.isBindingCode=1 and cd.code in (select cs.codeSetCd from RuleCodeExpandView cs where cs.serviceId = :serviceId and cs.codeSetCd is not null)")
    List<TCode> findEvaluatorCodeSetsByServiceId(@Param("serviceId") String serviceId)

    @Query(value="select distinct cd from TCode cd where cd.gid in (select rce.codeId from RuleCodeExpandView rce where rce.serviceId = :serviceId and rce.isCodeSet = 0)")
    List<TCode> findEvaluatorCodesByServiceId(@Param("serviceId") String serviceId)

    @Query(value="select distinct cd from TCode cd where cd.gid in (select rcv.codeId from RuleCodeExpandView rcv join TCodeAssociationFlat assoc on assoc.lsCodeId = rcv.codeId and assoc.associationCd = 'EHR_CODE_IS' where rcv.serviceId = :serviceId)")
    List<TCode> findEvaluatorGardeCodesMappedToEhrCodesByServiceId(@Param("serviceId") String serviceId)

    @Query(value="select distinct cd from TCode cd where cd.gid in (select assoc.rsCodeId from RuleCodeExpandView rcv join TCodeAssociationFlat assoc on assoc.lsCodeId = rcv.codeId and assoc.associationCd = 'EHR_CODE_IS' where rcv.serviceId = :serviceId)")
    List<TCode> findEvaluatorEhrCodesByServiceId(@Param("serviceId") String serviceId)

    @Query(value="select distinct cd from TCode cd where cd.gid in (select rcv.codeId from RuleCodeExpandView rcv left join TCodeAssociationFlat assoc on assoc.lsCodeId = rcv.codeId and assoc.associationCd = 'EHR_CODE_IS' where rcv.serviceId = :serviceId and rcv.isCodeSet = 0 and assoc.lsCodeId is null)")
    List<TCode> findEvaluatorCodesNotMappedToEhrCodesByServiceId(@Param("serviceId") String serviceId)

    @Query(value="select distinct cd from TCode cd where cd.gid in (select rcv.codeId from RuleCodeExpandView rcv join TCodeAssociationFlat assoc on assoc.lsCodeId = rcv.codeId and assoc.associationCd = 'STANDARD_CODE_IS' and (assoc.rsIsBindingCode = '0' or assoc.rsIsBindingCode is null) where rcv.serviceId = :serviceId)")
    List<TCode> findEvaluatorGardeCodesMappedToStandardCodesByServiceId(@Param("serviceId") String serviceId)

    @Query(value="select distinct cd from TCode cd where cd.gid in (select assoc.rsCodeId from RuleCodeExpandView rcv join TCodeAssociationFlat assoc on assoc.lsCodeId = rcv.codeId and assoc.associationCd = 'STANDARD_CODE_IS' and (assoc.rsIsBindingCode = '0' or assoc.rsIsBindingCode is null) where rcv.serviceId = :serviceId)")
    List<TCode> findEvaluatorStandardCodesByServiceId(@Param("serviceId") String serviceId)

    @Query(value="select distinct cd from TCode cd where cd.gid in (select rcv.codeId from RuleCodeExpandView rcv left join TCodeAssociationFlat assoc2 on assoc2.lsCodeId = rcv.codeId and assoc2.associationCd = 'STANDARD_CODE_IS' and assoc2.rsCodeSystem not like 'http://utah.edu/rehr%' where rcv.serviceId = :serviceId and rcv.isCodeSet = 0 and assoc2.lsCodeId is null)")
    List<TCode> findEvaluatorCodesNotMappedToStandardCodesByServiceId(@Param("serviceId") String serviceId)

    @Query(value="select distinct cd from TCode cd where cd.gid in (select rcv.codeId from RuleCodeExpandView rcv left join TCodeAssociationFlat assoc on assoc.lsCodeId = rcv.codeId and assoc.associationCd = 'STANDARD_CODE_IS' left join TCodeAssociationFlat assoc2 on assoc2.lsCodeId = rcv.codeId and assoc2.associationCd = 'EHR_CODE_IS' where rcv.serviceId = :serviceId and rcv.isCodeSet = 0 and assoc.lsCodeId is null and assoc2.lsCodeId is null)")
    List<TCode> findEvaluatorUnmappedCodesByServiceId(@Param("serviceId") String serviceId)

}

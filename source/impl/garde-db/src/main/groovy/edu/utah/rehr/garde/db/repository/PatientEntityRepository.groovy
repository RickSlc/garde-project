package edu.utah.rehr.garde.db.repository

import edu.utah.rehr.garde.db.model.PatientEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PatientEntityRepository extends JpaRepository<PatientEntity, String> {
}

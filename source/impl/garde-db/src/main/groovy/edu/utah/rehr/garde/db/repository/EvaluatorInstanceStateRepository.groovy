package edu.utah.rehr.garde.db.repository

import edu.utah.rehr.garde.db.model.EvaluatorInstanceState
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.CrossOrigin

import java.time.LocalDateTime

@Repository
@RepositoryRestResource(collectionResourceRel = "states", path = "states")
@CrossOrigin(origins = "*")
interface EvaluatorInstanceStateRepository extends JpaRepository<EvaluatorInstanceState, Integer> {

    List<EvaluatorInstanceState> findByServiceIdOrderByStateId(@Param("serviceId") String serviceId)
    List<EvaluatorInstanceState> findByInstanceIdOrderByStateId(@Param("instanceId") String instanceId)

    @Query("SELECT e from EvaluatorInstanceState e where e.startTime >= :startTime")
    List<EvaluatorInstanceState> findAllWithStartTimeAfter(@Param("startTime") LocalDateTime startTime)

    @Query("SELECT e from EvaluatorInstanceState e where e.startTime >= :startTime and e.serviceId = :serviceId order by e.instanceId, e.stateId")
    List<EvaluatorInstanceState> findAllForServiceWithStartTimeAfter(@Param("serviceId") String serviceId, @Param("startTime") LocalDateTime startTime)

    Optional<EvaluatorInstanceState> findOneByInstanceIdAndStateAndCompletionTimeIsNull(String instanceId, String state)

    Optional<EvaluatorInstanceState> findOneByInstanceIdAndStateAndStatus(String instanceId, String state, String status)

    @Query("SELECT es FROM EvaluatorInstanceState es WHERE es.stateId = (SELECT max(ee.stateId) FROM EvaluatorInstanceState ee WHERE ee.instanceId = :instanceId)")
    Optional<EvaluatorInstanceState> findLastInstanceState(@Param("instanceId") String instanceId)

}

package edu.utah.rehr.garde.db

import edu.utah.rehr.garde.db.loader.ContentRepositoryLoader
import edu.utah.rehr.garde.db.repository.*
import edu.utah.rehr.garde.terminology.db.repository.TCodeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class GardeDbConfig {

    @Autowired
    CohortRepository cohortRepository

    @Autowired
    RuleRepository ruleRepository

    @Autowired
    EvaluatorRuleRepository cohortRuleRepository

    @Autowired
    RuleCodeRepository ruleCodeRepository

    @Autowired
    TCodeRepository codeRepository

    @Autowired
    EvaluatorEntityRepository evaluatorEntityRepository

    @Autowired
    EvaluatorFhirResourceRepository evaluatorFhirResourceRepository

    @Bean
    ContentRepositoryLoader contentRepositoryLoader() {
        ContentRepositoryLoader contentRepositoryLoader = new ContentRepositoryLoader(
                cohortRepository
                , ruleRepository
                , cohortRuleRepository
                , ruleCodeRepository
                , codeRepository
                , evaluatorEntityRepository
                , evaluatorFhirResourceRepository
        )
        contentRepositoryLoader.loadAll()

        return contentRepositoryLoader
    }

}

package edu.utah.rehr.garde.db.model

import edu.utah.rehr.garde.terminology.db.model.TCode
import edu.utah.rehr.garde.terminology.db.model.TUriBinding
import jakarta.persistence.*
import java.time.LocalDate

@Entity
@Table(name="g_pat")
class PatientEntity {

    @Id
    @Column(name="ehr_pat_id", nullable = false)
    String patientId

    @ManyToOne
    @JoinColumn(name = "pat_id_code_system_cd", referencedColumnName = "uri_binding_cd")
    TUriBinding patientIdCodeSetUriBinding

    @ManyToOne
    @JoinColumn(name = "race_code_gid", referencedColumnName = "code_gid")
    TCode race

    @ManyToOne
    @JoinColumn(name = "ethnicity_code_gid", referencedColumnName = "code_gid")
    TCode ethnicity

    @Column(name="birth_date")
    LocalDate dateOfBirth

    @ManyToOne
    @JoinColumn(name = "sex_code_gid", referencedColumnName = "code_gid")
    TCode sexCode

    @ManyToOne
    @JoinColumn(name = "religion_code_gid", referencedColumnName = "code_gid")
    TCode religionCode

    @OneToMany(mappedBy = "patient", cascade=CascadeType.ALL)
    List<PatientCohort> cohorts

    @Column(name="batch_id")
    Long batchId

    @Version
    Long version

}

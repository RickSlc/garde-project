package edu.utah.rehr.garde.db.model

import jakarta.persistence.*

@Entity
@Table(name="g_rule_code_v", uniqueConstraints = [ @UniqueConstraint(columnNames = [ "rule_gid", "code_gid" ]) ])
class RuleCodeView {

    @Id
    @Column(name="rule_code_view_gcd")
    String ruleCodeViewCd

    @Column(name="evaluator_gid")
    Long evaluatorId

    @Column(name="service_id")
    String serviceId

    @Column(name="rule_gid")
    Long ruleId

    @Column(name="rule_cd")
    String ruleCd

    @Column(name="rule_label")
    String ruleLabel

    @Column(name="code_set")
    String codeSetCd

    @Column(name="auto_expand")
    Long autoExpand

    @Column(name="code_system")
    String codeSystem

    @Column(name="code_gid")
    String codeId

    @Column(name="code")
    String code

    @Column(name="code_label")
    String codeLabel

}

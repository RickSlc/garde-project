package edu.utah.rehr.garde.db.model

import jakarta.persistence.*

@Entity
@Table(name="g_evaluator")
class EvaluatorEntity implements Serializable {

    @Id
    @GeneratedValue
    @Column(name="evaluator_gid")
    Long gid

    @Column(name="service_id", nullable = false, unique = true)
    String serviceId

    @Column(name="title", nullable = false)
    String title

    @Column(name="guideline", nullable = false)
    String guideline

    @Column(name="code_system", nullable = false)
    String codeSystem

    @OneToMany(mappedBy = "evaluator", cascade = CascadeType.ALL, orphanRemoval = true)
    List<EvaluatorFhirResource> fhirResources

    void addFhirResource(EvaluatorFhirResource fhirResource) {
        fhirResources.add(fhirResource)
        fhirResource.evaluator = this
    }

    void removeFhirResource(EvaluatorFhirResource fhirResource) {
        fhirResources.remove(fhirResource)
        fhirResource.evaluator = null
    }

    EvaluatorDTO toEvaluatorDTO() {
        return new EvaluatorDTO(
                evaluatorId: serviceId
                , title: title
                , guideline: guideline
        )
    }

}

package edu.utah.rehr.garde.db.model

import edu.utah.rehr.garde.terminology.db.model.TCode
import jakarta.persistence.*

@Entity
@Table(name="g_rule_code", uniqueConstraints = [ @UniqueConstraint(columnNames = [ "rule_gid", "code_gid" ]) ])
class RuleCode {

    @Id
    @GeneratedValue
    @Column(name="rule_code_gid", nullable = false)
    Long ruleCodeId

    @ManyToOne
    @JoinColumn(name = "rule_gid", referencedColumnName = "rule_gid")
    Rule rule

    @ManyToOne
    @JoinColumn(name = "code_gid", referencedColumnName = "code_gid")
    TCode code

    @Column(name = "auto_expand", nullable = false)
    Integer autoExpand

}

package edu.utah.rehr.garde.db.repository

import edu.utah.rehr.garde.db.model.EvaluatorFhirResource
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface EvaluatorFhirResourceRepository extends JpaRepository<EvaluatorFhirResource, Long> {
}

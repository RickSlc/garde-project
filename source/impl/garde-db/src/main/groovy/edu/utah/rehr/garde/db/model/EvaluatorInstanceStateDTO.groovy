package edu.utah.rehr.garde.db.model

import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser

import java.time.LocalDateTime

class EvaluatorInstanceStateDTO {

    Long stateId
    String state
    String status
    LocalDateTime startTime
    LocalDateTime completionTime
    String message
    String resultsFile
    String jsonVariables

    JSONObject getVariables() {
        if (jsonVariables) {
            JSONParser parser = new JSONParser()
            Object jsonObject = parser.parse(jsonVariables)
            return (JSONObject) jsonObject
        }
        return new JSONObject()
    }

    String getVariableValue(String varName) {
        JSONObject json = getVariables()
        if (json) {
            return (String)json.get(varName)
        }
        return null
    }


}

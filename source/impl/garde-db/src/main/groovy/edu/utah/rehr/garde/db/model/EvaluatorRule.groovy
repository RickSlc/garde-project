package edu.utah.rehr.garde.db.model

import jakarta.persistence.*

@Entity
@Table(name="g_evaluator_rule", uniqueConstraints=@UniqueConstraint(columnNames = ["evaluator_gid","rule_gid"]))
class EvaluatorRule {

    @Id
    @GeneratedValue
    @Column(name="evaluator_rule_gid",nullable=false)
    Long gid

    @ManyToOne
    @JoinColumn(name = "evaluator_gid", referencedColumnName = "evaluator_gid")
    EvaluatorEntity evaluator

    @ManyToOne
    @JoinColumn(name = "rule_gid", referencedColumnName = "rule_gid")
    Rule rule

    @Version
    Long version

}

package edu.utah.rehr.garde.db.model

import edu.utah.rehr.garde.domain.core.model.dto.MeasureCategoryDTO

class EvaluatorDTO {

    String evaluatorId
    String title
    String guideline
    String codeSystem
    List<EvaluatorInstanceDTO> instances
    List<MeasureCategoryDTO> measureCategories

}

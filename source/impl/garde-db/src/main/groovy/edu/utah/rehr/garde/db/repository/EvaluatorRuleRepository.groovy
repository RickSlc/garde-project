package edu.utah.rehr.garde.db.repository

import edu.utah.rehr.garde.db.model.EvaluatorRule
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.CrossOrigin

@Repository
@RepositoryRestResource(collectionResourceRel = "evaluatorRules", path = "evaluatorRules")
@CrossOrigin(origins = "*")
interface EvaluatorRuleRepository extends JpaRepository<EvaluatorRule, Long> {
    List<EvaluatorRule> findByEvaluator_serviceId(String serviceId)
}

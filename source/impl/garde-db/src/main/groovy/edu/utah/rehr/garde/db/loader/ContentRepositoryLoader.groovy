package edu.utah.rehr.garde.db.loader

import edu.utah.rehr.garde.db.model.*
import edu.utah.rehr.garde.db.repository.*
import edu.utah.rehr.garde.domain.core.model.GEvaluator
import edu.utah.rehr.garde.domain.core.model.GFhirResource
import edu.utah.rehr.garde.terminology.db.model.TCode
import edu.utah.rehr.garde.terminology.db.repository.TCodeRepository
import groovy.util.logging.Log
import org.springframework.stereotype.Component

@Component
@Log
class ContentRepositoryLoader {

    CohortRepository cohortRepository
    RuleRepository ruleRepository
    EvaluatorRuleRepository evaluatorRuleRepository
    RuleCodeRepository ruleCodeRepository
    TCodeRepository codeRepository
    EvaluatorEntityRepository evaluatorEntityRepository
    EvaluatorFhirResourceRepository evaluatorFhirResourceRepository

    ContentRepositoryLoader(
            CohortRepository cohortRepository
            , RuleRepository ruleRepository
            , EvaluatorRuleRepository evaluatorRuleRepository
            , RuleCodeRepository ruleCodeRepository
            , TCodeRepository codeRepository
            , EvaluatorEntityRepository evaluatorEntityRepository
            , EvaluatorFhirResourceRepository evaluatorFhirResourceRepository
    ) {
        this.cohortRepository = cohortRepository
        this.ruleRepository = ruleRepository
        this.evaluatorRuleRepository = evaluatorRuleRepository
        this.ruleCodeRepository = ruleCodeRepository
        this.codeRepository = codeRepository
        this.evaluatorEntityRepository = evaluatorEntityRepository
        this.evaluatorFhirResourceRepository = evaluatorFhirResourceRepository
    }

    void loadAll() {

        if (!ruleRepository.findAll()) {
            log.info("NO RULES IN DB -> LOADING")
            List<Rule> rules = StarterContentProvider.getNccnRules()
            ruleRepository.saveAll(rules)
            List<Rule> rules2023 = StarterContentProvider.getNccn2023Rules()
            ruleRepository.saveAll(rules2023)
            List<Rule> glueRules = StarterContentProvider.getGlueRules()
            ruleRepository.saveAll(glueRules)
        }

        if (!cohortRepository.findAll()) {
            log.info("NO COHORTS IN DB -> LOADING")
            List<CohortEntity> cohorts = StarterContentProvider.getCohorts()
            cohortRepository.saveAll(cohorts)
        }

        if (!evaluatorEntityRepository.findAll()) {

            List<EvaluatorEntity> evaluators = StarterContentProvider.getEvaluators()
            evaluatorEntityRepository.saveAll(evaluators)

            for (EvaluatorEntity evaluator : evaluators) {

                List<Rule> rules = null

                if (evaluator.serviceId == GEvaluator.GENETIC_TESTING_RECOMMENDER_R4_2023.serviceId) {
                    rules = StarterContentProvider.getNccn2023Rules()
                } else if (evaluator.serviceId == GEvaluator.GENETIC_TESTING_RECOMMENDER_R4.serviceId) {
                    rules = StarterContentProvider.getNccnRules()
                } else if (evaluator.serviceId == GEvaluator.FH_GENETIC_TESTING_RECOMMENDER_R4_2018.serviceId) {
                    rules = StarterContentProvider.getGlueRules()
                }

                for (Rule rule : rules) { // get attached rules from unattached
                    Optional<Rule> oRule = ruleRepository.findOneByRuleCd(rule.ruleCd)
                    Rule attachedRule = oRule.get()
                    evaluatorRuleRepository.save(
                            new EvaluatorRule(
                                    evaluator: evaluator
                                    , rule: attachedRule
                            )
                    )
                }

                GEvaluator evaluatorEnum = GEvaluator.findByServiceId(evaluator.serviceId)

                for (GFhirResource fhirResource : evaluatorEnum.requiredFhirResources) {

                    evaluatorFhirResourceRepository.save(
                            new EvaluatorFhirResource(
                                    prefetchName: fhirResource.prefetchName
                                    , fhirResource: fhirResource.fhirResource
                                    , fhirQuery: fhirResource.fhirQuery
                                    , cardinality: fhirResource.cardinality
                                    , evaluator: evaluator
                            )
                    )


                }

            }
        }

        if (!ruleCodeRepository.findAll()) {
            log.info("NO RULE CODES IN DB -> LOADING")
            List<RuleCodeDTO> nccnRuleCodes = StarterContentProvider.getRuleCodes("classpath:/terminology/garde-rule-codes.csv")
            List<RuleCodeDTO> glueRuleCodes = StarterContentProvider.getRuleCodes("classpath:/terminology/glue-rule-codes.csv")

            List<RuleCodeDTO> ruleCodes = new ArrayList<>()
            ruleCodes.addAll(nccnRuleCodes)
            ruleCodes.addAll(glueRuleCodes)

            for (RuleCodeDTO ruleCodeDTO : ruleCodes) {

                Optional<Rule> oRule = ruleRepository.findOneByRuleCd(ruleCodeDTO.ruleCode)
                Optional<TCode> oCode = codeRepository.findByCodeSystemAndCode(ruleCodeDTO.codeSystem, ruleCodeDTO.code)

                log.info("LOADING RULE: " + ruleCodeDTO.ruleCodeSystem + "/" + ruleCodeDTO.ruleCode + " CODE: " + ruleCodeDTO.codeSystem + "/" + ruleCodeDTO.code)

                TCode code = null
                // Create the GARDE code if it doesn't exist already - implies it wasn't part of a code set
                if (oCode.isEmpty()) {
                    code = new TCode(
                            codeSystem: ruleCodeDTO.codeSystem
                            , code: ruleCodeDTO.code
                            , codeLabel: ruleCodeDTO.code
                    )
                    codeRepository.save(code)
                } else {
                    code = oCode.get()
                }

                if (oRule.isPresent()) { // rule should be loaded already (above)

                    RuleCode newRuleCode = new RuleCode(
                            rule: oRule.get()
                            , code: code
                            , autoExpand: ruleCodeDTO.autoExpand
                    )

                    ruleCodeRepository.save(newRuleCode)
                } else { // log error if not there
                    log.severe("FAILED -> RULE " + ruleCodeDTO.ruleCode + " NOT FOUND")
                }

            }
        }
    }
}

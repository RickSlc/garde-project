package edu.utah.rehr.garde.db.model

import edu.utah.rehr.garde.terminology.db.model.TCode
import jakarta.persistence.*

@Entity
@Table(name="g_pat_fhx",
        uniqueConstraints=@UniqueConstraint(columnNames = ["ehr_pat_id","ehr_enc_id", "ehr_rec_id"])
)
class PatientFamilyHx {

    @Id
    @GeneratedValue
    @Column(name="pat_fhx_gid")
    Long gid

    @Column(name="ehr_pat_id", nullable = false)
    String patientId

    @Column(name="ehr_enc_id", nullable = false)
    String encounterId

    @Column(name="ehr_rec_id", nullable = false)
    String recordId

    @ManyToOne
    @JoinColumn(name="cond_code_gid", referencedColumnName = "code_gid", nullable = false)
    TCode conditionCode

    @ManyToOne
    @JoinColumn(name="rel_code_gid", referencedColumnName = "code_gid", nullable = false)
    TCode relationCode

    @Column(name="cond_onset_age")
    Integer conditionOnsetAge

    @Column(name="fhx_comments")
    String comments

    @Column(name="batch_id")
    Long batchId

    @Version
    Long version

    String toEhrFHxRecordKey() {
        return patientId + "_" + encounterId + "_" + recordId
    }

}

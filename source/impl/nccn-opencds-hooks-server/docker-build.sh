#!/bin/bash

# stop running container
docker container stop bulkcdshooks

# remove the container anticpating rebuild
docker container rm bulkcdshooks

# Remove image of the container
docker image rm rehr/bulkcdshooks

# Rebuild the image of the container using Dockerfile
docker build -t rehr/bulkcdshooks .

#!/bin/bash
date

if [ -z "$1" ]
then
  echo "Env parameter required - test OR prod"
  exit
fi

if [ -z "$2" ]
then
  echo "AWS profile name required"
  exit
fi

export AWS_PROFILE=$2
AWS_ECR_TEST_SERVER=test-server-name-goes-here
AWS_ECR_SERVER=server-name-goes-here

if [ "$1" == "test" ]
then
# TEST AWS
  aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin "$AWS_ECR_TEST_SERVER"
  docker tag rehr/bulkcdshooks:latest "$AWS_ECR_TEST_SERVER"/bulkcdshooks:latest
  docker push "$AWS_ECR_TEST_SERVER"/bulkcdshooks:latest
fi

if [ "$1" == "prod" ]
then
  aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin "$AWS_ECR_SERVER"
  docker tag rehr/bulkcdshooks:latest "$AWS_ECR_SERVER"/bulkcdshooks:latest
  docker push "$AWS_ECR_SERVER"/bulkcdshooks:latest
fi

package edu.utah.rehr.cdshooks.server;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opencds.config.api.ConfigurationService;
import org.opencds.hooks.evaluation.service.HookEvaluation;
import org.opencds.hooks.lib.json.JsonUtil;
import org.opencds.hooks.model.request.CdsRequest;
import org.opencds.hooks.model.response.CdsResponse;
import org.opencds.hooks.model.util.DateUtil;
import org.opencds.hooks.services.util.Responses;
import org.opencds.hooks.services.util.ServicesBuilder;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Path("cds-services")
public class CdsHooksBulkService {
    private static final Log log = LogFactory.getLog(CdsHooksBulkService.class);
    private static final String EVAL_TIME = "eval-time";

    private final ConfigurationService configurationService;
    private final HookEvaluation hookEvaluation;
    private final JsonUtil jsonUtil;

    public CdsHooksBulkService(ConfigurationService configurationService, HookEvaluation hookEvaluation,
                           JsonUtil jsonUtil) {
        this.configurationService = configurationService;
        this.hookEvaluation = hookEvaluation;
        this.jsonUtil = jsonUtil;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response cdsServices(@Context UriInfo uriInfo) {
        // list CDS Services
        return Responses.ok(jsonUtil.toJson(ServicesBuilder.build(configurationService.getKnowledgeRepository()
                .getKnowledgeModuleService().getAll(km -> km.getCDSHook() != null))));
    }

    @OPTIONS
    @Produces(MediaType.APPLICATION_JSON)
    public Response cdsServicesOptions(@Context UriInfo uriInfo) {
        return Responses.ok();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{serviceId}")
    public Response invokeCdsService(@PathParam("serviceId") String serviceId, String payload, @Context UriInfo uriInfo,
                                     @Context HttpServletRequest req) {
        try {
            log.debug("CdsRequest Payload: " + payload);
            CdsRequest request = jsonUtil.fromJson(payload, CdsRequest.class);
            if (request == null) {
                return Responses.badRequest("no content provided");
            }
            log.debug("Base URI: " + uriInfo.getBaseUri());
            Date evalTime = null;
            if (req.getHeader(EVAL_TIME) != null) {
                try {
                    evalTime = DateUtil.iso8601StringToDate(req.getHeader(EVAL_TIME));
                } catch (Exception e) {
                    log.warn("Unable to parse eval-time header: " + req.getHeader(EVAL_TIME));
                }
            }
            // TODO validation?
            CdsResponse response = hookEvaluation.evaluate(serviceId, request, evalTime, uriInfo.getBaseUri());
            String responsePayload = jsonUtil.toJson(response);
            log.debug("CdsResponse Payload: " + responsePayload);
            return Responses.ok(responsePayload);
        } catch (Exception e) {
            e.printStackTrace();
            return Responses.internalServerError(e.getMessage());
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("bulk/{serviceId}")
    public Response invokeBulkCdsService(@PathParam("serviceId") String serviceId, String bulkPayload, @Context UriInfo uriInfo,
                                     @Context HttpServletRequest req) {
        try {

            log.info("BULK REQUEST RECEIVED AND PROCESSING.");
            CdsRequest[] requests = jsonUtil.fromJson(bulkPayload, CdsRequest[].class);
            if (requests == null || requests.length == 0) {
                return Responses.badRequest("no content provided");
            }

            log.info("BULK REQUESTS RECEIVED: " + requests.length + " REQUESTS.");

            log.debug("Base URI: " + uriInfo.getBaseUri());
            Date evalTime = null;

            if (req.getHeader(EVAL_TIME) != null) {
                try {
                    evalTime = DateUtil.iso8601StringToDate(req.getHeader(EVAL_TIME));
                } catch (Exception e) {
                    log.warn("Unable to parse eval-time header: " + req.getHeader(EVAL_TIME));
                }
            }

            List<CdsResponse> cdsResponses = new ArrayList<>();
            for (CdsRequest request:requests) {
                CdsResponse cdsResponse = hookEvaluation.evaluate(serviceId, request, evalTime, uriInfo.getBaseUri());
                if (cdsResponse != null) {
                    cdsResponses.add(cdsResponse);
                } else {
                    log.debug("CDS REQUEST INVALID: " + request);
                }
            }

            CdsResponse[] cdsResponseArray = cdsResponses.toArray(new CdsResponse[cdsResponses.size()]);
            String responsePayload = jsonUtil.toJson(cdsResponseArray);

            return Responses.ok(responsePayload);

        } catch (Exception e) {
            e.printStackTrace();
            return Responses.internalServerError(e.getMessage());
        }
    }

    @OPTIONS
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{serviceId}")
    public Response invokeCdsServicesOptions(@Context UriInfo uriInfo) {
        return Responses.ok();
    }

}

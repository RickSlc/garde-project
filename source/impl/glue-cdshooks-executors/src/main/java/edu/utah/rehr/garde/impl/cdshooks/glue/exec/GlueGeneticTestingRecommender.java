package edu.utah.rehr.garde.impl.cdshooks.glue.exec;

import edu.utah.rehr.garde.domain.core.model.GFhirResource;
import edu.utah.rehr.garde.domain.core.model.GlueRule;
import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxDTO;
import edu.utah.rehr.garde.domain.core.model.dto.ObservationDTO;
import edu.utah.rehr.garde.domain.core.model.dto.PatientDTO;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import edu.utah.rehr.garde.impl.cds.glue.rules.GlueGeneticTestingRules;
import edu.utah.rehr.garde.impl.cdshooks.glue.helper.GlueCdsRequestHelper;
import edu.utah.rehr.garde.impl.cdshooks.glue.helper.GlueCdsResponseHelper;
import org.hl7.fhir.r4.model.Bundle;
import org.opencds.hooks.engine.api.CdsHooksEvaluationContext;
import org.opencds.hooks.engine.api.CdsHooksExecutionEngine;
import org.opencds.hooks.model.request.CdsRequest;
import org.opencds.hooks.model.response.CdsResponse;

import java.util.List;
import java.util.Map;

public class GlueGeneticTestingRecommender implements CdsHooksExecutionEngine {

    private final ICodeService codeService;
    private final GlueGeneticTestingRules glueGeneticTestingRules;

    public GlueGeneticTestingRecommender(ICodeService codeService) {
        this.codeService = codeService;
        this.glueGeneticTestingRules = new GlueGeneticTestingRules(codeService);
    }

    @Override
    public CdsResponse evaluate(CdsRequest cdsRequest, CdsHooksEvaluationContext cdsHooksEvaluationContext) {

        PatientDTO patientDTO = GlueCdsRequestHelper.getPatientDto(cdsRequest, GFhirResource.Patient.getPrefetchName());
        List<FamilyHxDTO> fhxList = GlueCdsRequestHelper.getPrefetchFHxDtoList(cdsRequest, GFhirResource.FamilyMemberHistory.getPrefetchName());
        List<ObservationDTO> obslist = GlueCdsRequestHelper.getPrefetchObservationDtoList(cdsRequest, GFhirResource.Observation_Ldl_C.getPrefetchName());

        Map<GlueRule, Bundle> results = glueGeneticTestingRules.evaluate(fhxList, obslist);
        CdsResponse cdsResponse = GlueCdsResponseHelper.assemble(patientDTO, results);

        return cdsResponse;
    }

}

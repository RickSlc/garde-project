package edu.utah.rehr.garde.impl.cdshooks.glue.helper;

import edu.utah.rehr.garde.domain.core.model.*;
import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxDTO;
import edu.utah.rehr.garde.domain.core.model.dto.PatientDTO;
import org.hl7.fhir.r4.model.*;
import org.opencds.hooks.model.response.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GlueCdsResponseHelper {

    public static CdsResponse assemble(PatientDTO patientDTO, Map<GlueRule, Bundle> results) {

        CdsResponse cdsResponse = new CdsResponse();
        Card responseCard = new Card();

        Source source = new Source();
        source.setLabel(GEvaluator.FH_GENETIC_TESTING_RECOMMENDER_R4_2018.getGuideline());
        responseCard.setSource(source);

        if (results.isEmpty()) { // Patient did NOT meet criteria

            responseCard.setSummary("Patient did NOT meet criteria for genetic testing based on provided LDL-C values and family history.");
            responseCard.setIndicator(Indicator.INFO);
            cdsResponse.addCard(responseCard);

            return cdsResponse; // done
        }

        // results > 0 implies met criteria
        responseCard.setSummary("Recommend genetic testing.");
        responseCard.setIndicator(Indicator.WARNING);

        Suggestion suggestion = new Suggestion();
        StringBuilder suggestionMessage = new StringBuilder();
        suggestionMessage.append("Recommend genetic testing. Met criteria: ");

        int i = 0;
        for (Map.Entry<GlueRule, Bundle> entry : results.entrySet()) {
            Action action = getAction(patientDTO.getPatientId(), entry);
            suggestion.addAction(action);
            if (i++ > 0) {
                suggestionMessage.append("; ");
            }
            suggestionMessage.append(entry.getKey().getLabel());
        }
        suggestionMessage.append(".");

        suggestion.setLabel(suggestionMessage.toString());
        responseCard.addSuggestion(suggestion);
        cdsResponse.addCard(responseCard);

        return cdsResponse;
    }

    private static Action getAction(String patientId, Map.Entry<GlueRule, Bundle> entry ) {
        Action action = new Action();
        action.setType(ActionType.CREATE);
        action.setResource(entry.getValue());
        action.setDescription(patientId + "," + entry.getKey().getCode() ); //+ ",\"" + entry.getKey().getLabel() + "\"");
        return action;
    }


}

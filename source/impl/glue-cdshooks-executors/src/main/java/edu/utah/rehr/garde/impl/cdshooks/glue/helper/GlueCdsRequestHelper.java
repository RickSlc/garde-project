package edu.utah.rehr.garde.impl.cdshooks.glue.helper;

import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxDTO;
import edu.utah.rehr.garde.domain.core.model.dto.ObservationDTO;
import edu.utah.rehr.garde.domain.core.model.dto.PatientDTO;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.GFhirToDtoMapperR4;
import lombok.extern.java.Log;
import org.hl7.fhir.r4.model.*;
import org.opencds.hooks.model.r4.request.prefetch.R4PrefetchHelper;
import org.opencds.hooks.model.request.CdsRequest;
import org.opencds.hooks.model.request.prefetch.PrefetchResult;

import java.util.ArrayList;
import java.util.List;

@Log
public class GlueCdsRequestHelper {

    private static final R4PrefetchHelper r4PrefetchHelper = new R4PrefetchHelper();

    public static PatientDTO getPatientDto(CdsRequest cdsRequest, String patientPrefetchKey) {
        PrefetchResult<OperationOutcome, Resource> prefetchResult = cdsRequest.getPrefetchResource(patientPrefetchKey, r4PrefetchHelper);
        Patient patient = prefetchResult.getResource(Patient.class);
        return GFhirToDtoMapperR4.patientToDto(patient);
    }

    public static List<FamilyHxDTO> getPrefetchFHxDtoList(CdsRequest cdsRequest, String fhxBundlePrefetchKey) {

        List<FamilyHxDTO> fhxList = new ArrayList<>();
        PrefetchResult<OperationOutcome, Resource> prefetchResult = cdsRequest.getPrefetchResource(fhxBundlePrefetchKey, r4PrefetchHelper);

        if (prefetchResult.hasResource()) {

            Bundle bundle = prefetchResult.getResource(Bundle.class);
            for (Bundle.BundleEntryComponent entry : bundle.getEntry()) {
                FamilyMemberHistory fhx = (FamilyMemberHistory) entry.getResource();
                fhxList.add(GFhirToDtoMapperR4.familyMemberHistoryToDto(fhx));
            }
        }

        return fhxList;
    }

    public static List<ObservationDTO> getPrefetchObservationDtoList(CdsRequest cdsRequest, String prefetchKey) {

        List<ObservationDTO> obsList = new ArrayList<>();
        PrefetchResult<OperationOutcome, Resource> prefetchResult = cdsRequest.getPrefetchResource(prefetchKey, r4PrefetchHelper);

        if (prefetchResult.hasResource()) {

            Bundle bundle = prefetchResult.getResource(Bundle.class);
            for (Bundle.BundleEntryComponent entry : bundle.getEntry()) {
                Observation obsFhir = (Observation) entry.getResource();
                obsList.add(GFhirToDtoMapperR4.observationToDto(obsFhir));
            }
        }

        return obsList;
    }


}

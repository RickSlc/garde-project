package edu.utah.rehr.garde.terminology.db


import groovy.util.logging.Log
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaAuditing
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.test.context.ActiveProfiles
import org.springframework.transaction.annotation.EnableTransactionManagement

@Configuration
@ComponentScan(basePackages = ["edu.utah.rehr.garde.terminology.impl.jpa"])
@EnableAutoConfiguration
@EnableTransactionManagement
@EnableJpaRepositories
@EnableJpaAuditing
@ActiveProfiles(profiles = "termtest")
@Log
class TestTerminologyConfig {

    @Bean
    edu.utah.rehr.garde.terminology.db.service.TerminologyExportCsv bootstrapTerminologyProvider() {
        return new edu.utah.rehr.garde.terminology.db.service.TerminologyExportCsv()
    }


//    @Autowired
//    HCodeBindingRepository hCodeBindingRepository
//
//    @Autowired
//    CodeBindingRepository codeBindingRepository
//
//    @Autowired
//    UriBindingRepository uriBindingRepository

//    @Bean("hierarchicalCodeFilePath")
//    String hierarchicalCodeFilePath () {
//        return "classpath:terminology/bindings/h-codes.csv"
//    }
//
//    @Bean("uriBindingsFilePath")
//    String uriBindingsFilePath () {
//        return "classpath:terminology/bindings/garde-uri-bindings-map.csv"
//    }

}

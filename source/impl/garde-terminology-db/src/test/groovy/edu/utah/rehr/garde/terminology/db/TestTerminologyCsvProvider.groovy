package edu.utah.rehr.garde.terminology.db

import groovy.util.logging.Log
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner

import static org.junit.Assert.assertTrue

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = [TestTerminologyConfig.class])
@Log
class TestTerminologyCsvProvider {

    @Autowired
    edu.utah.rehr.garde.terminology.db.repository.TCodeRepository codeRepository

    @Autowired
    edu.utah.rehr.garde.terminology.db.repository.TCodeAssociationRepository codeAssociationRepository

    @Autowired
    edu.utah.rehr.garde.terminology.db.repository.TCodeAssociationFlatRepository codeAssociationFlatRepository

    @Autowired
    edu.utah.rehr.garde.terminology.db.service.TerminologyExportCsv terminologyCsvProvider

    @Test
    void checkConfig() {
        println("Checking config")
        assertTrue(codeRepository.findAll().size() > 0)
        assertTrue(codeAssociationRepository.findAll().size() > 0)
        assertTrue(codeAssociationFlatRepository.findAll().size() > 0)
    }

}

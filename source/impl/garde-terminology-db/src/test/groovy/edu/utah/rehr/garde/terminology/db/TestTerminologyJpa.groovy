package edu.utah.rehr.garde.terminology.db

import edu.utah.rehr.garde.domain.core.terminology.Code
import edu.utah.rehr.garde.domain.core.terminology.ITerminologyKey
import edu.utah.rehr.garde.domain.core.terminology.TerminologyEntityType
import edu.utah.rehr.garde.domain.core.terminology.binding.NccnTerminologyBinding
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService
import groovy.util.logging.Log
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner

import static org.junit.Assert.assertNotNull
import static org.junit.Assert.assertTrue

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = [TestTerminologyConfig.class])
@Log
class TestTerminologyJpa {

    @Autowired
    ICodeService codeService

    @Test
    void testValueSetIntersection() {
        println("INTERSECTION TEST")
        Code code = codeService.getCanonicalCode(NccnTerminologyBinding.BREAST_CANCER_CODE)
        Set<Code> codes = new HashSet<>()
        codes.add(code)
        Boolean intersects = codeService.intersects(NccnTerminologyBinding.BREAST_CANCER_CODE_SET, codes)
        println("INTERSECTS: " + intersects)
    }

//    @Test
//    void testCodeServiceWithTestCodeKeys() {
//        log.info("Test all terminology in TestCodeKey")
//
//        for (ITerminologyKey binding : TestCodeKey.values()) {
//            log.info("TEST FOR BINDING " + binding.getEntityType().toString() + ":" + binding.getKey())
//
//            switch (binding.getEntityType()) {
//
//                case TerminologyEntityType.URI:
//
//                    String uri = codeService.getUri(binding)
//                    assertNotNull(uri)
//                    break
//                case TerminologyEntityType.CODE_SET:
//
//                    Set<Code> codeSet = codeService.getCanonicalCodeSet(binding)
//                    assertTrue(codeSet.size() > 0)
//
//                    Set<Code> ehrCodeSet = codeService.getEhrCodeSet(binding)
//                    assertTrue(ehrCodeSet.size() > 0)
//                    break
//                case TerminologyEntityType.CODE:
//
//                    Code code = codeService.getCanonicalCode(binding)
//                    assertNotNull(code)
//
//                    Set<Code> ehrCodeSet = codeService.getEhrCodeSet(binding)
//                    assertTrue(ehrCodeSet?.size() > 0)
//                    break
//
//                default:
//                    assertTrue("TerminologyEntityType " + binding.getEntityType() + " IS UNEXPECTED.", false)
//            }
//        }
//    }

    @Test
    void testCodeServiceWithAllTerminologyBindings() {
        log.info("Test Terminology in NccnTerminologyBinding.")

        for (ITerminologyKey binding: NccnTerminologyBinding.values()) {
            log.info("TEST FOR BINDING " + binding.getEntityType().toString() + ":" + binding.getKey())

            switch (binding.getEntityType()) {

                case TerminologyEntityType.URI:

                    String uri = codeService.getUri(binding)
                    assertNotNull(uri)
                    break
                case TerminologyEntityType.CODE_SET:

                    Set<Code> codeSet = codeService.getCanonicalCodeSet(binding)
                    assertTrue(codeSet.size() > 0)

                    Set<Code> ehrCodeSet = codeService.getEhrCodeSet(binding)
                    assertTrue(ehrCodeSet.size() > 0)
                    break
                case TerminologyEntityType.CODE:

                    Code code = codeService.getCanonicalCode(binding)
                    assertNotNull(code)

                    Set<Code> ehrCodeSet = codeService.getEhrCodeSet(binding)
                    assertTrue(ehrCodeSet?.size() > 0)
                    break

                default:
                    assertTrue("TerminologyEntityType " + binding.getEntityType() + " IS UNEXPECTED.",false)
            }
        }
    }

    @Test
    void ehrCodeHasCodeSet() {
        Set<Code> ehrCodeSet = codeService.getEhrCodeSet(NccnTerminologyBinding.BREAST_CANCER_CODE)
        assertTrue(ehrCodeSet?.size() > 0)
    }

}

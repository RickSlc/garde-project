package edu.utah.rehr.garde.terminology.db.repository

import edu.utah.rehr.garde.terminology.db.model.TCode
import edu.utah.rehr.garde.terminology.db.model.TCodeAssociationFlat
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.CrossOrigin

@Repository
@RepositoryRestResource(collectionResourceRel = "associations", path = "codeAssociationsFlat")
@CrossOrigin(origins = "*")
interface TCodeAssociationFlatRepository extends JpaRepository<TCodeAssociationFlat, Long> {

//    @Query(value="select a1.* from garde.g_code_association_v a1 where ls_code_system = 'http://utah.edu/rehr/nccn' and ls_code = ?1", nativeQuery = true)
    @Query(value="select a1 from TCodeAssociationFlat a1 where a1.lsCodeSystem = 'http://utah.edu/rehr/nccn' and a1.lsCode = ?1")
    List<TCodeAssociationFlat> findByCodeSetCode(@Param("codeSetCode") String codeSetCode)

    List<TCodeAssociationFlat> findByLsCodeSystemAndLsCode(@Param("lsCodeSystem") String lsCodeSystem, @Param("lsCode") String lsCode)

    List<TCodeAssociationFlat> findByAssociationCd(@Param("assoc") String association)

//    List<TCodeAssociationFlat> findByLsCodeSystemAndAssociationCd(@Param("lsCodeSystem") String lsCodeSystem, @Param("associationCd") String associationCd)

    List<TCodeAssociationFlat> findByLsCodeSystemAndLsCodeAndAssociationCd(@Param("lsCodeSystem") String lsCodeSystem
                                                                           , @Param("lsCode") String lsCode
                                                                           , @Param("assoc") String associationCd)

    List<TCodeAssociationFlat> findByLsCodeSystemAndAssociationCdAndRsCodeSystemOrderByLsCode(String lsCodeSystem, String association, String rsCodeSystem)

}

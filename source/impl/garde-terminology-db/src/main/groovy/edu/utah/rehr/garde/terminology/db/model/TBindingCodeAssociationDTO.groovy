package edu.utah.rehr.garde.terminology.db.model

class TBindingCodeAssociationDTO {

    Long codeAssociationId
    String bindingCode
    Long codeId
    String codeSystem
    String code
    String codeLabel
    String association

}

package edu.utah.rehr.garde.terminology.db.loader

import edu.utah.rehr.garde.domain.core.accessory.UResult
import edu.utah.rehr.garde.domain.core.model.GEvaluator
import edu.utah.rehr.garde.domain.core.terminology.CodeSetDefinition
import edu.utah.rehr.garde.domain.core.terminology.provider.csv.TerminologyReaderCsv
import edu.utah.rehr.garde.terminology.db.model.Association
import edu.utah.rehr.garde.terminology.db.model.TCode
import edu.utah.rehr.garde.terminology.db.model.TCodeAssociation
import edu.utah.rehr.garde.terminology.db.model.TUriBinding
import edu.utah.rehr.garde.terminology.db.repository.TCodeAssociationRepository
import edu.utah.rehr.garde.terminology.db.repository.TCodeRepository
import edu.utah.rehr.garde.terminology.db.repository.TUriBindingRepository
import edu.utah.rehr.garde.terminology.db.service.TerminologyExportCsv
import groovy.util.logging.Log
import org.apache.commons.lang3.StringUtils
import org.apache.commons.text.WordUtils

@Log
class TerminologyLoader {

    private String codeSetFilePath
    private String codeSetDefinitionFilePath
    private TCodeAssociationRepository codeAssociationRepository
    private TCodeRepository codeRepository
    private TUriBindingRepository uriBindingRepository

    TerminologyLoader(
            String codeSetFilePath
            , String codeSetDefinitionFilePath
            , TCodeAssociationRepository codeAssociationRepository
            , TCodeRepository codeRepository
            , TUriBindingRepository uriBindingRepository
    ) {
        this.codeSetFilePath = codeSetFilePath
        this.codeSetDefinitionFilePath = codeSetDefinitionFilePath
        this.codeAssociationRepository = codeAssociationRepository
        this.codeRepository = codeRepository
        this.uriBindingRepository = uriBindingRepository

        loadBase()
        loadUris()
    }

    void loadBase() {

        if (codeAssociationRepository.findAll()) {
            return
        }

        log.info("NO ASSOCIATIONS IN DB -> LOADING GARDE TERMINOLOGY BASE")
        List<String[]> codeSetPairs = TerminologyReaderCsv.getCodeSetRecords(this.codeSetFilePath)
        Map<String, CodeSetDefinition> codeSetDefinitionMap = TerminologyReaderCsv.getCodeSetDefinitionMap(this.codeSetDefinitionFilePath)

        for (String[] codeSetPair : codeSetPairs) {
            String codeSystem = codeSetPair[0]
            String codeSetBinding = codeSetPair[1]
            String codeBinding = codeSetPair[2]

            Optional<TCode> oCodeSetCode = codeRepository.findBindingCode(codeSetBinding)
            TCode codeSetCode = null
            if (oCodeSetCode.isEmpty()) {

                TCode newCode = new TCode(
                        codeSystem: codeSystem
                        , code: codeSetBinding
                        , codeLabel: codeSetDefinitionMap?.get(codeSetBinding)?.definition ?: "(not found)"
                        , isBindingCode: 1
                )
//                log.info("SAVE 6")
                codeSetCode = codeRepository.save(newCode)

            } else {
                codeSetCode = oCodeSetCode.get()
            }

            Optional<TCode> oCodeBinding = codeRepository.findBindingCode(codeBinding)

            TCode codeSetMember = null
            if (oCodeBinding.isEmpty()) {
                TCode newCode = new TCode(
                        codeSystem: codeSystem
                        , code: codeBinding
                        , codeLabel: WordUtils.capitalize(codeBinding.replace("_", " ").toLowerCase())
                        , isBindingCode: 1
                )
//                log.info("SAVE 7")
                codeSetMember = codeRepository.save(newCode)
            } else {
                codeSetMember = oCodeBinding.get()
            }

            Optional<TCodeAssociation> oAssociation = codeAssociationRepository.findOneByLsCode_gidAndRsCode_gidAndAssociationCd(
                    codeSetMember.gid, codeSetCode.gid, Association.HAS_MEMBER.toString()
            )

            if (oAssociation.isEmpty()) {
                TCodeAssociation newAssoc = new TCodeAssociation(
                        lsCode: codeSetCode
                        , rsCode: codeSetMember
                        , associationCd: Association.HAS_MEMBER.toString()
                )
                codeAssociationRepository.save(newAssoc)
            }

        }
    }

    void loadUris() {

        if (uriBindingRepository.findAll()) {
            return
        }

        log.info("NO URIs IN DB -> LOADING URIs")
        List<String[]> uriBindings = TerminologyReaderCsv.getUriBindings("classpath:/terminology/garde-uri-bindings-map.csv")

        for (String[] uriBinding : uriBindings) {
            TUriBinding newUriBinding = new TUriBinding(
                    uriBindingType: uriBinding[0]
                    , uriBinding: uriBinding[1]
                    , uri: uriBinding[2]
            )
            uriBindingRepository.save(newUriBinding)
        }

    }

    UResult<Boolean> loadBindingCodeAssociations(GEvaluator evaluator, Association association, String codeFilePath) {

        if (codeAssociationRepository.findByLsCode_codeSystemAndAssociationCd(evaluator.bindingCodeSystem, association.code).size() > 0) { // already loaded
            return UResult.successResult(true, "ALREADY LOADED")
        }

        log.info("NO CODE BINDING " + association.code + " ASSOCIATIONS IN DB -> LOADING CODES FROM " + codeFilePath)
        List<String[]> bindingCodeAssociationRecords = TerminologyReaderCsv.getBindingCodeAssociationRecords(codeFilePath)
        var lineErrors = new ArrayList<String[]>()
        lineErrors.add(Arrays.asList(TerminologyReaderCsv.CODE_BINDING_HEADER))

        for (String[] bindingCodeAssociationRecord : bindingCodeAssociationRecords) {

            String bindingCode = bindingCodeAssociationRecord[0]
            String codeSystem = bindingCodeAssociationRecord[1]
            String code = bindingCodeAssociationRecord[2]
            String codeLabel = bindingCodeAssociationRecord[3]

            if (StringUtils.isEmpty(bindingCode)
                    || StringUtils.isEmpty(codeSystem)
                    || StringUtils.isEmpty(code)
            ) {
                log.severe("MAPPING ERROR: ${bindingCodeAssociationRecord}")

                lineErrors.add(bindingCodeAssociationRecord)
                continue
            }

            Optional<TCode> oBindingCode = codeRepository.findBindingCode(bindingCode)

            TCode lsCode = null
            if (oBindingCode.isEmpty()) {
                TCode newCode = new TCode(
                        codeSystem: evaluator.bindingCodeSystem
                        , code: bindingCode
                        , codeLabel: WordUtils.capitalize(bindingCode.replace("_", " ").toLowerCase())
                        , isBindingCode: 1
                )
//                log.info("SAVE 2 " + bindingCode)
                lsCode = codeRepository.save(newCode)
            } else {
                lsCode = oBindingCode.get()
            }

            Optional<TCode> oCode = codeRepository.findByCodeSystemAndCode(codeSystem, code)

            TCode rsCode = null
            if (oCode.isEmpty()) {
                TCode newCode = new TCode(
                        codeSystem: codeSystem
                        , code: code
                        , codeLabel: codeLabel
                        , isBindingCode: isAssociatedBindingCode(association)
                )
//                log.info("SAVE 3")
                rsCode = codeRepository.save(newCode)
            } else {
                rsCode = oCode.get()
            }

            Optional<TCodeAssociation> oAssoc = codeAssociationRepository.findOneByLsCode_gidAndRsCode_gidAndAssociationCd(
                    lsCode.gid, rsCode.gid, association.toString()
            )

            if (oAssoc.isEmpty()) {
                TCodeAssociation newAssoc = new TCodeAssociation(
                        lsCode: lsCode
                        , rsCode: rsCode
                        , associationCd: association.toString()
                )
                codeAssociationRepository.save(newAssoc)
            }

        }

        if (lineErrors.size() > 1) {
            return exportErrors(lineErrors, codeFilePath, bindingCodeAssociationRecords)
        }

        return UResult.successResult(true)
    }

    UResult<Boolean> loadBindingCodeAssociationUpdates(Association association, String codeFilePath) {

        List<String[]> bindingCodeAssociations = TerminologyReaderCsv.getBindingCodeAssociationRecords(codeFilePath)
        var lineErrors = new ArrayList<String[]>()
        lineErrors.add(TerminologyReaderCsv.CODE_BINDING_HEADER)

        for (String[] bindingCodeAssociationRecord : bindingCodeAssociations) {

            String bindingCode = bindingCodeAssociationRecord[0]
            String codeSystem = bindingCodeAssociationRecord[1]
            String code = bindingCodeAssociationRecord[2]
            String codeLabel = bindingCodeAssociationRecord[3]

            if (StringUtils.isEmpty(bindingCode)
                    || StringUtils.isEmpty(codeSystem)
                    || StringUtils.isEmpty(code)
            ) {
                log.severe("BINDING CODE RECORD ERROR: ${bindingCodeAssociationRecord}")
                lineErrors.add(bindingCodeAssociationRecord)
                continue
            }

            // Find & validate code binding exists
            Optional<TCode> oBindingCode = codeRepository.findBindingCode(bindingCode)
            if (oBindingCode.isEmpty()) {
                log.severe("INVALID BINDING CODE ERROR: ${bindingCode}")
                lineErrors.add(bindingCodeAssociationRecord)
                continue
            }

            TCode bindingTCode = oBindingCode.get()

            // Now check for the code - may or may not be a binding code
            Optional<TCode> oAssociatedCode = codeRepository.findByCodeSystemAndCode(codeSystem, code)

            TCode associatedCode

            // if it exists, check to see if label has been updated
            if (oAssociatedCode.isPresent()) {
                associatedCode = oAssociatedCode.get()
                associatedCode.codeLabel = codeLabel
            } else { // if it does not exist, add it
                associatedCode = new TCode(
                        codeSystem: codeSystem
                        , code: code
                        , codeLabel: codeLabel
                        , isBindingCode: isAssociatedBindingCode(association)
                )
            }

//            log.info("SAVE 1")
            codeRepository.save(associatedCode)

            // check for an existing association
            Optional<TCodeAssociation> oExistingAssociation = codeAssociationRepository
                    .findOneByLsCode_gidAndRsCode_gidAndAssociationCd(
                            bindingTCode.gid
                            , associatedCode.gid
                            , association.toString()
                    )

            if (oExistingAssociation.isEmpty()) {
                TCodeAssociation newAssociation = new TCodeAssociation(
                        lsCode: bindingTCode
                        , rsCode: associatedCode
                        , associationCd: association.toString()
                )

                codeAssociationRepository.save(newAssociation)
            }
        }

        if (lineErrors.size() > 1) {
            return exportErrors(lineErrors, codeFilePath, bindingCodeAssociations)
        }

        return UResult.successResult(true)
    }

    static int isAssociatedBindingCode(Association association) {
        // When association is HAS_MEMBER the lsCode.isBindingCode = 1 and rsCode.isBindingCode = 1; ls = left side, rs = right side
        int rsIsBindingCode = 1
        // When association is EHR_CODE_IS the lsCode.isBindingCode = 1 and rsCode.isBindingCode = 0
        // When association is STANDARD_CODE_IS the lsCode.isBindingCode = 1 and rsCode.isBindingCode = 0
        if (Association.EHR_CODE_IS == association || Association.STANDARD_CODE_IS == association) {
            rsIsBindingCode = 0
        }
        rsIsBindingCode
    }

    private UResult<Boolean> exportErrors(List<String[]> lineErrors, String filePath, List<String[]> codeBindings) {

        String errorFilePath = filePath + '.ERR'
        TerminologyExportCsv.exportCodeBindings(lineErrors, filePath + '.ERR')
        log.info("ERRORS LOADING TERMINOLOGY - SEE ERROR LOG: ${errorFilePath}")

        return UResult.warningResult(true, "${lineErrors.size() - 1}/${codeBindings.size()} ERRORS (${codeBindings.size() - lineErrors.size() + 1} LOADED). Please review the entered file.")
    }

}

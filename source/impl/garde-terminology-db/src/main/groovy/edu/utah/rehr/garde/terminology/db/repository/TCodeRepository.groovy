package edu.utah.rehr.garde.terminology.db.repository

import edu.utah.rehr.garde.terminology.db.model.TCode
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.CrossOrigin

@Repository
@RepositoryRestResource(collectionResourceRel = "codes", path = "tcodes")
@CrossOrigin(origins = "*")
interface TCodeRepository extends JpaRepository<TCode,Long> {

    Optional<TCode> findByCodeSystemAndCode(String codeSystem, String code)

    @Query(value="select distinct t from TCode t where t.gid in (select rce.codeId from RuleCodeExpandView rce where rce.serviceId=:serviceId and rce.isCodeSet = 0)")
    List<TCode> findRuleCodesByServiceId(@Param("serviceId") String serviceId)

    @Query(value="select distinct t from TCode t where t.gid in (select rce.codeId from RuleCodeExpandView rce where rce.serviceId=:serviceId and rce.isCodeSet = 0 and rce.codeSystem =:bindingCodeSystem)")
    List<TCode> findRuleCodesByCodeSystemAndAssociationAndServiceId(@Param("bindingCodeSystem") String bindingCodeSystem, @Param("serviceId") String serviceId)

    @Query(value="select distinct t from TCode t where t.gid in (select a1.rsCodeId from TCodeAssociationFlat a1 where a1.lsCodeSystem = 'http://utah.edu/rehr/nccn' and a1.associationCd = :association) order by t.code")
    List<TCode> findByAssociation(@Param("association") String association)

    @Query(value="select a1 from TCode a1 where a1.code = :code and a1.isBindingCode = 1")
    Optional<TCode> findBindingCode(@Param("code") String code)

}

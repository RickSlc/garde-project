package edu.utah.rehr.garde.terminology.db.model

import jakarta.persistence.*

@Entity
@Table(name = "g_code_association", uniqueConstraints = [@UniqueConstraint(columnNames = ["ls_code_gid", "association_cd" ,"rs_code_gid"])])
class TCodeAssociation {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "code_association_gid", nullable = false) // columnDefinition = "Meaningless system-generated record identifier.")
    Long gid

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "ls_code_gid", nullable = false) // columnDefinition = "Left side of the association reading left to right. FK to code.")
    TCode lsCode

    @Column(name = "association_cd", nullable = false, length = 100) // columnDefinition = "The human-readable association code.")
    String associationCd

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "rs_code_gid", nullable = false) // columnDefinition = "Right side of the association reading left to right. FK to code.")
    TCode rsCode

    @Version
    Long version

}

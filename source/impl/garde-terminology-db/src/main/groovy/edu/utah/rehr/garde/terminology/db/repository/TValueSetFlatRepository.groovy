package edu.utah.rehr.garde.terminology.db.repository

import edu.utah.rehr.garde.terminology.db.model.TValueSetFlat
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.CrossOrigin

@Repository
@RepositoryRestResource(collectionResourceRel = "valueSets", path = "valueSets")
@CrossOrigin(origins = "*")
interface TValueSetFlatRepository extends JpaRepository<TValueSetFlat, String> {

    List<TValueSetFlat> findByVsCode(@Param("valueSetCode") String vsCode)

    List<TValueSetFlat> findAllByOrderByVsCodeAscMemberBindingCodeAsc()

//    @Query(value="select vs.* from garde.g_value_set_v vs where vs.vs_code in (select rc.code_set from garde.g_rule_code_expand_v rc where rc.service_id=:serviceId and rc.code_set is not null) order by vs_code, m_code", nativeQuery = true)
    @Query(value="select vs from TValueSetFlat vs where vs.vsCode in (select rc.codeSetCd from RuleCodeExpandView rc where rc.serviceId=:serviceId and rc.codeSetCd is not null) order by vs.vsCode, vs.memberBindingCode")
    List<TValueSetFlat> findValueSetsByServiceId(@Param("serviceId") String serviceId)

}

package edu.utah.rehr.garde.terminology.db.model

import jakarta.persistence.*

@Entity
@Table(name="G_CODE_URI_BINDING",
        uniqueConstraints=[@UniqueConstraint(columnNames = ["URI_TYPE_CD", "URI_BINDING_CD", "URI"])
])
class TUriBinding {

    final static String CODE_SYSTEM_URI_TYPE = "CODE_SYSTEM_URI"
    final static String FHIR_EXTENSION_URI_TYPE = "FHIR_EXTENSION_URI"
    final static String GUIDELINE_URI_TYPE = "GUIDELINE_URI"

    @Id
    @Column(name="URI_BINDING_CD")
    String uriBinding

    @Column(name="URI_TYPE_CD", nullable = false)
    String uriBindingType

    @Column(name="URI", nullable = false)
    String uri

    @Version
    Long version

}

package edu.utah.rehr.garde.terminology.db.model

class TAssociatedCode {

    Long codeAssociationGid
    String association
    TCode code

}

package edu.utah.rehr.garde.terminology.db

import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService
import edu.utah.rehr.garde.domain.core.terminology.provider.csv.TerminologyMapsProviderCsv
import edu.utah.rehr.garde.domain.core.terminology.service.CodeServiceByMaps
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment

@Configuration
@Profile(["terminology-in-memory"])
class TerminologyInMemoryConfig {

    @Autowired
    private Environment env

    @Bean
    ICodeService codeService() {

        StringBuilder ehrCodesFilePath = new StringBuilder()

        if (env.getProperty("garde.dataProvider.files.terminology.ehrMappings") != null
                && env.getProperty("garde.dataProvider.files.root.directory") != null
                && env.getProperty("garde.dataProvider.files.input.directory") != null
        ) {

            ehrCodesFilePath
                    .append(env.getProperty("garde.dataProvider.files.root.directory"))
                    .append(env.getProperty("garde.dataProvider.files.terminology.ehrMappings"))

        }

            return new CodeServiceByMaps(
                TerminologyMapsProviderCsv.getCodeSetMap("classpath:terminology/garde-code-sets.csv")
                , TerminologyMapsProviderCsv.getCodeBindingsMap("classpath:terminology/garde-standard-code-bindings.csv")
                , TerminologyMapsProviderCsv.getCodeBindingsMap(ehrCodesFilePath.toString())
                , TerminologyMapsProviderCsv.getUriBindingsMap("classpath:terminology/garde-uri-bindings-map.csv")
        )
    }

}

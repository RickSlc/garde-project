package edu.utah.rehr.garde.terminology.db.model

import edu.utah.rehr.garde.domain.core.terminology.Code
import jakarta.persistence.*

@Entity
@Table(name = "g_code",
        uniqueConstraints=[@UniqueConstraint(columnNames = ["code", "code_system"])
        ])
class TCode {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "code_gid", nullable = false) // columnDefinition = "Meaningless identifier for the record.")
    Long gid

    @Column(name = "code", nullable = false, length = 100) // columnDefinition = "The coded identifier assigned by the code system for the item described by the code.")
    String code

    @Column(name = "code_label", nullable = false, length = 500) // columnDefinition = "The human-readable word/s representing the meaning of the code - short.")
    String codeLabel

    @Column(name = "code_system", nullable = false, length = 500) // columnDefinition = "Tne URI of the code system where the code was defined.")
    String codeSystem

    @Column(name="is_binding_code", nullable = true)
    Integer isBindingCode = 0
    // 0 false, 1 true

    @Version
    Long version

    Code toCode() {
        return new Code(
                codeSystem
                ,code
                ,codeLabel
        )
    }

    @Override
    boolean equals(Object obj) {

        if (obj == null)
            return false
        if (getClass() != obj.getClass())
            return false

        TCode other = (TCode) obj
        return codeSystem == other.codeSystem && code == other.code
    }

    @Override
    int hashCode() {
        return codeSystem.hashCode() + code.hashCode()
    }
}

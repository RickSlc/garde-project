package edu.utah.rehr.garde.terminology.db.model

class CodeBindingDTO {

    String memberAssociation
    String codeBinding
    TCode code

    @Override
    boolean equals(Object obj) {

        if (obj == null)
            return false
        if (getClass() != obj.getClass())
            return false

        CodeBindingDTO other = (CodeBindingDTO) obj
        return codeBinding == other.codeBinding && code == other.code
    }

    @Override
    int hashCode() {
        return codeBinding.hashCode() + code.hashCode()
    }

}

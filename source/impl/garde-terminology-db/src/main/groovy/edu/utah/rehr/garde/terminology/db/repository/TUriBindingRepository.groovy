package edu.utah.rehr.garde.terminology.db.repository

import edu.utah.rehr.garde.terminology.db.model.TUriBinding
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.CrossOrigin

@Repository
@RepositoryRestResource(collectionResourceRel = "uris", path = "uri")
@CrossOrigin(origins = "*")
interface TUriBindingRepository extends JpaRepository<TUriBinding, String> {

}

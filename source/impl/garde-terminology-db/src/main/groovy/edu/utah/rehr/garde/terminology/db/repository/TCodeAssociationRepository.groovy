package edu.utah.rehr.garde.terminology.db.repository

import edu.utah.rehr.garde.terminology.db.model.TCodeAssociation
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.CrossOrigin

@Repository
@RepositoryRestResource(collectionResourceRel = "associations", path = "associations")
@CrossOrigin(origins = "*")
interface TCodeAssociationRepository extends JpaRepository<TCodeAssociation, Long> {

    /**
     * findOneByLsCode_gidAndRsCode_gidAndAssociationCd is used to check if an association exists already or not
     * @param lsCodeGid
     * @param rsCodeGid
     * @param association
     * @return
     */

    Optional<TCodeAssociation> findOneByLsCode_gidAndRsCode_gidAndAssociationCd(Long lsCodeGid, Long rsCodeGid, String association)

    /**
     * findByLsCode_codeSystemAndAssociationCd is useful for fetching
     * code binding codes by using codeSystem = 'http://utah.edu/rehr/nccn' and associatonCd = 'STANDARD_CODE_IS'
     * For each TCodeAssociation the lsCode is the code binding, the rsCode is the standard code. Correctness is based
     * on the content -> every code binding must have a standard code association.
     * Or, valueSet binding codes by using codeSystem = 'http://utah.edu/rehr/nccn' and associatonCd = 'HAS_MEMBER'
     * For each TCodeAssociation the lsCode is the valueSet binding code, the rsCode is the value set member binding code
     * @param codeSystem
     * @param association
     * @return
     */
    List<TCodeAssociation> findByLsCode_codeSystemAndAssociationCd(String codeSystem, String association)

    /**
     * findOneByLsCode_codeSystemAndLsCode_codeAndAssociationCd is useful for fetching a specific code by the binding code
     * The query result TCodeAssociation.lsCode is the binding code
     * @param codeSystem
     * @param code
     * @param association
     * @return
     */
    List<TCodeAssociation> findByAssociationCdAndRsCode_codeSystemAndRsCode_code(String association, String codeSystem, String code)

    List<TCodeAssociation> findByLsCode_codeSystemAndLsCode_codeAndAssociationCd(String codeSystem, String code, String association)

    List<TCodeAssociation> findByAssociationCd(String associationCd)

}

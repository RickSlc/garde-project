package edu.utah.rehr.garde.terminology.db

import edu.utah.rehr.garde.domain.core.model.GEvaluator
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService
import edu.utah.rehr.garde.domain.core.terminology.service.CodeServiceByMaps
import edu.utah.rehr.garde.domain.core.utils.ExceptionUtil
import edu.utah.rehr.garde.domain.core.utils.SqlProcessorUtil
import edu.utah.rehr.garde.terminology.db.loader.TerminologyLoader
import edu.utah.rehr.garde.terminology.db.model.Association
import edu.utah.rehr.garde.terminology.db.provider.CodeServiceJpaMapsProvider
import edu.utah.rehr.garde.terminology.db.repository.TCodeAssociationRepository
import edu.utah.rehr.garde.terminology.db.repository.TCodeRepository
import edu.utah.rehr.garde.terminology.db.repository.TUriBindingRepository
import edu.utah.rehr.garde.terminology.db.service.TerminologyExportCsv
import groovy.util.logging.Log
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment

@Configuration
@Log
class GardeTerminologyDbConfig {

    @Autowired
    private Environment env

    @Autowired
    private TCodeAssociationRepository codeAssociationRepository

    @Autowired
    private TCodeRepository codeRepository

    @Autowired
    private TUriBindingRepository uriBindingRepository

    @Autowired
    private CodeServiceJpaMapsProvider codeServiceJpaMapsProvider

    @Value('${spring.datasource.url}')
    private String dbUrl

    @Value('${garde.schema:garde}')
    private String gardeSchema

    @Bean
    TerminologyExportCsv bootstrapTerminologyProvider() {
        return new TerminologyExportCsv()
    }

    @Bean
    ICodeService codeService() {
        return codeServiceByMaps()
    }

    CodeServiceByMaps codeServiceByMaps() {

        terminologyLoader() // make sure terminology is loaded first

        var codeSetMapResult = codeServiceJpaMapsProvider.getCodeSetMap()

        if (!codeSetMapResult.success()) {
            log.warning("ERROR RETRIEVING CODE SETS FROM codeServiceJpaMapsProvider. " + codeSetMapResult.message)
        }

        var standardCodeBindingsMapResult = codeServiceJpaMapsProvider.getStandardCodeBindingsMap()

        if (!standardCodeBindingsMapResult.success()) {
            log.warning("ERROR RETRIEVING STANDARD CODES FROM codeServiceJpaMapsProvider. " + standardCodeBindingsMapResult.message)
        }

        var getEhrCodeBindingsMapResult = codeServiceJpaMapsProvider.getEhrCodeBindingsMap()

        if (!getEhrCodeBindingsMapResult.success()) {
            log.warning("ERROR RETRIEVING EHR CODES FOUND VIA codeServiceJpaMapsProvider. " + getEhrCodeBindingsMapResult.message)
        }

        var uriMapResult = codeServiceJpaMapsProvider.getUriBindingsMap()

        if (!uriMapResult.success()) {
            log.warning("ERROR RETRIEVING URIs FROM codeServiceJpaMapsProvider.getUriBindingsMap(). " + uriMapResult.message)
        }

        return new CodeServiceByMaps(
                codeSetMapResult.getResult()
                , standardCodeBindingsMapResult.getResult()
                , getEhrCodeBindingsMapResult.getResult()
                , uriMapResult.getResult()
        )
    }

    @Bean
    SqlProcessorUtil sqlProcessorUtil() {
        String jdbcUrl = env.getProperty("spring.datasource.url")
        String jdbcUsername = env.getProperty("spring.datasource.username")
        String jdbcPassword = env.getProperty("spring.datasource.password")
        return new SqlProcessorUtil(jdbcUrl, jdbcUsername, jdbcPassword)
    }

    void updateDbModel() {

        log.info("UPDATING DB MODEL CHANGING TABLES TO VIEWS.")
        SqlProcessorUtil processor = sqlProcessorUtil()

        String script = ""
        String schemaName = ""

        if (dbUrl.contains("jdbc:postgresql")) {
            script = processor.getSqlFromFile("classpath:db/schema-postgresql.sql")
        } else if (dbUrl.contains("jdbc:sqlserver")) {
            script = processor.getSqlFromFile("classpath:db/schema-sqlserver.sql")
            script = processor.setReplacementVariables(script,"schema",gardeSchema + ".")
        } else {
            log.severe("NO DB MODEL UPDATE SCRIPT FOR: " + dbUrl)
            return
        }

        log.info("UPDATING DB MODEL FOR: " + dbUrl)
        String[] sqls = processor.splitSQLStatements(script)

        for(String sql:sqls) {
            log.info(sql)
            try {
                processor.execute(sql)
            } catch (Exception e) {
                log.warning("DB MODEL UPDATE FAILED: " + ExceptionUtil.getMessageString(e,200))
            }
        }
    }

    @Bean
    TerminologyLoader terminologyLoader() {

        if (!sqlProcessorUtil().objectExists("g_code_association_v", "VIEW")
            || !sqlProcessorUtil().objectExists("g_rule_code_expand_v", "VIEW")
            || !sqlProcessorUtil().objectExists("g_rule_code_v", "VIEW")
            || !sqlProcessorUtil().objectExists("g_value_set_v", "VIEW")

        ) {
            log.severe("DB CONFIG INCOMPLETE - VIEWS NEED TO BE CREATED.")
            updateDbModel()
        }

        TerminologyLoader loader = new TerminologyLoader(
                "classpath:terminology/garde-code-sets.csv"
                ,"classpath:terminology/garde-code-set-definitions.csv"
                ,codeAssociationRepository
                ,codeRepository
                ,uriBindingRepository
        )

        loader.loadBindingCodeAssociations(
                GEvaluator.GENETIC_TESTING_RECOMMENDER_R4_2023
                , Association.STANDARD_CODE_IS
                , "classpath:terminology/garde-standard-code-bindings.csv"
        )

        loader.loadBindingCodeAssociations(
                GEvaluator.FH_GENETIC_TESTING_RECOMMENDER_R4_2018
                , Association.STANDARD_CODE_IS
                , "classpath:terminology/glue-standard-codes.csv"
        )

        loader.loadUris()

        return loader
    }
}

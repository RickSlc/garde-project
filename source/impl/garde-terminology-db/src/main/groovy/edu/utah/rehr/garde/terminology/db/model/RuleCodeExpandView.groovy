package edu.utah.rehr.garde.terminology.db.model

import jakarta.persistence.*

@Entity
@Table(name="g_rule_code_expand_v", uniqueConstraints = [ @UniqueConstraint(columnNames = [ "rule_gid", "code_gid" ]) ])
class RuleCodeExpandView {

    @Id
    @Column(name="rule_code_expand_view_gcd")
    String ruleCodeExpandViewCd

    @Column(name="evaluator_gid")
    Long evaluatorId

    @Column(name="service_id")
    String serviceId

    @Column(name="is_code_set")
    Integer isCodeSet

    @Column(name="rule_code_gid")
    Long ruleCodeId

    @Column(name="rule_gid")
    Long ruleId

    @Column(name="rule_cd")
    String ruleCd

    @Column(name="rule_label")
    String ruleLabel

    @Column(name="code_set")
    String codeSetCd

    @Column(name="auto_expand")
    Long autoExpand

    @Column(name="code_system")
    String codeSystem

    @Column(name="code_gid")
    Long codeId

    @Column(name="code")
    String code

    @Column(name="code_label")
    String codeLabel

    @Column(name="is_binding_code")
    Integer isBindingCode

}

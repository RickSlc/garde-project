package edu.utah.rehr.garde.terminology.db.model

class TBindingCodeMappings {

    TCode bindingCode
    List<TAssociatedCode> mappings

}

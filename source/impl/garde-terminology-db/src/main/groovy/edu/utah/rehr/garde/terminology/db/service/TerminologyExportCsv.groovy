package edu.utah.rehr.garde.terminology.db.service

import edu.utah.rehr.garde.domain.core.terminology.provider.csv.TerminologyReaderCsv
import edu.utah.rehr.garde.terminology.db.model.TCodeAssociationFlat
import groovy.util.logging.Log
import org.apache.commons.csv.CSVPrinter
import org.apache.commons.csv.CSVFormat

@Log
class TerminologyExportCsv {

    static Boolean exportCodeBindings(List<String[]> records, String filePath) {

        CSVPrinter printer = new CSVPrinter(new FileWriter(filePath), CSVFormat.DEFAULT)
        printer.printRecord(TerminologyReaderCsv.CODE_BINDING_HEADER)

        int i = 0 // skip header
        for (String[] record: records) {
            if (i++ > 0)
                printer.printRecord( record[0], record[1], record[2], record[3] )
        }
        printer.flush()
        printer.close()
        return true
    }

    static Boolean exportBindingCodeMappings(List<TCodeAssociationFlat>  associatedCodes, String filePath) {

        CSVPrinter printer = new CSVPrinter(new FileWriter(filePath), CSVFormat.DEFAULT)
        printer.printRecord(TerminologyReaderCsv.CODE_BINDING_HEADER)

        for (TCodeAssociationFlat assoc: associatedCodes) {
            printer.printRecord(assoc.lsCode, assoc.rsCodeSystem, assoc.rsCode, assoc.rsCodeLabel )
        }
        printer.flush()
        printer.close()
        return true
    }

    static Boolean exportCodeSets(List<TCodeAssociationFlat>  associations, String filePath) {

        CSVPrinter printer = new CSVPrinter(new FileWriter(filePath), CSVFormat.DEFAULT)
        printer.printRecord(TerminologyReaderCsv.CODE_SET_HEADER)

        for (TCodeAssociationFlat assoc: associations) {
            printer.printRecord(assoc.lsCodeSystem, assoc.lsCode, assoc.rsCode)
        }
        printer.flush()
        printer.close()
        return true
    }

}

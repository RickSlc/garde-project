package edu.utah.rehr.garde.terminology.db.model

import jakarta.persistence.*
import java.time.LocalDateTime

@Entity
@Table(name="g_terminology_import")
class TerminologyImport {

    @Id
    @GeneratedValue
    Long logId

    @Column(name="file_name", unique = true)
    String fileName

    @Column(name="stored_file_name", unique = true)
    String storedFileName

    @Column(name="error_file_name", unique = true)
    String errorFileName

    @Column(name="imported_dts")
    LocalDateTime importDateTime

    @Column(name="status")
    String status

}

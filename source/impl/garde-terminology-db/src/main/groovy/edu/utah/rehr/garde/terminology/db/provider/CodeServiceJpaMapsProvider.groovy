package edu.utah.rehr.garde.terminology.db.provider

import edu.utah.rehr.garde.domain.core.accessory.UResult
import edu.utah.rehr.garde.domain.core.terminology.Code
import edu.utah.rehr.garde.domain.core.terminology.binding.NccnTerminologyBinding
import edu.utah.rehr.garde.terminology.db.model.Association
import edu.utah.rehr.garde.terminology.db.model.TCodeAssociationFlat
import edu.utah.rehr.garde.terminology.db.model.TUriBinding
import edu.utah.rehr.garde.terminology.db.repository.TCodeAssociationFlatRepository
import edu.utah.rehr.garde.terminology.db.repository.TUriBindingRepository
import groovy.util.logging.Log
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
@Log
class CodeServiceJpaMapsProvider {

    @Autowired
    TCodeAssociationFlatRepository codeAssociationFlatRepository

    @Autowired
    TUriBindingRepository uriBindingRepository

    UResult<Map<String, Set<Code>>> getCodeSetMap() {

        Optional<TUriBinding> oUriBinding = uriBindingRepository.findById(NccnTerminologyBinding.REHR_NCCN_CODE_SYSTEM_URI.key)
        if (oUriBinding.isEmpty()) {
            return UResult.errorResult("URI " + NccnTerminologyBinding.REHR_NCCN_CODE_SYSTEM_URI.key + " MISSING IN URI REPO.")
        }

        String codeSetUri = oUriBinding.get().uri

        List<TCodeAssociationFlat> codeSetAssociations = codeAssociationFlatRepository.findByLsCodeSystemAndAssociationCdAndRsCodeSystemOrderByLsCode(
                codeSetUri
                , Association.HAS_MEMBER.toString()
                , codeSetUri
        )

        if (codeSetAssociations.isEmpty()) {
            return UResult.errorResult("NO CODE SET ASSOCIATIONS FOR " + codeSetUri)
        }

        Map<String, Set<Code>> resultMap = new HashMap<>()

        for (TCodeAssociationFlat codeAssoc: codeSetAssociations) {

            Set<Code> codes

            if (!resultMap.containsKey(codeAssoc.lsCode)) {
                codes = new HashSet<>()
            } else {
                codes = resultMap.get(codeAssoc.lsCode)
            }

            codes.add(new Code(
                    codeAssoc.rsCode
                    , codeAssoc.rsCodeSystem
                    , codeAssoc.rsCode
                    , codeAssoc.rsCodeLabel
            ))
            resultMap.put(codeAssoc.lsCode, codes)

        }

        return UResult.successResult(resultMap)
    }

    UResult<Map<String, Set<Code>>> getCodeBindingsMap(Association association) {
        List<TCodeAssociationFlat> associations = codeAssociationFlatRepository.findByAssociationCd(
                association.toString())

        if (associations.isEmpty()) {
            return UResult.errorResult("NO CODE ASSOCIATIONS FOR " + association.toString())
        }

        Map<String, Set<Code>> resultMap = new HashMap<>()

        for (TCodeAssociationFlat codeAssoc: associations) {

            Set<Code> codes

            if (!resultMap.containsKey(codeAssoc.lsCode)) {
                codes = new HashSet<>()
            } else {
                codes = resultMap.get(codeAssoc.lsCode)
            }

            codes.add(new Code(
                    codeAssoc.lsCode
                    , codeAssoc.rsCodeSystem
                    , codeAssoc.rsCode
                    , codeAssoc.rsCodeLabel
            ))

            resultMap.put(codeAssoc.lsCode, codes)
        }

        return UResult.successResult(resultMap)
    }

    UResult<Map<String, Set<Code>>> getEhrCodeBindingsMap() {

        UResult<Map<String, Set<Code>>> result = getCodeBindingsMap(Association.EHR_CODE_IS)

        if (result.error()) { // convert error to warning
            log.warning("NO BINDINGS FOR " + Association.EHR_CODE_IS)
            return UResult.warningResult(new HashMap<>())
        }

        return result
    }

    UResult<Map<String, Set<Code>>> getStandardCodeBindingsMap() {

        UResult<Map<String, Set<Code>>> result = getCodeBindingsMap(Association.STANDARD_CODE_IS)

        if (result.error()) { // convert error to warning
            log.warning("NO BINDINGS FOR " + Association.STANDARD_CODE_IS)
            return UResult.warningResult(new HashMap<>())
        }

        return result
    }

    UResult<Map<String, String>> getUriBindingsMap() {

        Map<String, String> uriBindingsMap = new HashMap<>()
        List<TUriBinding> uriBindings = uriBindingRepository.findAll()

        if (uriBindings.isEmpty()) {
            return UResult.errorResult("NO URIS LOADED TO CREATE A MAP FROM")
        }

        for (TUriBinding uriBinding: uriBindings) {
            uriBindingsMap.put(uriBinding.uriBinding, uriBinding.uri)
        }

        return UResult.successResult(uriBindingsMap)
    }

}

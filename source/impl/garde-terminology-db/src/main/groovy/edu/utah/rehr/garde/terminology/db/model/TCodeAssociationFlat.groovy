package edu.utah.rehr.garde.terminology.db.model

import org.hibernate.annotations.Immutable

import jakarta.persistence.*

@Entity
@Immutable
@Table(name="g_code_association_v")
class TCodeAssociationFlat {

    @Id
    @Column(name="code_association_gid")
    Long codeAssociationId

    @Column(name="ls_code_gid")
    Long lsCodeId

    @Column(name="ls_code_system")
    String lsCodeSystem

    @Column(name="ls_code")
    String lsCode

    @Column(name="ls_code_label")
    String lsCodeLabel

    @Column(name="ls_is_binding_code")
    String lsIsBindingCode

    @Column(name="association_cd")
    String associationCd

    @Column(name="rs_code_gid")
    Long rsCodeId

    @Column(name="rs_code_system")
    String rsCodeSystem

    @Column(name="rs_code")
    String rsCode

    @Column(name="rs_code_label")
    String rsCodeLabel

    @Column(name="rs_is_binding_code")
    String rsIsBindingCode

}

package edu.utah.rehr.garde.terminology.db.model

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table
import org.hibernate.annotations.Immutable

@Entity
@Immutable
@Table(name="g_value_set_v")
class TValueSetFlat {

    @Id
    @Column(name="vs_code_association_gid")
    String gid

    @Column(name="vs_code_gid")
    Long vsCodeId

    @Column(name="vs_code_system")
    String vsCodeSystem

    @Column(name="vs_code")
    String vsCode

    @Column(name="vs_code_label")
    String vsCodeLabel

    @Column(name="m_code_gid")
    Long memberBindingCodeId

    @Column(name="m_code_system")
    String memberBindingCodeSystem

    @Column(name="m_code")
    String memberBindingCode

    @Column(name="m_code_label")
    String memberBindingCodeLabel

    @Column(name="association")
    String association

    @Column(name="code_gid")
    Long codeId

    @Column(name="code_system")
    String codeSystem

    @Column(name="code")
    String code

    @Column(name="code_label")
    String codeLabel

    @Override
    boolean equals(Object obj) {
        if (this == obj)
            return true
        if (obj == null)
            return false
        if (getClass() != obj.getClass())
            return false

        TValueSetFlat comparator = (TValueSetFlat)obj

        return  vsCodeSystem == comparator.codeSystem
                && vsCode == comparator.vsCode
                && codeSystem == comparator.codeSystem
                && code == comparator.code
    }

    @Override
    int hashCode() {
        return Objects.hash(vsCodeId) + Objects.hash(codeId)
    }

    @Override
    String toString() {
        StringBuffer out = new StringBuffer()
        out.append(vsCode).append(" (").append(vsCodeId).append(") ")
        out.append("(HAS_MEMBER) ").append(memberBindingCode)
        out.append(" (").append(association).append(") ")
        out.append(" (").append(codeId).append(") [").append(codeSystem).append("] ").append(code).append(" '").append(codeLabel).append("'")
        return out.toString()
    }
}

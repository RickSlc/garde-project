package edu.utah.rehr.garde.terminology.db.repository

import edu.utah.rehr.garde.terminology.db.model.TerminologyImport
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.CrossOrigin

@Repository
@RepositoryRestResource(collectionResourceRel = "terminologyImport", path = "terminologyImport")
@CrossOrigin(origins = "*")
interface TerminologyImportRepository extends JpaRepository<TerminologyImport, Long> {
}

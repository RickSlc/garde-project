package edu.utah.rehr.garde.terminology.db.model

enum Association {

    CANONICAL_CODE_IS("CANONICAL_CODE_IS")
    , EPIC_CODE_IS("EPIC_CODE_IS")
    , EHR_CODE_IS("EHR_CODE_IS")
    , STANDARD_CODE_IS("STANDARD_CODE_IS")
    , HAS_MEMBER("HAS_MEMBER")
    , CHILD_IS("CHILD_IS")
    , IS_BROADER_THAN("IS_BROADER_THAN")
    , IS_NARROWER_THAN("IS_NARROWER_THAN")
    , IS_EQUIVALENT_TO("IS_EQUIVALENT_TO")

    String code

    Association(String code) {
        this.code = code
    }

}

package edu.utah.rehr.garde.terminology.db.model

class CodeSetDTO {

    Long codeSetGid
    String codeSystem
    String code
    String codeSetLabel

    Set<CodeBindingDTO> members

}

package edu.utah.rehr.garde.impl.cds.knowledgemodule.nccn;


import edu.utah.rehr.garde.domain.core.model.Nccn2023Rule;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.FamilyMemberHistoryHelperR4;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.PatientHelperR4;
import edu.utah.rehr.garde.domain.core.terminology.Code;
import edu.utah.rehr.garde.domain.core.terminology.ITerminologyKey;
import edu.utah.rehr.garde.domain.core.terminology.binding.NccnTerminologyBinding;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import edu.utah.rehr.garde.domain.core.terminology.provider.csv.TerminologyMapsProviderCsv;
import edu.utah.rehr.garde.domain.core.terminology.service.CodeServiceByMaps;
import edu.utah.rehr.garde.domain.core.utils.DateUtil;
import edu.utah.rehr.garde.impl.cdshooks.nccn.rules.FamilialBreastCancerRiskDetection2023Rules;
import lombok.extern.java.Log;
import org.hl7.fhir.r4.model.FamilyMemberHistory;
import org.hl7.fhir.r4.model.Patient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.junit.Assert.*;

/**
 * @author RickSlc
 */
@RunWith(SpringRunner.class)
@Log
public class FamilialBreastCancerRiskDetection2023RulesTest {

    private final ICodeService codeService;
    private final FamilyMemberHistoryHelperR4 familyMemberHistoryHelper;
    private final FamilialBreastCancerRiskDetection2023Rules rules;

    private final String filePath = "test-files/pat-demog-de-id-nlp-tests.csv";

    private final Patient patient;
    private final Patient patientJewish;

    public FamilialBreastCancerRiskDetection2023RulesTest() {
        this.codeService = new CodeServiceByMaps(
                TerminologyMapsProviderCsv.getCodeSetMap("classpath:terminology/garde-code-sets.csv")
                , TerminologyMapsProviderCsv.getCodeBindingsMap("classpath:terminology/garde-standard-code-bindings.csv")
                , TerminologyMapsProviderCsv.getCodeBindingsMap("classpath:terminology/garde-ehr-code-bindings-ut.csv")
                , TerminologyMapsProviderCsv.getUriBindingsMap("classpath:terminology/garde-uri-bindings-map.csv")
        );

        PatientHelperR4 patientHelper = new PatientHelperR4(this.codeService);
        this.familyMemberHistoryHelper = new FamilyMemberHistoryHelperR4(this.codeService);

        this.rules = new FamilialBreastCancerRiskDetection2023Rules(
                this.codeService
                , this.familyMemberHistoryHelper
                , patientHelper);

        this.patient = patientHelper.toPatient(
                "1000", "u1000",
                DateUtil.getValidTestBirthDateBasedOnAge(30),
                "female",
                new Code("test-code-system", "test-religion-code", "test-religion")
        );

        this.patientJewish = patientHelper.toPatient(
                "1001", "u1001",
                DateUtil.getValidTestBirthDateBasedOnAge(30),
                "female",
                getCode(NccnTerminologyBinding.JEWISH_CODE)
        );

    }

    @Test
    public void testPrintFamilyMemberHistory() {
        log.info("testPrintFamilyMemberHistory json");
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                40,
                null, null);

        String json = FamilyMemberHistoryHelperR4.toJson(fhx1);
//        log.info(json);
        assertNotNull(json);
    }

    @Test
    public void testEvaluateBcAge40() {
        log.info("Validate true breast cancer case age 40");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                40,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);

        assertEquals(1, result.size());
        assertTrue(result.containsKey(Nccn2023Rule.BC_HAS_BREAST_CA_AGE_LTE_50_FHX_CRIT_2023.getCode()));
    }

    @Test
    public void testPancreaticCancerFirstDegreeDoesMeetCriteria() {
        log.info("Validate first degree relative with pancreatic cancer does meet criteria.");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.PANCREATIC_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(1, result.size());
        assertEquals(result.get(Nccn2023Rule.BC_FIRST_DEG_RELATIVE_PANCREATIC_CA_FHX_CRIT_2023.getCode()).size(), 1);
    }

    @Test
    public void testFatherWithOvarianCancerDoesNotMeetCriteria() {
        log.info("Validate father with ovarian cancer does not meet criteria.");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.FATHER_CODE),
                getCode(NccnTerminologyBinding.OVARIAN_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(0, result.size());
    }

    @Test
    public void testMaleSecondDegreeWithOvarianCancerDoesNotMeetCriteria() {
        log.info("Validate second degree males with ovarian cancer do not meet criteria.");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MATERNAL_GRANDFATHER_CODE),
                getCode(NccnTerminologyBinding.OVARIAN_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(0, result.size());
    }

    @Test
    public void testMaleThirdDegreeWithOvarianCancerDoesNotMeetCriteria() {
        log.info("Validate third degree males with ovarian cancer do not meet criteria.");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.FATHERS_FATHERS_FATHER_CODE),
                getCode(NccnTerminologyBinding.OVARIAN_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(0, result.size());
    }

    @Test
    public void testPancreaticCancerSecondDegreeDoesNotMeetCriteria() {
        log.info("Validate second degree relative with pancreatic cancer does not meet criteria.");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.PATERNAL_GRANDFATHER_CODE),
                getCode(NccnTerminologyBinding.PANCREATIC_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(0, result.size());
    }

    @Test
    public void testEvaluateBcAge50() {
        log.info("Validate true breast cancer case age 50");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                50,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);

        assertEquals(1, result.size());
        assertTrue(result.containsKey(Nccn2023Rule.BC_HAS_BREAST_CA_AGE_LTE_50_FHX_CRIT_2023.getCode()));
    }
    @Test
    public void testEvaluateAge51() {
        log.info("Validate false breast cancer test age 51");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                51,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(0, result.size());
    }

    @Test
    public void testEvaluateOvarianAge70() {
        log.info("Validate true ovarian cancer test age 70");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.OVARIAN_CANCER_CODE),
                "CODED_FHX",
                70,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(1, result.size());
        assertTrue(result.containsKey(Nccn2023Rule.BC_OVARIAN_CA_FHX_CRIT_2023.getCode()));
    }

    @Test
    public void testEvaluateBrca1() {
        log.info("Validate true BRCA1 patient");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BRCA_1_GENE_MUTATION_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(1, result.size());
        assertTrue(result.containsKey(Nccn2023Rule.BC_GENETIC_MUTATION_IN_FHX_2023.getCode()));
    }

    @Test
    public void testEvaluateBrca2() {
        log.info("Validate true BRCA2 patient");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BRCA_1_GENE_MUTATION_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(1, result.size());
        assertTrue(result.containsKey(Nccn2023Rule.BC_GENETIC_MUTATION_IN_FHX_2023.getCode()));
    }

    @Test
    public void testEvaluateBrca2OnThirdDegreeRelative() {
        log.info("Validate true BRCA2 patient");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHERS_MOTHERS_MOTHER_CODE),
                getCode(NccnTerminologyBinding.BRCA_1_GENE_MUTATION_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(1, result.size());
        assertTrue(result.containsKey(Nccn2023Rule.BC_GENETIC_MUTATION_IN_FHX_2023.getCode()));
    }

    @Test
    public void testEvaluate3DxWithMom3DxSisters() {
        log.info("Validate Mom with 3 Dx does not trigger HAS_3_RELATIVES_WITH_CA_FHX family member rule");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx2);

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(0, result.size());
    }

    @Test
    public void test2ThirdDegreeAndOneSecondDegreeRelativeMeetsCriteria() {
        log.info("Validate 2 third deg and one second meet the 2023 guidelines rule");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHERS_MOTHERS_MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MATERNAL_COUSIN_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx2);

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertTrue(result.containsKey(Nccn2023Rule.BC_3_RELATIVES_WITH_BREAST_OR_PROSTATE_CA_FHX_CRIT_2023.getCode()));
    }

    @Test
    public void testEvaluate2BreastCancerOver50() {
        log.info("Validate 2 relatives with BC don not trigger HAS_3_RELATIVES_WITH_CA_FHX family member rule");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MATERNAL_GRANDMOTHER_CODE),
                getEhrCode(NccnTerminologyBinding.MATERNAL_GRANDMOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                getEhrCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                73,
                null, null);

        fhxList.add(fhx1);

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MATERNAL_AUNT_CODE),
                getEhrCode(NccnTerminologyBinding.MATERNAL_AUNT_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                getEhrCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                51,
                null, null);

        fhxList.add(fhx2);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertTrue(result.isEmpty());
    }


    @Test
    public void testEvaluate3BreastCa_3Sisters() {
        log.info("Validate 3 sisters triggers BC_3_RELATIVES_WITH_BREAST_OR_PROSTATE_CA_FHX_CRIT rule");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.SISTER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "101", new Date(), "1001",
                getCode(NccnTerminologyBinding.SISTER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx2);

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "102", new Date(), "1002",
                getCode(NccnTerminologyBinding.SISTER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(1, result.size());
        assertTrue(result.containsKey(Nccn2023Rule.BC_3_RELATIVES_WITH_BREAST_OR_PROSTATE_CA_FHX_CRIT_2023.getCode()));
    }

    @Test
    public void testEvaluate3BreastCaWithMother2Sisters() {
        log.info("Validate mother and 2 sisters with breast cancer Dx does trigger HAS_3_RELATIVES_WITH_CA_FHX family member rule");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.SISTER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx2);

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.SISTER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(1, result.size());
        assertTrue(result.containsKey(Nccn2023Rule.BC_3_RELATIVES_WITH_BREAST_OR_PROSTATE_CA_FHX_CRIT_2023.getCode()));
    }

    @Test
    public void testEvaluateSingularRelativeWithDupConditions() {
        log.info("Validate 3 FHx statements with a duplicate on a single cardinality relative does not trigger 3 Dx rule.");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MATERNAL_GRANDMOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx2);

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MATERNAL_GRANDMOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(0, result.size());
    }

    @Test
    public void testEvaluate3DxWithMotherMatGrandmaMatUncleEhrCodes() {
        log.info("Validate 3 maternal relatives trigger BC_3_RELATIVES_WITH_BREAST_OR_PROSTATE_CA_FHX_CRIT using EHR codes.");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getEhrCode(NccnTerminologyBinding.MOTHER_CODE),
                getEhrCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "101", new Date(), "1001",
                getEhrCode(NccnTerminologyBinding.MATERNAL_GRANDMOTHER_CODE),
                getEhrCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx2);

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "102", new Date(), "10002",
                getEhrCode(NccnTerminologyBinding.MATERNAL_UNCLE_CODE),
                getEhrCode(NccnTerminologyBinding.PROSTATE_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertTrue(result.containsKey(Nccn2023Rule.BC_3_RELATIVES_WITH_BREAST_OR_PROSTATE_CA_FHX_CRIT_2023.getCode()));
    }

    @Test
    public void testEvaluate3DxWith2Mother1Sister() {
        log.info("Validate mother with 2 Dx and 1 sister does not trigger HAS_3_RELATIVES_WITH_CA_FHX family member rule");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.SISTER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx2);

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(0, result.size());
    }

    @Test
    public void testEvaluateJewish() {
        log.info("Validate jewish with mom with BC");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHERS_MOTHERS_MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patientJewish, fhxList);
        assertEquals(1, result.size());
        assertTrue(result.containsKey(Nccn2023Rule.BC_JEWISH_WITH_CA_FHX_CRIT_2023.getCode()));
    }

    @Test
    public void testEvaluateJewishNoDx() {
        log.info("Validate jewish with no relevant dx");

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.CANCER_KIDNEY_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(0, result.size());
    }

    private Code getCode(ITerminologyKey key) {

        Set<Code> codes = codeService.getCodeSet(key);

        for (Code code: codes) {
            return code;
        }

//        Code code = codeService.getCanonicalCode(key);
//
//        if (codes == null || codes.isEmpty()) {
//            log.severe("NO CODE for terminology key: " + key);
//            return null;
//        }

        return null;
    }

    private Code getEhrCode(ITerminologyKey key) {

        Set<Code> codeSet = codeService.getEhrCodeSet(key);

        if (codeSet == null || codeSet.size() == 0) {
            log.severe( "NO CODES for terminology key: " + key);
            return null;
        }

        for (Code code: codeSet) {
            return code; // return first if more than one
        }

        return null; // should never get here.
    }

    }

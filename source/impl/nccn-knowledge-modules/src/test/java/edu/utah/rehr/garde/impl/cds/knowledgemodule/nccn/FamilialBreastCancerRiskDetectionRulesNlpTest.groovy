package edu.utah.rehr.garde.impl.cds.knowledgemodule.nccn

import edu.utah.rehr.garde.domain.core.model.NccnRule
import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxDTO
import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxNlpDTO
import edu.utah.rehr.garde.domain.core.model.dto.PatientDTO
import edu.utah.rehr.garde.domain.core.model.fhir.r4.FamilyMemberHistoryHelperR4
import edu.utah.rehr.garde.domain.core.model.fhir.r4.PatientHelperR4
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService
import edu.utah.rehr.garde.domain.etl.provider.IFamilyHxDTOProvider
import edu.utah.rehr.garde.domain.etl.provider.IPatientDTOProvider
import edu.utah.rehr.garde.impl.cdshooks.nccn.rules.FamilialBreastCancerRiskDetectionRules
import groovy.util.logging.Log
import org.hl7.fhir.r4.model.FamilyMemberHistory
import org.hl7.fhir.r4.model.Patient
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertTrue

@RunWith(SpringRunner.class)
//@DataJpaTest
@ContextConfiguration(classes = TestRuleTerminologyConfig.class)
@Log
class FamilialBreastCancerRiskDetectionRulesNlpTest {

    @Autowired
    private ICodeService codeService
    @Autowired
    private FamilyMemberHistoryHelperR4 familyMemberHistoryHelper
    @Autowired
    private FamilialBreastCancerRiskDetectionRules rules
    @Autowired
    private PatientHelperR4 patientHelper

    @Autowired
    IPatientDTOProvider patientDTOProvider

    @Autowired
    IFamilyHxDTOProvider familyHxDTOProvider

    // EHR_PAT_ID                              COMMENT               TEST_PURPOSE
    String testPatient2 = 'TEST2' // - breast cancer with no coded age - nlp interprets comment "around 48" ONSET_RANGE AROUND -> range 45-51, min is chosen = 45 - is eligible
    String testPatient3 = 'TEST3' // - breast cancer with no coded age - nlp interprets comment "45" as ONSET_AGE = 45 - is eligible
    String testPatient4 = 'TEST4' // - breast cancer with no coded age - nlp interprets comment "in her late 30s" as ONSET_RANGE LATE 30 -> range 35 - 39, min is chosen
    String testPatient5 = 'TEST5' // - breast cancer WITH coded age 61 - nlp interprets comment "in her late 30s" as ONSET_RANGE LATE 30 -> range 35 - 39, min is chosen; CODED AGE IS OBSERVED
    String p1 = "E5EB81D01682E21CE053BF47649BF121" // - comment "pancreatic cancer" updates code "Cancer, Other" and changes eligibility
    String p2 = "E5EB81D016EAE21CE053BF47649BF121" // - comment "Pancreatic Cancer" updates code "Cancer" and changes eligibility
    String p3 = "E5EB81D01733E21CE053BF47649BF121" // - comment "ovarian cancer" updates code "Cancer" and changes eligibility
    String p4 = "E5EB81D01A3FE21CE053BF47649BF121" // - doesn't meet criteria
    String p5 = "E5EB81D016C4E21CE053BF47649BF121" // - male with breast cancer - eligible; comment "deceased 20 years ago" irrelevant
    String p6 = "E5EB81D01757E21CE053BF47649BF121" // - comment "age around 64" ONSET_RANGE 64 AROUND
    String p7 = "E5EB81D01A3FE21CE053BF47649BF121" // - "total colectomy 1996, a & w" to develop ONSET_PERIOD 1996 (ignore DECADE)
    String p8 = "E5EB81D019C5E21CE053BF47649BF121" // - "stage 4 since 2013, 1st diagnosed in 2008" another ONSET_PERIOD 2008 (ignore DECADE)
    String p9 = "E5EB81D019B3E21CE053BF47649BF121" // - "post-menopausal breast cancer (survived)" to add/test ONSET_STRING post-menopause
    String p10 = "E5EB81D0188DE21CE053BF47649BF121" // - "died at 60 y/o" to add/test ONSET_RANGE 60 BEFORE

    // - breast cancer with no coded age - nlp interprets "diagnosed in 2015" as ONSET_PERIOD = 2015; calc age in 2015 for age of onset: CAN'T - DON'T HAVE BIRTH DATE OF RELATIVE

    @Test
    void testFamilyMemberHistoryWithNlp_testPatient2() {
        log.info("testFamilyMemberHistoryWithNlp_testPatient2")

        String testPatientId = testPatient2

        List<FamilyHxNlpDTO> fhxPtoList = familyHxDTOProvider.getFamilyHxNlpDTOs(testPatientId)
        List<FamilyMemberHistory> fhxList = familyMemberHistoryHelper.familyHxNlpDTOListToFamilyMemberHistoryList(fhxPtoList)
        assertTrue(fhxList.size() > 0)

        PatientDTO patientDto = patientDTOProvider.getPatientDTO(testPatientId)
        Patient patient = patientHelper.toPatient(patientDto)

        Map<NccnRule, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList)

        assertEquals(1, result.keySet().size())
        assertTrue(result.keySet().contains(NccnRule.BC_HAS_BREAST_CA_AGE_LTE_45_FHX_CRIT))
    }

    @Test
    void testFamilyMemberHistoryWithNlp_testPatient3() {
        log.info("testFamilyMemberHistoryWithNlp_testPatient3")

        String testPatientId = testPatient3

        List<FamilyHxNlpDTO> fhxPtoList = familyHxDTOProvider.getFamilyHxNlpDTOs(testPatientId)
        List<FamilyMemberHistory> fhxList = familyMemberHistoryHelper.familyHxNlpDTOListToFamilyMemberHistoryList(fhxPtoList)
        assertTrue(fhxList.size() > 0)

        PatientDTO patientDto = patientDTOProvider.getPatientDTO(testPatientId)
        Patient patient = patientHelper.toPatient(patientDto)

        Map<NccnRule, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList)

        assertEquals(1, result.keySet().size())
        assertTrue(result.keySet().contains(NccnRule.BC_HAS_BREAST_CA_AGE_LTE_45_FHX_CRIT))
    }

    @Test
    void testFamilyMemberHistoryWithNlp_testPatient4() {
        log.info("testFamilyMemberHistoryWithNlp_testPatient4")

        String testPatientId = testPatient4

        List<FamilyHxNlpDTO> fhxPtoList = familyHxDTOProvider.getFamilyHxNlpDTOs(testPatientId)
        List<FamilyMemberHistory> fhxList = familyMemberHistoryHelper.familyHxNlpDTOListToFamilyMemberHistoryList(fhxPtoList)
        assertTrue(fhxList.size() > 0)

        PatientDTO patientDto = patientDTOProvider.getPatientDTO(testPatientId)
        Patient patient = patientHelper.toPatient(patientDto)

        Map<NccnRule, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList)

        assertEquals(1, result.keySet().size())
        assertTrue(result.keySet().contains(NccnRule.BC_HAS_BREAST_CA_AGE_LTE_45_FHX_CRIT))
    }

    @Test
    void testFamilyMemberHistoryWithNlp_testPatient5() {
        log.info("testFamilyMemberHistoryWithNlp_testPatient5")

        String testPatientId = testPatient5

        List<FamilyHxNlpDTO> fhxPtoList = familyHxDTOProvider.getFamilyHxNlpDTOs(testPatientId)
        List<FamilyMemberHistory> fhxList = familyMemberHistoryHelper.familyHxNlpDTOListToFamilyMemberHistoryList(fhxPtoList)
        assertTrue(fhxList.size() > 0)

        PatientDTO patientDto = patientDTOProvider.getPatientDTO(testPatientId)
        Patient patient = patientHelper.toPatient(patientDto)

        Map<NccnRule, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList)

        assertEquals(0, result.keySet().size())
    }

    @Test
    void testFamilyMemberHistoryWithNlp_p1() {
        log.info("testFamilyMemberHistoryWithNlp_p1")

        String testPatientId = p1

        List<FamilyHxNlpDTO> fhxPtoList = familyHxDTOProvider.getFamilyHxNlpDTOs(testPatientId)
        List<FamilyMemberHistory> fhxList = familyMemberHistoryHelper.familyHxNlpDTOListToFamilyMemberHistoryList(fhxPtoList)
        assertTrue(fhxList.size() > 0)

        PatientDTO patientDto = patientDTOProvider.getPatientDTO(testPatientId)
        Patient patient = patientHelper.toPatient(patientDto)

        Map<NccnRule, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList)

        assertEquals(1, result.keySet().size())
        assertTrue(result.keySet().contains(NccnRule.BC_PANCREATIC_CA_FHX_CRIT))

    }

    @Test
    void testFamilyMemberHistoryWithNlp_p2() {
        log.info("testFamilyMemberHistoryWithNlp_p2")

        String testPatientId = p2

        List<FamilyHxNlpDTO> fhxPtoList = familyHxDTOProvider.getFamilyHxNlpDTOs(testPatientId)
        List<FamilyMemberHistory> fhxList = familyMemberHistoryHelper.familyHxNlpDTOListToFamilyMemberHistoryList(fhxPtoList)
        assertTrue(fhxList.size() > 0)

        PatientDTO patientDto = patientDTOProvider.getPatientDTO(testPatientId)
        Patient patient = patientHelper.toPatient(patientDto)

        Map<NccnRule, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList)

        assertEquals(1, result.keySet().size())
        assertTrue(result.keySet().contains(NccnRule.BC_PANCREATIC_CA_FHX_CRIT))

    }

    @Test
    void testFamilyMemberHistoryWithNlp_p3() {
        log.info("testFamilyMemberHistoryWithNlp_p3 - eligible")

        String testPatientId = p3

        List<FamilyHxNlpDTO> fhxPtoList = familyHxDTOProvider.getFamilyHxNlpDTOs(testPatientId)
        List<FamilyMemberHistory> fhxList = familyMemberHistoryHelper.familyHxNlpDTOListToFamilyMemberHistoryList(fhxPtoList)
        assertTrue(fhxList.size() > 0)

        PatientDTO patientDto = patientDTOProvider.getPatientDTO(testPatientId)
        Patient patient = patientHelper.toPatient(patientDto)

        Map<NccnRule, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList)

        assertEquals(1, result.keySet().size())
        assertTrue(result.keySet().contains(NccnRule.BC_OVARIAN_CA_FHX_CRIT))

    }

    @Test
    void testFamilyMemberHistoryWithOutNlp_p3() {
        log.info("testFamilyMemberHistoryWithOutNlp_p3 - not eligible")

        String testPatientId = p3

        List<FamilyHxDTO> fhxDtoList = familyHxDTOProvider.getFamilyHxDTOs(testPatientId)
        List<FamilyMemberHistory> fhxList = familyMemberHistoryHelper.familyHxDTOListToFamilyMemberHistoryList(fhxDtoList)
        assertTrue(fhxList.size() > 0)

        PatientDTO patientDto = patientDTOProvider.getPatientDTO(testPatientId)
        Patient patient = patientHelper.toPatient(patientDto)

        Map<NccnRule, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList)

        assertEquals(0, result.keySet().size())
    }

    @Test
    void testFamilyMemberHistoryWithNlp_p4() {
        log.info("testFamilyMemberHistoryWithNlp_p4 - not eligible")

        String testPatientId = p4

        List<FamilyHxNlpDTO> fhxPtoList = familyHxDTOProvider.getFamilyHxNlpDTOs(testPatientId)
        List<FamilyMemberHistory> fhxList = familyMemberHistoryHelper.familyHxNlpDTOListToFamilyMemberHistoryList(fhxPtoList)
        assertTrue(fhxList.size() > 0)

        PatientDTO patientDto = patientDTOProvider.getPatientDTO(testPatientId)
        Patient patient = patientHelper.toPatient(patientDto)

        Map<NccnRule, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList)
        assertEquals(0, result.keySet().size())
    }

    @Test
    void testFamilyMemberHistoryWithNlp_p5() {
        log.info("testFamilyMemberHistoryWithNlp_p5 - eligible")

        String testPatientId = p5

        List<FamilyHxNlpDTO> fhxPtoList = familyHxDTOProvider.getFamilyHxNlpDTOs(testPatientId)
        List<FamilyMemberHistory> fhxList = familyMemberHistoryHelper.familyHxNlpDTOListToFamilyMemberHistoryList(fhxPtoList)
        assertTrue(fhxList.size() > 0)

        PatientDTO patientDto = patientDTOProvider.getPatientDTO(testPatientId)
        Patient patient = patientHelper.toPatient(patientDto)

        Map<NccnRule, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList)
        assertEquals(1, result.keySet().size())
        assertTrue(result.keySet().contains(NccnRule.BC_MALE_BREAST_CA_FHX_CRIT))

    }



}

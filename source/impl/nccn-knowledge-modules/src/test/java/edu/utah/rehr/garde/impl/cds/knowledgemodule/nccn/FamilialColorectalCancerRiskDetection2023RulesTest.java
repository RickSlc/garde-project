package edu.utah.rehr.garde.impl.cds.knowledgemodule.nccn;

import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.model.Nccn2023Rule;
import edu.utah.rehr.garde.domain.core.model.fhir.FuzzyAgeRangeQualifier;
import edu.utah.rehr.garde.domain.core.model.fhir.NlpOnsetType;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.FamilyMemberHistoryHelperR4;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.FuzzyAgeRangeInterpreterR4;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.PatientHelperR4;
import edu.utah.rehr.garde.domain.core.terminology.Code;
import edu.utah.rehr.garde.domain.core.terminology.binding.NccnTerminologyBinding;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import edu.utah.rehr.garde.domain.core.terminology.provider.csv.TerminologyMapsProviderCsv;
import edu.utah.rehr.garde.domain.core.terminology.service.CodeServiceByMaps;
import edu.utah.rehr.garde.impl.cdshooks.nccn.rules.FamilialColorectalCancerRiskDetection2023Rules;
import org.hl7.fhir.r4.model.FamilyMemberHistory;
import org.hl7.fhir.r4.model.Range;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class FamilialColorectalCancerRiskDetection2023RulesTest {

    private final ICodeService codeService;
    private final FamilyMemberHistoryHelperR4 familyMemberHistoryHelper;
    private final FamilialColorectalCancerRiskDetection2023Rules rules;
    private final FuzzyAgeRangeInterpreterR4 fuzzyAgeRangeInterpreter;

    public FamilialColorectalCancerRiskDetection2023RulesTest() {
        this.fuzzyAgeRangeInterpreter = new FuzzyAgeRangeInterpreterR4();
        this.codeService = new CodeServiceByMaps(
                TerminologyMapsProviderCsv.getCodeSetMap("classpath:terminology/garde-code-sets.csv")
                , TerminologyMapsProviderCsv.getCodeBindingsMap("classpath:terminology/garde-standard-code-bindings.csv")
                , TerminologyMapsProviderCsv.getCodeBindingsMap("classpath:terminology/garde-ehr-code-bindings-ut.csv")
                , TerminologyMapsProviderCsv.getUriBindingsMap("classpath:terminology/garde-uri-bindings-map.csv")
        );

        PatientHelperR4 patientHelper = new PatientHelperR4(this.codeService);
        this.familyMemberHistoryHelper = new FamilyMemberHistoryHelperR4(this.codeService);

        this.rules = new FamilialColorectalCancerRiskDetection2023Rules(
                this.codeService
                , this.familyMemberHistoryHelper
        );

    }

    @Test
    public void testCO_3_RELATIVES_WITH_CA_FHX_CRIT_COx3() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                null,
                null,
                null
        );

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "101", new Date(), "1000",
                NccnTerminologyBinding.MATERNAL_GRANDMOTHER_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                null,
                null,
                null
        );

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "102", new Date(), "1000",
                NccnTerminologyBinding.FATHER_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                null,
                null,
                null
        );

        fhxList.add(fhx);
        fhxList.add(fhx2);
        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);

        assertEquals(0, results.size()); // must be on the same side of the family
    }

    @Test
    public void testCO_3_RELATIVES_WITH_CA_FHX_CRIT_COx3_not_same_side() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                null,
                null,
                null
        );

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "101", new Date(), "1000",
                NccnTerminologyBinding.MATERNAL_GRANDMOTHER_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                null,
                null,
                null
        );

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "102", new Date(), "1000",
                NccnTerminologyBinding.MATERNAL_GRANDFATHER_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                null,
                null,
                null
        );

        fhxList.add(fhx);
        fhxList.add(fhx2);
        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);

        assertEquals(0, results.size()); // must be on the same side of the family
    }

    @Test
    public void testCO_3_RELATIVES_WITH_CA_FHX_CRIT_COx3_on_same_side() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                null,
                null,
                null
        );

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "101", new Date(), "1000",
                NccnTerminologyBinding.MATERNAL_GRANDMOTHER_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                null,
                null,
                null
        );

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "102", new Date(), "1000",
                NccnTerminologyBinding.MATERNAL_UNCLE_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                null,
                null,
                null
        );

        fhxList.add(fhx);
        fhxList.add(fhx2);
        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);

        assertTrue(results.containsKey(Nccn2023Rule.CO_3_RELATIVES_WITH_CA_FHX_CRIT_2023.getCode()));
    }
    @Test
    public void testCO_3_RELATIVES_WITH_CA_FHX_CRIT_COx4_3_on_same_side() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                null,
                null,
                null
        );

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "101", new Date(), "1001",
                NccnTerminologyBinding.MATERNAL_GRANDMOTHER_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                null,
                null,
                null
        );

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "102", new Date(), "1002",
                NccnTerminologyBinding.MATERNAL_UNCLE_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                null,
                null,
                null
        );

        FamilyMemberHistory fhx4 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "103", new Date(), "1003",
                NccnTerminologyBinding.PATERNAL_UNCLE_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                null,
                null,
                null
        );

        fhxList.add(fhx);
        fhxList.add(fhx2);
        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);

        assertTrue(results.containsKey(Nccn2023Rule.CO_3_RELATIVES_WITH_CA_FHX_CRIT_2023.getCode()));
    }

    @Test
    public void testCO_3_RELATIVES_WITH_CA_FHX_CRIT_MOMx3_should_fail() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                null,
                null,
                null
        );

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "101", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                null,
                null,
                null
        );

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "102", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                null,
                null,
                null
        );

        fhxList.add(fhx);
        fhxList.add(fhx2);
        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);

        assertTrue(results.isEmpty());
    }

    @Test
    public void testCO_2_LS_cancers_same_side() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.CANCER_STOMACH_CODE,
                "CODED_FHX",
                49,
                null,
                null
        );

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "101", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                60,
                null,
                null
        );

        fhxList.add(fhx);
        fhxList.add(fhx2);

        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);

        assertTrue(results.containsKey(Nccn2023Rule.CO_2_RELATIVES_WITH_1_LT_50_CA_FHX_CRIT_2023.getCode()));
    }

    @Test
    public void testCO_3_RELATIVES_WITH_CA_FHX_CRIT() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        Code aunt = new Code("AUNT","http://hl7.org/fhir/ValueSet/v3-FamilyMember","AUNT", "aunt");
        Code uterineCancer = new Code("CANCER_UTERINE", "http://snomed.info/sct", "371973000", "Malignant neoplasm of uterus (disorder)");

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                aunt,
                uterineCancer,
                "CODED_FHX",
                60,
                null,
                "age 60-ish"
        );

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "101", new Date(), "1000",
                aunt,
                uterineCancer,
                "CODED_FHX",
                60,
                null,
                "age 60-ish"
        );

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "102", new Date(), "1000",
                aunt,
                uterineCancer,
                "CODED_FHX",
                60,
                null,
                "age 60-ish"
        );

        fhxList.add(fhx);
        fhxList.add(fhx2);
        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);

        assertTrue(results.isEmpty()); // must be on the same side of the family - aunt has no side
    }

    @Test
    public void testCO_3_RELATIVES_WITH_CA_FHX_CRIT_1() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.MATERNAL_AUNT_CODE,
                NccnTerminologyBinding.UTERINE_CANCER_CODE,
                "CODED_FHX",
                60,
                null,
                "age 60-ish"
        );

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "101", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.UTERINE_CANCER_CODE,
                "CODED_FHX",
                60,
                null,
                "age 60-ish"
        );

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "102", new Date(), "1000",
                NccnTerminologyBinding.MATERNAL_GRANDMOTHER_CODE,
                NccnTerminologyBinding.UTERINE_CANCER_CODE,
                "CODED_FHX",
                60,
                null,
                "age 60-ish"
        );

        fhxList.add(fhx);
        fhxList.add(fhx2);
        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);

        assertEquals(1, results.size());
        assertTrue(results.containsKey(Nccn2023Rule.CO_3_RELATIVES_WITH_CA_FHX_CRIT_2023.getCode()));
    }

    @Test
    public void testCO_3_RELATIVES_WITH_CA_FHX_CRIT_2() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.MATERNAL_AUNT_CODE,
                NccnTerminologyBinding.CANCER_BLADDER_CODE,
                "CODED_FHX",
                60,
                null,
                "age 60-ish"
        );

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "101", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.CANCER_KIDNEY_CODE,
                "CODED_FHX",
                60,
                null,
                "age 60-ish"
        );

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "102", new Date(), "1000",
                NccnTerminologyBinding.SISTER_CODE,
                NccnTerminologyBinding.UTERINE_CANCER_CODE,
                "CODED_FHX",
                60,
                null,
                null
        );

        fhxList.add(fhx);
        fhxList.add(fhx2);
        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);

        assertEquals(1, results.size());
        assertTrue(results.containsKey(Nccn2023Rule.CO_3_RELATIVES_WITH_CA_FHX_CRIT_2023.getCode()));
    }

    @Test
    public void testCO_3_RELATIVES_WITH_CA_FHX_CRIT_3() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.PATERNAL_GRANDMOTHER_CODE,
                NccnTerminologyBinding.CANCER_BLADDER_CODE,
                "CODED_FHX",
                60,
                null,
                "age 60-ish"
        );

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "101", new Date(), "1000",
                NccnTerminologyBinding.FATHER_CODE,
                NccnTerminologyBinding.CANCER_KIDNEY_CODE,
                "CODED_FHX",
                60,
                null,
                "age 60-ish"
        );

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "102", new Date(), "1000",
                NccnTerminologyBinding.PATERNAL_AUNT_CODE,
                NccnTerminologyBinding.UTERINE_CANCER_CODE,
                "CODED_FHX",
                60,
                null,
                null
        );

        fhxList.add(fhx);
        fhxList.add(fhx2);
        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);

        assertEquals(1, results.size());
        assertTrue(results.containsKey(Nccn2023Rule.CO_3_RELATIVES_WITH_CA_FHX_CRIT_2023.getCode()));
    }

    @Test
    public void testCO_3_RELATIVES_WITH_CA_FHX_CRIT_4() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.PATERNAL_GRANDMOTHER_CODE,
                NccnTerminologyBinding.CANCER_BLADDER_CODE,
                "CODED_FHX",
                60,
                null,
                "age 60-ish"
        );

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "101", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE, // not on the same side of the family
                NccnTerminologyBinding.CANCER_KIDNEY_CODE,
                "CODED_FHX",
                60,
                null,
                "age 60-ish"
        );

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "102", new Date(), "1000",
                NccnTerminologyBinding.PATERNAL_AUNT_CODE,
                NccnTerminologyBinding.UTERINE_CANCER_CODE,
                "CODED_FHX",
                60,
                null,
                null
        );

        fhxList.add(fhx);
        fhxList.add(fhx2);
        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);

        assertEquals(0, results.size());
    }

    @Test
    public void testCO_3_RELATIVES_WITH_CA_FHX_CRIT_5() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.PATERNAL_GRANDMOTHER_CODE,
                NccnTerminologyBinding.CANCER_BLADDER_CODE,
                "CODED_FHX",
                60,
                null,
                "age 60-ish"
        );

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "101", new Date(), "1000",
                NccnTerminologyBinding.FATHER_CODE,
                NccnTerminologyBinding.CANCER_KIDNEY_CODE,
                "CODED_FHX",
                60,
                null,
                "age 60-ish"
        );

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "102", new Date(), "1000",
                NccnTerminologyBinding.PATERNAL_AUNT_CODE,
                NccnTerminologyBinding.CANCER_BRAIN_CODE,
                "CODED_FHX",
                60,
                null,
                null
        );

        fhxList.add(fhx);
        fhxList.add(fhx2);
        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);

        assertTrue(results.containsKey(Nccn2023Rule.CO_3_RELATIVES_WITH_CA_FHX_CRIT_2023.getCode()));
    }

    @Test
    public void testCO_ENDOMETRIAL_CA_AGE_LTE_50_FHX_CRIT() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        UResult<Range> ageRangeResult = fuzzyAgeRangeInterpreter.interpret(60, FuzzyAgeRangeQualifier.AROUND, NlpOnsetType.ONSET_RANGE);

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.ENDOMETRIAL_CANCER_CODE,
                "CODED_FHX",
                50,
                ageRangeResult.getResult(), // should use onset age and not this.
                "age 60-ish"
        );

        fhxList.add(fhx);
        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);
        assertTrue(results.containsKey(Nccn2023Rule.CO_FIRST_GEN_ENDOMETRIAL_CA_AGE_LTE_50_FHX_CRIT_2023.getCode()));
    }

    @Test
    public void testCO_ENDOMETRIAL_CA_AGE_LT_50_FHX_CRIT_0() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.UTERINE_CANCER_CODE,
                "CODED_FHX",
                51,
                null,
                null
        );

        fhxList.add(fhx);

        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);

        assertEquals(0, results.size());
    }

    @Test
    public void testCO_ENDOMETRIAL_CA_AGE_LT_50_FHX_CRIT_1() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                new Code("dummy","dummy","dummy"),
                codeService.getCanonicalCode(NccnTerminologyBinding.UTERINE_CANCER_CODE),
                "CODED_FHX",
                50,
                null,
                null
        );

        fhxList.add(fhx);

        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);

        assertTrue(results.isEmpty());
    }

    @Test
    public void testCO_GENETIC_FHX_CRIT() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.LYNCH_SYNDROME_CODE,
                "CODED_FHX",
                60,
                null,
                null
        );

        fhxList.add(fhx);

        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);

        assertEquals(1, results.size());
        assertTrue(results.containsKey(Nccn2023Rule.CO_GENETIC_FHX_CRIT_2023.getCode()));
    }

    @Test
    public void testCO_GENETIC_FHX_CRIT_DUMB_CODE() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                new Code("dummy","dummy","dummy"), // relationship is not specified in NCCN, only a family member of some kind.
                codeService.getCanonicalCode(NccnTerminologyBinding.LYNCH_SYNDROME_CODE),
                "CODED_FHX",
                60,
                null,
                null
        );

        fhxList.add(fhx);
        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);
        assertFalse(results.containsKey(Nccn2023Rule.CO_GENETIC_FHX_CRIT_2023.getCode()));
    }

    @Test
    public void testCO_GENETIC_FHX_CRIT_THIRD_GEN() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
//                new Code("dummy","dummy","dummy"), // relationship is not specified in NCCN, only a family member of some kind.
                codeService.getCanonicalCode(NccnTerminologyBinding.MOTHERS_FATHERS_SISTER_CODE),
                codeService.getCanonicalCode(NccnTerminologyBinding.LYNCH_SYNDROME_CODE),
                "CODED_FHX",
                100, // age is irrelevant
                null,
                null
        );

        fhxList.add(fhx);
        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);
        assertTrue(results.containsKey(Nccn2023Rule.CO_GENETIC_FHX_CRIT_2023.getCode()));
    }

    @Test
    public void testCO_GENETIC_FHX_CRIT_BRACA_1() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.BRCA_1_GENE_MUTATION_CODE, // should trigger breast logic, NOT colorectal logic
                "CODED_FHX",
                60,
                null,
                null
        );

        fhxList.add(fhx);
        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);
        assertEquals(0, results.size());
    }

    @Test
    public void testCO_COLON_CA_AGE_LTE_50_FHX_CRIT() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                50,
                null,
                null
        );

        fhxList.add(fhx);
        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);
        assertTrue(results.containsKey(Nccn2023Rule.CO_FIRST_GEN_COLON_CA_AGE_LTE_50_FHX_CRIT_2023.getCode()));
    }

    @Test
    public void testCO_COLON_CA_AGE_NOT_LTE_50_FHX_CRIT() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                51,
                null,
                null
        );

        fhxList.add(fhx);

        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);
        assertEquals(0, results.size());
    }

    @Test
    public void testCO_COLON_CA_AGE_LTE_50_FHX_CRIT_AGE_1() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                1,
                null,
                null
        );

        fhxList.add(fhx);
        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);
        assertTrue(results.containsKey(Nccn2023Rule.CO_FIRST_GEN_COLON_CA_AGE_LTE_50_FHX_CRIT_2023.getCode()));
    }

    @Test
    public void testCO_COLON_CA_AGE_LTE_50_FHX_CRIT_NLP_AGE_50() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        UResult<Range> ageRangeResult = fuzzyAgeRangeInterpreter.interpret(50, FuzzyAgeRangeQualifier.DECADE, NlpOnsetType.ONSET_RANGE);

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                null,
                ageRangeResult.getResult(),
                null
        );

        fhxList.add(fhx);
        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);
        assertTrue(results.containsKey(Nccn2023Rule.CO_FIRST_GEN_COLON_CA_AGE_LTE_50_FHX_CRIT_2023.getCode()));
    }

    @Test
    public void testCO_COLON_CA_AGE_LTE_50_FHX_CRIT_NLP() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        UResult<Range> ageRangeResult = fuzzyAgeRangeInterpreter.interpret(53, FuzzyAgeRangeQualifier.AROUND, NlpOnsetType.ONSET_RANGE);

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                null,
                ageRangeResult.getResult(),
                null
        );

        fhxList.add(fhx);
        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);
        assertTrue(results.containsKey(Nccn2023Rule.CO_FIRST_GEN_COLON_CA_AGE_LTE_50_FHX_CRIT_2023.getCode()));
    }

    @Test
    public void testCO_COLON_CA_AGE_LTE_50_FHX_CRIT_NLP_TIGHT() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        fuzzyAgeRangeInterpreter.setTightRanges();
        UResult<Range> ageRangeResult = fuzzyAgeRangeInterpreter.interpret(53, FuzzyAgeRangeQualifier.AROUND, NlpOnsetType.ONSET_RANGE);

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.ENDOMETRIAL_CANCER_CODE,
                "CODED_FHX",
                null,
                ageRangeResult.getResult(),
                null
        );

        fhxList.add(fhx);
        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);
        assertEquals(0, results.size());
    }

    @Test
    public void testCO_COLON_CA_AGE_LT_50_FHX_CRIT_NLP_LOOSE_NOT_ENOUGH() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        fuzzyAgeRangeInterpreter.setLooseRanges();
        UResult<Range> ageRangeResult = fuzzyAgeRangeInterpreter.interpret(54, FuzzyAgeRangeQualifier.AROUND, NlpOnsetType.ONSET_RANGE);

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                NccnTerminologyBinding.MOTHER_CODE,
                NccnTerminologyBinding.COLON_CANCER_CODE,
                "CODED_FHX",
                null,
                ageRangeResult.getResult(),
                null
        );

        fhxList.add(fhx);
        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);
        assertEquals(0, results.size());
    }

    @Test
    public void testCO_COLON_CA_AGE_LT_50_FHX_CRIT_8() {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();

        fuzzyAgeRangeInterpreter.setLooseRanges();
        UResult<Range> ageRangeResult = fuzzyAgeRangeInterpreter.interpret(50, FuzzyAgeRangeQualifier.AROUND, NlpOnsetType.ONSET_RANGE);

        FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                new Code("test.value.not.in.set", "COUSIN", "Cousin"),
                codeService.getCanonicalCode(NccnTerminologyBinding.COLON_CANCER_CODE),
                "CODED_FHX",
                null,
                ageRangeResult.getResult(),
                null
        );

        fhxList.add(fhx);

        Map<String, List<FamilyMemberHistory>> results = rules.evaluate(fhxList);

        assertEquals(0, results.size());
    }

}

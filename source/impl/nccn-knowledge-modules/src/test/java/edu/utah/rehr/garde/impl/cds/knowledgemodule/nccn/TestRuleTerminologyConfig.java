package edu.utah.rehr.garde.impl.cds.knowledgemodule.nccn;

import edu.utah.rehr.garde.domain.core.model.fhir.r4.FamilyMemberHistoryHelperR4;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.PatientHelperR4;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import edu.utah.rehr.garde.domain.core.terminology.provider.csv.TerminologyMapsProviderCsv;
import edu.utah.rehr.garde.domain.core.terminology.service.CodeServiceByMaps;
import edu.utah.rehr.garde.domain.etl.provider.IFamilyHxDTOProvider;
import edu.utah.rehr.garde.domain.etl.provider.IPatientDTOProvider;
import edu.utah.rehr.garde.impl.cdshooks.nccn.rules.FamilialBreastCancerRiskDetection2023Rules;
import edu.utah.rehr.garde.impl.cdshooks.nccn.rules.FamilialBreastCancerRiskDetectionRules;
import edu.utah.rehr.garde.impl.cdshooks.nccn.rules.FamilialColorectalCancerRiskDetection2023Rules;
import edu.utah.rehr.garde.impl.cdshooks.nccn.rules.FamilialColorectalCancerRiskDetectionRules;
import edu.utah.rehr.garde.impl.etl.provider.FamilyHxDTOProviderCsv;
import edu.utah.rehr.garde.impl.etl.provider.PatientDTOProviderCsv;
import edu.utah.rehr.garde.impl.etl.service.CsvService;
import groovy.util.logging.Log;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Log
class TestRuleTerminologyConfig {

    @Bean
    public ICodeService codeService() {
        return new CodeServiceByMaps(
                TerminologyMapsProviderCsv.getCodeSetMap("classpath:terminology/garde-code-sets.csv")
                , TerminologyMapsProviderCsv.getCodeBindingsMap("classpath:terminology/garde-standard-code-bindings.csv")
                , TerminologyMapsProviderCsv.getCodeBindingsMap("classpath:terminology/garde-ehr-code-bindings-ut.csv")
                , TerminologyMapsProviderCsv.getUriBindingsMap("classpath:terminology/garde-uri-bindings-map.csv")
        );
    }

    @Bean
    IFamilyHxDTOProvider familyHxDTOProvider() {
        return new FamilyHxDTOProviderCsv(new CsvService(), "test-files/pat-fhx-de-id-nlp-tests.csv");
    }

    @Bean
    IPatientDTOProvider patientDTOProvider() {
        return new PatientDTOProviderCsv();
    }

    @Bean
    FamilyMemberHistoryHelperR4 familyMemberHistoryHelperR4() {
        return new FamilyMemberHistoryHelperR4(codeService());
    }

    @Bean
    PatientHelperR4 patientHelperR4() {

        PatientHelperR4 helper = new PatientHelperR4(codeService());

        return helper;
    }

    @Bean
    FamilialBreastCancerRiskDetectionRules familialBreastCancerRiskDetectionRules() {
        return new FamilialBreastCancerRiskDetectionRules(codeService(), familyMemberHistoryHelperR4(), patientHelperR4());
    }

    @Bean
    FamilialBreastCancerRiskDetection2023Rules familialBreastCancerRiskDetection2023Rules() {
        return new FamilialBreastCancerRiskDetection2023Rules(codeService(), familyMemberHistoryHelperR4(), patientHelperR4());
    }

    @Bean
    FamilialColorectalCancerRiskDetectionRules familialColorectalCancerRiskDetectionRules() {
        return new FamilialColorectalCancerRiskDetectionRules(codeService(), familyMemberHistoryHelperR4());
    }

    @Bean
    FamilialColorectalCancerRiskDetection2023Rules familialColorectalCancerRiskDetection2023Rules() {
        return new FamilialColorectalCancerRiskDetection2023Rules(codeService(), familyMemberHistoryHelperR4());
    }

}

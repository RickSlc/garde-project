package edu.utah.rehr.garde.impl.cds.knowledgemodule.nccn;


import edu.utah.rehr.garde.domain.core.model.NccnRule;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.FamilyMemberHistoryHelperR4;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.PatientHelperR4;
import edu.utah.rehr.garde.domain.core.terminology.Code;
import edu.utah.rehr.garde.domain.core.terminology.ITerminologyKey;
import edu.utah.rehr.garde.domain.core.terminology.binding.NccnTerminologyBinding;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import edu.utah.rehr.garde.impl.cdshooks.nccn.rules.FamilialBreastCancerRiskDetectionRules;
import lombok.extern.java.Log;
import org.hl7.fhir.r4.model.FamilyMemberHistory;
import org.hl7.fhir.r4.model.Patient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.*;

/**
 * @author RickSlc
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestRuleTerminologyConfig.class)
@Log
public class FamilialBreastCancerRiskDetectionRulesTest {
    @Autowired
    private ICodeService codeService;
    @Autowired
    private FamilyMemberHistoryHelperR4 familyMemberHistoryHelper;
    @Autowired
    private FamilialBreastCancerRiskDetectionRules rules;
    @Autowired
    private PatientHelperR4 patientHelper;

    @Test
    public void testPrintFamilyMemberHistory() {
        log.info("testPrintFamilyMemberHistory json");
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                40,
                null, null);

        String json = FamilyMemberHistoryHelperR4.toJson(fhx1);
        log.info(json);
        assertNotNull(json);
    }

    @Test
    public void testEvaluateBcAge40() {
        log.info("Validate true breast cancer case age 40");

        Patient patient = patientHelper.toPatient(
                "1000", "u1000",
                new GregorianCalendar(1990, Calendar.FEBRUARY, 11).getTime(),
                "female",
                new Code("test-code-system", "test-religion-code", "test-religion")
        );

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                40,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);

        assertEquals(1, result.size());
        assertTrue(result.containsKey(NccnRule.BC_HAS_BREAST_CA_AGE_LTE_45_FHX_CRIT.getCode()));
    }

    @Test
    public void testEvaluateMaleWithMotherBcAge40() {
        log.info("Validate false male patient with fhx breast cancer case age 40");

        Patient patient = patientHelper.toPatient(
                "1000", "u1000",
                new GregorianCalendar(1990, Calendar.FEBRUARY, 11).getTime(),
                "male",
                new Code("test-code-system", "test-religion-code", "test-religion")
        );

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                40,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(1, result.size());
        assertEquals(result.get(NccnRule.BC_HAS_BREAST_CA_AGE_LTE_45_FHX_CRIT.getCode()).size(), 1);
    }

    @Test
    public void testEvaluateAge46() {
        log.info("Validate false breast cancer test age 46");

        Patient patient = patientHelper.toPatient(
                "1000", "u1000",
                new GregorianCalendar(1990, Calendar.FEBRUARY, 11).getTime(),
                "female",
                new Code("test-code-system", "test-religion-code", "test-religion")
        );

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                46,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(0, result.size());
    }

    @Test
    public void testEvaluateOvarianAge70() {
        log.info("Validate true ovarian cancer test age 70");

        Patient patient = patientHelper.toPatient(
                "1000", "u1000",
                new GregorianCalendar(1990, Calendar.FEBRUARY, 11).getTime(),
                "female",
                new Code("test-code-system", "test-religion-code", "test-religion")
        );

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.OVARIAN_CANCER_CODE),
                "CODED_FHX",
                70,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(1, result.size());
        assertTrue(result.containsKey(NccnRule.BC_OVARIAN_CA_FHX_CRIT.getCode()));
    }

    @Test
    public void testEvaluateBrca1() {
        log.info("Validate true BRCA1 patient");

        Patient patient = patientHelper.toPatient(
                "1000", "u1000",
                new GregorianCalendar(1990, Calendar.FEBRUARY, 11).getTime(),
                "female",
                new Code("test-code-system", "test-religion-code", "test-religion")
        );

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BRCA_1_GENE_MUTATION_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(1, result.size());
        assertTrue(result.containsKey(NccnRule.BC_GENETIC_MUTATION_IN_FHX.getCode()));
    }

    @Test
    public void testEvaluateBrca2() {
        log.info("Validate true BRCA2 patient");

        Patient patient = patientHelper.toPatient(
                "1000", "u1000",
                new GregorianCalendar(1990, Calendar.FEBRUARY, 11).getTime(),
                "female",
                new Code("test-code-system", "test-religion-code", "test-religion")
        );

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BRCA_1_GENE_MUTATION_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(1, result.size());
        assertTrue(result.containsKey(NccnRule.BC_GENETIC_MUTATION_IN_FHX.getCode()));
    }



    @Test
    public void testEvaluate3DxWithMom3DxSisters() {
        log.info("Validate Mom with 3 Dx does not trigger HAS_3_RELATIVES_WITH_CA_FHX family member rule");

        Patient patient = patientHelper.toPatient(
                "1000", "u1000",
                new GregorianCalendar(1990, Calendar.FEBRUARY, 11).getTime(),
                "female",
                new Code("test-code-system", "test-religion-code", "test-religion")
        );

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx2);

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(0, result.size());
    }

    @Test
    public void testEvaluate2_CO_Cancers() {

        // Looks like a bug - test
        // CANCER_STOMACH,MATERNAL_GRANDFATHER,73
        // CANCER_COLON,MATERNAL_AUNT,60

        log.info("Validate 3 sisters triggers HAS_3_RELATIVES_WITH_CA_FHX family member rule");

        Patient patient = patientHelper.toPatient(
                "1000", "u1000",
                new GregorianCalendar(1990, Calendar.FEBRUARY, 11).getTime(),
                "female",
                new Code("test-code-system", "test-religion-code", "test-religion")
        );

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MATERNAL_GRANDFATHER_CODE),
                getEhrCode(NccnTerminologyBinding.MATERNAL_GRANDFATHER_CODE),
                getCode(NccnTerminologyBinding.CANCER_STOMACH_CODE ),
                getEhrCode(NccnTerminologyBinding.CANCER_STOMACH_CODE ),
                "CODED_FHX",
                73,
                null, null);

        fhxList.add(fhx1);

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MATERNAL_AUNT_CODE),
                getEhrCode(NccnTerminologyBinding.MATERNAL_AUNT_CODE),
                getCode(NccnTerminologyBinding.COLON_CANCER_CODE),
                getEhrCode(NccnTerminologyBinding.COLON_CANCER_CODE),
                "CODED_FHX",
                60,
                null, null);

        fhxList.add(fhx2);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(0, result.size());
    }


    @Test
    public void testEvaluate3DxWith3Sisters() {
        log.info("Validate 3 sisters triggers HAS_3_RELATIVES_WITH_CA_FHX family member rule");

        Patient patient = patientHelper.toPatient(
                "1000", "u1000",
                new GregorianCalendar(1990, Calendar.FEBRUARY, 11).getTime(),
                "female",
                new Code("test-code-system", "test-religion-code", "test-religion")
        );

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.SISTER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.SISTER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx2);

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.SISTER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(1, result.size());
        assertTrue(result.containsKey(NccnRule.BC_3_RELATIVES_WITH_FHX_CA_CRIT.getCode()));
    }

    @Test
    public void testEvaluate3DxWithMother2Sisters() {
        log.info("Validate mother and 2 sisters with Dx does trigger HAS_3_RELATIVES_WITH_CA_FHX family member rule");

        Patient patient = patientHelper.toPatient(
                "1000", "u1000",
                new GregorianCalendar(1990, Calendar.FEBRUARY, 11).getTime(),
                "female",
                new Code("test-code-system", "test-religion-code", "test-religion")
        );

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.SISTER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx2);

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.SISTER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(1, result.size());
        assertTrue(result.containsKey(NccnRule.BC_3_RELATIVES_WITH_FHX_CA_CRIT.getCode()));
    }

    @Test
    public void testEvaluate3DxWith3SingularRelatives() {
        log.info("Validate 3 singular relatives triggers HAS_3_RELATIVES_WITH_CA_FHX family member rule");

        Patient patient = patientHelper.toPatient(
                "1000", "u1000",
                new GregorianCalendar(1990, Calendar.FEBRUARY, 11).getTime(),
                "female",
                new Code("test-code-system", "test-religion-code", "test-religion")
        );

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.PATERNAL_GRANDMOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx2);

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.PATERNAL_GRANDFATHER_CODE),
                getCode(NccnTerminologyBinding.PANCREATIC_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(1, result.size());
        assertTrue(result.containsKey(NccnRule.BC_PANCREATIC_CA_FHX_CRIT.getCode()));
    }

    @Test
    public void testEvaluate3DxWithMotherFather() {
        log.info("Validate 3 singular relatives triggers HAS_3_RELATIVES_WITH_CA_FHX family member rule");

        Patient patient = patientHelper.toPatient(
                "1000", "u1000",
                new GregorianCalendar(1990, Calendar.FEBRUARY, 11).getTime(),
                "female",
                new Code("test-code-system", "test-religion-code", "test-religion")
        );

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                new Code("http://epic.com/uhealth/clarity/zc_relation", "1", "MOTHER"),
                new Code("http://epic.com/uhealth/clarity/zc_medical_hx", "5", "CANCER, BREAST"),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "101", new Date(), "1000",
                new Code("http://epic.com/uhealth/clarity/zc_relation", "5", "MATERNAL GRANDMOTHER"),
                new Code("http://epic.com/uhealth/clarity/zc_medical_hx", "5", "CANCER, BREAST"),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx2);

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "102", new Date(), "1000",
                new Code("http://epic.com/uhealth/clarity/zc_relation", "2", "FATHER"),
                new Code("http://epic.com/uhealth/clarity/zc_medical_hx", "8", "CANCER, PROSTATE"),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(0, result.size());
    }

    @Test
    public void testEvaluate3DxWith2Mother1Sister() {
        log.info("Validate mother with 2 Dx and 1 sister does not trigger HAS_3_RELATIVES_WITH_CA_FHX family member rule");

        Patient patient = patientHelper.toPatient(
                "1000", "u1000",
                new GregorianCalendar(1990, Calendar.FEBRUARY, 11).getTime(),
                "female",
                new Code("test-code-system", "test-religion-code", "test-religion")
        );

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        FamilyMemberHistory fhx2 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.SISTER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx2);

        FamilyMemberHistory fhx3 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx3);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(0, result.size());
    }

    @Test
    public void testEvaluateJewish() {
        log.info("Validate jewish with mom with BC");

        Patient patient = patientHelper.toPatient(
                "1000", "u1000",
                new GregorianCalendar(1990, Calendar.FEBRUARY, 11).getTime(),
                "female",
                getCode(NccnTerminologyBinding.JEWISH_CODE)
        );

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.BREAST_CANCER_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(1, result.size());
        assertTrue(result.containsKey(NccnRule.BC_JEWISH_WITH_CA_FHX_CRIT.getCode()));
    }

    @Test
    public void testEvaluateJewishNoDx() {
        log.info("Validate jewish with no relevant dx");

        Patient patient = patientHelper.toPatient(
                "1000", "u1000",
                new GregorianCalendar(1990, Calendar.FEBRUARY, 11).getTime(),
                "female",
                getCode(NccnTerminologyBinding.JEWISH_CODE)
        );

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        FamilyMemberHistory fhx1 = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "1000",
                getCode(NccnTerminologyBinding.MOTHER_CODE),
                getCode(NccnTerminologyBinding.CANCER_KIDNEY_CODE),
                "CODED_FHX",
                null,
                null, null);

        fhxList.add(fhx1);

        Map<String, List<FamilyMemberHistory>> result = rules.evaluate(patient, fhxList);
        assertEquals(0, result.size());
    }

    private Code getCode(ITerminologyKey key) {
        Code code = codeService.getCanonicalCode(key);

        if (code == null) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "NO CODE for terminology key: " + key);
            return null;
        }

        return code;
    }

    private Code getEhrCode(ITerminologyKey key) {

        Set<Code> codeSet = codeService.getEhrCodeSet(key);

        if (codeSet == null || codeSet.size() == 0) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "NO CODES for terminology key: " + key);
            return null;
        }

        for (Code code: codeSet) {
            return code; // return first if more than one
        }

        return null; // should never get here.
    }

    private String getCodeSystem(ITerminologyKey key) {
        return codeService.getCodeSystem(key);
    }

    private String getUri(ITerminologyKey key) {
        return codeService.getUri(key);
    }

}

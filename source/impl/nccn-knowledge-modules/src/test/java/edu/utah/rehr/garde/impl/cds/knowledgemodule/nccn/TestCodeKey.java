package edu.utah.rehr.garde.impl.cds.knowledgemodule.nccn;

import edu.utah.rehr.garde.domain.core.terminology.ITerminologyKey;
import edu.utah.rehr.garde.domain.core.terminology.TerminologyEntityType;

public enum TestCodeKey implements ITerminologyKey {

    // From FamilyHxHelper
    EHR_PAT_ID_CODE_SYSTEM              (TerminologyEntityType.URI, "EHR_PAT_ID_CODE_SYSTEM")
    ,EHR_MRN_CODE_SYSTEM             (TerminologyEntityType.URI, "EHR_MRN_CODE_SYSTEM")
    ,HL7_RELIGION_EXTENSION_URI    (TerminologyEntityType.URI, "HL7_RELIGION_EXTENSION_URI")
    ,MOTHER_CODE            (TerminologyEntityType.CODE, "MOTHER")
    ,FATHER_CODE            (TerminologyEntityType.CODE, "FATHER")

    ,CATHOLIC_CODE          (TerminologyEntityType.CODE,"ROMAN_CATHOLIC_CHURCH")
    ,JEWISH_CODE            (TerminologyEntityType.CODE, "JEWISH")

    ,APC_GENE_MUTATION(TerminologyEntityType.CODE, "APC_GENE_MUTATION")
    ,ATM_GENE_MUTATION(TerminologyEntityType.CODE, "ATM_GENE_MUTATION")
    ,AUNT(TerminologyEntityType.CODE, "AUNT")
    ,BRCA_1_GENE_MUTATION(TerminologyEntityType.CODE, "BRCA_1_GENE_MUTATION")
    ,BRCA_2_GENE_MUTATION(TerminologyEntityType.CODE, "BRCA_2_GENE_MUTATION")
    ,BROTHER(TerminologyEntityType.CODE, "BROTHER")
//    ,CANCER(TerminologyEntityType.CODE, "CANCER")
    ,CANCER_BLADDER(TerminologyEntityType.CODE, "CANCER_BLADDER")
    ,CANCER_BRAIN(TerminologyEntityType.CODE, "CANCER_BRAIN")
    ,CANCER_BREAST(TerminologyEntityType.CODE, "CANCER_BREAST")
    ,CANCER_COLON(TerminologyEntityType.CODE, "CANCER_COLON")
    ,CANCER_KIDNEY(TerminologyEntityType.CODE, "CANCER_KIDNEY")
    ,CANCER_OVARIAN(TerminologyEntityType.CODE, "CANCER_OVARIAN")
    ,CANCER_PANCREAS(TerminologyEntityType.CODE, "CANCER_PANCREAS")
    ,CANCER_PROSTATE(TerminologyEntityType.CODE, "CANCER_PROSTATE")
    ,CANCER_RECTAL(TerminologyEntityType.CODE, "CANCER_RECTAL")
    ,CANCER_SMALL_INTESTINE(TerminologyEntityType.CODE, "CANCER_SMALL_INTESTINE")
    ,CANCER_STOMACH(TerminologyEntityType.CODE, "CANCER_STOMACH")
    ,CANCER_URETHRAL(TerminologyEntityType.CODE, "CANCER_URETHRAL")
    ,CANCER_UTERINE_OR_ENDOMETRIAL(TerminologyEntityType.CODE, "CANCER_UTERINE_OR_ENDOMETRIAL")
    ,CDH1_GENE_MUTATION(TerminologyEntityType.CODE, "CDH1_GENE_MUTATION")
    ,CHEK2_GENE_MUTATION(TerminologyEntityType.CODE, "CHEK2_GENE_MUTATION")
    ,DAUGHTER(TerminologyEntityType.CODE, "DAUGHTER")
//    ,DIVORCED(TerminologyEntityType.CODE, "DIVORCED")
    ,EPCAM_GENE_MUTATION(TerminologyEntityType.CODE, "EPCAM_GENE_MUTATION")
    ,FAMILIAL_ADENOMATOUS_POLYPOSIS(TerminologyEntityType.CODE, "FAMILIAL_ADENOMATOUS_POLYPOSIS")
    ,FATHER(TerminologyEntityType.CODE, "FATHER")
    ,HEREDITARY_NONPOLYPOSIS_COLON_CANCER(TerminologyEntityType.CODE, "HEREDITARY_NONPOLYPOSIS_COLON_CANCER")
//    ,LEGALLY_SEPARATED(TerminologyEntityType.CODE, "LEGALLY_SEPARATED")
//    ,LIFE_PARTNER_OR_DOMESTIC_PARTNER(TerminologyEntityType.CODE, "LIFE_PARTNER_OR_DOMESTIC_PARTNER")
    ,LYNCH_SYNDROME(TerminologyEntityType.CODE, "LYNCH_SYNDROME")
//    ,MARRIED(TerminologyEntityType.CODE, "MARRIED")
    ,MATERNAL_AUNT(TerminologyEntityType.CODE, "MATERNAL_AUNT")
    ,MATERNAL_GRANDFATHER(TerminologyEntityType.CODE, "MATERNAL_GRANDFATHER")
    ,MATERNAL_GRANDMOTHER(TerminologyEntityType.CODE, "MATERNAL_GRANDMOTHER")
    ,MATERNAL_UNCLE(TerminologyEntityType.CODE, "MATERNAL_UNCLE")
    ,MLH1_GENE_MUTATION(TerminologyEntityType.CODE, "MLH1_GENE_MUTATION")
    ,MOTHER(TerminologyEntityType.CODE, "MOTHER")
    ,MSH2_GENE_MUTATION(TerminologyEntityType.CODE, "MSH2_GENE_MUTATION")
    ,MSH6_GENE_MUTATION(TerminologyEntityType.CODE, "MSH6_GENE_MUTATION")
    ,MUTYH_GENE_MUTATION(TerminologyEntityType.CODE, "MUTYH_GENE_MUTATION")
    ,PALB2_GENE_MUTATION(TerminologyEntityType.CODE, "PALB2_GENE_MUTATION")
    ,PATERNAL_AUNT(TerminologyEntityType.CODE, "PATERNAL_AUNT")
    ,PATERNAL_GRANDFATHER(TerminologyEntityType.CODE, "PATERNAL_GRANDFATHER")
    ,PATERNAL_GRANDMOTHER(TerminologyEntityType.CODE, "PATERNAL_GRANDMOTHER")
    ,PATERNAL_UNCLE(TerminologyEntityType.CODE, "PATERNAL_UNCLE")
    ,PMS2_GENE_MUTATION(TerminologyEntityType.CODE, "PMS2_GENE_MUTATION")
    ,PTEN_GENE_MUTATION(TerminologyEntityType.CODE, "PTEN_GENE_MUTATION")
//    ,RELATIVE(TerminologyEntityType.CODE, "RELATIVE")
    ,SERRATED_POLYPOSIS(TerminologyEntityType.CODE, "SERRATED_POLYPOSIS")
//    ,SINGLE(TerminologyEntityType.CODE, "SINGLE")
    ,SISTER(TerminologyEntityType.CODE, "SISTER")
    ,SON(TerminologyEntityType.CODE, "SON")
    ,TP53_GENE_MUTATION(TerminologyEntityType.CODE, "TP53_GENE_MUTATION")
    ,UNCLE(TerminologyEntityType.CODE, "UNCLE")
//    ,UNKNOWN_MARITAL_STATUS(TerminologyEntityType.CODE, "UNKNOWN_MARITAL_STATUS")
//    ,WIDOWED(TerminologyEntityType.CODE, "WIDOWED")
        ;

    private final String key;
    private final TerminologyEntityType entityType;

    TestCodeKey(TerminologyEntityType entityType, String key) {
        this.entityType = entityType;
        this.key=key;
    }

    @Override
    public TerminologyEntityType getEntityType() {
        return this.entityType;
    }

    @Override
    public String getKey() {
        return this.key;
    }
}



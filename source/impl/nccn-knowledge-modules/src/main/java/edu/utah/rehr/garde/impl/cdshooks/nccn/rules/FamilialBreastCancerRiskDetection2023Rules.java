package edu.utah.rehr.garde.impl.cdshooks.nccn.rules;

import edu.utah.rehr.garde.domain.core.model.Nccn2023Rule;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.FamilyMemberHistoryHelperR4;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.PatientHelperR4;
import edu.utah.rehr.garde.domain.core.terminology.Code;
import edu.utah.rehr.garde.domain.core.terminology.binding.NccnTerminologyBinding;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import lombok.extern.java.Log;
import org.hl7.fhir.r4.model.FamilyMemberHistory;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Quantity;
import org.hl7.fhir.r4.model.Range;

import java.util.*;

@Log
public class FamilialBreastCancerRiskDetection2023Rules {

    private final FamilyMemberHistoryHelperR4 familyMemberHistoryHelper;
    private final PatientHelperR4 patientHelper;
    private final ICodeService codeService;

    public FamilialBreastCancerRiskDetection2023Rules(ICodeService codeService, FamilyMemberHistoryHelperR4 familyMemberHistoryHelper, PatientHelperR4 patientHelper) {
        this.codeService = codeService;
        this.familyMemberHistoryHelper = familyMemberHistoryHelper;
        this.patientHelper = patientHelper;
    }

    public Map<String,List<FamilyMemberHistory>> evaluate(Patient patient, List<FamilyMemberHistory> fhxList) {

        int MAX_BREAST_CANCER_AGE = 50;

        // Map for tracking which FHx were associated with met rule criteria
        Map<String,List<FamilyMemberHistory>> metRuleResultMap = new HashMap<>();

        // Lists for tracking diagnosis counts and met rule maps
        List<FamilyMemberHistory> familyMembersWithBreastOrProstateCancer = new ArrayList<>();
        List<FamilyMemberHistory> hasGeneticMutationInFhxList = new ArrayList<>();
        List<FamilyMemberHistory> hasBreastCancerAge_LTE_Max_FhxList = new ArrayList<>();
        List<FamilyMemberHistory> hasOvarianCaFhxList = new ArrayList<>();
        List<FamilyMemberHistory> hasPancreaticCaFhxList = new ArrayList<>();
        List<FamilyMemberHistory> isJewishAndCaFhxList = new ArrayList<>();
        List<FamilyMemberHistory> hasMaleBreastCaFhxList = new ArrayList<>();

        Set<Code> religionCodes = patientHelper.getReligionCodeSet(patient);

        // Run all rules for each family history - track results using the map
        for (FamilyMemberHistory fhx : fhxList) {

            Set<Code> relativeCodes = familyMemberHistoryHelper.getRelativeCodings(fhx);
            Set<Code> dxCodes = familyMemberHistoryHelper.getConditionCodings(fhx);
            Integer ageOfOnset = familyMemberHistoryHelper.getAgeOfOnset(fhx);

            // Validate FHx Dx sex and diagnosis are nonsensical - skip males with ovarian cancer and females with prostate cancer
            if (familyMemberHistoryHelper.isNonsensicalDiagnosisAndSex(fhx)) {
                continue;
            }

            if (ageOfOnset == null) { // check for range, if there is a range, set onset to minimum of range
                Range ageRange = familyMemberHistoryHelper.getOnsetRange(fhx);
                if (ageRange != null) {
                    Quantity lowQuantity = ageRange.getLow();
                    ageOfOnset = Integer.parseInt(lowQuantity.getValue().toPlainString());
                }
            }

            if (codeService.intersects(NccnTerminologyBinding.JEWISH_CODE, religionCodes)
                    && codeService.intersects(NccnTerminologyBinding.BC_JEWISH_CA_RISK_2023_CODE_SET, dxCodes)) {

                isJewishAndCaFhxList.add(fhx);
                metRuleResultMap.put(Nccn2023Rule.BC_JEWISH_WITH_CA_FHX_CRIT_2023.getCode(),isJewishAndCaFhxList);
            }

            if (codeService.intersects(NccnTerminologyBinding.BC_GENE_MUTATION_2023_CODE_SET,dxCodes)) {
                hasGeneticMutationInFhxList.add(fhx);
                metRuleResultMap.put(Nccn2023Rule.BC_GENETIC_MUTATION_IN_FHX_2023.getCode(),hasGeneticMutationInFhxList);
            }

            if (codeService.intersects(NccnTerminologyBinding.BREAST_CANCER_CODE, dxCodes)
                    && familyMemberHistoryHelper.isFirstOrSecondDegreeRelative(relativeCodes)
                    && (ageOfOnset != null && ageOfOnset <= MAX_BREAST_CANCER_AGE)) {

                hasBreastCancerAge_LTE_Max_FhxList.add(fhx);
                metRuleResultMap.put(Nccn2023Rule.BC_HAS_BREAST_CA_AGE_LTE_50_FHX_CRIT_2023.getCode(), hasBreastCancerAge_LTE_Max_FhxList);
            }

            if ((codeService.intersects(NccnTerminologyBinding.BREAST_CANCER_CODE, dxCodes)
                            || codeService.intersects(NccnTerminologyBinding.PROSTATE_CANCER_CODE, dxCodes))
                    && familyMemberHistoryHelper.isFirstOrSecondOrThirdDegreeRelative(relativeCodes)) {
                familyMembersWithBreastOrProstateCancer.add(fhx);
            }

            if (codeService.intersects(NccnTerminologyBinding.BREAST_CANCER_CODE, dxCodes)
                    && familyMemberHistoryHelper.isFirstOrSecondOrThirdDegreeRelative(relativeCodes)
                && codeService.intersects(NccnTerminologyBinding.MALE_RELATIVE_2023_CODE_SET, relativeCodes)) {

                hasMaleBreastCaFhxList.add(fhx);
                metRuleResultMap.put(Nccn2023Rule.BC_MALE_BREAST_CA_FHX_CRIT_2023.getCode(), hasMaleBreastCaFhxList);
            }

            if (codeService.intersects(NccnTerminologyBinding.OVARIAN_CANCER_CODE, dxCodes)
                    && familyMemberHistoryHelper.isFirstOrSecondDegreeRelative(relativeCodes)) {

                hasOvarianCaFhxList.add(fhx);
                metRuleResultMap.put(Nccn2023Rule.BC_OVARIAN_CA_FHX_CRIT_2023.getCode(), hasOvarianCaFhxList);
            }

            if (codeService.intersects(NccnTerminologyBinding.PANCREATIC_CANCER_CODE, dxCodes)
                    && familyMemberHistoryHelper.isFirstDegreeRelative(relativeCodes)) {

                hasPancreaticCaFhxList.add(fhx);
                metRuleResultMap.put(Nccn2023Rule.BC_FIRST_DEG_RELATIVE_PANCREATIC_CA_FHX_CRIT_2023.getCode(), hasPancreaticCaFhxList);
            }

        }

        Map<NccnTerminologyBinding, List<FamilyMemberHistory>> threeOnSameSide = familyMemberHistoryHelper.getCountOnTheSameSide(3, familyMembersWithBreastOrProstateCancer, 3);

        for (NccnTerminologyBinding side: threeOnSameSide.keySet()) {
            List<FamilyMemberHistory> fhxs = threeOnSameSide.get(side);
            metRuleResultMap.put(Nccn2023Rule.BC_3_RELATIVES_WITH_BREAST_OR_PROSTATE_CA_FHX_CRIT_2023.getCode(), fhxs);
            break;
        }

        return metRuleResultMap;
    }

}

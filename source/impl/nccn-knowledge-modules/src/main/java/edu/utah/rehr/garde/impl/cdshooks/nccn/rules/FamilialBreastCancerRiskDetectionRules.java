package edu.utah.rehr.garde.impl.cdshooks.nccn.rules;

import edu.utah.rehr.garde.domain.core.model.NccnRule;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.FamilyMemberHistoryHelperR4;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.PatientHelperR4;
import edu.utah.rehr.garde.domain.core.terminology.Code;
import edu.utah.rehr.garde.domain.core.terminology.binding.NccnTerminologyBinding;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import lombok.extern.java.Log;
import org.hl7.fhir.r4.model.FamilyMemberHistory;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Quantity;
import org.hl7.fhir.r4.model.Range;

import java.util.*;

@Log
public class  FamilialBreastCancerRiskDetectionRules {

    private final FamilyMemberHistoryHelperR4 familyMemberHistoryHelper;
    private final PatientHelperR4 patientHelper;
    private final ICodeService codeService;

    public FamilialBreastCancerRiskDetectionRules(ICodeService codeService, FamilyMemberHistoryHelperR4 familyMemberHistoryHelper, PatientHelperR4 patientHelper) {
        this.codeService = codeService;
        this.familyMemberHistoryHelper = familyMemberHistoryHelper;
        this.patientHelper = patientHelper;
    }

    public Map<String,List<FamilyMemberHistory>> evaluate(Patient patient, List<FamilyMemberHistory> fhxList) {

        Integer MAX_BREAST_CANCER_AGE = 45;

        // Map for tracking which FHx were associated with met rule criteria
        Map<String,List<FamilyMemberHistory>> metRuleResultMap = new HashMap<>();

        // Lists for tracking diagnosis counts and met rule maps
        List<FamilyMemberHistory> familyMembersWithCancer = new ArrayList<>();
        List<FamilyMemberHistory> hasGeneticMutationInFhxList = new ArrayList<>();
        List<FamilyMemberHistory> hasBreastCaFhxList = new ArrayList<>();
        List<FamilyMemberHistory> hasOvarianCaFhxList = new ArrayList<>();
        List<FamilyMemberHistory> hasPancreaticCaFhxList = new ArrayList<>();
        List<FamilyMemberHistory> isJewishAndCaFhxList = new ArrayList<>();
        List<FamilyMemberHistory> hasMaleBreastCaFhxList = new ArrayList<>();

        Set<Code> religionCodes = patientHelper.getReligionCodeSet(patient);

        // Run all rules for each family history - track results using the map
        for (FamilyMemberHistory fhx : fhxList) {

            String fhxRecordId = familyMemberHistoryHelper.getEhrFhxKey(fhx);
            Set<Code> relativeCodes = familyMemberHistoryHelper.getRelativeCodings(fhx);
            Boolean isMaleRelative = codeService.intersects(NccnTerminologyBinding.MALE_RELATIVE_CODE_SET, relativeCodes);
            Boolean isFemaleRelative = codeService.intersects(NccnTerminologyBinding.FEMALE_RELATIVE_CODE_SET, relativeCodes);
            Set<Code> dxCodes = familyMemberHistoryHelper.getConditionCodings(fhx);
            Integer ageOfOnset = familyMemberHistoryHelper.getAgeOfOnset(fhx);

            // Validate FHx Dx sex and diagnosis are sensical - no males with ovarian cancer, no females with prostate cancer
            if (isFemaleRelative == Boolean.TRUE && codeService.intersects(NccnTerminologyBinding.MALE_DX_CODE_SET, dxCodes)) {
                log.warning("IGNORING - FEMALE HAD MALE DX " + fhxRecordId);
                continue;
            }

            if (isMaleRelative == Boolean.TRUE && codeService.intersects(NccnTerminologyBinding.FEMALE_DX_CODE_SET, dxCodes)) {
                log.warning("IGNORING - MALE HAD FEMALE DX " + fhxRecordId);
                continue;
            }

            if (ageOfOnset == null) { // check for range, if there is a range, set onset to minimum of range
                Range ageRange = familyMemberHistoryHelper.getOnsetRange(fhx);
                if (ageRange != null) {
                    Quantity lowQuantity = ageRange.getLow();
                    Integer low = Integer.valueOf(lowQuantity.getValue().toPlainString());
                    ageOfOnset = low;
                }
            }

            if (codeService.intersects(NccnTerminologyBinding.JEWISH_CODE, religionCodes)
                    && codeService.intersects(NccnTerminologyBinding.BC_JEWISH_CA_RISK_CODE_SET, dxCodes)) {

                isJewishAndCaFhxList.add(fhx);
                metRuleResultMap.put(NccnRule.BC_JEWISH_WITH_CA_FHX_CRIT.getCode(),isJewishAndCaFhxList);
            }

            if (codeService.intersects(NccnTerminologyBinding.BC_GENETIC_MUTATION_CODE_SET,dxCodes)) {
                hasGeneticMutationInFhxList.add(fhx);
                metRuleResultMap.put(NccnRule.BC_GENETIC_MUTATION_IN_FHX.getCode(),hasGeneticMutationInFhxList);
            }

            if (codeService.intersects(NccnTerminologyBinding.BREAST_CANCER_CODE, dxCodes)
                    && familyMemberHistoryHelper.isFirstOrSecondDegreeRelative(relativeCodes)
                    && familyMemberHistoryHelper.isAgeOfOnsetLTE(ageOfOnset, MAX_BREAST_CANCER_AGE)) {

                hasBreastCaFhxList.add(fhx);
                metRuleResultMap.put(NccnRule.BC_HAS_BREAST_CA_AGE_LTE_45_FHX_CRIT.getCode(), hasBreastCaFhxList);
            }

            if ((codeService.intersects(NccnTerminologyBinding.BREAST_CANCER_CODE, dxCodes)
                || codeService.intersects(NccnTerminologyBinding.PROSTATE_CANCER_CODE, dxCodes))
                    && familyMemberHistoryHelper.isFirstOrSecondDegreeRelative(relativeCodes)) {

                familyMembersWithCancer.add(fhx);
            }

            if (codeService.intersects(NccnTerminologyBinding.BREAST_CANCER_CODE, dxCodes)
                && codeService.intersects(NccnTerminologyBinding.MALE_RELATIVE_CODE_SET, relativeCodes)) {

                hasMaleBreastCaFhxList.add(fhx);
                metRuleResultMap.put(NccnRule.BC_MALE_BREAST_CA_FHX_CRIT.getCode(), hasMaleBreastCaFhxList);
            }

            if (codeService.intersects(NccnTerminologyBinding.OVARIAN_CANCER_CODE, dxCodes)
                    && familyMemberHistoryHelper.isFirstOrSecondDegreeRelative(relativeCodes)) {

                hasOvarianCaFhxList.add(fhx);
                metRuleResultMap.put(NccnRule.BC_OVARIAN_CA_FHX_CRIT.getCode(), hasOvarianCaFhxList);
            }

            if (codeService.intersects(NccnTerminologyBinding.PANCREATIC_CANCER_CODE, dxCodes)
                    && familyMemberHistoryHelper.isFirstOrSecondDegreeRelative(relativeCodes)) {

                hasPancreaticCaFhxList.add(fhx);
                metRuleResultMap.put(NccnRule.BC_PANCREATIC_CA_FHX_CRIT.getCode(),hasPancreaticCaFhxList);
            }

        }

        Map<NccnTerminologyBinding, List<FamilyMemberHistory>> threeOnSameSide = familyMemberHistoryHelper.getCountOnTheSameSide(2, familyMembersWithCancer, 3);
        for (NccnTerminologyBinding side: threeOnSameSide.keySet()) {
            List<FamilyMemberHistory> fhxs = threeOnSameSide.get(side);
            metRuleResultMap.put(NccnRule.BC_3_RELATIVES_WITH_FHX_CA_CRIT.getCode(), familyMembersWithCancer);
            break;
        }

        return metRuleResultMap;
    }

}

package edu.utah.rehr.garde.impl.cdshooks.nccn.rules;

import edu.utah.rehr.garde.domain.core.model.NccnRule;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.FamilyMemberHistoryHelperR4;
import edu.utah.rehr.garde.domain.core.terminology.Code;
import edu.utah.rehr.garde.domain.core.terminology.binding.NccnTerminologyBinding;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import org.hl7.fhir.r4.model.FamilyMemberHistory;
import org.hl7.fhir.r4.model.Quantity;
import org.hl7.fhir.r4.model.Range;

import java.util.*;
import java.util.logging.Logger;

public class FamilialColorectalCancerRiskDetectionRules {

    private final Logger log = Logger.getLogger(this.getClass().getName());

    private final FamilyMemberHistoryHelperR4 familyMemberHistoryHelper;
    private final ICodeService codeService;

    public final Code MOTHER_CODE;
    public final Code FATHER_CODE;
    public final Code PATERNAL_GRANDFATHER_CODE;
    public final Code PATERNAL_GRANDMOTHER_CODE;
    public final Code MATERNAL_GRANDFATHER_CODE;
    public final Code MATERNAL_GRANDMOTHER_CODE;

    public FamilialColorectalCancerRiskDetectionRules(ICodeService codeService, FamilyMemberHistoryHelperR4 familyMemberHistoryHelper) {

        this.codeService = codeService;
        this.familyMemberHistoryHelper = familyMemberHistoryHelper;

        this.MOTHER_CODE = getCode(NccnTerminologyBinding.MOTHER_CODE);
        this.FATHER_CODE = getCode(NccnTerminologyBinding.FATHER_CODE);
        this.PATERNAL_GRANDFATHER_CODE = getCode(NccnTerminologyBinding.PATERNAL_GRANDFATHER_CODE);
        this.PATERNAL_GRANDMOTHER_CODE = getCode(NccnTerminologyBinding.PATERNAL_GRANDMOTHER_CODE);
        this.MATERNAL_GRANDFATHER_CODE = getCode(NccnTerminologyBinding.MATERNAL_GRANDFATHER_CODE);
        this.MATERNAL_GRANDMOTHER_CODE = getCode(NccnTerminologyBinding.MATERNAL_GRANDMOTHER_CODE);

    }

    private Code getCode(NccnTerminologyBinding codeBinding) {
        Code testCode = codeService.getCanonicalCode(codeBinding);
        if (testCode == null) {
            log.severe("REQUIRED CODE " + codeBinding + " NOT FOUND BY codeService.");
        }
        return testCode;
    }

    public Map<String,List<FamilyMemberHistory>> evaluate(List<FamilyMemberHistory> fhxList) {

        // Map for tracking which FHx were associated with met rule criteria
        Map<String,List<FamilyMemberHistory>> metRuleResultMap = new HashMap<>();

        // Lists for tracking diagnosis counts and met rule maps
        List<FamilyMemberHistory> familyMembersWithCancer = new ArrayList<>();
        List<FamilyMemberHistory> geneticFhx = new ArrayList<>();
        List<FamilyMemberHistory> coCancerLTE50Fhx = new ArrayList<>();
        List<FamilyMemberHistory> endoCancerLTE50Fhx = new ArrayList<>();


        // Run all rules for each family history record - track results using maps
        for (FamilyMemberHistory fhx : fhxList) {

            String fhxRecordId = familyMemberHistoryHelper.getEhrFhxKey(fhx);

            Set<Code> relativeCodes = familyMemberHistoryHelper.getRelativeCodings(fhx);
            Boolean isMaleRelative = codeService.intersects(NccnTerminologyBinding.MALE_RELATIVE_CODE_SET, relativeCodes);
            Boolean isFemaleRelative = codeService.intersects(NccnTerminologyBinding.FEMALE_RELATIVE_CODE_SET, relativeCodes);
            Set<Code> dxCodes = familyMemberHistoryHelper.getConditionCodings(fhx);
            Integer ageOfOnset = familyMemberHistoryHelper.getAgeOfOnset(fhx);

            // Validate FHx Dx is gender correct - no males with ovarian cancer, no females with prostate cancer
            if (isFemaleRelative == Boolean.TRUE && codeService.intersects(NccnTerminologyBinding.MALE_DX_CODE_SET, dxCodes)) {
                log.warning("IGNORING - FEMALE HAD MALE DX " + fhxRecordId);
                continue;
            }

            if (isMaleRelative == Boolean.TRUE && codeService.intersects(NccnTerminologyBinding.FEMALE_DX_CODE_SET, dxCodes)) {
                log.warning("IGNORING - MALE HAD FEMALE DX " + fhxRecordId);
                continue;
            }

            if (ageOfOnset == null) { // check for range, if there is a range, set onset to minimum of range
                Range ageRange = familyMemberHistoryHelper.getOnsetRange(fhx);
                if (ageRange != null) {
                    Quantity lowQuantity = ageRange.getLow();
                    ageOfOnset = Integer.parseInt(lowQuantity.getValue().toPlainString());
                }
            }

            if (codeService.intersects(NccnTerminologyBinding.COLORECTAL_CANCER_MUTATION_RISK_CODE_SET, dxCodes)) {

                geneticFhx.add(fhx);
                metRuleResultMap.put(NccnRule.CO_GENETIC_FHX_CRIT.getCode(), geneticFhx);
            }

            if (codeService.intersects(NccnTerminologyBinding.COLON_CANCER_CODE, dxCodes)
                    && familyMemberHistoryHelper.isFirstOrSecondDegreeRelative(relativeCodes)
                    && ageOfOnset != null && ageOfOnset <= 50) {

                coCancerLTE50Fhx.add(fhx);
                metRuleResultMap.put(NccnRule.CO_COLON_CA_AGE_LT_50_FHX_CRIT.getCode(), coCancerLTE50Fhx);
            }

            if (codeService.intersects(NccnTerminologyBinding.ENDOMETRIAL_CANCER_CODE, dxCodes)
                    && familyMemberHistoryHelper.isFirstOrSecondDegreeRelative(relativeCodes)
                    && ageOfOnset != null && ageOfOnset <= 50) {

                endoCancerLTE50Fhx.add(fhx);
                metRuleResultMap.put(NccnRule.CO_ENDOMETRIAL_CA_AGE_LT_50_FHX_CRIT.getCode(), endoCancerLTE50Fhx);
            }

            if ( codeService.intersects(NccnTerminologyBinding.COLORECTAL_CANCER_DX_RISK_CODE_SET, dxCodes) ) {
                familyMembersWithCancer.add(fhx);
            }
        }

        Map<NccnTerminologyBinding, List<FamilyMemberHistory>> threeOnSameSide = familyMemberHistoryHelper.getCountOnTheSameSide(2, familyMembersWithCancer, 3);

        for (NccnTerminologyBinding side: threeOnSameSide.keySet()) {
            List<FamilyMemberHistory> fhxs = threeOnSameSide.get(side);
            if (containsUterineOrColonCancer(fhxs)){
                metRuleResultMap.put(NccnRule.CO_3_RELATIVES_WITH_CA_FHX_CRIT.getCode(), fhxs);
                break;
            }
        }

        return metRuleResultMap;
    }

    private boolean containsUterineOrColonCancer(List<FamilyMemberHistory> familyMembersWithCancer) {

        for (FamilyMemberHistory fhx:familyMembersWithCancer) {

            Set<Code> dxCodes = familyMemberHistoryHelper.getConditionCodings(fhx);
            if (codeService.intersects(NccnTerminologyBinding.COLON_CANCER_CODE, dxCodes)
            || codeService.intersects(NccnTerminologyBinding.UTERINE_CANCER_CODE, dxCodes))
            {
                return true;
            }
        }

        return false;
    }

}

package edu.utah.rehr.garde.impl.cdshooks.nccn.rules;

import edu.utah.rehr.garde.domain.core.model.Nccn2023Rule;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.FamilyMemberHistoryHelperR4;
import edu.utah.rehr.garde.domain.core.terminology.Code;
import edu.utah.rehr.garde.domain.core.terminology.binding.NccnTerminologyBinding;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import org.hl7.fhir.r4.model.FamilyMemberHistory;
import org.hl7.fhir.r4.model.Quantity;
import org.hl7.fhir.r4.model.Range;

import java.util.*;
import java.util.logging.Logger;

public class FamilialColorectalCancerRiskDetection2023Rules {

    private final Logger log = Logger.getLogger(this.getClass().getName());

    private final FamilyMemberHistoryHelperR4 familyMemberHistoryHelper;
    private final ICodeService codeService;

    public final Code MOTHER_CODE;
    public final Code FATHER_CODE;

    public FamilialColorectalCancerRiskDetection2023Rules(ICodeService codeService, FamilyMemberHistoryHelperR4 familyMemberHistoryHelper) {

        this.codeService = codeService;
        this.familyMemberHistoryHelper = familyMemberHistoryHelper;

        this.MOTHER_CODE = getCode(NccnTerminologyBinding.MOTHER_CODE);
        this.FATHER_CODE = getCode(NccnTerminologyBinding.FATHER_CODE);
    }

    private Code getCode(NccnTerminologyBinding codeBinding) {
        Code testCode = codeService.getCanonicalCode(codeBinding);
        if (testCode == null) {
            log.severe("REQUIRED CODE " + codeBinding + " NOT FOUND BY codeService.");
        }
        return testCode;
    }

    public Map<String,List<FamilyMemberHistory>> evaluate(List<FamilyMemberHistory> fhxList) {

        // Map for tracking which FHx were associated with met rule criteria
        Map<String,List<FamilyMemberHistory>> metRuleResultMap = new HashMap<>();

        // Lists for tracking diagnosis counts and met rule maps
        List<FamilyMemberHistory> familyMembersWithCancer = new ArrayList<>();
        List<FamilyMemberHistory> geneticFhx = new ArrayList<>();
        List<FamilyMemberHistory> coCancerLTE50Fhx = new ArrayList<>();
        List<FamilyMemberHistory> endoCancerLTE50Fhx = new ArrayList<>();


        // Run all rules for each family history record - track status and results using maps
        for (FamilyMemberHistory fhx : fhxList) {

            Set<Code> relativeCodes = familyMemberHistoryHelper.getRelativeCodings(fhx);
            Set<Code> dxCodes = familyMemberHistoryHelper.getConditionCodings(fhx);
            Integer ageOfOnset = familyMemberHistoryHelper.getAgeOfOnset(fhx);

            // Validate FHx Dx is gender correct - skip males with ovarian cancer and females with prostate cancer
            if (familyMemberHistoryHelper.isNonsensicalDiagnosisAndSex(fhx)) {
                continue;
            }

            if (ageOfOnset == null) { // check for range, if there is a range, set onset to minimum of range
                Range ageRange = familyMemberHistoryHelper.getOnsetRange(fhx);
                if (ageRange != null) {
                    Quantity lowQuantity = ageRange.getLow();
                    ageOfOnset = Integer.parseInt(lowQuantity.getValue().toPlainString());
                }
            }

            if (codeService.intersects(NccnTerminologyBinding.COLORECTAL_CANCER_MUTATION_RISK_CODE_SET, dxCodes)
                && familyMemberHistoryHelper.isFirstOrSecondOrThirdDegreeRelative(relativeCodes)
            ) {
                geneticFhx.add(fhx);
                metRuleResultMap.put(Nccn2023Rule.CO_GENETIC_FHX_CRIT_2023.getCode(), geneticFhx);
            }

            if (codeService.intersects(NccnTerminologyBinding.COLON_CANCER_CODE, dxCodes)
                    && familyMemberHistoryHelper.isFirstDegreeRelative(relativeCodes)
                    && ageOfOnset != null && ageOfOnset <= 50) {

                coCancerLTE50Fhx.add(fhx);
                metRuleResultMap.put(Nccn2023Rule.CO_FIRST_GEN_COLON_CA_AGE_LTE_50_FHX_CRIT_2023.getCode(), coCancerLTE50Fhx);
            }

            if (codeService.intersects(NccnTerminologyBinding.ENDOMETRIAL_CANCER_CODE, dxCodes)
                    && familyMemberHistoryHelper.isFirstDegreeRelative(relativeCodes)
                    && ageOfOnset != null && ageOfOnset <= 50) {

                endoCancerLTE50Fhx.add(fhx);
                metRuleResultMap.put(Nccn2023Rule.CO_FIRST_GEN_ENDOMETRIAL_CA_AGE_LTE_50_FHX_CRIT_2023.getCode(), endoCancerLTE50Fhx);
            }

            if ( codeService.intersects(NccnTerminologyBinding.COLORECTAL_CANCER_DX_RISK_CODE_SET, dxCodes)
                    && familyMemberHistoryHelper.isFirstOrSecondDegreeRelative(relativeCodes)
            ) {
                familyMembersWithCancer.add(fhx);
            }
        }

        Map<NccnTerminologyBinding,List<FamilyMemberHistory>> twoOnSameSide = familyMemberHistoryHelper.getCountOnTheSameSide(2, familyMembersWithCancer, 2); // need 2 within 2 generations

        for (NccnTerminologyBinding side: twoOnSameSide.keySet()) {
            List<FamilyMemberHistory> fhxs = twoOnSameSide.get(side);
            if (containsOneLessThanAge(fhxs,50)) {
                metRuleResultMap.put(Nccn2023Rule.CO_2_RELATIVES_WITH_1_LT_50_CA_FHX_CRIT_2023.getCode(), fhxs);
                break;
            }
        }

        Map<NccnTerminologyBinding,List<FamilyMemberHistory>> threeOnSameSide = familyMemberHistoryHelper.getCountOnTheSameSide(2, familyMembersWithCancer, 3); // need 2 within 2 generations

        for (NccnTerminologyBinding side: threeOnSameSide.keySet()) {
            List<FamilyMemberHistory> fhxs = threeOnSameSide.get(side);
            metRuleResultMap.put(Nccn2023Rule.CO_3_RELATIVES_WITH_CA_FHX_CRIT_2023.getCode(), fhxs);
        }

        return metRuleResultMap;
    }

    private boolean containsOneLessThanAge(List<FamilyMemberHistory> familyMembersWithCancer, int age) {

        for (FamilyMemberHistory fhx:familyMembersWithCancer) {
            if (null != familyMemberHistoryHelper.getAgeOfOnset(fhx)) {
                int thisAge = familyMemberHistoryHelper.getAgeOfOnset(fhx);
                if (thisAge > 0 && thisAge < age) {
                    return true;
                }
            }
        }

        return false;
    }

}

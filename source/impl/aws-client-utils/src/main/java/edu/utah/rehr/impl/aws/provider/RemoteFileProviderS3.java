package edu.utah.rehr.impl.aws.provider;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.provider.IRemoteFileProvider;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Component
public class RemoteFileProviderS3 implements IRemoteFileProvider {

    private Logger log = Logger.getLogger(this.getClass().getName());

    @Autowired
    AmazonS3 s3client;

    /**
     *
     * @param bucketSearchPath contains the bucket name - convention bucketName:fileSearchPath
     * @return
     */
    @Override
    public UResult<List<String>> lsRemoteDirectory(String bucketSearchPath) {

        String[] s3Props = bucketSearchPath.split(":");
        String bucket = s3Props[0];
        String searchPath = s3Props[1];

        log.info("REMOTE DIRECTORY ls " + bucket + ":" + searchPath);

        ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
                .withBucketName(bucket)
                .withPrefix(searchPath)
                .withDelimiter("/")
                ;

        List<String> fileNames = new ArrayList<>();
        ObjectListing objectListing = s3client.listObjects(listObjectsRequest);
        for(S3ObjectSummary os : objectListing.getObjectSummaries()) {
            fileNames.add(os.getKey());
        }

        return UResult.successResult(fileNames);
    }

    @Override
    public UResult<Boolean> remoteFileExists(String remoteFilePath) {

        UResult<List<String>> fileNamesResult = lsRemoteDirectory(remoteFilePath);
        List<String> fileNames = fileNamesResult.getResult();

        return UResult.successResult(fileNames.contains(remoteFilePath));
    }

    @Override
    public UResult<Boolean> deleteRemoteFiles(String remoteFilePath) {

        String[] destinationFileProps = remoteFilePath.split(":");
        String destinationBucket = destinationFileProps[0];
        String destinationFilePath = destinationFileProps[1];

        s3client.deleteObject(
                destinationBucket
                , destinationBucket
        );

        return UResult.successResult(true);
    }

    @Override
    public UResult<Boolean> getRemoteFile(String remoteFilePath, String localFilePath) {

        String[] parts = remoteFilePath.split(":");
        String bucket = parts[0];
        String filePath = parts[1];

        log.info("REMOTE DIRECTORY getRemoteFile " + bucket + ":" + filePath);

        S3Object s3object = s3client.getObject(bucket, filePath);
        S3ObjectInputStream inputStream = s3object.getObjectContent();
        try {
            File newFile = new File(localFilePath);
            FileUtils.copyInputStreamToFile(inputStream, newFile);
        } catch (IOException e) {
            e.printStackTrace();
            return UResult.errorResult(false, e);
        }

        return UResult.successResult(true);
    }

    @Override
    public UResult<Boolean> putOnRemoteFileSystem(String localFilePath, String remoteFilePath) {

        String[] destinationFileProps = remoteFilePath.split(":");
        String destinationBucket = destinationFileProps[0];
        String destinationFilePath = destinationFileProps[1];

        s3client.putObject(
                destinationBucket
                , destinationFilePath
                , new File(localFilePath)
        );

        return UResult.successResult(true);
    }

    @Override
    public UResult<Boolean> moveRemoteFile(String remoteSourceFilePath, String remoteDestinationFilePath) {

        log.info("MOVE REMOTE FILE FROM " + remoteSourceFilePath + " TO " + remoteDestinationFilePath);

        String[] sourceFileProps = remoteSourceFilePath.split(":");
        String sourceBucket = sourceFileProps[0];
        String sourceFilePath = sourceFileProps[1];

        String[] destinationFileProps = remoteDestinationFilePath.split(":");
        String destinationBucket = destinationFileProps[0];
        String destinationFilePath = destinationFileProps[1];

        CopyObjectResult copyObjectResult = s3client.copyObject(
                sourceBucket
                , sourceFilePath
                , destinationBucket
                , destinationFilePath
        );

        s3client.deleteObject(
                sourceBucket
                , sourceFilePath
        );

        return UResult.successResult(true);
    }
}

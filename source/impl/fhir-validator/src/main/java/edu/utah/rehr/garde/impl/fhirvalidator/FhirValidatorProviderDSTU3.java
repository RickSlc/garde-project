package edu.utah.rehr.garde.impl.fhirvalidator;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.context.support.DefaultProfileValidationSupport;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.validation.FhirValidator;
import ca.uhn.fhir.validation.SingleValidationMessage;
import ca.uhn.fhir.validation.ValidationResult;
import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.model.validator.IFhirValidatorProvider;
import org.hl7.fhir.common.hapi.validation.support.*;
import org.hl7.fhir.common.hapi.validation.validator.FhirInstanceValidator;
import org.hl7.fhir.dstu3.model.StructureDefinition;
import org.hl7.fhir.dstu3.model.ValueSet;
import org.hl7.fhir.instance.model.api.IBaseResource;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;


public class FhirValidatorProviderDSTU3 implements IFhirValidatorProvider {

    private FhirContext fhirContext;
    private IParser parserJson;
    private FhirValidator validator;

    public FhirValidatorProviderDSTU3(List<String> jsonStructureDefinitions, List<String> jsonValueSets) {
        fhirContext = FhirContext.forDstu3();
        parserJson = fhirContext.newJsonParser();

        // Create a chain that will hold our modules
        ValidationSupportChain supportChain = new ValidationSupportChain();

        // DefaultProfileValidationSupport supplies base FHIR definitions. This is generally required
        // even if you are using custom profiles, since those profiles will derive from the base
        // definitions.
        DefaultProfileValidationSupport defaultSupport = new DefaultProfileValidationSupport(fhirContext);
        supportChain.addValidationSupport(defaultSupport);

        // This module supplies several code systems that are commonly used in validation
        supportChain.addValidationSupport(new CommonCodeSystemsTerminologyService(fhirContext));

        // This module implements terminology services for in-memory code validation
        supportChain.addValidationSupport(new InMemoryTerminologyServerValidationSupport(fhirContext));

        // Create a PrePopulatedValidationSupport which can be used to load custom definitions.
        // In this example we're loading two things, but in a real scenario we might
        // load many StructureDefinitions, ValueSets, CodeSystems, etc.
        PrePopulatedValidationSupport prePopulatedSupport = new PrePopulatedValidationSupport(fhirContext);

        for (String jsonStructureDefinition : jsonStructureDefinitions) {
            prePopulatedSupport.addStructureDefinition( parseStructureDefinition(jsonStructureDefinition) );
        }

//        /*Not available for DSTU3, hence commented out for now*/
//        for (String jsonValueSet : jsonValueSets) {
//            prePopulatedSupport.addValueSet(parseValueSet(jsonValueSet));
//        }

        // Add the custom definitions to the chain
        supportChain.addValidationSupport(prePopulatedSupport);

        // Wrap the chain in a cache to improve performance
        CachingValidationSupport cache = new CachingValidationSupport(supportChain);

        // Create a validator using the FhirInstanceValidator module. We can use this
        // validator to perform validation
        FhirInstanceValidator validatorModule = new FhirInstanceValidator(cache);
        this.validator = fhirContext.newValidator().registerValidatorModule(validatorModule);
    }

    @Override
    public UResult<List<SingleValidationMessage>> validate(String fhirResource) {
        ValidationResult result = validator.validateWithResult(fhirResource);
        return UResult.successResult(result.getMessages());
    }

    @Override
    public UResult<List<SingleValidationMessage>> validate(IBaseResource fhirResource) {
        ValidationResult result = validator.validateWithResult(fhirResource);
        return UResult.successResult(result.getMessages());
    }

    private StructureDefinition parseStructureDefinition(String structureDefinitionString) {
        InputStream is = new ByteArrayInputStream(structureDefinitionString.getBytes(StandardCharsets.UTF_8));
        StructureDefinition profile = parserJson.parseResource(StructureDefinition.class, is);
        return profile;
    }

    private ValueSet parseValueSet(String valueSetString) {
        InputStream is = new ByteArrayInputStream(valueSetString.getBytes(StandardCharsets.UTF_8));
        ValueSet valueSet = parserJson.parseResource(ValueSet.class, is);
        return valueSet;
    }


}

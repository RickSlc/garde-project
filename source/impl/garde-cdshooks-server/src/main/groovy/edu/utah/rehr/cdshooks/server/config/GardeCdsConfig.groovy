package edu.utah.rehr.cdshooks.server.config

import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService
import edu.utah.rehr.garde.domain.ehr.config.EhrConfig
import edu.utah.rehr.garde.domain.ehr.model.SmartDataElementMetaList
import edu.utah.rehr.garde.domain.cdshooks.utils.FhirR4JsonUtil
import edu.utah.rehr.garde.impl.cdshooks.glue.exec.GlueGeneticTestingRecommender
import edu.utah.rehr.garde.impl.cdshooks.nccn.exec.NccnGeneticTestingRecommender2023_R4
import edu.utah.rehr.garde.impl.cdshooks.nccn.exec.NccnGeneticTestingRecommenderR4
import org.opencds.hooks.lib.json.JsonUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.Profile

@Configuration
@Profile("gardecds")
@Import([EhrConfig.class])
class GardeCdsConfig {

    @Autowired
    ICodeService codeService

    @Autowired
    SmartDataElementMetaList sdeMetaList

    @Bean
    NccnGeneticTestingRecommenderR4 nccnGeneticTestingRecommenderR4(ICodeService codeService) {
        return new NccnGeneticTestingRecommenderR4(codeService, sdeMetaList)
    }

    @Bean
    NccnGeneticTestingRecommender2023_R4 nccnGeneticTestingRecommender2023_r4(ICodeService codeService){
        return new NccnGeneticTestingRecommender2023_R4(codeService, sdeMetaList)
    }

    @Bean
    GlueGeneticTestingRecommender glueGeneticTestingRecommender(ICodeService codeService) {
        return new GlueGeneticTestingRecommender(codeService)
    }

    @Bean
    JsonUtil r4JsonUtil() {
        return new FhirR4JsonUtil()
    }

}

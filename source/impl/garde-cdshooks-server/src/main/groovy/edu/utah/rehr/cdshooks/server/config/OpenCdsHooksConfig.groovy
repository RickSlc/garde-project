package edu.utah.rehr.cdshooks.server.config


import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService
import edu.utah.rehr.garde.domain.cdshooks.utils.FhirR4JsonUtil
import edu.utah.rehr.garde.impl.cdshooks.nccn.exec.NccnGeneticTestingRecommender2023_R4
import edu.utah.rehr.garde.impl.cdshooks.nccn.exec.NccnGeneticTestingRecommenderR4
import org.opencds.config.api.ConfigData
import org.opencds.config.api.ConfigurationService
import org.opencds.config.api.cache.CacheService
import org.opencds.config.api.strategy.ConfigStrategy
import org.opencds.config.classpath.ClasspathConfigStrategy
import org.opencds.config.file.FileConfigStrategy
import org.opencds.config.service.CacheServiceImpl
import org.opencds.hooks.lib.json.JsonUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.context.annotation.PropertySource

@Configuration
@Profile("opencds")
@PropertySource(['file:${user.home}/.opencds/opencds-hooks.properties'])
class OpenCdsHooksConfig {

    @Value('${knowledge-repository.type}')
    String configType

    @Value('${knowledge-repository.path}')
    String configPath

    @Value('${km.threads}')
    Integer kmThreads

    @Autowired
    ICodeService codeService

    @Bean
    ConfigData configData() {
        ConfigData cd = new ConfigData()
        cd.configType = configType
        cd.configPath = configPath
        cd.kmThreads = kmThreads
        return cd
    }

    @Bean
    CacheService cacheService() {
        return new CacheServiceImpl()
    }

    @Bean
    JsonUtil r4JsonUtil() {
        return new FhirR4JsonUtil()
    }

    @Bean
    ConfigurationService configurationService() {
        Set<ConfigStrategy> strategies = new HashSet<>()
        strategies.add(new FileConfigStrategy())
        strategies.add(new ClasspathConfigStrategy())
        return new ConfigurationService(
                strategies
                , cacheService()
                , configData()
        )
    }

    @Bean
    NccnGeneticTestingRecommenderR4 nccnGeneticTestingRecommenderR4(ICodeService codeService) {
        return new NccnGeneticTestingRecommenderR4(codeService)
    }

    @Bean
    NccnGeneticTestingRecommender2023_R4 nccnGeneticTestingRecommender2023_R4(ICodeService codeService) {
        return new NccnGeneticTestingRecommender2023_R4(codeService)
    }

}

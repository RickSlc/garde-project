package edu.utah.rehr.cdshooks.server.config

import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@Configuration
@ComponentScan(basePackages = [
        "edu.utah.rehr.garde.terminology.db"
])
@EntityScan(basePackages = ["edu.utah.rehr.garde.terminology.db.model"])
@EnableJpaRepositories(basePackages = ["edu.utah.rehr.garde.terminology.db.repository"])
@Profile("jpa")
class JpaConfig {
}

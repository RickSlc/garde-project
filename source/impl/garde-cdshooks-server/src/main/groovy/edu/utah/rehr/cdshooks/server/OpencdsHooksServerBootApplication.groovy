package edu.utah.rehr.cdshooks.server

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class OpencdsHooksServerBootApplication {

    static void main(String[] args) {
        SpringApplication.run(OpencdsHooksServerBootApplication, args)
    }

}

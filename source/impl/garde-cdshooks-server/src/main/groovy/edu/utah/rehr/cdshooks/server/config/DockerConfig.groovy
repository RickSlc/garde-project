package edu.utah.rehr.cdshooks.server.config

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.context.annotation.PropertySource

@Configuration
@Profile("docker")
@PropertySource("file:/root/.opencds/opencds-hooks.properties")
class DockerConfig {
}

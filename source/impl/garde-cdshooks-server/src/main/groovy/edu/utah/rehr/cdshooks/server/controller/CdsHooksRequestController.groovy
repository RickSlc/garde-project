package edu.utah.rehr.cdshooks.server.controller

import edu.utah.rehr.garde.domain.core.model.GEvaluator
import edu.utah.rehr.garde.domain.core.terminology.Code
import edu.utah.rehr.garde.domain.core.terminology.service.CodeServiceByMaps
import edu.utah.rehr.garde.domain.cdshooks.service.GCdsHookService
import edu.utah.rehr.garde.impl.cdshooks.glue.exec.GlueGeneticTestingRecommender
import edu.utah.rehr.garde.impl.cdshooks.nccn.exec.NccnGeneticTestingRecommender2023_R4
import edu.utah.rehr.garde.impl.cdshooks.nccn.exec.NccnGeneticTestingRecommenderR4
import edu.utah.rehr.garde.terminology.db.provider.CodeServiceJpaMapsProvider
import groovy.util.logging.Log
import org.apache.commons.lang3.NotImplementedException
import org.opencds.hooks.engine.api.CdsHooksExecutionEngine
import org.opencds.hooks.lib.json.JsonUtil
import org.opencds.hooks.model.request.CdsRequest
import org.opencds.hooks.model.response.CdsResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/r4/hooks")
@CrossOrigin(origins = "*")
@Log
class CdsHooksRequestController {

    private JsonUtil r4JsonUtil
    private NccnGeneticTestingRecommenderR4 nccnGeneticTestingRecommenderR4
    private NccnGeneticTestingRecommender2023_R4 nccnGeneticTestingRecommenderR42023
    private GlueGeneticTestingRecommender glueGeneticTestingRecommender
    private CodeServiceByMaps codeServiceByMaps
    private CodeServiceJpaMapsProvider codeServiceJpaMapsProvider

    @Autowired
    CdsHooksRequestController(
            NccnGeneticTestingRecommenderR4 nccnGeneticTestingRecommenderR4
            , NccnGeneticTestingRecommender2023_R4 nccnGeneticTestingRecommenderR42023
            , GlueGeneticTestingRecommender glueGeneticTestingRecommender
            , CodeServiceJpaMapsProvider codeServiceJpaMapsProvider
            , CodeServiceByMaps codeServiceByMaps
            , JsonUtil r4JsonUtil
    ) {
        this.nccnGeneticTestingRecommenderR4 = nccnGeneticTestingRecommenderR4
        this.nccnGeneticTestingRecommenderR42023 = nccnGeneticTestingRecommenderR42023
        this.glueGeneticTestingRecommender = glueGeneticTestingRecommender
        this.codeServiceJpaMapsProvider = codeServiceJpaMapsProvider
        this.codeServiceByMaps = codeServiceByMaps
        this.r4JsonUtil = r4JsonUtil
    }

    @GetMapping("/hello")
    String hello() {
        return "Hello CDS Hooks"
    }

    @GetMapping("/refreshTerminology")
    String refreshTerminology() {
        log.info("REFRESH CDS HOOKS TERMINOLOGY")

        Map<String, Set<Code>> standardCodeMap = codeServiceJpaMapsProvider.getStandardCodeBindingsMap().getResult()
        log.info("LOADING ${standardCodeMap.keySet().size()} STANDARD CODE BINDINGS")
        codeServiceByMaps.setStandardCodeBindingsMap(standardCodeMap)

        Map<String, Set<Code>> ehrCodeMap = codeServiceJpaMapsProvider.getEhrCodeBindingsMap().getResult()
        log.info("LOADING ${ehrCodeMap.keySet().size()} EHR CODE BINDINGS")
        codeServiceByMaps.setEhrCodeBindingsMap(ehrCodeMap)

        Map<String, String> uriBindingsMap = codeServiceJpaMapsProvider.getUriBindingsMap().getResult()
        log.info("LOADING ${uriBindingsMap.keySet().size()} URI CODE BINDINGS")
        codeServiceByMaps.setUriBindingsMap(codeServiceJpaMapsProvider.getUriBindingsMap().getResult())

        return "SUCCESS"
    }

    @GetMapping(value="/cds-services", produces = MediaType.APPLICATION_JSON_VALUE)
    String getCdsServices() throws IOException {
       return GCdsHookService.cdsHooksServiceDescriptionsJson
    }

    @PostMapping("/cds-services/{serviceId}")
    String invokeCdsService(@PathVariable("serviceId") String serviceId, @RequestBody String cdsRequestPayload) {

        if (isNotSupportedService(serviceId)) {
            throw new NotImplementedException("Service " + serviceId + " not supported")
        }

        CdsRequest cdsRequest = r4JsonUtil.fromJson(cdsRequestPayload, CdsRequest.class)
        CdsHooksExecutionEngine executionEngine = getCdsHooksExecutionEngine(serviceId)
        CdsResponse cdsResponse = executionEngine.evaluate(cdsRequest, null)
        String responsePayload = r4JsonUtil.toJson(cdsResponse)

        return responsePayload
    }

    @PostMapping("/cds-services/bulk/{serviceId}")
    String invokeBulkCdsService(@PathVariable("serviceId") String serviceId, @RequestBody String cdsRequestPayload) {

        if (isNotSupportedService(serviceId)) {
            throw new NotImplementedException("Service " + serviceId + " not supported")
        }

        List<CdsResponse> cdsResponses = new ArrayList<>()

        CdsRequest[] requests = r4JsonUtil.fromJson(cdsRequestPayload, CdsRequest[].class)
        CdsHooksExecutionEngine executionEngine = getCdsHooksExecutionEngine(serviceId)

        for (CdsRequest cdsRequest: requests) {
            CdsResponse cdsResponse = executionEngine.evaluate(cdsRequest, null)
            cdsResponses.add(cdsResponse)
        }

        String responsePayload = r4JsonUtil.toJson(cdsResponses.toArray(new CdsResponse[cdsResponses.size()]))

        return responsePayload
    }

    private static boolean isNotSupportedService(String serviceId) {
        return GEvaluator.findByServiceId(serviceId) == null
    }

    private CdsHooksExecutionEngine getCdsHooksExecutionEngine(String serviceId) {

        if (serviceId == GEvaluator.GENETIC_TESTING_RECOMMENDER_R4.serviceId) {
            return nccnGeneticTestingRecommenderR4
        }

        else if (serviceId == GEvaluator.GENETIC_TESTING_RECOMMENDER_R4_2023.serviceId) {
            return nccnGeneticTestingRecommenderR42023
        }

        else if (serviceId == GEvaluator.FH_GENETIC_TESTING_RECOMMENDER_R4_2018.serviceId) {
            return glueGeneticTestingRecommender
        }

        throw new NotImplementedException("Service " + serviceId + " not supported")
    }

}

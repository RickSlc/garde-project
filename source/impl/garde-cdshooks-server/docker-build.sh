#!/bin/bash

# stop running container
docker container stop garde-cdshooks
docker container stop garde-cdshooks-arm64
# docker container stop garde-cdshooks-win-amd64

# remove the container anticipating rebuild
docker container rm garde-cdshooks
docker container rm garde-cdshooks-arm64
# docker container rm garde-cdshooks-win-amd64

# Remove image of the container
docker image rm rickslc/garde-cdshooks
docker image rm rickslc/garde-cdshooks-arm64
# docker image rm rickslc/garde-cdshooks-win-amd64

# Rebuild the image of the container using Dockerfile -- this is for linux and intel
docker buildx build --platform linux/amd64 -t rickslc/garde-cdshooks .

# This one is for arm64 - macOS arm chips or similar
docker buildx build --platform linux/arm64 -t rickslc/garde-cdshooks-arm64 .

# This one is for arm64 - macOS arm chips or similar
# docker buildx build --platform windows/amd64 -t rickslc/garde-cdshooks-win-amd64 .

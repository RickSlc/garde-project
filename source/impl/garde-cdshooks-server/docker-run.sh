#!/bin/bash

# Create a new container from the image - for stable release (no debugger; runs in the background)
docker run -p 8091:8091 --name garde-cdshooks-server -d rehr/garde-cdshooks-server
#
# Add debug port mapping for debugging
#docker run -e -p 8081:8080 -p 8001:8000 --name bulkcdshooks rehr/bulkcdshooks

# for debugging - open shell
#docker run -e JAVA_TOOL_OPTIONS="-agentlib:jdwp=transport=dt_socket,address=8000,server=y,suspend=n" -p 8000:8000 rehr/bulkcdshooks

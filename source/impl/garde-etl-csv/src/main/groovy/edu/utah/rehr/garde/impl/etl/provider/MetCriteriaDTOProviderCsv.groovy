package edu.utah.rehr.garde.impl.etl.provider

import edu.utah.rehr.garde.domain.core.model.GlueRule
import edu.utah.rehr.garde.domain.core.model.Nccn2023Rule
import edu.utah.rehr.garde.domain.core.model.NccnRule
import edu.utah.rehr.garde.domain.core.model.dto.MetCriteriaDTO
import edu.utah.rehr.garde.domain.etl.service.ICsvService
import groovy.util.logging.Log
import org.apache.commons.csv.CSVRecord

@Log
class MetCriteriaDTOProviderCsv {

    private final ICsvService csvService

    MetCriteriaDTOProviderCsv(ICsvService csvService) {
        this.csvService = csvService
    }

    char delimiter = ","
    char quote = '"'
    Boolean skipFirstLine = true

    static String[] CSV_COLUMN_HEADERS = [
            "EHR_PAT_ID"
            , "EHR_ENC_ID"
            , "EHR_REC_ID"
            , "COHORT_CD"
            , "RULE_ID"
            , "RULE_CD"
    ]

    void write(String fileName, List<MetCriteriaDTO> metCriteriaDTOList, Boolean appendFile) {

        List<List<String>> lines = new ArrayList<>()

        try {
            for (MetCriteriaDTO m : metCriteriaDTOList) {

                String ruleCode = 'NOT_FOUND_ERROR'
                if (NccnRule.findById(m.criteriaId) != null) {
                    ruleCode = NccnRule.findById(m.criteriaId).getCode()
                } else if (Nccn2023Rule.findById(m.criteriaId) != null) {
                    ruleCode = Nccn2023Rule.findById(m.criteriaId).getCode()
                } else if (GlueRule.findById(m.criteriaId) != null) {
                    ruleCode = GlueRule.findById(m.criteriaId).getCode()
                } else {
                    throw new RuntimeException("NO RULES FOUND FOR CRITERIA " + m.criteriaId)
                }

                List<String> line = new ArrayList<>()
                line.add(m.ehrPatientId)
                line.add(m.ehrEncounterId)
                line.add(m.ehrRecordId)
                line.add(m.cohortCd)
                line.add(m.criteriaId)
                line.add(ruleCode)

                lines.add(line)
            }
        } catch (Exception e) {
            e.printStackTrace()
        }

        csvService.writeToCsv(new FileWriter(fileName, appendFile), CSV_COLUMN_HEADERS, lines, ',' as char, '"' as char, !appendFile)
    }

    List<MetCriteriaDTO> readFromCsv(String filePath) {

        List<MetCriteriaDTO> metCriteriaDTOList = new ArrayList<>()

        FileReader fileReader = null
        Iterable<CSVRecord> records = null
        try {
            records = csvService.getCsvRecords(filePath, CSV_COLUMN_HEADERS, ',' as char, '"' as char)
            for (CSVRecord record:records){
                if (record.get(0) == "EHR_PAT_ID") { // skip header
                    continue
                }
                metCriteriaDTOList.add(new MetCriteriaDTO(record.get(0),record.get(1),record.get(2),record.get(3),record.get(4)))
            }
        } finally {
            if (fileReader != null) {
                try {
                    fileReader.close()
                } catch (IOException ex) {
                    log.severe("CAN'T CLOSE FILE READER IN readFromCsv.")
                }
            }
        }

        return metCriteriaDTOList
    }

}

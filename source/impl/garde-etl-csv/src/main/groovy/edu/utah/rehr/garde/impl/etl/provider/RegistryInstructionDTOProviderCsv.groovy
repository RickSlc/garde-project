package edu.utah.rehr.garde.impl.etl.provider


import edu.utah.rehr.garde.domain.core.model.dto.RegistryInstructionDTO
import edu.utah.rehr.garde.impl.etl.service.CsvService
import groovy.util.logging.Log
import org.apache.commons.csv.CSVRecord

@Log
class RegistryInstructionDTOProviderCsv {

    static String[] header = [
            "EHR_PAT_ID_CODE_SYSTEM"
            ,"EHR_PAT_ID"
            ,"INSTRUCTION"
            ,"SDE_TYPE"
            ,"SDE"
            ,"SDE_ID"
            ,"SDE_VALUE"
    ]

    static List<RegistryInstructionDTO> readFromCsv(String filePath, CsvService csvService) {

        List<RegistryInstructionDTO> registryInstructionDTOs = new ArrayList<>()

        FileReader fileReader = null
        Iterable<CSVRecord> records = null
        try {
            records = csvService.getCsvRecords(filePath, header, ',' as char, '"' as char)
            for (CSVRecord record:records){
                if (record.get(1) == "EHR_PAT_ID") { // skip header
                    continue
                }
                registryInstructionDTOs.add(new RegistryInstructionDTO(
                        record.get(0)
                        ,record.get(1)
                        ,record.get(2)
                        ,record.get(3)
                        ,record.get(4)
                        ,record.get(5)
                        ,record.get(6)
                ))
            }
        } finally {
            if (fileReader != null) {
                try {
                    fileReader.close()
                } catch (IOException ex) {
                    log.severe("CAN'T CLOSE FILE READER IN readFromCsv.")
                }
            }
        }

        return registryInstructionDTOs
    }


    static void  writeToFile(String fileName, List<RegistryInstructionDTO> registryInstructionList, String delimiter, boolean append){
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, append))){
            int line = 0
            for(RegistryInstructionDTO dto : registryInstructionList) {
                line++
                StringBuffer row = new StringBuffer()
                if (line == 1) {
                    row.append(header[0])
                            .append(delimiter).append("\"").append(header[1]).append("\"")
                            .append(delimiter).append("\"").append(header[2]).append("\"")
                            .append(delimiter).append("\"").append(header[3]).append("\"")
                            .append(delimiter).append("\"").append(header[4]).append("\"")
                            .append(delimiter).append("\"").append(header[5]).append("\"")
                            .append(delimiter).append("\"").append(header[6]).append("\"").append("\n")
                }
                row.append(dto.ehrPatientIdCodeSystem)
                        .append(delimiter).append("\"").append(dto.ehrPatientId).append("\"")
                        .append(delimiter).append("\"").append(dto.instruction).append("\"")
                        .append(delimiter).append("\"").append(dto.dataElementType).append("\"")
                        .append(delimiter).append("\"").append(dto.dataElementName).append("\"")
                        .append(delimiter).append("\"").append(dto.dataElementId).append("\"")
                        .append(delimiter).append("\"").append(dto.dataElementValue).append("\"").append("\n")

                writer.write(row.toString())
            }
            writer.flush()
        } catch (IOException e) {
            log.severe("Error while writing to file: " + fileName)
        }
    }

}
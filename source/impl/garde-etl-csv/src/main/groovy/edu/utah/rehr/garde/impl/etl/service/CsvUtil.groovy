package edu.utah.rehr.garde.impl.etl.service

import com.opencsv.bean.ColumnPositionMappingStrategy
import com.opencsv.bean.StatefulBeanToCsv
import com.opencsv.bean.StatefulBeanToCsvBuilder
import com.opencsv.exceptions.CsvDataTypeMismatchException
import com.opencsv.exceptions.CsvRequiredFieldEmptyException

class CsvUtil {

    static <T> void writeBeansToCsv(List<T> beans, PrintWriter writer, Class<T> beanType)
            throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        StatefulBeanToCsv<T> beanToCsv = getCsvBuilderWithStrategy(beanType, writer)
        beanToCsv.write(beans)
    }

    private static <T> StatefulBeanToCsv<T> getCsvBuilderWithStrategy(Class<T> beanType, PrintWriter writer) {
        ColumnPositionMappingStrategy<T> strategy = new ColumnPositionMappingStrategy<>()
        strategy.setType(beanType)

        return new StatefulBeanToCsvBuilder<T>(writer)
                .withMappingStrategy(strategy)
                .build()
    }
}
package edu.utah.rehr.garde.impl.etl.service

import edu.utah.rehr.garde.domain.core.model.PatientDataElement
import edu.utah.rehr.garde.domain.etl.service.ICsvService
import groovy.util.logging.Log
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVRecord

@Log
class CsvService implements ICsvService {

    String recordSeparator = "\r\n"
    char comment = '#'
    String columnSeparator = ","

    @Override
    Iterable<CSVRecord> getCsvRecords(String filePath, String[] HEADERS, char delimiter, char quote) {

        if (!isValidHeader(new FileReader(filePath), HEADERS, columnSeparator)) {
            throw new RuntimeException("Invalid CSV header")
        }

        CSVFormat csvFormat = CSVFormat.DEFAULT.builder().setCommentMarker(comment).build()
        CSVParser parser = new CSVParser(new FileReader(filePath), csvFormat)

        return parser
    }

    @Override
    Integer writeToCsv(Writer writer, String[] headers, List<List<String>> rows, char delimiter, char quote, boolean includeHeader) {

        int rowNum = 0
        for (List<String> row:rows) {
            rowNum++
            StringBuilder line = new StringBuilder()

            if (rowNum == 1 && includeHeader) { // add header
                int headerNum = 0
                for (String colName:headers) {
                    headerNum++
                    if (headerNum == headers.size()) { // last column add line separator
                        line.append(colName).append(recordSeparator)
                    } else {
                        line.append(colName).append(delimiter)
                    }
                }
            }

            int colNum = 0
            for (String colValue:row) {
                colNum++
                if (colNum == row.size()) { // last column add line separator
                    line.append(colValue).append(recordSeparator)
                } else {
                    line.append(colValue).append(delimiter)
                }
            }

            writer.write(line.toString())
        }

        writer.flush()
        writer.close()

        return rowNum
    }


    final static String[] DATA_ELEMENT_HEADERS = ["DE_PAT_ID","SDE_ID","SDE_VALUE","PAT_SEX","PAT_DOB"]

    @Override
    Integer writePatientDataElementsToCsv(Writer writer, List<PatientDataElement> dataElements, char delimiter, char quote) {

        int records = 0
        for (PatientDataElement de: dataElements) {

            StringBuilder line = new StringBuilder()
                    .append(de.getPatientId()).append(delimiter)
                    .append(de.getDataElementId()).append(delimiter)
                    .append(de.getDataElementValue()).append(delimiter)
                    .append(de.getPatientSex()).append(delimiter)
                    .append(de.getPatientDateOfBirth())
                    .append(recordSeparator)

            writer.write(line.toString())
            records++
        }

        writer.flush()
        writer.close()

        return records
    }

}

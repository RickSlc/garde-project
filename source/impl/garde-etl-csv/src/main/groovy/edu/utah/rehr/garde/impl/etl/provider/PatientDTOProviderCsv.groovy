package edu.utah.rehr.garde.impl.etl.provider

import jakarta.validation.ConstraintViolation
import jakarta.validation.Validation
import jakarta.validation.Validator

import edu.utah.rehr.garde.domain.core.model.dto.PatientDTO
import edu.utah.rehr.garde.domain.core.utils.StringTo
import edu.utah.rehr.garde.domain.etl.provider.IPatientDTOProvider
import edu.utah.rehr.garde.domain.etl.service.ICsvService
import edu.utah.rehr.garde.impl.etl.service.CsvService
import groovy.util.logging.Log
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVRecord
import org.apache.commons.lang3.StringUtils

import java.time.LocalDate

@Log
class PatientDTOProviderCsv implements IPatientDTOProvider {

    private final ICsvService csvService
    private Map<String, PatientDTO> patientDTOMap

    PatientDTOProviderCsv() {
        this.csvService = new CsvService()
    }
    static String[] NCCN_MIN_PAT_HEADER = [
            "EHR_PAT_ID_CODE_SYSTEM"
            ,"EHR_PAT_ID"
            ,"BIRTH_DATE"
            ,"EHR_SEX_CODE_SYSTEM"
            ,"EHR_SEX_CODE"
            ,"EHR_SEX_LABEL"
            ,"EHR_RELIGION_CODE_SYSTEM"
            ,"EHR_RELIGION_CODE"
            ,"EHR_RELIGION_LABEL"
            ,"PREV_MET_CRITERIA_LIST"
    ]

    static String[] PAT_HEADER = [
            "EHR_PAT_ID_CODE_SYSTEM"
            ,"EHR_PAT_ID"
            ,"BIRTH_DATE"

            , "ADDRESS_1"
            , "ADDRESS_2"
            , "CITY"
            , "STATE"
            , "ZIP"

            ,"EHR_SEX_CODE_SYSTEM"
            ,"EHR_SEX_CODE"
            ,"EHR_SEX_LABEL"

            ,"EHR_MARITAL_STATUS_CODE_SYSTEM"
            ,"EHR_MARITAL_STATUS_CODE"
            ,"EHR_MARITAL_STATUS_LABEL"

            ,"EHR_RACE_CODE_SYSTEM"
            ,"EHR_RACE_CODE"
            ,"EHR_RACE_LABEL"

            ,"EHR_ETHNICITY_CODE_SYSTEM"
            ,"EHR_ETHNICITY_CODE"
            ,"EHR_ETHNICITY_LABEL"

            ,"EHR_RELIGION_CODE_SYSTEM"
            ,"EHR_RELIGION_CODE"
            ,"EHR_RELIGION_LABEL"

            ,"EHR_LANGUAGE_CODE_SYSTEM"
            ,"EHR_LANGUAGE_CODE"
            ,"EHR_LANGUAGE_LABEL"

            ,"PREV_MET_CRITERIA_LIST"
    ]

    @Override
    List<PatientDTO> getPatientDTOs(String patientFactFilePath) {

        checkFile(patientFactFilePath)
        Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(new FileReader(patientFactFilePath))

        Map<String,Integer> headerIndex = CsvService.headerIndex(NCCN_MIN_PAT_HEADER)
        List<PatientDTO> patientDTOs = new ArrayList<>()
        int i = 0
        for (CSVRecord record: records) {
            i++
            if (i == 1) { // skip header
                continue
            }
            PatientDTO p = toPatientNccnDTO(headerIndex, record)
            patientDTOs.add(p)
        }

        log.info(patientDTOs.size() + " PATIENT CSV RECORDS TRANSFORMED INTO DTOs")

        return patientDTOs
    }

    @Override
    Map<String, PatientDTO> getPatientDTOMap(String patientFactFilePath) {

        checkFile(patientFactFilePath)

        if (patientDTOMap == null || patientDTOMap.isEmpty()) {
            Map<String, PatientDTO> map = new HashMap<>()
            for (PatientDTO p:getPatientDTOs()) {
                map.put(p.patientId, p)
            }
            patientDTOMap = map
            log.info("PATIENT DTO MAP CREATED WITH " + map.size() + " RECORDS.")
        }

        return patientDTOMap
    }

    private void checkFile(String filePath) {
        if ( StringUtils.isEmpty(filePath) ) {
            throw new RuntimeException("Patient data file not found: ${filePath}")
        }

        if (!csvService.isValidHeader(new FileReader(filePath), NCCN_MIN_PAT_HEADER, ",")) {
            throw new RuntimeException("Invalid CSV header in patient data file ${filePath}")
        }
    }

    static PatientDTO toPatientNccnDTO(Map<String,Integer> headerIndex, CSVRecord rec) {

        LocalDate birthDate = null

        try {
            birthDate = StringTo.aLocalDate(rec.get(headerIndex.get("BIRTH_DATE")), "yyyy-MM-dd")
        } catch (Exception e) {
            String birthDateValue = rec.get(headerIndex.get("BIRTH_DATE")) ? rec.get(headerIndex.get("BIRTH_DATE")) : "(empty)"
            String msg = "Properly formatted birth dates are required: EHR_PAT_ID = ${rec.get(headerIndex.get("EHR_PAT_ID"))} BIRTH_DATE = ${birthDateValue}. The correct format is YYYY-MM-DD."
            log.severe(msg)
            throw new RuntimeException(msg)
        }

        PatientDTO p = new PatientDTO(
                patientIdCodeSystem: rec.get(headerIndex.get("EHR_PAT_ID_CODE_SYSTEM"))
                , patientId: rec.get(headerIndex.get("EHR_PAT_ID"))
                , sexCodeSystem: rec.get(headerIndex.get("EHR_SEX_CODE_SYSTEM"))
                , sexCode: rec.get(headerIndex.get("EHR_SEX_CODE"))
                , sexLabel: rec.get(headerIndex.get("EHR_SEX_LABEL"))
                , birthDateString: rec.get(headerIndex.get("BIRTH_DATE"))
                , religionCodeSystem: rec.get(headerIndex.get("EHR_RELIGION_CODE_SYSTEM"))
                , religionCode: rec.get(headerIndex.get("EHR_RELIGION_CODE"))
                , religionLabel: rec.get(headerIndex.get("EHR_RELIGION_LABEL"))
        )

        String mcList = rec.get(headerIndex.get("PREV_MET_CRITERIA_LIST"))

        if (mcList) {
            p.previouslyMetCriteriaCodes = mcList.split("\\|")
        }

        return p
    }

    static PatientDTO toPatientDTO(Map<String,Integer> headerIndex, CSVRecord rec) {

        LocalDate birthDate = parseDate(
                rec.get(headerIndex.get("EHR_PAT_ID")) // needed for error msg
                , rec.get(headerIndex.get("BIRTH_DATE")) // date to pares
        )

        PatientDTO p = new PatientDTO(
                patientIdCodeSystem: rec.get(headerIndex.get("EHR_PAT_ID_CODE_SYSTEM"))
                , patientId: rec.get(headerIndex.get("EHR_PAT_ID"))
                , birthDate: birthDate
                , address1: rec.get(headerIndex.get("ADDRESS_1"))
                , address2: rec.get(headerIndex.get("ADDRESS_2"))
                , state: rec.get(headerIndex.get("STATE"))
                , zip: rec.get(headerIndex.get("ZIP"))
                , sexCodeSystem: rec.get(headerIndex.get("EHR_SEX_CODE_SYSTEM"))
                , sexCode: rec.get(headerIndex.get("EHR_SEX_CODE"))
                , sexLabel: rec.get(headerIndex.get("EHR_SEX_LABEL"))
                , maritalStatusCodeSystem: rec.get(headerIndex.get("EHR_MARITAL_STATUS_CODE_SYSTEM"))
                , maritalStatusCode: rec.get(headerIndex.get("EHR_MARITAL_STATUS_CODE"))
                , maritalStatusLabel: rec.get(headerIndex.get("EHR_MARITAL_STATUS_LABEL"))
                , raceCodeSystem: rec.get(headerIndex.get("EHR_RACE_CODE_SYSTEM"))
                , raceCode: rec.get(headerIndex.get("EHR_RACE_CODE"))
                , raceLabel: rec.get(headerIndex.get("EHR_RACE_LABEL"))
                , ethnicityCodeSystem: rec.get(headerIndex.get("EHR_ETHNICITY_CODE_SYSTEM"))
                , ethnicityCode: rec.get(headerIndex.get("EHR_ETHNICITY_CODE"))
                , ethnicityLabel: rec.get(headerIndex.get("EHR_ETHNICITY_LABEL"))
                , religionCodeSystem: rec.get(headerIndex.get("EHR_RELIGION_CODE_SYSTEM"))
                , religionCode: rec.get(headerIndex.get("EHR_RELIGION_CODE"))
                , religionLabel: rec.get(headerIndex.get("EHR_RELIGION_LABEL"))
                , langCodeSystem: rec.get(headerIndex.get("EHR_LANGUAGE_CODE_SYSTEM"))
                , langCode: rec.get(headerIndex.get("EHR_LANGUAGE_CODE"))
                , langLabel: rec.get(headerIndex.get("EHR_LANGUAGE_LABEL"))
        )

        String mcList = rec.get(headerIndex.get("PREV_MET_CRITERIA_LIST"))

        if (mcList) {
            p.previouslyMetCriteriaCodes = mcList.split("\\|")
        }

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator()

        Set<ConstraintViolation<PatientDTO>> violations = validator.validate(p)
        if (!violations.isEmpty()) {
            // Handle validation errors (could log or throw an exception)
            for (ConstraintViolation<PatientDTO> violation : violations) {
                log.warning("Validation error in record: " + rec.toString())
                log.warning(violation.getPropertyPath() + ": " + violation.getMessage())
            }
        }

        return p
    }

    private static LocalDate parseDate(String patId, String birthDateString) {

        try {
            return StringTo.aLocalDate(birthDateString, "yyyy-MM-dd")
        } catch (Exception e) {
            String birthDateValue = birthDateString ? birthDateString : "(empty)"
            String msg = "Properly formatted birth dates are required: EHR_PAT_ID = ${patId} BIRTH_DATE = ${birthDateValue}. The correct format is YYYY-MM-DD."
            log.severe(msg)
            throw new RuntimeException(msg)
        }

    }


}

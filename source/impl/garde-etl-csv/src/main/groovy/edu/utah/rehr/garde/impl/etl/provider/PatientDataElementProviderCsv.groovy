package edu.utah.rehr.garde.impl.etl.provider

import edu.utah.rehr.garde.domain.core.accessory.UResult
import edu.utah.rehr.garde.domain.core.model.Constant
import edu.utah.rehr.garde.domain.core.model.GCohort
import edu.utah.rehr.garde.domain.core.model.PatientDataElement
import edu.utah.rehr.garde.domain.core.provider.IPatientDataElementProvider
import edu.utah.rehr.garde.impl.etl.service.CsvService
import org.apache.commons.csv.CSVRecord
import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Component

import java.util.logging.Logger

@Component
class PatientDataElementProviderCsv implements IPatientDataElementProvider {

    private Logger log = Logger.getLogger(this.getClass().getName())

    String inFilePath
    String outFilePath
    char delimiter = "|"
    char quote = '"'

    private CsvService csvService

    PatientDataElementProviderCsv() {
        CsvService csvService = new CsvService()
    }

    @Override
    UResult<List<PatientDataElement>> getPatientDataElements(GCohort cohort, Integer version) {
        return UResult.successResult(getDataElementsFromCsv())
    }

    List<PatientDataElement> getDataElementsFromCsv() {
        assert StringUtils.isNotEmpty(inFilePath)

        List<PatientDataElement> patientDataElements = new ArrayList<>()

        if (Constant.NO_RESULT == inFilePath) {
            log.warning("NO DATA ELEMENT FILE FOUND FOR IMPORTING" )
            return patientDataElements
        }

        CsvService csvService = new CsvService()
        Iterable<CSVRecord> records = csvService.getCsvRecords(
                inFilePath
                , CsvService.DATA_ELEMENT_HEADERS
                , delimiter
                , quote
        )

        for (CSVRecord record: records) {

            PatientDataElement patientDataElement = new PatientDataElement()
            patientDataElement.patientId = record.get("DE_PAT_ID")
            patientDataElement.patientSex = record.get("PAT_SEX")
            patientDataElement.patientDateOfBirth = record.get("PAT_DOB")
            patientDataElement.dataElementId = record.get("SDE_ID")
            patientDataElement.dataElementValue = record.get("SDE_VALUE")

            patientDataElements.add(patientDataElement)
        }

        return patientDataElements
    }

    @Override
    UResult<Integer> writePatientDataElements(List<PatientDataElement> dataElements) {
        assert StringUtils.isNotEmpty(outFilePath)

        Integer writeCount = csvService.writePatientDataElementsToCsv(
                new FileWriter(outFilePath)
                , dataElements
                , delimiter
                , quote
        )

        return UResult.successResult(writeCount)
    }

}

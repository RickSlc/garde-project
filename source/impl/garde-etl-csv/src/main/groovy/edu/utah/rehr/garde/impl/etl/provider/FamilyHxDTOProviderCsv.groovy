package edu.utah.rehr.garde.impl.etl.provider

import edu.utah.rehr.garde.domain.core.model.Constant
import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxDTO
import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxNlpDTO
import edu.utah.rehr.garde.domain.core.utils.StringTo
import edu.utah.rehr.garde.domain.etl.provider.IFamilyHxDTOProvider
import edu.utah.rehr.garde.domain.etl.service.ICsvService
import edu.utah.rehr.garde.impl.etl.service.CsvService
import groovy.util.logging.Log
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVRecord
import org.apache.commons.lang3.StringUtils

@Log
class FamilyHxDTOProviderCsv implements IFamilyHxDTOProvider {

    private final ICsvService csvService
    String familyHxFilePath

    FamilyHxDTOProviderCsv(ICsvService csvService, String familyHxFilePath) {
        this.csvService = csvService
        this.familyHxFilePath = familyHxFilePath
    }

    FamilyHxDTOProviderCsv(ICsvService csvService) {
        this.csvService = csvService
    }

    char delimiter = ","
    char quote = '"'

    static String[] CSV_COLUMN_HEADERS = [
            "EHR_FAMILY_HX_ID_CODE_SYSTEM"
            , "EHR_FAMILY_HX_ID"
            , "EHR_PAT_ID_CODE_SYSTEM"
            , "EHR_PAT_ID"
            , "EHR_ENC_ID_CODE_SYSTEM"
            , "EHR_ENC_ID"
            , "EHR_REC_ID"
            , "EHR_REC_DT"
            , "EHR_COND_CODE_SYSTEM"
            , "EHR_COND_CODE"
            , "EHR_COND_LABEL"
            , "EHR_RELATION_CODE_SYSTEM"
            , "EHR_RELATION_CODE"
            , "EHR_RELATION_LABEL"
            , "EHR_COND_ONSET_AGE"
            , "EHR_COMMENTS"
    ]

    static String[] CSV_COLUMN_HEADERS_NLP_EXTENSION = [
            "NLP_COND_CODE_SYSTEM"
            , "NLP_COND_CODE"
            , "NLP_COND_LABEL"
            , "NLP_COND_ONSET_TYPE"
            , "NLP_COND_ONSET_AGE"
            , "NLP_COND_ONSET_AGE_RANGE"
            , "NLP_RELATION_CODE_SYSTEM"
            , "NLP_RELATION_CODE"
            , "NLP_RELATION_LABEL"

    ]

    static String[] nlpCsvArray() {
        List<String> both = new ArrayList<String>(CSV_COLUMN_HEADERS.length + CSV_COLUMN_HEADERS_NLP_EXTENSION.length)
        Collections.addAll(both, CSV_COLUMN_HEADERS)
        Collections.addAll(both, CSV_COLUMN_HEADERS_NLP_EXTENSION)
        return both.toArray(new String[both.size()])
    }

    @Override
    void setSource(String source) {
        familyHxFilePath = source
    }

    private void checkFile() {
        if (Constant.NO_RESULT == familyHxFilePath || StringUtils.isEmpty(familyHxFilePath)) {
            log.severe("INVALID FILE PATH TO FAMILY_HX")
            throw new RuntimeException("PATIENT FHX DATA FILE NOT FOUND: ${familyHxFilePath}")
        }

        FileReader reader = new FileReader(familyHxFilePath)
        if (!csvService.isValidHeader(reader, CSV_COLUMN_HEADERS, ",")) {
            log.severe("INVALID CSV HEADER IN FAMILY_HX")
            throw new RuntimeException("Invalid CSV header in family history data file ${familyHxFilePath}")
        }

    }

    @Override
    List<FamilyHxDTO> getFamilyHxDTOs(String patientId) {
        Map<String, List<FamilyHxDTO>> map = getFamilyHxDTOMap()
        return map.get(patientId)
    }

    @Override
    List<FamilyHxNlpDTO> getFamilyHxNlpDTOs(String patientId) {
        Map<String, List<FamilyHxNlpDTO>> map = getFamilyHxNlpDTOMap()
        return map.get(patientId)
    }

    @Override
    Integer writeFamilyHxNlpDTO(String filePath, List<FamilyHxNlpDTO> familyHxDTOList) {

        List<List<String>> rows = new ArrayList<>()
        for (FamilyHxNlpDTO fhx : familyHxDTOList) {
            List<String> row = new ArrayList<>()
            row.add(fhx.familyHxDTO.familyHxIdCodeSystem)
            row.add(fhx.familyHxDTO.familyHxId)
            row.add(fhx.familyHxDTO.patientIdCodeSystem)
            row.add(fhx.familyHxDTO.patientId)
            row.add(fhx.familyHxDTO.encounterIdCodeSystem)
            row.add(fhx.familyHxDTO.encounterId)
            row.add(fhx.familyHxDTO.recordId)
            row.add(fhx.familyHxDTO.encounterDate.toString())
            row.add(fhx.familyHxDTO.conditionCodeSystem)
            row.add(fhx.familyHxDTO.conditionCode)
            row.add(fhx.familyHxDTO.conditionLabel)
            row.add(fhx.familyHxDTO.relationshipCodeSystem)
            row.add(fhx.familyHxDTO.relationshipCode)
            row.add(fhx.familyHxDTO.relationshipLabel)
            row.add(String.format("%s", fhx.familyHxDTO.conditionOnsetAge))
            row.add(fhx.familyHxDTO.comments)
            row.add(fhx.nlpConditionCodeSystem)
            row.add(fhx.nlpConditionCode)
            row.add(fhx.nlpConditionLabel)
            row.add(fhx.nlpConditionOnsetType)
            row.add(String.format("%s", fhx.nlpConditionOnsetAge))
            row.add(fhx.nlpConditionOnsetAgeRange)
            row.add(fhx.nlpRelationshipCodeSystem)
            row.add(fhx.nlpRelationshipCode)
            row.add(fhx.nlpRelationshipLabel)
            rows.add(row)
        }

        Integer rowsWritten = csvService.writeToCsv(new FileWriter(filePath), nlpCsvArray(), rows, '|' as char, '"' as char)

        return rowsWritten
    }

    @Override
    List<FamilyHxDTO> getFamilyHxDTOs() {

        checkFile()
        Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(new FileReader(familyHxFilePath))
        Map<String, Integer> headerIndex = CsvService.headerIndex(CSV_COLUMN_HEADERS)
        List<FamilyHxDTO> familyHxDTOs = new ArrayList<>()

        int i = 0
        for (CSVRecord record : records) {
            i++
            if (i == 1) { // skip header
                continue
            }
            FamilyHxDTO p = toFamilyHxDTO(headerIndex, record)
            familyHxDTOs.add(p)
        }

        log.info(familyHxDTOs.size() + " FAMILY HX CSV RECORDS TRANSFORMED INTO DTOs")

        return familyHxDTOs
    }

    @Override
    Map<String, List<FamilyHxDTO>> getFamilyHxDTOMap() {

        checkFile()
        Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(new FileReader(familyHxFilePath))
        Map<String, Integer> headerIndex = CsvService.headerIndex(CSV_COLUMN_HEADERS)
        Map<String, List<FamilyHxDTO>> fhxDTOMap = new HashMap<>()

        int i = 0
        for (CSVRecord record : records) {
            i++
            if (i == 1) { // skip header
                continue
            }

            FamilyHxDTO f = toFamilyHxDTO(headerIndex, record)

            if (fhxDTOMap.get(f.patientId) != null) {
                List<FamilyHxDTO> fHxList = fhxDTOMap.get(f.patientId)
                fHxList.add(f)
                fhxDTOMap.put(f.patientId, fHxList)
            } else {
                List<FamilyHxDTO> fHxList = new ArrayList<>()
                fHxList.add(f)
                fhxDTOMap.put(f.patientId, fHxList)
            }

        }

        log.info("FAMILY HX CSV RECORDS TRANSFORMED FOR " + fhxDTOMap.size() + " PEOPLE")

        return fhxDTOMap
    }

//    @Override
//    Map<String, List<FamilyHxDTO>> getFamilyHxDTOMap() {
//
//        checkFile()
//        Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(new FileReader(familyHxFilePath))
//        Map<String,Integer> headerIndex = CsvService.headerIndex(CSV_COLUMN_HEADERS)
//        Map<String, List<FamilyHxDTO>> fhxDTOMap = new HashMap<>()
//
//        int i = 0
//        for (CSVRecord record: records) {
//            i++
//            if (i == 1) { // skip header
//                continue
//            }
//
//            FamilyHxDTO f = toFamilyHxDTO(headerIndex, record)
//
//            if (fhxDTOMap.get(f.patientId) != null) {
//                List<FamilyHxDTO> fHxList = fhxDTOMap.get(f.patientId)
//                fHxList.add(f)
//                fhxDTOMap.put(f.patientId, fHxList)
//            } else {
//                List<FamilyHxDTO> fHxList = new ArrayList<>()
//                fHxList.add(f)
//                fhxDTOMap.put(f.patientId, fHxList)
//            }
//
//        }
//
//        log.info("FAMILY HX CSV RECORDS TRANSFORMED FOR " + fhxDTOMap.size() + " PEOPLE")
//
//        return fhxDTOMap
//    }

    @Override
    Map<String, List<FamilyHxNlpDTO>> getFamilyHxNlpDTOMap() {

        checkFile()
        Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(new FileReader(familyHxFilePath))
        Map<String, Integer> headerIndex = CsvService.headerIndex(nlpCsvArray())
        Map<String, List<FamilyHxNlpDTO>> fhxNlpDTOMap = new HashMap<>()

        int i = 0
        for (CSVRecord record : records) {
            i++
            if (i == 1) { // skip header
                continue
            }

            FamilyHxNlpDTO f = toFamilyHxNlpDTO(headerIndex, record)

            if (fhxNlpDTOMap.get(f.familyHxDTO.patientId) != null) {
                List<FamilyHxNlpDTO> fHxList = fhxNlpDTOMap.get(f.familyHxDTO.patientId)
                fHxList.add(f)
                fhxNlpDTOMap.put(f.familyHxDTO.patientId, fHxList)
            } else {
                List<FamilyHxNlpDTO> fHxList = new ArrayList<>()
                fHxList.add(f)
                fhxNlpDTOMap.put(f.familyHxDTO.patientId, fHxList)
            }

        }

        log.info("DONE PROCESSING PATIENT FHX CSV RECORDS: " + fhxNlpDTOMap.size())

        return fhxNlpDTOMap
    }

    static FamilyHxDTO toFamilyHxDTO(Map<String, Integer> headerIndex, CSVRecord rec) {

        FamilyHxDTO f = new FamilyHxDTO()
        f.familyHxIdCodeSystem = rec.get(headerIndex.get("EHR_FAMILY_HX_ID_CODE_SYSTEM"))
        f.familyHxId = rec.get(headerIndex.get("EHR_FAMILY_HX_ID"))
        f.patientIdCodeSystem = rec.get(headerIndex.get("EHR_PAT_ID_CODE_SYSTEM"))
        f.patientId = rec.get(headerIndex.get("EHR_PAT_ID"))
        f.encounterIdCodeSystem = rec.get(headerIndex.get("EHR_ENC_ID_CODE_SYSTEM"))
        f.encounterId = rec.get(headerIndex.get("EHR_ENC_ID"))
        f.recordId = rec.get(headerIndex.get("EHR_REC_ID"))
        f.conditionCodeSystem = rec.get(headerIndex.get("EHR_COND_CODE_SYSTEM"))
        f.conditionCode = rec.get(headerIndex.get("EHR_COND_CODE"))
        f.conditionLabel = rec.get(headerIndex.get("EHR_COND_LABEL"))
        f.relationshipCodeSystem = rec.get(headerIndex.get("EHR_RELATION_CODE_SYSTEM"))
        f.relationshipCode = rec.get(headerIndex.get("EHR_RELATION_CODE"))
        f.relationshipLabel = rec.get(headerIndex.get("EHR_RELATION_LABEL"))
        f.comments = rec.get(headerIndex.get("EHR_COMMENTS"))

        if (StringUtils.isNotEmpty(rec.get(headerIndex.get("EHR_REC_DT")))) {
            f.encounterDateString = rec.get(headerIndex.get("EHR_REC_DT"))
        } else {
            log.warning("NO EHR_REC_DT for EHR_PAT_ID " + rec.get(headerIndex.get("EHR_PAT_ID")))
        }

        if (StringUtils.isNotEmpty(rec.get(headerIndex.get("EHR_COND_ONSET_AGE")))) {
            String onsetAge = rec.get(headerIndex.get("EHR_COND_ONSET_AGE"))
            f.setConditionOnsetAgeString(onsetAge)
        }

        return f
    }

    static FamilyHxNlpDTO toFamilyHxNlpDTO(Map<String, Integer> headerIndex, CSVRecord rec) {

        FamilyHxNlpDTO fhxNlpDTO = new FamilyHxNlpDTO()
        fhxNlpDTO.familyHxDTO = toFamilyHxDTO(headerIndex, rec)

        fhxNlpDTO.nlpConditionCodeSystem = rec.get(headerIndex.get("NLP_COND_CODE_SYSTEM"))
        fhxNlpDTO.nlpConditionCode = rec.get(headerIndex.get("NLP_COND_CODE"))
        fhxNlpDTO.nlpConditionLabel = rec.get(headerIndex.get("NLP_COND_LABEL"))
        fhxNlpDTO.nlpConditionOnsetType = rec.get(headerIndex.get("NLP_COND_ONSET_TYPE"))
        fhxNlpDTO.nlpConditionOnsetAge = StringTo.aInteger(rec.get(headerIndex.get("NLP_COND_ONSET_AGE")))
        fhxNlpDTO.nlpConditionOnsetAgeRange = rec.get(headerIndex.get("NLP_COND_ONSET_AGE_RANGE"))
        fhxNlpDTO.nlpRelationshipCodeSystem = rec.get(headerIndex.get("NLP_RELATION_CODE_SYSTEM"))
        fhxNlpDTO.nlpRelationshipCode = rec.get(headerIndex.get("NLP_RELATION_CODE"))
        fhxNlpDTO.nlpRelationshipLabel = rec.get(headerIndex.get("NLP_RELATION_LABEL"))

        return fhxNlpDTO
    }

}

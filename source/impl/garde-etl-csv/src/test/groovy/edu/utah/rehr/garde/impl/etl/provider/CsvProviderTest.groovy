package edu.utah.rehr.garde.impl.etl.provider

import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxDTO
import edu.utah.rehr.garde.domain.core.model.dto.PatientDTO
import edu.utah.rehr.garde.domain.etl.service.ICsvService
import edu.utah.rehr.garde.impl.etl.service.CsvService
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test

import static org.junit.jupiter.api.Assertions.assertTrue

class CsvProviderTest {

    static ICsvService csvService

    @BeforeAll
    static void init() {
        csvService = new CsvService()
    }

    @Test
    void sanityTest() {
        assertTrue( 1 == 1)
    }

    @Test
    void getPatientDTOsFromCsvTest() {

        String path = "test-files/pat-demog-de-id-20220810.csv"

        PatientDTOProviderCsv patientDTOProvider = new PatientDTOProviderCsv(csvService)
        List<PatientDTO> patientDTOs = patientDTOProvider.getPatientDTOs(path)

        assertTrue(patientDTOs.size() == 1000)
    }

    @Test
    void getFamilyHxDTOsFromCsvTest() {

        String path = "test-files/pat-fhx-de-id-20220810.csv"

        FamilyHxDTOProviderCsv providerCsv = new FamilyHxDTOProviderCsv(csvService)
        List<FamilyHxDTO> fhxDTOs = providerCsv.getFamilyHxDTOs(path)

        assertTrue(fhxDTOs.size() > 1000)
    }


}

package edu.utah.rehr.garde.impl.etl.service

import edu.utah.rehr.garde.domain.core.model.dto.RegistryInstructionDTO
import org.junit.jupiter.api.Test

import java.nio.charset.Charset
import java.nio.charset.StandardCharsets

import static org.junit.jupiter.api.Assertions.assertTrue

class CsvUtilTest {

    RegistryInstructionDTO b1 = new RegistryInstructionDTO(
            "codeSystem"
            , "patId1"
            , "create"
            , "string"
            , "ele1"
            , "eleId1"
            , "string-val1"
    )
    RegistryInstructionDTO b2 = new RegistryInstructionDTO(
            "codeSystem"
            , "patId2"
            , "create"
            , "string"
            , "ele2"
            , "eleId2"
            , "string-val2"
    )
    RegistryInstructionDTO b3 = new RegistryInstructionDTO(
            "codeSystem"
            , "patId3"
            , "create"
            , "string"
            , "ele3"
            , "eleId3"
            , "string-val3"
    )

    static Charset DEFAULT_CHARSET = StandardCharsets.UTF_8

    List<RegistryInstructionDTO> getAll3() {
        List<RegistryInstructionDTO> registryInstructionDTOs = new ArrayList<>()
        registryInstructionDTOs.add(b1)
        registryInstructionDTOs.add(b2)
        registryInstructionDTOs.add(b3)
        return registryInstructionDTOs
    }

    @Test
    void testTest() {
        assertTrue(true)
    }

    @Test
    void CsvWriteNewTest() {
        String filePath = "test-files/CsvUtil-test1.csv"
        boolean append = false
        PrintWriter writer = new PrintWriter(
                new OutputStreamWriter(new FileOutputStream(filePath, append), DEFAULT_CHARSET))
        CsvUtil.writeBeansToCsv(getAll3(), writer, RegistryInstructionDTO.class)

    }

}

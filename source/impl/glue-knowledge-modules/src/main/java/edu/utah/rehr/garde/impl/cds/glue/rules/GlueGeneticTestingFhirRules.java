package edu.utah.rehr.garde.impl.cds.glue.rules;

import edu.utah.rehr.garde.domain.core.model.GlueRule;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.FamilyMemberHistoryHelperR4;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.PatientHelperR4;
import edu.utah.rehr.garde.domain.core.terminology.Code;
import edu.utah.rehr.garde.domain.core.terminology.binding.GlueTerminologyBinding;
import edu.utah.rehr.garde.domain.core.terminology.binding.NccnTerminologyBinding;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import lombok.extern.java.Log;
import org.hl7.fhir.r4.model.*;

import java.util.*;

/**
 * The rules implemented in this class are based on Ken Kawamoto's interpretation of the following journal article:
 *
 * Sturm, A. C., Knowles, J. W., Gidding, et al (2018). Clinical genetic testing for familial hypercholesterolemia:
 * JACC scientific expert panel. *Journal of the American College of Cardiology*, *72*(6).
 *
 * The rules within cover the article's "Genetic testing for familial hypercholesterolemia should be offered [when
 * these rules apply]" section.
 */

@Log
public class GlueGeneticTestingFhirRules {

    private final PatientHelperR4 patientHelper;
    private final FamilyMemberHistoryHelperR4 familyMemberHistoryHelper;
    private final ICodeService codeService;

    public GlueGeneticTestingFhirRules(ICodeService codeService) {
        this.codeService = codeService;
        this.patientHelper = new PatientHelperR4(codeService);
        this.familyMemberHistoryHelper = new FamilyMemberHistoryHelperR4(codeService);
    }

    public Map<GlueRule, Bundle> evaluate(
            Patient patient
            , List<FamilyMemberHistory> fhxList
            , List<Observation> obsList
    ) {

        Map<GlueRule, Bundle> results = new HashMap<>();
        List<Observation> resultLdlObsGte190 = new ArrayList<>();
        List<FamilyMemberHistory> resultFirstDegreeCadNoAge = new ArrayList<>();
        List<FamilyMemberHistory> resultFirstDegreePrematureCad = new ArrayList<>();

        List<Observation> resultLdlObsGte250 = new ArrayList<>();

        if (obsList != null)
        for (Observation obs : obsList) {

            Integer obsValue = getValueInteger(obs);

            if (obsValue >= 190 && obsValue < 250){
                resultLdlObsGte190.add(obs);
            } else if (obsValue >= 250) {
                resultLdlObsGte250.add(obs);
            }

        }

        if (fhxList != null)
        for (FamilyMemberHistory fhx : fhxList) {

            Set<Code> relativeCodes = familyMemberHistoryHelper.getRelativeCodings(fhx);
            Set<Code> dxCodes = familyMemberHistoryHelper.getConditionCodings(fhx);
            Integer ageOfOnset = familyMemberHistoryHelper.getAgeOfOnset(fhx);
            boolean isMaleRelative = codeService.intersects(NccnTerminologyBinding.MALE_RELATIVE_2023_CODE_SET, relativeCodes);
            boolean isFemaleRelative = codeService.intersects(NccnTerminologyBinding.FEMALE_RELATIVE_2023_CODE_SET, relativeCodes);

            if (familyMemberHistoryHelper.isFirstDegreeRelative(relativeCodes)) {

                if (codeService.intersects(GlueTerminologyBinding.CORONARY_ARTERY_DISEASE, dxCodes)) {

                    if (ageOfOnset == null) {
                        resultFirstDegreeCadNoAge.add(fhx);
                    }
                    else if ((isMaleRelative && ageOfOnset <=55)
                            || (isFemaleRelative && ageOfOnset <= 65)) {

                        resultFirstDegreePrematureCad.add(fhx);
                    }

                }
                else if (codeService.intersects(GlueTerminologyBinding.PREMATURE_CORONARY_ARTERY_DISEASE, dxCodes)) {
                    resultFirstDegreePrematureCad.add(fhx);
                }

            }
        } // for fhx

        Bundle bundle = new Bundle();
        bundle.setType(Bundle.BundleType.COLLECTION);

        if (resultLdlObsGte250.size() >= 2) {

            for (Observation observation : resultLdlObsGte250) {
                Bundle.BundleEntryComponent entry = new Bundle.BundleEntryComponent();
                entry.setResource(observation);
                bundle.addEntry(entry);
            }

            results.put(GlueRule.FH_LDL_GT_250, bundle);
        }

        if (resultLdlObsGte190.size() >= 2
                || (resultLdlObsGte190.size() + resultLdlObsGte250.size()) >= 2 // if there is one between 190 and 250 and one or more gte 250 then fire rule
                && resultFirstDegreePrematureCad.size() > 0
        ) {

            for (Observation observation : resultLdlObsGte190) {
                Bundle.BundleEntryComponent entry = new Bundle.BundleEntryComponent();
                entry.setResource(observation);
                bundle.addEntry(entry);
            }

            for (Observation observation : resultLdlObsGte250) {
                Bundle.BundleEntryComponent entry = new Bundle.BundleEntryComponent();
                entry.setResource(observation);
                bundle.addEntry(entry);
            }

            for (FamilyMemberHistory familyHistory : resultFirstDegreePrematureCad) {
                Bundle.BundleEntryComponent entry = new Bundle.BundleEntryComponent();
                entry.setResource(familyHistory);
                bundle.addEntry(entry);
            }

            results.put(GlueRule.FH_LDL_GT_190_FHX_PREMATURE_CAD, bundle);
        }

        else if (resultLdlObsGte190.size() >= 2
                && resultFirstDegreeCadNoAge.size() > 0
        ) {

            for (Observation observation : resultLdlObsGte190) {
                Bundle.BundleEntryComponent entry = new Bundle.BundleEntryComponent();
                entry.setResource(observation);
                bundle.addEntry(entry);
            }

            for (FamilyMemberHistory familyHistory : resultFirstDegreeCadNoAge) {
                Bundle.BundleEntryComponent entry = new Bundle.BundleEntryComponent();
                entry.setResource(familyHistory);
                bundle.addEntry(entry);
            }

            results.put(GlueRule.FH_LDL_GT_190_FHX_CAD_NO_AGE, bundle);
        }

        return results;
    }

    public Integer getValueInteger(Observation observation) {
        if (observation == null || !observation.hasValueIntegerType()) {
            return null;
        }
        return observation.getValueIntegerType().getValue();
    }
}

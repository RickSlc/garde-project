package edu.utah.rehr.garde.impl.cds.glue.rules;

import edu.utah.rehr.garde.domain.core.model.GlueRule;
import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxDTO;
import edu.utah.rehr.garde.domain.core.model.dto.ObservationDTO;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.FamilyMemberHistoryHelperR4;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.GDtoToFhirMapperR4;
import edu.utah.rehr.garde.domain.core.terminology.Code;
import edu.utah.rehr.garde.domain.core.terminology.binding.GlueTerminologyBinding;
import edu.utah.rehr.garde.domain.core.terminology.binding.NccnTerminologyBinding;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import lombok.extern.java.Log;
import org.hl7.fhir.r4.model.Bundle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log
public class GlueGeneticTestingRules {

    private final ICodeService codeService;
    private final FamilyMemberHistoryHelperR4 familyMemberHistoryHelper;

    public GlueGeneticTestingRules(ICodeService codeService) {
        this.codeService = codeService;
        this.familyMemberHistoryHelper = new FamilyMemberHistoryHelperR4(codeService);
    }

    public Map<GlueRule, Bundle> evaluate(
            List<FamilyHxDTO> fhxList
            , List<ObservationDTO> obsList
    ) {

        Map<GlueRule, Bundle> results = new HashMap<>();
        List<ObservationDTO> resultLdlObsGte190 = new ArrayList<>();
        List<FamilyHxDTO> resultFirstDegreeCadNoAge = new ArrayList<>();
        List<FamilyHxDTO> resultFirstDegreePrematureCad = new ArrayList<>();

        List<ObservationDTO> resultLdlObsGte250 = new ArrayList<>();

        if (obsList != null)
            for (ObservationDTO obs : obsList) {

                Code obsCode = obs.toObservationCode();
                if (!codeService.isSetMember(GlueTerminologyBinding.LDL_C, obsCode)) {
                    log.warning("UNMAPPED OR NON-LDL C CODE DISCOVERED: " + obsCode.getCode() + " observationId: " + obs.getObservationId());
                    continue; // skip if non-LDL code slipped through
                }

                if (obs.getObservationValueInteger() >= 190 && obs.getObservationValueInteger() < 250) {
                    resultLdlObsGte190.add(obs);
                } else if (obs.getObservationValueInteger() >= 250) {
                    resultLdlObsGte250.add(obs);
                }

            }

        if (fhxList != null)
            for (FamilyHxDTO fhx : fhxList) {

                Code relativeCode = fhx.toRelationCode();
                Code dxCode = fhx.toConditionCode();
                Integer ageOfOnset = fhx.getConditionOnsetAge();

                boolean isMaleRelative = codeService.isSetMember(NccnTerminologyBinding.MALE_RELATIVE_2023_CODE_SET, relativeCode);
                boolean isFemaleRelative = codeService.isSetMember(NccnTerminologyBinding.FEMALE_RELATIVE_2023_CODE_SET, relativeCode);

                if (familyMemberHistoryHelper.isFirstDegreeRelative(relativeCode)) {

                    if (codeService.isSetMember(GlueTerminologyBinding.CORONARY_ARTERY_DISEASE, dxCode)) {

                        if (ageOfOnset == null) {
                            resultFirstDegreeCadNoAge.add(fhx);
                        } else if ((isMaleRelative && ageOfOnset <= 55)
                                || (isFemaleRelative && ageOfOnset <= 65)) {

                            resultFirstDegreePrematureCad.add(fhx);
                        }

                    } else if (codeService.isSetMember(GlueTerminologyBinding.PREMATURE_CORONARY_ARTERY_DISEASE, dxCode)) {
                        resultFirstDegreePrematureCad.add(fhx);
                    }

                }
            } // for fhx

        Bundle bundle = new Bundle();
        bundle.setType(Bundle.BundleType.COLLECTION);

        if (resultLdlObsGte250.size() >= 2) {

            for (ObservationDTO obsDto : resultLdlObsGte250) {
                Bundle.BundleEntryComponent entry = new Bundle.BundleEntryComponent();
                entry.setResource(GDtoToFhirMapperR4.dtoToObservation(obsDto));
                bundle.addEntry(entry);
            }

            results.put(GlueRule.FH_LDL_GT_250, bundle);
        }

        if (    // if there are >= two between 190 and 250,
                // or one between 190 and 250 and one more gte 250 then fire rule,
                // and one premature CAD FHx
                (resultLdlObsGte190.size() >= 2 || (resultLdlObsGte190.size() + resultLdlObsGte250.size()) >= 2)
                        && resultFirstDegreePrematureCad.size() > 0
        ) {

            for (ObservationDTO obsDto : resultLdlObsGte190) {
                Bundle.BundleEntryComponent entry = new Bundle.BundleEntryComponent();
                entry.setResource(GDtoToFhirMapperR4.dtoToObservation(obsDto));
                bundle.addEntry(entry);
            }

            for (ObservationDTO obsDto : resultLdlObsGte250) {
                Bundle.BundleEntryComponent entry = new Bundle.BundleEntryComponent();
                entry.setResource(GDtoToFhirMapperR4.dtoToObservation(obsDto));
                bundle.addEntry(entry);
            }

            for (FamilyHxDTO fhxDto : resultFirstDegreePrematureCad) {
                Bundle.BundleEntryComponent entry = new Bundle.BundleEntryComponent();
                entry.setResource(GDtoToFhirMapperR4.dtoToFamilyMemberHistory(fhxDto));
                bundle.addEntry(entry);
            }

            results.put(GlueRule.FH_LDL_GT_190_FHX_PREMATURE_CAD, bundle);

        } else if (resultLdlObsGte190.size() >= 2 // or when 2 get 190 and none gte 250 and 1 or mor fhx
                && resultFirstDegreeCadNoAge.size() > 0
        ) {

            for (ObservationDTO obsDto : resultLdlObsGte190) {
                Bundle.BundleEntryComponent entry = new Bundle.BundleEntryComponent();
                entry.setResource(GDtoToFhirMapperR4.dtoToObservation(obsDto));
                bundle.addEntry(entry);
            }

            for (FamilyHxDTO fhxDto : resultFirstDegreeCadNoAge) {
                Bundle.BundleEntryComponent entry = new Bundle.BundleEntryComponent();
                entry.setResource(GDtoToFhirMapperR4.dtoToFamilyMemberHistory(fhxDto));
                bundle.addEntry(entry);
            }

            results.put(GlueRule.FH_LDL_GT_190_FHX_CAD_NO_AGE, bundle);
        }

        return results;
    }

}

package edu.utah.rehr.garde.impl.cds.glue.rules;

import edu.utah.rehr.garde.domain.core.model.GlueRule;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.FamilyMemberHistoryHelperR4;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.ObservationHelperR4;
import edu.utah.rehr.garde.domain.core.terminology.Code;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import edu.utah.rehr.garde.domain.core.terminology.provider.csv.TerminologyMapsProviderCsv;
import edu.utah.rehr.garde.domain.core.terminology.service.CodeServiceByMaps;
import lombok.extern.java.Log;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.FamilyMemberHistory;
import org.hl7.fhir.r4.model.Observation;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.Assert.*;

@Log
public class TestGlueGeneticTestingRules {

    private final ICodeService codeService;
    private final GlueGeneticTestingFhirRules glueGeneticTestingRules;
    private final FamilyMemberHistoryHelperR4 familyMemberHistoryHelper;

    private final FamilyMemberHistory fhxPrematureCad;
    private final FamilyMemberHistory fhxCad;

    private final FamilyMemberHistory fhxPrematureCadEhr;
    private final FamilyMemberHistory fhxCadEhr;

    public TestGlueGeneticTestingRules() {
        this.codeService = new CodeServiceByMaps(
                TerminologyMapsProviderCsv.getCodeSetMap("classpath:terminology/garde-code-sets.csv")
                , TerminologyMapsProviderCsv.getCodeBindingsMap("classpath:terminology/glue-standard-codes.csv")
                , TerminologyMapsProviderCsv.getCodeBindingsMap("classpath:terminology/glue-ehr-codes-ut.csv")
                , TerminologyMapsProviderCsv.getUriBindingsMap("classpath:terminology/glue-uris.csv")
        );
        this.familyMemberHistoryHelper = new FamilyMemberHistoryHelperR4(codeService);
        this.glueGeneticTestingRules = new GlueGeneticTestingFhirRules(codeService);

        this.fhxPrematureCad = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "patId_1",
                new Code("http://hl7.org/fhir/ValueSet/v3-FamilyMember","NMTH","natural mother"), // Relation
                new Code("http://snomed.info/sct","429007001","Premature coronary artery disease"), // Dx
                "CODED_FHX",
                40,
                null, null);

        this.fhxCad = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "patId_1",
                new Code("http://hl7.org/fhir/ValueSet/v3-FamilyMember","NMTH","natural mother"), // Relation
                new Code("http://snomed.info/sct","53741008","Coronary artery disease"), // Dx
                "CODED_FHX",
                null,
                null, null);

        Code prematureCadCode = new Code(
                "http://epic.com/uhealth/clarity/zc_medical_hx"
                , "2100000018"
                ,"Premature Coronary Artery disease"
        );

        Code cadCode = new Code(
                "http://epic.com/uhealth/clarity/zc_medical_hx"
                , "156"
                ,"Coronary Artery Disease"
        );

        this.fhxPrematureCadEhr = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "patId_1",
                new Code("http://epic.com/uhealth/clarity/zc_relation","3","Sister"), // Relation
                prematureCadCode, // Dx
                "CODED_FHX",
                40,
                null, null);

        this.fhxCadEhr = familyMemberHistoryHelper.toFamilyMemberHistory(
                "100", new Date(), "patId_1",
                new Code("http://epic.com/uhealth/clarity/zc_relation","3","Sister"), // Relation
                cadCode, // Dx
                "CODED_FHX",
                null,
                null, null);
    }

    private final Code ldlCode1 = new Code(
            "http://loinc.org"
            ,"13457-7"
            ,"LDL Cholesterol (Mass/Vol in Serum or Plasma)"
    );

    private final Code ldlCode2 = new Code(
            "http://loinc.org"
            ,"18262-6"
            ,"LDL Cholesterol (Calc) [Mass/Vol in Serum or Plasma]"
    );

    private final Observation p1_obs1 = ObservationHelperR4.toObservation(
            "patId_1"
            , ldlCode1
            , 250
            , new Date()
    );

    private final Observation p1_obs2 = ObservationHelperR4.toObservation(
            "patId_1"
            , ldlCode2
            , 250
            , new Date()
    );

    private final Observation p2_obs1 = ObservationHelperR4.toObservation(
            "patId_2"
            , ldlCode1
            , 190
            , new Date()
    );

    private final Observation p2_obs2 = ObservationHelperR4.toObservation(
            "patId_2"
            , ldlCode2
            , 190
            , new Date()
    );

    @Test
    public void sanity() {
        log.info("Sanity test -- testing is happening!");
        assertEquals(1 + 1, 2);
    }

    @Test
    public void testLdlTwo250() {
        log.info("LDL 2 at 250 - true");

        List<Observation> observations = new ArrayList<>();
        observations.add(p1_obs1);
        observations.add(p1_obs2);

        Map<GlueRule, Bundle> result = glueGeneticTestingRules.evaluate(null,null, observations);
        assertTrue(result.containsKey(GlueRule.FH_LDL_GT_250));
    }

    @Test
    public void testLdlOnlyOne250Fails() {
        log.info("LDL Only one - fail");

        List<Observation> observations = new ArrayList<>();
        observations.add(p1_obs1);

        Map<GlueRule, Bundle> result = glueGeneticTestingRules.evaluate(null,null, observations);
        assertTrue(result.isEmpty());
    }

    @Test
    public void testLdl190AndFhxTrue() {
        log.info("LDL 2 190s and FHX premature CD - true");

        List<Observation> observations = new ArrayList<>();
        observations.add(p2_obs1);
        observations.add(p2_obs2);

        List<FamilyMemberHistory> fhxs = new ArrayList<>();
        fhxs.add(fhxPrematureCad);

        Map<GlueRule, Bundle> result = glueGeneticTestingRules.evaluate(null,fhxs, observations);
        assertTrue(result.containsKey(GlueRule.FH_LDL_GT_190_FHX_PREMATURE_CAD));
    }

    @Test
    public void testLdl190AndFhxEhrTrue() {
        log.info("LDL 2 190s and FHX premature CAD EHR codes - true");

        List<Observation> observations = new ArrayList<>();
        observations.add(p2_obs1);
        observations.add(p2_obs2);

        List<FamilyMemberHistory> fhxs = new ArrayList<>();
        fhxs.add(fhxPrematureCadEhr);

        Map<GlueRule, Bundle> result = glueGeneticTestingRules.evaluate(null,fhxs, observations);
        assertTrue(result.containsKey(GlueRule.FH_LDL_GT_190_FHX_PREMATURE_CAD));
    }


}

package edu.utah.rehr.garde.impl.cdshooks;

import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.model.GCohort;
import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxDTO;
import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxNlpDTO;
import edu.utah.rehr.garde.domain.core.model.dto.PatientDTO;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.FamilyMemberHistoryHelperR4;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.PatientHelperR4;
import edu.utah.rehr.garde.domain.core.utils.ListPager;
import edu.utah.rehr.garde.domain.etl.provider.IFamilyHxDTOProvider;
import edu.utah.rehr.garde.domain.etl.provider.IPatientDTOProvider;
import edu.utah.rehr.garde.domain.cdshooks.ICdsHooksRequestFactory;
import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.FamilyMemberHistory;
import org.hl7.fhir.r4.model.Patient;
import org.opencds.hooks.model.context.HookContext;
import org.opencds.hooks.model.context.WritableHookContext;
import org.opencds.hooks.model.request.CdsRequest;
import org.opencds.hooks.model.request.WritableCdsRequest;
import org.springframework.util.StopWatch;

import java.io.BufferedReader;
import java.io.FileReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.logging.Level;

@Log
public class NccnCdsHooksRequestFactoryR4 implements ICdsHooksRequestFactory {

    private final PatientHelperR4 patientHelper;
    private final FamilyMemberHistoryHelperR4 familyMemberHistoryHelper;
    private final IPatientDTOProvider patientDTOProvider;
    private final IFamilyHxDTOProvider familyHxDTOProvider;

    private Map<String, List<PatientDTO>> patientDtoCache = new HashMap<>();
    private Map<String, Map<String, List<FamilyHxDTO>>> familyHxDtoCache = new HashMap<>();

    private Map<String, Map<String, List<FamilyHxNlpDTO>>> familyHxNlpDtoCache = new HashMap<>();

    public NccnCdsHooksRequestFactoryR4(
            PatientHelperR4 patientHelper
            , FamilyMemberHistoryHelperR4 familyMemberHistoryHelper
            , IPatientDTOProvider patientDTOProvider
            , IFamilyHxDTOProvider familyHxDTOProvider
    ) {
        this.patientHelper = patientHelper;
        this.familyMemberHistoryHelper = familyMemberHistoryHelper;
        this.patientDTOProvider = patientDTOProvider;
        this.familyHxDTOProvider = familyHxDTOProvider;
    }

    @Override
    public void flush() {
        patientDtoCache = new HashMap<>();
        familyHxDtoCache = new HashMap<>();
        familyHxNlpDtoCache = new HashMap<>();
    }

    @Override
    public void flushInstance(String instanceId) {
        patientDtoCache.remove(instanceId);
        familyHxDtoCache.remove(instanceId);
        familyHxNlpDtoCache.remove(instanceId);
    }

    /**
     * Creates CdsRequests for all of the patients in the population.
     */
    @Override
    public UResult<List<CdsRequest>> create() {
        return UResult.errorResult(new UnsupportedOperationException("CdsHooksRequestFactoryFamilyHxCancerRiskR4.create() not currently supported."));
    }

    /**
     * Create CdsRequests from files with PatientDTO and FamilyHxDTO
     *
     * @param params bucket of key value pairs with parameters for the evaluation
     * @return UResult for error handling returning CdsRequests
     */
    @Override
    public UResult<List<CdsRequest>> create(Map<String, String> params) {
        checkParams(params);

        String patientDTOFilePath = params.get("patientDTO_filePath");
        String familyHxDTOFilePath = params.get("familyHxDTO_filePath");
        String fhirServiceEndpoint = params.get("fhirServiceEndpoint");
        String outputType = params.get("outputType");
        String instanceId = params.get("instanceId");

        boolean usesNlp = hasNlpFacts(familyHxDTOFilePath);

        try {

            // check cache
            List<PatientDTO> patientDTOs = patientDtoCache.get(instanceId);
            if (patientDTOs == null) {
                log.info("RETRIEVING PATIENT DTOs");
                patientDTOs = patientDTOProvider.getPatientDTOs(patientDTOFilePath);
                patientDtoCache.put(instanceId, patientDTOs);
                log.info(patientDTOs.size() + " PATIENT DTOs RETRIEVED");
            }

            log.info("BUILD FACTORY SETTINGS - patientDTOFilePath: " + patientDTOFilePath + ", familyHxDTOFilePath: " + familyHxDTOFilePath + ", fhirServiceEndpoint: " + fhirServiceEndpoint + ", usesNlp: " + usesNlp);
            List<CdsRequest> cdsRequests;
            if (usesNlp) {
                log.info("BUILDING CDS HOOKS REQUESTS WITH NLP-DERIVED FACTS");
                cdsRequests = getNlpCdsRequests(patientDTOs, instanceId, familyHxDTOFilePath, fhirServiceEndpoint, outputType);
            } else {
                log.info("BUILDING CDS HOOKS REQUESTS");
                cdsRequests = getCdsRequests(patientDTOs, instanceId, familyHxDTOFilePath, null,fhirServiceEndpoint, outputType);
            }
            log.info("BUILDING CDS HOOK REQUESTS - COMPLETE");

            return UResult.successResult(cdsRequests);

        } catch (Exception e) {
            return UResult.errorResult(e);
        }

    }

    private static void checkParams(Map<String, String> params) {
        if (StringUtils.isEmpty(params.get("patientDTO_filePath"))) {
            throw new IllegalArgumentException("patientDTO_filePath cannot be null or empty");
        }
        if (StringUtils.isEmpty(params.get("familyHxDTO_filePath"))) {
            throw new IllegalArgumentException("familyHxDTO_filePath cannot be null or empty");
        }
        if (StringUtils.isEmpty(params.get("outputType"))) {
            throw new IllegalArgumentException("outputType cannot be null or empty");
        }
        if (StringUtils.isEmpty(params.get("fhirServiceEndpoint"))) {
            throw new IllegalArgumentException("fhirServiceEndpoint cannot be null or empty");
        }
    }

    boolean hasNlpFacts(String filePath) {

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
            String firstLine = bufferedReader.readLine();
            boolean answer = firstLine.toUpperCase().contains("NLP");
            bufferedReader.close();
            return answer;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    @Override
    public UResult<List<CdsRequest>> create(String serviceId, Integer page, Integer pageSize, Map<String, String> params, List<PatientDTO> patientDTOs) {
        checkParams(params);

        StopWatch requestStopWatch = new StopWatch();
        requestStopWatch.start();

        String patientDTOFilePath = params.get("patientDTO_filePath");
        String familyHxDTOFilePath = params.get("familyHxDTO_filePath");
        String fhirServiceEndpoint = params.get("fhirServiceEndpoint");
        String outputType = params.get("outputType");
        String instanceId = params.get("instanceId");
        boolean usesNlp = "true".equals(params.get("useNlp"));

        List<PatientDTO> patientDTOListToProcess = ListPager.getPage(patientDTOs, pageSize, page);

        log.info("PROCESSING PATIENT DATA PAGE " + page + " with " + patientDTOListToProcess.size() + " PATIENTS");
        log.info("BUILD FACTORY SETTINGS - patientDTOFilePath: " + patientDTOFilePath + ", familyHxDTOFilePath: " +
                familyHxDTOFilePath + ", fhirServiceEndpoint: " + fhirServiceEndpoint + ", usesNlp: " + usesNlp);

        List<CdsRequest> cdsRequests;

        if (usesNlp) {
            log.info("BUILDING CDS HOOKS REQUESTS WITH NLP-DERIVED FACTS");
            cdsRequests = getNlpCdsRequests(patientDTOListToProcess, instanceId, familyHxDTOFilePath, fhirServiceEndpoint, outputType);
        } else {
            log.info("BUILDING CDS HOOKS REQUESTS");
            cdsRequests = getCdsRequests(patientDTOListToProcess, instanceId, familyHxDTOFilePath, null, fhirServiceEndpoint, outputType);
        }

        requestStopWatch.stop();
        log.info("BUILDING CDS HOOK REQUESTS - PAGE " + page + " COMPLETED IN " + requestStopWatch.getTotalTimeSeconds() + " (SEC)");

        return UResult.successResult(cdsRequests);
    }

    private List<CdsRequest> getNlpCdsRequests(List<PatientDTO> patientDTOs, String instanceId, String familyHxDTOFilePath, String fhirServiceEndpoint, String outputType) {
        List<CdsRequest> cdsRequests = new ArrayList<>();

        if (!familyHxNlpDtoCache.containsKey(instanceId)) { // if not in map, retrieve and put in map
            log.info("RETRIEVING FHX NLP DTOs");
            familyHxDTOProvider.setSource(familyHxDTOFilePath);
            Map<String, List<FamilyHxNlpDTO>> fHxNlpDTOMap = familyHxDTOProvider.getFamilyHxNlpDTOMap();
            familyHxNlpDtoCache.put(instanceId, fHxNlpDTOMap);
            log.info("RETRIEVED FHX NLP DTOs FOR " + fHxNlpDTOMap.size() + " PATIENTS");
        }

        Map<String, List<FamilyHxNlpDTO>> fHxNlpDTOMap = familyHxNlpDtoCache.get(instanceId);

        log.info("BUILDING CDS HOOK REQUESTS FROM NLP-DERIVED FACTS");
        for (PatientDTO patDTO : patientDTOs) {
            if (fHxNlpDTOMap.get(patDTO.getPatientId()) != null) {

                Patient fhirPatient = patientHelper.toPatient(patDTO);
                List<FamilyMemberHistory> fhxNlpList = new ArrayList<>();

                for (FamilyHxNlpDTO fhxNlpDTO : fHxNlpDTOMap.get(patDTO.getPatientId())) {
                    FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(fhxNlpDTO);
                    fhxNlpList.add(fhx);
                }

                CdsRequest cdsRequest = new WritableCdsRequest();
                cdsRequest.setHook("patient-view");
//                cdsRequest.setHook("scheduled-eval");
                try {
                    cdsRequest.setFhirServer(new URL(fhirServiceEndpoint));
                } catch (MalformedURLException e) {
                    log.warning("BUILD FACTORY SETTINGS - familyHxDTOFilePath: " + familyHxDTOFilePath + ", fhirServerEndpoint: " + fhirServiceEndpoint);
                    log.log(Level.WARNING, "Exception setting CdsHook request fhir server URL", e);
                }
                cdsRequest.setHookInstance(UUID.randomUUID().toString());

                HookContext context = new WritableHookContext();
                context.add("patientId", patDTO.getPatientId());
                context.add("outputType", outputType);

                cdsRequest.setContext(context);

                Bundle fhxBundle = getBundle(fhxNlpList);

                cdsRequest.addPrefetchResource("patient", fhirPatient);
                cdsRequest.addPrefetchResource("patientFHxBundle", fhxBundle);

                cdsRequests.add(cdsRequest);
            }
        }
        log.info(cdsRequests.size() + " CDS HOOK REQUESTS BUILT");
        return cdsRequests;
    }

    private List<CdsRequest> getCdsRequests(List<PatientDTO> patientDTOs, String instanceId, String familyHxDTOFilePath, String patientLdlObsDTOFilePath, String fhirServiceEndpoint, String outputType) {

        List<CdsRequest> cdsRequests = new ArrayList<>();

        if (!familyHxDtoCache.containsKey(instanceId)) {
            log.info("RETRIEVING FHX DTOs");
            familyHxDTOProvider.setSource(familyHxDTOFilePath);
            Map<String, List<FamilyHxDTO>> newFHxDTOMap = familyHxDTOProvider.getFamilyHxDTOMap();
            familyHxDtoCache.put(instanceId, newFHxDTOMap);
            log.info("RETRIEVED FHX DTOs FOR " + newFHxDTOMap.size() + " PATIENTS");
        }

        Map<String, List<FamilyHxDTO>> fHxDTOMap = familyHxDtoCache.get(instanceId);

        log.info(fHxDTOMap.size() + " IN POPULATION HAVE FAMILY HX" );
        log.info("BUILD REQUESTS - EXTRACTING FAMILY HX FOR " + patientDTOs.size() + " PEOPLE; " + fHxDTOMap.size() + " HAVE FAMILY HX" );
        for (PatientDTO patDTO : patientDTOs) {

            if (fHxDTOMap.get(patDTO.getPatientId()) == null) { // no family history, next.
                continue;
            }

            Patient fhirPatient = patientHelper.toPatient(patDTO);
            List<FamilyMemberHistory> fhxList = new ArrayList<>();

            for (FamilyHxDTO fhxDTO : fHxDTOMap.get(patDTO.getPatientId())) {
                FamilyMemberHistory fhx = familyMemberHistoryHelper.toFamilyMemberHistory(fhxDTO);
                fhxList.add(fhx);
            }

            CdsRequest cdsRequest = new WritableCdsRequest();
            cdsRequest.setHook("patient-view");
//                cdsRequest.setHook("scheduled-eval");
            try {
                cdsRequest.setFhirServer(new URL(fhirServiceEndpoint));
            } catch (MalformedURLException e) {
                log.warning("BUILD FACTORY SETTINGS - familyHxDTOFilePath: " + familyHxDTOFilePath + ", fhirServerEndpoint: " + fhirServiceEndpoint);
                log.log(Level.WARNING, "Exception setting CdsHook request fhir server URL", e);
            }
            cdsRequest.setHookInstance(UUID.randomUUID().toString());

            HookContext context = new WritableHookContext();
            context.add("patientId", patDTO.getPatientId());
            context.add("outputType", outputType);
            String delimitedPreviouslyMetCriteria = patDTO.getPreviouslyMetCriteriaCodesDelimitedString("\\|");
            if (delimitedPreviouslyMetCriteria != null) {
                context.add("previouslyMetCriteriaList", delimitedPreviouslyMetCriteria);
            }

            cdsRequest.setContext(context);

            Bundle fhxBundle = getBundle(fhxList);

            cdsRequest.addPrefetchResource("patient", fhirPatient);
            cdsRequest.addPrefetchResource("patientFHxBundle", fhxBundle);

            cdsRequests.add(cdsRequest);
        }
        log.info(cdsRequests.size() + " CDS HOOK REQUESTS BUILT");
        return cdsRequests;
    }

    @Override
    public UResult<List<CdsRequest>> create(GCohort populationCohort, GCohort targetCohort, Integer version) {
        return UResult.errorResult(new UnsupportedOperationException("CdsHooksRequestFactoryFamilyHxCancerRiskR4.create(Cohort populationCohort, Cohort targetCohort, Integer version) not currently supported."));
    }

    private Bundle getBundle(List<FamilyMemberHistory> fhxs) {

        Bundle bundle = new Bundle();
        bundle.setType(Bundle.BundleType.BATCH);

        for (FamilyMemberHistory fhx : fhxs) {
            bundle.addEntry()
                    .setFullUrl(fhx.getId())
                    .setResource(fhx)
            ;
        }

        return bundle;
    }

}

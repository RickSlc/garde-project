package edu.utah.rehr.garde.impl.cdshooks;

import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.model.dto.PatientDTO;
import edu.utah.rehr.garde.domain.cdshooks.ICdsHooksRequestFactory;
import lombok.NonNull;
import lombok.extern.java.Log;
import org.opencds.hooks.model.request.CdsRequest;

import java.util.List;
import java.util.Map;

@Log
public class NccnCdsHooksRequestFactory {
    
    Map<String, ICdsHooksRequestFactory> factories;

    public NccnCdsHooksRequestFactory(Map<String, ICdsHooksRequestFactory> factories) {
        this.factories = factories;
    }

    public UResult<List<CdsRequest>> getCdsHooksRequests(
            @NonNull String serviceId
            , @NonNull Integer page
            , @NonNull Integer pageSize
            , @NonNull Map<String,String> params
            , @NonNull List<PatientDTO> patientDTOs) {

        ICdsHooksRequestFactory factory = factories.get(serviceId);

        if (page == 1) {
            factory.flush(); // clear cache based on file name - same file may have been updated/fixed for rerun.
        }

        if (factory == null) {
            return UResult.errorResult(new Exception("NccnCdsHooksRequestFactory " + serviceId + " NOT RESOLVABLE."));
        }

        return factory.create(serviceId, page, pageSize, params, patientDTOs);
    }

    public void flushInstance(String serviceId, String instanceId) {
        log.info("FLUSHING CACHE FOR " + serviceId + " INSTANCE " + instanceId);
        ICdsHooksRequestFactory factory = factories.get(serviceId);
        factory.flushInstance(instanceId);
    }

}

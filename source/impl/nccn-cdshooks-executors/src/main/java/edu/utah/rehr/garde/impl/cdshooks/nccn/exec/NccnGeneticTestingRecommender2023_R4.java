package edu.utah.rehr.garde.impl.cdshooks.nccn.exec;

import edu.utah.rehr.garde.domain.core.model.fhir.r4.FamilyMemberHistoryHelperR4;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.PatientHelperR4;
import edu.utah.rehr.garde.domain.core.terminology.binding.NccnTerminologyBinding;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import edu.utah.rehr.garde.domain.ehr.model.SmartDataElementMetaList;
import edu.utah.rehr.garde.impl.cdshooks.nccn.rules.FamilialBreastCancerRiskDetection2023Rules;
import edu.utah.rehr.garde.impl.cdshooks.nccn.rules.FamilialColorectalCancerRiskDetection2023Rules;
import edu.utah.rehr.garde.impl.cdshooks.nccn.helper.CdsHooksHelper;
import edu.utah.rehr.garde.impl.cdshooks.nccn.helper.CdsHooksResponseGardePatientMetCriteriaAssembler;
import edu.utah.rehr.garde.impl.cdshooks.nccn.helper.CdsHooksResponseGardeRegistryAssembler;
import lombok.extern.java.Log;
import org.hl7.fhir.r4.model.FamilyMemberHistory;
import org.hl7.fhir.r4.model.Patient;
import org.opencds.hooks.engine.api.CdsHooksEvaluationContext;
import org.opencds.hooks.engine.api.CdsHooksExecutionEngine;
import org.opencds.hooks.model.request.CdsRequest;
import org.opencds.hooks.model.response.Card;
import org.opencds.hooks.model.response.CdsResponse;
import org.opencds.hooks.model.response.Indicator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

/**
 *
 * @author RickSlc CDSHooks implementation for early onset breast cancer risk detection.
 */
@Log
public class NccnGeneticTestingRecommender2023_R4 implements CdsHooksExecutionEngine {
    public final String PATIENT_PREFETCH_KEY = "patient";
    public final String FHX_BUNDLE_PREFETCH_KEY = "patientFHxBundle";


    //    private final R4P
    private String NCCN_GENETIC_ASSESSMENT_GUIDELINE_URI;
    private String EHR_PAT_ID_CODE_SYSTEM;

    private FamilialBreastCancerRiskDetection2023Rules breastCancerRules;
    private FamilialColorectalCancerRiskDetection2023Rules colorectalCancerRules;
    private CdsHooksResponseGardePatientMetCriteriaAssembler cdsResponseAssembler;
    private CdsHooksResponseGardeRegistryAssembler cdsHooksResponseRegistryAssembler;

    public NccnGeneticTestingRecommender2023_R4(ICodeService codeService, SmartDataElementMetaList sdeMetaList) {

        FamilyMemberHistoryHelperR4 familyMemberHistoryHelperR4 = new FamilyMemberHistoryHelperR4(codeService);
        PatientHelperR4 patientHelperR4 = new PatientHelperR4(codeService);

        try {
            this.breastCancerRules = new FamilialBreastCancerRiskDetection2023Rules(codeService, familyMemberHistoryHelperR4, patientHelperR4);
            this.colorectalCancerRules = new FamilialColorectalCancerRiskDetection2023Rules(codeService, familyMemberHistoryHelperR4);
        } catch (Exception e) {
            log.log(Level.SEVERE, "Failed to instantiate rules.", e);
            return;
        }

        NCCN_GENETIC_ASSESSMENT_GUIDELINE_URI = codeService.getUri(NccnTerminologyBinding.NCCN_GENETIC_ASSESSMENT_GUIDELINE_URI);
        EHR_PAT_ID_CODE_SYSTEM = codeService.getCodeSystem(NccnTerminologyBinding.EHR_FHX_DX_CODE_SYSTEM);

        this.cdsResponseAssembler = new CdsHooksResponseGardePatientMetCriteriaAssembler(familyMemberHistoryHelperR4);

        if (!sdeMetaList.getSdes().isEmpty()) {
            this.cdsHooksResponseRegistryAssembler = new CdsHooksResponseGardeRegistryAssembler(codeService, sdeMetaList);
        }
    }

    @Override
    public CdsResponse evaluate(CdsRequest cdsRequest, CdsHooksEvaluationContext cdsHooksEvaluationContext) {

        // Get data from request
        Patient patient = CdsHooksHelper.getPatientPrefetch(cdsRequest, PATIENT_PREFETCH_KEY);
        String patientId = CdsHooksHelper.getPatientId(cdsRequest);
        String outputType = CdsHooksHelper.getOutputType(cdsRequest);
        List<FamilyMemberHistory> fhxList = CdsHooksHelper.getFhxBundlePrefetch(cdsRequest, FHX_BUNDLE_PREFETCH_KEY);

        if (patient == null || patientId == null) {
            CdsResponse cdsResponse = new CdsResponse();
            Card responseCard = new Card();
            responseCard.setSummary("PATIENT DATA PROCESSING ERROR IN EVALUATE");
            responseCard.setIndicator(Indicator.INFO);
            cdsResponse.addCard(responseCard);
        }

        // Execute NCCN breast and colorectal rules
        Map<String, List<FamilyMemberHistory>> metNccnCriteriaRuleMap = new HashMap<>(breastCancerRules.evaluate(patient, fhxList));
        metNccnCriteriaRuleMap.putAll(colorectalCancerRules.evaluate(fhxList));

        if ("MET_CRITERIA_OUTPUT_TYPE".equals(outputType)) {

            return cdsResponseAssembler.assemble(cdsRequest
                    , EHR_PAT_ID_CODE_SYSTEM
                    , NCCN_GENETIC_ASSESSMENT_GUIDELINE_URI
                    , metNccnCriteriaRuleMap);
        }

        if ("REGISTRY_INSTRUCTION_OUTPUT_TYPE".equals(outputType)) { // if configured

            if (cdsHooksResponseRegistryAssembler == null) {
                throw new IllegalArgumentException("OUTPUT TYPE " + outputType + " REQUIRES SDE CONFIGURATION.");
            }

            return cdsHooksResponseRegistryAssembler.assemble(
                    cdsRequest
                    , NCCN_GENETIC_ASSESSMENT_GUIDELINE_URI
                    , metNccnCriteriaRuleMap);
        }

        throw new IllegalArgumentException("OUTPUT TYPE " + outputType + " NOT SUPPORTED.");
    }

}

package edu.utah.rehr.garde.impl.cdshooks.nccn.helper;

import edu.utah.rehr.garde.domain.core.model.Nccn2023Rule;
import edu.utah.rehr.garde.domain.core.model.NccnRule;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.PatientHelperR4;
import edu.utah.rehr.garde.domain.core.terminology.binding.NccnTerminologyBinding;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import edu.utah.rehr.garde.domain.core.utils.SetUtils;
import edu.utah.rehr.garde.domain.ehr.model.SmartDataElement;
import edu.utah.rehr.garde.domain.ehr.model.SmartDataElementMetaList;
import lombok.extern.java.Log;
import org.hl7.fhir.r4.model.*;
import org.opencds.hooks.model.request.CdsRequest;
import org.opencds.hooks.model.response.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Log
public class CdsHooksResponseGardeRegistryAssembler {

    private final String NCCN_GENETIC_ASSESSMENT_GUIDELINE_URI;
    private final SmartDataElementMetaList.SmartDataElementMeta metCriteriaDateSdeMeta;
    private final SmartDataElementMetaList.SmartDataElementMeta metCriteriaSdeMeta;
    private final SmartDataElementMetaList.SmartDataElementMeta metCriteriaRulesSdeMeta;

    private final PatientHelperR4 patientHelperR4;

    public CdsHooksResponseGardeRegistryAssembler(ICodeService codeService, SmartDataElementMetaList sdeMetaList) {

        this.patientHelperR4 =  new PatientHelperR4(codeService);

        this.NCCN_GENETIC_ASSESSMENT_GUIDELINE_URI = codeService.getUri(NccnTerminologyBinding.NCCN_GENETIC_ASSESSMENT_GUIDELINE_URI);

        if (sdeMetaList.getSdes().isEmpty()) {
            throw new RuntimeException("SDEs ARE NOT MAPPED - REGISTRY OUTPUT REQUIRES SDE CONFIG.");
        }

        Optional<SmartDataElementMetaList.SmartDataElementMeta> oMetCriteriaDateSdeMeta = sdeMetaList.findSmartDataElementMetaByKey(SmartDataElement.NCCN_GENETIC_COUNSELING_CRITERIA_MET_DATE.getKey());
        Optional<SmartDataElementMetaList.SmartDataElementMeta> oMetCriteriaSdeMeta = sdeMetaList.findSmartDataElementMetaByKey(SmartDataElement.NCCN_GENETIC_COUNSELING_CRITERIA_MET.getKey());
        Optional<SmartDataElementMetaList.SmartDataElementMeta> oMetCriteriaRulesSdeMeta = sdeMetaList.findSmartDataElementMetaByKey(SmartDataElement.NCCN_GENETIC_COUNSELING_CRITERIA_MET_RULE_LIST.getKey());

        if (oMetCriteriaDateSdeMeta.isEmpty()
                || oMetCriteriaRulesSdeMeta.isEmpty()
                || oMetCriteriaSdeMeta.isEmpty()) {
            throw new RuntimeException("SDEs ARE NOT MAPPED PROPERLY: "
                    + SmartDataElement.NCCN_GENETIC_COUNSELING_CRITERIA_MET_DATE.getKey()
                    + " & " + SmartDataElement.NCCN_GENETIC_COUNSELING_CRITERIA_MET.getKey()
                    + " & " + SmartDataElement.NCCN_GENETIC_COUNSELING_CRITERIA_MET_RULE_LIST.getKey()
            );
        }

        metCriteriaDateSdeMeta = oMetCriteriaDateSdeMeta.get();
        metCriteriaSdeMeta = oMetCriteriaSdeMeta.get();
        metCriteriaRulesSdeMeta = oMetCriteriaRulesSdeMeta.get();
    }
    public CdsResponse assemble(CdsRequest cdsRequest, String guidelineUri, Map<String, List<FamilyMemberHistory>> currentNccnMetCriteriaRuleMap) {

        CdsResponse cdsResponse = new CdsResponse();
        Card responseCard = new Card();

        String patientId = CdsHooksHelper.getPatientId(cdsRequest);
        Patient patient = CdsHooksHelper.getPatientPrefetch(cdsRequest,"patient");
        String patientIdCodeSystem = patientHelperR4.getPatientIdCodeSystem(patient, patientId);

        Set<String> previouslyMetCriteriaRule = CdsHooksHelper.getPreviouslyMetRulesSet(cdsRequest);

        Source source = new Source();
        source.setLabel(guidelineUri);
        responseCard.setSource(source);

        // if previous and current criteria are empty -> done - patient did not/does not meet criteria
        if (currentNccnMetCriteriaRuleMap.isEmpty() && previouslyMetCriteriaRule.isEmpty()) {

            responseCard.setSummary("No action. Does NOT meet NCCN criteria for genetic testing.");
            responseCard.setIndicator(Indicator.INFO);
            cdsResponse.addCard(responseCard);

            return cdsResponse;
        }

        // if previous NO LONGER MEETS CRITERIA and current criteria are empty -> done - no updates needed
        if (currentNccnMetCriteriaRuleMap.isEmpty() &&
                (previouslyMetCriteriaRule.contains(NccnRule.NO_LONGER_MEETS_CRITERIA.getCode())
                 || previouslyMetCriteriaRule.contains(Nccn2023Rule.NO_LONGER_MEETS_CRITERIA_2023.getCode())
                )
        ) {
            responseCard.setSummary("No action. No longer meets criteria for genetic testing.");
            responseCard.setIndicator(Indicator.INFO);
            cdsResponse.addCard(responseCard);

            return cdsResponse;
        }

        // if the previously met criteria is the same as newly met criteria -> done, no changes needed
        if (SetUtils.setsAreEqual(currentNccnMetCriteriaRuleMap.keySet(), previouslyMetCriteriaRule)) {

            responseCard.setSummary("No action. Previously met criteria for genetic testing and continues to meet the same criteria.");
            responseCard.setIndicator(Indicator.INFO);
            cdsResponse.addCard(responseCard);

            return cdsResponse;
        }

        //////////////////////////////////////////////////////
        // All the following scenarios suggest communication
        //////////////////////////////////////////////////////
        Suggestion suggestion = new Suggestion();
        suggestion.setLabel("Communicate updates to the population health registry.");
        CommunicationRequest communicationRequest = createCommunicationRequest(patientId);

        // Setup communication payload CSV header; CSV will contain all write/update registry instructions
        StringBuilder payload = new StringBuilder();
        payload // header
                .append("EHR_PAT_ID_CODE_SYSTEM")
                .append("|").append("EHR_PAT_ID")
                .append("|").append("INSTRUCTION")
                .append("|").append("SDE_TYPE")
                .append("|").append("SDE")
                .append("|").append("SDE_ID")
                .append("|").append("SDE_VALUE").append("\n");

        String instruction = "EMPTY";
        String metCriteriaMessage = "EMPTY";
        String metCriteriaCodeList = "EMPTY";

        if (!currentNccnMetCriteriaRuleMap.isEmpty() && previouslyMetCriteriaRule.isEmpty()) {
            responseCard.setSummary("Add to registry. Met NCCN criteria for genetic testing.");
            instruction = "CREATE";
            metCriteriaMessage = CdsHooksHelper.getRuleLabels(currentNccnMetCriteriaRuleMap.keySet().stream().toList());
            metCriteriaCodeList = CdsHooksHelper.getRuleCodeList(currentNccnMetCriteriaRuleMap, ",");

        }

        // if the previously met criteria is NOT the same as newly met criteria, update the registry
        if (!SetUtils.setsAreEqual(currentNccnMetCriteriaRuleMap.keySet(), previouslyMetCriteriaRule)
            && !previouslyMetCriteriaRule.isEmpty()
        ) {
            responseCard.setSummary("Update the registry. Met new NCCN criteria for genetic testing.");
            instruction = "UPDATE";
            metCriteriaMessage = CdsHooksHelper.getRuleLabels(currentNccnMetCriteriaRuleMap.keySet().stream().toList());
            metCriteriaCodeList = CdsHooksHelper.getRuleCodeList(currentNccnMetCriteriaRuleMap, ",");
        }

        // Patient previously met criteria and no longer does (already checked previous for NO LONGER MEETS...)
        if (currentNccnMetCriteriaRuleMap.isEmpty() && !previouslyMetCriteriaRule.isEmpty()) {
            responseCard.setSummary("Update the registry. Previously met criteria for genetic testing and no longer does.");
            instruction = "UPDATE";
            metCriteriaMessage = NccnRule.NO_LONGER_MEETS_CRITERIA.getLabel();
            metCriteriaCodeList = NccnRule.NO_LONGER_MEETS_CRITERIA.getCode();
        }

        payload
                .append(patientIdCodeSystem)
                .append("|").append(patientId)
                .append("|").append(instruction)
                .append("|").append(metCriteriaDateSdeMeta.getType())
                .append("|").append(metCriteriaDateSdeMeta.getKey())
                .append("|").append(metCriteriaDateSdeMeta.getId())
                .append("|").append(LocalDate.now().format(DateTimeFormatter.ISO_DATE)).append("\n")
        ;

        payload
                .append(patientIdCodeSystem)
                .append("|").append(patientId)
                .append("|").append(instruction)
                .append("|").append(metCriteriaSdeMeta.getType())
                .append("|").append(metCriteriaSdeMeta.getKey())
                .append("|").append(metCriteriaSdeMeta.getId())
                .append("|").append(metCriteriaMessage).append("\n")
        ;

        payload
                .append(patientIdCodeSystem)
                .append("|").append(patientId)
                .append("|").append(instruction)
                .append("|").append(metCriteriaRulesSdeMeta.getType())
                .append("|").append(metCriteriaRulesSdeMeta.getKey())
                .append("|").append(metCriteriaRulesSdeMeta.getId())
                .append("|").append(metCriteriaCodeList).append("\n")
        ;

        communicationRequest.addPayload().setContent(new StringType(payload.toString()));

        Action action = new Action();
        action.setType(ActionType.UPDATE);
        action.setDescription("Follow registry editing instructions in the action payload.");
        action.setResource(communicationRequest);

        suggestion.addAction(action);
        responseCard.addSuggestion(suggestion);
        cdsResponse.addCard(responseCard);

        return cdsResponse;
    }

    private CommunicationRequest createCommunicationRequest(String patientId) {
        Reference nccnGeneticTestingGuideline = new Reference("RiskAssessment/" + NCCN_GENETIC_ASSESSMENT_GUIDELINE_URI);
        nccnGeneticTestingGuideline.setDisplay(NCCN_GENETIC_ASSESSMENT_GUIDELINE_URI);

        CommunicationRequest communicationRequest = new CommunicationRequest();
        communicationRequest.setId(UUID.randomUUID().toString());
        communicationRequest.setSubject(new Reference("Patient/" + patientId));
        communicationRequest.setStatus(CommunicationRequest.CommunicationRequestStatus.ACTIVE);
        communicationRequest.setSender(nccnGeneticTestingGuideline);

        List<Reference> reasonReferenceList = new ArrayList<>();
        reasonReferenceList.add(nccnGeneticTestingGuideline);
        communicationRequest.setReasonReference(reasonReferenceList);
        communicationRequest.addRecipient().setDisplay("Population health management familial cancer risk registry");
        return communicationRequest;
    }

}

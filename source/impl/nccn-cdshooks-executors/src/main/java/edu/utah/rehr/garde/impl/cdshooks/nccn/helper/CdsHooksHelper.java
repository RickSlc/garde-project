package edu.utah.rehr.garde.impl.cdshooks.nccn.helper;

import edu.utah.rehr.garde.domain.core.model.Nccn2023Rule;
import edu.utah.rehr.garde.domain.core.model.NccnRule;
import edu.utah.rehr.garde.domain.core.utils.SetUtils;
import org.hl7.fhir.r4.model.*;
import org.opencds.hooks.model.context.HookContext;
import org.opencds.hooks.model.r4.request.prefetch.R4PrefetchHelper;
import org.opencds.hooks.model.request.CdsRequest;
import org.opencds.hooks.model.request.prefetch.PrefetchResult;

import java.util.*;
import java.util.logging.Logger;

public class CdsHooksHelper {
    private static final Logger log = Logger.getLogger(CdsHooksHelper.class.getName());
    private static final R4PrefetchHelper r4PrefetchHelper = new R4PrefetchHelper();

    public static Set<String> getPreviouslyMetRulesSet(CdsRequest cdsRequest) {

        HookContext hookContext = cdsRequest.getContext();
        String previouslyMetCriteriaList = hookContext.get("previouslyMetCriteriaList", String.class); // pipe-delimited string

        if (previouslyMetCriteriaList != null) {
            return SetUtils.convertDelimitedStringToSet(previouslyMetCriteriaList, "\\|");
        }

        return new HashSet<>();
    }

    public static String getRuleCodeList(Map<String, List<FamilyMemberHistory>> metNccnCriteriaRuleMap, String delimiter) {

        StringBuilder ruleList = new StringBuilder();

        int i = 0;
        for (String ruleCode : metNccnCriteriaRuleMap.keySet()) {

            if (i == 0) {
                ruleList.append(ruleCode);
            } else {
                ruleList.append(delimiter).append(ruleCode);
            }
            i++;
        }

        return ruleList.toString();
    }

    public static List<String> ruleCodeListToRuleLabelList(List<String> ruleCodeList) {
        List<String> ruleLabelList = new ArrayList<>();
        for (String ruleCode:ruleCodeList) {

            NccnRule rule = NccnRule.findByCode(ruleCode);
            if (rule != null) {
                ruleLabelList.add(rule.getLabel());
            } else {

                Nccn2023Rule rule2023 = Nccn2023Rule.findByCode(ruleCode);
                if (rule2023 != null) {
                    ruleLabelList.add(rule2023.getLabel());
                }
            }
        }
        return ruleLabelList;
    }
    public static String getRuleLabels(List<String> ruleCodeList) {

        List<String> ruleLabelList = ruleCodeListToRuleLabelList(ruleCodeList);

        StringBuilder ruleList = new StringBuilder();

        int i = 0;
        for (String ruleLabel : ruleLabelList) {
            i++;
            if (ruleLabelList.size() > 1) {
                ruleList.append(i).append(") ");
                ruleList.append(ruleLabel);
            } else {
                ruleList.append(ruleLabel);
            }
            if (ruleLabelList.size() != i) {
                ruleList.append("; ");
            }
        }

        return ruleList.toString();
    }

    public static String getPatientId(CdsRequest cdsRequest) {

        HookContext hookContext = cdsRequest.getContext();
        String patientId = hookContext.get("patientId", String.class);

        return patientId;
    }

    public static String getOutputType(CdsRequest cdsRequest) {

        HookContext hookContext = cdsRequest.getContext();
        String outputType = hookContext.get("outputType", String.class);

        return outputType;
    }

    public static Patient getPatientPrefetch(CdsRequest request, String patientPrefetchKey) {

        try {
            PrefetchResult<OperationOutcome, Resource> prefetchResult = request.getPrefetchResource(patientPrefetchKey, r4PrefetchHelper);
            return prefetchResult.getResource(Patient.class);
        } catch (Exception e) {
            log.severe("PREFETCH FAILURE FOR " + getPatientId(request));
            throw new RuntimeException(e);
        }
    }

    public static List<FamilyMemberHistory> getFhxBundlePrefetch(CdsRequest cdsRequest, String fhxBundlePrefetchKey) {

        List<FamilyMemberHistory> fhxList = new ArrayList<>();
        PrefetchResult<OperationOutcome, Resource> prefetchResult = cdsRequest.getPrefetchResource(fhxBundlePrefetchKey, r4PrefetchHelper);

        if (prefetchResult.hasResource()) {

            Bundle bundle = prefetchResult.getResource(Bundle.class);
            for (Bundle.BundleEntryComponent entry : bundle.getEntry()) {
                FamilyMemberHistory fhx = (FamilyMemberHistory) entry.getResource();
                fhxList.add(fhx);
            }
        }

        return fhxList;
    }


}

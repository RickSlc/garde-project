package edu.utah.rehr.garde.impl.cdshooks.nccn.helper;

import edu.utah.rehr.garde.domain.core.model.GCohort;
import edu.utah.rehr.garde.domain.core.model.IRule;
import edu.utah.rehr.garde.domain.core.model.Nccn2023Rule;
import edu.utah.rehr.garde.domain.core.model.NccnRule;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.FamilyMemberHistoryHelperR4;
import edu.utah.rehr.garde.domain.core.terminology.Code;
import lombok.extern.java.Log;
import org.hl7.fhir.r4.model.*;
import org.opencds.hooks.model.request.CdsRequest;
import org.opencds.hooks.model.response.*;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

@Log
public class CdsHooksResponseGardePatientMetCriteriaAssembler {

    private final FamilyMemberHistoryHelperR4 familyMemberHistoryHelperR4;
    public CdsHooksResponseGardePatientMetCriteriaAssembler(FamilyMemberHistoryHelperR4 familyMemberHistoryHelperR4) {
        this.familyMemberHistoryHelperR4 = familyMemberHistoryHelperR4;
    }

    public CdsResponse assemble(CdsRequest cdsRequest
            , String patientIdCodeSystem
            , String guidelineUri
            , Map<String, List<FamilyMemberHistory>> currentMetNccnCriteriaRuleMap) {

        String patientId = CdsHooksHelper.getPatientId(cdsRequest);
        CdsResponse cdsResponse = new CdsResponse();
        Card responseCard = new Card();

        Source source = new Source();
        source.setLabel(guidelineUri);
        responseCard.setSource(source);

        if (currentMetNccnCriteriaRuleMap.isEmpty()) { // Patient did NOT meet criteria

            responseCard.setSummary("Patient did NOT meet criteria for genetic testing based on provided family history.");
            responseCard.setIndicator(Indicator.INFO);
            cdsResponse.addCard(responseCard);

            return cdsResponse; // done
        }

        // metNccnCriteriaRuleMap > 0 implies met criteria
        responseCard.setSummary("Genetic testing recommended.");
        responseCard.setIndicator(Indicator.WARNING);

        // For each rule there may be multiple FHx contributors
        for (String ruleCode : currentMetNccnCriteriaRuleMap.keySet()) {

            IRule rule = NccnRule.findByCode(ruleCode);
            if (rule == null) {
                rule = Nccn2023Rule.findByCode(ruleCode);
            }

            Suggestion suggestion = new Suggestion();
            suggestion.setLabel("Communicate this finding to genetic counseling.");

            // Bundle for suggestion action - CommunicationResource and contributing FamilyMemberHistory
            Bundle bundle = new Bundle();
            bundle.setId(UUID.randomUUID().toString());
            bundle.setType(Bundle.BundleType.BATCHRESPONSE);

            CommunicationRequest communicationRequest = new CommunicationRequest();
            communicationRequest.setId(UUID.randomUUID().toString());
            communicationRequest.setStatus(CommunicationRequest.CommunicationRequestStatus.ACTIVE);
            communicationRequest.setSender(new Reference().setDisplay(guidelineUri));
            communicationRequest.addRecipient().setDisplay("Familial cancer population health management registry");

            StringBuilder payloadValues = new StringBuilder();

            payloadValues
                    .append(patientIdCodeSystem)
                    .append(",").append(patientId)
                    .append(",").append(rule.getId())
                    .append(",").append(GCohort.MetNccnCriteriaForGeneticTesting.getCode())
            ;

            communicationRequest.addPayload().setContent(new StringType(payloadValues.toString()));
            bundle.addEntry().setResource(communicationRequest);

            Action action = new Action();
            action.setType(ActionType.CREATE);

            StringBuilder actionDescription = new StringBuilder()
                    .append("Communicate patient ").append(patientId)
                    .append(" met criteria for genetic testing by rule ").append(ruleCode)
                    .append(" based on family history: \n") // additional appends below
                    ;

            List<FamilyMemberHistory> metRuleCriteriaFhxList = currentMetNccnCriteriaRuleMap.get(ruleCode);

            // Add each fhx to the action that met criteria
            for (FamilyMemberHistory fhx:metRuleCriteriaFhxList) {

                Set<Code> relativeCodes = familyMemberHistoryHelperR4.getRelativeCodings(fhx);
                Set<Code> conditionCodes = familyMemberHistoryHelperR4.getConditionCodings(fhx);

                if (relativeCodes.isEmpty() || conditionCodes.isEmpty()) {
                    log.warning("ERROR CREATING RESPONSE MESSAGE: " + payloadValues);
                } else {
                    // pull first code to build CDS Hooks message.
                    Code relativeCode = relativeCodes.stream()
                            .findFirst()
                            .orElse(null);

                    Code conditionCode = conditionCodes.stream()
                            .findFirst()
                            .orElse(null);

                    actionDescription.append(relativeCode.getCodeLabel()).append(" had ").append(conditionCode.getCodeLabel()).append("\n");
                }

                bundle.addEntry().setResource(fhx);
            }

            action.setDescription(actionDescription.toString());
            action.setResource(bundle);
            suggestion.addAction(action);
            responseCard.addSuggestion(suggestion);
        }

        cdsResponse.addCard(responseCard);
        return cdsResponse;
    }
}

package edu.utah.rehr.garde.domain.core.service;


import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.model.*;
import org.hl7.fhir.dstu3.model.FamilyMemberHistory;
import org.hl7.fhir.dstu3.model.Patient;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author RickSlc
 */
@Deprecated
public interface ICohortService {

    UResult<Integer> loadPatientCohort(GCohort cohort, List<PatientFact> patients, Integer version);

    UResult<Integer> loadFamilyHxFacts(List<FamilyHxFact> fhxFacts, Integer version);

    UResult<Integer> exportCohort(GCohort cohort, Integer version, String outputFilePath);

    UResult<Integer> updateCohort(GCohort cohort, Integer version);

    UResult<List<MetCriteriaMessage>> getMetCriteriaMessages(GCohort cohort, Integer version);

    UResult<List<MetCriteriaMessage>> getAllMetCriteriaMessages(GCohort cohort);

    UResult<List<PatientChatbot>> getChatTranscripts(String chatScriptId);

    UResult<PatientChatbot> getPatientChatbot(String patientId, String chatScriptId);

    UResult<PatientChatbot> savePatientChatbot(PatientChatbot patientChatbot);

    UResult<List<PatientChatbot>> getLoadedChatTranscripts(LocalDateTime sinceDate);

    ///////// cohortDao
    int batchUpdateCohort(GCohort cohort, Set<String> patientIds, String action, Integer version);
    int batchUpdateCohort(Query query, Integer version);
    List<PatientFact> getPatientFacts(GCohort cohort);
    PatientFact getPatientFact(String patientId);
    UResult<List<Patient>> getPatients(Query query);
    UResult<List<Patient>> getPatients(GCohort cohort, Integer version);
    Set<String> getPatientIds(Query query, String patientIdParameter);
    UResult<List<PatientFact>> getPatientFactsByCohort(String studyCd);

    /////// familyHxDao
    List<FamilyMemberHistory> getPatientFamilyMemberHistory(String patientId);
    UResult<List<FamilyHxFact>> getFamilyHxFacts(GCohort cohort);
    UResult<Map<String, List<FamilyMemberHistory>>> getPatientFamilyMemberHistoryMap(GCohort cohort);

    /////// IMetCriteriaProvider
    List<MetCriteriaMessage> getAllPatientMetCriteriaMessages();
    List<MetCriteriaMessage> getMetCriteriaMessages(Integer version);
    UResult<Integer> batchLoadMetCriteriaFacts(List<MetNccnCriteriaFact> currentFactList, Integer version);

}

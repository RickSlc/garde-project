package edu.utah.rehr.garde.domain.core.model.dto;

import edu.utah.rehr.garde.domain.core.terminology.CodeAnalytics;

import java.util.List;

public class CategoryCodeAnalyticsDTO {

    public String category;
    public List<CodeAnalytics> codeAnalytics;
    public String errorMessage;

}

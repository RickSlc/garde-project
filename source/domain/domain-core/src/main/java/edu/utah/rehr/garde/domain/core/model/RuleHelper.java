package edu.utah.rehr.garde.domain.core.model;

import java.lang.reflect.Field;

public class RuleHelper {

    public static <E extends Enum<E>> E findById(Class<E> enumClass, String id) {

        try {

            for (E e: enumClass.getEnumConstants()) {
                Field idField =  e.getClass().getDeclaredField("id");
                idField.setAccessible(true);
                if (idField.get(e).equals(id)) {
                    return e;
                }
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return null;
    }
    public static <E extends Enum<E>> E findByCode(Class<E> enumClass, String code) {

        try {

            for (E e: enumClass.getEnumConstants()) {
                Field idField =  e.getClass().getDeclaredField("code");
                idField.setAccessible(true);
                if (idField.get(e).equals(code)) {
                    return e;
                }
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return null;
    }
}

package edu.utah.rehr.garde.domain.core.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;

/**
 *
 * @author RickSlc
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
public enum GCohort {

    // Breast and colorectal cancer risk detection cohorts
    MetNccnScreeningCriteria("MET_NCCN_SCREENING_CRITERIA",null, "Met NCCN screening criteria for genetic testing evaluation", "Adults seen in a primary care clinic in the past 3 years between ages 25 and 60")
    , MetNccn2023CriteriaForGeneticTesting("MET_NCCN_2023_GENETIC_TESTING_CRITERIA", "genetic-testing-recommender-r4-2023", "Met NCCN 2023 genetic testing criteria", "People who met genetic testing criteria (NCCN 2023) for familial breast, colon, pancreatic, or ovarian cancer")
    , MetNccnCriteriaForGeneticTesting("MET_NCCN_GENETIC_TESTING_CRITERIA", "genetic-testing-recommender-r4", "Met NCCN genetic testing criteria", "People who met genetic testing criteria for familial breast, colon, pancreatic, or ovarian cancer")

    // Familial Hypercholesterolemia cohorts
    , MetFamilialHypercholesterolemiaScreeningCriteria("MET_FH_2018_SCREENING_CRITERIA",null, "Met familial hypercholesterolemia screening criteria for genetic testing evaluation", "Adults seen in a primary care clinic in the past year.")
    , MetFamilialHypercholesterolemiaCriteriaForGeneticTesting("MET_FH_2018_GENETIC_TESTING_CRITERIA", "fh-genetic-testing-recommender-r4-2018", "Met FH 2018 genetic testing criteria", "People who met genetic testing criteria for familial hypercholesterolemia")
    ;

    private final String code;
    private final String cdsHooksServiceCode;
    private final String label;
    private final String description;

    GCohort(String code, String cdsHooksServiceCode, String label, String description) {
        this.label = label;
        this.code = code;
        this.cdsHooksServiceCode = cdsHooksServiceCode;
        this.description = description;
    }

    public static GCohort findByCode(String code) {

        for (GCohort cohort: GCohort.values()) {
            if (cohort.getCode().equals(code)) {
                return cohort;
            }
        }

        return null;
    }

}

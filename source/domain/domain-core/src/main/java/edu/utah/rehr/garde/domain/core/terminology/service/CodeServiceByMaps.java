package edu.utah.rehr.garde.domain.core.terminology.service;

import edu.utah.rehr.garde.domain.core.terminology.Code;
import edu.utah.rehr.garde.domain.core.terminology.ITerminologyKey;
import edu.utah.rehr.garde.domain.core.terminology.TerminologyEntityType;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import lombok.extern.java.Log;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@Log
public class CodeServiceByMaps implements ICodeService {

    private final Map<String, Set<Code>> codeSetMap;
    private Map<String, Set<Code>> standardCodeBindingsMap;
    private Map<String, Set<Code>> ehrCodeBindingsMap;
    private Map<String, String> uriBindingsMap;

    public CodeServiceByMaps(
            Map<String, Set<Code>> codeSetMap
            , Map<String, Set<Code>> standardCodeBindingsMap
            , Map<String, Set<Code>> ehrCodeBindingsMap
            , Map<String, String> uriBindingsMap
    ) {
        this.codeSetMap = codeSetMap;
        this.standardCodeBindingsMap = standardCodeBindingsMap;
        this.ehrCodeBindingsMap = ehrCodeBindingsMap;
        this.uriBindingsMap = uriBindingsMap;
    }

    public void setStandardCodeBindingsMap(Map<String, Set<Code>> standardCodeBindingsMap) {
        this.standardCodeBindingsMap = standardCodeBindingsMap;
    }

    public void setEhrCodeBindingsMap(Map<String, Set<Code>> ehrCodeBindingsMap) {
        this.ehrCodeBindingsMap = ehrCodeBindingsMap;
    }

    public void setUriBindingsMap(Map<String, String> uriBindingsMap) {
        this.uriBindingsMap = uriBindingsMap;
    }

    @Override
    public Code getCanonicalCode(ITerminologyKey key) {
        return getOneStandardCodeFromSet(key.getKey());
    }

    @Override
    public Code getCanonicalCodeForEhrCode(Code code) {
        if (StringUtils.isEmpty(code.getBindingCode())) {
            log.severe("NO BINDING CODE FOR CODE " + code);
            return null;
        }
        return getOneStandardCodeFromSet(code.getBindingCode());
    }

    Code getOneStandardCodeFromSet(String bindingCode) {
        Set<Code> standardCodeSet = standardCodeBindingsMap.get(bindingCode);

        if (standardCodeSet == null || standardCodeSet.isEmpty()) {
            log.severe("NO STANDARD CODE MAPPINGS FOR " + bindingCode);
            return null;
        }

        Object[] codeObjects = standardCodeSet.toArray();
        if (codeObjects.length > 1) {
            log.warning("MULTIPLE STANDARD/CANON CODES FOUND FOR BINDING CODE " + bindingCode);
        }
        return  (Code)codeObjects[0];
    }

    @Override
    public Set<Code> getCanonicalCodeSet(ITerminologyKey setKey) {
        throw new NotImplementedException("getCanonicalCodeSet not supported in CSV implementation");
    }

    @Override
    public Set<Code> getEhrCodeSet(ITerminologyKey setKey) {
        Set<Code> ehrCodes = ehrCodeBindingsMap.get(setKey.getKey());
        return new HashSet<>(ehrCodes);
    }

    @Override
    public String getCodeSystem(ITerminologyKey key) {
        return uriBindingsMap.get(key.getKey());
    }

    @Override
    public String getUri(ITerminologyKey key) {
        return uriBindingsMap.get(key.getKey());
    }

    @Override
    public Boolean isCanonicalSetMember(ITerminologyKey setKey, Code code) {
        throw new NotImplementedException("getCanonicalCode not supported in CSV implementation");
    }

    @Override
    public Boolean isEhrSetMember(ITerminologyKey setKey, Code code) {
        return getEhrCodeSet(setKey).contains(code);
    }

    @Override
    public Boolean isSetMember(ITerminologyKey setKey, Code code) {
        Set<Code> codeSet = getCodeSet(setKey);
        return codeSet.contains(code);
    }

    @Override
    public Boolean intersects(ITerminologyKey setKey, Set<Code> codes) {

        Set<Code> comparatorSet = getCodeSet(setKey);
        comparatorSet.retainAll(codes);

        return !comparatorSet.isEmpty();
    }

    public Set<Code> getCodeSet(ITerminologyKey key) {

        Set<Code> allCodesSet = new HashSet<>();

        if (key.getEntityType().equals(TerminologyEntityType.CODE)) {

            Set<Code> standardCodeSet = standardCodeBindingsMap.get(key.getKey());
            if (standardCodeSet != null && !standardCodeSet.isEmpty()) {
                allCodesSet.addAll(standardCodeSet);
            }

            Set<Code> ehrCodeSet = ehrCodeBindingsMap.get(key.getKey());
            if (ehrCodeSet != null && !ehrCodeSet.isEmpty()) {
                allCodesSet.addAll(ehrCodeSet);
            }

            return allCodesSet;
        }

        Set<Code> codeSet = codeSetMap.get(key.getKey());

        if (codeSet == null || codeSet.isEmpty()) {
            log.severe("CODE SET " + key.getKey() + " IS EMPTY");
        }

        for (Code code : codeSet) { // loop through binding codes and adds their associated codes

            Set<Code> standardCodeSet = standardCodeBindingsMap.get(code.getCode());
            if (standardCodeSet != null && !standardCodeSet.isEmpty()) {
                allCodesSet.addAll(standardCodeSet);
            }

            Set<Code> ehrCodeSet = ehrCodeBindingsMap.get(code.getCode());
            if (ehrCodeSet != null && !ehrCodeSet.isEmpty()) {
                allCodesSet.addAll(ehrCodeSet);
            }

        }

        if (allCodesSet == null || allCodesSet.isEmpty()) {
            log.severe("CODE SET " + key.getKey() + " IS EMPTY");
        }

        return allCodesSet;
    }

}

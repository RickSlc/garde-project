package edu.utah.rehr.garde.domain.core.utils;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil {

    public static Date getValidTestBirthDateBasedOnAge(int age) {
        // Get the current year
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);

        // Calculate the birth year
        int birthYear = currentYear - age;

        // Create a new GregorianCalendar instance and set the birth date
        GregorianCalendar birthDate = new GregorianCalendar(birthYear, Calendar.JANUARY, 1);

        // Handle the leap year (29th of February) for individuals born on a leap year
        if (birthDate.isLeapYear(birthYear)) {
            birthDate.set(Calendar.MONTH, Calendar.FEBRUARY);
            birthDate.set(Calendar.DAY_OF_MONTH, 29);
        }

        return birthDate.getTime();
    }

    public static String toString(Date date, DateTimeFormatter formatter) {
        if (date == null || formatter == null) {
            return null;
        }
        LocalDateTime localDateTime = toLocalDateTime(date);
        return localDateTime.format(formatter);
    }

    public static String toString(Date date, String dateFormat) {
        if (date == null || dateFormat == null) {
            return null;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        return simpleDateFormat.format(date);
    }

    public static String toString(LocalDate localDate, String dateFormat) {
        if (localDate == null || dateFormat == null) {
            return null;
        }
        Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        return toString(date, dateFormat);
    }

    public static LocalDateTime toLocalDateTime(Date date) {
        if (date == null) {
            return null;
        }
        return date.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }

    public static LocalDate toLocalDate(Date date) {
        if (date == null) {
            return null;
        }
        return date.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime()
                .toLocalDate();
    }

    public static Date toDate(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return null;
        }
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date toDate(LocalDate localDate) {
        if (localDate == null) {
            return null;
        }
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

}

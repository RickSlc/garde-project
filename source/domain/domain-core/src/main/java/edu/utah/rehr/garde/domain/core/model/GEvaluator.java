package edu.utah.rehr.garde.domain.core.model;

import lombok.Getter;
import lombok.NonNull;

import java.util.*;

@Getter
public enum GEvaluator {

    FH_GENETIC_TESTING_RECOMMENDER_R4_2018(
            "fh-genetic-testing-recommender-r4-2018"
            , "Genetic Testing for FH Recommender"
            , "Genetic Testing for Familial Hypercholesterolemia (FH) Recommender (2018)"
            ,"http://utah.edu/rehr/glue"
            , GCohort.MetFamilialHypercholesterolemiaScreeningCriteria
            , new GCohort[]{GCohort.MetFamilialHypercholesterolemiaCriteriaForGeneticTesting}
            , new GFhirResource[]{GFhirResource.Patient, GFhirResource.FamilyMemberHistory, GFhirResource.Observation_Ldl_C}
    )
    , GENETIC_TESTING_RECOMMENDER_R4_2023(
            "genetic-testing-recommender-r4-2023"
            , "Genetic Testing for HBOC Recommender (2023)"
            , "Genetic Testng for Hereditary Breast, Ovarian, Pancreatic, or Colorectal Cancer (HBOC) Recommender (NCCN 2023)"
            ,"http://utah.edu/rehr/nccn"
            , GCohort.MetNccnScreeningCriteria
            , new GCohort[]{GCohort.MetNccn2023CriteriaForGeneticTesting}
            , new GFhirResource[]{GFhirResource.NccnPatient, GFhirResource.FamilyMemberHistory}
    )
    , GENETIC_TESTING_RECOMMENDER_R4(
            "genetic-testing-recommender-r4"
            , "Genetic Testing for HBOC Recommender (2019)"
            , "Genetic Testing for Hereditary Breast, Ovarian, Pancreatic, and Colorectal (HBOC) Recommender (NCCN 2019)"
            ,"http://utah.edu/rehr/nccn"
            , GCohort.MetNccnScreeningCriteria
            , new GCohort[]{GCohort.MetNccnCriteriaForGeneticTesting}
            , new GFhirResource[]{GFhirResource.NccnPatient, GFhirResource.FamilyMemberHistory}
    );

    private final String serviceId;
    private final String title;
    private final String guideline;
    private final String bindingCodeSystem;
    private final GCohort cohortToEvaluate;
    private final GCohort[] generatedCohorts;
    /**
     * Populate with each CDS Hooks prefetch variable name [0], FHIR Resource [1], and FHIR query [2]
     */
    private final GFhirResource[] requiredFhirResources;

    GEvaluator(String serviceId
            , String title
            , String guideline
            , String bindingCodeSystem
            , GCohort cohortToEvaluate
            , GCohort[] generatedCohorts
            , GFhirResource[] requiredFhirResources) {
        this.serviceId = serviceId;
        this.title = title;
        this.guideline = guideline;
        this.bindingCodeSystem = bindingCodeSystem;
        this.cohortToEvaluate = cohortToEvaluate;
        this.generatedCohorts = generatedCohorts;
        this.requiredFhirResources = requiredFhirResources;
    }

    public static GEvaluator findByServiceId(@NonNull String serviceId) {
        for (GEvaluator evaluator : values()) {
            if (serviceId.equals(evaluator.serviceId)) {
                return evaluator;
            }
        }
        return null;
    }

    public static GEvaluator findByBindingCodeSystem(@NonNull String bindingCodeSystem) {
        for (GEvaluator evaluator : values()) {
            if (bindingCodeSystem.equals(evaluator.bindingCodeSystem)) {
                return evaluator;
            }
        }
        return null;
    }

    public static Set<String> getFhirResourceSetByServiceId(String serviceId) {
        GEvaluator evaluator = findByServiceId(serviceId);
        if (evaluator == null) {
            return new HashSet<>();
        }
        Set<String> fhirResourceSet = new HashSet<>();
        for (GFhirResource fhirResource : evaluator.requiredFhirResources) {
            fhirResourceSet.add(fhirResource.getFhirResource());
        }
        return fhirResourceSet;
    }

    public static Map<String,List<GFhirResource>> getFhirResourceMapByServiceId(String serviceId) {
        GEvaluator evaluator = findByServiceId(serviceId);
        if (evaluator == null) {
            return null;
        }
        Map<String, List<GFhirResource>> fhirResourceMap = new HashMap<>();
        List<GFhirResource> fhirResourceList = new ArrayList<>();
        for (GFhirResource fhirResource : evaluator.requiredFhirResources) {
            fhirResourceList.add(fhirResource);
            fhirResourceMap.put(fhirResource.getFhirResource(), fhirResourceList);
        }
        return fhirResourceMap;
    }

}

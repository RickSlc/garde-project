package edu.utah.rehr.garde.domain.core.utils;

import java.util.ArrayList;
import java.util.List;

public class CharacterUtil {

    public final static int CHAR_RANGE_ABOVE_256 = 256;
    public final static int CHAR_RANGE_ABOVE_128 = 128;

    public static List<String> getUnicodeCharacters(String input) {
        return getUnicodeCharactersAboveCharRange(input, CHAR_RANGE_ABOVE_128);
    }

    public static List<String> getUnicodeCharactersGTE256(String input) {
        return getUnicodeCharactersAboveCharRange(input, CHAR_RANGE_ABOVE_256);
    }

    public static List<String> getUnicodeCharactersAboveCharRange(String input, int charRange) {

        List<String> unicodeChars = new ArrayList<>();

        for (int i = 0; i < input.length(); i++) {

            StringBuilder stringBuilder = new StringBuilder();

            int cp = Character.codePointAt(input, i);
            int charCount = Character.charCount(cp);
            if (charCount > 1) {
                i += charCount - 1; // 2.
                if (i >= input.length()) {
                    throw new IllegalArgumentException("truncated unexpectedly");
                }
            }

            if (cp >= charRange) {
                stringBuilder
                        .append("char[").append(i).append("][")
                        .append(String.format("\\u%x", cp)).append("][").appendCodePoint(cp).append("]");
                unicodeChars.add(stringBuilder.toString());
            }
        }

        return unicodeChars;
    }







}

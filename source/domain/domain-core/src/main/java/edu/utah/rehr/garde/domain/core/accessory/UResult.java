package edu.utah.rehr.garde.domain.core.accessory;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

public class UResult<T> {

    private T result;
    private UResultStatus resultStatus = UResultStatus.INCOMPLETE;
    private Throwable exception;

    private String message;

//    private UResult(UResultStatus resultStatus) {
//        this.result = result;
//    }
    private UResult(UResultStatus resultStatus) {
    this.resultStatus = resultStatus;
}

    private UResult(UResultStatus resultStatus, String message) {
        this.resultStatus = resultStatus;
        this.message = message;
    }

    private UResult(UResultStatus resultStatus, T result) {
        this.resultStatus = resultStatus;
        this.result = result;
    }

    private UResult(UResultStatus resultStatus, T result, String message) {
        this.resultStatus = resultStatus;
        this.result = result;
        this.message = message;
    }

    private UResult(UResultStatus resultStatus, T result, Throwable exception) {
        this.message = ExceptionUtils.getRootCauseMessage(exception);
        this.resultStatus = resultStatus;
        this.result = result;
        this.exception = exception;
    }

    public static <T> UResult<T> result(UResultStatus resultStatus) {
        return new UResult<>(resultStatus);
    }

    public static <T> UResult<T> result(UResultStatus resultStatus, T result) {
        return new UResult<>(resultStatus, result);
    }

    public static <T> UResult<T> successResult() {
        return new UResult<>(UResultStatus.SUCCESS);
    }

//    public static <T> UResult<T> successResult(String message) {
//        return new UResult<>(UResultStatus.success, message);
//    }

    public static <T> UResult<T> successResult(T result) {
        return new UResult<>(UResultStatus.SUCCESS, result);
    }

    public static <T> UResult<T> successResult(T result, String message) {
        return new UResult<>(UResultStatus.SUCCESS, result, message);
    }

    public static <T> UResult<T> warningResult() {
        return new UResult<>(UResultStatus.WARNING);
    }

    public static <T> UResult<T> warningResult(String message) {
        return new UResult<>(UResultStatus.WARNING, message);
    }

    public static <T> UResult<T> warningResult(T result) {
        return new UResult<>(UResultStatus.WARNING, result);
    }

    public static <T> UResult<T> warningResult(T result, String message) {
        return new UResult<>(UResultStatus.WARNING, result, message);
    }

    public static <T> UResult<T> warningResult(T result, Throwable exception) {
        return new UResult<>(UResultStatus.WARNING, result, exception);
    }

    public static <T> UResult<T> errorResult() {
        return new UResult<>(UResultStatus.ERROR);
    }

    public static <T> UResult<T> errorResult(String message) {
        return new UResult<>(UResultStatus.ERROR, message);
    }

    public static <T> UResult<T> errorResult(T result) {
        return new UResult<>(UResultStatus.ERROR, result);
    }

    public static <T> UResult<T> errorResult(T result, String message) {
        return new UResult<>(UResultStatus.ERROR, result, message);
    }

    public static <T> UResult<T> errorResult(T result, Throwable exception) {
        return new UResult<>(UResultStatus.ERROR, result, exception);
    }

    public static <T> UResult<T> errorResult(Throwable exception) {
        return new UResult<>(UResultStatus.ERROR, null, exception);
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public UResultStatus getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(UResultStatus resultStatus) {
        this.resultStatus = resultStatus;
    }

    public Throwable getException() {
        return exception;
    }

    public void setException(Throwable exception) {
        this.exception = exception;
    }

    public String getMessage() {

        StringBuilder theMessage = new StringBuilder();

        if (this.message != null) {
            theMessage.append(this.message);
        }

        if (this.exception != null) {
            if (StringUtils.isNotEmpty(theMessage.toString())) {
                theMessage.append("\n");
            }
            theMessage.append(ExceptionUtils.getStackTrace(exception));
        }

        return theMessage.toString();
    }

    public Boolean success() {
        return resultStatus.equals(UResultStatus.SUCCESS);
    }

    public Boolean success(UResultStatus tolerance) {

        switch (tolerance) {

            case ERROR:
                return true;
            case WARNING:
                return resultStatus.equals(UResultStatus.WARNING) || resultStatus.equals(UResultStatus.SUCCESS);
            case SUCCESS:
                return resultStatus.equals(UResultStatus.SUCCESS);

            default:
                return false;
        }

    }

    public Boolean warning() {
        return resultStatus.equals(UResultStatus.WARNING);
    }

    public Boolean warnOrSuccess() {
        return resultStatus.equals(UResultStatus.SUCCESS) || resultStatus.equals(UResultStatus.WARNING);
    }

    public Boolean error() {
        return resultStatus.equals(UResultStatus.ERROR) || resultStatus.equals(UResultStatus.INCOMPLETE);
    }

}


package edu.utah.rehr.garde.domain.core.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;

@Getter
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Nccn2023Rule implements IRule {

    BC_GENETIC_MUTATION_IN_FHX_2023("BC_GENETIC_MUTATION_IN_FHX_2023", "B1", "101", "BRCA1/2, CHEK2, ATM, PALB2, TP53, PTEN, CDH1, Cowden Syndrome, Li-Fraumeni Syndrome, PTEN Hamartomata tumor syndrome (PHTS), STK11, BRIP1, BARD1, RAD51C, RAD51D, CDKN2A and HOB13 within 3 generations of the proband","INCLUSION")
    , BC_HAS_BREAST_CA_AGE_LTE_50_FHX_CRIT_2023("BC_BREAST_CA_AGE_LT_50_FHX_CRIT_2023","B2","111", "One or more first or second-degree relative with breast cancer age <= 50 years","INCLUSION")
    , BC_OVARIAN_CA_FHX_CRIT_2023("BC_OVARIAN_CA_FHX_CRIT_2023", "B3","121","One or more first or second-degree relatives with ovarian cancer","INCLUSION")
    , BC_FIRST_DEG_RELATIVE_PANCREATIC_CA_FHX_CRIT_2023("BC_FIRST_DEG_RELATIVE_PANCREATIC_CA_FHX_CRIT_2023", "B4", "161", "One or more first degree relatives with pancreatic cancer","INCLUSION")
    , BC_3_RELATIVES_WITH_BREAST_OR_PROSTATE_CA_FHX_CRIT_2023("BC_3_RELATIVES_WITH_BREAST_OR_PROSTATE_CA_FHX_CRIT_2023", "B5","131", "Three or more 1st, 2nd, or 3rd degree relatives with breast or prostate cancer on the same side of the family with at least one 1st or 2nd degree relative","INCLUSION")
    , BC_MALE_BREAST_CA_FHX_CRIT_2023("BC_MALE_BREAST_CA_FHX_CRIT_2023", "B6","141", "Breast cancer in a first, second, or third degree male relative","INCLUSION")
    , BC_JEWISH_WITH_CA_FHX_CRIT_2023("BC_JEWISH_WITH_CA_FHX_CRIT_2023", "B7","151", "Jewish and any breast or prostate cancer within 3 generations at any age","INCLUSION")

    , CO_GENETIC_FHX_CRIT_2023("CO_GENETIC_FHX_CRIT_2023", "C1", "201","MLH1, MSH2, PMS2, MSH6, EPCAM, MYH, or MUTYH genes, Lynch syndrome, familial adenomatous polyposis (FAP), adenomatous polyposis coli (APC), serrated polyposis or polyposis discovered in the coded family history within 3 generations from the proband","INCLUSION")
    , CO_FIRST_GEN_COLON_CA_AGE_LTE_50_FHX_CRIT_2023("CO_FIRST_GEN_COLON_CA_AGE_LTE_50_FHX_CRIT_2023", "C2", "211","One or more first degree relatives with colon cancer age <= 50","INCLUSION")
    , CO_FIRST_GEN_ENDOMETRIAL_CA_AGE_LTE_50_FHX_CRIT_2023("CO_FIRST_GEN_ENDOMETRIAL_CA_AGE_LTE_50_FHX_CRIT_2023", "C3", "221","One or more first degree relatives with endometrial cancer age <= 50","INCLUSION")
    , CO_2_RELATIVES_WITH_1_LT_50_CA_FHX_CRIT_2023("CO_2_RELATIVES_WITH_1_LT_50_CA_FHX_CRIT_2023", "C4", "232","Two or more first-degree or second-degree relatives with Lynch syndrome, HNPCC, colon cancer, endometrial, uterine, ovarian, stomach, gastric, small bowel, small intestine, kidney, ureteral, bladder, urethra, brain, or pancreas with >= 1 relative age < 50 years on the same side of the family","INCLUSION")
    , CO_3_RELATIVES_WITH_CA_FHX_CRIT_2023("CO_3_RELATIVES_WITH_CA_FHX_CRIT_2023", "C5", "241","Three or more first or second degree relatives with Lynch syndrome, hereditary non-polyposis colorectal cancer (HNPCC), colon cancer, endometrial cancer, uterine cancer, ovarian cancer, stomach cancer, gastric cancer, small bowel cancer, small intestine cancer, Kidney cancer, ureteral cancer, bladder cancer, urethra cancer, brain cancer, OR pancreas cancer regardless of age AND on the same side of the family","INCLUSION")
    , NO_LONGER_MEETS_CRITERIA_2023("NO_LONGER_MEETS_CRITERIA_2023", "NO", "10","No longer meets criteria for genetic testing due to a family history update.","EXCLUSION");

    private final String id;
    private final String code;

    private final String shortCode;
    private final String label;
    private final String criteriaType;

    Nccn2023Rule(String code, String shortCode, String id, String label, String criteriaType) {
        this.id = id;
        this.shortCode = shortCode;
        this.code = code;
        this.label = label;
        this.criteriaType = criteriaType;
    }

    public static Nccn2023Rule findByCode(String code) {

        for (Nccn2023Rule rule: Nccn2023Rule.values()) {
            if (rule.getCode().equals(code)) {
                return rule;
            }
        }
        return null;
    }

    public static Nccn2023Rule findById(String id) {

        for (Nccn2023Rule rule: Nccn2023Rule.values()) {
            if (rule.getId().equals(id)) {
                return rule;
            }
        }
        return null;
    }

//    @Override
//    public String toString() {
//
//        StringBuilder str = new StringBuilder();
//        str.append("{id:").append(id)
//                .append(", code:").append(code)
//                .append(", shortCode:").append(shortCode)
//                .append(", label:").append(label)
//                .append(", type:").append(criteriaType).append("}")
//        ;
//
//        return str.toString();
//    }
//    @Override
//    public String toString() {
//
//        StringBuilder str = new StringBuilder();
//        str.append("{id:\"").append(id).append("\"")
//                .append(", code:\"").append(code).append("\"")
//                .append(", shortCode:\"").append(shortCode).append("\"")
//                .append(", label:\"").append(label).append("\"")
//                .append(", type:\"").append(criteriaType).append("\"}")
//        ;
//
//        return str.toString();
//    }
}

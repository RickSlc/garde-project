package edu.utah.rehr.garde.domain.core.model.fhir.r4;

import edu.utah.rehr.garde.domain.core.terminology.Code;
import org.hl7.fhir.r4.model.*;

import java.util.Date;

public class ObservationHelperR4 {

    public static void setPatientId(Observation observation, String patientId) {
        // Create a reference to the patient resource using the given patient ID
        Reference patientReference = new Reference("Patient/" + patientId);
        observation.setSubject(patientReference);
    }

    public static String getPatientId(Observation observation) {
        Reference patientReference = observation.getSubject();
        if (patientReference != null) {
            String patientRef = patientReference.getReference();
            if (patientRef != null && patientRef.startsWith("Patient/")) {
                return patientRef.substring("Patient/".length());
            }
        }
        return null;
    }

    // Method to set the observation code
    public static void setObservationCode(Observation observation, Code code) {
        Coding coding = new Coding()
                .setSystem(code.getCodeSystem())
                .setCode(code.getCode())
                .setDisplay(code.getCodeLabel());
        CodeableConcept codeableConcept = new CodeableConcept().addCoding(coding);
        observation.setCode(codeableConcept);
    }

    // Method to get the observation code
    public static Code getObservationCode(Observation observation) {
        CodeableConcept codeableConcept = observation.getCode();
        if (codeableConcept != null && !codeableConcept.getCoding().isEmpty()) {
            Coding coding = codeableConcept.getCodingFirstRep();
            return new Code(coding.getCodeElement().getSystem(), coding.getCodeElement().getCode(), coding.getDisplayElement().getValue());
        }
        return null;
    }

    // Method to set the observation integer value
    public static void setValueInteger(Observation observation, int value) {
        observation.setValue(new org.hl7.fhir.r4.model.IntegerType(value));
    }

    // Method to get the observation integer value
    public static Integer getValueInteger(Observation observation) {
        if (observation.hasValueIntegerType()) {
            return observation.getValueIntegerType().getValue();
        }
        return null;
    }

    // Method to set the effective date (observation date)
    public static void setEffectiveDateTime(Observation observation, Date effectiveDate) {
        observation.setEffective(new DateTimeType(effectiveDate));
    }

    // Method to get the effective date (observation date)
    public static Date getEffectiveDateTime(Observation observation) {
        if (observation.hasEffectiveDateTimeType()) {
            return observation.getEffectiveDateTimeType().getValue();
        }
        return null;
    }

    // Method to set the observation status
    public static void setObservationStatus(Observation observation, Observation.ObservationStatus status) {
        observation.setStatus(status);
    }

    // Method to create and return an observation with all parameters
    public static Observation toObservation(String patientId, Code obsCode, int value, Date effectiveDate) {
        Observation observation = new Observation();
        setPatientId(observation, patientId);
        setObservationCode(observation, obsCode);
        setValueInteger(observation, value);
        setEffectiveDateTime(observation, effectiveDate);
        setObservationStatus(observation, Observation.ObservationStatus.FINAL);
        return observation;
    }

}

package edu.utah.rehr.garde.domain.core.terminology;

import lombok.Getter;

@Getter
public class CodeSetDefinition {

    private final String codeSystem;
    private final String codeSetBinding;
    private final String definition;

    public CodeSetDefinition(String codeSystem, String codeSetBinding, String definition) {
        this.codeSystem = codeSystem;
        this.codeSetBinding = codeSetBinding;
        this.definition = definition;
    }

}

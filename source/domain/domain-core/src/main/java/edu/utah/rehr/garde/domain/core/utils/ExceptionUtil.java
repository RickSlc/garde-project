package edu.utah.rehr.garde.domain.core.utils;

public class ExceptionUtil {

    /**
     * Returns the first N characters of an exception's message.
     *
     * @param e the exception
     * @param n the number of characters to return
     * @return the first N characters of the exception's message or the full message if it is shorter than N
     */
    public static String getMessageString(Exception e, int n) {
        if (e == null) {
            throw new IllegalArgumentException("Exception cannot be null");
        }
        if (n <= 0) {
            return e.getMessage();
        }

        String message = e.getMessage();
        if (message == null) {
            return null;
        }

        return message.length() <= n ? message : message.substring(0, n);
    }
}

package edu.utah.rehr.garde.domain.core.provider;

import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.model.GCohort;
import edu.utah.rehr.garde.domain.core.model.PatientDataElement;

import java.util.List;

public interface IPatientDataElementProvider {

    UResult<List<PatientDataElement>> getPatientDataElements(GCohort cohort, Integer version);
    UResult<Integer> writePatientDataElements(List<PatientDataElement> dataElements);

}

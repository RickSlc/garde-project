package edu.utah.rehr.garde.domain.core.terminology;

import edu.utah.rehr.garde.domain.core.model.dto.CategoryCodeAnalyticsDTO;
import edu.utah.rehr.garde.domain.core.model.dto.MeasureCategoryDTO;
import edu.utah.rehr.garde.domain.core.model.dto.MeasureDTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CategoricalCodeAnalytics {

    private final Map<String, Map<Code, CodeAnalytics>> categoryCodeMap;

    public CategoricalCodeAnalytics() {
        this.categoryCodeMap = new HashMap<>();
    }

    public Map<String, Map<Code, CodeAnalytics>> getCategoryCodeMap() {
        return categoryCodeMap;
    }

    public CodeAnalytics getCodeCount(String category, Code code) {
        return categoryCodeMap.get(category).get(code);
    }

    public void add(String category, Code code) {

        if (!categoryCodeMap.containsKey(category)) {
            // category doesn't exist, create new and add first code
            Map<Code, CodeAnalytics> codeMap = new HashMap<>();
            codeMap.put(code, new CodeAnalytics(code, 1));
            categoryCodeMap.put(category, codeMap);
        } else if (categoryCodeMap.get(category).get(code) != null) {
            // category exists, code exists
            CodeAnalytics codeAnalytics = categoryCodeMap.get(category).get(code);
            codeAnalytics.incrementCount();
        } else {
            // category exists, code does not, add new
            categoryCodeMap.get(category)
                    .put(code, new CodeAnalytics(code, 1));
        }

    }

    public void add(String category, CodeAnalytics codeAnalytics) {

        if (!categoryCodeMap.containsKey(category)) {
            // category doesn't exist, create new and add first code
            Map<Code, CodeAnalytics> codeMap = new HashMap<>();
            codeMap.put(codeAnalytics.getCode(), codeAnalytics);
            categoryCodeMap.put(category, codeMap);
        } else if (categoryCodeMap.get(category).get(codeAnalytics.getCode()) != null) {
            // category exists, code exists
            CodeAnalytics ca = categoryCodeMap.get(category).get(codeAnalytics.getCode());
            ca.incrementCount();
        } else {
            // category exists, code does not, add new
            categoryCodeMap.get(category)
                    .put(codeAnalytics.getCode(), codeAnalytics);
        }

    }

    public List<MeasureCategoryDTO> toMeasureCategoryDTO() {
        List<MeasureCategoryDTO> measureCategoryDTOList = new ArrayList<>();

        for (String category : categoryCodeMap.keySet()) {

            MeasureCategoryDTO measureCategoryDTO = new MeasureCategoryDTO();
            measureCategoryDTO.measureCategory = category;

            Map<Code, CodeAnalytics> codeCountMap = categoryCodeMap.get(category);
            List<MeasureDTO> measures = new ArrayList<>();

            for (Code code : codeCountMap.keySet()) {
                CodeAnalytics codeAnalytics = codeCountMap.get(code);
                String measureLabel = code.getCodeSystem() + ":" + code.getCodeLabel() + ":" + code.getCode();
                MeasureDTO measure = new MeasureDTO();
                measure.measureLabel = measureLabel;
                measure.measureValue = "" + codeAnalytics.count;
                measures.add(measure);
            }
            measureCategoryDTO.measures = measures;
            measureCategoryDTOList.add(measureCategoryDTO);
        }

        return measureCategoryDTOList;
    }

    public List<CategoryCodeAnalyticsDTO> toCategoryCodeAnalyticsDTO() {

        List<CategoryCodeAnalyticsDTO> categoryCodeAnalyticsDTOList = new ArrayList<>();

        for (String category : categoryCodeMap.keySet()) {

            Map<Code, CodeAnalytics> codeMap = categoryCodeMap.get(category);
            List<CodeAnalytics> codeAnalyticsList = new ArrayList<>(codeMap.values());
            CategoryCodeAnalyticsDTO categoryCodeAnalyticsDTO = new CategoryCodeAnalyticsDTO();
            categoryCodeAnalyticsDTO.category = category;
            categoryCodeAnalyticsDTO.codeAnalytics = codeAnalyticsList;
            categoryCodeAnalyticsDTOList.add(categoryCodeAnalyticsDTO);

        }

        return categoryCodeAnalyticsDTOList;
    }


}

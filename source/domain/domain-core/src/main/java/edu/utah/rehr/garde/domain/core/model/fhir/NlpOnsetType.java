package edu.utah.rehr.garde.domain.core.model.fhir;

public enum NlpOnsetType {

    ONSET_RANGE
    , ONSET_AGE
    , ONSET_PERIOD
    ;

    public static NlpOnsetType findByName(String name) {
        for (NlpOnsetType onsetType: NlpOnsetType.values()) {
            if (onsetType.name().equals(name)) {
                return onsetType;
            }
        }
        return null;
    }
}

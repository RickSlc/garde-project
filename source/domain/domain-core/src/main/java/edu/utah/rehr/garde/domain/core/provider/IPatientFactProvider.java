package edu.utah.rehr.garde.domain.core.provider;

import edu.utah.rehr.garde.domain.core.model.GCohort;
import edu.utah.rehr.garde.domain.core.model.PatientFact;

import java.util.List;

@Deprecated
public interface IPatientFactProvider {

    List<PatientFact> getPatientFacts(GCohort cohort, Long version);

}

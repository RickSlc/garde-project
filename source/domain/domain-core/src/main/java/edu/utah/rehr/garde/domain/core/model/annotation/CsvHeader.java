package edu.utah.rehr.garde.domain.core.model.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface CsvHeader {
    String name();
    boolean required() default false;
}
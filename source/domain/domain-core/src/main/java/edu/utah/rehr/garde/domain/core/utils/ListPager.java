package edu.utah.rehr.garde.domain.core.utils;

import java.util.List;

public class ListPager {
    public static <T> List<T> getPage(List<T> items, int pageSize, int pageNumber) {
        int startIndex = (pageNumber - 1) * pageSize;
        int endIndex = Math.min(startIndex + pageSize, items.size());

        if (startIndex >= endIndex) {
            // Invalid page number, return an empty list
            return List.of();
        } else {
            return items.subList(startIndex, endIndex);
        }
    }
}

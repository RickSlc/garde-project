package edu.utah.rehr.garde.domain.core.utils;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.jdbc.DataSourceBuilder;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

@Log
public class SqlProcessorUtil {

    private final DataSource dataSource;
    private final String url;
    private final String username;
    private final String password;

    public SqlProcessorUtil(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;

        this.dataSource = getDataSource();
        if (this.dataSource == null) {
            log.severe("INVALID DB PROPERTIES - " + this.getClass().getCanonicalName() + " UNUSABLE.");
        }

        if (!url.contains("jdbc:sqlserver")) {
            try {
                logTables();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
    
    public String setReplacementVariables(String sql, String variable, String value) {
        String updatedSql = sql.replace("{{" + variable + "}}", value);
        return updatedSql;
    }

    public void logTables() throws SQLException {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs0 = null;
        try {

            conn = dataSource.getConnection();
            DatabaseMetaData metaData = conn.getMetaData();
            log.info("CONNECTED TO " + metaData.getDatabaseProductName());
            stmt = conn.createStatement();

            int tableCount = 0;

            if (url.contains("jdbc:postgresql")) {

                rs0 = stmt.executeQuery("SELECT tablename FROM pg_catalog.pg_tables where schemaname = 'public'");
                while (rs0.next()) {
                    log.info("POSTGRES FOUND TABLE: " + rs0.getString(1) );
                    tableCount++;
                }

            } else if (url.contains("jdbc:mysql")) {

                rs0 = stmt.executeQuery("show tables");
                while (rs0.next()) {
                    log.info("MYSQL FOUND TABLE: " + rs0.getString(1) + ":" + rs0.getString(2));
                    tableCount++;
                }

            } else if (url.contains("jdbc:sqlserver")) {

                rs0 = stmt.executeQuery("SELECT TABLE_NAME, TABLE_TYPE FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE in ('BASE TABLE','VIEW')");
                while (rs0.next()) {
                    log.info("SQLSERVER FOUND TABLE: " + rs0.getString(1) + ":" + rs0.getString(2));
                    tableCount++;
                }

            } else if (url.contains("jdbc:oracle")) {

                rs0 = stmt.executeQuery("SELECT TNAME, TABTPE FROM TAB");
                while (rs0.next()) {
                    log.info("ORACLE FOUND TABLE: " + rs0.getString(1) + ":" + rs0.getString(2));
                    tableCount++;
                }

            } else {
                log.warning("UNKNOWN - QUERY TO CHECK TABLES FOR: " + url);
            }

        } catch (SQLException e) {
            log.warning(e.getMessage());
        } finally {
            closeQuietly(rs0);
            closeQuietly(stmt);
            closeQuietly(conn);
        }
    }

    public String getSqlFromFile(String fileName) {
        try {
            return FileReaderUtil.getStringFromFile(fileName);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String[] splitSQLStatements(String sql) {
        return sql.split(";\\s*");
    }

    public void execute(String sql) {

        Connection conn = null;
        PreparedStatement preparedStatement = null;

        try {
            log.info("DB USER: " + username);

            conn = dataSource.getConnection();
            DatabaseMetaData metaData = conn.getMetaData();
            log.info("CONNECTED TO " + metaData.getDatabaseProductName());

            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.execute();

        } catch (Throwable e) {
            throw new RuntimeException(e);
        } finally {
            closeQuietly(preparedStatement);
            closeQuietly(conn);
        }
    }

    public boolean objectExists(String objectName, String objectType) {

        Connection conn = null;
        ResultSet resultSet = null;
        boolean found = false;

        try {

            conn = dataSource.getConnection();
            DatabaseMetaData dbmd = conn.getMetaData();
            resultSet = dbmd.getTables(null, null, objectName, new String[]{objectType.toUpperCase()});

            if (resultSet.next()) {
                found = true;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeQuietly(resultSet);
            closeQuietly(conn);
        }

        return found;
    }

    public Map<String, Object> getStoredAppProperties() {

        Map<String, Object> propertySource = new HashMap<>();
        Connection conn = null;
        PreparedStatement preparedStatementResultSet = null;
        ResultSet resultSet = null;

        try {
            conn = dataSource.getConnection();
            preparedStatementResultSet = conn.prepareStatement("SELECT property_cd, property_value, instructions FROM g_app_property");
            resultSet = preparedStatementResultSet.executeQuery();

            while (resultSet.next()) {
                propertySource.put(resultSet.getString(1), resultSet.getString(2));
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeQuietly(resultSet);
            closeQuietly(preparedStatementResultSet);
            closeQuietly(conn);
        }

        return propertySource;
    }

    private void closeQuietly(AutoCloseable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception e) {
                log.warning(e.getMessage());
            }
        }
    }

    private DataSource getDataSource() {

        if (StringUtils.isEmpty(url)
                || StringUtils.isEmpty(username)
                || StringUtils.isEmpty(password)) {
            return null;
        }

        DataSource ds = DataSourceBuilder
                .create()
                .url(url)
                .username(username)
                .password(password)
                .build();

        return ds;
    }

}

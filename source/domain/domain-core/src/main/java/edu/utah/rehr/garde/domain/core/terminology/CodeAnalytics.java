package edu.utah.rehr.garde.domain.core.terminology;

public class CodeAnalytics {

    public int count;
    public int isMappedAsStandardCode;
    public int isMappedAsEhrCode;
    private final Code code;

    public CodeAnalytics(Code code) {
        this.code = code;
        this.count = 1;
    }

    public CodeAnalytics(Code code, int count) {
        this.code = code;
        this.count = count;
    }

    public Code getCode() {
        return code;
    }

    public void incrementCount() {
        this.count++;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Code) {
            return this.code.equals(obj);
        }
        if (obj instanceof CodeAnalytics) {
            Code compCode = ((CodeAnalytics) obj).code;
            return this.code.equals(compCode);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return code.hashCode();
    }
}

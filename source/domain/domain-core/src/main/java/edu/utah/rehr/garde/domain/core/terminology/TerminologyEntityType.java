package edu.utah.rehr.garde.domain.core.terminology;

/**
 *
 * @author RickSlc
 */
public enum TerminologyEntityType {

    CODE("CODE")
//    , CODE_SYSTEM("CODE_SYSTEM")
    , CODE_SET("CODE_SET")
    , URI("URI")
    ;

    String key;

    TerminologyEntityType(String key) {
        this.key=key;
    }

    public String getKey() {
        return key;
    }
}

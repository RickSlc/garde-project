package edu.utah.rehr.garde.domain.core.model;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
public class MetCriteriaFactKey {

    private final String ehrPatientId;
    private final String ehrEncounterId;
    private final String ehrRecordId;
    private final String cohortCd;
    private final String criteriaId;

    public MetCriteriaFactKey(String ehrPatientId, String ehrEncounterId, String ehrRecordId, String cohortCd, String criteriaId) {

        if (StringUtils.isEmpty(ehrPatientId) ) {
            throw new IllegalArgumentException("ehrPatientId is required.");
        }
        if (StringUtils.isEmpty(ehrPatientId) ) {
            throw new IllegalArgumentException("ehrEncounterId is required.");
        }
        if (StringUtils.isEmpty(ehrPatientId) ) {
            throw new IllegalArgumentException("ehrRecordId is required.");
        }
        if (StringUtils.isEmpty(ehrPatientId) ) {
            throw new IllegalArgumentException("cohortCd is required.");
        }
        if (StringUtils.isEmpty(ehrPatientId) ) {
            throw new IllegalArgumentException("criteriaId is required.");
        }

        this.ehrPatientId = ehrPatientId;
        this.ehrEncounterId = ehrEncounterId;
        this.ehrRecordId = ehrRecordId;
        this.cohortCd = cohortCd;
        this.criteriaId = criteriaId;
    }

    @Override
    public String toString() {
        return ehrPatientId + "\\" +
                ehrEncounterId + "\\" +
                ehrRecordId + "\\" +
                cohortCd + "\\" +
                criteriaId
                ;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj == null || !this.getClass().isAssignableFrom(obj.getClass())) {
            return false;
        }

        MetCriteriaFactKey key = (MetCriteriaFactKey) obj;

        return (this.ehrPatientId.equals(key.getEhrPatientId())
                && this.ehrEncounterId.equals(key.getEhrEncounterId())
                && this.ehrRecordId.equals(key.getEhrRecordId())
                && this.cohortCd.equals(key.getCohortCd())
                && this.criteriaId.equals(key.getCriteriaId())
        );
    }

    @Override
    public int hashCode() {
        return this.ehrPatientId.hashCode()
                + this.ehrEncounterId.hashCode()
                + this.ehrRecordId.hashCode()
                + cohortCd.hashCode() * 37
                + criteriaId.hashCode() * 17
                ;
    }

}

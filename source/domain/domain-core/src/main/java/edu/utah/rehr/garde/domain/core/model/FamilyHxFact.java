package edu.utah.rehr.garde.domain.core.model;

import java.util.Date;
@Deprecated
public class FamilyHxFact {

    private String familyHxId;
    private String ehrCodeSystem;
    private String ehrPatientId;
    private Integer ehrEncounterId;
    private Integer ehrRecordId;
    private Date ehrEncounterDate;
    private String recordSource;
    private Date recordDate;

    private String ehrConditionCodeSystem;
    private String ehrConditionCode;
    private String ehrConditionLabel;
    private String canonicalConditionCodeSystem;
    private String canonicalConditionCode;
    private String canonicalConditionLabel;
    private String internalConditionCode;

    private String ehrRelationshipCodeSystem;
    private String ehrRelationshipCode;
    private String ehrRelationshipLabel;
    private String canonicalRelationshipCodeSystem;
    private String canonicalRelationshipCode;
    private String canonicalRelationshipLabel;
    private String internalRelationshipCode;

    private Integer ehrConditionOnsetAge;
    private String comments;

    public FamilyHxFact() {}

    public FamilyHxFact(String familyHxId, String ehrCodeSystem, String ehrPatientId, Integer ehrEncounterId, Integer ehrRecordId, Date ehrEncounterDate
            , String ehrConditionCodeSystem, String ehrConditionCode, String ehrConditionLabel
            , String canonicalConditionCodeSystem, String canonicalConditionCode, String canonicalConditionLabel
            , String internalConditionCode
            , String ehrRelationshipCodeSystem, String ehrRelationshipCode, String ehrRelationshipLabel
            , String canonicalRelationshipCodeSystem, String canonicalRelationshipCode, String canonicalRelationshipLabel
            , String internalRelationshipCode
            , Integer ehrConditionOnsetAge, String comments) {

        this.familyHxId = familyHxId;
        this.ehrCodeSystem = ehrCodeSystem;
        this.ehrPatientId = ehrPatientId;
        this.ehrEncounterId = ehrEncounterId;
        this.ehrRecordId = ehrRecordId;
        this.ehrEncounterDate = ehrEncounterDate;
        this.ehrConditionCodeSystem = ehrConditionCodeSystem;
        this.ehrConditionCode = ehrConditionCode;
        this.ehrConditionLabel = ehrConditionLabel;
        this.canonicalConditionCodeSystem = canonicalConditionCodeSystem;
        this.canonicalConditionCode = canonicalConditionCode;
        this.canonicalConditionLabel = canonicalConditionLabel;
        this.internalConditionCode = internalConditionCode;
        this.ehrRelationshipCodeSystem = ehrRelationshipCodeSystem;
        this.ehrRelationshipCode = ehrRelationshipCode;
        this.ehrRelationshipLabel = ehrRelationshipLabel;
        this.canonicalRelationshipCodeSystem = canonicalRelationshipCodeSystem;
        this.canonicalRelationshipCode = canonicalRelationshipCode;
        this.canonicalRelationshipLabel = canonicalRelationshipLabel;
        this.internalRelationshipCode = internalRelationshipCode;
        this.ehrConditionOnsetAge = ehrConditionOnsetAge;
        this.comments = comments;

    }

    /**
     * Constructor for EHR-only codes
     * @param familyHxId
     * @param ehrCodeSystem
     * @param ehrPatientId
     * @param ehrEncounterId
     * @param ehrRecordId
     * @param ehrEncounterDate
     * @param recordSource
     * @param recordDate
     * @param ehrConditionCodeSystem
     * @param ehrConditionCode
     * @param ehrConditionLabel
     * @param ehrRelationshipCodeSystem
     * @param ehrRelationshipCode
     * @param ehrRelationshipLabel
     * @param ehrConditionOnsetAge
     * @param comments
     */
    public FamilyHxFact(String familyHxId, String ehrCodeSystem, String ehrPatientId, Integer ehrEncounterId, Integer ehrRecordId, Date ehrEncounterDate, String recordSource, Date recordDate, String ehrConditionCodeSystem, String ehrConditionCode, String ehrConditionLabel, String ehrRelationshipCodeSystem, String ehrRelationshipCode, String ehrRelationshipLabel, Integer ehrConditionOnsetAge, String comments) {
        this.familyHxId = familyHxId;
        this.ehrCodeSystem = ehrCodeSystem;
        this.ehrPatientId = ehrPatientId;
        this.ehrEncounterId = ehrEncounterId;
        this.ehrRecordId = ehrRecordId;
        this.ehrEncounterDate = ehrEncounterDate;
        this.recordSource = recordSource;
        this.recordDate = recordDate;
        this.ehrConditionCodeSystem = ehrConditionCodeSystem;
        this.ehrConditionCode = ehrConditionCode;
        this.ehrConditionLabel = ehrConditionLabel;
        this.ehrRelationshipCodeSystem = ehrRelationshipCodeSystem;
        this.ehrRelationshipCode = ehrRelationshipCode;
        this.ehrRelationshipLabel = ehrRelationshipLabel;
        this.ehrConditionOnsetAge = ehrConditionOnsetAge;
        this.comments = comments;
    }

    public String getFamilyHxId() {
        return familyHxId;
    }

    public String getEhrCodeSystem() {
        return ehrCodeSystem;
    }

    public void setEhrCodeSystem(String ehrCodeSystem) {
        this.ehrCodeSystem = ehrCodeSystem;
    }

    public String getEhrPatientId() {
        return ehrPatientId;
    }

    public void setEhrPatientId(String ehrPatientId) {
        this.ehrPatientId = ehrPatientId;
    }

    public Integer getEhrEncounterId() {
        return ehrEncounterId;
    }

    public void setEhrEncounterId(Integer ehrEncounterId) {
        this.ehrEncounterId = ehrEncounterId;
    }

    public Integer getEhrRecordId() {
        return ehrRecordId;
    }

    public void setEhrRecordId(Integer ehrRecordId) {
        this.ehrRecordId = ehrRecordId;
    }

    public Date getEhrEncounterDate() {
        return ehrEncounterDate;
    }

    public void setEhrEncounterDate(Date ehrEncounterDate) {
        this.ehrEncounterDate = ehrEncounterDate;
    }

    public String getEhrConditionCodeSystem() {
        return ehrConditionCodeSystem;
    }

    public void setEhrConditionCodeSystem(String ehrConditionCodeSystem) {
        this.ehrConditionCodeSystem = ehrConditionCodeSystem;
    }

    public String getEhrConditionCode() {
        return ehrConditionCode;
    }

    public void setEhrConditionCode(String ehrConditionCode) {
        this.ehrConditionCode = ehrConditionCode;
    }

    public String getEhrConditionLabel() {
        return ehrConditionLabel;
    }

    public void setEhrConditionLabel(String ehrConditionLabel) {
        this.ehrConditionLabel = ehrConditionLabel;
    }

    public String getCanonicalConditionCodeSystem() {
        return canonicalConditionCodeSystem;
    }

    public void setCanonicalConditionCodeSystem(String canonicalConditionCodeSystem) {
        this.canonicalConditionCodeSystem = canonicalConditionCodeSystem;
    }

    public String getCanonicalConditionCode() {
        return canonicalConditionCode;
    }

    public void setCanonicalConditionCode(String canonicalConditionCode) {
        this.canonicalConditionCode = canonicalConditionCode;
    }

    public String getCanonicalConditionLabel() {
        return canonicalConditionLabel;
    }

    public void setCanonicalConditionLabel(String canonicalConditionLabel) {
        this.canonicalConditionLabel = canonicalConditionLabel;
    }

    public String getInternalConditionCode() {
        return internalConditionCode;
    }

    public void setInternalConditionCode(String internalConditionCode) {
        this.internalConditionCode = internalConditionCode;
    }

    public String getEhrRelationshipCodeSystem() {
        return ehrRelationshipCodeSystem;
    }

    public void setEhrRelationshipCodeSystem(String ehrRelationshipCodeSystem) {
        this.ehrRelationshipCodeSystem = ehrRelationshipCodeSystem;
    }

    public String getEhrRelationshipCode() {
        return ehrRelationshipCode;
    }

    public void setEhrRelationshipCode(String ehrRelationshipCode) {
        this.ehrRelationshipCode = ehrRelationshipCode;
    }

    public String getEhrRelationshipLabel() {
        return ehrRelationshipLabel;
    }

    public void setEhrRelationshipLabel(String ehrRelationshipLabel) {
        this.ehrRelationshipLabel = ehrRelationshipLabel;
    }

    public String getCanonicalRelationshipCodeSystem() {
        return canonicalRelationshipCodeSystem;
    }

    public void setCanonicalRelationshipCodeSystem(String canonicalRelationshipCodeSystem) {
        this.canonicalRelationshipCodeSystem = canonicalRelationshipCodeSystem;
    }

    public String getCanonicalRelationshipCode() {
        return canonicalRelationshipCode;
    }

    public void setCanonicalRelationshipCode(String canonicalRelationshipCode) {
        this.canonicalRelationshipCode = canonicalRelationshipCode;
    }

    public String getCanonicalRelationshipLabel() {
        return canonicalRelationshipLabel;
    }

    public void setCanonicalRelationshipLabel(String canonicalRelationshipLabel) {
        this.canonicalRelationshipLabel = canonicalRelationshipLabel;
    }

    public String getInternalRelationshipCode() {
        return internalRelationshipCode;
    }

    public void setInternalRelationshipCode(String internalRelationshipCode) {
        this.internalRelationshipCode = internalRelationshipCode;
    }

    public Integer getEhrConditionOnsetAge() {
        return ehrConditionOnsetAge;
    }

    public void setEhrConditionOnsetAge(Integer ehrConditionOnsetAge) {
        this.ehrConditionOnsetAge = ehrConditionOnsetAge;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getRecordSource() {
        return recordSource;
    }

    public void setRecordSource(String recordSource) {
        this.recordSource = recordSource;
    }

    public Date getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(Date recordDate) {
        this.recordDate = recordDate;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        FamilyHxFact fhx = (FamilyHxFact) obj;

        return (fhx.getEhrCodeSystem().equals(fhx.ehrCodeSystem)
                && fhx.getEhrPatientId().equals(this.ehrPatientId)
                && fhx.getEhrEncounterId().equals(this.ehrEncounterId)
                && fhx.getEhrRecordId().equals(this.ehrRecordId)
        );
    }

    @Override
    public int hashCode() {
        return Integer.parseInt(this.ehrPatientId)
                + this.ehrEncounterId
                + this.ehrRecordId;
    }

}

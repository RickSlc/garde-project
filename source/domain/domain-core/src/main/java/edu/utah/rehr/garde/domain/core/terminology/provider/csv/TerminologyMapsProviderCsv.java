package edu.utah.rehr.garde.domain.core.terminology.provider.csv;

import edu.utah.rehr.garde.domain.core.terminology.Code;
import lombok.extern.java.Log;

import java.util.*;

@Log
public class TerminologyMapsProviderCsv {

    public static Map<String, Set<Code>> getCodeSetMap(String csvFile) {

        Map<String, Set<Code>> codeSetMap = new HashMap<>();

        List<String[]> codeSetRows = TerminologyReaderCsv.getCodeSetRecords(csvFile);

        for (String[] row: codeSetRows) {
            String codeSystem = row[0];
            String codeSetBinding = row[1];
            String bindingCode = row[2];

            Set<Code> codes;
            if (codeSetMap.containsKey(codeSetBinding)) {
                codes = codeSetMap.get(codeSetBinding);
            } else {
                codes = new HashSet<>();
            }

            codes.add(new Code(bindingCode, codeSystem, bindingCode, bindingCode));
            codeSetMap.put(codeSetBinding, codes);
        }

        return codeSetMap;
    }

    public static Map<String, Set<Code>> getCodeBindingsMap(String csvFile) {

        Map<String, Set<Code>> codeBindingMap = new HashMap<>();

        List<String[]> codeBindingsRows = TerminologyReaderCsv.getBindingCodeAssociationRecords(csvFile);

        for (String[] row: codeBindingsRows) {
            String bindingCode = row[0];
            String codeSystem = row[1];
            String code = row[2];
            String codeLabel = row[3];

            Set<Code> codes;
            if (codeBindingMap.containsKey(bindingCode)) {
                codes = codeBindingMap.get(bindingCode);
            } else {
                codes = new HashSet<>();
            }

            codes.add(new Code(bindingCode, codeSystem, code, codeLabel));
            codeBindingMap.put(bindingCode, codes);
        }


        return codeBindingMap;
    }

    public static Map<String, String> getUriBindingsMap(String csvFile) {

        Map<String, String> uriBindingMap = new HashMap<>();
        List<String[]> uriBindingsRows = TerminologyReaderCsv.getUriBindings(csvFile);

        for (String[] row: uriBindingsRows) {
            String uriBinding = row[1];
            String uri = row[2];

            uriBindingMap.put(uriBinding, uri);
        }

        return uriBindingMap;
    }

}

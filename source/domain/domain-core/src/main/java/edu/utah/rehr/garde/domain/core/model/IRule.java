package edu.utah.rehr.garde.domain.core.model;

public interface IRule {

    String getId();

    String getCode();

    String getShortCode();

    String getLabel();

    String getCriteriaType();

}

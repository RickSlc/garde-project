package edu.utah.rehr.garde.domain.core.provider;

import edu.utah.rehr.garde.domain.core.model.FamilyHxFact;
import edu.utah.rehr.garde.domain.core.model.GCohort;

import java.util.List;

@Deprecated
public interface IFamilyHxFactProvider {

    List<FamilyHxFact> getFamilyHxFacts(GCohort cohort, Long version);

}

package edu.utah.rehr.garde.domain.core.terminology.provider.csv;

import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.terminology.CodeSetDefinition;
import edu.utah.rehr.garde.domain.core.utils.FileReaderUtil;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.*;

public class TerminologyReaderCsv {

    private static final Logger log = LoggerFactory.getLogger(TerminologyReaderCsv.class);

    public final static String[] CODE_SET_HEADER = {"CODE_SYSTEM", "SET_BINDING", "CODE_BINDING"};
    public final static String[] CODE_BINDING_HEADER = {"CODE_BINDING", "CODE_SYSTEM", "CODE", "CODE_LABEL"};
    public final static String[] URI_MAPPING_HEADER = {"URI_TYPE","URI_BINDING","URI"};
    public final static String[] CODE_SET_DEFINITION_HEADER = {"CODE_SYSTEM","SET_BINDING","DEFINITION"};

    public static List<String[]> getCodeSetDefinitions(String csvFilePath) {

        Iterable<CSVRecord> records = getRecords(CODE_SET_DEFINITION_HEADER, csvFilePath);

        List<String[]> codeSetDefinitions = new ArrayList<>();

        for (CSVRecord record : records) {
            String[] recordValues = {record.get(CODE_SET_DEFINITION_HEADER[0]), record.get(CODE_SET_DEFINITION_HEADER[1]), record.get(CODE_SET_DEFINITION_HEADER[2])};
            codeSetDefinitions.add(recordValues);
        }

        return codeSetDefinitions;
    }

    public static Map<String, CodeSetDefinition> getCodeSetDefinitionMap(String csvFilePath) {
        List<String[]> codeSetDefinitions = getCodeSetDefinitions(csvFilePath);
        Map<String, CodeSetDefinition> codeSetDefinitionMap = new HashMap<>();
        for (String[] codeSetDef: codeSetDefinitions) {

            CodeSetDefinition codeSetDefinition = new CodeSetDefinition(
                codeSetDef[0]
                    , codeSetDef[1]
                    , codeSetDef[2]
            );

            codeSetDefinitionMap.put(codeSetDef[1], codeSetDefinition);
        }
        return codeSetDefinitionMap;
    }

    public static List<String[]> getUriBindings(String csvFilePath) {

        Iterable<CSVRecord> records = getRecords(URI_MAPPING_HEADER, csvFilePath);

        List<String[]> uriBindings = new ArrayList<>();

        for (CSVRecord record : records) {
            String[] recordValues = {record.get(URI_MAPPING_HEADER[0]), record.get(URI_MAPPING_HEADER[1]), record.get(URI_MAPPING_HEADER[2])};
            uriBindings.add(recordValues);
        }

        return uriBindings;
    }

    public static List<String[]> getCodeSetRecords(String csvFilePath) {

        Iterable<CSVRecord> records = getRecords(CODE_SET_HEADER, csvFilePath);

        List<String[]> codeSetRecords = new ArrayList<>();

        for (CSVRecord record : records) {
            String[] recordValues = {record.get(CODE_SET_HEADER[0]), record.get(CODE_SET_HEADER[1]), record.get(CODE_SET_HEADER[2])};
            codeSetRecords.add(recordValues);
        }

        return codeSetRecords;
    }

    public static List<String[]> getBindingCodeAssociationRecords(String csvFilePath) {

        Iterable<CSVRecord> records = getRecords(CODE_BINDING_HEADER, csvFilePath);

        List<String[]> codeBindingRecords = new ArrayList<>();

        for (CSVRecord record : records) {
//            String[] recordValues = { record.get(CODE_BINDING_HEADER[0])
//                    , record.get(CODE_BINDING_HEADER[1])
//                    , record.get(CODE_BINDING_HEADER[2])
//                    , record.get(CODE_BINDING_HEADER[3]) };
            String[] recordValues = { record.get("CODE_BINDING")
                    , record.get("CODE_SYSTEM")
                    , record.get("CODE")
                    , record.get("CODE_LABEL") };
            codeBindingRecords.add(recordValues);
        }

        return codeBindingRecords;
    }

    static String[] removeUtf8_BOM(String[] arrayWithBOM) {
        return Arrays.stream(arrayWithBOM)
                .map(s -> s.replaceFirst("\uFEFF", ""))  // remove BOM
                .toArray(String[]::new);  // convert back to array
    }

    public static UResult<Boolean> isValidHeader(String[] expectedHeader, String csvFile) {
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            String line = br.readLine();
            if ( line.contains("\uFEFF") ) {
                return UResult.errorResult("HEADER CONTAINS UTF-8 BOM - NOT SUPPORTED");
            }
            if (line != null) {
                String[] actualHeader = line
                        .replace(" ","")
                        .replace("\"", "")
                        .split(",");
                return UResult.successResult(Arrays.equals(expectedHeader, removeUtf8_BOM(actualHeader)));
            }
            return UResult.successResult(false);
        } catch (Exception e) {
            return UResult.errorResult(e);
        }
    }

    private static Iterable<CSVRecord> getRecords(String[] header, String csvFilePath) {

        try {
            Reader reader = FileReaderUtil.getReader(csvFilePath);
            CSVFormat format = CSVFormat.DEFAULT
                    .builder()
                    .setHeader(header)
                    .setSkipHeaderRecord(true) // Optional: Explicitly set if header is in first record
                    .setCommentMarker('#')
                    .build();

            return format.parse(reader);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

}

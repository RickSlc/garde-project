package edu.utah.rehr.garde.domain.core.provider;

import edu.utah.rehr.garde.domain.core.model.GCohort;
import org.hl7.fhir.dstu3.model.Patient;

import java.util.List;
import java.util.Map;

@Deprecated
public interface IFhirPatientProvider {

    List<Patient> getFhirPatients(GCohort cohort, Long version);
    Map<String, Patient> getFhirPatientMap(GCohort cohort, Long version);

}

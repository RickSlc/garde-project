package edu.utah.rehr.garde.domain.core.model.fhir.r4;

import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.model.fhir.FuzzyAgeRangeQualifier;
import edu.utah.rehr.garde.domain.core.model.fhir.NlpOnsetType;
import lombok.extern.java.Log;
import org.hl7.fhir.r4.model.Range;
import org.hl7.fhir.r4.model.SimpleQuantity;

@Log
public class FuzzyAgeRangeInterpreterR4 {

    private final int LOOSE_RANGE = 3;
    private final int TIGHT_RANGE = 2;

    // Default to LOOSE_RANGE
    private int NLP_AROUND_RANGE = LOOSE_RANGE;
    private int NLP_EARLY_RANGE = LOOSE_RANGE;
    private int NLP_LATE_RANGE = LOOSE_RANGE;
    private int NLP_MID_RANGE = LOOSE_RANGE;

    public void setNLP_AROUND_RANGE(int NLP_AROUND_RANGE) {
        this.NLP_AROUND_RANGE = NLP_AROUND_RANGE;
    }
    public void setNLP_EARLY_RANGE(int NLP_EARLY_RANGE) {
        this.NLP_EARLY_RANGE = NLP_EARLY_RANGE;
    }
    public void setNLP_LATE_RANGE(int NLP_LATE_RANGE) {
        this.NLP_LATE_RANGE = NLP_LATE_RANGE;
    }
    public void setNLP_MID_RANGE(int NLP_MID_RANGE) {
        this.NLP_MID_RANGE = NLP_MID_RANGE;
    }

    public UResult<Range> interpret(Integer onsetAgeRangeAnchorValue, FuzzyAgeRangeQualifier fuzzyAgeRangeQualifierInput, NlpOnsetType onsetType) {

        FuzzyAgeRangeQualifier fuzzyAgeRangeQualifier = fuzzyAgeRangeQualifierInput;

        if (fuzzyAgeRangeQualifierInput == null) {
            return UResult.warningResult("fuzzyAgeRangeQualifier is a required field." );
        }

        if (onsetType != null && onsetType.equals(NlpOnsetType.ONSET_PERIOD)) {
            return UResult.warningResult(NlpOnsetType.ONSET_PERIOD.name() + " not supported");
        }

        // ONSET_AGE DECADE is an error. Correct.
        if (onsetType != null
                && onsetType.equals(NlpOnsetType.ONSET_AGE)
                && FuzzyAgeRangeQualifier.DECADE.equals(fuzzyAgeRangeQualifierInput)
        ) {
            return UResult.warningResult("ONSET_AGE and DECADE should be converted to an ONSET_AGE EXACT elsewhere.");
        }

        if (onsetType != null
                && fuzzyAgeRangeQualifier.equals(FuzzyAgeRangeQualifier.BEFORE)
                && onsetType.equals(NlpOnsetType.ONSET_RANGE)) {
            return UResult.warningResult(NlpOnsetType.ONSET_RANGE.name() + " " + FuzzyAgeRangeQualifier.BEFORE.name() + " not supported");
        }

        if (onsetAgeRangeAnchorValue == null
                || onsetAgeRangeAnchorValue <= 0
                || onsetAgeRangeAnchorValue > 120
                || fuzzyAgeRangeQualifier == null) {

            String errorMessage = "Range not set. Invalid or unsupported parameters: " + onsetAgeRangeAnchorValue;
            if (fuzzyAgeRangeQualifier == null) {
                errorMessage += "and null fuzzyAgeRangeQualifier";
            } else {
                errorMessage += " " + fuzzyAgeRangeQualifier.getKey();
            }

            log.warning(errorMessage);
            return UResult.warningResult(errorMessage);
        }

        int min = 0;
        int max = 0;

        if (FuzzyAgeRangeQualifier.EARLY.equals(fuzzyAgeRangeQualifier) && onsetAgeRangeAnchorValue % 10 == 0) { // early decade -> 50-53

            min = onsetAgeRangeAnchorValue; // Needs to be divisible by 10
            max = onsetAgeRangeAnchorValue + NLP_EARLY_RANGE;

        } else if (FuzzyAgeRangeQualifier.LATE.equals(fuzzyAgeRangeQualifier) && onsetAgeRangeAnchorValue % 10 == 0) { // late decade -> 56 to 59

            min = onsetAgeRangeAnchorValue - NLP_LATE_RANGE - 1;
            max = onsetAgeRangeAnchorValue + 9;

        } else if (FuzzyAgeRangeQualifier.MID.equals(fuzzyAgeRangeQualifier) && onsetAgeRangeAnchorValue % 10 == 0) { // +/- 3 of middle if decade -> 53 to 57

            int mid = onsetAgeRangeAnchorValue + 5;
            min = mid - NLP_MID_RANGE;
            max = mid + NLP_MID_RANGE;

        } else if (FuzzyAgeRangeQualifier.DECADE.equals(fuzzyAgeRangeQualifier) && onsetAgeRangeAnchorValue % 10 == 0) { // decade of number; 30s -> min = 30 max = 39

            min = onsetAgeRangeAnchorValue;
            max = onsetAgeRangeAnchorValue + 9;

        } else if (FuzzyAgeRangeQualifier.AROUND.equals(fuzzyAgeRangeQualifier)) { // +/- 3 age

            min = onsetAgeRangeAnchorValue - NLP_AROUND_RANGE;
            max = onsetAgeRangeAnchorValue + NLP_AROUND_RANGE;
        }

        if (max > 0) { // when max > 0 implies valid range conditions have been met and set

            SimpleQuantity qmin = new SimpleQuantity();
            qmin.setValue(min);

            SimpleQuantity qmax = new SimpleQuantity();
            qmax.setValue(max);

            Range ageRange = new Range();
            ageRange.setLow(qmin);
            ageRange.setHigh(qmax);

            return UResult.successResult(ageRange);
        }

        String errorMessage = "Range not set. Invalid or unsupported parameters: " + onsetAgeRangeAnchorValue;
        if (fuzzyAgeRangeQualifier == null) {
            errorMessage += " and null fuzzyAgeRangeQualifier";
        } else {
            errorMessage += " " + fuzzyAgeRangeQualifier.getKey();
        }

        log.warning(errorMessage);
        return UResult.errorResult(errorMessage);
    }

    public void setLooseRanges() {

        NLP_AROUND_RANGE = LOOSE_RANGE;
        NLP_EARLY_RANGE = LOOSE_RANGE;
        NLP_LATE_RANGE = LOOSE_RANGE;
        NLP_MID_RANGE = LOOSE_RANGE;
    }

    public void setTightRanges() {

        NLP_AROUND_RANGE = TIGHT_RANGE;
        NLP_EARLY_RANGE = TIGHT_RANGE;
        NLP_LATE_RANGE = TIGHT_RANGE;
        NLP_MID_RANGE = TIGHT_RANGE;
    }

}

package edu.utah.rehr.garde.domain.core.model.fhir.r4;

import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxDTO;
import edu.utah.rehr.garde.domain.core.model.dto.ObservationDTO;
import edu.utah.rehr.garde.domain.core.model.dto.PatientDTO;
import edu.utah.rehr.garde.domain.core.utils.DateUtil;
import org.hl7.fhir.r4.model.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class GFhirToDtoMapperR4 {

    public static List<FamilyHxDTO> toFamilyHxDtoFromBundle(Bundle bundle) {

        List<FamilyHxDTO> familyHxDtoList = new ArrayList<>();

        // Iterate through each entry in the bundle
        for (Bundle.BundleEntryComponent entry : bundle.getEntry()) {

            if (entry.getResource() instanceof FamilyMemberHistory familyMemberHistory) {
                FamilyHxDTO dto = familyMemberHistoryToDto(familyMemberHistory);
                familyHxDtoList.add(dto);
            }
        }
        return familyHxDtoList;
    }

    public static List<ObservationDTO> toObservationDtoFromBundle(Bundle bundle) {

        List<ObservationDTO> obsDtoList = new ArrayList<>();

        // Iterate through each entry in the bundle
        for (Bundle.BundleEntryComponent entry : bundle.getEntry()) {

            if (entry.getResource() instanceof Observation observation) {
                ObservationDTO dto = observationToDto(observation);
                obsDtoList.add(dto);
            }
        }
        return obsDtoList;
    }

    public static FamilyHxDTO familyMemberHistoryToDto(FamilyMemberHistory familyMemberHistory) {
        FamilyHxDTO dto = new FamilyHxDTO();

        // Set family history identifier
        if (familyMemberHistory.hasIdentifier()) {
            Identifier identifier = familyMemberHistory.getIdentifierFirstRep();
            dto.setFamilyHxIdCodeSystem(identifier.getSystem());
            String familyHxId = identifier.getValue();
            dto.setFamilyHxId(familyHxId);
            String[] parts = familyHxId.split("_");
            if (parts.length == 3) {
                dto.setPatientId(parts[0]);
                dto.setEncounterId(parts[1]);
                dto.setRecordId(parts[2]);
            }
        }

        // Set patient identifier
        if (familyMemberHistory.hasPatient() && familyMemberHistory.getPatient().hasIdentifier()) {
            Reference patientReference = familyMemberHistory.getPatient();
            Identifier patientIdentifier = patientReference.getIdentifier();
            dto.setPatientIdCodeSystem(patientIdentifier.getSystem());
            dto.setPatientId(patientIdentifier.getValue());
        }

        // Set encounter date
        if (familyMemberHistory.hasDate()) {
            LocalDateTime fhxDate = DateUtil.toLocalDateTime(familyMemberHistory.getDate());
            dto.setEncounterDateString(fhxDate.format(DateTimeFormatter.ISO_DATE_TIME));
        }

        // Set relationship
        if (familyMemberHistory.hasRelationship()) {
            Coding relationshipCoding = familyMemberHistory.getRelationship().getCodingFirstRep();
            dto.setRelationshipCodeSystem(relationshipCoding.getSystem());
            dto.setRelationshipCode(relationshipCoding.getCode());
            dto.setRelationshipLabel(relationshipCoding.getDisplay());
        }

        // Set condition
        if (familyMemberHistory.hasCondition()) {
            FamilyMemberHistory.FamilyMemberHistoryConditionComponent condition = familyMemberHistory.getConditionFirstRep();
            Coding conditionCoding = condition.getCode().getCodingFirstRep();
            dto.setConditionCodeSystem(conditionCoding.getSystem());
            dto.setConditionCode(conditionCoding.getCode());
            dto.setConditionLabel(conditionCoding.getDisplay());

            if (condition.hasOnsetAge() && condition.getOnsetAge().hasValue()) {
                dto.setConditionOnsetAgeString(condition.getOnsetAge().getValue().toString());
            }
        }

        // Set comments
        if (familyMemberHistory.hasNote() && !familyMemberHistory.getNote().isEmpty()) {
            dto.setComments(familyMemberHistory.getNoteFirstRep().getText());
        }

        return dto;
    }

    public static PatientDTO patientToDto(Patient patient) {
        PatientDTO dto = new PatientDTO();

        // Set patient identifier
        if (patient.hasIdentifier()) {
            dto.setPatientIdCodeSystem(patient.getIdentifierFirstRep().getSystem());
            dto.setPatientId(patient.getIdentifierFirstRep().getValue());
        }

        if (patient.hasId()) {
            dto.setPatientId(patient.getIdElement().getIdPart());
        }

        Enumerations.AdministrativeGender gender = patient.getGender();
        if (gender == null) {
            gender = Enumerations.AdministrativeGender.NULL;
        }

        dto.setSexCodeSystem(gender.getSystem());
        dto.setSexCode(gender.toCode());
        dto.setSexLabel(gender.getDisplay());


        // Set birth date
        if (patient.hasBirthDate()) {
            LocalDate date = DateUtil.toLocalDate(patient.getBirthDate());
            dto.setBirthDateString(date.format(DateTimeFormatter.ISO_DATE));
        }

        // Set race
        patient.getExtension().stream()
                .filter(extension -> "http://utah.edu/rehr/StructureDefinition/race".equals(extension.getUrl()))
                .findFirst()
                .ifPresent(extension -> {
                    Coding raceCoding = ((CodeableConcept) extension.getValue()).getCodingFirstRep();
                    dto.setRaceCodeSystem(raceCoding.getSystem());
                    dto.setRaceCode(raceCoding.getCode());
                    dto.setRaceLabel(raceCoding.getDisplay());
                });

        // Set ethnicity
        patient.getExtension().stream()
                .filter(extension -> "http://utah.edu/rehr/StructureDefinition/ethnicity".equals(extension.getUrl()))
                .findFirst()
                .ifPresent(extension -> {
                    Coding ethnicityCoding = ((CodeableConcept) extension.getValue()).getCodingFirstRep();
                    dto.setEthnicityCodeSystem(ethnicityCoding.getSystem());
                    dto.setEthnicityCode(ethnicityCoding.getCode());
                    dto.setEthnicityLabel(ethnicityCoding.getDisplay());
                });

        // Set language
        if (!patient.getCommunication().isEmpty()) {
            Coding langCoding = patient.getCommunicationFirstRep().getLanguage().getCodingFirstRep();
            dto.setLangCodeSystem(langCoding.getSystem());
            dto.setLangCode(langCoding.getCode());
            dto.setLangLabel(langCoding.getDisplay());
        }

        // Set marital status
        if (patient.hasMaritalStatus()) {
            Coding maritalStatusCoding = patient.getMaritalStatus().getCodingFirstRep();
            dto.setMaritalStatusCodeSystem(maritalStatusCoding.getSystem());
            dto.setMaritalStatusCode(maritalStatusCoding.getCode());
            dto.setMaritalStatusLabel(maritalStatusCoding.getDisplay());
        }

        return dto;
    }

    public static ObservationDTO observationToDto(Observation observation) {
        ObservationDTO dto = new ObservationDTO();

        // Set observation identifier
        if (observation.hasIdentifier()) {
            Identifier identifier = observation.getIdentifierFirstRep();
            dto.setObservationIdCodeSystem(identifier.getSystem());
            dto.setObservationId(identifier.getValue());
        }

        // Set patient identifier
        if (observation.hasSubject() && observation.getSubject().hasIdentifier()) {
            Reference subjectReference = observation.getSubject();
            Identifier subjectIdentifier = subjectReference.getIdentifier();
            dto.setPatientIdCodeSystem(subjectIdentifier.getSystem());
            dto.setPatientId(subjectIdentifier.getValue());
        }

        // Set encounter identifier
        if (observation.hasEncounter() && observation.getEncounter().hasIdentifier()) {
            Reference encounterReference = observation.getEncounter();
            Identifier encounterIdentifier = encounterReference.getIdentifier();
            dto.setEncounterIdCodeSystem(encounterIdentifier.getSystem());
            dto.setEncounterId(encounterIdentifier.getValue());
        }

        // Set observation date/time
        if (observation.hasEffectiveDateTimeType()) {
            LocalDateTime observationDateTime = DateUtil.toLocalDateTime(observation.getEffectiveDateTimeType().getValue());
            dto.setObservationDateTimeString(observationDateTime.format(DateTimeFormatter.ISO_DATE_TIME));
        }

        // Set observation type (category)
        if (observation.hasCategory()) {
            Coding categoryCoding = observation.getCategoryFirstRep().getCodingFirstRep();
            dto.setObservationTypeCodeSystem(categoryCoding.getSystem());
            dto.setObservationType(categoryCoding.getCode());
            dto.setObservationType(categoryCoding.getDisplay());
        }

        // Set observation code
        if (observation.hasCode()) {
            Coding codeCoding = observation.getCode().getCodingFirstRep();
            dto.setObservationCodeSystem(codeCoding.getSystem());
            dto.setObservationCode(codeCoding.getCode());
            dto.setObservationLabel(codeCoding.getDisplay());
        }

        // Set observation value
        if (observation.hasValueQuantity()) {
            dto.setObservationValue(String.valueOf(observation.getValueQuantity().getValue()));
            if (observation.getValueQuantity().hasUnit()) {
                dto.setObservationValueUnits(observation.getValueQuantity().getUnit());
            }
        } else if (observation.hasValueStringType()) {
            dto.setObservationValue(observation.getValueStringType().getValue());
        }

        return dto;
    }
}
package edu.utah.rehr.garde.domain.core.terminology;

import lombok.extern.java.Log;

import java.util.Objects;

/**
 * @author RickSlc
 */
@Log
public class Code {

    private String bindingCode;
    private final String codeSystem;
    private final String code;
    private final String codeLabel;

    public Code(String codeSystem, String code, String codeLabel) {
        ensureNotNull(codeSystem, code, codeLabel);
        this.codeSystem = codeSystem;
        this.code = code;
        this.codeLabel = codeLabel;
    }

    public Code(String bindingCode, String codeSystem, String code, String codeLabel) {
        ensureNotNull(bindingCode, codeSystem, code, codeLabel);
        this.bindingCode = bindingCode;
        this.codeSystem = codeSystem;
        this.code = code;
        this.codeLabel = codeLabel;
    }

    // other methods remain the same...

    private void ensureNotNull(Object... objects) {
        for (Object obj : objects) {
            Objects.requireNonNull(obj, "Part of the code description is null!");
        }
    }

    public String getCodeSystem() {
        return codeSystem;
    }

    public String getCode() {
        return code;
    }

    public String getCodeLabel() {
        return codeLabel;
    }

    public String getBindingCode() {
        return bindingCode;
    }

    public void setBindingCode(String bindingCode) {
        this.bindingCode = bindingCode;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj instanceof Code comparator) {
            return codeSystem.equals(comparator.getCodeSystem())
                    && code.equals(comparator.getCode());
        }

        return false;
    }

    @Override
    public int hashCode() {
        return codeSystem.hashCode() * 7 + code.hashCode() * 13;
    }

    @Override
    public String toString() {
        return "Code : {" +
                "bindingCode='" + bindingCode + '\'' +
                ", codeSystem='" + codeSystem + '\'' +
                ", code='" + code + '\'' +
                ", codeLabel='" + codeLabel + '\'' +
                "}";
    }
}

package edu.utah.rehr.garde.domain.core.model;

public class PatientDataElement {

    private String patientId;
    private String patientDateOfBirth;
    private String patientSex;
    private String dataElementId;
    private String dataElementName;
    private String dataElementValue;
    private String dataElementDataType;

    public PatientDataElement(){}

    public String getPatientId() {
        return patientId;
    }

    public String getPatientDateOfBirth() {
        return patientDateOfBirth;
    }

    public void setPatientDateOfBirth(String patientDateOfBirth) {
        this.patientDateOfBirth = patientDateOfBirth;
    }

    public String getPatientSex() {
        return patientSex;
    }

    public void setPatientSex(String patientSex) {
        this.patientSex = patientSex;
    }

    public String getDataElementValue() {
        return dataElementValue;
    }

    public void setDataElementValue(String dataElementValue) {
        this.dataElementValue = dataElementValue;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getDataElementId() {
        return dataElementId;
    }

    public void setDataElementId(String dataElementId) {
        this.dataElementId = dataElementId;
    }

    public String getDataElementName() {
        return dataElementName;
    }

    public void setDataElementName(String dataElementName) {
        this.dataElementName = dataElementName;
    }

    public String getDataElementDataType() {
        return dataElementDataType;
    }

    public void setDataElementDataType(String dataElementDataType) {
        this.dataElementDataType = dataElementDataType;
    }
}

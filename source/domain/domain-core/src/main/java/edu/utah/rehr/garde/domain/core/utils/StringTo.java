package edu.utah.rehr.garde.domain.core.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.logging.Logger;

public class StringTo {

    static Logger log = Logger.getLogger(StringTo.class.getName());

    public static Integer aInteger(String str) {

        try {
            return Integer.parseInt(str);
        } catch (Exception e) {
            return null;
        }
    }

    public static Long aLong(String str) {
        return Long.parseLong(str);
    }

    public static Date aDate(String str, String format) {

        try {
            return new SimpleDateFormat(format).parse(str);
        } catch (ParseException e) {
            StringTo.log.warning("DATE FORMAT ERROR: " + str + ":" + format);
        }
        return null;
    }

    public static LocalDateTime aLocalDateTime(String str, String format) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        LocalDateTime localDateTime = LocalDateTime.parse(str, formatter);

        return localDateTime;
    }

    public static LocalDate aLocalDate(String str, String format) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        LocalDate localDate = LocalDate.parse(str, formatter);

        return localDate;
    }


}

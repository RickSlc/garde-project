package edu.utah.rehr.garde.domain.core.model.fhir.r4;

import edu.utah.rehr.garde.domain.core.model.PatientFact;
import edu.utah.rehr.garde.domain.core.model.dto.PatientDTO;
import edu.utah.rehr.garde.domain.core.terminology.Code;
import edu.utah.rehr.garde.domain.core.terminology.binding.NccnTerminologyBinding;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import org.apache.commons.lang3.StringUtils;
import org.hl7.fhir.r4.model.*;

import java.security.InvalidParameterException;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class PatientHelperR4 {

    private final ICodeService codeService;

    public PatientHelperR4(ICodeService codeService) {
        this.codeService = codeService;
    }

    /**
     * Calculates the current age of the patient from the birth date.
     *
     * @param patient The FHIR Patient resource.
     * @return The current age of the patient, or null if birth date is not present.
     */
    public Integer calculateCurrentAge(Patient patient) {
        if (patient == null || !patient.hasBirthDate()) {
            return null;
        }
        LocalDate birthdate = convertToLocalDate(patient.getBirthDateElement());
        return calculateAge(birthdate, LocalDate.now());
    }

    /**
     * Calculates the age of the patient on a given date.
     *
     * @param patient The FHIR Patient resource.
     * @param date    The date on which to calculate the age.
     * @return The age of the patient on the given date, or null if birth date is not present.
     */
    public Integer calculateAgeOnDate(Patient patient, LocalDate date) {
        if (patient == null || !patient.hasBirthDate()) {
            return null;
        }
        LocalDate birthdate = convertToLocalDate(patient.getBirthDateElement());
        return calculateAge(birthdate, date);
    }

    /**
     * Helper method to calculate age.
     *
     * @param birthdate The patient's birth date.
     * @param date      The date to calculate the age on.
     * @return The calculated age.
     */
    private Integer calculateAge(LocalDate birthdate, LocalDate date) {
        if (birthdate == null || date == null) {
            return null;
        }
        return Period.between(birthdate, date).getYears();
    }

    /**
     * Converts a FHIR DateType to LocalDate.
     *
     * @param dateType The FHIR DateType object.
     * @return The corresponding LocalDate object.
     */
    private LocalDate convertToLocalDate(DateType dateType) {
        if (dateType == null) {
            return null;
        }
        return LocalDate.of(dateType.getYear(), dateType.getMonth() + 1, dateType.getDay());
    }

    public Set<Code> getReligionCodeSet(Patient patient) {

        if (patient.getExtension() == null || patient == null) {
            return null;
        }

        Set<Code> codeSet = new HashSet<>();

        String patientExtensionReligionUri = codeService.getUri(NccnTerminologyBinding.HL7_RELIGION_EXTENSION_URI);

        for (Extension ext : patient.getExtension()) {

            if (patientExtensionReligionUri.equals(ext.getUrl())) {
                CodeableConcept religion = (CodeableConcept) ext.getValue();

                for (Coding coding : religion.getCoding()) {

                    Code religionCode = new Code(coding.getSystem(), coding.getCode(), coding.getDisplay());
                    codeSet.add(religionCode);
                }
            }
        }

        return codeSet;
    }

    public Code getGenderCode(Patient patient) {
        Enumerations.AdministrativeGender gender = patient.getGender();
        return new Code(gender.getSystem(), gender.toCode(), gender.getDisplay());
    }

    public String getEhrPatientId(Patient patient) {
        String ehrPatIdCodeSystem = codeService.getCodeSystem(NccnTerminologyBinding.EHR_PAT_ID_CODE_SYSTEM);
        return getPatientId(ehrPatIdCodeSystem, patient);
    }

    public String getPatientIdCodeSystem(Patient patient, String patientId) {

        if (patient == null) {
            throw new InvalidParameterException("NULL Patient. Cannot retrieve patientIdCodeSystem.");
        }
        if (patientId == null) {
            throw new InvalidParameterException("NULL patientId. Cannot retrieve patientIdCodeSystem.");
        }

        for (Identifier id : patient.getIdentifier()) {
            if (id.getValue().equals(patientId)) {
                return id.getSystem();
            }
        }

        return "NO_PATIENT_CODE_SYSTEM:" + patientId;
    }

    public String getPatientId(String codeSystem, Patient patient) {

        for (Identifier id : patient.getIdentifier()) {

            if (codeSystem.equals(id.getSystem())) {
                return id.getValue();
            }
        }
        return null;
    }

    public Patient toPatient(
            String patientId
            , String mrn
            , Date dob
            , String genderCode
            , Code religionCode
    ) {

        Patient patient = new Patient();

        patient.setId(patientId);

        String ehrCodeSystem = codeService.getCodeSystem(NccnTerminologyBinding.EHR_PAT_ID_CODE_SYSTEM);

        patient.addIdentifier()
                .setSystem(ehrCodeSystem)
                .setValue(patientId);

        String localMrnCodeSystem = codeService.getCodeSystem(NccnTerminologyBinding.EHR_MRN_CODE_SYSTEM);

        patient.addIdentifier()
                .setSystem(localMrnCodeSystem)
                .setValue(mrn);

        HumanName patientName = patient.addName()
                .setFamily("testLastName");

        patientName.addGiven("testFirstName");

        patientName.addGiven("testMiddleName");

        patient.setBirthDate(dob);

        if ("male".equals(genderCode)) {
            patient.setGender(Enumerations.AdministrativeGender.MALE);
        } else if ("female".equals(genderCode)) {
            patient.setGender(Enumerations.AdministrativeGender.FEMALE);
        } else if ("other".equals(genderCode)) {
            patient.setGender(Enumerations.AdministrativeGender.OTHER);
        } else if ("unknown".equals(genderCode)) {
            patient.setGender(Enumerations.AdministrativeGender.UNKNOWN);
        } else {
            patient.setGender(Enumerations.AdministrativeGender.NULL);
        }

        CodeableConcept religionConcept = new CodeableConcept();
        religionConcept.addCoding()
                .setCode(religionCode.getCode())
                .setSystem(religionCode.getCodeSystem())
                .setDisplay(religionCode.getCodeLabel())
        ;

        String religionExtensionUri = codeService.getUri(NccnTerminologyBinding.HL7_RELIGION_EXTENSION_URI);

        Extension extension = new Extension();
        extension.setUrl(religionExtensionUri);
        extension.setValue(religionConcept);

        patient.getExtension().add(extension);

        return patient;
    }

    public Patient toPatient(PatientDTO pt) {

        Patient patient = new Patient();

        patient.setId(pt.getPatientId());

        patient.addIdentifier()
                .setSystem(pt.getPatientIdCodeSystem())
                .setValue(pt.getPatientId());

        if (pt.getBirthDate() != null) {
            Date dob = Date.from(pt.getBirthDate().atStartOfDay(ZoneId.systemDefault()).toInstant());
            patient.setBirthDate(dob);
        }

        Enumerations.AdministrativeGender gender = Enumerations.AdministrativeGender.NULL;

//        if (pt.getSexLabel() == null) {
//            gender = Enumerations.AdministrativeGender.NULL;
//        }

        if ("male".equalsIgnoreCase(pt.getSexLabel())) {
            gender = Enumerations.AdministrativeGender.MALE;
        } else if ("female".equalsIgnoreCase(pt.getSexLabel())) {
            gender = Enumerations.AdministrativeGender.FEMALE;
        } else if ("other".equalsIgnoreCase(pt.getSexLabel())) {
            gender = Enumerations.AdministrativeGender.OTHER;
        } else {
            gender = Enumerations.AdministrativeGender.UNKNOWN;
        }

        patient.setGender(gender);

        CodeableConcept religionConcept = new CodeableConcept();
        boolean hasReligionCode = false;

        if (StringUtils.isNotEmpty(pt.getReligionCode())) {

            religionConcept.addCoding()
                    .setCode(pt.getReligionCode())
                    .setSystem(pt.getReligionCodeSystem())
                    .setDisplay(pt.getReligionLabel())
            ;
            hasReligionCode = true;
        }

        if (hasReligionCode) {
            Extension extension = new Extension();
            extension.setUrl(codeService.getUri(NccnTerminologyBinding.HL7_RELIGION_EXTENSION_URI));
            extension.setValue(religionConcept);

            patient.getExtension().add(extension);
        }

        return patient;
    }

    public Patient toPatient(PatientFact pt) {

        Patient patient = new Patient();

        patient.setId(pt.getEhrPatientId());

        patient.addIdentifier()
                .setSystem(pt.getEhrCodeSystem())
                .setValue(pt.getEhrPatientId());

        Date dob = Date.from(pt.getDob().atStartOfDay(ZoneId.systemDefault()).toInstant());
        patient.setBirthDate(dob);

        Enumerations.AdministrativeGender gender = Enumerations.AdministrativeGender.fromCode(pt.getCanonicalSexCode());

        if (gender != null) {
            patient.setGender(gender);
        } else {
            patient.setGender(Enumerations.AdministrativeGender.NULL);
        }

        CodeableConcept religionConcept = new CodeableConcept();
        Boolean hasReligionCode = false;

        if (StringUtils.isNotEmpty(pt.getEhrReligionCode())) {

            religionConcept.addCoding()
                    .setCode(pt.getEhrReligionCode())
                    .setSystem(pt.getEhrReligionCodeSystem())
                    .setDisplay(pt.getEhrReligionLabel())
            ;
            hasReligionCode = true;
        }

        if (StringUtils.isNotEmpty(pt.getCanonicalReligionCode())) {

            religionConcept.addCoding()
                    .setCode(pt.getCanonicalReligionCode())
                    .setSystem(pt.getCanonicalReligionCodeSystem())
                    .setDisplay(pt.getCanonicalReligionLabel())
            ;
            hasReligionCode = true;
        }

        if (hasReligionCode) {
            Extension extension = new Extension();
            extension.setUrl(codeService.getUri(NccnTerminologyBinding.HL7_RELIGION_EXTENSION_URI));
            extension.setValue(religionConcept);

            patient.getExtension().add(extension);
        }

        return patient;
    }

}

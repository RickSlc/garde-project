package edu.utah.rehr.garde.domain.core.provider;

import edu.utah.rehr.garde.domain.core.accessory.UResult;

import java.io.File;
import java.util.List;

public interface IRemoteFileProvider {

    UResult<List<String>> lsRemoteDirectory(String remoteDirectorySearchPattern);
    UResult<Boolean> remoteFileExists(String remoteFilePath);
    UResult<Boolean> deleteRemoteFiles(String localFilePath);
    UResult<Boolean> getRemoteFile(String remoteFilePath, String localFilePath);
    UResult<Boolean> putOnRemoteFileSystem(String localFilePath, String remoteFilePath);
    UResult<Boolean> moveRemoteFile(String remoteSourceFilePath, String remoteDestinationFilePath);

    default UResult<Boolean> localFileExists(String localFilePath) {
        try {
            File f = new File(localFilePath);
            if (f.exists()) {
                return UResult.successResult(true);
            }

            return UResult.successResult(false);

        } catch(Exception e){
            return UResult.errorResult(false, e);
        }
    }

    default UResult<Boolean> deleteLocalFile(String localFilePath) {
        try {
            File f = new File(localFilePath);
            if (f.delete()) {
                return UResult.successResult(true);
            }

            return UResult.successResult(false);

        } catch(Exception e){
            return UResult.errorResult(false, e);
        }

    }

}

package edu.utah.rehr.garde.domain.core.model;

import lombok.Getter;

@Getter
public enum GFhirResource {

    /**
     * Defines garde fhir resources and their queries
     */
    NccnPatient("patient","Patient","Patient/{{context.patientId}}", GCardinality.One)
    , Patient("patient","Patient","Patient/{{context.patientId}}", GCardinality.One)
    , FamilyMemberHistory("patientFHxBundle","FamilyMemberHistory","FamilyMemberHistory?subject={{context.patientId}}", GCardinality.ZeroOrMore)
    , Observation_Ldl_C("patientObs_Ldl_C_Bundle", "Observation","Observation?subject=Patient/{{context.patientId}}&amp;code=LDL_C", GCardinality.ZeroOrMore)
    ;

    private final String prefetchName;
    private final String fhirResource;
    private final String fhirQuery;
    private final GCardinality cardinality;

    GFhirResource(String prefetchName,String fhirResource, String fhirQuery, GCardinality cardinality) {
        this.prefetchName = prefetchName;
        this.fhirResource = fhirResource;
        this.fhirQuery = fhirQuery;
        this.cardinality = cardinality;
    }

}

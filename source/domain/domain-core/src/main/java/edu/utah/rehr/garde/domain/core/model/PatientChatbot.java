package edu.utah.rehr.garde.domain.core.model;

import java.util.Date;

public class PatientChatbot {

    private Long chatTranscriptId;
    private String ehrPatientId;
    private String chatScriptId;
    private String chatServiceUrl;
    private Date urlCreateDate;
    private Date chatCompleteDate;
    private Date chatReceivedDate;
    private Date chatLoadDate;
    private Integer transcriptId;
    private String transcriptText;
    private byte[] transcriptPdf;
    private String fileName;

    public void setChatTranscriptId(Long chatTranscriptId) {
        this.chatTranscriptId = chatTranscriptId;
    }

    public Long getChatTranscriptId() {
        return chatTranscriptId;
    }

    public String getEhrPatientId() {
        return ehrPatientId;
    }

    public void setEhrPatientId(String ehrPatientId) {
        this.ehrPatientId = ehrPatientId;
    }

    public String getChatScriptId() {
        return chatScriptId;
    }

    public void setChatScriptId(String chatScriptId) {
        this.chatScriptId = chatScriptId;
    }

    public String getChatServiceUrl() {
        return chatServiceUrl;
    }

    public void setChatServiceUrl(String chatServiceUrl) {
        this.chatServiceUrl = chatServiceUrl;
    }

    public Date getUrlCreateDate() {
        return urlCreateDate;
    }

    public void setUrlCreateDate(Date urlCreateDate) {
        this.urlCreateDate = urlCreateDate;
    }

    public Date getChatCompleteDate() {
        return chatCompleteDate;
    }

    public void setChatCompleteDate(Date chatCompleteDate) {
        this.chatCompleteDate = chatCompleteDate;
    }

    public Date getChatReceivedDate() {
        return chatReceivedDate;
    }

    public void setChatReceivedDate(Date chatReceivedDate) {
        this.chatReceivedDate = chatReceivedDate;
    }

    public Date getChatLoadDate() {
        return chatLoadDate;
    }

    public void setChatLoadDate(Date chatLoadDate) {
        this.chatLoadDate = chatLoadDate;
    }

    public String getTranscriptText() {
        return transcriptText;
    }

    public void setTranscriptText(String transcriptText) {
        this.transcriptText = transcriptText;
    }

    public byte[] getTranscriptPdf() {
        return transcriptPdf;
    }

    public void setTranscriptPdf(byte[] transcriptPdf) {
        this.transcriptPdf = transcriptPdf;
    }

    public void setTranscriptId(Integer transcriptId) {
        this.transcriptId = transcriptId;
    }

    public Integer getTranscriptId() {
        return transcriptId;
    }

    public void setFileName(String fileName) { this.fileName = fileName; }

    public String getFileName() { return fileName; }
}



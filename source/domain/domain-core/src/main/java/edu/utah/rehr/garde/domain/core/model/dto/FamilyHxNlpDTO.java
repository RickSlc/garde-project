package edu.utah.rehr.garde.domain.core.model.dto;

public class FamilyHxNlpDTO {

    private FamilyHxDTO familyHxDTO;
    private String nlpConditionCodeSystem;
    private String nlpConditionCode;
    private String nlpConditionLabel;
    private String nlpConditionOnsetType;
    private Integer nlpConditionOnsetAge;
    private String nlpConditionOnsetAgeRange;

    private String nlpRelationshipCodeSystem;
    private String nlpRelationshipCode;
    private String nlpRelationshipLabel;

    public void setFamilyHxDTO(FamilyHxDTO familyHxDTO) {
        this.familyHxDTO = familyHxDTO;
    }

    public FamilyHxDTO getFamilyHxDTO() {
        return familyHxDTO;
    }

    public String getNlpConditionCodeSystem() {
        return nlpConditionCodeSystem;
    }

    public void setNlpConditionCodeSystem(String nlpConditionCodeSystem) {
        this.nlpConditionCodeSystem = nlpConditionCodeSystem;
    }

    public String getNlpConditionCode() {
        return nlpConditionCode;
    }

    public void setNlpConditionCode(String nlpConditionCode) {
        this.nlpConditionCode = nlpConditionCode;
    }

    public String getNlpConditionLabel() {
        return nlpConditionLabel;
    }

    public void setNlpConditionLabel(String nlpConditionLabel) {
        this.nlpConditionLabel = nlpConditionLabel;
    }

    public Integer getNlpConditionOnsetAge() {
        return nlpConditionOnsetAge;
    }

    public void setNlpConditionOnsetAge(Integer nlpConditionOnsetAge) {
        this.nlpConditionOnsetAge = nlpConditionOnsetAge;
    }

    public String getNlpConditionOnsetAgeRange() {
        return nlpConditionOnsetAgeRange;
    }

    public void setNlpConditionOnsetAgeRange(String nlpConditionOnsetAgeRange) {
        this.nlpConditionOnsetAgeRange = nlpConditionOnsetAgeRange;
    }

    public String getNlpRelationshipCodeSystem() {
        return nlpRelationshipCodeSystem;
    }

    public void setNlpConditionOnsetType(String nlpConditionOnsetType) {
        this.nlpConditionOnsetType = nlpConditionOnsetType;
    }

    public String getNlpConditionOnsetType() {
        return nlpConditionOnsetType;
    }

    public void setNlpRelationshipCodeSystem(String nlpRelationshipCodeSystem) {
        this.nlpRelationshipCodeSystem = nlpRelationshipCodeSystem;
    }

    public String getNlpRelationshipCode() {
        return nlpRelationshipCode;
    }

    public void setNlpRelationshipCode(String nlpRelationshipCode) {
        this.nlpRelationshipCode = nlpRelationshipCode;
    }

    public String getNlpRelationshipLabel() {
        return nlpRelationshipLabel;
    }

    public void setNlpRelationshipLabel(String nlpRelationshipLabel) {
        this.nlpRelationshipLabel = nlpRelationshipLabel;
    }

    @Override
    public boolean equals(Object o) {
        boolean parentIsEqual = super.equals(o);
        return o instanceof FamilyHxNlpDTO && parentIsEqual;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }


}


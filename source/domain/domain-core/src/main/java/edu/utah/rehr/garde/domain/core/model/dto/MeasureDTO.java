package edu.utah.rehr.garde.domain.core.model.dto;

import java.util.Properties;

public class MeasureDTO {

    public String measureLabel;
    public String measureValue;
    public Properties properties;

}

package edu.utah.rehr.garde.domain.core.model;

import java.util.Date;
@Deprecated
public class MetNccnCriteriaFact extends MetCriteriaFactKey {

    private String metCriteriaId;
    private Date ehrEncounterDate;
    private String recordSourceCd;
    private Date recordDate;
    private String ehrRelativeCodeSystem;
    private String ehrRelativeCode;
    private String ehrRelativeLabel;
    private String canonicalRelativeCodeSystem;
    private String canonicalRelativeCode;
    private String canonicalRelativeLabel;
    private String ehrFamilyHxCodeSystem;
    private String ehrFamilyHxCode;
    private String ehrFamilyHxCodeLabel;
    private String canonicalFamilyHxCodeSystem;
    private String canonicalFamilyHxCode;
    private String canonicalFamilyHxCodeLabel;
    private Integer ageOfOnset;
    private String familyHxComment;
    private Float confidence;

  public MetNccnCriteriaFact(MetCriteriaFactKey key) {
    super(key.getEhrPatientId(), key.getEhrEncounterId(), key.getEhrRecordId(), key.getCohortCd(), key.getCriteriaId());
  }

  public String getMetCriteriaId() {
    return metCriteriaId;
  }

  public void setMetCriteriaId(String metCriteriaId) {
    this.metCriteriaId = metCriteriaId;
  }

  public Date getEhrEncounterDate() {
    return ehrEncounterDate;
  }

  public void setEhrEncounterDate(Date ehrEncounterDate) {
    this.ehrEncounterDate = ehrEncounterDate;
  }

  public String getRecordSourceCd() {
    return recordSourceCd;
  }

  public void setRecordSourceCd(String recordSourceCd) {
    this.recordSourceCd = recordSourceCd;
  }

  public Date getRecordDate() {
    return recordDate;
  }

  public void setRecordDate(Date recordDate) {
    this.recordDate = recordDate;
  }

  public String getEhrRelativeCodeSystem() {
    return ehrRelativeCodeSystem;
  }

  public void setEhrRelativeCodeSystem(String ehrRelativeCodeSystem) {
    this.ehrRelativeCodeSystem = ehrRelativeCodeSystem;
  }

  public String getEhrRelativeCode() {
    return ehrRelativeCode;
  }

  public void setEhrRelativeCode(String ehrRelativeCode) {
    this.ehrRelativeCode = ehrRelativeCode;
  }

  public String getEhrRelativeLabel() {
    return ehrRelativeLabel;
  }

  public void setEhrRelativeLabel(String ehrRelativeLabel) {
    this.ehrRelativeLabel = ehrRelativeLabel;
  }

  public String getCanonicalRelativeCodeSystem() {
    return canonicalRelativeCodeSystem;
  }

  public void setCanonicalRelativeCodeSystem(String canonicalRelativeCodeSystem) {
    this.canonicalRelativeCodeSystem = canonicalRelativeCodeSystem;
  }

  public String getCanonicalRelativeCode() {
    return canonicalRelativeCode;
  }

  public void setCanonicalRelativeCode(String canonicalRelativeCode) {
    this.canonicalRelativeCode = canonicalRelativeCode;
  }

  public String getCanonicalRelativeLabel() {
    return canonicalRelativeLabel;
  }

  public void setCanonicalRelativeLabel(String canonicalRelativeLabel) {
    this.canonicalRelativeLabel = canonicalRelativeLabel;
  }

  public String getEhrFamilyHxCodeSystem() {
    return ehrFamilyHxCodeSystem;
  }

  public void setEhrFamilyHxCodeSystem(String ehrFamilyHxCodeSystem) {
    this.ehrFamilyHxCodeSystem = ehrFamilyHxCodeSystem;
  }

  public String getEhrFamilyHxCode() {
    return ehrFamilyHxCode;
  }

  public void setEhrFamilyHxCode(String ehrFamilyHxCode) {
    this.ehrFamilyHxCode = ehrFamilyHxCode;
  }

  public String getEhrFamilyHxCodeLabel() {
    return ehrFamilyHxCodeLabel;
  }

  public void setEhrFamilyHxCodeLabel(String ehrFamilyHxCodeLabel) {
    this.ehrFamilyHxCodeLabel = ehrFamilyHxCodeLabel;
  }

  public String getCanonicalFamilyHxCodeSystem() {
    return canonicalFamilyHxCodeSystem;
  }

  public void setCanonicalFamilyHxCodeSystem(String canonicalFamilyHxCodeSystem) {
    this.canonicalFamilyHxCodeSystem = canonicalFamilyHxCodeSystem;
  }

  public String getCanonicalFamilyHxCode() {
    return canonicalFamilyHxCode;
  }

  public void setCanonicalFamilyHxCode(String canonicalFamilyHxCode) {
    this.canonicalFamilyHxCode = canonicalFamilyHxCode;
  }

  public String getCanonicalFamilyHxCodeLabel() {
    return canonicalFamilyHxCodeLabel;
  }

  public void setCanonicalFamilyHxCodeLabel(String canonicalFamilyHxCodeLabel) {
    this.canonicalFamilyHxCodeLabel = canonicalFamilyHxCodeLabel;
  }

  public Integer getAgeOfOnset() {
    return ageOfOnset;
  }

  public void setAgeOfOnset(Integer ageOfOnset) {
    this.ageOfOnset = ageOfOnset;
  }

  public String getFamilyHxComment() {
    return familyHxComment;
  }

  public void setFamilyHxComment(String familyHxComment) {
    this.familyHxComment = familyHxComment;
  }

  public Float getConfidence() {
    return confidence;
  }

  public void setConfidence(Float confidence) {
    this.confidence = confidence;
  }

  @Override
  public String toString() {
    return super.toString();
  }

  @Override
  public boolean equals(Object obj) {

    return super.equals(obj);
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }

}

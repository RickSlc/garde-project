package edu.utah.rehr.garde.domain.core.api;

import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.model.GCohort;

import java.util.Map;

public interface IPopulationCoordinator {

    UResult<Boolean> systemTest();

    UResult<Integer> prepareScreeningPopulation(GCohort cohort, Integer version);

    UResult<Integer> extractPatientFacts(GCohort cohort, Integer version);

    UResult<Integer> evaluatePopulation(GCohort sourceCohort, GCohort targetCohort, Integer version);

    UResult<Integer> exportPopulation(GCohort cohort, Integer version, Exporter exporter, Map<String,String> paramMap);

    UResult<Boolean> coordinatePopulationUpdate(
            GCohort evaluationCohort
            , GCohort metCriteriaCohort
            , Integer version
            , Map<String,String> params
    );

    }


package edu.utah.rehr.garde.domain.core.accessory;

public enum UResultStatus {

    SUCCESS,
    WARNING,
    ERROR,
    INCOMPLETE

}

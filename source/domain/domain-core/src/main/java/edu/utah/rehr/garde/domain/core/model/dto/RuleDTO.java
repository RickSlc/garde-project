package edu.utah.rehr.garde.domain.core.model.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class RuleDTO {
    private final Long ruleId;
    private final String ruleCd;
    private final String ruleLabel;
    private final String ruleBadge;

    public RuleDTO(Long ruleId, String ruleCd, String ruleLabel, String ruleBadge) {
        this.ruleId = ruleId;
        this.ruleCd = ruleCd;
        this.ruleLabel = ruleLabel;
        this.ruleBadge = ruleBadge;
    }

}

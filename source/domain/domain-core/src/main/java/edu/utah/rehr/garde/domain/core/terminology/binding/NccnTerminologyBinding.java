package edu.utah.rehr.garde.domain.core.terminology.binding;

import edu.utah.rehr.garde.domain.core.terminology.ITerminologyKey;
import edu.utah.rehr.garde.domain.core.terminology.TerminologyEntityType;

public enum NccnTerminologyBinding implements ITerminologyKey {

    // FamilyMemberHistoryHelper
    REHR_NCCN_CODE_SYSTEM_URI(TerminologyEntityType.URI, "REHR_NCCN_CODE_SYSTEM_URI")
    , REHR_NCCN_FHX_ID_CODE_SYSTEM_URI(TerminologyEntityType.URI, "REHR_NCCN_FHX_ID_CODE_SYSTEM_URI")
    , REHR_NCCN_FHX_SOURCE_EXENSION_URI(TerminologyEntityType.URI, "REHR_NCCN_FHX_SOURCE_EXENSION_URI")

    , NLP_SOURCE_URI(TerminologyEntityType.URI, "NLP_SOURCE_URI")
    , BC_ADD_RISK_CODE_SET(TerminologyEntityType.CODE_SET, "BC_ADD_RISK")
    , BC_GENE_MUTATION_CODE_SET(TerminologyEntityType.CODE_SET, "BC_GENE_MUTATION")
    , BC_JEWISH_CA_RISK_CODE_SET(TerminologyEntityType.CODE_SET, "BC_JEWISH_CA_RISK")
    , CO_ADD_RISK_CODE_SET(TerminologyEntityType.CODE_SET, "CO_ADD_RISK")
    , CO_GENE_MUTATION_CODE_SET(TerminologyEntityType.CODE_SET, "CO_GENE_MUTATION")

    // 2023 versions
    , BC_ADD_RISK_2023_CODE_SET(TerminologyEntityType.CODE_SET, "BC_ADD_RISK_2023")
    , BC_GENE_MUTATION_2023_CODE_SET(TerminologyEntityType.CODE_SET, "BC_GENE_MUTATION_2023")
    , BC_JEWISH_CA_RISK_2023_CODE_SET(TerminologyEntityType.CODE_SET, "BC_JEWISH_CA_RISK_2023")

    , FEMALE_RELATIVE_2023_CODE_SET(TerminologyEntityType.CODE_SET, "FEMALE_RELATIVE_2023")
    , MALE_RELATIVE_2023_CODE_SET(TerminologyEntityType.CODE_SET, "MALE_RELATIVE_2023")

    , FEMALE_CODE(TerminologyEntityType.CODE, "FEMALE")
    , MALE_CODE(TerminologyEntityType.CODE, "MALE")
    , OTHER_CODE(TerminologyEntityType.CODE, "OTHER")
    , UNKNOWN_CODE(TerminologyEntityType.CODE, "UNKNOWN")

    // List of relatives that can only be one person (can't have two biological fathers).
    // Useful for validation.
    , RELATIVE_CARDINALITY_1(TerminologyEntityType.CODE_SET, "RELATIVE_CARDINALITY_1")

    // These FAMILY_SIDE code sets are mutually exclusive and are intended to be added to each other.
    // P = paternal
    // M = maternal
    // Avoiding the "F" is female or father and "M" is male or maternal
    , FAMILY_SIDE_GEN_1(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_1")
    , FAMILY_SIDE_GEN_1P(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_1P")
    , FAMILY_SIDE_GEN_1M(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_1M")

    , FAMILY_SIDE_GEN_2(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_2")
    , FAMILY_SIDE_GEN_2P(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_2P")
    , FAMILY_SIDE_GEN_2PP(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_2PP")
    , FAMILY_SIDE_GEN_2PM(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_2PM")

    , FAMILY_SIDE_GEN_2M(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_2M")
    , FAMILY_SIDE_GEN_2MP(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_2MP")
    , FAMILY_SIDE_GEN_2MM(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_2MM")

    , FAMILY_SIDE_GEN_3P(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_3P")
    , FAMILY_SIDE_GEN_3PP(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_3PP")
    , FAMILY_SIDE_GEN_3PPP(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_3PPP")
    , FAMILY_SIDE_GEN_3PPM(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_3PPM")

    , FAMILY_SIDE_GEN_3PM(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_3PM")
    , FAMILY_SIDE_GEN_3PMP(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_3PMP")
    , FAMILY_SIDE_GEN_3PMM(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_3PMM")

    , FAMILY_SIDE_GEN_3M(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_3M")
    , FAMILY_SIDE_GEN_3MP(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_3MP")
    , FAMILY_SIDE_GEN_3MPP(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_3MPP")
    , FAMILY_SIDE_GEN_3MPM(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_3MPM")

    , FAMILY_SIDE_GEN_3MM(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_3MM")
    , FAMILY_SIDE_GEN_3MMP(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_3MMP")
    , FAMILY_SIDE_GEN_3MMM(TerminologyEntityType.CODE_SET, "FAMILY_SIDE_GEN_3MMM")

    , FIRST_DEGREE_RELATIVE(TerminologyEntityType.CODE_SET, "FIRST_DEGREE_RELATIVE")
    , SECOND_DEGREE_RELATIVE(TerminologyEntityType.CODE_SET, "SECOND_DEGREE_RELATIVE")
    , THIRD_DEGREE_RELATIVE(TerminologyEntityType.CODE_SET, "THIRD_DEGREE_RELATIVE")

    // PatientHelper
    // EHR_PAT_ID_CODE_SYSTEM  (TerminologyEntityType.URI, "EHR_PAT_ID_CODE_SYSTEM")
    , EHR_PAT_ID_CODE_SYSTEM     (TerminologyEntityType.URI, "EHR_PAT_ID_CODE_SYSTEM")
    , MOTHER_CODE(TerminologyEntityType.CODE, "MOTHER")
    , FATHER_CODE(TerminologyEntityType.CODE, "FATHER")

    , EHR_MRN_CODE_SYSTEM (TerminologyEntityType.URI, "EHR_MRN_CODE_SYSTEM")

    , RELIGION_CODE_SET          (TerminologyEntityType.CODE_SET, "RELIGION")
    , HL7_RELIGION_EXTENSION_URI (TerminologyEntityType.URI, "HL7_RELIGION_EXTENSION_URI")

    // EarlyOnsetBreastCancerRiskDetectionRules
    , BREAST_CANCER_CODE     (TerminologyEntityType.CODE, "CANCER_BREAST")
    , OVARIAN_CANCER_CODE    (TerminologyEntityType.CODE, "CANCER_OVARIAN")
    , PANCREATIC_CANCER_CODE (TerminologyEntityType.CODE, "CANCER_PANCREAS")
    , PROSTATE_CANCER_CODE   (TerminologyEntityType.CODE, "CANCER_PROSTATE")
    , JEWISH_CODE            (TerminologyEntityType.CODE, "JEWISH")

    , FEMALE_DX_CODE_SET     (TerminologyEntityType.CODE_SET, "FEMALE_DX")
    , MALE_DX_CODE_SET       (TerminologyEntityType.CODE_SET, "MALE_DX")

    , FEMALE_RELATIVE_CODE_SET       (TerminologyEntityType.CODE_SET, "FEMALE_RELATIVE")
    , MALE_RELATIVE_CODE_SET       (TerminologyEntityType.CODE_SET, "MALE_RELATIVE")
    , BC_GENETIC_MUTATION_CODE_SET (TerminologyEntityType.CODE_SET, "BC_GENE_MUTATION")

    // EarlyOnsetColorectalCancerRiskDetectionRules
    , COLON_CANCER_CODE(TerminologyEntityType.CODE,"CANCER_COLON")
    , ENDOMETRIAL_CANCER_CODE(TerminologyEntityType.CODE,"CANCER_ENDOMETRIAL")
    , UTERINE_CANCER_CODE(TerminologyEntityType.CODE,"CANCER_UTERINE")

    , COLORECTAL_CANCER_MUTATION_RISK_CODE_SET(TerminologyEntityType.CODE_SET,"CO_GENE_MUTATION")
    , COLORECTAL_CANCER_DX_RISK_CODE_SET(TerminologyEntityType.CODE_SET,"CO_ADD_RISK")

    , EHR_FHX_DX_CODE_SYSTEM     (TerminologyEntityType.URI, "EHR_FHX_DX_CODE_SYSTEM")
    , NCCN_GENETIC_ASSESSMENT_GUIDELINE_URI(TerminologyEntityType.URI, "NCCN_GENETIC_ASSESSMENT_GUIDELINE_URI")
    , NCCN_GENETIC_ASSESSMENT_GUIDELINE_2023_URI(TerminologyEntityType.URI, "NCCN_GENETIC_ASSESSMENT_GUIDELINE_2023_URI")

    // TESTS
    , CANCER_KIDNEY_CODE(TerminologyEntityType.CODE, "CANCER_KIDNEY")
    , CANCER_BLADDER_CODE(TerminologyEntityType.CODE, "CANCER_BLADDER")
    , CANCER_STOMACH_CODE(TerminologyEntityType.CODE, "CANCER_STOMACH")
    , BRCA_1_GENE_MUTATION_CODE(TerminologyEntityType.CODE, "BRCA_1_GENE_MUTATION")
    , CATHOLIC_CODE(TerminologyEntityType.CODE, "ROMAN_CATHOLIC_CHURCH")

    , PATERNAL_HALF_SIBLING_CODE(TerminologyEntityType.CODE, "PATERNAL_HALF_SIBLING")
    , MATERNAL_HALF_SIBLING_CODE(TerminologyEntityType.CODE, "MATERNAL_HALF_SIBLING")
    , HALF_SIBLING_CODE(TerminologyEntityType.CODE, "HALF_SIBLING")
    , SISTER_CODE(TerminologyEntityType.CODE, "SISTER")
    , BROTHER_CODE(TerminologyEntityType.CODE, "BROTHER")
    , DAUGHTER_CODE(TerminologyEntityType.CODE, "DAUGHTER")
    , MATERNAL_GRANDMOTHER_CODE(TerminologyEntityType.CODE, "MATERNAL_GRANDMOTHER")
    , MATERNAL_GRANDFATHER_CODE(TerminologyEntityType.CODE, "MATERNAL_GRANDFATHER")
    , PATERNAL_GRANDMOTHER_CODE(TerminologyEntityType.CODE, "PATERNAL_GRANDMOTHER")
    , PATERNAL_GRANDFATHER_CODE(TerminologyEntityType.CODE, "PATERNAL_GRANDFATHER")
    , AUNT_CODE(TerminologyEntityType.CODE,"AUNT")
    , MATERNAL_AUNT_CODE(TerminologyEntityType.CODE, "MATERNAL_AUNT")
    , PATERNAL_AUNT_CODE(TerminologyEntityType.CODE, "PATERNAL_AUNT")
    , MATERNAL_UNCLE_CODE(TerminologyEntityType.CODE, "MATERNAL_UNCLE")
    , PATERNAL_UNCLE_CODE(TerminologyEntityType.CODE, "PATERNAL_UNCLE")
    , CANCER_BRAIN_CODE(TerminologyEntityType.CODE, "CANCER_BRAIN")
    , LYNCH_SYNDROME_CODE(TerminologyEntityType.CODE, "LYNCH_SYNDROME")

    // 3rd deg relatives
    , PATERNAL_COUSIN_CODE(TerminologyEntityType.CODE, "PATERNAL_COUSIN")
    , MATERNAL_COUSIN_CODE(TerminologyEntityType.CODE, "MATERNAL_COUSIN")

    , FATHERS_FATHERS_FATHER_CODE(TerminologyEntityType.CODE, "FATHERS_FATHERS_FATHER")
    , FATHERS_FATHERS_MOTHER_CODE(TerminologyEntityType.CODE, "FATHERS_FATHERS_MOTHER")
    , FATHERS_MOTHERS_FATHER_CODE(TerminologyEntityType.CODE, "FATHERS_MOTHERS_FATHER")
    , FATHERS_MOTHERS_MOTHER_CODE(TerminologyEntityType.CODE, "FATHERS_MOTHERS_MOTHER")
    , FATHERS_FATHERS_BROTHER_CODE(TerminologyEntityType.CODE, "FATHERS_FATHERS_BROTHER")
    , FATHERS_FATHERS_SISTER_CODE(TerminologyEntityType.CODE, "FATHERS_FATHERS_SISTER")
    , FATHERS_MOTHERS_BROTHER_CODE(TerminologyEntityType.CODE, "FATHERS_MOTHERS_BROTHER")
    , FATHERS_MOTHERS_SISTER_CODE(TerminologyEntityType.CODE, "FATHERS_MOTHERS_SISTER")

    , MOTHERS_FATHERS_FATHER_CODE(TerminologyEntityType.CODE, "MOTHERS_FATHERS_FATHER")
    , MOTHERS_FATHERS_MOTHER_CODE(TerminologyEntityType.CODE, "MOTHERS_FATHERS_MOTHER")
    , MOTHERS_MOTHERS_FATHER_CODE(TerminologyEntityType.CODE, "MOTHERS_MOTHERS_FATHER")
    , MOTHERS_MOTHERS_MOTHER_CODE(TerminologyEntityType.CODE, "MOTHERS_MOTHERS_MOTHER")
    , MOTHERS_FATHERS_BROTHER_CODE(TerminologyEntityType.CODE, "MOTHERS_FATHERS_BROTHER")
    , MOTHERS_FATHERS_SISTER_CODE(TerminologyEntityType.CODE, "MOTHERS_FATHERS_SISTER")
    , MOTHERS_MOTHERS_BROTHER_CODE(TerminologyEntityType.CODE, "MOTHERS_MOTHERS_BROTHER")
    , MOTHERS_MOTHERS_SISTER_CODE(TerminologyEntityType.CODE, "MOTHERS_MOTHERS_SISTER")

    ;

    final TerminologyEntityType terminologyEntityType;
    final String key;

    NccnTerminologyBinding(TerminologyEntityType type, String key) {
        this.terminologyEntityType = type;
        this.key = key;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public TerminologyEntityType getEntityType() {
        return terminologyEntityType;
    }
}

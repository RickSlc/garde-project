package edu.utah.rehr.garde.domain.core.model.fhir.r4;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxDTO;
import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxNlpDTO;
import edu.utah.rehr.garde.domain.core.model.fhir.FuzzyAgeRangeQualifier;
import edu.utah.rehr.garde.domain.core.model.fhir.NlpOnsetType;
import edu.utah.rehr.garde.domain.core.terminology.Code;
import edu.utah.rehr.garde.domain.core.terminology.FamilySideTerminologyService;
import edu.utah.rehr.garde.domain.core.terminology.binding.NccnTerminologyBinding;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import edu.utah.rehr.garde.domain.core.utils.DateUtil;
import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.r4.model.*;
import org.hl7.fhir.r4.model.FamilyMemberHistory.FamilyMemberHistoryConditionComponent;

import java.time.ZoneId;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author RickSlc
 */
@Log
public class FamilyMemberHistoryHelperR4 {

    private final ICodeService codeService;
    private final FuzzyAgeRangeInterpreterR4 fuzzyAgeRangeInterpreter;
    private final FamilySideTerminologyService familySideTerminologyService;
    public FamilyMemberHistoryHelperR4(ICodeService codeService) {

        this.codeService = codeService;
        this.fuzzyAgeRangeInterpreter = new FuzzyAgeRangeInterpreterR4();
        this.familySideTerminologyService = new FamilySideTerminologyService(codeService);

    }

    public boolean isNonsensicalDiagnosisAndSex(FamilyMemberHistory fhx) {

        String fhxRecordId = getEhrFhxKey(fhx);
        Set<Code> relativeCodes = getRelativeCodings(fhx);
        boolean isMaleRelative = codeService.intersects(NccnTerminologyBinding.MALE_RELATIVE_2023_CODE_SET, relativeCodes);
        boolean isFemaleRelative = codeService.intersects(NccnTerminologyBinding.FEMALE_RELATIVE_2023_CODE_SET, relativeCodes);

        // if the relative is not mapped to a domain, both will be false -> assume it's ok.
        if (!(isMaleRelative || isFemaleRelative)) {
            if (!relativeCodes.isEmpty()) {
                log.warning("CANNOT VALIDATE SEX AND DIAGNOSIS ARE SENSIBLE - RELATIVE SEX WAS NOT DISCOVERABLE FOR FHX " + fhxRecordId + " WITH RELATIVE " + relativeCodes.iterator().next());
            } else {
                log.warning("CANNOT VALIDATE SEX AND DIAGNOSIS ARE SENSIBLE - RELATIVE SEX WAS NOT DISCOVERABLE FOR " + fhxRecordId);
            }
            return false;
        }

        Set<Code> dxCodes = getConditionCodings(fhx);

        // Validate FHx Dx sex and diagnosis are sensible - no males with ovarian cancer, no females with prostate cancer
        if (isFemaleRelative == Boolean.TRUE && codeService.intersects(NccnTerminologyBinding.MALE_DX_CODE_SET, dxCodes)) {
            log.warning("IGNORING - FEMALE HAD MALE DX " + fhxRecordId);
            return true;
        }

        if (isMaleRelative == Boolean.TRUE && codeService.intersects(NccnTerminologyBinding.FEMALE_DX_CODE_SET, dxCodes)) {
            log.warning("IGNORING - MALE HAD FEMALE DX " + fhxRecordId);
            return true;
        }

        return false;
    }

    public String getFamilyHxId(FamilyMemberHistory fhx) {

        List<Identifier> identifiers = fhx.getIdentifier();
        return identifiers.get(0).getValue();
    }

    public Set<Code> getRelativeCodings(FamilyMemberHistory fhx) {

        if (isNullRelationship(fhx)) {
            log.severe("NO RELATIONSHIP " + toJson(fhx));
            return new HashSet<>();
        }
        return toCodeSet(getFamilyHxId(fhx), fhx.getRelationship().getCoding());
    }

    private Set<Code> toCodeSet(String fhxId, List<Coding> codings) {

        Set<Code> codes = new HashSet<>();
        for (Coding coding : codings) {

            if (StringUtils.isNotEmpty(coding.getSystem())
                    && StringUtils.isNotEmpty(coding.getCode())
                    && StringUtils.isNotEmpty(coding.getDisplay())) {

                codes.add(new Code(coding.getSystem(), coding.getCode(), coding.getDisplay()));
            } else {
                String msg = String.format("INCOMPLETE FHIR CODING SKIPPED - fhxId:%s system:%s code:%s display:%s", fhxId, coding.getSystem(), coding.getCode(), coding.getDisplay());
                log.warning(msg);
            }

        }
        return codes;
    }

    private boolean isNullRelationship(FamilyMemberHistory fhx) {

        if (fhx == null) {
            log.severe("FamilyMemberHistory IS NULL");
            return true;
        }

        if (fhx.getRelationship() == null) {
            log.severe("FamilyMemberHistory.getRelationship() IS NULL");
            return true;
        }

        if (fhx.getRelationship().getCoding() == null) {
            log.severe("FamilyMemberHistory.getRelationship().getCoding() IS NULL");
            return true;
        }

        return false;
    }

    public Set<Code> getConditionCodings(FamilyMemberHistory fhx) {

        if (isNullCondition(fhx)) {
            return null;
        }

        return toCodeSet(getFamilyHxId(fhx), fhx.getCondition().get(0).getCode().getCoding());
    }

    private boolean isNullCondition(FamilyMemberHistory fhx) {

        return (fhx == null
                || fhx.getCondition() == null
                || fhx.getCondition().get(0) == null
                || fhx.getCondition().get(0).getCode() == null
                || fhx.getCondition().get(0).getCode().getCoding() == null
                );
    }

    public Integer getAgeOfOnset(FamilyMemberHistory fhx) {

        try {
            List<FamilyMemberHistoryConditionComponent> conditions = fhx.getCondition();

            for (FamilyMemberHistoryConditionComponent condition : conditions) { // return first age if there are multiple
                if (condition.getOnset() instanceof Age) {
                    Age age = condition.getOnsetAge();
                    return Integer.valueOf(age.getValue().toBigInteger().toString());
                }
            }

        } catch (FHIRException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Error processing FamilyMemberHistory onsetAge for ", e);
        }

        return null;
    }

    public Range getOnsetRange(FamilyMemberHistory fhx) {

        if (fhx== null || fhx.getCondition() == null) {
            return null;
        }

        try {
            List<FamilyMemberHistoryConditionComponent> conditions = fhx.getCondition();

            for (FamilyMemberHistoryConditionComponent condition : conditions) { // return first range if there are multiple
                if (condition.getOnset() instanceof Range) {
                    return condition.getOnsetRange();
                }
            }

        } catch (FHIRException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Error processing FamilyMemberHistory onsetRange. ", e);
        }

        return null;
    }

    public FamilyMemberHistory toFamilyMemberHistory(
            String fHxId,
            Date fHxDate,
            String patientId,
            Code relationCode,
            Code ehrRelationCode,
            Code conditionCode,
            Code ehrConditionCode,
            String onsetSource,
            Integer onsetAge,
            Range onsetAgeRange,
            String comments
    ) {

        FamilyMemberHistory familyHistory = toFamilyMemberHistory(fHxId, fHxDate, patientId, relationCode, conditionCode, onsetSource, onsetAge, onsetAgeRange, comments);

        familyHistory.getRelationship().addCoding()
                .setCode(ehrRelationCode.getCode())
                .setSystem(ehrRelationCode.getCodeSystem())
                .setDisplay(ehrRelationCode.getCodeLabel());

        FamilyMemberHistoryConditionComponent condition = familyHistory.addCondition();
        condition.getCode().addCoding()
                .setCode(ehrConditionCode.getCode())
                .setSystem(ehrConditionCode.getCodeSystem())
                .setDisplay(ehrConditionCode.getCodeLabel());

        return familyHistory;
    }

    public List<FamilyMemberHistory> familyHxDTOListToFamilyMemberHistoryList(List<FamilyHxDTO> familyHxDTOList) {

        List<FamilyMemberHistory> familyMemberHistoryList = new ArrayList<>();
        for (FamilyHxDTO f: familyHxDTOList) {
            familyMemberHistoryList.add(toFamilyMemberHistory(f));
        }
        return familyMemberHistoryList;
    }

    public List<FamilyMemberHistory> familyHxNlpDTOListToFamilyMemberHistoryList(List<FamilyHxNlpDTO> familyHxNlpDTOList) {

        List<FamilyMemberHistory> familyMemberHistoryList = new ArrayList<>();
        for (FamilyHxNlpDTO f: familyHxNlpDTOList) {
            familyMemberHistoryList.add(toFamilyMemberHistory(f));
        }
        return familyMemberHistoryList;
    }

    public FamilyMemberHistory toFamilyMemberHistory(FamilyHxNlpDTO familyHxNlpDTO) {

        FamilyHxDTO familyHxDTO = familyHxNlpDTO.getFamilyHxDTO();

        FamilyMemberHistory familyMemberHistory = toFamilyMemberHistory(familyHxDTO);
        List<FamilyMemberHistoryConditionComponent> conditions = familyMemberHistory.getCondition();
        FamilyMemberHistoryConditionComponent condition = conditions.get(0);

        boolean hasNlpResult = false;

        if (familyHxNlpDTO.getNlpConditionCode() != null) {
            condition.getCode().addCoding()
                    .setCode(familyHxNlpDTO.getNlpConditionCode())
                    .setSystem(familyHxNlpDTO.getNlpConditionCodeSystem())
                    .setDisplay(familyHxNlpDTO.getNlpConditionLabel());
            hasNlpResult = true;
        }

        if (familyHxNlpDTO.getNlpRelationshipCode() != null) {
            familyMemberHistory.getRelationship().addCoding()
                    .setCode(familyHxNlpDTO.getNlpRelationshipCode())
                    .setSystem(familyHxNlpDTO.getNlpRelationshipCodeSystem())
                    .setDisplay(familyHxNlpDTO.getNlpRelationshipLabel());
            hasNlpResult = true;
        }

        if (familyHxNlpDTO.getNlpConditionOnsetAge() != null
                && familyHxDTO.getConditionOnsetAge() == null
        ) { // only choose NLP if no age of onset

            FuzzyAgeRangeQualifier ageRangeQualifier = FuzzyAgeRangeQualifier.findByKey(familyHxNlpDTO.getNlpConditionOnsetAgeRange());
            NlpOnsetType nlpOnsetType = NlpOnsetType.findByName(familyHxNlpDTO.getNlpConditionOnsetType());

            // Correct the NLP output when ONSET_AGE and DECADE - should be exact age and nit a range
            if (nlpOnsetType != null
                    && nlpOnsetType.equals(NlpOnsetType.ONSET_AGE)
                    && FuzzyAgeRangeQualifier.DECADE.equals(ageRangeQualifier)
            ) {
                Age age = new Age();
                age.setValue(familyHxNlpDTO.getNlpConditionOnsetAge());
                condition.setOnset(age);
            } else {

                UResult<Range> ageOnsetRangeResult = fuzzyAgeRangeInterpreter.interpret(familyHxNlpDTO.getNlpConditionOnsetAge(), ageRangeQualifier, nlpOnsetType);

                if (ageOnsetRangeResult.success()) {
                    Range ageOnsetRange = ageOnsetRangeResult.getResult();
                    condition.setOnset(ageOnsetRange);
                } else {
                    log.warning(ageOnsetRangeResult.getMessage());
                }

            }
            hasNlpResult = true;
        }

        if (hasNlpResult) {

            String onsetExtensionUri = codeService.getUri(NccnTerminologyBinding.NLP_SOURCE_URI);
            List<Extension> extensions = new ArrayList<>();

            Extension ext = new Extension(onsetExtensionUri);
            StringType onsetSourceType = new StringType();
            onsetSourceType.setValue("condition.note");
            ext.setValue(onsetSourceType);
            extensions.add(ext);

            condition.setExtension(extensions);
        }

        return familyMemberHistory;
    }

    public FamilyMemberHistory toFamilyMemberHistory(FamilyHxDTO familyHxDTO) {

        Code relationCode = new Code(
                familyHxDTO.getRelationshipCodeSystem()
                , familyHxDTO.getRelationshipCode()
                , familyHxDTO.getRelationshipLabel()
        );

        Code conditionCode = new Code(
                familyHxDTO.getConditionCodeSystem()
                , familyHxDTO.getConditionCode()
                , familyHxDTO.getConditionLabel()
        );

        ZoneId defaultZoneId = ZoneId.systemDefault();

        String fhxId = familyHxDTO.getFamilyHxId();
        if (!fhxId.contains("_")) {
            fhxId = String.format("%s_%s_%s", familyHxDTO.getPatientId(), familyHxDTO.getEncounterId(), familyHxDTO.getRecordId());
        }

        Date encDate = null;

        if (familyHxDTO.getEncounterDate() != null) {
            encDate = DateUtil.toDate(familyHxDTO.getEncounterDate());
        } else {
            log.warning("NULL ENCOUNTER DATE FOR ENCOUNTER = " + familyHxDTO.getEncounterId() + " AND PATIENT = " + familyHxDTO.getPatientId());
        }

        return toFamilyMemberHistory(
                fhxId
                , encDate
                , familyHxDTO.getPatientId()
                , relationCode
                , conditionCode
                , familyHxDTO.getAgeSource()
                , familyHxDTO.getConditionOnsetAge()
                , null
                , familyHxDTO.getComments()
        );
    }

    public FamilyMemberHistory toFamilyMemberHistory(
            String fHxId,
            Date fHxDate,
            String patientId,
            NccnTerminologyBinding relationCodeBinding,
            NccnTerminologyBinding conditionCodeBinding,
            String onsetSource,
            Integer onsetAge,
            Range onsetAgeRange,
            String comments
    ) {

        Code relationCode = getCode(relationCodeBinding);
        if ( relationCode == null) {
            throw new RuntimeException("relationCodeBinding '" + relationCodeBinding + "' is invalid.");
        }

        Code conditionCode = getCode(conditionCodeBinding);
        if (conditionCode == null) {
            throw new RuntimeException("conditionCodeBinding '" + conditionCodeBinding + "' is invalid.");
        }

        return toFamilyMemberHistory(
                fHxId,
                fHxDate,
                patientId,
                relationCode,
                conditionCode,
                onsetSource,
                onsetAge,
                onsetAgeRange,
                comments
        );

    }

    public FamilyMemberHistory toFamilyMemberHistory(
            String fHxId,
            Date fHxDate,
            String patientId,
            Code relationCode,
            Code conditionCode,
            String onsetSource,
            Integer onsetAge,
            Range onsetAgeRange,
            String comments
    ) {

        FamilyMemberHistory familyHistory = new FamilyMemberHistory();

        familyHistory.setId(fHxId);

        familyHistory.setStatus(FamilyMemberHistory.FamilyHistoryStatus.COMPLETED);
        familyHistory.setDate(fHxDate);

        String ehrFHxCodeSystem = codeService.getCodeSystem(NccnTerminologyBinding.REHR_NCCN_FHX_ID_CODE_SYSTEM_URI);

        familyHistory.addIdentifier()
                .setSystem(ehrFHxCodeSystem)
                .setValue(fHxId);

        familyHistory.setPatient(new Reference("Patient/" + patientId));

        familyHistory.getRelationship().addCoding()
                .setCode(relationCode.getCode())
                .setSystem(relationCode.getCodeSystem())
                .setDisplay(relationCode.getCodeLabel());

        FamilyMemberHistoryConditionComponent condition = familyHistory.addCondition();
        condition.getCode().addCoding()
                .setCode(conditionCode.getCode())
                .setSystem(conditionCode.getCodeSystem())
                .setDisplay(conditionCode.getCodeLabel());

        List<Extension> extensions = new ArrayList<>();

        if ("NLP".equals(onsetSource)) {

            String onsetExtensionUri = codeService.getUri(NccnTerminologyBinding.NLP_SOURCE_URI);

            Extension ext = new Extension(onsetExtensionUri);
            StringType onsetSourceType = new StringType();
            onsetSourceType.setValue("condition.note");
            ext.setValue(onsetSourceType);
            extensions.add(ext);

            condition.setExtension(extensions);
        }

        if (onsetAge != null) {

            Age age = new Age();
            age.setValue(onsetAge);
            condition.setOnset(age);

        } else if (onsetAgeRange != null) {

            condition.setOnset(onsetAgeRange);
        }

        if (comments != null) {
            Annotation annotation = new Annotation();
            List<Annotation> annotations = new ArrayList<>();
            annotations.add(annotation);
            familyHistory.setNote(annotations);
        }

        return familyHistory;
    }

    public boolean isAgeOfOnsetLTE(Integer ageOfOnset, Integer maxAge) {

        if (ageOfOnset == null || maxAge == null) {
            return false;
        }
        else
            return ageOfOnset <= maxAge;
    }

    public boolean isFirstOrSecondOrThirdDegreeRelative(Set<Code> relativeCodes) {
        if (!familySideTerminologyService.isRecognizedFamilyRelation(relativeCodes)) {
            return false;
        }
        return familySideTerminologyService.isLteNthDegreeRelative(3, relativeCodes);
    }

    public boolean isFirstOrSecondDegreeRelative(Set<Code> relativeCodes) {
        if (!familySideTerminologyService.isRecognizedFamilyRelation(relativeCodes)) {
            return false;
        }
        return familySideTerminologyService.isLteNthDegreeRelative(2, relativeCodes);
    }

    public boolean isFirstDegreeRelative(Code relativeCode) {
        Set<Code> codes = new HashSet<>();
        codes.add(relativeCode);

        return isFirstDegreeRelative(codes);
    }

    public boolean isFirstDegreeRelative(Set<Code> relativeCodes) {
        if (!familySideTerminologyService.isRecognizedFamilyRelation(relativeCodes)) {
            return false;
        }
        return familySideTerminologyService.isLteNthDegreeRelative(1, relativeCodes);
    }

    public String getEhrFhxKey(FamilyMemberHistory fhx) {

        if (fhx == null) {
            log.severe("PASSED FHX IS NULL getEhrFhxKey");
            return null;
        }

        String ehrFhxKeyCodeSystem = codeService.getCodeSystem(NccnTerminologyBinding.REHR_NCCN_FHX_ID_CODE_SYSTEM_URI);

        for (Identifier identifier : fhx.getIdentifier()) {

            if (ehrFhxKeyCodeSystem.equals(identifier.getSystem())) {
                return identifier.getValue();
            }

        }
        return null;
    }

    public String[] getEhrFhxKeyParts(FamilyMemberHistory fhx) {

        String keyParts = getEhrFhxKey(fhx);

        if (keyParts == null) {
            log.severe("ERROR EXTRACTING EhrFhxKey");
            return null;
        }

        return keyParts.split("_");
    }

    private static final Gson gson = new GsonBuilder()
            .registerTypeAdapter(FamilyMemberHistory.class, new FamilyMemberHistoryAdapter())
            .create();

    public static String toJson(FamilyMemberHistory fhx) {
        // Convert FamilyMemberHistory object to JSON string
        return gson.toJson(fhx);
    }

    public boolean isDuplicateFHxStatement(List<Set<Code>> relativeList, List<Set<Code>> dxList, Set<Code> relativeCodes, Set<Code> dxCodes) {

        int i = 0;
        for (Set<Code> relativesCompareSet: relativeList) {
            Set<Code> dxCodeComoareSet = dxList.get(i++);
            if (
                    codeService.intersects(NccnTerminologyBinding.RELATIVE_CARDINALITY_1, relativesCompareSet)
                    && codeService.intersects(relativesCompareSet,relativeCodes)
                            && codeService.intersects(dxCodeComoareSet, dxCodes)
            ) {
                return true;
            }
        }
        return false;
    }

    public Map<NccnTerminologyBinding,List<FamilyMemberHistory>> getCountOnTheSameSide(int generations, List<FamilyMemberHistory> fhxList, int count) {

        if (generations < 1 || generations > 3) {
            String error = generations + " is NOT SUPPORTED";
            log.severe(error);
            throw new RuntimeException(error);
        }

        List<FamilyMemberHistory> fatherSide = new ArrayList<>();
        List<FamilyMemberHistory> motherSide = new ArrayList<>();

        List<FamilyMemberHistory> fathersFather = new ArrayList<>();
        List<FamilyMemberHistory> fathersMother = new ArrayList<>();
        List<FamilyMemberHistory> mothersFather = new ArrayList<>();
        List<FamilyMemberHistory> mothersMother = new ArrayList<>();

        List<FamilyMemberHistory> fathersFathersFather = new ArrayList<>();
        List<FamilyMemberHistory> fathersFathersMother = new ArrayList<>();
        List<FamilyMemberHistory> fathersMothersFather = new ArrayList<>();
        List<FamilyMemberHistory> fathersMothersMother = new ArrayList<>();

        List<FamilyMemberHistory> mothersFathersFather = new ArrayList<>();
        List<FamilyMemberHistory> mothersFathersMother = new ArrayList<>();
        List<FamilyMemberHistory> mothersMothersFather = new ArrayList<>();
        List<FamilyMemberHistory> mothersMothersMother = new ArrayList<>();

        List<Set<Code>> relativeList = new ArrayList<>();
        List<Set<Code>> dxList = new ArrayList<>();

        for (FamilyMemberHistory fhx : fhxList) {

            Set<Code> relativeCodes = getRelativeCodings(fhx);
            Set<Code> dxCodes = getConditionCodings(fhx);

            // Need to make sure singular relationships and dx are not duplicated
            // i.e. mom doesn't have breast cancer twice or similar
            if (isDuplicateFHxStatement(relativeList, dxList, relativeCodes, dxCodes)) {
                continue;
            }

            relativeList.add(relativeCodes);
            dxList.add(dxCodes);

            if (generations == 1) {

                if (codeService.intersects(familySideTerminologyService.FATHERS_FAMILY_SIDE_CODE_SET, relativeCodes)) {
                    fatherSide.add(fhx);
                }

                if (codeService.intersects(familySideTerminologyService.MOTHERS_FAMILY_SIDE_CODE_SET, relativeCodes)) {
                    motherSide.add(fhx);
                }
            }

            if (generations == 2) {
                if (codeService.intersects(familySideTerminologyService.FATHERS_FATHER_FAMILY_SIDE_CODE_SET, relativeCodes)) {
                    fathersFather.add(fhx);
                }

                if (codeService.intersects(familySideTerminologyService.FATHERS_MOTHER_FAMILY_SIDE_CODE_SET, relativeCodes)) {
                    fathersMother.add(fhx);
                }

                if (codeService.intersects(familySideTerminologyService.MOTHERS_FATHER_FAMILY_SIDE_CODE_SET, relativeCodes)) {
                    mothersFather.add(fhx);
                }

                if (codeService.intersects(familySideTerminologyService.MOTHERS_MOTHER_FAMILY_SIDE_CODE_SET, relativeCodes)) {
                    mothersMother.add(fhx);
                }
            }

            if (generations == 3) {

                if (codeService.intersects(familySideTerminologyService.FATHERS_FATHERS_FATHER_FAMILY_SIDE_CODE_SET, relativeCodes)) {
                    fathersFathersFather.add(fhx);
                }

                if (codeService.intersects(familySideTerminologyService.FATHERS_FATHERS_MOTHER_FAMILY_SIDE_CODE_SET, relativeCodes)) {
                    fathersFathersMother.add(fhx);
                }

                if (codeService.intersects(familySideTerminologyService.FATHERS_MOTHERS_FATHER_FAMILY_SIDE_CODE_SET, relativeCodes)) {
                    fathersMothersFather.add(fhx);
                }

                if (codeService.intersects(familySideTerminologyService.FATHERS_MOTHERS_MOTHER_FAMILY_SIDE_CODE_SET, relativeCodes)) {
                    fathersMothersMother.add(fhx);
                }

                if (codeService.intersects(familySideTerminologyService.MOTHERS_FATHERS_FATHER_FAMILY_SIDE_CODE_SET, relativeCodes)) {
                    mothersFathersFather.add(fhx);
                }

                if (codeService.intersects(familySideTerminologyService.MOTHERS_FATHERS_MOTHER_FAMILY_SIDE_CODE_SET, relativeCodes)) {
                    mothersFathersMother.add(fhx);
                }

                if (codeService.intersects(familySideTerminologyService.MOTHERS_MOTHERS_FATHER_FAMILY_SIDE_CODE_SET, relativeCodes)) {
                    mothersMothersFather.add(fhx);
                }

                if (codeService.intersects(familySideTerminologyService.MOTHERS_MOTHERS_MOTHER_FAMILY_SIDE_CODE_SET, relativeCodes)) {
                    mothersMothersMother.add(fhx);
                }
            }
        }

        Map<NccnTerminologyBinding, List<FamilyMemberHistory>> familySideMap = new HashMap<>();

        if (generations == 1) {

            if (fatherSide.size() >= count) {
                familySideMap.put(NccnTerminologyBinding.FAMILY_SIDE_GEN_1P, fatherSide);
            }
            if (motherSide.size() >= count) {
                familySideMap.put(NccnTerminologyBinding.FAMILY_SIDE_GEN_1M, motherSide);
            }

            return familySideMap;

        } else if (generations == 2) {

            if (fathersFather.size() >= count) {
                familySideMap.put(NccnTerminologyBinding.FAMILY_SIDE_GEN_2PP, fathersFather);
            }
            if (fathersMother.size() >= count) {
                familySideMap.put(NccnTerminologyBinding.FAMILY_SIDE_GEN_2PM, fathersMother);
            }
            if (mothersFather.size() >= count) {
                familySideMap.put(NccnTerminologyBinding.FAMILY_SIDE_GEN_2MP, mothersFather);
            }
            if (mothersMother.size() >= count) {
                familySideMap.put(NccnTerminologyBinding.FAMILY_SIDE_GEN_2MM, mothersMother);
            }

            return familySideMap;
        }

        // Third gen
        if (fathersFathersFather.size() >= count) {
            familySideMap.put(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PPP, fathersFathersFather);
        }
        if (fathersFathersMother.size() >= count) {
            familySideMap.put(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PPM, fathersFathersMother);
        }
        if (fathersMothersFather.size() >= count) {
            familySideMap.put(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PMP, fathersMothersFather);
        }
        if (fathersMothersMother.size() >= count) {
            familySideMap.put(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PMM, fathersMothersMother);
        }
        if (mothersFathersFather.size() >= count) {
            familySideMap.put(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MPP, mothersFathersFather);
        }
        if (mothersFathersMother.size() >= count) {
            familySideMap.put(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MPM, mothersFathersMother);
        }
        if (mothersMothersFather.size() >= count) {
            familySideMap.put(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MMP, mothersMothersFather);
        }
        if (mothersMothersMother.size() >= count) {
            familySideMap.put(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MMM, mothersMothersMother);
        }

        return familySideMap;
    }

    private Code getCode(NccnTerminologyBinding binding) {
        Set<Code> codes = codeService.getCodeSet(binding);
        for (Code code:codes) {
            return code;
        }
        return null;
    }

}

package edu.utah.rehr.garde.domain.core.model;

import java.util.Date;

public class MetCriteriaMessage {

    String ehrPatientId;
    String messageCd;
    String messageLabel;
    Date encounterDate;
    Date versionDate;

    public String getEhrPatientId() {
        return ehrPatientId;
    }

    public void setEhrPatientId(String ehrPatientId) {
        this.ehrPatientId = ehrPatientId;
    }

    public String getMessageCd() {
        return messageCd;
    }

    public void setMessageCd(String messageCd) {
        this.messageCd = messageCd;
    }

    public String getMessageLabel() {
        return messageLabel;
    }

    public void setMessageLabel(String messageLabel) {
        this.messageLabel = messageLabel;
    }

    public Date getEncounterDate() {
        return encounterDate;
    }

    public void setEncounterDate(Date encounterDate) {
        this.encounterDate = encounterDate;
    }

    public Date getVersionDate() {
        return versionDate;
    }

    public void setVersionDate(Date versionDate) {
        this.versionDate = versionDate;
    }

    public String getMessage(int index) {

        return index + ") " + messageLabel;
    }
}

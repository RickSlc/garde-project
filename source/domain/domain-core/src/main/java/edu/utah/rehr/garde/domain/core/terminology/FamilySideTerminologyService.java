package edu.utah.rehr.garde.domain.core.terminology;

import edu.utah.rehr.garde.domain.core.terminology.binding.NccnTerminologyBinding;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import lombok.extern.java.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Log
public class FamilySideTerminologyService {

    private final ICodeService codeService;

    public Set<Code> FAMILY_SIDE_GEN_1_CODE_SET;
    public Set<Code> FATHERS_FAMILY_SIDE_CODE_SET;
    public Set<Code> MOTHERS_FAMILY_SIDE_CODE_SET;
    public Set<Code> FATHERS_FATHER_FAMILY_SIDE_CODE_SET;
    public Set<Code> FATHERS_MOTHER_FAMILY_SIDE_CODE_SET;
    public Set<Code> MOTHERS_FATHER_FAMILY_SIDE_CODE_SET;
    public Set<Code> MOTHERS_MOTHER_FAMILY_SIDE_CODE_SET;
    public Set<Code> FATHERS_FATHERS_FATHER_FAMILY_SIDE_CODE_SET;
    public Set<Code> FATHERS_FATHERS_MOTHER_FAMILY_SIDE_CODE_SET;
    public Set<Code> FATHERS_MOTHERS_FATHER_FAMILY_SIDE_CODE_SET;
    public Set<Code> FATHERS_MOTHERS_MOTHER_FAMILY_SIDE_CODE_SET;
    public Set<Code> MOTHERS_FATHERS_FATHER_FAMILY_SIDE_CODE_SET;
    public Set<Code> MOTHERS_FATHERS_MOTHER_FAMILY_SIDE_CODE_SET;
    public Set<Code> MOTHERS_MOTHERS_FATHER_FAMILY_SIDE_CODE_SET;
    public Set<Code> MOTHERS_MOTHERS_MOTHER_FAMILY_SIDE_CODE_SET;

    public FamilySideTerminologyService(ICodeService codeService) {
        this.codeService = codeService;
    }

    private void loadCodeSets() {

        if (this.FAMILY_SIDE_GEN_1_CODE_SET == null) {
            this.FAMILY_SIDE_GEN_1_CODE_SET = getFamilySideRelations(NccnTerminologyBinding.FAMILY_SIDE_GEN_1);

            this.FATHERS_FAMILY_SIDE_CODE_SET = getFamilySideRelations(NccnTerminologyBinding.FAMILY_SIDE_GEN_1P);
            this.MOTHERS_FAMILY_SIDE_CODE_SET = getFamilySideRelations(NccnTerminologyBinding.FAMILY_SIDE_GEN_1M);

            this.FATHERS_FATHER_FAMILY_SIDE_CODE_SET = getFamilySideRelations(NccnTerminologyBinding.FAMILY_SIDE_GEN_2PP);
            this.FATHERS_MOTHER_FAMILY_SIDE_CODE_SET = getFamilySideRelations(NccnTerminologyBinding.FAMILY_SIDE_GEN_2PM);
            this.MOTHERS_FATHER_FAMILY_SIDE_CODE_SET = getFamilySideRelations(NccnTerminologyBinding.FAMILY_SIDE_GEN_2MP);
            this.MOTHERS_MOTHER_FAMILY_SIDE_CODE_SET = getFamilySideRelations(NccnTerminologyBinding.FAMILY_SIDE_GEN_2MM);

            this.FATHERS_FATHERS_FATHER_FAMILY_SIDE_CODE_SET = getFamilySideRelations(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PPP);
            this.FATHERS_FATHERS_MOTHER_FAMILY_SIDE_CODE_SET = getFamilySideRelations(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PPM);
            this.FATHERS_MOTHERS_FATHER_FAMILY_SIDE_CODE_SET = getFamilySideRelations(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PMP);
            this.FATHERS_MOTHERS_MOTHER_FAMILY_SIDE_CODE_SET = getFamilySideRelations(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PMM);
            this.MOTHERS_FATHERS_FATHER_FAMILY_SIDE_CODE_SET = getFamilySideRelations(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MPP);
            this.MOTHERS_FATHERS_MOTHER_FAMILY_SIDE_CODE_SET = getFamilySideRelations(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MPM);
            this.MOTHERS_MOTHERS_FATHER_FAMILY_SIDE_CODE_SET = getFamilySideRelations(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MMP);
            this.MOTHERS_MOTHERS_MOTHER_FAMILY_SIDE_CODE_SET = getFamilySideRelations(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MMM);
        }
    }

    public boolean isRecognizedFamilyRelation(Set<Code> codes) {

        loadCodeSets();

        return codeService.intersects(FATHERS_FATHERS_FATHER_FAMILY_SIDE_CODE_SET, codes)
                || codeService.intersects(FATHERS_FATHERS_MOTHER_FAMILY_SIDE_CODE_SET, codes)
                || codeService.intersects(FATHERS_MOTHERS_FATHER_FAMILY_SIDE_CODE_SET, codes)
                || codeService.intersects(FATHERS_MOTHERS_MOTHER_FAMILY_SIDE_CODE_SET, codes)
                || codeService.intersects(MOTHERS_FATHERS_FATHER_FAMILY_SIDE_CODE_SET, codes)
                || codeService.intersects(MOTHERS_FATHERS_MOTHER_FAMILY_SIDE_CODE_SET, codes)
                || codeService.intersects(MOTHERS_MOTHERS_FATHER_FAMILY_SIDE_CODE_SET, codes)
                || codeService.intersects(MOTHERS_MOTHERS_MOTHER_FAMILY_SIDE_CODE_SET, codes);

    }
    public boolean isOnSameSide(NccnTerminologyBinding s1, NccnTerminologyBinding s2) {

        loadCodeSets();

        Set<Code> s1Set = codeService.getCodeSet(s1);
        NccnTerminologyBinding s1Side = getLeastFamilySide(s1Set);
        Set<Code> s1SideSet = getFamilySideRelations(s1Side);

        Set<Code> s2Set = codeService.getCodeSet(s2);
        NccnTerminologyBinding s2Side = getLeastFamilySide(s2Set);
        Set<Code> s2SideSet = getFamilySideRelations(s2Side);

        return s1SideSet.containsAll(s2SideSet) || s2SideSet.containsAll(s1SideSet);
    }

    public int getGeneration(NccnTerminologyBinding familySideGeneration) {

        loadCodeSets();

        String s1 = getSuffix(familySideGeneration.getKey());
        if (!(s1.startsWith("1") || s1.startsWith("2") || s1.startsWith("3"))) {
            throw new RuntimeException("Family side mot recognized: " + familySideGeneration);
        }
        return Integer.parseInt(s1.substring(0,1));
    }

    public NccnTerminologyBinding getLeastFamilySide(Set<Code> familyRelation) {

        loadCodeSets();

        if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_1, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_1;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_1M, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_1M;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_1P, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_1P;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_2, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_2;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_2P, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_2P;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_2M, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_2M;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_2PP, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_2PP;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_2PM, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_2PM;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_2MP, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_2MP;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_2MM, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_2MM;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3P, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3P;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3M, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3M;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PP, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3PP;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PM, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3PM;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MP, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3MP;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MM, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3MM;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PPP, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3PPP;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PPM, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3PPM;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PMP, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3PMP;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PMM, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3PMM;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MPP, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3MPP;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MPM, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3MPM;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MMP, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3MMP;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MMM, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3MMM;
        }
        else {
            throw new RuntimeException(familyRelation + " is not a valid family relationship.");
        }

    }

    public NccnTerminologyBinding getMaxFamilySide(Set<Code> familyRelation) {

        loadCodeSets();

        if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PPP, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3PPP;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PPM, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3PPM;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PMP, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3PMP;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PMM, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3PMM;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MPP, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3MPP;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MPM, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3MPM;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MMP, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3MMP;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MMM, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3MMM;
        }

        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PP, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3PP;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PM, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3PM;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MP, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3MP;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MM, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3MM;
        }

        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3P, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3P;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_3M, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_3M;
        }

        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_2PP, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_2PP;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_2PM, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_2PM;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_2MP, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_2MP;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_2MM, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_2MM;
        }

        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_2P, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_2P;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_2M, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_2M;
        }

        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_2, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_2;
        }

        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_1M, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_1P;
        }
        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_1P, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_1M;
        }

        else if ( codeService.intersects(NccnTerminologyBinding.FAMILY_SIDE_GEN_1, familyRelation) ) {
            return NccnTerminologyBinding.FAMILY_SIDE_GEN_1;
        }

        else {
            throw new RuntimeException(familyRelation + " is not a valid family relationship.");
        }

    }


    private Set<Code> getFamilySideRelations(NccnTerminologyBinding terminologyKey) {

        List<NccnTerminologyBinding> codeSetKeys = new ArrayList<>();
        codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1);

        switch (terminologyKey) {
            case FAMILY_SIDE_GEN_1:
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_1P:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1P);
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_1M:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1M);
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_2:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_2P:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2P);
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_2M:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2M);
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_2PP:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2PP);
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_2PM:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2PM);
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_2MP:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2MP);
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_2MM:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2MM);
                return codeService.union(codeSetKeys);

            case FAMILY_SIDE_GEN_3P:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3P);
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_3PP:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2PP);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PP);
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_3PM:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2PM);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PM);
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_3PPP:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2PP);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PP);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PPP);
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_3PPM:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2PP);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PP);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PPM);
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_3PMP:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2PM);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PM);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PMP);
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_3PMM:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2PM);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3P);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PM);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PMM);
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_3M:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3M);
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_3MP:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2MP);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MP);
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_3MM:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2MM);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MM);
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_3MPP:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2MP);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MP);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MPP);
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_3MPM:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2MP);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MP);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MPM);
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_3MMP:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2MM);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MM);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MMP);
                return codeService.union(codeSetKeys);
            case FAMILY_SIDE_GEN_3MMM:
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2MM);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3M);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MM);
                codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MMM);
                return codeService.union(codeSetKeys);

            default:
                log.severe(terminologyKey.getKey() + " NOT SUPPORTED");

        }

        return null;
    }

    public boolean isLteNthDegreeRelative(int degree, Set<Code> relativeCodes) {

        loadCodeSets();

        if (degree < 1 || degree > 3) {
            throw new RuntimeException("Family relative degrees 1 through 3 are supported. " + degree + " is not.");
        }

        if (relativeCodes == null || relativeCodes.isEmpty()) {
            return false;
        }

        int relativeDegree = getRelativeDegree(relativeCodes);
        return relativeDegree <= degree;
    }

    public int getRelativeDegree(Set<Code> relativeCodes) {

        loadCodeSets();

        if (relativeCodes == null || relativeCodes.isEmpty()) {
            log.warning("NULL OR EMPTY RELATIVE CAN'T BE EVALUATED");
            return 0;
        }

        if (codeService.intersects(FATHERS_FAMILY_SIDE_CODE_SET, relativeCodes)
                || codeService.intersects(MOTHERS_FAMILY_SIDE_CODE_SET, relativeCodes)) {
            return 1;
        }

        if (codeService.intersects(FATHERS_FATHER_FAMILY_SIDE_CODE_SET, relativeCodes)
                    || codeService.intersects(FATHERS_MOTHER_FAMILY_SIDE_CODE_SET, relativeCodes)
                    || codeService.intersects(MOTHERS_FATHER_FAMILY_SIDE_CODE_SET, relativeCodes)
                    || codeService.intersects(MOTHERS_MOTHER_FAMILY_SIDE_CODE_SET, relativeCodes) ) {
            return 2;
        }

        if (codeService.intersects(FATHERS_FATHERS_FATHER_FAMILY_SIDE_CODE_SET, relativeCodes)
                || codeService.intersects(FATHERS_FATHERS_MOTHER_FAMILY_SIDE_CODE_SET, relativeCodes)
                || codeService.intersects(FATHERS_MOTHERS_FATHER_FAMILY_SIDE_CODE_SET, relativeCodes)
                || codeService.intersects(FATHERS_MOTHERS_MOTHER_FAMILY_SIDE_CODE_SET, relativeCodes)
                || codeService.intersects(MOTHERS_FATHERS_FATHER_FAMILY_SIDE_CODE_SET, relativeCodes)
                || codeService.intersects(MOTHERS_FATHERS_MOTHER_FAMILY_SIDE_CODE_SET, relativeCodes)
                || codeService.intersects(MOTHERS_MOTHERS_FATHER_FAMILY_SIDE_CODE_SET, relativeCodes)
                || codeService.intersects(MOTHERS_MOTHERS_MOTHER_FAMILY_SIDE_CODE_SET, relativeCodes)) {
            return 3;
        }

        throw new RuntimeException("Relative not supported: " + relativeCodes);
    }

    private String getSuffix(String familySideKey) {
        return familySideKey.replace("FAMILY_SIDE_GEN_","");
    }

}




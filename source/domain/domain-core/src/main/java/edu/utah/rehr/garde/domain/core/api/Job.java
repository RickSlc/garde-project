package edu.utah.rehr.garde.domain.core.api;

public enum Job {

    UpdateCohort("UPDATE_COHORT")
    , ExtractPopulation("EXTRACT_POP")
    , ExtractFacts("EXTRACT_POP_FACTS")
    , ExecutePopulationCds("EXEC_POP_CDS")
    , ExportPopulation("EXPORT_POP")
    , LoadPatientTranscripts("LOAD_TRANSCRIPTS")
    , ExportTranscripts("EXPORT_TRANSCRIPTS")
    , LoadStudyArtifacts("LOAD_STUDY_ARTIFACTS")
    , LoadMetCriteriaSDEsAllPatients("LOAD_SDES")
    , TestInvitaeAuth("TEST_CG")
    , TestConfig("TEST_CONFIG")
    , TestChatLinkRetrieve("TEST_LINKS")
    , TestJobs("TEST_JOBS")
    , WriteDataElement("WRITE_DE")
    , ReadDataElement("READ_DE")
    ;

    private final String jobCode;

    Job(String jobCode) {
        this.jobCode = jobCode;
    }

    public String getJobCode() {
        return jobCode;
    }

    public static Job getJob(String jobCode) {

        for (Job job: Job.values()){
            if (job.jobCode.equals(jobCode)) {
                return job;
            }
        }

        return null;
    }

}

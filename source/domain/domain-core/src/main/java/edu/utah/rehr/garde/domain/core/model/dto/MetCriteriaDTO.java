package edu.utah.rehr.garde.domain.core.model.dto;

import edu.utah.rehr.garde.domain.core.model.GlueRule;
import edu.utah.rehr.garde.domain.core.model.MetCriteriaFactKey;
import edu.utah.rehr.garde.domain.core.model.Nccn2023Rule;
import edu.utah.rehr.garde.domain.core.model.NccnRule;
import lombok.extern.java.Log;

@Log
public class MetCriteriaDTO extends MetCriteriaFactKey {

    private final String criteriaCd;
    private final String criteriaLabel;

    public MetCriteriaDTO(String ehrPatientId, String ehrEncounterId, String ehrRecordId, String cohortCd, String criteriaId) {
        super(ehrPatientId, ehrEncounterId, ehrRecordId, cohortCd, criteriaId);
        if (NccnRule.findById(criteriaId) != null) {
            this.criteriaCd = NccnRule.findById(criteriaId).getCode();
            this.criteriaLabel = NccnRule.findById(criteriaId).getLabel();
        } else if (Nccn2023Rule.findById(criteriaId) != null) {
            this.criteriaCd = Nccn2023Rule.findById(criteriaId).getCode();
            this.criteriaLabel = Nccn2023Rule.findById(criteriaId).getLabel();
        } else if (GlueRule.findById(criteriaId) != null) {
            this.criteriaCd = GlueRule.findById(criteriaId).getCode();
            this.criteriaLabel = GlueRule.findById(criteriaId).getLabel();
        } else {
            this.criteriaCd = null;
            this.criteriaLabel = null;
            log.warning("INVALID criteriaId FOR \n" + this);
        }
    }

    public RuleDTO toRuleDTO() {

        String ruleLabel = "";
        String ruleBadge = "";
        if (NccnRule.findById(getCriteriaId()) != null) {
            ruleLabel = NccnRule.findById(getCriteriaId()).getLabel();
            ruleBadge = NccnRule.findById(getCriteriaId()).getShortCode();
        } else if (Nccn2023Rule.findById(getCriteriaId()) != null) {
            ruleLabel = Nccn2023Rule.findById(getCriteriaId()).getLabel();
            ruleBadge = Nccn2023Rule.findById(getCriteriaId()).getShortCode();
        } else if (GlueRule.findById(getCriteriaId()) != null) {
            ruleLabel = GlueRule.findById(getCriteriaId()).getLabel();
            ruleBadge = GlueRule.findById(getCriteriaId()).getShortCode();
        } else {
            ruleBadge = "ER";
            ruleLabel = "NOT_FOUND";
        }

        return new RuleDTO(
                Long.parseLong(getCriteriaId())
                , criteriaCd
                , ruleLabel
                , ruleBadge
        );

    }

    @Override
    public String toString() {

        String metCriteria = "ehrPatientId: " + super.getEhrPatientId() + ", " +
                "ehrEncounterId: " + super.getEhrEncounterId() + ", " +
                "ehrRecordId: " + super.getEhrRecordId() + ", " +
                "cohortCd: " + super.getCohortCd() + ", " +
                "criteriaId: " + super.getCriteriaId() + ", " +
                "criteriaCd: " + this.criteriaCd + ", " +
                "criteriaLabel: " + this.criteriaLabel + "\n";

        return metCriteria;
    }
}

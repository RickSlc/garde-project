package edu.utah.rehr.garde.domain.core.model.fhir;

public enum FuzzyAgeRangeQualifier {

    AROUND("AROUND") // she was around 45
    , EXACT("EXACT") // she was 36
    , EARLY("EARLY") // she was in her early 30s
    , MID("MID")     // she was in her mid thirties
    , LATE("LATE")   // she was in her late 50s
    , BEFORE("BEFORE") // died 2010 - event happened before 2010
    , DECADE("DECADE") // she was in her 60s
    , PREMENOPAUSAL("PREMENOPAUSAL")
    , POSTMENOPAUSAL("POSTMENOPAUSAL")
    ;

    private final String key;

    FuzzyAgeRangeQualifier(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public static FuzzyAgeRangeQualifier findByKey(String key) {
        for (FuzzyAgeRangeQualifier t: FuzzyAgeRangeQualifier.values()) {
            if (t.getKey().equals(key)) {
                return t;
            }
        }
        return null;
    }

}

package edu.utah.rehr.garde.domain.core.model.dto;

import edu.utah.rehr.garde.domain.core.model.annotation.CsvHeader;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
public class BasePatientDTO {

    @NonNull
    @CsvHeader(name = "EHR_PAT_ID_CODE_SYSTEM", required = true)
    private String patientIdCodeSystem;

    @NonNull
    @CsvHeader(name = "EHR_PAT_ID", required = true)
    private String patientId;

}

package edu.utah.rehr.garde.domain.core.terminology.binding;

import edu.utah.rehr.garde.domain.core.terminology.ITerminologyKey;
import edu.utah.rehr.garde.domain.core.terminology.TerminologyEntityType;

public enum GlueTerminologyBinding implements ITerminologyKey {

    LDL_C(TerminologyEntityType.CODE)
    , CORONARY_ARTERY_DISEASE(TerminologyEntityType.CODE)
    , PREMATURE_CORONARY_ARTERY_DISEASE(TerminologyEntityType.CODE)
    ;

    final TerminologyEntityType terminologyEntityType;
    final String key;

    GlueTerminologyBinding(TerminologyEntityType type) {
        this.terminologyEntityType = type;
        this.key = name();
    }

    @Override
    public TerminologyEntityType getEntityType() {
        return terminologyEntityType;
    }

    @Override
    public String getKey() {
        return key;
    }
}

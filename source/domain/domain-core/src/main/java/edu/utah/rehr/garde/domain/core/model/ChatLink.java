package edu.utah.rehr.garde.domain.core.model;

public class ChatLink {

    Integer chatScriptId;
    String patientId;
    String chatLink;

    public Integer getChatScriptId() {
        return chatScriptId;
    }

    public void setChatScriptId(Integer chatScriptId) {
        this.chatScriptId = chatScriptId;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getChatLink() {
        return chatLink;
    }

    public void setChatLink(String chatLink) {
        this.chatLink = chatLink;
    }

}

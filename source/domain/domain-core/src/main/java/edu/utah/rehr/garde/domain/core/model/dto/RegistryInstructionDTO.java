package edu.utah.rehr.garde.domain.core.model.dto;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
public class RegistryInstructionDTO {

    private final String ehrPatientIdCodeSystem;
    private final String ehrPatientId;
    private final String instruction;
    private final String dataElementType;
    private final String dataElementName;
    private final String dataElementId;
    private final String dataElementValue;

    public RegistryInstructionDTO(
            String ehrPatientIdCodeSystem
            , String ehrPatientId
            , String instruction
            , String dataElementType
            , String dataElementName
            , String dataElementId
            , String dataElementValue) {

        if (StringUtils.isEmpty(ehrPatientIdCodeSystem)) {
            throw new IllegalArgumentException("ehrPatientIdCodeSystem must not be empty");
        }
        if (StringUtils.isEmpty(ehrPatientId)) {
            throw new IllegalArgumentException("ehrPatientId must not be empty");
        }
        if (StringUtils.isEmpty(instruction)) {
            throw new IllegalArgumentException("instruction must not be empty");
        }
        if (StringUtils.isEmpty(dataElementType)) {
            throw new IllegalArgumentException("dataElementType must not be empty");
        }
        if (StringUtils.isEmpty(dataElementName)) {
            throw new IllegalArgumentException("dataElementName must not be empty");
        }
        if (StringUtils.isEmpty(dataElementId)) {
            throw new IllegalArgumentException("dataElementId must not be empty");
        }
        if (StringUtils.isEmpty(dataElementValue)) {
            throw new IllegalArgumentException("dataElementValue must not be empty");
        }

        this.ehrPatientIdCodeSystem = ehrPatientIdCodeSystem;
        this.ehrPatientId = ehrPatientId;
        this.instruction = instruction;
        this.dataElementType = dataElementType;
        this.dataElementName = dataElementName;
        this.dataElementId = dataElementId;
        this.dataElementValue = dataElementValue;
    }
}
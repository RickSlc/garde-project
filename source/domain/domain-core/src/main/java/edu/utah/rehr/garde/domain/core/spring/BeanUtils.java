package edu.utah.rehr.garde.domain.core.spring;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.util.ReflectionUtils;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class BeanUtils implements ApplicationContextAware {
    private static ApplicationContext context;

    public BeanUtils() {
    }

    public static <T> T getBean(Class<T> beanClass) {
        return context.getBean(beanClass);
    }

    public static <T> T getBean(String name, Class<T> beanClass) {
        return name == null ? getBean(beanClass) : context.getBean(name, beanClass);
    }

    public static <T> T getBean(String bean, Class<T> beanClass, Supplier<T> getter, Consumer<T> setter) {
        T value = getter.get();
        if (value == null) {
            synchronized (beanClass) {
                value = getter.get();
                if (value == null) {
                    value = getBean(bean, beanClass);
                    setter.accept(value);
                }
            }
        }

        return value;
    }

    public static void enumerateInjectedProperties(Object instance, BiConsumer<String, Object> consumer) {
        ReflectionUtils.doWithFields(instance.getClass(), (field) -> {
            field.setAccessible(true);
            Value value = field.getAnnotation(Value.class);
            if (value != null) {
                String propName = value.value();
                propName = StringUtils.substringAfter(propName, "${");
                propName = StringUtils.substringBeforeLast(propName, "?");
                consumer.accept(propName, field.get(instance));
            }

        });
    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }
}

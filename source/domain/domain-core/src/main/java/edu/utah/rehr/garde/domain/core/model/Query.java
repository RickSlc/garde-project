package edu.utah.rehr.garde.domain.core.model;

import java.util.Arrays;
import java.util.List;
@Deprecated
public enum Query {

    SELECT_MAX_VERSION("SELECT_MAX_VERSION"
            , "Select the max version of the n_vrsn table."
            , new String[]{})
    , INSERT_NEW_VRSN("INSERT_NEW_VRSN"
            , "Insert new version."
            , new String[]{"NEW_VERSION"})
    , TRUNCATE_PAT_SCREENED("TRUNCATE_PAT_SCREENED"
            , "Truncate the n_pat_screened table in preparation for new."
            , new String[]{})
    , IDB_TRUNCATE_FHX("IDB_TRUNCATE_FHX"
            , "Truncate the FHx table in preparation for new facts."
            , new String[]{})
    , ETL_FHX_FACTS_FOR_NCCN_RULES("ETL_FHX_FACTS_FOR_NCCN_RULES"
            , "Etl the FHx facts, transform them by adding terminology, and load them into intermediate db."
            , new String[]{"NEW_VERSION"})
    , E_PATIENTS_WHO_MEET_NCCN_SCREENING_CRITERIA("E_PATIENTS_WHO_MEET_NCCN_SCREENING_CRITERIA"
            , "Query for new patients and extract/return their IDs."
            , new String[]{"NEW_VERSION"})
    , E_PATIENTS_WHO_PREVIOUSLY_MET_NCCN_SCREENING_CRITERIA("E_PATIENTS_WHO_PREVIOUSLY_MET_NCCN_SCREENING_CRITERIA"
            , "Query for patients whom previously met screening criteria extract/return their IDs."
            , new String[]{"NEW_VERSION"})
    , E_PATIENTS_WHO_PREVIOUSLY_MET_NCCN_GENETIC_COUNSELING_CRITERIA("E_PATIENTS_WHO_PREVIOUSLY_MET_NCCN_GENETIC_COUNSELING_CRITERIA"
            , "Query for patients whom previously met screening criteria extract/return their IDs."
            , new String[]{"NEW_VERSION"})
    , ETL_PATIENTS_WHO_MEET_NCCN_SCREENING_CRITERIA("ETL_PATIENTS_WHO_MEET_NCCN_SCREENING_CRITERIA"
            , "Query for patients whom previously met screening criteria extract/return their IDs."
            , new String[]{"NEW_VERSION"})
    , ET_PATIENT_DEMOG_MET_NCCN_SCREENING_CRITERIA("ET_PATIENT_DEMOG_MET_NCCN_SCREENING_CRITERIA"
            , "Extract and transform patient demographics for those who met screening criteria."
            , new String[]{})
    , IDB_GET_MET_CRITERIA("IDB_GET_MET_CRITERIA", "Select logically current family hx that met rule criteria. "
            , new String[]{})
    , IDB_UPDATE_MET_GC_CRITERIA_COHORT("IDB_UPDATE_MET_GC_CRITERIA_COHORT", "Insert new and logically delete patients who no longer meet criteria."
             , new String[]{"NEW_VERSION"})
    , EDW_PATIENT_SELECT("EDW_PATIENT_SELECT","Select PatientFact from EDW. Needs the where clause appended to filter for intended population."
            , new String[]{})
    , IDB_GET_STUDY_PATIENTS("IDB_GET_U01_PILOT_PATIENTS", "Retrieve study patients from n_pat_study"
            , new String[]{"STUDY_CD"})
    , IDB_GET_NEW_VERSION_MET_CRITERIA_MSG("IDB_GET_NEW_VERSION_MET_CRITERIA_MSG", "Get messages for incremental updates explaining why the patient met criteria for genetic counseling."
            , new String[]{"NEW_VERSION"})
    , IDB_GET_ALL_PATIENTS_MET_CRITERIA_MSGS("IDB_GET_ALL_PATIENTS_MET_CRITERIA_MSGS", "Get all patient messages explaining why the patient met criteria for genetic counseling."
            , new String[]{})
    ;

    private final String code;
    private final String description;
    private final List<String> variableList;

    Query(String code, String description, String[] replacementVariables) {
        this.code = code;
        this.description = description;
        this.variableList = Arrays.asList(replacementVariables);
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return this.description;
    }

    public List<String> getVariableList() {
        return this.variableList;
    }

    @Override
    public String toString() {
        return this.code;
    }
}


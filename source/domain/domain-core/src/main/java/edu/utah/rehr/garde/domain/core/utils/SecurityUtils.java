package edu.utah.rehr.garde.domain.core.utils;

import edu.utah.rehr.garde.domain.core.accessory.UResult;
import org.opencds.sec.lib.Config;
import org.opencds.sec.lib.Decryptor;

public class SecurityUtils {

    public static String unencrypt(String encryptedValue) {

        Config config = new Config()
                .withMaster("OPENCDS_MASTER")
                .withAlgorithm("PBEWITHSHA256AND128BITAES-CBC-BC");
        Decryptor decryptor = new Decryptor(config);
        String unencrypted = decryptor.decrypt(encryptedValue);

        return unencrypted;
    }

    public static UResult<String> decrypt(String encryptedValue) {

        try {
            String unencrypted = unencrypt(encryptedValue);
            return UResult.successResult(unencrypted);
        } catch (Exception e) {
            return UResult.errorResult(e);
        }
    }

}

package edu.utah.rehr.garde.domain.core.provider;

import edu.utah.rehr.garde.domain.core.model.GCohort;
import org.hl7.fhir.dstu3.model.FamilyMemberHistory;

import java.util.List;
import java.util.Map;

@Deprecated
public interface IFhirFamilyMemberHistoryProvider {

    Map<String, List<FamilyMemberHistory>> getFhirFamilyMemberHistoryMap(GCohort cohort, Long version);

}

package edu.utah.rehr.garde.domain.core.model.dto;

import java.util.List;

public class MeasureCategoryDTO {

    public String measureCategory;
    public List<MeasureDTO> measures;

}

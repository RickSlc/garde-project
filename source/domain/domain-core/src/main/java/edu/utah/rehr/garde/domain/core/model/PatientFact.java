package edu.utah.rehr.garde.domain.core.model;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;

@Deprecated
public class PatientFact {

    String orgCodeSystem;
    String orgPatientId;
    String ehrCodeSystem;
    String ehrPatientId;
    String lastName;
    String middleName;
    String firstName;
    LocalDate dob;

    String departmentId;
    String race;
    String ethnicity;

    String ehrSexCodeSystem;
    String ehrSexCode;
    String ehrSexLabel;

    String ehrReligionCodeSystem;
    String ehrReligionCode;
    String ehrReligionLabel;

    String internalSexCodeSystem;
    String internalSexCode;
    String internalSexLabel;

    String internalReligionCodeSystem;
    String internalReligionCode;
    String internalReligionLabel;

    String canonicalSexCodeSystem;
    String canonicalSexCode;
    String canonicalSexLabel;

    String canonicalReligionCodeSystem;
    String canonicalReligionCode;
    String canonicalReligionLabel;

    private Boolean isValidKey(String ehrCodeSystem, String ehrPatientId) {
        return !StringUtils.isEmpty(ehrCodeSystem) && !StringUtils.isEmpty(ehrPatientId);
    }

    private Boolean isValidDemographicRecord(
            String orgCodeSystem, String orgPatientId
            , String ehrCodeSystem, String ehrPatientId
            , String lastName, String firstName
            , LocalDate dob
            , String internalSexCode
    ) {
        return StringUtils.isNotEmpty(orgCodeSystem)
                && StringUtils.isNotEmpty(orgPatientId)
                && StringUtils.isNotEmpty(ehrCodeSystem)
                && StringUtils.isNotEmpty(ehrPatientId)
                && StringUtils.isNotEmpty(lastName)
                && StringUtils.isNotEmpty(firstName)
                && dob != null
                && StringUtils.isNotEmpty(internalSexCode)
        ;
    }

    public PatientFact(){}

    public PatientFact(String orgCodeSystem, String orgPatientId
            , String ehrCodeSystem, String ehrPatientId
            , String lastName, String middleName, String firstName
            , LocalDate dob
            , String ehrSexCodeSystem, String ehrSexCode, String ehrSexLabel
            , String internalSexCodeSystem, String internalSexCode, String internalSexLabel
            , String ehrReligionCodeSystem, String ehrReligionCode, String ehrReligionLabel
            , String internalReligionCodeSystem, String internalReligionCode, String internalReligionLabel
    ) {
        assert isValidDemographicRecord(orgCodeSystem, orgPatientId, ehrCodeSystem, ehrPatientId, lastName, firstName, dob, internalSexCode);

        this.orgCodeSystem = orgCodeSystem;
        this.orgPatientId = orgPatientId;
        this.ehrCodeSystem = ehrCodeSystem;
        this.ehrPatientId = ehrPatientId;
        this.lastName = lastName;
        this.middleName = middleName;
        this.firstName = firstName;
        this.dob = dob;
        this.ehrSexCodeSystem = ehrSexCodeSystem;
        this.ehrSexCode = ehrSexCode;
        this.ehrSexLabel = ehrSexLabel;
        this.ehrReligionCodeSystem = ehrReligionCodeSystem;
        this.ehrReligionCode = ehrReligionCode;
        this.ehrReligionLabel = ehrReligionLabel;
        this.internalSexCodeSystem = internalSexCodeSystem;
        this.internalSexCode = internalSexCode;
        this.internalSexLabel = internalSexLabel;
        this.internalReligionCodeSystem = internalReligionCodeSystem;
        this.internalReligionCode = internalReligionCode;
        this.internalReligionLabel = internalReligionLabel;
    }

    public PatientFact(String ehrCodeSystem, String ehrPatientId
            , String ehrSexCodeSystem, String ehrSexCode, String ehrSexLabel
            , String ehrReligionCodeSystem, String ehrReligionCode, String ehrReligionLabel
            , String canonicalSexCodeSystem, String canonicalSexCode, String canonicalSexLabel
            , String canonicalReligionCodeSystem, String canonicalReligionCode, String canonicalReligionLabel) {

        assert isValidKey(ehrCodeSystem, ehrPatientId);

        this.ehrCodeSystem = ehrCodeSystem;
        this.ehrPatientId = ehrPatientId;
        this.ehrSexCodeSystem = ehrSexCodeSystem;
        this.ehrSexCode = ehrSexCode;
        this.ehrSexLabel = ehrSexLabel;
        this.ehrReligionCodeSystem = ehrReligionCodeSystem;
        this.ehrReligionCode = ehrReligionCode;
        this.ehrReligionLabel = ehrReligionLabel;
        this.canonicalSexCodeSystem = canonicalSexCodeSystem;
        this.canonicalSexCode = canonicalSexCode;
        this.canonicalSexLabel = canonicalSexLabel;
        this.canonicalReligionCodeSystem = canonicalReligionCodeSystem;
        this.canonicalReligionCode = canonicalReligionCode;
        this.canonicalReligionLabel = canonicalReligionLabel;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public String getOrgCodeSystem() {
        return orgCodeSystem;
    }

    public void setOrgCodeSystem(String orgCodeSystem) {
        this.orgCodeSystem = orgCodeSystem;
    }

    public String getOrgPatientId() {
        return orgPatientId;
    }

    public void setOrgPatientId(String orgPatientId) {
        this.orgPatientId = orgPatientId;
    }

    public String getEhrCodeSystem() {
        return ehrCodeSystem;
    }

    public void setEhrCodeSystem(String ehrCodeSystem) {
        this.ehrCodeSystem = ehrCodeSystem;
    }

    public String getEhrPatientId() {
        return ehrPatientId;
    }

    public void setEhrPatientId(String ehrPatientId) {
        this.ehrPatientId = ehrPatientId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public String getEhrSexCodeSystem() {
        return ehrSexCodeSystem;
    }

    public void setEhrSexCodeSystem(String ehrSexCodeSystem) {
        this.ehrSexCodeSystem = ehrSexCodeSystem;
    }

    public String getEhrSexCode() {
        return ehrSexCode;
    }

    public void setEhrSexCode(String ehrSexCode) {
        this.ehrSexCode = ehrSexCode;
    }

    public String getEhrSexLabel() {
        return ehrSexLabel;
    }

    public void setEhrSexLabel(String ehrSexLabel) {
        this.ehrSexLabel = ehrSexLabel;
    }

    public String getEhrReligionCodeSystem() {
        return ehrReligionCodeSystem;
    }

    public void setEhrReligionCodeSystem(String ehrReligionCodeSystem) {
        this.ehrReligionCodeSystem = ehrReligionCodeSystem;
    }

    public String getEhrReligionCode() {
        return ehrReligionCode;
    }

    public void setEhrReligionCode(String ehrReligionCode) {
        this.ehrReligionCode = ehrReligionCode;
    }

    public String getEhrReligionLabel() {
        return ehrReligionLabel;
    }

    public void setEhrReligionLabel(String ehrReligionLabel) {
        this.ehrReligionLabel = ehrReligionLabel;
    }

    public String getCanonicalSexCodeSystem() {
        return canonicalSexCodeSystem;
    }

    public void setCanonicalSexCodeSystem(String canonicalSexCodeSystem) {
        this.canonicalSexCodeSystem = canonicalSexCodeSystem;
    }

    public String getCanonicalSexCode() {
        return canonicalSexCode;
    }

    public void setCanonicalSexCode(String canonicalSexCode) {
        this.canonicalSexCode = canonicalSexCode;
    }

    public String getCanonicalSexLabel() {
        return canonicalSexLabel;
    }

    public void setCanonicalSexLabel(String canonicalSexLabel) {
        this.canonicalSexLabel = canonicalSexLabel;
    }

    public String getCanonicalReligionCodeSystem() {
        return canonicalReligionCodeSystem;
    }

    public void setCanonicalReligionCodeSystem(String canonicalReligionCodeSystem) {
        this.canonicalReligionCodeSystem = canonicalReligionCodeSystem;
    }

    public String getCanonicalReligionCode() {
        return canonicalReligionCode;
    }

    public void setCanonicalReligionCode(String canonicalReligionCode) {
        this.canonicalReligionCode = canonicalReligionCode;
    }

    public String getCanonicalReligionLabel() {
        return canonicalReligionLabel;
    }

    public void setCanonicalReligionLabel(String canonicalReligionLabel) {
        this.canonicalReligionLabel = canonicalReligionLabel;
    }

    public String getInternalSexCodeSystem() {
        return internalSexCodeSystem;
    }

    public void setInternalSexCodeSystem(String internalSexCodeSystem) {
        this.internalSexCodeSystem = internalSexCodeSystem;
    }

    public String getInternalSexCode() {
        return internalSexCode;
    }

    public void setInternalSexCode(String internalSexCode) {
        this.internalSexCode = internalSexCode;
    }

    public String getInternalSexLabel() {
        return internalSexLabel;
    }

    public void setInternalSexLabel(String internalSexLabel) {
        this.internalSexLabel = internalSexLabel;
    }

    public String getInternalReligionCodeSystem() {
        return internalReligionCodeSystem;
    }

    public void setInternalReligionCodeSystem(String internalReligionCodeSystem) {
        this.internalReligionCodeSystem = internalReligionCodeSystem;
    }

    public String getInternalReligionCode() {
        return internalReligionCode;
    }

    public void setInternalReligionCode(String internalReligionCode) {
        this.internalReligionCode = internalReligionCode;
    }

    public String getInternalReligionLabel() {
        return internalReligionLabel;
    }

    public void setInternalReligionLabel(String internalReligionLabel) {
        this.internalReligionLabel = internalReligionLabel;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj == null || !(obj instanceof PatientFact comparator)) {
            return false;
        }

        return this.ehrCodeSystem.equals(comparator.getEhrCodeSystem())
                && this.ehrPatientId.equals(comparator.getEhrPatientId());
    }

    @Override
    public int hashCode() {
        return this.ehrCodeSystem.hashCode() * 13
                + this.ehrPatientId.hashCode() * 97;
    }
}



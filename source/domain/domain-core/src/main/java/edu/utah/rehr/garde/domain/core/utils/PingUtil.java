package edu.utah.rehr.garde.domain.core.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.util.logging.Logger;

public class PingUtil {

    private static final Logger log = Logger.getLogger(PingUtil.class.getName());

    public static Boolean isReachable(String dbAddress, Integer port) {

        try {
            InetAddress inetAddress = InetAddress.getByName(dbAddress);
            if (inetAddress.isReachable(port)) {
                log.info(dbAddress + ":" + port + " IS REACHABLE.");
                return true;
            }

        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            log.warning(sw.toString());
        }

        log.info(dbAddress + ":" + port + " IS *** NOT *** REACHABLE.");
        return false;
    }

}

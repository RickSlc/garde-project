package edu.utah.rehr.garde.domain.core.utils;

import com.ibm.icu.text.CharsetDetector;
import com.ibm.icu.text.CharsetMatch;
import lombok.NonNull;
import lombok.extern.java.Log;
import org.springframework.core.io.ClassPathResource;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.logging.Logger;

@Log
public class FileReaderUtil {

    public static String detectCharset(@NonNull String filePath) {
        int MAX_CHARS_TO_READ = 10000;

        byte[] buffer = new byte[MAX_CHARS_TO_READ];

        try (InputStream inputStream = new FileInputStream(filePath)) {

            int bytesRead = inputStream.read(buffer, 0, MAX_CHARS_TO_READ);

            // Check for BOM
            String bomCharsetName = detectBOM(buffer);
            if (bomCharsetName != null) {
                return bomCharsetName;
            }

            if (bytesRead == -1) {
                log.severe("ERROR: empty or unreadable file");
                return null;
            }

            CharsetDetector detector = new CharsetDetector();
            detector.setText(buffer);
            CharsetMatch match = detector.detect();

            if (match != null) {
                return match.getName();
            } else {
                log.severe("ERROR: Unrecognized Charset in " + filePath);
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            log.severe("ERROR: " + filePath + " not readable.");
            return null;
        }
    }

    private static String detectBOM(byte[] charBytes) throws IOException {

        if (charBytes.length >= 3) {
            // Check for UTF-8 BOM
            if ((charBytes[0] & 0xFF) == 0xEF && (charBytes[1] & 0xFF) == 0xBB && (charBytes[2] & 0xFF) == 0xBF) {
                return "UTF-8 BOM";
            }
        }
        return null;
    }

    public static Reader getReader(String filePath) {

        if (filePath.contains("classpath:")) {
            String file = filePath.replace("classpath:", "");
            ClassPathResource classPathResource = new ClassPathResource(file);

            Logger.getLogger(FileReaderUtil.class.getName()).info("RETRIEVE FILE FROM CLASSPATH: " + file);

            try {
                InputStream is = classPathResource.getInputStream();
                return new InputStreamReader(is);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        try {
            return new FileReader(filePath);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getStringFromFile(String fileName) throws IOException {

        Reader reader = getReader(fileName);
        StringBuilder builder = new StringBuilder();

        try (BufferedReader buffReader = new BufferedReader(reader)) {
            String line;
            while ((line = buffReader.readLine()) != null) {
                builder.append(line).append("\n");
            }
        }
        return builder.toString();
    }

}

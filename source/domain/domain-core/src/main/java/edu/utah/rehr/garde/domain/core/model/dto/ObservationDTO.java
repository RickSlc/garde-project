package edu.utah.rehr.garde.domain.core.model.dto;

import edu.utah.rehr.garde.domain.core.model.annotation.CsvHeader;
import edu.utah.rehr.garde.domain.core.terminology.Code;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.java.Log;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Log
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ObservationDTO extends BasePatientDTO {

    @NonNull
    @CsvHeader(name = "EHR_OBS_ID_CODE_SYSTEM", required = true)
    private String observationIdCodeSystem;
    @NonNull
    @CsvHeader(name = "EHR_OBS_ID", required = true)
    private String observationId;

    @CsvHeader(name = "EHR_ENC_ID_CODE_SYSTEM")
    private String encounterIdCodeSystem;
    @CsvHeader(name = "EHR_ENC_ID")
    private String encounterId;

    @NonNull
    @CsvHeader(name = "EHR_OBS_DATE_TIME")
    private String observationDateTimeString;

    public @NonNull LocalDateTime getObservationDateTime() {
        if (observationDateTimeString.length() <= 10) {
            return LocalDate.parse(observationDateTimeString, DateTimeFormatter.ISO_DATE).atStartOfDay();
        }
        return LocalDateTime.parse(observationDateTimeString, DateTimeFormatter.ISO_DATE_TIME);
    }

    @NonNull
    @CsvHeader(name = "EHR_OBS_TYPE_CODE_SYSTEM",required = true)
    private String observationTypeCodeSystem;
    @NonNull
    @CsvHeader(name = "EHR_OBS_TYPE", required = true)
    private String observationType;

    @NonNull
    @CsvHeader(name = "EHR_OBS_CODE_SYSTEM", required = true)
    private String observationCodeSystem;
    @NonNull
    @CsvHeader(name = "EHR_OBS_CODE", required = true)
    private String observationCode;
    @NonNull
    @CsvHeader(name = "EHR_OBS_LABEL")
    private String observationLabel;

    public Code toObservationCode() {
        return new Code(
                observationCodeSystem
                , observationCode
                , observationLabel
        );
    }

    @NonNull
    @CsvHeader(name = "EHR_OBS_VALUE")
    private String observationValue;

    public Double getObservationValueDouble() {
        return Double.parseDouble(observationValue);
    }

    public Integer getObservationValueInteger() {

        if (observationValue.contains(".")) {
            log.warning("Converting a decimal value " + observationValue +" into an Integer. May lose precision.");
            return (int) Double.parseDouble(observationValue);
        }

        try {
            return Integer.parseInt(observationValue);
        } catch (NumberFormatException e) {
            log.warning(observationValue + " CAN'T BE CONVERTED TO AN INTEGER");
        }

        return null;
    }

    @CsvHeader(name = "EHR_OBS_VALUE_UNITS")
    private String observationValueUnits;

}

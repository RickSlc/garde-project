package edu.utah.rehr.garde.domain.core.provider;

import edu.utah.rehr.garde.domain.core.model.annotation.CsvHeader;
import edu.utah.rehr.garde.domain.core.model.dto.BasePatientDTO;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import java.io.FileReader;
import java.lang.reflect.Field;
import java.nio.file.Paths;
import java.text.Normalizer;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DtoProvider {

    private static final Logger LOGGER = Logger.getLogger(DtoProvider.class.getName());

    public static <T> Map<String, List<T>> csvToDtoMap(String filename, Class<T> clazz) throws Exception {
        List<T> list = csvToDto(filename, clazz);

        Map<String, List<T>> map = new HashMap<>();
        for(T dto : list) {
            if (dto instanceof BasePatientDTO) {
                String patientId = ((BasePatientDTO) dto).getPatientId();
                map.computeIfAbsent(patientId, k -> new java.util.ArrayList<>()).add(dto);
            } else {
                throw new IllegalArgumentException(clazz.getName() + " is not an instance of BaseDTO");
            }
        }
        return map;
    }

    public static <T> List<T> csvToDto(String filename, Class<T> clazz) throws Exception {

        int ERROR_TOLERANCE = 50;
        int errorCount = 0;

        List<T> records = new ArrayList<>();

        try (FileReader reader = new FileReader(Paths.get(filename).toFile());
             CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.builder().setHeader().setCommentMarker('#').build())) {

            // Retrieve CSV headers and standardize them
            Map<String, String> headerMap = new HashMap<>();
            for (String header : csvParser.getHeaderMap().keySet()) {
                headerMap.put(standardizeName(header), header);
            }

            int inputFileLineNumber = 0;
            for (CSVRecord csvRecord : csvParser) {
                inputFileLineNumber++;
                T dto = clazz.getDeclaredConstructor().newInstance();

                List<Field> fields = new ArrayList<>();
                Class<?> current = clazz;
                while (current != null && current != Object.class) {
                    fields.addAll(List.of(current.getDeclaredFields()));
                    current = current.getSuperclass();
                }

                for (Field field : fields) {

                    if (field.isAnnotationPresent(CsvHeader.class)) {
                        CsvHeader annotation = field.getAnnotation(CsvHeader.class);
                        String headerName = annotation.name();
                        String standardizedHeader = standardizeName(headerName);
                        String originalHeader = headerMap.get(standardizedHeader);

                        // Check if required fields are missing values
                        if (annotation.required()
                                && headerMap.containsKey(standardizedHeader)
                                && StringUtils.isEmpty(csvRecord.get(originalHeader))
                        ) {
                            throw new IllegalArgumentException("Required field '" + headerName + "' is missing a value on line " + inputFileLineNumber + " in CSV file");
                        }
                        // Check if required fields are missing
                        else if (annotation.required()
                                && !headerMap.containsKey(standardizedHeader)
                        ) {
                            throw new IllegalArgumentException("Required field '" + headerName + "' is missing in CSV file");
                        }

                        // Retrieve and set the field value if header is present
                        if (originalHeader != null && csvRecord.isMapped(originalHeader)) {
                            String value = csvRecord.get(originalHeader);
                            if (
                                    (standardizedHeader.contains("date")
                                            || standardizedHeader.contains("dt"))
                                            && !standardizedHeader.contains("time")
                                            && !standardizedHeader.contains("tm")
                                            && !standardizedHeader.contains("dts") // common db abbrev for date timestamp

                            ) {
                                try {
                                    validateDateFormat(value, headerName, DateTimeFormatter.ISO_LOCAL_DATE);
                                } catch (IllegalArgumentException e) {
                                    errorCount++;
                                    if (errorCount > ERROR_TOLERANCE && annotation.required()) {
                                        LOGGER.log(Level.WARNING, "Over " + ERROR_TOLERANCE + " invalid date formats for field '" + headerName + "': " + value);
                                        throw e;
                                    }
                                    LOGGER.log(Level.WARNING, "Invalid date format for field '" + headerName + "': " + value);
                                }

                            } else if (standardizedHeader.contains("time")
                                    || standardizedHeader.contains("tm")
                                    || standardizedHeader.contains("dts")
                            ) {
                                try {
                                    validateDateFormat(value, headerName, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
                                } catch (IllegalArgumentException e) {
                                    errorCount++;
                                    if (errorCount > ERROR_TOLERANCE && annotation.required()) {
                                        LOGGER.log(Level.WARNING, "Over " + ERROR_TOLERANCE + " invalid date formats for field '" + headerName + "': " + value);
                                        throw e;
                                    }
                                    LOGGER.log(Level.WARNING, "Invalid date format for field '" + headerName + "': " + value);
                                }
                            }
                            field.setAccessible(true);
                            field.set(dto, value);
                        }
                    }
                }
                records.add(dto);
            }
        }
        return records;
    }

    private static void validateDateFormat(String dateStr, String fieldName, DateTimeFormatter formatter) {

        try {
            LocalDate.parse(dateStr, formatter);
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException("Invalid format for field '" + fieldName + "': " + dateStr);
        }
    }

    private static String standardizeName(String name) {
        if (name == null) {
            return null;
        }
        return Normalizer.normalize(name, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "")
                .toLowerCase()
                .replace("_", "")
                .replace("-", "")
                .replace(" ", "")
                .replaceAll("(.)([A-Z])", "$1$2").toLowerCase();
    }
}
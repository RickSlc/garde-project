package edu.utah.rehr.garde.domain.core.model.fhir.r4;
import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxDTO;
import edu.utah.rehr.garde.domain.core.model.dto.ObservationDTO;
import edu.utah.rehr.garde.domain.core.model.dto.PatientDTO;
import edu.utah.rehr.garde.domain.core.terminology.Code;
import edu.utah.rehr.garde.domain.core.terminology.binding.NccnTerminologyBinding;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import edu.utah.rehr.garde.domain.core.utils.DateUtil;
import org.hl7.fhir.r4.model.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class GDtoToFhirMapperR4 {

    public static FamilyMemberHistory dtoToFamilyMemberHistory(FamilyHxDTO dto) {
        FamilyMemberHistory familyMemberHistory = new FamilyMemberHistory();

        // Set identifier
        familyMemberHistory.addIdentifier(new Identifier()
                .setSystem(dto.getFamilyHxIdCodeSystem())
                .setValue(dto.getFamilyHxId()));

        // Set patient
        familyMemberHistory.setPatient(new Reference()
                .setIdentifier(new Identifier()
                        .setSystem(dto.getPatientIdCodeSystem())
                        .setValue(dto.getPatientId())));

        // Set date
        if (dto.getEncounterDate() != null) {
            LocalDateTime encDate = dto.getEncounterDate();
            familyMemberHistory.setDate(DateUtil.toDate(encDate));
        }

        // Set relationship
        familyMemberHistory.setRelationship(new CodeableConcept().addCoding(new Coding()
                .setSystem(dto.getRelationshipCodeSystem())
                .setCode(dto.getRelationshipCode())
                .setDisplay(dto.getRelationshipLabel())));

        // Set condition
        FamilyMemberHistory.FamilyMemberHistoryConditionComponent condition = new FamilyMemberHistory.FamilyMemberHistoryConditionComponent();
        condition.setCode(new CodeableConcept().addCoding(new Coding()
                .setSystem(dto.getConditionCodeSystem())
                .setCode(dto.getConditionCode())
                .setDisplay(dto.getConditionLabel())));

        if (dto.getConditionOnsetAge() != null) {
                Age age = new Age();
                age.setValue(dto.getConditionOnsetAge());
                condition.setOnset(age);
        }

        familyMemberHistory.addCondition(condition);

        if (dto.getComments() != null) {
            Annotation annotation = new Annotation();
            annotation.setText(dto.getComments());
            List<Annotation> annotations = new ArrayList<>();
            annotations.add(annotation);
            familyMemberHistory.setNote(annotations);
        }

        return familyMemberHistory;
    }

    public static Patient dtoToPatient(PatientDTO dto, ICodeService codeService) {
        Patient patient = new Patient();

        patient.addIdentifier()
                .setSystem(dto.getPatientIdCodeSystem())
                .setValue(dto.getPatientId());

        patient.setId(dto.getPatientId());
        patient.setBirthDate(DateUtil.toDate(dto.getBirthDate()));

        Code sexCode = dto.toSexCode();

        if (codeService.isSetMember(NccnTerminologyBinding.FEMALE_CODE, sexCode)) {
            patient.setGender(Enumerations.AdministrativeGender.FEMALE);
        }
        else if (codeService.isSetMember(NccnTerminologyBinding.MALE_CODE, sexCode)) {
            patient.setGender(Enumerations.AdministrativeGender.MALE);
        }
        else if (codeService.isSetMember(NccnTerminologyBinding.OTHER_CODE, sexCode)) {
            patient.setGender(Enumerations.AdministrativeGender.OTHER);
        }
        else {
            patient.setGender(Enumerations.AdministrativeGender.UNKNOWN);
        }

        // Map other fields as needed
        patient.addExtension()
                .setUrl("http://utah.edu/rehr/StructureDefinition/race")
                .setValue(new CodeableConcept().addCoding(
                        new Coding()
                                .setSystem(dto.getRaceCodeSystem())
                                .setCode(dto.getRaceCode())
                                .setDisplay(dto.getRaceLabel())
                ));

        patient.addExtension()
                .setUrl("http://utah.edu/rehr/StructureDefinition/ethnicity")
                .setValue(new CodeableConcept().addCoding(
                        new Coding()
                                .setSystem(dto.getEthnicityCodeSystem())
                                .setCode(dto.getEthnicityCode())
                                .setDisplay(dto.getEthnicityLabel())
                ));

        patient.addCommunication()
                .setLanguage(new CodeableConcept().addCoding(
                        new Coding()
                                .setSystem(dto.getLangCodeSystem())
                                .setCode(dto.getLangCode())
                                .setDisplay(dto.getLangLabel())
                ));

        patient.setMaritalStatus(new CodeableConcept().addCoding(
                new Coding()
                        .setSystem(dto.getMaritalStatusCodeSystem())
                        .setCode(dto.getMaritalStatusCode())
                        .setDisplay(dto.getMaritalStatusLabel())
        ));

        return patient;
    }

    public static Observation dtoToObservation(ObservationDTO dto) {
        Observation observation = new Observation();

        // Set identifier
        observation.addIdentifier(new Identifier()
                .setSystem(dto.getObservationIdCodeSystem())
                .setValue(dto.getObservationId()));

        // Set patient reference
        observation.setSubject(new Reference()
                .setIdentifier(new Identifier()
                        .setSystem(dto.getPatientIdCodeSystem())
                        .setValue(dto.getPatientId())));

        // Set encounter reference
        if (dto.getEncounterIdCodeSystem() != null && dto.getEncounterId() != null) {
            observation.setEncounter(new Reference()
                    .setIdentifier(new Identifier()
                            .setSystem(dto.getEncounterIdCodeSystem())
                            .setValue(dto.getEncounterId())));
        }

        // Set effective date/time
        observation.setEffective(new DateTimeType(DateUtil.toDate(dto.getObservationDateTime())));

        // Set observation type (category)
        observation.addCategory(new CodeableConcept().addCoding(new Coding()
                .setSystem(dto.getObservationTypeCodeSystem())
                .setCode(dto.getObservationType())
                .setDisplay(dto.getObservationType())));

        observation.setStatus(Observation.ObservationStatus.FINAL);

        // Set code
        observation.setCode(new CodeableConcept().addCoding(new Coding()
                .setSystem(dto.getObservationCodeSystem())
                .setCode(dto.getObservationCode())
                .setDisplay(dto.getObservationLabel())));

        // Set value
        try {
            Quantity quantity = getQuantity(dto);
            observation.setValue(quantity);
        } catch (NumberFormatException e) {
            // If not a number, set as a string
            observation.setValue(new StringType(dto.getObservationValue()));
        }

        return observation;
    }

    private static Quantity getQuantity(ObservationDTO dto) {
        Quantity quantity = new Quantity();
        // Attempt to parse as a numeric value
        String observationValue = dto.getObservationValue();
        if (observationValue.contains(".")) {
            quantity.setValue(Double.parseDouble(observationValue));
        } else {
            quantity.setValue(Integer.parseInt(observationValue));
        }
        if (dto.getObservationValueUnits() != null) {
            quantity.setUnit(dto.getObservationValueUnits());
        }
        return quantity;
    }
}

package edu.utah.rehr.garde.domain.core.model.dto;

import com.google.common.base.Objects;
import edu.utah.rehr.garde.domain.core.model.annotation.CsvHeader;
import edu.utah.rehr.garde.domain.core.terminology.Code;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(force = true)
public class FamilyHxDTO extends BasePatientDTO {

    @CsvHeader(name = "EHR_FAMILY_HX_ID_CODE_SYSTEM", required = true)
    private String familyHxIdCodeSystem;
    @CsvHeader(name = "EHR_FAMILY_HX_ID", required = true)
    private String familyHxId;

    @EqualsAndHashCode.Exclude // fhir FamilyMemberHistory doesn't have encounter. GARDE uses an alternative strategy to code it into a composite key, familyHxId, without code system
    @CsvHeader(name = "EHR_ENC_ID_CODE_SYSTEM")
    private String encounterIdCodeSystem;
    @CsvHeader(name = "EHR_ENC_ID")
    private String encounterId;
    @CsvHeader(name = "EHR_REC_ID")
    private String recordId;
    @CsvHeader(name = "EHR_REC_DATE")
//    @Setter
    private String encounterDateString;

    private static final DateTimeFormatter CUSTOM_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public LocalDateTime getEncounterDate() {
        if (StringUtils.isNotEmpty(encounterDateString)) {
            if (encounterDateString.length() <= 10) {
                return LocalDate.parse(encounterDateString, DateTimeFormatter.ISO_LOCAL_DATE).atStartOfDay();
            }
            try {
                return LocalDateTime.parse(encounterDateString, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
            } catch (DateTimeParseException e) {
                try {
                    return LocalDateTime.parse(encounterDateString, CUSTOM_DATE_TIME_FORMATTER);
                } catch (DateTimeParseException ex) {
                    throw new RuntimeException("ENCOUNTER DATE TIME FORMAT SUPPORTED: " + encounterDateString);
                }
            }
        }
        return null;    }

    @CsvHeader(name = "EHR_COND_CODE_SYSTEM", required = true)
    private String conditionCodeSystem;
    @CsvHeader(name = "EHR_COND_CODE", required = true)
    private String conditionCode;
    @CsvHeader(name = "EHR_COND_LABEL")
    private String conditionLabel;

    public Code toConditionCode() {
        if (StringUtils.isEmpty(conditionCodeSystem) || StringUtils.isEmpty(conditionCode)) {
            return null;
        } else {
            return new Code(
                    conditionCodeSystem
                    , conditionCode
                    , conditionLabel
            );
        }
    }

    @CsvHeader(name = "EHR_COND_ONSET_AGE")
    private String conditionOnsetAgeString;

    public Integer getConditionOnsetAge() {
        if (StringUtils.isNotEmpty(conditionOnsetAgeString)) {
            return (int) Double.parseDouble(conditionOnsetAgeString);
        }
        return null;
    }

    @CsvHeader(name = "EHR_RELATION_CODE_SYSTEM", required = true)
    private String relationshipCodeSystem;
    @CsvHeader(name = "EHR_RELATION_CODE", required = true)
    private String relationshipCode;
    @CsvHeader(name = "EHR_RELATION_LABEL")
    private String relationshipLabel;

    public Code toRelationCode() {
        if (StringUtils.isEmpty(relationshipCodeSystem) || StringUtils.isEmpty(relationshipCode)) {
            return null;
        } else {
            return new Code(
                    relationshipCodeSystem
                    , relationshipCode
                    , relationshipLabel
            );
        }
    }

    @CsvHeader(name = "EHR_COMMENTS")
    private String comments;

    String toStatement() {
        StringBuilder statement = new StringBuilder();
        statement.append(relationshipLabel).append(" had ").append(conditionLabel);
        if (StringUtils.isNotEmpty(conditionOnsetAgeString)) {
            statement.append(" at ").append(conditionOnsetAgeString);
        }
        return statement.toString();
    }

    // In the case where NLP finds/corrects field values, the source is described in these fields
    private String conditionSource;
    private String relationshipSource;
    private String ageSource;

/*
    public FamilyHxDTO(String familyHxIdCodeSystem, String familyHxId, String patientIdCodeSystem, String patientId
            , String encounterIdCodeSystem, String encounterId
            , String recordId, String conditionCodeSystem, String conditionCode, String conditionLabel
            , String relationshipCodeSystem, String relationshipCode
            , String relationshipLabel, String comments) {
        if (StringUtils.isEmpty(familyHxIdCodeSystem))
            throw new IllegalArgumentException("familyHxIdCodeSystem cannot be null or empty");
        if (StringUtils.isEmpty(familyHxId))
            throw new IllegalArgumentException("familyHxId cannot be null or empty");
        if (StringUtils.isEmpty(patientIdCodeSystem))
            throw new IllegalArgumentException("patientIdCodeSystem cannot be null or empty");
        if (StringUtils.isEmpty(patientId))
            throw new IllegalArgumentException("patientId cannot be null or empty");
        if (StringUtils.isEmpty(relationshipCodeSystem))
            throw new IllegalArgumentException("relationshipCodeSystem cannot be null or empty");
        if (StringUtils.isEmpty(relationshipCode))
            throw new IllegalArgumentException("relationshipCode cannot be null or empty");
        if (StringUtils.isEmpty(conditionCodeSystem))
            throw new IllegalArgumentException("conditionCodeSystem cannot be null or empty");
        if (StringUtils.isEmpty(conditionCode))
            throw new IllegalArgumentException("conditionCode cannot be null or empty");

        this.familyHxIdCodeSystem = familyHxIdCodeSystem;
        this.familyHxId = familyHxId;
        this.patientIdCodeSystem = patientIdCodeSystem;
        this.patientId = patientId;
        this.encounterIdCodeSystem = encounterIdCodeSystem;
        this.encounterId = encounterId;
        this.recordId = recordId;
        this.conditionCodeSystem = conditionCodeSystem;
        this.conditionCode = conditionCode;
        this.conditionLabel = conditionLabel;
        this.relationshipCodeSystem = relationshipCodeSystem;
        this.relationshipCode = relationshipCode;
        this.relationshipLabel = relationshipLabel;
        this.comments = comments;
    }

    public FamilyHxDTO(String familyHxIdCodeSystem, String familyHxId, String patientIdCodeSystem, String patientId
            , String conditionCodeSystem, String conditionCode, String conditionOnsetAge, String relationshipCodeSystem
            , String relationshipCode) {

        if (StringUtils.isEmpty(familyHxIdCodeSystem))
            throw new IllegalArgumentException("familyHxIdCodeSystem cannot be null or empty");
        if (StringUtils.isEmpty(familyHxId))
            throw new IllegalArgumentException("familyHxId cannot be null or empty");
        if (StringUtils.isEmpty(patientIdCodeSystem))
            throw new IllegalArgumentException("patientIdCodeSystem cannot be null or empty");
        if (StringUtils.isEmpty(patientId))
            throw new IllegalArgumentException("patientId cannot be null or empty");
        if (StringUtils.isEmpty(relationshipCodeSystem))
            throw new IllegalArgumentException("relationshipCodeSystem cannot be null or empty");
        if (StringUtils.isEmpty(relationshipCode))
            throw new IllegalArgumentException("relationshipCode cannot be null or empty");
        if (StringUtils.isEmpty(conditionCodeSystem))
            throw new IllegalArgumentException("conditionCodeSystem cannot be null or empty");
        if (StringUtils.isEmpty(conditionCode))
            throw new IllegalArgumentException("conditionCode cannot be null or empty");

        this.patientIdCodeSystem = patientIdCodeSystem;
        this.patientId = patientId;
        this.conditionCodeSystem = conditionCodeSystem;
        this.conditionCode = conditionCode;
        this.conditionOnsetAgeString = conditionOnsetAge;
        this.relationshipCodeSystem = relationshipCodeSystem;
        this.relationshipCode = relationshipCode;
    }
*/
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        FamilyHxDTO that = (FamilyHxDTO) o;
//        return Objects.equal(familyHxIdCodeSystem, that.familyHxIdCodeSystem) && Objects.equal(familyHxId, that.familyHxId) && Objects.equal(getPatientIdCodeSystem(), that.getPatientIdCodeSystem()) && Objects.equal(getPatientId(), that.getPatientId()) && Objects.equal(conditionCodeSystem, that.conditionCodeSystem) && Objects.equal(conditionCode, that.conditionCode) && Objects.equal(relationshipCodeSystem, that.relationshipCodeSystem) && Objects.equal(relationshipCode, that.relationshipCode);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hashCode(familyHxIdCodeSystem, familyHxId, getPatientIdCodeSystem(), getPatientId(), conditionCodeSystem, conditionCode, relationshipCodeSystem, relationshipCode);
//    }
}



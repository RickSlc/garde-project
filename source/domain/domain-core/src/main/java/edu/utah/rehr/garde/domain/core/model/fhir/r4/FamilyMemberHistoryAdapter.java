package edu.utah.rehr.garde.domain.core.model.fhir.r4;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import org.hl7.fhir.r4.model.FamilyMemberHistory;

import java.io.IOException;

public class FamilyMemberHistoryAdapter extends TypeAdapter<FamilyMemberHistory> {

    @Override
    public void write(JsonWriter out, FamilyMemberHistory value) throws IOException {
        // Custom serialization logic (simplified example)
        out.beginObject();
        out.name("id").value(value.getId());
        // Add more fields here as needed
        out.endObject();
    }

    @Override
    public FamilyMemberHistory read(JsonReader in) throws IOException {
        // Custom deserialization logic (simplified example)
        FamilyMemberHistory fhx = new FamilyMemberHistory();
        in.beginObject();
        while (in.hasNext()) {
            switch (in.nextName()) {
                case "id":
                    fhx.setId(in.nextString());
                    break;
                // Add more fields here as needed
                default:
                    in.skipValue();
                    break;
            }
        }
        in.endObject();
        return fhx;
    }
}
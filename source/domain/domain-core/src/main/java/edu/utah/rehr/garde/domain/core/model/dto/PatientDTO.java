package edu.utah.rehr.garde.domain.core.model.dto;

import com.google.common.base.Objects;
import edu.utah.rehr.garde.domain.core.model.annotation.CsvHeader;
import edu.utah.rehr.garde.domain.core.terminology.Code;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Data
@EqualsAndHashCode(callSuper = true)
public class PatientDTO extends BasePatientDTO {

    @NotNull
    @Setter
    @CsvHeader(name = "BIRTH_DATE", required = true)
    private String birthDateString;

    public LocalDate getBirthDate() {
        return LocalDate.parse(birthDateString,DateTimeFormatter.ISO_DATE);
    }

    @CsvHeader(name = "ADDRESS1")
    private String address1;

    @CsvHeader(name = "ADDRESS2")
    private String address2;

    @CsvHeader(name = "CITY")
    private String city;

    @CsvHeader(name = "STATE")
    private String state;

    @CsvHeader(name = "ZIP")
    private String zip;

    @NotEmpty
    @EqualsAndHashCode.Exclude
    @CsvHeader(name = "EHR_SEX_CODE_SYSTEM", required = true)
    private String sexCodeSystem;

    @NotEmpty
    @CsvHeader(name = "EHR_SEX_CODE", required = true)
    private String sexCode;

    @EqualsAndHashCode.Exclude
    @CsvHeader(name = "EHR_SEX_LABEL")
    private String sexLabel;

    @CsvHeader(name = "EHR_MARITAL_STATUS_CODE_SYSTEM")
    private String maritalStatusCodeSystem;

    @CsvHeader(name = "EHR_MARITAL_STATUS_CODE")
    private String maritalStatusCode;

    @CsvHeader(name = "EHR_MARITAL_STATUS_LABEL")
    private String maritalStatusLabel;

    @CsvHeader(name = "EHR_LANGUAGE_CODE_SYSTEM")
    private String langCodeSystem;

    @CsvHeader(name = "EHR_LANGUAGE_CODE")
    private String langCode;

    @CsvHeader(name = "EHR_LANGUAGE_LABEL")
    private String langLabel;

    @CsvHeader(name = "EHR_RACE_CODE_SYSTEM")
    private String raceCodeSystem;

    @CsvHeader(name = "EHR_RACE_CODE")
    private String raceCode;

    @CsvHeader(name = "EHR_RACE_LABEL")
    private String raceLabel;

    @CsvHeader(name = "EHR_ETHNICITY_CODE_SYSTEM")
    private String ethnicityCodeSystem;

    @CsvHeader(name = "EHR_ETHNICITY_CODE")
    private String ethnicityCode;

    @CsvHeader(name = "EHR_ETHNICITY_LABEL")
    private String ethnicityLabel;

    @CsvHeader(name = "EHR_RELIGION_CODE_SYSTEM")
    private String religionCodeSystem;

    @CsvHeader(name = "EHR_RELIGION_CODE")
    private String religionCode;

    @CsvHeader(name = "EHR_RELIGION_LABEL")
    private String religionLabel;

    @CsvHeader(name = "PREVIOUSLY_MET_CRITERIA_CODES")
    private String[] previouslyMetCriteriaCodes;

    public String getPreviouslyMetCriteriaCodesDelimitedString(String delimiter) {
        if (previouslyMetCriteriaCodes != null) {
            return String.join(delimiter, previouslyMetCriteriaCodes);
        }
        return null;
    }

    public Code toSexCode() {
        if (StringUtils.isEmpty(sexCodeSystem) || StringUtils.isEmpty(sexCode)) {
            return null;
        }
        return new Code(
                sexCodeSystem
                , sexCode
                , sexLabel
        );
    }

    public Code toRaceCode() {
        if (StringUtils.isEmpty(raceCodeSystem) || StringUtils.isEmpty(raceCode)) {
            return null;
        }
        return new Code(
                raceCodeSystem
                , raceCode
                , raceLabel
        );
    }

    public Code toReligionCode() {
        if (StringUtils.isEmpty(religionCodeSystem) || StringUtils.isEmpty(religionCode)) {
            return null;
        }
        return new Code(
                religionCodeSystem
                , religionCode
                , religionLabel
        );
    }

    public Code toEthnicityCode() {
        if (StringUtils.isEmpty(ethnicityCodeSystem) || StringUtils.isEmpty(ethnicityCode)) {
            return null;
        }
        return new Code (
                ethnicityCodeSystem
                , ethnicityCode
                , ethnicityLabel
        );
    }

    public Code toMaritalStatusCode() {
        if (StringUtils.isEmpty(maritalStatusCodeSystem) || StringUtils.isEmpty(maritalStatusCode)) {
            return null;
        }
        return new Code(
                maritalStatusCodeSystem
                , maritalStatusCode
                , maritalStatusLabel
        );
    }

    public String toString() {
        String patientDtoString = "patientIdCodeSystem: " + getPatientIdCodeSystem() + "\n" +
                "patientId: " + getPatientId() + "\n" +
                "birthDate: " + getBirthDateString() + "\n" +
                "sex" + toSexCode().toString() + "\n";

        return patientDtoString;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        PatientDTO that = (PatientDTO) o;
//        return Objects.equal(patientIdCodeSystem, that.patientIdCodeSystem)
//                && Objects.equal(patientId, that.patientId);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hashCode(patientIdCodeSystem, patientId);
//    }
}

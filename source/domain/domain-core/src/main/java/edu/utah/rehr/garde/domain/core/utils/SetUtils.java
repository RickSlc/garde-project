package edu.utah.rehr.garde.domain.core.utils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class SetUtils {

    public static Set<String> convertDelimitedStringToSet(String str, String delimiter) {
        return new HashSet<>(Arrays.asList(str.split(delimiter)));
    }

    public static <T> boolean setsAreEqual(Set<T> set1, Set<T> set2) {
        if (set1 == null || set2 == null) {
            return false;
        }

        if (set1.size() != set2.size()) {
            return false;
        }

        return set1.containsAll(set2);
    }
}

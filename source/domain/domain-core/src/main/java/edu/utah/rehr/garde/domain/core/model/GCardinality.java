package edu.utah.rehr.garde.domain.core.model;

public enum GCardinality {

    ZeroOrOne,
    ZeroOrMore,
    One,
    OneOrMore

}

package edu.utah.rehr.garde.domain.core.terminology;

/**
 *
 * @author RickSlc
 */
public interface ITerminologyKey {

    TerminologyEntityType getEntityType();
    String getKey();

}

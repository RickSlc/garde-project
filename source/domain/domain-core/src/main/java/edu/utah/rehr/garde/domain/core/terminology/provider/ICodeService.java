package edu.utah.rehr.garde.domain.core.terminology.provider;

import edu.utah.rehr.garde.domain.core.terminology.Code;
import edu.utah.rehr.garde.domain.core.terminology.ITerminologyKey;
import edu.utah.rehr.garde.domain.core.terminology.binding.NccnTerminologyBinding;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author RickSlc
 */
public interface ICodeService {

    @Deprecated
    Code getCanonicalCode(ITerminologyKey key);
    @Deprecated
    Code getCanonicalCodeForEhrCode(Code code);
    @Deprecated
    Set<Code> getCanonicalCodeSet(ITerminologyKey setKey);

    @Deprecated
    Set<Code> getEhrCodeSet(ITerminologyKey setKey);

    Set<Code> getCodeSet(ITerminologyKey setKey);

    String getCodeSystem(ITerminologyKey key);

    String getUri(ITerminologyKey key);

    @Deprecated
    Boolean isCanonicalSetMember(ITerminologyKey setKey, Code code);

    @Deprecated
    Boolean isEhrSetMember(ITerminologyKey setKey, Code code);

    Boolean isSetMember(ITerminologyKey setKey, Code code);

    Boolean intersects(ITerminologyKey setKey, Set<Code> codes);

    default boolean intersects(Set<Code> s1, Set<Code> s2) {
        if (s1 == null || s2 == null) {
            return false;
        }
        Set<Code> result = new HashSet<>(s1);
        result.retainAll(s2);
        return !result.isEmpty();
    }

    default Set<Code> union(List<NccnTerminologyBinding> sets) {

        Set<Code> combinedSet = new HashSet<>();

        for (ITerminologyKey key: sets) {
            Set<Code> codeSet = getCodeSet(key);
            combinedSet.addAll(codeSet);
        }

        return combinedSet;
    }

}

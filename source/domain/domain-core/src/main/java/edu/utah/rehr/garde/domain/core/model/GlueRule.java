package edu.utah.rehr.garde.domain.core.model;


public enum GlueRule implements IRule {

    FH_LDL_GT_190_FHX_PREMATURE_CAD("FH1","1000","Persistent LDL-C > 190 and a first degree relative with premature CAD","INCLUSION")
    , FH_LDL_GT_190_FHX_CAD_NO_AGE("FH2","1001","Persistent LDL-C > 190 and first degree relative with CAD assumed premature (unknown age of onset)","INCLUSION")
    , FH_LDL_GT_250("FH3","1010","Persistent LDL-C >= 250","INCLUSION")
//    , FH_FIRST_DEG_REL_CAD("FH3","","First degree relative has coronary artery disease (CAD)","INCLUSION")
//    , FH_FIRST_DEG_REL_PREMATURE_CAD("FH5","","First degree relative has premature coronary artery disease (CAD) ","INCLUSION")
//    , FH_LDL_GT_160_FHX("FH6","","LDL-C > 160 and <= 190 and family history of CAD or premature CAD","INCLUSION")
    ;

    private final String id;

    private final String shortCode;
    private final String label;
    private final String criteriaType;

    GlueRule(String shortCode, String id, String label, String criteriaType) {
        this.id = id;
        this.shortCode = shortCode;
        this.label = label;
        this.criteriaType = criteriaType;
    }

    public static GlueRule findByCode(String code) {
        for (GlueRule rule: GlueRule.values()) {
            if (rule.getCode().equals(code)) {
                return rule;
            }
        }
        return null;
    }

    public static GlueRule findById(String id) {
        for (GlueRule rule: GlueRule.values()) {
            if (rule.getId().equals(id)) {
                return rule;
            }
        }
        return null;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getCode() {
        return name();
    }

    @Override
    public String getShortCode() {
        return shortCode;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public String getCriteriaType() {
        return criteriaType;
    }
}

package edu.utah.rehr.garde.domain.core.provider;

import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.model.MetCriteriaMessage;
import edu.utah.rehr.garde.domain.core.model.MetNccnCriteriaFact;

import java.util.List;
@Deprecated
public interface IMetCriteriaProvider {

    List<MetCriteriaMessage> getAllPatientMetCriteriaMessages();
    List<MetCriteriaMessage> getMetCriteriaMessages(int version);
    UResult<Integer> batchLoadMetCriteriaFacts(List<MetNccnCriteriaFact> currentFactList, int version);

}

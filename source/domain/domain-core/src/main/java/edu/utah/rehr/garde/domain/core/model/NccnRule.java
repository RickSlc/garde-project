package edu.utah.rehr.garde.domain.core.model;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum NccnRule implements IRule {

    BC_GENETIC_MUTATION_IN_FHX("BC_GENETIC_FHX_CRIT", "BC-0", "100", "BRCA1/2, CHEK2, ATM, PALB2, TP53, PTEN, CDH1, Cowden Syndrome, Li-Fraumeni Syndrome discovered in the coded family history","INCLUSION")
    , BC_HAS_BREAST_CA_AGE_LTE_45_FHX_CRIT("BC_BREAST_CA_AGE_LT_45_FHX_CRIT","BC-1","110", "First or second degree relative with beast cancer age <= 45","INCLUSION")
    , BC_OVARIAN_CA_FHX_CRIT("BC_OVARIAN_CA_FHX_CRIT", "BC-2","120","First or second degree relative with ovarian cancer","INCLUSION")
    , BC_PANCREATIC_CA_FHX_CRIT("BC_PANCREATIC_CA_FHX_CRIT", "BC-3", "160", "First or second degree relative with pancreatic cancer","INCLUSION")
    , BC_3_RELATIVES_WITH_FHX_CA_CRIT("BC_3_RELATIVES_WITH_FHX_CA_CRIT", "BC-4","130", "Three or more first or second degree relatives with breast, prostate, or pancreas cancer on the same side of the family","INCLUSION")
    , BC_MALE_BREAST_CA_FHX_CRIT("BC_MALE_BREAST_CA_FHX_CRIT", "BC-5","140", "Breast cancer in a male relative - brother, father, uncle, son, grandfather","INCLUSION")
    , BC_JEWISH_WITH_CA_FHX_CRIT("BC_JEWISH_WITH_CA_FHX_CRIT", "BC-6","150", "Jewish and any breast, ovarian, prostate, pancreas anywhere in the family at any age","INCLUSION")

    ,CO_GENETIC_FHX_CRIT("CO_GENETIC_FHX_CRIT", "CO-0", "200","MLH1, MSH2, PMS2, MSH6, EPCAM, Lynch syndrome, hereditary non-polyposis colorectal cancer (HNPCC), familial adenomatous polyposis (FAP), APC, MYH, MUTYH, serrated polyposis, or polyposis discovered in the coded family history","INCLUSION")
    ,CO_COLON_CA_AGE_LT_50_FHX_CRIT("CO_COLON_CA_AGE_LT_50_FHX_CRIT", "CO-1", "210","First or second degree relative with colon cancer <= 50","INCLUSION")
    ,CO_ENDOMETRIAL_CA_AGE_LT_50_FHX_CRIT("CO_ENDOMETRIAL_CA_AGE_LT_50_FHX_CRIT", "CO-2", "220","First or second degree relative with endometrial cancer age of onset < 50","INCLUSION")
    ,CO_3_RELATIVES_WITH_CA_FHX_CRIT("CO_3_RELATIVES_WITH_CA_FHX_CRIT", "CO-3", "230","Three or more first or second degree relatives with Lynch syndrome, hereditary non-polyposis colorectal cancer (HNPCC), colon cancer, endometrial cancer, uterine cancer, ovarian cancer, stomach cancer, gastric cancer, small bowel cancer, small intestine cancer, Kidney cancer, ureteral cancer, bladder cancer, urethra cancer, brain cancer, pancreas cancer AND at least one is colon or endometrial cancer AND all on the same side of the family","INCLUSION")
    ,NO_LONGER_MEETS_CRITERIA("NO_LONGER_MEETS_CRITERIA", "NL-0", "0","No longer meets criteria for genetic testing due to a family history update.","EXCLUSION");

    private final String id;
    private final String code;

    private final String shortCode;
    private final String label;
    private final String criteriaType;

    NccnRule(String code, String shortCode, String id, String label, String criteriaType) {
        this.id = id;
        this.shortCode = shortCode;
        this.code = code;
        this.label = label;
        this.criteriaType = criteriaType;
    }

    public String getCriteriaType() {
        return criteriaType;
    }

    public String getShortCode() {
        return shortCode;
    }
    public String getCode() {
        return code;
    }

    public String getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public static NccnRule findByCode(String code) {

        for (NccnRule rule:NccnRule.values()) {
            if (rule.getCode().equals(code)) {
                return rule;
            }
        }
        return null;
    }

    public static NccnRule findById(String id) {

        for (NccnRule rule:NccnRule.values()) {
            if (rule.getId().equals(id)) {
                return rule;
            }
        }
        return null;
    }
}

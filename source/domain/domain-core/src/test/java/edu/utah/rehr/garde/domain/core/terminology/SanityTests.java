package edu.utah.rehr.garde.domain.core.terminology;

import edu.utah.rehr.garde.domain.core.terminology.provider.csv.TerminologyReaderCsv;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SanityTests {

    private static final Logger log = LoggerFactory.getLogger(SanityTests.class);
//
//    @Test
//    void arrayEquals() {
//
//        String testHeader = "CODE_BINDING\",\"CODE_SYSTEM\",\"CODE\",\"CODE_LABEL";
//        String[] testArray = testHeader.replace("\"","").split(",");
//
//        assertArrayEquals(TerminologyReaderCsv.CODE_BINDING_HEADER, testArray);
//
//    }
//
//    @Test
//    void isValidHeader() {
//
//        String testHeader = "CODE_BINDING,CODE_SYSTEM,CODE,CODE_LABEL";
//        String[] testArray = testHeader.split(",");
//        var result = TerminologyReaderCsv.isValidHeader(TerminologyReaderCsv.CODE_BINDING_HEADER, "test-files/code-bindings-test.csv");
//        assertTrue(result.getResult());
//
//    }
//
//    @Test
//    void isValidHeaderBOMTest() {
//
//        String testHeader = "CODE_BINDING,CODE_SYSTEM,CODE,CODE_LABEL";
//        String[] testArray = testHeader.split(",");
//        var result = TerminologyReaderCsv.isValidHeader(TerminologyReaderCsv.CODE_BINDING_HEADER, "test-files/code-bindings-utf-bom-test.csv");
//        assertTrue(result.getResult());
//
//    }

//    @Test
//    void wth() {
//        try (BufferedReader br = new BufferedReader(new FileReader("test-files/code-bindings-utf-bom-test.csv"))) {
//            String line = br.readLine();
//            if (line != null) {
//                String[] actualHeader = line
//                        .replace(" ","")
//                        .replace("\"","")
//                        .split(",");
//                log.debug("File header : " + Arrays.toString(actualHeader));
//                log.debug("Expected header : " + Arrays.toString(TerminologyReaderCsv.CODE_BINDING_HEADER));
//                assertEquals(actualHeader[0], TerminologyReaderCsv.CODE_BINDING_HEADER[0]);
//                assertTrue("CODE_BINDING".equals(actualHeader[0]));
//                assertTrue("CODE_BINDING".equals(TerminologyReaderCsv.CODE_BINDING_HEADER[0]));
//                assertArrayEquals(TerminologyReaderCsv.CODE_BINDING_HEADER, removeBOM(actualHeader));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }

}

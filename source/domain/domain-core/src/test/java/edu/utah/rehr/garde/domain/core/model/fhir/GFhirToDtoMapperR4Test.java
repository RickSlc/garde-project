package edu.utah.rehr.garde.domain.core.model.fhir;

import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxDTO;
import edu.utah.rehr.garde.domain.core.model.dto.ObservationDTO;
import edu.utah.rehr.garde.domain.core.model.dto.PatientDTO;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.GDtoToFhirMapperR4;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.GFhirToDtoMapperR4;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import edu.utah.rehr.garde.domain.core.terminology.provider.csv.TerminologyMapsProviderCsv;
import edu.utah.rehr.garde.domain.core.terminology.service.CodeServiceByMaps;
import edu.utah.rehr.garde.domain.core.utils.DateUtil;
import org.hl7.fhir.r4.model.*;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

public class GFhirToDtoMapperR4Test {

    private final ICodeService codeService;

    public GFhirToDtoMapperR4Test() {
        this.codeService = new CodeServiceByMaps(
                TerminologyMapsProviderCsv.getCodeSetMap("classpath:terminology/glue-code-sets.csv")
                , TerminologyMapsProviderCsv.getCodeBindingsMap("classpath:terminology/garde-standard-code-bindings.csv")
                , TerminologyMapsProviderCsv.getCodeBindingsMap("classpath:terminology/glue-ehr-codes-ut.csv")
                , TerminologyMapsProviderCsv.getUriBindingsMap("classpath:terminology/glue-uris.csv")
        );
    }

    @Test
    public void sanityTest() {
        assertTrue(true);
    }

    @Test
    public void testPatientRoundTrip() {

        PatientDTO dto = new PatientDTO();
        dto.setPatientIdCodeSystem("patientId-cs");
        dto.setPatientId("1");
        dto.setBirthDateString("2000-01-01");
        dto.setSexCodeSystem("http://hl7.org/fhir/administrative-gender");
        dto.setSexCode("female");
        dto.setSexLabel("Female");

        dto.setRaceCodeSystem("race-cs");
        dto.setRaceCode("white");
        dto.setRaceLabel("White");

        dto.setEthnicityCodeSystem("ethnicity-cs");
        dto.setEthnicityCode("not-hispanic");
        dto.setEthnicityLabel("Not-hispanic label");

        Patient fhirPatient = GDtoToFhirMapperR4.dtoToPatient(dto, codeService);
        PatientDTO dto2 = GFhirToDtoMapperR4.patientToDto(fhirPatient);

        assertEquals(dto, dto2);
    }

    @Test
    public void testFamilyHxRoundTrip() {

        FamilyHxDTO dto = new FamilyHxDTO();
        dto.setFamilyHxIdCodeSystem("familyHxId-cs");
        dto.setFamilyHxId("P1_E1_R1");

        dto.setPatientIdCodeSystem("patientId-cs");
        dto.setPatientId("P1");

        dto.setEncounterIdCodeSystem("encounterId-cs"); // no slot in fhir familyHx for encounter or code system
        dto.setEncounterId("E1");
        dto.setRecordId("R1");
        dto.setEncounterDateString("2000-01-01T12:00:01");

        dto.setConditionCodeSystem("condition-cs");
        dto.setConditionCode("condition-cd");
        dto.setConditionLabel("condition-label");

        dto.setConditionOnsetAgeString("90");

        dto.setRelationshipCodeSystem("relationship-cs");
        dto.setRelationshipCode("relationship-cd");
        dto.setRelationshipLabel("relationship-label");

        dto.setComments("some comments");

        FamilyMemberHistory fhirFamilyMemberHistory = GDtoToFhirMapperR4.dtoToFamilyMemberHistory(dto);
        FamilyHxDTO dto2 = GFhirToDtoMapperR4.familyMemberHistoryToDto(fhirFamilyMemberHistory);

        assertEquals(dto, dto2);
    }

    @Test
    public void testObservationRoundTrip() {

        ObservationDTO dto = new ObservationDTO();

        dto.setObservationIdCodeSystem("observationId-cs");
        dto.setObservationId("O1");
        dto.setPatientIdCodeSystem("patientId-cs");
        dto.setPatientId("P1");
        dto.setObservationDateTimeString("2020-01-01T12:01:01");
        dto.setObservationCodeSystem("observationCode-cs");
        dto.setObservationCode("observationCode-cd");
        dto.setObservationLabel("observationCode-label");
        dto.setObservationValue("100.0");
        dto.setObservationValueUnits("obs-value-units");

        dto.setEncounterIdCodeSystem("encounterId-cs");
        dto.setEncounterId("E1");

        Observation fhirObservation = GDtoToFhirMapperR4.dtoToObservation(dto);
        ObservationDTO dto2 = GFhirToDtoMapperR4.observationToDto(fhirObservation);

        assertEquals(dto, dto2);
    }

        @Test
    void testFamilyMemberHistoryToDto() {
        FamilyMemberHistory familyMemberHistory = new FamilyMemberHistory();
        familyMemberHistory.addIdentifier()
                .setSystem("familyHxIdSystem")
                .setValue("familyHxId");
        familyMemberHistory.setPatient(new Reference()
                .setIdentifier(new Identifier()
                        .setSystem("patientIdSystem")
                        .setValue("patientId")));
        familyMemberHistory.setDate(DateUtil.toDate(LocalDateTime.of(2023, 1, 1, 10, 0)));
        familyMemberHistory.setRelationship(new CodeableConcept().addCoding(
                new Coding()
                        .setSystem("relationshipCodeSystem")
                        .setCode("relationshipCode")
                        .setDisplay("relationshipLabel")
        ));
        FamilyMemberHistory.FamilyMemberHistoryConditionComponent condition = new FamilyMemberHistory.FamilyMemberHistoryConditionComponent();
        condition.setCode(new CodeableConcept().addCoding(
                new Coding()
                        .setSystem("conditionCodeSystem")
                        .setCode("conditionCode")
                        .setDisplay("conditionLabel")
        ));
        condition.setOnset(new Age().setValue(30));
        familyMemberHistory.addCondition(condition);

        FamilyHxDTO dto = GFhirToDtoMapperR4.familyMemberHistoryToDto(familyMemberHistory);

        assertEquals("familyHxIdSystem", dto.getFamilyHxIdCodeSystem());
        assertEquals("familyHxId", dto.getFamilyHxId());
        assertEquals("patientIdSystem", dto.getPatientIdCodeSystem());
        assertEquals("patientId", dto.getPatientId());
        assertEquals(LocalDateTime.of(2023, 1, 1, 10, 0), dto.getEncounterDate());
        assertEquals("relationshipCodeSystem", dto.getRelationshipCodeSystem());
        assertEquals("relationshipCode", dto.getRelationshipCode());
        assertEquals("relationshipLabel", dto.getRelationshipLabel());
        assertEquals("conditionCodeSystem", dto.getConditionCodeSystem());
        assertEquals("conditionCode", dto.getConditionCode());
        assertEquals("conditionLabel", dto.getConditionLabel());
        assertEquals(Integer.valueOf(30), dto.getConditionOnsetAge());
        assertNull(dto.getComments());
    }

    @Test
    void testPatientToDto() {
        Patient patient = new Patient();
        patient.setId("patientId");
        patient.setBirthDate(DateUtil.toDate(LocalDate.of(1990, 1, 1)));

        patient.addExtension()
                .setUrl("http://utah.edu/rehr/StructureDefinition/race")
                .setValue(new CodeableConcept().addCoding(
                        new Coding()
                                .setSystem("raceCodeSystem")
                                .setCode("raceCode")
                                .setDisplay("raceLabel")
                ));

        patient.addExtension()
                .setUrl("http://utah.edu/rehr/StructureDefinition/ethnicity")
                .setValue(new CodeableConcept().addCoding(
                        new Coding()
                                .setSystem("ethnicityCodeSystem")
                                .setCode("ethnicityCode")
                                .setDisplay("ethnicityLabel")
                ));

        patient.addCommunication()
                .setLanguage(new CodeableConcept().addCoding(
                        new Coding()
                                .setSystem("langCodeSystem")
                                .setCode("langCode")
                                .setDisplay("langLabel")
                ));

        patient.setMaritalStatus(new CodeableConcept().addCoding(
                new Coding()
                        .setSystem("maritalStatusCodeSystem")
                        .setCode("maritalStatusCode")
                        .setDisplay("maritalStatusLabel")
        ));

        PatientDTO dto = GFhirToDtoMapperR4.patientToDto(patient);

        assertEquals("patientId", dto.getPatientId());
        assertEquals(LocalDate.of(1990, 1, 1), dto.getBirthDate());
        assertEquals("raceCodeSystem", dto.getRaceCodeSystem());
        assertEquals("raceCode", dto.getRaceCode());
        assertEquals("raceLabel", dto.getRaceLabel());
        assertEquals("ethnicityCodeSystem", dto.getEthnicityCodeSystem());
        assertEquals("ethnicityCode", dto.getEthnicityCode());
        assertEquals("ethnicityLabel", dto.getEthnicityLabel());
        assertEquals("langCodeSystem", dto.getLangCodeSystem());
        assertEquals("langCode", dto.getLangCode());
        assertEquals("langLabel", dto.getLangLabel());
        assertEquals("maritalStatusCodeSystem", dto.getMaritalStatusCodeSystem());
        assertEquals("maritalStatusCode", dto.getMaritalStatusCode());
        assertEquals("maritalStatusLabel", dto.getMaritalStatusLabel());
    }

    @Test
    void testObservationToDto() {
        Observation observation = new Observation();
        observation.addIdentifier()
                .setSystem("observationIdCodeSystem")
                .setValue("observationId");
        observation.setSubject(new Reference()
                .setIdentifier(new Identifier()
                        .setSystem("patientIdCodeSystem")
                        .setValue("patientId")));
        observation.setEncounter(new Reference()
                .setIdentifier(new Identifier()
                        .setSystem("encounterIdCodeSystem")
                        .setValue("encounterId")));
        observation.setEffective(new DateTimeType(DateUtil.toDate(LocalDateTime.of(2023, 1, 1, 10, 0))));
        observation.addCategory(new CodeableConcept().addCoding(
                new Coding()
                        .setSystem("observationTypeCodeSystem")
                        .setCode("observationType")
                        .setDisplay("observationType")
        ));
        observation.setCode(new CodeableConcept().addCoding(
                new Coding()
                        .setSystem("observationCodeSystem")
                        .setCode("observationCode")
                        .setDisplay("observationLabel")
        ));
        observation.setValue(new Quantity().setValue(100).setUnit("units"));

        ObservationDTO dto = GFhirToDtoMapperR4.observationToDto(observation);

        assertEquals("observationIdCodeSystem", dto.getObservationIdCodeSystem());
        assertEquals("observationId", dto.getObservationId());
        assertEquals("patientIdCodeSystem", dto.getPatientIdCodeSystem());
        assertEquals("patientId", dto.getPatientId());
        assertEquals("encounterIdCodeSystem", dto.getEncounterIdCodeSystem());
        assertEquals("encounterId", dto.getEncounterId());
        assertEquals(LocalDateTime.of(2023, 1, 1, 10, 0), dto.getObservationDateTime());
        assertEquals("observationTypeCodeSystem", dto.getObservationTypeCodeSystem());
        assertEquals("observationType", dto.getObservationType());
        assertEquals("observationType", dto.getObservationType());
        assertEquals("observationCodeSystem", dto.getObservationCodeSystem());
        assertEquals("observationCode", dto.getObservationCode());
        assertEquals("observationLabel", dto.getObservationLabel());
        assertEquals("100", dto.getObservationValue());
        assertEquals("units", dto.getObservationValueUnits());
    }


}

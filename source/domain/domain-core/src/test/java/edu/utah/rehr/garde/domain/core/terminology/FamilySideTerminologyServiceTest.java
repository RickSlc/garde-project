package edu.utah.rehr.garde.domain.core.terminology;

import edu.utah.rehr.garde.domain.core.terminology.binding.NccnTerminologyBinding;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import edu.utah.rehr.garde.domain.core.terminology.provider.csv.TerminologyMapsProviderCsv;
import edu.utah.rehr.garde.domain.core.terminology.service.CodeServiceByMaps;
import lombok.extern.java.Log;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@Log
public class FamilySideTerminologyServiceTest {

    private final ICodeService codeService;
    private final FamilySideTerminologyService familySideTerminologyService;

    public FamilySideTerminologyServiceTest() {
        this.codeService = new CodeServiceByMaps(
                TerminologyMapsProviderCsv.getCodeSetMap("classpath:terminology/garde-code-sets.csv")
                , TerminologyMapsProviderCsv.getCodeBindingsMap("classpath:terminology/garde-standard-code-bindings.csv")
                , TerminologyMapsProviderCsv.getCodeBindingsMap("classpath:terminology/garde-ehr-code-bindings-ut.csv")
                , TerminologyMapsProviderCsv.getUriBindingsMap("classpath:terminology/garde-uri-bindings-map.csv")
        );

        familySideTerminologyService = new FamilySideTerminologyService(codeService);
    }

    @Test
    public void test() {
        assertTrue(true);
    }

    @Test
    public void testLeastFamilySide1() {
        Set<Code> sister = getCodeSet(NccnTerminologyBinding.SISTER_CODE);
        NccnTerminologyBinding familySide = familySideTerminologyService.getLeastFamilySide(sister);
        assertSame(NccnTerminologyBinding.FAMILY_SIDE_GEN_1, familySide);
    }

    @Test
    public void testLeastFamilySide1M() {
        Set<Code> mom = getCodeSet(NccnTerminologyBinding.MOTHER_CODE);
        NccnTerminologyBinding familySide = familySideTerminologyService.getLeastFamilySide(mom);
        assertSame(NccnTerminologyBinding.FAMILY_SIDE_GEN_1M, familySide);
    }

    @Test
    public void testLeastFamilySide1P() {
        Set<Code> relative = getCodeSet(NccnTerminologyBinding.FATHER_CODE);
        NccnTerminologyBinding familySide = familySideTerminologyService.getLeastFamilySide(relative);
        assertSame(NccnTerminologyBinding.FAMILY_SIDE_GEN_1P, familySide);
    }

    @Test
    public void testLeastFamilySide1Daughter() {
        Set<Code> relative = getCodeSet(NccnTerminologyBinding.DAUGHTER_CODE);
        NccnTerminologyBinding familySide = familySideTerminologyService.getLeastFamilySide(relative);
        assertSame(NccnTerminologyBinding.FAMILY_SIDE_GEN_1, familySide);
    }

    @Test
    public void testLeastFamilySide2M() {
        Set<Code> relative = getCodeSet(NccnTerminologyBinding.MATERNAL_AUNT_CODE);
        NccnTerminologyBinding familySide = familySideTerminologyService.getLeastFamilySide(relative);
        assertSame(NccnTerminologyBinding.FAMILY_SIDE_GEN_2M, familySide);
    }

    @Test
    public void testLeastFamilySide2MM() {
        Set<Code> relative = getCodeSet(NccnTerminologyBinding.MATERNAL_GRANDMOTHER_CODE);
        NccnTerminologyBinding familySide = familySideTerminologyService.getLeastFamilySide(relative);
        assertSame(NccnTerminologyBinding.FAMILY_SIDE_GEN_2MM, familySide);
    }

    @Test
    public void testLeastFamilySide2MP() {
        Set<Code> relative = getCodeSet(NccnTerminologyBinding.MATERNAL_GRANDFATHER_CODE);
        NccnTerminologyBinding familySide = familySideTerminologyService.getLeastFamilySide(relative);
        assertSame(NccnTerminologyBinding.FAMILY_SIDE_GEN_2MP, familySide);
    }

    @Test
    public void testLeastFamilySide2PM() {
        Set<Code> relative = getCodeSet(NccnTerminologyBinding.PATERNAL_GRANDMOTHER_CODE);
        NccnTerminologyBinding familySide = familySideTerminologyService.getLeastFamilySide(relative);
        assertSame(NccnTerminologyBinding.FAMILY_SIDE_GEN_2PM, familySide);
    }

    @Test
    public void testLeastFamilySide2PP() {
        Set<Code> relative = getCodeSet(NccnTerminologyBinding.PATERNAL_GRANDFATHER_CODE);
        NccnTerminologyBinding familySide = familySideTerminologyService.getLeastFamilySide(relative);
        assertSame(NccnTerminologyBinding.FAMILY_SIDE_GEN_2PP, familySide);
    }

    @Test
    public void testLeastFamilySide3PPP() {
        Set<Code> relative = getCodeSet(NccnTerminologyBinding.FATHERS_FATHERS_FATHER_CODE);
        NccnTerminologyBinding familySide = familySideTerminologyService.getLeastFamilySide(relative);
        assertSame(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PPP, familySide);
    }

    @Test
    public void testLeastFamilySide3PPM() {
        Set<Code> relative = getCodeSet(NccnTerminologyBinding.FATHERS_FATHERS_MOTHER_CODE);
        NccnTerminologyBinding familySide = familySideTerminologyService.getLeastFamilySide(relative);
        assertSame(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PPM, familySide);
    }

    @Test
    public void testLeastFamilySide3PMP() {
        Set<Code> relative = getCodeSet(NccnTerminologyBinding.FATHERS_MOTHERS_FATHER_CODE);
        NccnTerminologyBinding familySide = familySideTerminologyService.getLeastFamilySide(relative);
        assertSame(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PMP, familySide);
    }

    @Test
    public void testLeastFamilySide3PMM() {
        Set<Code> relative = getCodeSet(NccnTerminologyBinding.FATHERS_MOTHERS_MOTHER_CODE);
        NccnTerminologyBinding familySide = familySideTerminologyService.getLeastFamilySide(relative);
        assertSame(NccnTerminologyBinding.FAMILY_SIDE_GEN_3PMM, familySide);
    }

    @Test
    public void testLeastFamilySide3MPP() {
        Set<Code> relative = getCodeSet(NccnTerminologyBinding.MOTHERS_FATHERS_FATHER_CODE);
        NccnTerminologyBinding familySide = familySideTerminologyService.getLeastFamilySide(relative);
        assertSame(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MPP, familySide);
    }

    @Test
    public void testLeastFamilySide3MPM() {
        Set<Code> relative = getCodeSet(NccnTerminologyBinding.MOTHERS_FATHERS_MOTHER_CODE);
        NccnTerminologyBinding familySide = familySideTerminologyService.getLeastFamilySide(relative);
        assertSame(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MPM, familySide);
    }

    @Test
    public void testLeastFamilySide3MMP() {
        Set<Code> relative = getCodeSet(NccnTerminologyBinding.MOTHERS_MOTHERS_FATHER_CODE);
        NccnTerminologyBinding familySide = familySideTerminologyService.getLeastFamilySide(relative);
        assertSame(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MMP, familySide);
    }

    @Test
    public void testLeastFamilySide3MMM() {
        Set<Code> relative = getCodeSet(NccnTerminologyBinding.MOTHERS_MOTHERS_MOTHER_CODE);
        NccnTerminologyBinding familySide = familySideTerminologyService.getLeastFamilySide(relative);
        assertSame(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MMM, familySide);
    }

    @Test
    public void testSameSideMomDad() {

        assertFalse(familySideTerminologyService.isOnSameSide(
                NccnTerminologyBinding.MOTHER_CODE
                , NccnTerminologyBinding.FATHER_CODE)
        );
    }

    @Test
    public void testSameSideMomPAunt() {

        assertFalse(familySideTerminologyService.isOnSameSide(
                NccnTerminologyBinding.MOTHER_CODE
                , NccnTerminologyBinding.PATERNAL_AUNT_CODE)
        );
    }

    @Test
    public void testSameSideMomMAunt() {

        assertTrue(familySideTerminologyService.isOnSameSide(
                NccnTerminologyBinding.MOTHER_CODE
                , NccnTerminologyBinding.MATERNAL_AUNT_CODE)
        );
    }

    @Test
    public void testSameSideDadPAunt() {

        assertTrue(familySideTerminologyService.isOnSameSide(
                NccnTerminologyBinding.FATHER_CODE
                , NccnTerminologyBinding.PATERNAL_AUNT_CODE)
        );
    }

    @Test
    public void testSameSideDadPGUncle() {

        assertTrue(familySideTerminologyService.isOnSameSide(
                NccnTerminologyBinding.FATHER_CODE
                , NccnTerminologyBinding.FATHERS_FATHERS_BROTHER_CODE)
        );
    }

    @Test
    public void testSameSideBroPGUncle() {

        assertTrue(familySideTerminologyService.isOnSameSide(
                NccnTerminologyBinding.BROTHER_CODE
                , NccnTerminologyBinding.FATHERS_FATHERS_BROTHER_CODE)
        );
    }

    @Test
    public void testSameSideSisPGUncle() {

        assertTrue(familySideTerminologyService.isOnSameSide(
                NccnTerminologyBinding.SISTER_CODE
                , NccnTerminologyBinding.FATHERS_FATHERS_BROTHER_CODE)
        );
    }

    @Test
    public void testSameSideSisPGUncleDiffOrder() {

        assertTrue(familySideTerminologyService.isOnSameSide(
                NccnTerminologyBinding.FATHERS_FATHERS_BROTHER_CODE
                , NccnTerminologyBinding.SISTER_CODE)
        );
    }

    @Test
    public void testSameSideGAuntGUncle() {

        assertTrue(familySideTerminologyService.isOnSameSide(
                NccnTerminologyBinding.FATHERS_FATHERS_BROTHER_CODE
                , NccnTerminologyBinding.FATHERS_FATHERS_SISTER_CODE)
        );
    }

    @Test
    public void testSameSideMGAuntMGUncle() {

        assertTrue(familySideTerminologyService.isOnSameSide(
                NccnTerminologyBinding.MOTHERS_FATHERS_BROTHER_CODE
                , NccnTerminologyBinding.MOTHERS_FATHERS_SISTER_CODE)
        );
    }

    @Test
    public void testSameSideMGAuntPGUncle() {

        assertFalse(familySideTerminologyService.isOnSameSide(
                NccnTerminologyBinding.FATHERS_FATHERS_BROTHER_CODE
                , NccnTerminologyBinding.MOTHERS_FATHERS_SISTER_CODE)
        );
    }

    @Test
    public void testSameSideFFF_FFM() {

        assertFalse(familySideTerminologyService.isOnSameSide(
                NccnTerminologyBinding.FATHERS_FATHERS_FATHER_CODE
                , NccnTerminologyBinding.FATHERS_FATHERS_MOTHER_CODE)
        );
    }

    @Test
    public void testSameSideNotFamilyMemberRuntimeException() {

        try {
            familySideTerminologyService.isOnSameSide(
                    NccnTerminologyBinding.MOTHER_CODE
                    , NccnTerminologyBinding.BC_ADD_RISK_CODE_SET);
            fail();
        } catch (RuntimeException e) {
            assertTrue(true);
        }
    }

    @Test
    public void testRelativeDegree1Mom() {

        Set<Code> relation = getCodeSet(NccnTerminologyBinding.MOTHER_CODE);
        assertEquals(1, familySideTerminologyService.getRelativeDegree(relation));
    }

    @Test
    public void testRelativeDegree1Daughter() {

        Set<Code> relation = getCodeSet(NccnTerminologyBinding.DAUGHTER_CODE);
        assertEquals(1, familySideTerminologyService.getRelativeDegree(relation));
    }

    @Test
    public void testRelativeDegree2Grandpa() {

        Set<Code> relation = getCodeSet(NccnTerminologyBinding.PATERNAL_GRANDFATHER_CODE);
        assertEquals(2, familySideTerminologyService.getRelativeDegree(relation));

        Set<Code> relation2 = getCodeSet(NccnTerminologyBinding.MATERNAL_GRANDFATHER_CODE);
        assertEquals(2, familySideTerminologyService.getRelativeDegree(relation2));
    }

    @Test
    public void testRelativeDegree2Uncle() {

        Set<Code> relation = getCodeSet(NccnTerminologyBinding.PATERNAL_UNCLE_CODE);
        assertEquals(2, familySideTerminologyService.getRelativeDegree(relation));

        Set<Code> relation2 = getCodeSet(NccnTerminologyBinding.MATERNAL_UNCLE_CODE);
        assertEquals(2, familySideTerminologyService.getRelativeDegree(relation2));
    }

    @Test
    public void testRelativeDegree3Cousin() {

        Set<Code> relation = getCodeSet(NccnTerminologyBinding.MATERNAL_COUSIN_CODE);
        assertEquals(3, familySideTerminologyService.getRelativeDegree(relation));

        Set<Code> relation2 = getCodeSet(NccnTerminologyBinding.PATERNAL_COUSIN_CODE);
        assertEquals(3, familySideTerminologyService.getRelativeDegree(relation2));
    }

    @Test
    public void testRelativeDegree3GAunt() {

        Set<Code> relation = getCodeSet(NccnTerminologyBinding.FATHERS_FATHERS_SISTER_CODE);
        assertEquals(3, familySideTerminologyService.getRelativeDegree(relation));

        Set<Code> relation2 = getCodeSet(NccnTerminologyBinding.FATHERS_MOTHERS_SISTER_CODE);
        assertEquals(3, familySideTerminologyService.getRelativeDegree(relation2));

        Set<Code> relation3 = getCodeSet(NccnTerminologyBinding.MOTHERS_FATHERS_SISTER_CODE);
        assertEquals(3, familySideTerminologyService.getRelativeDegree(relation3));

        Set<Code> relation4 = getCodeSet(NccnTerminologyBinding.MOTHERS_MOTHERS_SISTER_CODE);
        assertEquals(3, familySideTerminologyService.getRelativeDegree(relation4));
    }

    @Test
    public void testRelativeDegree3GGF() {

        Set<Code> relation = getCodeSet(NccnTerminologyBinding.FATHERS_FATHERS_FATHER_CODE);
        assertEquals(3, familySideTerminologyService.getRelativeDegree(relation));

        Set<Code> relation2 = getCodeSet(NccnTerminologyBinding.FATHERS_MOTHERS_FATHER_CODE);
        assertEquals(3, familySideTerminologyService.getRelativeDegree(relation2));

        Set<Code> relation3 = getCodeSet(NccnTerminologyBinding.MOTHERS_FATHERS_FATHER_CODE);
        assertEquals(3, familySideTerminologyService.getRelativeDegree(relation3));

        Set<Code> relation4 = getCodeSet(NccnTerminologyBinding.MOTHERS_MOTHERS_FATHER_CODE);
        assertEquals(3, familySideTerminologyService.getRelativeDegree(relation4));
    }

    @Test
    public void testRelativeDegree3GGM() {

        Set<Code> relation = getCodeSet(NccnTerminologyBinding.FATHERS_FATHERS_MOTHER_CODE);
        assertEquals(3, familySideTerminologyService.getRelativeDegree(relation));

        Set<Code> relation2 = getCodeSet(NccnTerminologyBinding.FATHERS_MOTHERS_MOTHER_CODE);
        assertEquals(3, familySideTerminologyService.getRelativeDegree(relation2));

        Set<Code> relation3 = getCodeSet(NccnTerminologyBinding.MOTHERS_FATHERS_MOTHER_CODE);
        assertEquals(3, familySideTerminologyService.getRelativeDegree(relation3));

        Set<Code> relation4 = getCodeSet(NccnTerminologyBinding.MOTHERS_MOTHERS_MOTHER_CODE);
        assertEquals(3, familySideTerminologyService.getRelativeDegree(relation4));
    }

    @Test
    public void testRelativeDegreeNull() {
        assertEquals(0, familySideTerminologyService.getRelativeDegree(null));
    }

    @Test
    public void testRelativeDegreeWrongCode() {

        Set<Code> relation = getCodeSet(NccnTerminologyBinding.CANCER_KIDNEY_CODE);

        try {
            int ans = familySideTerminologyService.getRelativeDegree(relation);
            fail();
        } catch (RuntimeException e) {
            assertTrue(true);
        }
    }

    @Test
    public void isRecognizedFamilyRelationMomSet() {
        assertTrue(familySideTerminologyService.isRecognizedFamilyRelation(getCodeSet(NccnTerminologyBinding.MOTHER_CODE)));
    }

    @Test
    public void isRecognizedFamilyRelationMom() {

        Set<Code> codes = new HashSet<>();
        Code mom = codeService.getCanonicalCode(NccnTerminologyBinding.MOTHER_CODE);
        codes.add(mom);

        assertTrue(familySideTerminologyService.isRecognizedFamilyRelation(codes));
    }

    @Test
    public void testFatherIntersection() {

        Set<Code> codes = new HashSet<>();
        Code mom = codeService.getCanonicalCode(NccnTerminologyBinding.FATHER_CODE);
        codes.add(mom);

        assertTrue( codeService.intersects(familySideTerminologyService.FATHERS_FATHERS_FATHER_FAMILY_SIDE_CODE_SET, codes) );
    }

    @Test
    public void testHalfSibling() {
        // half sibling has ambiguous side. should not be included in FAMILY_SIDE_GEN_1M or FAMILY_SIDE_GEN_1P
        // BUT we decided to keep it in since it is fairly rare and could be
        Set<Code> codes = new HashSet<>();
        Code code = codeService.getCanonicalCode(NccnTerminologyBinding.HALF_SIBLING_CODE);
        codes.add(code);

        assertTrue( codeService.intersects(familySideTerminologyService.FATHERS_FATHERS_FATHER_FAMILY_SIDE_CODE_SET, codes) );
        assertTrue( codeService.intersects(familySideTerminologyService.MOTHERS_FATHERS_FATHER_FAMILY_SIDE_CODE_SET, codes) );
    }

    @Test
    public void testPaternalHalfSibling() {
        Set<Code> codes = new HashSet<>();
        Code code = codeService.getCanonicalCode(NccnTerminologyBinding.PATERNAL_HALF_SIBLING_CODE);
        codes.add(code);

        assertTrue( codeService.intersects(familySideTerminologyService.FATHERS_FATHERS_FATHER_FAMILY_SIDE_CODE_SET, codes) );
        assertFalse( codeService.intersects(familySideTerminologyService.MOTHERS_FATHERS_FATHER_FAMILY_SIDE_CODE_SET, codes) );
    }

    @Test
    public void testMaternalHalfSibling() {
        Set<Code> codes = new HashSet<>();
        Code code = codeService.getCanonicalCode(NccnTerminologyBinding.MATERNAL_HALF_SIBLING_CODE);
        codes.add(code);

        assertFalse( codeService.intersects(familySideTerminologyService.FATHERS_FATHERS_FATHER_FAMILY_SIDE_CODE_SET, codes) );
        assertTrue( codeService.intersects(familySideTerminologyService.MOTHERS_FATHERS_FATHER_FAMILY_SIDE_CODE_SET, codes) );
    }

    @Test
    public void testMotherIntersection() {

        Set<Code> codes = new HashSet<>();
        Code mom = codeService.getCanonicalCode(NccnTerminologyBinding.MOTHER_CODE);
        codes.add(mom);

        assertTrue( codeService.intersects(familySideTerminologyService.MOTHERS_MOTHERS_MOTHER_FAMILY_SIDE_CODE_SET, codes) );
    }

    @Test
    public void  testMomUnionIntersect() {

        Set<Code> momCodes = new HashSet<>();
        Code momCode = codeService.getCanonicalCode(NccnTerminologyBinding.MOTHER_CODE);
        momCodes.add(momCode);

        List<NccnTerminologyBinding> codeSetKeys = new ArrayList<>();
        codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_1M);
        codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2);
        codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2M);
        codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_2MM);
        codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3M);
        codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MM);
        codeSetKeys.add(NccnTerminologyBinding.FAMILY_SIDE_GEN_3MMM);
        Set<Code> codeSet = codeService.union(codeSetKeys);

        assertTrue(codeService.intersects(codeSet,momCodes));
    }

    @Test
    public void testMotherSet() {

        Set<Code> codes = new HashSet<>();
        Code mom = codeService.getCanonicalCode(NccnTerminologyBinding.MOTHER_CODE);
        codes.add(mom);

        Set<Code> codeSet = codeService.getCodeSet(NccnTerminologyBinding.FAMILY_SIDE_GEN_1M);

        assertTrue( codeService.intersects(codeSet, codes) );
    }

    Set<Code> getCodeSet(NccnTerminologyBinding key) {

//        return codeService.getCodeSet(key);

        if (key.getEntityType().equals(TerminologyEntityType.CODE)) {
            Code code = codeService.getCanonicalCode(key);
            Set<Code> codeSet = new HashSet<>();
            codeSet.add(code);
            return codeSet;
        }

        else if (key.getEntityType().equals(TerminologyEntityType.CODE_SET)) {
            return codeService.getCodeSet(key);
        }

        return null;
    }

}

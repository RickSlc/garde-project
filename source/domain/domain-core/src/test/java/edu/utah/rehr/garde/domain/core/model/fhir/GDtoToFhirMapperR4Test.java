package edu.utah.rehr.garde.domain.core.model.fhir;

import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxDTO;
import edu.utah.rehr.garde.domain.core.model.dto.ObservationDTO;
import edu.utah.rehr.garde.domain.core.model.dto.PatientDTO;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.GDtoToFhirMapperR4;
import edu.utah.rehr.garde.domain.core.provider.DtoProvider;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import edu.utah.rehr.garde.domain.core.terminology.provider.csv.TerminologyMapsProviderCsv;
import edu.utah.rehr.garde.domain.core.terminology.service.CodeServiceByMaps;
import org.hl7.fhir.r4.model.FamilyMemberHistory;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Patient;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class GDtoToFhirMapperR4Test {

    private final ICodeService codeService;

    public GDtoToFhirMapperR4Test() {
        this.codeService = new CodeServiceByMaps(
                TerminologyMapsProviderCsv.getCodeSetMap("classpath:terminology/glue-code-sets.csv")
                , TerminologyMapsProviderCsv.getCodeBindingsMap("classpath:terminology/garde-standard-code-bindings.csv")
                , TerminologyMapsProviderCsv.getCodeBindingsMap("classpath:terminology/glue-ehr-codes-ut.csv")
                , TerminologyMapsProviderCsv.getUriBindingsMap("classpath:terminology/glue-uris.csv")
        );
    }

    @Test
    public void sanityTest() {
        assertTrue(true);
    }

    @Test
    public void testFamilyHxDTO() throws Exception {
        List<FamilyHxDTO> dtoList = DtoProvider.csvToDto("test-files/ut-DE-ID-pat-fhx-20220810.csv", FamilyHxDTO.class);
        List<FamilyMemberHistory> fhriResourceList = new ArrayList<>();
        for (FamilyHxDTO dto : dtoList) {
            fhriResourceList.add(GDtoToFhirMapperR4.dtoToFamilyMemberHistory(dto));
        }

        assertTrue(fhriResourceList.size() > 0);
    }

    @Test
    public void testPatientDTO() throws Exception {
        List<PatientDTO> dtoList = DtoProvider.csvToDto("test-files/ut-DE-ID-pat-demog-20220810.csv", PatientDTO.class);
        List<Patient> fhriResourceList = new ArrayList<>();
        for (PatientDTO dto : dtoList) {
            fhriResourceList.add(GDtoToFhirMapperR4.dtoToPatient(dto,codeService));
        }

        assertTrue(fhriResourceList.size() > 0);
    }

    @Test
    public void testObservationDTO() throws Exception {
        List<ObservationDTO> dtoList = DtoProvider.csvToDto("test-files/ut-DE-ID-pat-obs-20240910.csv", ObservationDTO.class);
        List<Observation> fhriResourceList = new ArrayList<>();
        for (ObservationDTO dto : dtoList) {
            fhriResourceList.add(GDtoToFhirMapperR4.dtoToObservation(dto));
        }

        assertTrue(fhriResourceList.get(0).hasCategory());
    }

}

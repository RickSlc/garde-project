package edu.utah.rehr.garde.domain.core.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class IRuleTest {

    @Test
    void testTest() {
        assertTrue(true);
    }

    @Test
    void helperByIdTest() {

        IRule rule = RuleHelper.findById(NccnRule.class, "100");
        assertEquals("BC_GENETIC_FHX_CRIT", rule.getCode());
    }
    @Test
    void ruleByIdTest() {
        IRule rule = NccnRule.findById("100");
        assertEquals("BC_GENETIC_FHX_CRIT", rule.getCode());
    }

    @Test
    void helperByCodeTest() {
        IRule rule = RuleHelper.findByCode(NccnRule.class, "BC_GENETIC_FHX_CRIT");
        assertEquals("100", rule.getId());
    }
    @Test
    void codeByIdTest() {
        IRule rule = NccnRule.findByCode("BC_GENETIC_FHX_CRIT");
        assertEquals("100", rule.getId());
    }
}

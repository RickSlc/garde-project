package edu.utah.rehr.garde.domain.core.model.dto;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

public class FamilyHxDTOTest {

    @Test
    public void sanityTest() {
        assertTrue(true);
    }

    @Test
    public void testDtoEmptyConstructor() {

        FamilyHxDTO dto1 = new FamilyHxDTO();
        assertNotNull(dto1);
    }

    @Test
    public void testDtoConstruction() {

        FamilyHxDTO dto = new FamilyHxDTO();
        dto.setFamilyHxId("1");

        assertEquals("1", dto.getFamilyHxId());
    }

    @Test
    public void testDtoEncDateString() {

        FamilyHxDTO dto = new FamilyHxDTO();
        dto.setFamilyHxId("1");
        dto.setEncounterDateString("2024-01-01");

        assertEquals(dto.getEncounterDate(), LocalDateTime.of(2024, 1, 1, 0, 0, 0));
    }

}

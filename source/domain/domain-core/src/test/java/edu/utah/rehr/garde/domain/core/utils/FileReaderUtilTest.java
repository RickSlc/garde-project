package edu.utah.rehr.garde.domain.core.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FileReaderUtilTest {

    @Test
    void  testFileTypeUtf8_BOM() {
        var fileType = FileReaderUtil.detectCharset("test-files/family-hx-utf8-bom.txt");
        System.out.println(fileType);
        Assertions.assertNotNull(fileType);
    }

    @Test
    void  testFileTypeIso_8859_1() {
        var fileType = FileReaderUtil.detectCharset("test-files/family-hx-iso-8859-1.txt");
        System.out.println(fileType);
        Assertions.assertNotEquals(fileType, "UTF-8 BOM");
    }


}

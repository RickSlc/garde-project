package edu.utah.rehr.garde.domain.core.provider;

import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxDTO;
import edu.utah.rehr.garde.domain.core.model.dto.ObservationDTO;
import edu.utah.rehr.garde.domain.core.model.dto.PatientDTO;
import edu.utah.rehr.garde.domain.core.utils.ExceptionUtil;
import lombok.extern.java.Log;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Log
public class DtoProviderTest {

    @Test
    public void sanityTest() {
        assertTrue(true);
    }

    @Test
    public void testWithMinReqHeader() throws Exception {
        List<PatientDTO> patientDTOList = DtoProvider.csvToDto("test-files/ut-DE-ID-pat-demog-20220810-min-req.csv", PatientDTO.class);
        assertEquals(10, patientDTOList.size());
    }

    @Test
    public void testWithMinReqHeaderLowerCase() throws Exception {
        List<PatientDTO> patientDTOList = DtoProvider.csvToDto("test-files/ut-DE-ID-pat-demog-20220810-lower.csv", PatientDTO.class);
        assertEquals(1000, patientDTOList.size());
    }

    @Test
    public void testWithNOTMinReqHeaderLowerCase() {

        try {
            // Missing sex code should throw exception
            List<PatientDTO> patientDTOList = DtoProvider.csvToDto("test-files/ut-DE-ID-pat-demog-20220810-min-req-NOT.csv", PatientDTO.class);
        } catch (Exception e) {
            log.info("EXPECTED TEST EXCEPTION: " + ExceptionUtil.getMessageString(e,100));
            assertTrue(true);
        }
    }

    @Test
    public void testRealFHxFile() throws Exception {
        List<FamilyHxDTO> dtoList = DtoProvider.csvToDto("test-files/ut-DE-ID-fhx-50k.csv", FamilyHxDTO.class);
        log.info("FHX RECORDS: " + dtoList.size());
        assertTrue(dtoList.size() > 10000);
    }

    @Test
    public void testRealFHxFileMap() throws Exception {
        Map<String,List<FamilyHxDTO>> dtoList = DtoProvider.csvToDtoMap("test-files/ut-DE-ID-pat-fhx-20220810.csv", FamilyHxDTO.class);
        log.info("FHX PATIENT MAP SIZE: " + dtoList.size());
        assertEquals(1000, dtoList.size());
    }

    @Test
    public void testFHxWithMinReqHeader() throws Exception {
        List<FamilyHxDTO> dtoList = DtoProvider.csvToDto("test-files/ut-DE-ID-pat-fhx-20220810.csv", FamilyHxDTO.class);
        assertTrue(dtoList.size() > 0);
    }

    @Test
    public void testObsLdl() throws Exception {
        List<ObservationDTO> dtoList = DtoProvider.csvToDto("test-files/ut-DE-ID-pat-obs-20240910.csv", ObservationDTO.class);
        assertTrue(dtoList.size() > 0);
    }

    @Test
    public void testObsLdlMissingData() {
        try {
            // Missing obs_id should throw exception
            List<ObservationDTO> dtoList = DtoProvider.csvToDto("test-files/ut-DE-ID-pat-obs-20240910-1.csv", ObservationDTO.class);
        } catch (Exception e) {
            log.info("EXPECTED TEST EXCEPTION: " + ExceptionUtil.getMessageString(e,100));
            assertTrue(true);
        }
    }

    @Test
    public void testObsLdlMissingReqCol() {
        try {
            // Missing obs_id should throw exception
            List<ObservationDTO> dtoList = DtoProvider.csvToDto("test-files/ut-DE-ID-pat-obs-20240910-2.csv", ObservationDTO.class);
        } catch (Exception e) {
            log.info("EXPECTED TEST EXCEPTION: " + ExceptionUtil.getMessageString(e,100));
            assertTrue(true);
        }
    }



}

package edu.utah.rehr.garde.domain.etl.provider;

import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxDTO;
import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxNlpDTO;

import java.util.List;
import java.util.Map;

public interface IFamilyHxDTOProvider {

    void setSource(String source);

    List<FamilyHxDTO> getFamilyHxDTOs();

    List<FamilyHxDTO> getFamilyHxDTOs(String patientId);
    List<FamilyHxNlpDTO> getFamilyHxNlpDTOs(String patientId);

    Map<String, List<FamilyHxDTO>> getFamilyHxDTOMap();

    Map<String, List<FamilyHxNlpDTO>> getFamilyHxNlpDTOMap();

    Integer writeFamilyHxNlpDTO(String familyHxFilePath, List<FamilyHxNlpDTO> familyHxDTOList);

}

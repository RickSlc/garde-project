package edu.utah.rehr.garde.domain.etl.service;

import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.model.PatientDataElement;
import org.apache.commons.csv.CSVRecord;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface ICsvService {

    static Map<String, Integer> headerIndex(String[] header) {
        Map<String, Integer> index = new HashMap<>();
        if (index.isEmpty()) {
            int i = 0;
            for (String col : header) {
                index.put(col, i++);
            }
        }
        return index;
    }

    static boolean isValidHeader(Reader fileReader, String[] columnNames, String columnSeparator) throws IOException {
        try (BufferedReader br = new BufferedReader(fileReader)) {

            String line = br.readLine();
            if ( line.contains("\uFEFF") ) {
                throw new IllegalArgumentException("HEADER CONTAINS UTF-8 BOM - NOT SUPPORTED. Save the file as UTF-8 and retry.");
            }

            String[] firstLine = line.replace(" ", "").replace("\"","").split(columnSeparator);
            for (int i = 0; i < firstLine.length; i++) {
                if (!firstLine[i].equals(columnNames[i])) {
                    throw new IllegalArgumentException("The provided file header " + firstLine[i] + " at index " + i + " doesn't match " + columnNames[i]);
                }
            }
            br.close();
            return true;
        }
    }

    /**
     * Remove the byte order mark (BOM) from files saved as UTF-8 BOM. Invisible BOMs interfere with string compare.
     * @param s
     * @return
     */
    static String removeBOM(String s) {
        if (s.startsWith("\uFEFF")) {
            s = s.substring(1);
        }
        return s;
    }

    Iterable<CSVRecord> getCsvRecords(String filePath, String[] HEADERS, char delimiter, char quote);
    Integer writeToCsv(Writer writer, String[] headers, List<List<String>> rows, char delimiter, char quote, boolean includeHeader);

    Integer writePatientDataElementsToCsv(Writer writer, List<PatientDataElement> dataElements, char delimiter, char quote);

}

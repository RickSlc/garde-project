package edu.utah.rehr.garde.domain.etl.provider;

import edu.utah.rehr.garde.domain.core.model.dto.PatientDTO;

import java.util.List;
import java.util.Map;

public interface IPatientDTOProvider {

    List<PatientDTO> getPatientDTOs(String retrievalSpec);

    Map<String, PatientDTO> getPatientDTOMap(String retrievalSpec);

}

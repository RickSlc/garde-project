package edu.utah.rehr.garde.domain.cdshooks.service;

import com.google.gson.Gson;
import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.cdshooks.ICdsHooksEvaluator;
import lombok.extern.java.Log;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.opencds.hooks.lib.json.JsonUtil;
import org.opencds.hooks.model.discovery.Services;
import org.opencds.hooks.model.request.CdsRequest;
import org.opencds.hooks.model.response.CdsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.Collections;

@Component
@Log
public class CdsHooksEvaluator implements ICdsHooksEvaluator {

    private final RestTemplate cdsHooksRestTemplate;

    private final JsonUtil jsonUtilR4;

    @Value("${rehr.nccn.cdshooks.server.url}")
    private String cdsHooksServerUrl;

    @Value("${garde.data.page.size}") // add a :defaultVal
    private Integer dataPageSize;

    @Value("${rehr.nccn.cdshooks.server.username}")
    private String username;

    @Value("${rehr.nccn.cdshooks.server.password}")
    private String password;

    @Autowired
    public CdsHooksEvaluator(
            @Qualifier("cdsHooksRestTemplate") RestTemplate cdsHooksRestTemplate
            , JsonUtil jsonUtilR4
    ) {
        this.cdsHooksRestTemplate = cdsHooksRestTemplate;
        this.jsonUtilR4 = jsonUtilR4;
    }

    private boolean isLastCharSlash(String str) {
        int lastIndex = str.length() - 1;
        return str.charAt(lastIndex) == '/';
    }

    private String getCdsHooksServerUrl() {
        if (isLastCharSlash(cdsHooksServerUrl)) { // remove slash and re-add when needed
            return cdsHooksServerUrl.substring(0, cdsHooksServerUrl.length() - 2);
        }
        return cdsHooksServerUrl;
    }

//    private RestTemplate getRestTemplate() {
//      if (selfSignedRestTemplate == null) {
//          return new RestTemplate();
//      }
//      return selfSignedRestTemplate;
//    }

    @Override
    public UResult<Boolean> isAlive() {

        log.info("CDS HOOKS IS ALIVE: " + getCdsHooksServerUrl());

        try {
            ResponseEntity<String> response = cdsHooksRestTemplate.getForEntity(getCdsHooksServerUrl(), String.class);

            if (!HttpStatus.OK.equals(response.getStatusCode())) {
                log.warning("CDS HOOKS SERVER NOT FOUND: " + getCdsHooksServerUrl());
                return UResult.errorResult();
            }

        } catch (Exception e) {
            log.info("CDS HOOKS IS NOT ALIVE: " + getCdsHooksServerUrl());
            return UResult.errorResult(e);
        }

        return UResult.successResult(true);
    }

    @Override
    public UResult<Boolean> refreshTerminology() {

        String refreshTerminologyUrl = (getCdsHooksServerUrl() + "/").replace("cds-services/","refreshTerminology");
        log.info("REFRESH CDS HOOKS TERMINOLOGY: " + refreshTerminologyUrl);

        try {
            ResponseEntity<String> response = cdsHooksRestTemplate.getForEntity(refreshTerminologyUrl, String.class);

            if (!HttpStatus.OK.equals(response.getStatusCode())) {
                log.warning("CDS HOOKS SERVICE NOT FOUND: " + refreshTerminologyUrl);
                return UResult.errorResult();
            }

        } catch (Exception e) {
            log.warning("CDS HOOKS TERMINOLOGY REFRESH FAILED: " + refreshTerminologyUrl);
            log.severe(ExceptionUtils.getStackTrace(e));
            return UResult.errorResult(e);
        }


        return UResult.successResult(true);
    }

    @Override
    public UResult<Services> getServices() {

        try {
            ResponseEntity<String> response = cdsHooksRestTemplate.getForEntity(getCdsHooksServerUrl(), String.class);

            if (!HttpStatus.OK.equals(response.getStatusCode())) {
                log.warning("CDS HOOKS SERVER NOT FOUND: " + getCdsHooksServerUrl());
                return UResult.errorResult();
            }

            Gson gson = new Gson();

            Services services = gson.fromJson(response.getBody(), Services.class);
            return UResult.successResult(services);

        } catch (Exception e) {
            return UResult.errorResult(e);
        }

    }

    @Override
    public UResult<CdsResponse> evaluate(final CdsRequest cdsRequest, final String cdsServiceId) {

        String jsonRequest = null;
        ResponseEntity<String> response = null;
        String restRequestUrl = getCdsHooksServerUrl() + "/" + cdsServiceId;

        try {
            HttpHeaders headers = createHeaders();

            jsonRequest = jsonUtilR4.toJson(cdsRequest);
            HttpEntity<String> httpEntity = new HttpEntity<>(jsonRequest, headers);

            response = cdsHooksRestTemplate.exchange(restRequestUrl, HttpMethod.POST, httpEntity, String.class);
            CdsResponse cdsResponse = jsonUtilR4.fromJson(response.getBody(), CdsResponse.class);

            if (!HttpStatus.OK.equals(response.getStatusCode())) {
                logError(jsonRequest, cdsServiceId, response);
                return UResult.errorResult();
            }

            return UResult.successResult(cdsResponse);

        } catch (Exception e) {
            logError(jsonRequest, restRequestUrl, response);
            return UResult.errorResult(e);
        }
    }

    @Override
    public UResult<CdsResponse[]> bulkEvaluate(final CdsRequest[] cdsRequests, final String cdsServiceId) {

        String bulkRequestJsonArray = null;
        ResponseEntity<String> response = null;
        String bulkRequestUrl = getCdsHooksServerUrl() + "/bulk/" + cdsServiceId;

        log.info("BULK EVALUATE: " + bulkRequestUrl);

        try {
            bulkRequestJsonArray = jsonUtilR4.toJson(cdsRequests);
            HttpEntity<String> httpEntity = new HttpEntity<>(bulkRequestJsonArray, createHeaders());

            response = cdsHooksRestTemplate.exchange(bulkRequestUrl, HttpMethod.POST, httpEntity, String.class);
            CdsResponse[] cdsResponses = jsonUtilR4.fromJson(response.getBody(), CdsResponse[].class);

            if (!HttpStatus.OK.equals(response.getStatusCode())) {
                logError(bulkRequestJsonArray, bulkRequestUrl, response);
                return UResult.errorResult();
            }

            return UResult.successResult(cdsResponses);

        } catch (Exception e) {
            logError(bulkRequestJsonArray, bulkRequestUrl, response);
            return UResult.errorResult(e);
        }
    }

    HttpHeaders createHeaders(){
        HttpHeaders headers = new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(
                    auth.getBytes(StandardCharsets.US_ASCII) );
            String authHeader = "Basic " + new String( encodedAuth );
            set( "Authorization", authHeader );
        }};

        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);

        return headers;
    }

    private void logError(String jsonRequest, String url, ResponseEntity<String> response) {
        log.severe("CDS REQ URL = " + url);
//        log.severe("CDS REQ PAYLOAD:\n " + jsonRequest + "\n");

        if (response == null) return;
        log.severe("CDS HOOKS REQUEST FAILED: " + response.getStatusCode());
//        log.severe("CDS RESP PAYLOAD: \n" + response.getBody());
    }

}

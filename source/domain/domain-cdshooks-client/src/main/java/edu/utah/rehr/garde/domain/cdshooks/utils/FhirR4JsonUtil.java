package edu.utah.rehr.garde.domain.cdshooks.utils;

import com.google.gson.GsonBuilder;
import org.hl7.fhir.r4.model.*;
import org.opencds.hooks.model.json.CdsHooksJsonUtil;
import org.opencds.hooks.model.r4.support.ActionJsonSupport;
import org.opencds.hooks.model.r4.support.ResourceJsonSupport;
import org.opencds.hooks.model.response.Action;

public class FhirR4JsonUtil extends CdsHooksJsonUtil {

    public FhirR4JsonUtil() {
        super(new GsonBuilder());

        // CdsResponse
        getGsonBuilder().registerTypeAdapter(Action.class, new ActionJsonSupport());

        getGsonBuilder().registerTypeAdapter(Resource.class, new ResourceJsonSupport());
        getGsonBuilder().registerTypeAdapter(Observation.class, new ResourceJsonSupport());
        getGsonBuilder().registerTypeAdapter(FamilyMemberHistory.class, new ResourceJsonSupport());
        getGsonBuilder().registerTypeAdapter(Patient.class, new ResourceJsonSupport());
        getGsonBuilder().registerTypeAdapter(Bundle.class, new ResourceJsonSupport());
        getGsonBuilder().registerTypeAdapter(CommunicationRequest.class, new ResourceJsonSupport());

        getGsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        buildGson();
    }

}

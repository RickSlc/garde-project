package edu.utah.rehr.garde.domain.cdshooks.factory;

import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.model.GCohort;
import edu.utah.rehr.garde.domain.core.model.GEvaluator;
import edu.utah.rehr.garde.domain.core.model.GFhirResource;
import edu.utah.rehr.garde.domain.core.model.dto.FamilyHxDTO;
import edu.utah.rehr.garde.domain.core.model.dto.ObservationDTO;
import edu.utah.rehr.garde.domain.core.model.dto.PatientDTO;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.GDtoToFhirMapperR4;
import edu.utah.rehr.garde.domain.core.provider.DtoProvider;
import edu.utah.rehr.garde.domain.core.terminology.provider.ICodeService;
import edu.utah.rehr.garde.domain.core.utils.ListPager;
import edu.utah.rehr.garde.domain.cdshooks.ICdsHooksRequestFactory;
import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.FamilyMemberHistory;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Patient;
import org.opencds.hooks.model.context.HookContext;
import org.opencds.hooks.model.context.WritableHookContext;
import org.opencds.hooks.model.request.CdsRequest;
import org.opencds.hooks.model.request.WritableCdsRequest;
import org.springframework.util.StopWatch;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.logging.Level;

@Log
public class GCdsHooksRequestFactory implements ICdsHooksRequestFactory {

    private final ICodeService codeService;

    private Map<String, Map<String, List<FamilyHxDTO>>> familyHxDtoCache = new HashMap<>();
    private Map<String, Map<String, List<ObservationDTO>>> obsDtoCache = new HashMap<>();

    public GCdsHooksRequestFactory(ICodeService codeService) {
        this.codeService = codeService;
    }

    @Override
    public void flush() {
        familyHxDtoCache = new HashMap<>();
        obsDtoCache = new HashMap<>();
    }

    @Override
    public void flushInstance(String instanceId) {
        familyHxDtoCache.remove(instanceId);
        obsDtoCache.remove(instanceId);
    }

    @Override
    public UResult<List<CdsRequest>> create(String serviceId, Integer page, Integer pageSize, Map<String, String> params, List<PatientDTO> patientDTOs) {
        checkParams(params);

        StopWatch requestStopWatch = new StopWatch();
        requestStopWatch.start();

        String patientDTOFilePath = params.get("patientDTO_filePath");
        String familyHxDTOFilePath = params.get("familyHxDTO_filePath");
        String patientLdlObsDTOFilePath = params.get("patientLdlObsDTO_filePath");
        String fhirServiceEndpoint = params.get("fhirServiceEndpoint");
        String outputType = params.get("outputType");
        String instanceId = params.get("instanceId");

        List<PatientDTO> patientDTOListToProcess = ListPager.getPage(patientDTOs, pageSize, page);

        log.info("PROCESSING PATIENT DATA PAGE " + page + " with " + patientDTOListToProcess.size() + " PATIENTS");
        log.info("BUILD FACTORY SETTINGS - patientDTOFilePath: " + patientDTOFilePath + ", familyHxDTOFilePath: " +
                familyHxDTOFilePath + ", patientLdlObsDTOFilePath: " + patientLdlObsDTOFilePath + ", fhirServiceEndpoint: " +
                fhirServiceEndpoint);

        log.info("BUILDING CDS HOOKS REQUESTS");

        try {

            List<CdsRequest> cdsRequests = getCdsRequests(serviceId, instanceId, patientDTOListToProcess, familyHxDTOFilePath, patientLdlObsDTOFilePath, fhirServiceEndpoint, outputType);
            requestStopWatch.stop();
            log.info("BUILDING CDS HOOK REQUESTS - PAGE " + page + " COMPLETED IN " + requestStopWatch.getTotalTimeSeconds() + " (SEC)");

            return UResult.successResult(cdsRequests);

        } catch (Exception e) {
            requestStopWatch.stop();
            log.info("ERROR BUILDING CDS HOOK REQUESTS - PAGE " + page);
            return UResult.errorResult(e);
        }

    }

    private List<CdsRequest> getCdsRequests(
            String serviceId
            , String instanceId
            , List<PatientDTO> patientDTOs
            , String familyHxDTOFilePath
            , String patientLdlObsDTOFilePath
            , String fhirServiceEndpoint
            , String outputType) throws Exception {

        Set<String> fhirResources = GEvaluator.getFhirResourceSetByServiceId(serviceId);

        List<CdsRequest> cdsRequests = new ArrayList<>();
        Map<String, List<FamilyHxDTO>> fHxDTOMap= new HashMap<>();
        Map<String, List<ObservationDTO>> obsDTOMap = new HashMap<>();

        if (fhirResources.contains("FamilyMemberHistory")) {

            if (!familyHxDtoCache.containsKey(instanceId) && familyHxDTOFilePath != null && !familyHxDTOFilePath.isEmpty()) {
                log.info("CACHE EMPTY - RETRIEVING FHX DTOs");
                Map<String, List<FamilyHxDTO>> newFHxDTOMap = DtoProvider.csvToDtoMap(familyHxDTOFilePath, FamilyHxDTO.class);
                familyHxDtoCache.put(instanceId, newFHxDTOMap);
                log.info("RETRIEVED FHX DTOs FOR " + newFHxDTOMap.size() + " PATIENTS");
            }

            fHxDTOMap = familyHxDtoCache.get(instanceId);
            log.info("BUILD REQUESTS - EXTRACTING FAMILY HX FOR " + patientDTOs.size() + " PEOPLE.");
        }

        if (fhirResources.contains("Observation")) {

            if (!obsDtoCache.containsKey(instanceId) && patientLdlObsDTOFilePath != null) {
                log.info("RETRIEVING OBSERVATION DTOs");
                Map<String, List<ObservationDTO>> newObsDTOMap = DtoProvider.csvToDtoMap(patientLdlObsDTOFilePath, ObservationDTO.class);
                obsDtoCache.put(instanceId, newObsDTOMap);
                log.info("RETRIEVED OBSERVATION DTOs FOR " + newObsDTOMap.size() + " PATIENTS");
            }

            obsDTOMap = obsDtoCache.get(instanceId);
        }

        log.info(fHxDTOMap.size() + " IN POPULATION HAVE FAMILY HX; " + obsDTOMap.size() + " OBSERVATIONS." );

        for (PatientDTO patDTO : patientDTOs) {

            if (patDTO.getPatientId() == null) {
                log.severe("PatientDTO.patientId IS NULL: " + patDTO.toString());
                throw new RuntimeException("PatientDTO.patientId IS NULL: " + patDTO.toString());
            }

            if (fHxDTOMap.get(patDTO.getPatientId()) == null
                && obsDTOMap.get(patDTO.getPatientId()) == null
            ) { // no family history or observations - next.
                continue;
            }

            CdsRequest cdsRequest = new WritableCdsRequest();
            cdsRequest.setHook("scheduled-evaluation");

            try {
                cdsRequest.setFhirServer(new URL(fhirServiceEndpoint));
            } catch (MalformedURLException e) {
                log.warning("BUILD FACTORY SETTINGS - familyHxDTOFilePath: " + familyHxDTOFilePath + ", fhirServerEndpoint: " + fhirServiceEndpoint);
                log.log(Level.WARNING, "Exception setting CdsHook request fhir server URL", e);
            }

            cdsRequest.setHookInstance(UUID.randomUUID().toString());

            HookContext context = new WritableHookContext();
            context.add("patientId", patDTO.getPatientId());
            context.add("outputType", outputType);
            String delimitedPreviouslyMetCriteria = patDTO.getPreviouslyMetCriteriaCodesDelimitedString("\\|");
            if (delimitedPreviouslyMetCriteria != null) {
                context.add("previouslyMetCriteriaList", delimitedPreviouslyMetCriteria);
            }

            cdsRequest.setContext(context);

            Map<String,List<GFhirResource>> resourceMap = GEvaluator.getFhirResourceMapByServiceId(serviceId);

            for (String resourceType : resourceMap.keySet()) {
                List<GFhirResource> fhirResourcesForService = resourceMap.get(resourceType);
                for (GFhirResource fhirResource : fhirResourcesForService) {
                    if ("Patient".equals(fhirResource.getFhirResource())) {
                        Patient fhirPatient = GDtoToFhirMapperR4.dtoToPatient(patDTO,codeService);
                        cdsRequest.addPrefetchResource(fhirResource.getPrefetchName(), fhirPatient);
                    }
                    if ("Observation".equals(fhirResource.getFhirResource())) {
                        cdsRequest.addPrefetchResource(fhirResource.getPrefetchName(), getObsBundle(patDTO, obsDTOMap));
                    }
                    if ("FamilyMemberHistory".equals(fhirResource.getFhirResource())) {
                        cdsRequest.addPrefetchResource(fhirResource.getPrefetchName(), getFHxBundle(patDTO, fHxDTOMap));
                    }
                }
            }

            cdsRequests.add(cdsRequest);
        }
        log.info(cdsRequests.size() + " CDS HOOK REQUESTS BUILT");
        return cdsRequests;
    }

    private static Bundle getFHxBundle(PatientDTO patDTO, Map<String, List<FamilyHxDTO>> fHxDTOMap) {
        Bundle fhxBundle = new Bundle();
        fhxBundle.setType(Bundle.BundleType.BATCH);

        List<FamilyHxDTO> fhxDTOList = fHxDTOMap.get(patDTO.getPatientId());

        if (fhxDTOList == null) {
            return fhxBundle;
        }

        for (FamilyHxDTO fhxDTO : fhxDTOList) {
            FamilyMemberHistory fhx = GDtoToFhirMapperR4.dtoToFamilyMemberHistory(fhxDTO);
            fhxBundle.addEntry(new Bundle.BundleEntryComponent().setResource(fhx));
        }

        return fhxBundle;
    }

    private static Bundle getObsBundle(PatientDTO patDTO, Map<String, List<ObservationDTO>> obsDTOMap) {
        Bundle obsBundle = new Bundle();
        obsBundle.setType(Bundle.BundleType.BATCH);

        List<ObservationDTO> obsDTOList = obsDTOMap.get(patDTO.getPatientId());

        if (obsDTOList == null) {
            return obsBundle;
        }
        for (ObservationDTO obsDTO : obsDTOList) {
            Observation obs = GDtoToFhirMapperR4.dtoToObservation(obsDTO);
            obsBundle.addEntry(new Bundle.BundleEntryComponent().setResource(obs));
        }
        return obsBundle;
    }

    @Override
    public UResult<List<CdsRequest>> create(GCohort populationCohort, GCohort targetCohort, Integer version) {
        return UResult.errorResult(new UnsupportedOperationException("CdsHooksRequestFactoryFamilyHxCancerRiskR4.create(Cohort populationCohort, Cohort targetCohort, Integer version) not currently supported."));
    }

    /**
     * Creates CdsRequests for all of the patients in the population.
     */
    @Override
    public UResult<List<CdsRequest>> create() {
        return UResult.errorResult(new UnsupportedOperationException("GlueCdsHooksRequestFactory.create() not currently supported."));
    }

    /**
     * Create CdsRequests from files with PatientDTO and FamilyHxDTO
     *
     * @param params
     * @return
     */
    @Override
    public UResult<List<CdsRequest>> create(Map<String, String> params) {
        return UResult.errorResult(new UnsupportedOperationException("GlueCdsHooksRequestFactory.create(Map<String, String> params) not currently supported."));
    }

    private static void checkParams(Map<String, String> params) {

        String serviceId = params.get("cdsServiceId");

        if (StringUtils.isEmpty(params.get("patientDTO_filePath"))) {
            throw new IllegalArgumentException("patientDTO_filePath cannot be null or empty");
        }
        if (StringUtils.isEmpty(params.get("familyHxDTO_filePath"))) {
            throw new IllegalArgumentException("familyHxDTO_filePath cannot be null or empty");
        }
        if (GEvaluator.FH_GENETIC_TESTING_RECOMMENDER_R4_2018.getServiceId().equals(serviceId) && StringUtils.isEmpty(params.get("patientLdlObsDTO_filePath"))) {
            throw new IllegalArgumentException("patientLdlObsDTO_filePath cannot be null or empty");
        }
        if (StringUtils.isEmpty(params.get("outputType"))) {
            throw new IllegalArgumentException("outputType cannot be null or empty");
        }
        if (StringUtils.isEmpty(params.get("fhirServiceEndpoint"))) {
            throw new IllegalArgumentException("fhirServiceEndpoint cannot be null or empty");
        }
    }

}

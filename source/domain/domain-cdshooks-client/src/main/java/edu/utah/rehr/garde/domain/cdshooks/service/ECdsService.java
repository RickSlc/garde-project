package edu.utah.rehr.garde.domain.cdshooks.service;

@Deprecated
public enum ECdsService {

    GENETIC_TESTING_RECOMMENDER_R4_2023("genetic-testing-recommender-r4-2023")
    , GENETIC_TESTING_RECOMMENDER_R4("genetic-testing-recommender-r4")
    , GENETIC_TESTING_RECOMMENDER_STU3("genetic-testing-recommender-stu3");

    String serviceId;

    ECdsService(String serviceId) {
        this.serviceId = serviceId;
    }

    public static String findServiceId(ECdsService cdsService) {

        for (ECdsService eCdsService:values()) {
            if (eCdsService == cdsService) {
                return eCdsService.serviceId;
            }
        }
        return null;
    }

    public String getServiceId() {
        return serviceId;
    }
}

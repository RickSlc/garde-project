package edu.utah.rehr.garde.domain.cdshooks.service;

import edu.utah.rehr.garde.domain.cdshooks.ICdsHooksResponseInterpreter;
import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.accessory.UResultStatus;
import edu.utah.rehr.garde.domain.core.model.GCohort;
import edu.utah.rehr.garde.domain.core.model.GEvaluator;
import edu.utah.rehr.garde.domain.core.model.GlueRule;
import edu.utah.rehr.garde.domain.core.model.dto.MetCriteriaDTO;
import edu.utah.rehr.garde.domain.core.model.fhir.r4.FamilyMemberHistoryHelperR4;
import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CommunicationRequest;
import org.hl7.fhir.r4.model.FamilyMemberHistory;
import org.hl7.fhir.r4.model.ResourceType;
import org.opencds.hooks.model.response.Action;
import org.opencds.hooks.model.response.Card;
import org.opencds.hooks.model.response.CdsResponse;
import org.opencds.hooks.model.response.Suggestion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

@Log
@Component
public class CdsHooksResponseMetCriteriaInterpreterR4 implements ICdsHooksResponseInterpreter<MetCriteriaDTO> {

    @Autowired
    private FamilyMemberHistoryHelperR4 familyMemberHistoryHelper;

    @Autowired
    private UResultStatus tolerance;

    @Override
    public UResult<List<MetCriteriaDTO>> interpret(String serviceId, List<CdsResponse> cdsResponses) {

        if (GEvaluator.FH_GENETIC_TESTING_RECOMMENDER_R4_2018.getServiceId().equals(serviceId)) {
            return interpretActionStrategy(cdsResponses);
        }

        return interpret(cdsResponses);
    }

    public UResult<List<MetCriteriaDTO>> interpretActionStrategy(List<CdsResponse> cdsResponses) {
        List<MetCriteriaDTO> metCriteriaList = new ArrayList<>();

        int count = 0;
        int cohortCount = 0;
        for (CdsResponse cdsResponse : cdsResponses) {

            count++;
            List<Card> cards = cdsResponse.getCards();

            if (cards.isEmpty()) {
                continue;
            }

            Card card = cards.get(0);
            StringBuilder msg = new StringBuilder();

            msg.append("\n").append(++cohortCount)
                    .append(") CDS RESPONSE SUMMARY: ")
                    .append(card.getSummary())
            ;

            List<Suggestion> suggestions = card.getSuggestions();

            if (suggestions != null) {

                for (Suggestion suggestion : suggestions) {
                    msg.append("\n\tSUGGESTION: ").append(suggestion.getLabel());

                    for (Action action : suggestion.getActions()) {
                        msg.append("\n\t\tACTION: " + action.getDescription());

//                        Bundle bundle = (Bundle) action.getResource();
                        String payload = action.getDescription();
                        String[] vars = payload.replace("\"", "").split(",");
                        String patientId = vars[0];
                        String ruleCode = vars[1];

                        GlueRule rule = GlueRule.findByCode(ruleCode);
                        String ruleId = "-1";
                        if (rule != null) {
                            ruleId = rule.getId();
                        }

                        MetCriteriaDTO metCriteria = new MetCriteriaDTO(
                                patientId
                                , "0"
                                , "0"
                                , GCohort.MetFamilialHypercholesterolemiaScreeningCriteria.getCode()
                                , ruleId
                        );

                        metCriteriaList.add(metCriteria);
                        log.info(msg.toString());
                    }
                }
            }
        }
        return UResult.successResult(metCriteriaList);
    }

    @Override
    public UResult<List<MetCriteriaDTO>> interpret(List<CdsResponse> cdsResponses) {

        List<MetCriteriaDTO> metCriteriaList = new ArrayList<>();

        int count = 0;
        int cohortCount = 0;
        for (CdsResponse cdsResponse : cdsResponses) {

            count++;
            List<Card> cards = cdsResponse.getCards();

            if (cards.isEmpty()) {
                continue;
            }

            Card card = cards.get(0);
            StringBuilder msg = new StringBuilder();

            msg.append("\n").append(++cohortCount)
                    .append(") CDS RESPONSE SUMMARY: ")
                    .append(card.getSummary())
            ;

            List<Suggestion> suggestions = card.getSuggestions();

            if (suggestions != null) for (Suggestion suggestion : suggestions) {
                msg.append("\n\tSUGGESTION: ").append(suggestion.getLabel());

                for (Action action : suggestion.getActions()) {
                    msg.append("\n\t\tACTION: " + action.getDescription());

                    Bundle bundle = (Bundle) action.getResource();

                    CommunicationRequest communicationRequest = null;
                    List<FamilyMemberHistory> fhxList = new ArrayList<>();

                    String codeSystem = null;
                    String patientId = null;
                    String ruleId = null;
                    String cohortCd = null;

                    // List of resources - CommunicationRequest and FamilyMemberHistory
                    for (Bundle.BundleEntryComponent bundleComponent : bundle.getEntry()) {

                        ResourceType resourceType = bundleComponent.getResource().getResourceType();
                        String resourceName = resourceType.name();

                        if ("CommunicationRequest".equals(resourceName)) {

                            communicationRequest = (CommunicationRequest) bundleComponent.getResource();

                            String payload = communicationRequest.getPayloadFirstRep().getContentStringType().getValue();
                            String[] vars = payload.replace("\"", "").split(",");
                            codeSystem = vars[0];
                            patientId = vars[1];
                            ruleId = vars[2];
                            cohortCd = vars[3];
                        } else if ("FamilyMemberHistory".equals(resourceName)) {

                            FamilyMemberHistory fhx = (FamilyMemberHistory) bundleComponent.getResource();
                            String[] ehrKeyParts = familyMemberHistoryHelper.getEhrFhxKeyParts(fhx);

                            if (ehrKeyParts == null || ehrKeyParts.length != 3) {
                                String errorMsg = "EHR key format error. Expected <patientId_encounterId_recordId> in FamilyMemberHistory identifiers.";
//                                    return UResult.errorResult(new Exception(errorMsg));
                                log.severe(errorMsg);
                            }

                            String ehrPatId = ehrKeyParts[0];
                            String ehrEncId = ehrKeyParts[1];
                            String ehrRecId = ehrKeyParts[2];

                            if (StringUtils.isNotEmpty(ehrPatId)
                                    && StringUtils.isNotEmpty(ehrEncId)
                                    && StringUtils.isNotEmpty(ehrRecId)
                                    && StringUtils.isNotEmpty(cohortCd)
                                    && StringUtils.isNotEmpty(ruleId)) {

                                MetCriteriaDTO metCriteria = new MetCriteriaDTO(
                                        patientId
                                        , ehrEncId
                                        , ehrRecId
                                        , cohortCd
                                        , ruleId
                                );

                                metCriteriaList.add(metCriteria);
//                                    log.info(String.format("MetCriteriaKey: patientId=%s ehrEncId=%s ehrRecId=%s cohortCd=%s ruleId=%s", patientId, ehrEncId, ehrRecId, cohortCd, ruleId));
                            } else {
                                log.warning("FHx RESOURCE NOT LOADED - INCOMPLETE MetCriteriaKey");
                                log.warning(String.format("MetCriteriaKey: patientId=%s ehrEncId=%s ehrRecId=%s cohortCd=%s ruleId=%s", patientId, ehrEncId, ehrRecId, cohortCd, ruleId));
                            }


                        } else {
                            log.log(Level.SEVERE, "UNEXPECTED resource encountered: " + resourceName);
                        }
                    }

                    if (communicationRequest == null) {
                        log.severe("NO CommunicationRequest in cdsResponse: " + msg);
                    }

                }
//                    log.info(msg.toString());
            }
        }

        log.info("FINISHED ANALYZING " + cdsResponses.size() + " CDS RESPONSES");

        return UResult.successResult(metCriteriaList);
    }

}

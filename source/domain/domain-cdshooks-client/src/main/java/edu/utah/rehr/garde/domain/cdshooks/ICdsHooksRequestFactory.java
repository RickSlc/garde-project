package edu.utah.rehr.garde.domain.cdshooks;

import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.model.GCohort;
import edu.utah.rehr.garde.domain.core.model.dto.PatientDTO;
import org.opencds.hooks.model.request.CdsRequest;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public interface ICdsHooksRequestFactory {

    UResult<List<CdsRequest>> create();
    UResult<List<CdsRequest>> create(Map<String,String> params);

    UResult<List<CdsRequest>> create(String serviceId, Integer page, Integer pageSize, Map<String,String> params, List<PatientDTO> patientDTOs);
    UResult<List<CdsRequest>> create(GCohort sourceCohort, GCohort targetCohort, Integer version);

    void flush();

    void flushInstance(String instanceId);

}

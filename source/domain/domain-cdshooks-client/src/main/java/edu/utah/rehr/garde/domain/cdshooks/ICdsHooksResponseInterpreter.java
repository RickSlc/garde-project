package edu.utah.rehr.garde.domain.cdshooks;

import edu.utah.rehr.garde.domain.core.accessory.UResult;
import org.opencds.hooks.model.response.CdsResponse;

import java.util.List;

public interface ICdsHooksResponseInterpreter<T> {

    UResult<List<T>> interpret(String serviceId, List<CdsResponse> cdsResponses);
    UResult<List<T>> interpret(List<CdsResponse> cdsResponses);

}

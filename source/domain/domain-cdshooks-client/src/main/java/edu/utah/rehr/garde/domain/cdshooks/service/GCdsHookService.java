package edu.utah.rehr.garde.domain.cdshooks.service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import edu.utah.rehr.garde.domain.core.model.GEvaluator;
import edu.utah.rehr.garde.domain.core.model.GFhirResource;
import lombok.NonNull;
import org.opencds.hooks.model.discovery.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GCdsHookService {

    public static Service getCdsHooksServiceDescription(@NonNull GEvaluator gardeEvaluator) {

        Service service = new Service();
        service.setHook("scheduled-evaluation");
        service.setId(gardeEvaluator.getServiceId());
        service.setTitle(gardeEvaluator.getTitle());
        service.setDescription(gardeEvaluator.getGuideline());

        Map<String,String> prefetch = new HashMap<>();

        for (GFhirResource prefetchEntry : gardeEvaluator.getRequiredFhirResources()) {
            prefetch.put(prefetchEntry.getPrefetchName(), prefetchEntry.getFhirQuery());
        }

        service.setPrefetch(prefetch);
        return service;
    }

    public static List<Service> getCdsHooksServiceDescriptions() {
        List <Service> services = new ArrayList<>();
        for (GEvaluator gardeEvaluator : GEvaluator.values()) {
            services.add(getCdsHooksServiceDescription(gardeEvaluator));
        }
        return services;
    }

    public static String getCdsHooksServiceDescriptionsJson() {
        Gson gson = new Gson();

        // Create services array
        JsonArray array = new JsonArray();

        List<Service> servicesList = getCdsHooksServiceDescriptions();
        for (Service service : servicesList) {

            JsonObject serviceJson = new JsonObject();
            serviceJson.addProperty("hook", service.getHook());
            serviceJson.addProperty("title", service.getTitle());
            serviceJson.addProperty("description", service.getDescription());
            serviceJson.addProperty("id", service.getId());

            JsonObject prefetch = new JsonObject();
            for(String prefetchKey:service.getPrefetch().keySet()) {
                String value = service.getPrefetch().get(prefetchKey);
                prefetch.addProperty(prefetchKey, value);
            }
            serviceJson.add("prefetch", prefetch);

            array.add(serviceJson);
        }

        // Creat services object
        JsonObject services = new JsonObject();
        services.add("services",array);

        return gson.toJson(services);
    }


}

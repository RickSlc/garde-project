package edu.utah.rehr.garde.domain.cdshooks;

import edu.utah.rehr.garde.domain.core.accessory.UResult;
import org.opencds.hooks.model.discovery.Services;
import org.opencds.hooks.model.request.CdsRequest;
import org.opencds.hooks.model.response.CdsResponse;

public interface ICdsHooksEvaluator {

    UResult<Boolean> isAlive();
    UResult<Boolean> refreshTerminology();
    UResult<Services> getServices();
    UResult<CdsResponse> evaluate(CdsRequest cdsRequest, String cdsServiceId);
    UResult<CdsResponse[]> bulkEvaluate(final CdsRequest[] cdsRequests, final String cdsServiceId);

}

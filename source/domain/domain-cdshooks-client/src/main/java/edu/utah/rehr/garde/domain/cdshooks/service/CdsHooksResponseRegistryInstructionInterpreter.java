package edu.utah.rehr.garde.domain.cdshooks.service;

import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.model.dto.RegistryInstructionDTO;
import edu.utah.rehr.garde.domain.cdshooks.ICdsHooksResponseInterpreter;
import lombok.extern.java.Log;
import org.hl7.fhir.r4.model.CommunicationRequest;
import org.opencds.hooks.model.response.Action;
import org.opencds.hooks.model.response.Card;
import org.opencds.hooks.model.response.CdsResponse;
import org.opencds.hooks.model.response.Suggestion;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Log
@Component
public class CdsHooksResponseRegistryInstructionInterpreter implements ICdsHooksResponseInterpreter<RegistryInstructionDTO> {

    @Override
    public UResult<List<RegistryInstructionDTO>> interpret(String serviceId, List<CdsResponse> cdsResponses) {
        return null;
    }

    @Override
    public UResult<List<RegistryInstructionDTO>> interpret(List<CdsResponse> cdsResponses) {

        List<RegistryInstructionDTO> registryInstructionDTOList = new ArrayList<>();

        int count = 0;
        for (CdsResponse response : cdsResponses) {

            List<Card> cards = response.getCards();

            if (cards.isEmpty()) {
                log.warning("NO CDS HOOK RESPONSE CARD FOR " + response);
                continue;
            }

            if (cards.size() > 1) {
                log.warning("EXPECT ONE CDS HOOK RESPONSE CARD AND FOUND " + cards.size() + " FOR " + response);
                continue;
            }

            Card card = cards.get(0); // there should be only one
            String summary = card.getSummary();

            if (summary.startsWith("No action")) { // nothing to do
                continue;
            }

            StringBuilder msg = new StringBuilder();

            msg.append("\n").append(++count)
                    .append(") CDS RESPONSE SUMMARY: ")
                    .append(card.getSummary())
            ;

            List<Suggestion> suggestions = card.getSuggestions();
            // only one suggestion
            Suggestion suggestion = suggestions.get(0);
            List<Action> actions = suggestion.getActions();
            // and one action
            Action action = actions.get(0);

            msg.append("\n\tSUGGESTION: ").append(suggestion.getLabel());
            msg.append("\n\t\tACTION: " + action.getDescription());

            CommunicationRequest communicationRequest = (CommunicationRequest) action.getResource();
            String payload = communicationRequest.getPayloadFirstRep().getContentStringType().getValue();

            Scanner scanner = new Scanner(payload);
            int lineNumber = 0;
            while (scanner.hasNextLine()) {
                lineNumber++;
                String[] record = scanner.nextLine().split("\\|");
                if (lineNumber > 1) { // skip header
                    RegistryInstructionDTO regDTO = new RegistryInstructionDTO(
                            record[0]
                            , record[1]
                            , record[2]
                            , record[3]
                            , record[4]
                            , record[5]
                            , record[6]
                    );
                    registryInstructionDTOList.add(regDTO);
                }
            }
            scanner.close();
            log.info(msg.toString());
        }

        return UResult.successResult(registryInstructionDTOList);
    }
}

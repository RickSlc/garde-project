package edu.utah.rehr.garde.domain.ehr.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "epic")
public class SmartDataElementMetaList {

    private List<SmartDataElementMeta> sdes;

    public List<SmartDataElementMeta> getSdes() {
        return Objects.requireNonNullElseGet(sdes, ArrayList::new);
    }

    public Optional<SmartDataElementMeta> findSmartDataElementMetaByKey(String key) {
        return sdes.stream()
                .filter(smartDataElementMeta -> key.equals(smartDataElementMeta.getKey()))
                .findFirst();
    }

    @Getter
    @Setter
    public static class SmartDataElementMeta {
        private String key;
        private String id;
        private String type;
    }

}

package edu.utah.rehr.garde.domain.ehr.service;

import edu.utah.rehr.garde.domain.core.accessory.UResult;
import edu.utah.rehr.garde.domain.core.model.PatientFact;
import org.hl7.fhir.dstu3.model.Patient;

import java.util.List;

public interface IEhrClientService {

    UResult<String> readPatientDataElement(String patientId, String dataElementId);
    UResult<Boolean> writePatientDataElement(String patientId, String dataElementId, String dataElementValue);
    UResult<Integer> createCohortFile(String filePathAndName, String cohortId, List<PatientFact> patients);
    UResult<Integer> addPatientsToCohort(String cohortId, List<Patient> patients);

}

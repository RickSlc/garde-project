package edu.utah.rehr.garde.domain.ehr.model;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@ConfigurationProperties(prefix = "epic2.sde")
@Profile("epic2")
public class SmartDataElementProperties {
    private Map<String, String> id;
    public Map<String, String> getId() {
        return id;
    }

    public void setId(Map<String, String> id) {
        this.id = id;
    }

    public String getIdValue(String name) {
        return id.get(name);
    }

}

package edu.utah.rehr.garde.domain.ehr.config;


import edu.utah.rehr.garde.domain.ehr.model.SmartDataElementMetaList;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EhrConfig {

    @Bean
    SmartDataElementMetaList sdeMetaList() {
        return new SmartDataElementMetaList();
    }

}

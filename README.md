# GARDE README

GARDE is a population clinical decision support (CDS) platform based on Fast Healthcare Interoperability
Resources (FHIR) and CDS Hooks standards to support interoperability and logic sharing beyond single
vendor solutions.

GARDE project website https://reimagineehr.utah.edu/innovations/garde/

Details on the GARDE project and its software architecture can be found at https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9006693/

## GARDE Recommends Genetic Testing
1. when risk of hereditary breast, ovarian, pancreas, or prostate cancer (HBOC) is detected based on NCCN guidelines.
To use GARDE to recommend genetic testing for HBOC, see the specifications below in "Running GARDE with your data".
2. when risk of familial hypercholesterolemia (FH) is detected based on JACC guidelines. To use GARDE to recommend
genetic testing for FH, the specifications are in [README-FH](docs/README-FH.md).

## GARDE Project Tech

GARDE requires three servers:

1. Population Coordinator - SpringBoot application with an embedded Vue.js webapp
2. CDS Hooks - SpringBoot application running the OpenCDS Hooks server
3. Garde Database - a PostgreSQL database

This project, garde-project, is a Java-Maven project containing all the code for GARDE for #1 and #2. The following maven 
modules contain the servers:

| Java Project          | Directory                                 | Jar                                                         | Description                                                                                                                                                    |
|-----------------------|-------------------------------------------|-------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| garde-popcoord-webapp | source/app/garde-popcoord-webapp          | target/garde-popcoord-webapp<build-version>-spring-boot.jar | GARDE's Population Coordinator processes population analysis requests                                                                                          |
| garde-cdshooks-server | source/impl/garde-cdshooks-server | target/garde-cdshooks-server<build-version>-spring-boot.jar | GARDE's clinical decision support (CDS) engine based on the [CDS Hooks](https://cds-hooks.org/) standard that processes CDS requests and returns CDS responses |

## Building GARDE Docker images

See Docker build and push scripts in the ./docker directory.

## Deploying GARDE via Docker (Recommended)

See the instructions in the garde-docker project [here](https://bitbucket.org/RickSlc/garde-docker)

See alternative instructions to build/run GARDE in sections below.

## Configuring & Running the GARDE Demo

Once GARDE is deployed and you can see the web application's login page, you can run the demo. 

### Running the GARDE Demo
1. Open a web browser to http://yourserver.org:8090 and login. The demo does not
   require https. https needs to be setup on your server before real data are used. Log into the demo using

Default user/password: admin/adminPassword


   <img src="docs/images/GARDE-Dashboard-v3.jpg" alt="GARDE running in Docker Desktop"/>


2. Make sure the two checks are green next to the Population Coordinator and CDS Hooks Server.

3. Load the terminology file [garde/terminology/garde-ehr-code-bindings-ut.csv](garde/terminology/garde-ehr-code-bindings-ut.csv) - see "Load the terminology mappings file"
   in [README-Terminology.md](docs/README-Terminology.md) section "Load the terminology mappings file."

Once loaded, you should see the numbers change in the Code Summary in the columns that lead with a green E (EHR codes) as shown in the documentation.
4. With the terminology codes loaded, click on the Evaluators menu item on the left and then NCCN 2023 to start the demo evaluation.


   <img src="docs/images/GARDE-Evaluator-v3.png" alt="GARDE Evaluator"/>

5. Click "Run" and choose the demo files from garde/demo-files/u-DE-ID-pat-demog-20220810.csv for demographics and
    u-DE-ID-pat-fhx-20220810.csv for the family history.


   <img src="docs/images/GARDE-Evaluator-run-dialog.png" alt=""/>


   <img src="docs/images/GARDE-run-file-picker.jpg" alt=""/>

6. Click Run at the bottom of the dialog.

7. The Evaluation will start. Progress is reported state by state until the process
    completes.

   <img src="docs/images/GARDE-evaluation-log.png" alt=""/>

8. Click on the "REVIEW RESULTS" button to view the results.


   <img src="docs/images/GARDE-evaluator-met-criteria-results.png" alt=""/>


9. Export the results if desired by clicking on the down arrow in the header.

10. That's it! You have just run 1000 patients through GARDE and discovered those who met NCCN genetic testing criteria.

## Running GARDE with your data

The GARDE demo reads data from two CSV files, patient demographics and patient family history. See the CSV file
specifications and EHR-specific extraction instructions below to create your own files.

### Patient demographics CSV spec

| Column Header Name     | Required | Requires Mapping | Format     | Description                                                                                        |
|------------------------|----------|------------------|------------|----------------------------------------------------------------------------------------------------|
| EHR_PAT_ID_CODE_SYSTEM | Y        | Y                | URI        | EHR patientId code system URI                                                                      |
| EHR_PAT_ID             | Y        | N                | String     | Unique identifier for the patient                                                                  |
| BIRTH_DATE             | Y        | N                | yyyy-mm-dd | ISO 8601-formatted birth date                                                                      |
| EHR_SEX_CODE_SYSTEM    | Y        | Y                | URI        | EHR Sex code system URI                                                                            |
| EHR_SEX_CODE           | Y        | Y                | String     | EHR sex code                                                                                       |
| EHR_SEX_LABEL          | Y        | Y                | String     | EHR sex label                                                                                      |
| EHR_RELIGION_CODE_SYSTEM | N        | Y                | URI        | EHR religion code system URI                                                                       |
| EHR_RELIGION_CODE      | N        | Y                | String     | EHR religion code                                                                                  |
| EHR_RELIGION_LABEL     | N        | Y                | String     | EHR religion label                                                                                 |
| PREV_MET_CRITERIA_LIST | N        | N                | Comma-delimted list | If the person is already on the registry, this is the list of criteria that they most recently met |

Required - all columns must exist in the CSV fies. When an column is not required, that means the column value may be empty in any given row.

Requires Mapping - (see "Mapping your organizations data onto GARDE's" below)

### SQL to extract patient demographics CSV from Epic

The SQL query below will extract demographics data from Epic's Clarity database following the spec above. Edit the query by replacing
YOUR_INSTITUTION with the name of your institution and YOUR_POPULATION that will contain the table/view name containing the
set of patient IDs that you want to run through GARDE. Also, validate that the output date formats match the CSV specs.

```sql
select 'http://epic.com/YOUR_INSTITUTION/clarity/patient.pat_id' EHR_PAT_ID_CODE_SYSTEM
     , pt.pat_id                                                 EHR_PAT_ID
     , pt.birth_date                                             BIRTH_DATE
     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_sex'         EHR_SEX_CODE_SYSTEM
     , sx.rcpt_mem_sex_c                                         EHR_SEX_CODE
     , sx.name                                                   EHR_SEX_LABEL
     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_religion'    EHR_RELIGION_CODE_SYSTEM
     , rl.religion_c                                             EHR_RELIGION_CODE
     , rl.name                                                   EHR_RELIGION_LABEL
     , null                                                      PREV_MET_CRITERIA_LIST -- null if not synchronizing with a registry
from CLARITY.patient pt
         join YOUR_POPULATION pat_denom on pat_denom.pat_id = pt.pat_id -- table or sub-query with patients population to evaluate
         left outer join CLARITY.zc_sex sx on (sx.rcpt_mem_sex_c = pt.sex_c)
         left outer join CLARITY.zc_religion rl on (rl.religion_c = pt.religion_c)
```

### SQL to extract patient demographics AND previously met criteria from an Epic registry

For those who are sending GARDE's findings to a registry, there is an additional field required, PREV_MET_CRITERIA_LIST,
that allows GARDE to give specific registry-specific synchronization instructions. In the above query it is set to null
if/when registry synchronization is not required. In the query below, the smart data element is used both to

```sql
with met_criteria as (

select pt.pat_id, sv.SMRTDTA_ELEM_VALUE prev_met_criteria_list
from clarity.SMRTDTA_ELEM_DATA sd
join clarity.SMRTDTA_ELEM_PATIENT pt on pt.hlv_id = sd.hlv_id
join clarity.SMRTDTA_ELEM_VALUE sv on sv.hlv_id = sd.hlv_id and sd.element_id = YOUR_SMART_DATA_ELEMENT_ID -- met criteria SDE ID

)

select 'http://epic.com/YOUR_INSTITUTION/clarity/patient.pat_id' EHR_PAT_ID_CODE_SYSTEM
     , pt.pat_id                                                 EHR_PAT_ID
     , pt.birth_date                                             BIRTH_DATE
     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_sex'         EHR_SEX_CODE_SYSTEM
     , sx.rcpt_mem_sex_c                                         EHR_SEX_CODE
     , sx.name                                                   EHR_SEX_LABEL
     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_religion'    EHR_RELIGION_CODE_SYSTEM
     , rl.religion_c                                             EHR_RELIGION_CODE
     , rl.name                                                   EHR_RELIGION_LABEL
     , mc.prev_met_criteria_list                                 PREV_MET_CRITERIA_LIST
from clarity.patient pt
     join YOUR_POPULATION pat_denom on pat_denom.pat_id = pt.pat_id -- table or sub-query with patients population to evaluate
     left join met_criteria mc on mc.pat_id = pt.pat_id -- table with patients to evaluate
     left join clarity.zc_sex sx on (sx.rcpt_mem_sex_c = pt.sex_c)
     left join clarity.zc_religion rl on (rl.religion_c = pt.religion_c)
```

### Patient family history CSV from Epic

| Column Header Name       | Required | Requires Mapping | Format     | Description                                            |
|--------------------------|----------|------------------|------------|--------------------------------------------------------|
| EHR_FAMILY_HX_ID_CODE_SYSTEM| Y        | Y                | URI        | EHR familyHistoryId code system                        |
| EHR_FAMILY_HX_ID| Y        |                  | String     | EHR familyHistoryId                                    |
| EHR_PAT_ID_CODE_SYSTEM   | Y        | Y                | URI        | EHR patientId code system URI                          |
| EHR_PAT_ID               | Y        |                  | String     | Unique identifier for the patient                      |
| EHR_ENC_ID_CODE_SYSTEM|          | Y                | URI        | EHR encounterId code system URI                        |
| EHR_ENC_ID|          |                  | String     | EHR encounterId                                        |
| EHR_REC_ID|          |                  | String     | Family history recordId                                |
| EHR_REC_DT|          |                  | yyyy-mm-dd | Date of the record                                     |
| EHR_COND_CODE_SYSTEM| Y        | Y                | URI        | Family history condition code system URI               |
| EHR_COND_CODE| Y        | Y                | String     | EHR condition code                                     |
| EHR_COND_LABEL| Y        | Y                | String     | EHR condition label                                    |
| EHR_RELATION_CODE_SYSTEM| Y        | Y                | URI        | EHR relative/relation code system                      |
| EHR_RELATION_CODE| Y        | Y                | String     | EHR relative/relation code                             |
| EHR_RELATION_LABEL| Y        | Y                | String     | EHR relative/relation label                            |
| EHR_COND_ONSET_AGE|          |                  | Number     | EHR condition age of onset                             |
| EHR_COMMENTS|          |                  | String     | Comments clinicians add to clarify the coded statement |

#### <span style="color:cyan">ALL of the columns are required in the CSV file in the specified order. When a Column is not marked as Required the column values may be empty.</span>

### SQL to extract family hx from Epic

The SQL query below will extract family history data from Epic's Clarity database following the spec above. Edit the
query by replacing YOUR_INSTITUTION with the name of your institution and YOUR_POPULATION that will contain the table/view/sub-query
name containing the set of patient IDs that you want to run GARDE on. Validate that the output formats
match the CSV specs, dates especially.

CAUTION: Reading the CSV file into Excel may auto-format several of the columns from the correct format to
the wrong format, especially dates (from 2000-01-31 to 1/31/00) and IDs with leading zeros (leading zeros will be dropped).
Use a text editor to verify the true values for each column.

```sql
WITH max_fhx_contact AS (

    SELECT
        fhx.pat_id,
        MAX(fhx.pat_enc_csn_id) max_enc_id
    FROM CLARITY.family_hx fhx
        JOIN YOUR_POPULATION pop ON pop.pat_id = fhx.pat_id
    GROUP BY fhx.pat_id
)

SELECT
    'http://epic.com/YOUR_INSTITUTION/clarity/family_hx/pat_id__pat_enc_csn_id__line'  EHR_FAMILY_HX_ID_CODE_SYSTEM
     , fhx.pat_id || '_' || fhx.pat_enc_csn_id || '_' || fhx.line EHR_FAMILY_HX_ID
     , 'http://epic.com/YOUR_INSTITUTION/clarity/patient.pat_id'  EHR_PAT_ID_CODE_SYSTEM
     , fhx.pat_id           EHR_PAT_ID
     , 'http://epic.com/YOUR_INSTITUTION/clarity/pat_enc.pat_enc_csn_id'  EHR_ENC_ID_CODE_SYSTEM
     , fhx.pat_enc_csn_id   EHR_ENC_ID
     , fhx.line             EHR_REC_ID
     , fhx.contact_date     EHR_REC_DT
     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_medical_hx'  EHR_COND_CODE_SYSTEM
     , fhx.medical_hx_c     EHR_COND_CODE
     , zmhx.title           EHR_COND_LABEL
     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_relation'  EHR_RELATION_CODE_SYSTEM
     , fhx.relation_c       EHR_RELATION_CODE
     , zrel.title           EHR_RELATION_LABEL
     , fhx.age_of_onset     EHR_COND_ONSET_AGE
     , fhx.comments         EHR_COMMENTS
FROM
    CLARITY.family_hx       fhx
        JOIN max_fhx_contact             mx ON mx.pat_id = fhx.pat_id AND mx.max_enc_id = fhx.pat_enc_csn_id
        JOIN CLARITY.zc_medical_hx   zmhx ON zmhx.medical_hx_c = fhx.medical_hx_c
        JOIN CLARITY.zc_relation     zrel ON zrel.relation_c = fhx.relation_c 
             and fhx.relation_c != 15 -- FILTER OUT RELATION THA MEANS "NO FAMILY HISTORY." CHECK YOUR VALUES, THEY MAY BE DIFFERENT! 
```

### Common extraction errors

* Birth date is not formatted correctly. WARNING -- if you are using Excel to manage your CSV data, it may change and reformat the
  date to its format (dd/mm/yyyy or similar), even if it has been extracted correctly (yyyy-mm-dd).
* Column names are not uppercase. Fix - column names are currently case sensitive. Change them to uppercase. There is a ticket to make them all case insensitive.
* Misspellings in column names. Fix - be picky, the column names need to match exactly.
* Extra columns are added. Fix - delete extras.
* Columns are out of order. Fix - make sure the order is correct.
* Null values have the word "NULL" for a value. Fix - delete the "NULL" strings and leave the column empty.
* Cardinality of the join with YOUR_POPULATION table creates duplicate family history records. Fix - ensure YOUR_POPULATION table or query contains distinct pat_id.
* CSV file character set is UTF-8 BOM and has invisible characters that interferes with Java String comparisons during validation. Fix - save the file as UTF-8 (no BOM)

GARDE will help diagnose many of these issues, but it's best to get these issues resolved while creating the extracts such
that additional editing is not required after the extract has been performed.

### Mapping your site's terminology to GARDE

Once you have extracted the data from the EHR, you'll need to map the codes from your version of Epic to GARDE's codes.

See Authoring Mappings for Your Site in [README-Terminology.md](docs/README-Terminology.md)

## Running GARDE via web application

The GARDE web app is useful for validating that GARDE has been configured correctly, to see that the servers are running,
to see and verify terminology code mappings, and is also a good place to run/test that the data files meet specification.
Once your patient demographics and family history CSVs are ready and you have mapped/loaded your site's terminology
code, run the GARDE evaluator exactly as described above for the demo, only using the CSV files generated for your site.

GARDE detects and reports errors. If you encounter an error, read the provided message and make the proper adjustments. If
an error occurs but there is no message, or if the error message is vague, review the application logs in YOUR_DEMO_DATA_FOLDER/appLogs
to see more detailed error information.

## Running GARDE by CLI

GARDE can be run periodically on a schedule using command-line tools such as curl to access GARDE's RESTful web service
gateway. Using curl you can run GARDE as follows (make sure you replace YOUR_DEMO_DATA_FOLDER with your installation's path):

```shell

INSTANCE_ID=$(uuidgen)

curl -X POST https://garde-server:8090/runEvaluation \
     -u admin:adminPassword \
     -F "serviceId=genetic-testing-recommender-r4-2023" \
     -F "instanceId=${INSTANCE_ID}" \
     -F "outputType=MET_CRITERIA_OUTPUT_TYPE" \
#     -F "outputType=REGISTRY_INSTRUCTION_OUTPUT_TYPE" \
     -F "patFHxFile=@YOUR_DEMO_DATA_FOLDER/demo-files/ut-DE-ID-pat-fhx-20220810.csv" \
     -F "patDemogFile=@YOUR_DEMO_DATA_FOLDER/demo-files/ut-DE-ID-pat-demog-20220810.csv"

```

This example uses the demographics and family history files from the demo (last two lines of the command). You'll replace those
paths with paths to files from your institution.

Once the evaluation finishes, the response will contain a result file name. The following command will download the file.
Also, the commented line in the command above is for those who are managing a patient population registry. Review the two
different formats (choose one format per command) to determine which is best for your purpose.

```shell
curl -u admin:adminPassword \
     -O https://garde-server:8090/downloadResults/<result-file>.csv
```

The results file contains the information needed to conduct downstream ETL operations (patientId, met criteria, etc.).

More information about GARDE is available in the included docs/README files.

## Building the GARDE project

GARDE is a multi-module Maven project (see the pom.xml files). If you are interested in building GARDE, access to the OpenCDS Maven repository is required and can be requested via OpenCDS.org.
With proper access simply run

```
mvn clean install
```

A successful build process will build jars. The jar locations/file-names in the table above near the top.

**A database is required** to run garde. Especially while configuring/testing garde, using Docker to instantiate a database
is recommended. See our [Docker project](https://bitbucket.org/RickSlc/garde-docker/src/main/docker/docker-compose.yml)
configurations for examples.

Once you have a database up and running, ensure the properties for the application fit your environment, including the db
properties, by reviewing the application-<profile>.properties in /INSTALL_DIR/garde-project/garde/config. When the
properties appear to be correct run garde.

## Starting the GARDE Web App and REST Services via CLI

```bash
java -Xmx4g -jar garde-popcoord-webapp-<build-version>-spring-boot.jar --spring.config.location=file:///INSTALL_DIR/garde-project/garde/config/ | tee /INSTALL_DIR/garde-project/garde/appLogs/garde-popcoord-webapp-$(date +'%Y%m%d_%H%M%S').log
```

After a clean startup, visit http://localhost:8090/garde and login using the default user/password (admin/adminPassword).
These credentials can be changed/secured properly by resetting them in the application-basicsecurity.properties.

```properties
spring.security.user.name=admin
spring.security.user.password=adminPassword
```

#!/bin/bash

date

# Contact the GARDE team to get a username and password for the nexus server
# 8084 is read/write.
# 8083 is the read-only repo. Tagging 8084 and 8083 to test client experience.
docker login https://nexus.chpc.utah.edu:8084

docker tag rickslc/garde-popcoord-webapp:latest nexus.chpc.utah.edu:8084/garde/garde-popcoord-webapp:latest
docker tag rickslc/garde-popcoord-webapp:latest nexus.chpc.utah.edu:8083/garde/garde-popcoord-webapp:latest
docker push nexus.chpc.utah.edu:8084/garde/garde-popcoord-webapp:latest

docker tag rickslc/garde-popcoord-webapp-arm64:latest nexus.chpc.utah.edu:8084/garde/garde-popcoord-webapp-arm64:latest
docker tag rickslc/garde-popcoord-webapp-arm64:latest nexus.chpc.utah.edu:8083/garde/garde-popcoord-webapp-arm64:latest
docker push nexus.chpc.utah.edu:8084/garde/garde-popcoord-webapp-arm64:latest

docker tag rickslc/garde-cdshooks:latest nexus.chpc.utah.edu:8084/garde/garde-cdshooks:latest
docker tag rickslc/garde-cdshooks:latest nexus.chpc.utah.edu:8083/garde/garde-cdshooks:latest
docker push nexus.chpc.utah.edu:8084/garde/garde-cdshooks:latest

docker tag rickslc/garde-cdshooks-arm64:latest nexus.chpc.utah.edu:8084/garde/garde-cdshooks-arm64:latest
docker tag rickslc/garde-cdshooks-arm64:latest nexus.chpc.utah.edu:8083/garde/garde-cdshooks-arm64:latest
docker push nexus.chpc.utah.edu:8084/garde/garde-cdshooks-arm64:latest

# docker tag rickslc/garde-cdshooks-win-amd64:latest nexus.chpc.utah.edu:8084/garde/garde-cdshooks-win-amd64:latest
# docker tag rickslc/garde-cdshooks-win-amd64:latest nexus.chpc.utah.edu:8083/garde/garde-cdshooks-win-amd64:latest
# docker push nexus.chpc.utah.edu:8084/garde/garde-cdshooks-win-amd64:latest

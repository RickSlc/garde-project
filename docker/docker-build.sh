#!/bin/bash

date

# cli maven not working. intelliJ maven build works
# mvn clean install -DskipTests

# build from source/app/garde-rest-services/docker-build.sh
cd ../source/app/garde-popcoord-webapp
./docker-build.sh

cd ../../impl/garde-cdshooks-server
./docker-build.sh


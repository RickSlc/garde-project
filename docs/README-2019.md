# GARDE README 

GARDE is a population clinical decision support (CDS) platform based on Fast Healthcare Interoperability 
Resources (FHIR) and CDS Hooks standards to support interoperability and logic sharing beyond single 
vendor solutions. 

GARDE project website https://reimagineehr.utah.edu/innovations/garde/

Details on the GARDE project and its software architecture can be found at https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9006693/

## GARDE Project Tech

GARDE requires three servers:

1. Population Coordinator - SpringBoot application with an embedded Vue.js webapp

2. CDS Hooks - SpringBoot application running the OpenCDS Hooks server

3. Garde Database - a PostgreSQL database

Each of the servers have been implemented using Docker as follows:

| Project                                       | Docker build                                    | Docker image               | Docker container       |
|-----------------------------------------------|-------------------------------------------------|----------------------------|------------------------|
| garde-popcoord-webapp                         | source/app/garde-popcoord-webapp/docker-build.sh | rickslc/garde-popcoord-webapp | garde-popcoord-webapp  |
| nccn-opecds-hooks-springboot-server           | source/app/garde-cdshooks/docker-build.sh       | rickslc/garde-cdshooks        | garde-cds-hooks        |
| (See [PostgreSQL project](https://www.postgresql.org/)) | [postgreSQL](https://hub.docker.com/_/postgres) | postgres         | garde-db-postres       |


## Deploying GARDE using Docker


The following instructions explain how to deploy GARDE using Docker and how to run a demo with sample data that is provided with GARDE�s Docker.

0. Prerequisite - Docker docker is installed running.

1. Create a network in docker by running the following command
```
docker network create garde-network
```
2. Get GARDE's Population Coordinator image
```
docker pull rickslc/garde-popcoord-webapp
```
3. Get GARDE's CDS Hooks  image
```
docker pull rickslc/garde-cdshooks
```
4. Copy the folder garde-project/demo-data folder to <span style="color: red">YOUR_DEMO_DATA_FOLDER</span>.
```
cp -r garde-project/demo-data YOUR_DEMO_DATA_FOLDER
```
5. Create/run the GARDE database container
```
docker run --network garde-network \
--network-alias garde-db \
--name garde-db-postgres \
-e POSTGRES_PASSWORD=gardePwd \
-d -p 5432:5432 postgres
```
6. Create/run GARDE's Population Coordinator container

Replace <span style="color: red">YOUR_DEMO_DATA_FOLDER</span> in the command below with the path to your new demo-data folder.
```
docker run -p 8090:8090 \
--rm \
--memory="4g" \
--network garde-network \
--mount type=bind,source=YOUR_DEMO_DATA_FOLDER/demo-data,target=/shared/garde \
--name garde-popcoord-webapp \
-e "SPRING_PROFILES_ACTIVE=gardeDocker,selfsigned" \
-e CATALINA_OPTS="-Xmn2g" \
rickslc/garde-popcoord-webapp
```
The above command creates and runs the container with stdout directed to the shell. 
Ctrl-C kills the process and removes (-rm) the container.

To run the container in the background and persist its state, change the "-rm" flag to "-d".

7. <span style="color: green">In a new shell</span>, create/run GARDE's CDS Hooks server container

Replace <span style="color: red">YOUR_DEMO_DATA_FOLDER</span> in the command below with the path to your new demo-data folder.
```
docker run -p 8091:8091 \
--rm \
--memory="4g" \
--network garde-network \
--network-alias garde-cdshooks-server \
--mount type=bind,source=YOUR_DEMO_DATA_FOLDER/demo-data,target=/shared/garde \
--name garde-cdshooks \
-e "SPRING_PROFILES_ACTIVE=gardeCdsDocker,gardecds,jpa,basicsecuritycds" \
-e CATALINA_OPTS="-Xmn2g" \
rickslc/garde-cdshooks
```
This command also creates and runs the container with stdout directed to the shell.
Ctrl-C kills the process and removes (-rm) the container.

To run the container in the background and persist its state, change the "-rm" flag to "-d".

8. In a web browser (Tested using Google Chrome) visit localhost:8090.The default user/password is 

*admin/adminPassword*

To change the user and password edit the properties in <span style="color: red">YOUR_DEMO_DATA_FOLDER</span>/config/gardeDocker.properties

```properties
spring.security.user.name=admin
spring.security.user.password=adminPassword
```

<img src="images/GARDE-Dashboard.jpg" alt="GARDE dashboard snapshot"/>

## Running the GARDE demo

9. Click on the Dashboard menu item on the left, click on EVALUATE POPULATION. If this option is not available there is
likely a configuration problem. As the picture indicates, there should be a green check next to the Population Coordinator 
and the CDS Hooks Server. If not, check the log files in the <span style="color: red">YOUR_DEMO_DATA_FOLDER</span>/logs directory.

- Select the demo demographic file (ut-DE-ID-pat-demog-20220810.csv) and family history 
file (ut-DE-ID-pat-fhx-20220810.csv) from <span style="color: red">YOUR_DEMO_DATA_FOLDER/demo-files</span> 
and click RUN

<img src="images/GARDE-Evaluate-choose-files.jpg" alt="Choose files snapshot"/>

10. Evaluation completion

<img src="images/GARDE-Evaluate-completed.jpg" alt="Evaluation complete snapshot"/>

### Results Review
11. Click on the REVIEW RESULTS button to see the patient-level results

<img src="images/GARDE-Evaluate-results-review.jpg" alt="Review results, people who met genetic testing criteria snapshot"/>

### Results Output File
The results are in <span style="color: red">YOUR_DEMO_DATA_FOLDER/output/</span>(file name in the <i>Evaluation completed</i> message) 
as follows below:

<img src="images/GARDE-Evaluate-results-file.jpg" alt="Snapshot of the results file."/>

Each row in the file represents a family history record from the EHR that triggered a specified rule,
including the EHR patient ID, the encounter ID, the family history record number, the rule cohort, and a human-readable code
that links to the rule. There are often multiple family history records that are required to trigger a rule,
i.e. there are often multiple rows for each person.

12. If an error occurs, check the docker server logs. The typical problems:
* One of the servers isn't running (see list below) or isn't configured properly - make 
sure the database is running, then the Population Coordinator, then the Open CDSHooks server. Check the properties
in <span style="color: red">YOUR_DEMO_DATA_FOLDER/config/*.properties</span>
* The CSV file doesn't meet the specification. Date format errors are easy to overlook - see specs below

## Running GARDE with your data

The GARDE demo reads data from two CSV files, patient demographics and patient family history. See the CSV file 
specifications and EHR-specific extraction instructions below to create your own files.

### Patient demographics CSV spec

| Column Header Name           | Required | Requires Mapping | Format     | Description                                                                                        |
|------------------------------|----------|------------------|------------|----------------------------------------------------------------------------------------------------|
| EHR_PAT_ID_CODE_SYSTEM       | Y        | Y                | URI        | EHR patientId code system URI                                                                      |
| EHR_PAT_ID                   | Y        | N                | String     | Unique identifier for the patient                                                                  |
| BIRTH_DATE                   | Y        | N                | yyyy-mm-dd | ISO 8601-formatted birth date                                                                      |
| EHR_SEX_CODE_SYSTEM          | Y        | Y                | URI        | EHR Sex code system URI                                                                            |
| EHR_SEX_CODE                 | Y        | Y                | String     | EHR sex code                                                                                       |
| EHR_SEX_LABEL                | Y        | Y                | String     | EHR sex label                                                                                      |
| EHR_RELIGION_CODE_SYSTEM     | N        | Y                | URI        | EHR religion code system URI                                                                       |
| EHR_RELIGION_CODE            | N        | Y                | String     | EHR religion code                                                                                  |
| EHR_RELIGION_LABEL           | N        | Y                | String     | EHR religion label                                                                                 |
| PREVIOUSLY_MET_CRITERIA_LIST | N        | N                | Comma-delimted list | If the person is already on the registry, this is the list of criteria that they most recently met |

Required - all columns must exist in the CSV fies. When an column is not required, that means the column value may be empty in any given row.

Requires Mapping - (see "Mapping your organizations data onto GARDE's" below)

### SQL to extract patient demographics CSV from Epic

The SQL query below will extract demographics data from Epic's Clarity database following the spec above. Edit the query by replacing 
YOUR_INSTITUTION with the name of your institution and YOUR_TABLE that will contain the table/view name containing the
set of patient IDs that you want to run through GARDE. Also, validate that the output date formats match the CSV specs.

```
select 'http://epic.com/YOUR_INSTITUTION/clarity/patient.pat_id' EHR_PAT_ID_CODE_SYSTEM
     , pt.pat_id                                                 EHR_PAT_ID
     , pt.birth_date                                             BIRTH_DATE
     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_sex'         EHR_SEX_CODE_SYSTEM
     , sx.rcpt_mem_sex_c                                         EHR_SEX_CODE
     , sx.name                                                   EHR_SEX_LABEL
     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_religion'    EHR_RELIGION_CODE_SYSTEM
     , rl.religion_c                                             EHR_RELIGION_CODE
     , rl.name                                                   EHR_RELIGION_LABEL
from CLARITY_RAW.patient pt
         join YOUR_TABLE pat_denom on pat_denom.pat_id = pt.pat_id -- table with patients to evaluate
         left outer join CLARITY.zc_sex sx on (sx.rcpt_mem_sex_c = pt.sex_c)
         left outer join CLARITY.zc_religion rl on (rl.religion_c = pt.religion_c)
```

### Patient family history CSV from Epic

| Column Header Name       | Required | Requires Mapping | Format     | Description                                            |
|--------------------------|----------|------------------|------------|--------------------------------------------------------|
| EHR_FAMILY_HX_ID_CODE_SYSTEM| Y        | Y                | URI        | EHR familyHistoryId code system                        |
| EHR_FAMILY_HX_ID| Y        |                  | String     | EHR familyHistoryId                                    |
| EHR_PAT_ID_CODE_SYSTEM   | Y        | Y                | URI        | EHR patientId code system URI                          |
| EHR_PAT_ID               | Y        |                  | String     | Unique identifier for the patient                      |
| EHR_ENC_ID_CODE_SYSTEM|          | Y                | URI        | EHR encounterId code system URI                        |
| EHR_ENC_ID|          |                  | String     | EHR encounterId                                        |
| EHR_REC_ID|          |                  | String     | Family history recordId                                |
| EHR_REC_DT|          |                  | yyyy-mm-dd | Date of the record                                     |
| EHR_COND_CODE_SYSTEM| Y        | Y                | URI        | Family history condition code system URI               |
| EHR_COND_CODE| Y        | Y                | String     | EHR condition code                                     |
| EHR_COND_LABEL| Y        | Y                | String     | EHR condition label                                    |
| EHR_RELATION_CODE_SYSTEM| Y        | Y                | URI        | EHR relative/relation code system                      |
| EHR_RELATION_CODE| Y        | Y                | String     | EHR relative/relation code                             |
| EHR_RELATION_LABEL| Y        | Y                | String     | EHR relative/relation label                            |
| EHR_COND_ONSET_AGE|          |                  | Number     | EHR condition age of onset                             |
| EHR_COMMENTS|          |                  | String     | Comments clinicians add to clarify the coded statement |

#### <span style="color:cyan">ALL of the columns are required in the CSV file in the specified order. When a Column is not marked as Required the column values may be empty.</span>

### SQL to extract family hx from Epic

The SQL query below will extract family history data from Epic's Clarity database following the spec above. Edit the 
query by replacing YOUR_INSTITUTION with the name of your institution and YOUR_TABLE that will contain the table/view 
name containing the set of patient IDs that you want to run through GARDE. Also, validate that the output date formats 
match the CSV specs.

```
WITH max_fhx_contact AS (

    SELECT
        fhx.pat_id,
        MAX(fhx.pat_enc_csn_id) max_enc_id
    FROM CLARITY.family_hx fhx
        JOIN n_test_pat tst ON tst.ehr_pat_id = fhx.pat_id
    GROUP BY fhx.pat_id
)

SELECT
    'http://epic.com/YOUR_INSTITUTION/clarity/family_hx/pat_id__pat_enc_csn_id__line'  famil_hx_id_code_system
     , fhx.pat_id || '_' || fhx.pat_enc_csn_id || '_' || fhx.line family_hx_id
     , 'http://epic.com/YOUR_INSTITUTION/clarity/patient.pat_id'  ehr_pat_id_code_system
     , fhx.pat_id           ehr_pat_id
     , 'http://epic.com/YOUR_INSTITUTION/clarity/pat_enc.pat_enc_csn_id'  ehr_enc_id_code_system
     , fhx.pat_enc_csn_id   ehr_enc_id
     , fhx.line             ehr_rec_id
     , fhx.contact_date     ehr_rec_dt
     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_medical_hx'  ehr_cond_code_system
     , fhx.medical_hx_c     ehr_cond_code
     , zmhx.title           ehr_cond_label
     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_relation'  ehr_relation_code_system
     , fhx.relation_c       ehr_relation_code
     , zrel.title           ehr_relation_label
     , fhx.age_of_onset     ehr_cond_onset_age
     , fhx.comments         ehr_comments
FROM
    CLARITY.family_hx       fhx
        JOIN max_fhx_contact             mx ON mx.pat_id = fhx.pat_id AND mx.max_enc_id = fhx.pat_enc_csn_id
        JOIN YOUR_TABLE pat_denom on pat_denom.pat_id = fhx.pat_id -- table with patients to evaluate
        JOIN clarity_org.zc_medical_hx   zmhx ON zmhx.medical_hx_c = fhx.medical_hx_c
        JOIN clarity_org.zc_relation     zrel ON zrel.relation_c = fhx.relation_c 
             and fhx.relation_c != 15 -- FILTER OUT "NO FAMILY HISTORY." CHECK YOUR VALUES, THEY MAY BE DIFFERENT! 
```

### Mapping your organizations data to GARDE's
The CSV specs indicate which fields require mappings. You can map your organizations codes to GARDE's using the
GARDE web app. Start GARDE (see demo instructions) and click on <i>Recommenders/Code Mappings</i>

<img src="images/GARDE-CodeMappings.jpg" alt="Snapshot of the terminology code mapping interface"/>

Every code on the page is used by the GARDE algorithm. More code coverage implies more opportunities for detecting 
individual familial risk.  

The blue-green S icon indicates standard codes (SNOMED CT, FHIR, etc.) 
and the red E icon indicates EHR codes. The added coded mappings need to match exactly the codes extracted into CSVs 
from the queries above.

### Algorithm rules and code mappings
The algorithm rules are viewable by clicking on the <i>Recommenders/Rules</i>

<img src="images/GARDE-Rules.jpg" alt="Snapshot of the algorithm rules"/>

## Building the GARDE projects

### Building the SpringBoot servers - Population Coordinator and CDS Hooks

Prerequisites:

* Java 17
* Maven
* Access to the OpenCDS repositories - request access via opencds.org

Go to the project root directory, garde-app, and run
```
> mvn clean install -DskipTests
```
(Tests can be run in individual projects but are not configured for the global build at this point in time.)

#### Additional SpringBoot resources
* [Building SpringBoot Apps](https://spring.io/guides/gs/spring-boot/)
* [Running SpringBoot Apps](https://docs.spring.io/spring-boot/docs/1.5.22.RELEASE/reference/html/using-boot-running-your-application.html)

### Build the GARDE administration web app 

Prerequisites:
* [npm](https://www.npmjs.com/)

Go to the project directory
```
> cd source/app/garde-admin-webapp
> npm run build
```

To run the server
```
> npm run serve
```

To preview the GARDE admin web app, visit the link posted in the output after running the server, localhost:8080 
unless 8080 is busy in which case the next open port will be selected.

If the Population Coordinator (localhost:8090) and CDS Hooks servers (localhost:8091) are not running, 
functionality will be limited.


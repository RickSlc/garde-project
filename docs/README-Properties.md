# GARDE Properties README
GARDE uses properties files to declare settings for specific services. Property file names contain Spring profile 
names that are used as application startup parameters to boot and load desired service configurations. 

Property filename examples

```
application.properties
application-local.properties
application-rick.properties
application-docker.properties
```

where *local*, *rick*, and *docker* are the respective profile names. The first file, application.properties, without the extension has no profile 
association and is always read first. When the spring profile *local* is specified spring reads the properties from
the *application-local.properties* file. The order profiles are specified on the command line is also important. Spring reads them from left
to right, i.e. if property X is in two profiles, the value of X from the profile on the right-most profile will be X's value in the app.

SpringBoot property file directories and profiles are specified on the app's startup command-line. If one is developing and all of the servers are 
on the local machine, the following command is an example that first reads application.properties, then application-local.properties, 
then application-rick.properties.

```shell
java -jar the-springboot-app.jar \
--spring.config.location=garde-data/config/ \
--spring.profiles.active=local,rick,selfsigned \
> garde-data/garde-popcoord-webapp.log
```

Notice *selfsign*, the last profile specified in the above command, is not associated with a properties file. This implies it is 
specified in a spring configuration in the code base. Code-configured profiles are generally located in *config* packages and class 
names ending with *Config*, i.e. app configurations are declarable.

### Current profiles
| apps           | profile          | purpose                                                                                                                                                       |
|----------------|------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| GARDE          | basicsecurity    | adds basic security configurations                                                                                                                            |
| GARDE          | selfsigned       | adds a special rest template to allow self-signed certificates                                                                                                |
| GARDE          | rick             | settings for Rick's machine only - one may want to copy it, change the profile name, and add the profile to the boot startup command to use personal settings |
| GARDE          | gardeDocker      | Configuration for docker-based GARDE app.                                                                                                                     |
| GARDE          | gardeLocal       | Configuration for cli-based CDS services                                                                                                                      |
| GARDE, GardeCDS | local            | designed for running GARDE on a desktop/laptop and using localhost to run the services                                                                        |
| GARDE, GardeCDS | nosecurity       | disables GARDE's security - useful for development                                                                                                            |
| GARDE, GardeCDS | jpa              | enables the jpa object-relational database mappings and services.                                                                                             |
| GardeCDS        | basicsecuritycds | adds basic security configurations                                                                                                                            |
| GardeCDS        | gardecds         | GARDE config for CDS services                                                                                                                                 |
| GardeCDS        | gardeCdsDocker   | Configuration for docker-based CDS services                                                                                                                   |
| GardeCDS        | gardeCdsLocal    | Configuration for cli-based CDS Hooks server app.                                                                                                             |

Due to the dynamic nature of these property profile capabilities this list changes frequently. New profiles are continuously 
created for new situations/environments encountered, i.e. it contains commonly used profiles/properties, but
is not intended to be a complete list.

## Property Examples

### GARDE service settings

```properties
garde.popcoord.endpoint=https://localhost:8090
garde.websocket.endpoint=wss://localhost:8090/channel/evaluators
spring.security.user.name=admin
spring.security.user.password=adminPassword
garde.dataProvider=LOCAL_FILE_SYSTEM
garde.data.page.size=1000

# https - self-signed SSL for local
server.ssl.key-store-type=PKCS12
server.ssl.key-store=classpath:keystore/garde-ss.p12
server.ssl.key-store-password=keyst0rePassw0rd!
server.ssl.key-alias=gardess
server.ssl.enabled=true
```

### CDS Hooks service settings

```properties
server.port=8091
# Fhir server has not been released - this is a placeholder
rehr.nccn.fhir.server.url=https://localhost:8095/fhirServer
rehr.nccn.cdshooks.server.url=https://localhost:8091/r4/hooks/cds-services/
rehr.nccn.cdshooks.server.username=cdsUser
rehr.nccn.cdshooks.server.password=cdsPassword
```

### Database and JPA settings

```properties
spring.sql.init.platform=postgresql
spring.sql.init.mode=always
spring.sql.init.continue-on-error=true

spring.jpa.defer-datasource-initialization=true
spring.jpa.database=POSTGRESQL
spring.jpa.hibernate.ddl-auto=update
spring.jpa.show-sql=false

# db settings to run classpath:schema-postresql.sql after jpa creates tables
spring.sql.init.platform=postgresql
spring.jpa.defer-datasource-initialization=true
spring.sql.init.mode=always
spring.sql.init.continue-on-error=true

# URL prefix for jpa rest services -> SERVICE_URL/data/<mapped-entity-name>
spring.data.rest.basePath=/data

spring.datasource.url=jdbc:postgresql://localhost:5432/postgres
spring.datasource.username=postgres
spring.datasource.password=gardePwd
```

### Chatbot service settings

```properties
chatbot.endpoint=http://xxx.xxx.xxx:8080/ubot/
chatbot.secret=xxxxxxxxxxxxxxxx
```

### EHR service settings
```properties
epic.writeSdeService=epic/2013/Clinical/Utility/SETSMARTDATAVALUES/SmartData/Values
epic.readSdeService=epic/2013/Clinical/Utility/GETSMARTDATAVALUES/SmartData/Values
epic.fhirMetadataService=fhir/r4/metadata

epic.interconnectServer=https://epic.xxxx.xxx/Interconnect-KMM-CODE/api/
epic.testPatientId=xxxxxxx
epic.clientId=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
epic.interconnectUser.encrypted=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
epic.interconnectPassword.encrypted=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
epic.userId=xxxxxxxx
```

### AWS S3 service settings (no support for demo)

```properties
s3.profile=data-exchange
s3.region=us-east-1
s3.dataProvider.bucket=s3-population-coordinator-prod
s3.dataProvider.root.directory=prod
s3.dataProvider.input.directory=/input
s3.dataProvider.patientFacts.searchPattern=/PATIENT_2
s3.dataProvider.familyHxFacts.searchPattern=/PATIENT_FHX_2
s3.dataProvider.studyArm.searchPattern=/PATIENT_OUTREACH_2
s3.dataProvider.output.directory=/output
```

### SFTP service settings (no support for demo)

```properties
# ftp is not yet supported for the demo
sftp.server=sftp.xxyy.xyz
sftp.folder.pretest=/in/prod/pretest
sftp.folder.pretest.backup=/out/prod/pretest
sftp.folder.negtest=/in/prod/posttest
sftp.folder.negtest.backup=/out/prod/posttest
sftp.username.encrypted=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
sftp.password.encrypted=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
```

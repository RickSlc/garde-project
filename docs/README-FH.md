# GARDE's Genetic Testing Recommender for Familial Hypercholoesterolemia (FH)

BACKGROUND: Although awareness of familial hypercholesterolemia (FH) is increasing, this common, potentially fatal, treatable
condition remains underdiagnosed. Despite FH being a genetic disorder, genetic testing is rarely used. The Familial
Hypercholesterolemia Foundation convened an international expert panel to assess the utility of FH genetic testing.
The rationale includes the following: 1) facilitation of definitive diagnosis; 2) pathogenic variants indicate higher
cardiovascular risk, which indicates the potential need for more aggressive lipid lowering; 3) increase in initiation of
and adherence to therapy; and 4) cascade testing of at-risk relatives. The Expert Consensus Panel recommends that FH
genetic testing become the standard of care for patients with definite or probable FH, as well as for their at-risk
relatives. Testing should include the genes encoding the low-density lipoprotein receptor (LDLR), apolipoprotein B
(APOB), and proprotein convertase subtilisin/kexin 9 (PCSK9); other genes may also need to be considered for analysis
based on patient phenotype. Expected outcomes include greater diagnoses, more effective cascade testing, initiation of
therapies at earlier ages, and more accurate risk stratification.

## FH Guidelines for Genetic Testing

   <img src="images/FH-JACC-Guideline.jpg" alt="" style="width: 1000px;"/>

*Sturm, A. C., Knowles, J. W., Gidding, et al (2018). Clinical genetic testing for familial hypercholesterolemia: JACC
scientific expert panel. \*Journal of the American College of Cardiology\*, \*72\*(6).*

## GARDE Interpretation of the FH Guidelines for Genetic Testing

**<span style="color:lightgreen">Status: GARDE interpretations for FH are currently being reviewed, tested, and refined.</span>**

GARDE's goal is to identify adult patients with probable FH who need genetic testing to facilitate a definitive
diagnosis and initiation of proper treatment. With this in mind and in reference to the FH Guidelines, GARDE
identifies adults who *should be offered* genetic testing (see guideline above).
 
## Running GARDE to identify people with probable FH

* Install GARDE. [Instructions here](https://bitbucket.org/RickSlc/garde-docker)
* Map your terminology codes to GARDE's FH codes, log into GARDE, and import into your terminology mappings using the
terminology importer. [Instructions here](README-Terminology.md)
* Extract the population to be evaluated. Instructions below.
* Extract three datasets, 1) patient demographics, 2) family history, and 3) LDL-C observations data for your population. Instructions below.
* Log into GARDE, choose *Genetic Testing Recommender for FH* from the evaluator picklist at the top, and Evaluator/FH 2018 from the
left-side menu, then click Run, and then select the patient 3 data files previously extracted.
* GARDE will evaluate the population and identify individuals who meet criteria for genetic testing.
* Results can then be downloaded for whatever purpose you have in mind.

### Capture the population to be evaluated
#### Epic/Clarity Oracle Scripts

```sql
drop table fh_pat_to_eval_tmp
;
-- Create a temp table with the population that will be evaluated
create table fh_pat_to_eval_tmp as
SELECT distinct ENC.PAT_ID epic_pat_id, iid.identity_id mrn
FROM CLARITY_ORG.pat_enc ENC
       INNER JOIN CLARITY_ORG.patient PAT
       INNER JOIN CLARITY_ORG.identity_id iid on iid.pat_id = PAT.PAT_ID and iid.identity_type_id = 1 ON PAT.PAT_ID = ENC.PAT_ID
WHERE ENC.ENC_TYPE_C IN ('101','76')    -- Office or telehealth encounter type
  AND ENC.APPT_STATUS_C IN (2,6)        -- Completed or arrived
  AND ENC.CONTACT_DATE >= sysdate - 365 -- Look back a year
  AND floor(months_between(SYSDATE, PAT.BIRTH_DATE)/12) >= 18 -- age >= 18
  AND ENC.DEPARTMENT_ID in ( -- Change this list to "Primary Care" department IDs for your site. 
                            3401,11556,11559,11561,1201,1214,1202,2501,11773,2503,1401,3002,3004,1301,1322,1302,1601,11894,11827,11821,12022,11820,3903,3904,11414,10083,1501,1506,
                            1301,1501,2501,1601,1201,2401,3903,3002,1401,3401,11827,11556,10109,1203,10514,11778,3905,1503,10978,2901,2001,11725,1607,11819,10108,1309,11414,10988,
                            1001,2003,11991,11671,1901,1101,11717,11326,1403,10110,11482,3006,3501,3801,1104,11388,11684,2910,1003,3601,12152,2601,12151,11715
  )
;

CREATE INDEX fh_pat_to_eval_tmp_i1 ON fh_pat_to_eval_tmp(_PAT_ID);

-- Check patient identifiers for inconsistencies 
select count(*) row_cnt, count(distinct epic_pat_id), count(distinct mrn) from fh_pat_denom_tmp;

-- Setup de-identification map
create table fh_pat_to_eval as
SELECT epic_pat_id
     , mrn
     , RAWTOHEX(SYS_GUID()) de_pat_id
     , TRUNC(DBMS_RANDOM.VALUE(1,30)) random_offset_days
from fh_pat_to_eval_tmp
;

CREATE INDEX fh_pat_to_eval_i1 ON fh_pat_to_eval(EPIC_PAT_ID);
CREATE INDEX fh_pat_to_eval_i2 ON fh_pat_to_eval(MRN);
CREATE INDEX fh_pat_to_eval_i3 ON fh_pat_to_eval(de_pat_id);

drop table fh_pat_to_eval_tmp
;
```

### Dataset 1 extraction CSV specification -- patient demographics

| Column Header Name             | Required | Requires Mapping | Format              | Description                        |
|--------------------------------|----------|------------------|---------------------|------------------------------------|
| EHR_PAT_ID_CODE_SYSTEM         | Y        | Y                | URI                 | EHR patientId code system URI      |
| EHR_PAT_ID                     | Y        | N                | String              | Unique identifier for the patient  |
| BIRTH_DATE                     | Y        | N                | yyyy-mm-dd          | ISO 8601-formatted birth date      |
| ADDRESS_1                      | N        | N                | String              | Patient address line 1             |
| ADDRESS_2                      | N        | N                | String              | Patient address line 2             |
| CITY                           | N        | N                | String              | Patient address city               |
| STATE                          | N        | N                | String              | Patient address state              |
| ZIP                            | N        | N                | String              | Patient address zip code           |
| EHR_SEX_CODE_SYSTEM            | Y        | N                | URI                 | EHR Sex code system URI            |
| EHR_SEX_CODE                   | Y        | N                | String              | EHR sex code                       |
| EHR_SEX_LABEL                  | Y        | N                | String              | EHR sex label                      |
| EHR_MARITAL_STATUS_CODE_SYSTEM | N        | N                | URI                 | EHR marital status code system URI |
| EHR_MARITAL_STATUS_CODE        | N        | N                | String              | EHR marital status code            |
| EHR_MARITAL_STATUS_LABEL       | N        | N                | String              | EHR marital status label           |
| EHR_RACE_CODE_SYSTEM           | N        | Y                | URI                 | EHR race code system URI           |
| EHR_RACE_CODE                  | N        | Y                | String              | EHR race code                      |
| EHR_RACE_LABEL                 | N        | Y                | String              | EHR race label                     |
| EHR_ETHNICITY_CODE_SYSTEM      | N        | Y                | URI                 | EHR ethnicity code system URI      |
| EHR_ETHNICITY_CODE             | N        | Y                | String              | EHR ethnicity code                 |
| EHR_ETHNICITY_LABEL            | N        | Y                | String              | EHR ethnicity label                |
| EHR_RELIGION_CODE_SYSTEM       | N        | Y                | URI                 | EHR religion code system URI       |
| EHR_RELIGION_CODE              | N        | Y                | String              | EHR religion code                  |
| EHR_RELIGION_LABEL             | N        | Y                | String              | EHR religion label                 |
| EHR_LANGUAGE_CODE_SYSTEM       | N        | Y                | URI                 | EHR language code system URI       |
| EHR_LANGUAGE_CODE              | N        | Y                | String              | EHR language code                  |
| EHR_LANGUAGE_LABEL             | N        | Y                | String              | EHR language label                 |

Required - all columns must exist in the CSV fies. When a column is not required, it implies the column value may be empty in any given row.

Requires Mapping - (see "Mapping your organizations data onto GARDE's" below)

### SQL to extract patient demographics CSV from Epic

The SQL query below will extract demographics data from Epic's Clarity database following the spec above. Edit the query by replacing
YOUR_INSTITUTION with the name of your institution and YOUR_POPULATION that will contain the table/view name containing the
set of patient IDs that you want to run through GARDE. Also, validate that the output date formats match the CSV specs.

```sql
-- create a table to keep/store the data or extract it directly from the query
-- If you want the de-identified set, remove the first two EHR_PAT_ID-related columns and rename the following two
-- to EHR_PAT_ID_CODE_SYSTEM, and EHR_PAT_ID. 
create table fh_pat_demog as
select 'http://epic.com/YOUR_INSTITUTION/clarity/patient/pat_id'         EHR_PAT_ID_CODE_SYSTEM
     , pt.pat_id                                                EHR_PAT_ID
     , 'http://utah.edu/garde/patient/de_pat_id'                DE_PAT_ID_CODE_SYSTEM
     , pat_denom.de_pat_id                                      DE_PAT_ID
     , pt.birth_date + pat_denom.random_offset_days             BIRTH_DATE
     , (select adr1.address from clarity_org.pat_address adr1 where adr1.pat_id = pt.pat_id and line = 1) ADDRESS_1
     , (select adr2.address from clarity_org.pat_address adr2 where adr2.pat_id = pt.pat_id and line = 2) ADDRESS_2
     , pt.city
     , st.name state
     , pt.zip

     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_sex/sex_c'            EHR_SEX_CODE_SYSTEM
     , sx.rcpt_mem_sex_c                                         EHR_SEX_CODE
     , sx.title                                                  EHR_SEX_LABEL

     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_marital_status/marital_status_c'       EHR_MARITAL_STATUS_CODE_SYSTEM
     , ms.marital_status_c                                       EHR_MARITAL_STATUS_CODE
     , ms.title                                                  EHR_MARITAL_STATUS_LABEL

     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_patient_race/race_c'         EHR_RACE_CODE_SYSTEM
     , rc1.patient_race_c                                        EHR_RACE_CODE
     , zr1.title                                                 EHR_RACE_LABEL

     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_ethnic_group/ethnicity_c'         EHR_ETHNICITY_CODE_SYSTEM
     , eg.ethnic_group_c                                         EHR_ETHNICITY_CODE
     , eg.title                                                  EHR_ETHNICITY_LABEL

     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_religion/religion_c'             EHR_RELIGION_CODE_SYSTEM
     , rl.religion_c                                             EHR_RELIGION_CODE
     , rl.title                                                  EHR_RELIGION_LABEL

     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_language/language_c'             EHR_LANGUAGE_CODE_SYSTEM
     , lang.language_c                                           EHR_LANGUAGE_CODE
     , lang.title                                                EHR_LANGUAGE_LABEL

from CLARITY.patient pt
       join fh_pat_to_eval pat_denom on pat_denom.epic_pat_id = pt.pat_id -- table with patients to evaluate
       left outer join CLARITY.zc_sex sx on (sx.rcpt_mem_sex_c = pt.sex_c)
       left outer join CLARITY.zc_religion rl on (rl.religion_c = pt.religion_c)
       left outer join CLARITY.zc_state st on (st.state_c = pt.state_c)
       left outer join CLARITY.zc_marital_status ms on (ms.marital_status_c = pt.marital_status_c)
       left outer join CLARITY.zc_ethnic_group eg on (eg.ethnic_group_c = pt.ethnic_group_c)
       left outer join (select r1.* from CLARITY.patient_race r1 where r1.line = 1) rc1 on rc1.pat_id = pt.pat_id
       left outer join CLARITY.zc_patient_race zr1 on zr1.patient_race_c = rc1.patient_race_c
       left outer join (select r2.* from CLARITY.patient_race r2 where r2.line = 2) rc2 on rc2.pat_id = pt.pat_id
       left outer join CLARITY.zc_patient_race zr2 on zr2.patient_race_c = rc2.patient_race_c
       left outer join CLARITY.zc_language lang on (lang.language_c = pt.language_c
```
### Dataset 2 extraction CSV specification -- patient family history

| Column Header Name           | Required | Requires Mapping | Format     | Description                                            |
|------------------------------|----------|------------------|------------|--------------------------------------------------------|
| EHR_FAMILY_HX_ID_CODE_SYSTEM | Y        | Y                | URI        | EHR familyHistoryId code system                        |
| EHR_FAMILY_HX_ID             | Y        |                  | String     | EHR familyHistoryId                                    |
| EHR_PAT_ID_CODE_SYSTEM       | Y        | Y                | URI        | EHR patientId code system URI                          |
| EHR_PAT_ID                   | Y        |                  | String     | Unique identifier for the patient                      |
| EHR_ENC_ID_CODE_SYSTEM       |          | Y                | URI        | EHR encounterId code system URI                        |
| EHR_ENC_ID                   |          |                  | String     | EHR encounterId                                        |
| EHR_REC_ID                   |          |                  | String     | Family history recordId                                |
| EHR_REC_DT                   |          |                  | yyyy-mm-dd | Date of the record                                     |
| EHR_COND_CODE_SYSTEM         | Y        | Y                | URI        | Family history condition code system URI               |
| EHR_COND_CODE                | Y        | Y                | String     | EHR condition code                                     |
| EHR_COND_LABEL               | Y        | Y                | String     | EHR condition label                                    |
| EHR_RELATION_CODE_SYSTEM     | Y        | Y                | URI        | EHR relative/relation code system                      |
| EHR_RELATION_CODE            | Y        | Y                | String     | EHR relative/relation code                             |
| EHR_RELATION_LABEL           | Y        | Y                | String     | EHR relative/relation label                            |
| EHR_COND_ONSET_AGE           |          |                  | Number     | EHR condition age of onset                             |
| EHR_COMMENTS                 |          |                  | String     | Comments clinicians add to clarify the coded statement |

#### <span style="color:cyan">ALL columns are required in the CSV file in the specified order. When a Column is not marked as Required the column values may be empty.</span>

### SQL to extract family hx from Epic

The SQL query below will extract family history data from Epic's Clarity database following the spec above. Edit the
query by replacing YOUR_INSTITUTION with the name of your institution and YOUR_POPULATION that will contain the table/view/sub-query
name containing the set of patient IDs that you want to run GARDE on. Validate that the output formats
match the CSV specs, dates especially.

CAUTION: Reading the CSV file into Excel may auto-format several of the columns from the correct format to
the wrong format, especially dates (from 2000-01-31 to 1/31/00) and IDs with leading zeros (leading zeros will be dropped).
Use a text editor to verify the true values for each column.

```sql
WITH max_fhx_contact AS (

    SELECT
        fhx.pat_id,
        MAX(fhx.pat_enc_csn_id) max_enc_id
    FROM CLARITY.family_hx fhx
        JOIN YOUR_POPULATION pop ON pop.pat_id = fhx.pat_id
    GROUP BY fhx.pat_id
)

SELECT
    'http://epic.com/YOUR_INSTITUTION/clarity/family_hx/pat_id__pat_enc_csn_id__line'  EHR_FAMILY_HX_ID_CODE_SYSTEM
     , fhx.pat_id || '_' || fhx.pat_enc_csn_id || '_' || fhx.line EHR_FAMILY_HX_ID
     , 'http://epic.com/YOUR_INSTITUTION/clarity/patient.pat_id'  EHR_PAT_ID_CODE_SYSTEM
     , fhx.pat_id           EHR_PAT_ID
     , 'http://epic.com/YOUR_INSTITUTION/clarity/pat_enc.pat_enc_csn_id'  EHR_ENC_ID_CODE_SYSTEM
     , fhx.pat_enc_csn_id   EHR_ENC_ID
     , fhx.line             EHR_REC_ID
     , fhx.contact_date     EHR_REC_DT
     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_medical_hx'  EHR_COND_CODE_SYSTEM
     , fhx.medical_hx_c     EHR_COND_CODE
     , zmhx.title           EHR_COND_LABEL
     , 'http://epic.com/YOUR_INSTITUTION/clarity/zc_relation'  EHR_RELATION_CODE_SYSTEM
     , fhx.relation_c       EHR_RELATION_CODE
     , zrel.title           EHR_RELATION_LABEL
     , fhx.age_of_onset     EHR_COND_ONSET_AGE
     , fhx.comments         EHR_COMMENTS
FROM
    CLARITY.family_hx       fhx
        JOIN max_fhx_contact             mx ON mx.pat_id = fhx.pat_id AND mx.max_enc_id = fhx.pat_enc_csn_id
        JOIN CLARITY.zc_medical_hx   zmhx ON zmhx.medical_hx_c = fhx.medical_hx_c
        JOIN CLARITY.zc_relation     zrel ON zrel.relation_c = fhx.relation_c 
             and fhx.relation_c != 15 -- FILTER OUT RELATION THA MEANS "NO FAMILY HISTORY." CHECK YOUR VALUES, THEY MAY BE DIFFERENT! 
```

### Dataset 3 extraction CSV specification -- patient LDL-C Lab Observations

| Column Header Name       | Required | Requires Mapping | Format | Description                       |
|--------------------------|----------|------------------|--------|-----------------------------------|
| EHR_OBS_ID_CODE_SYSTEM   | Y        | Y                | URI    | EHR obs ID code system            |
| EHR_OBS_ID               | Y        |                  | String | EHR obs ID                        |
| EHR_PAT_ID_CODE_SYSTEM   | Y        | Y                | URI    | EHR patientId code system URI     |
| EHR_PAT_ID               | Y        |                  | String | Unique identifier for the patient |
| EHR_ENC_ID_CODE_SYSTEM   |          | Y                | URI    | EHR encounterId code system URI   |
| EHR_ENC_ID               |          |                  | String | EHR encounterId                   |
| EHR_OBS_TYPE_CODE_SYSTEM | Y        |                  | String | Observation type code system      |
| EHR_OBS_TYPE_CODE        | Y        |                  | String | Observation type code             |
| EHR_OBS_CODE_SYSTEM      | Y        | Y                | String | Observation code system           |
| EHR_OBS_CODE             | Y        | Y                | String | Observation code                  |
| EHR_OBS_LABEL            |          |                  | String | Human-readable bservation label   |
| EHR_OBS_DATE_TIME        | Y        |                  | String | Observation datetime              |
| EHR_OBS_VALUE            |          |                  | String | Observation value                 |
| EHR_OBS_VALUE_UNITS      |          |                  | String | Observation value units           |
| AGE_WHEN_RESULTED        |          |                  | String | Age of patient when resulted      |


### SQL to extract LDL-C from Epic Clarity

```sql
drop table fh_pat_obs
;
create table fh_pat_obs as
    SELECT 
     'http://epic.com/YOUR_INSTITUTION/clarity/order_proc/order_proc_id' EHR_OBS_ID_CODE_SYSTEM
    , OP.ORDER_PROC_ID                          EHR_OBS_ID
    , 'http://epic.com/YOUR_INSTITUTION/clarity/patient/pat_id' EHR_PAT_ID_CODE_SYSTEM
    , OP.PAT_ID                                 EHR_PAT_ID
     , 'http://utah.edu/garde/patient/de_pat_id'                DE_PAT_ID_CODE_SYSTEM
     , pat_denom.de_pat_id                                      DE_PAT_ID    
    , 'http://epic.com/YOUR_INSTITUTION/clarity/pat_enc' EHR_ENC_ID_CODE_SYSTEM
    , OP.pat_enc_csn_id                         EHR_ENC_ID
    , 'http://terminology.hl7.org/CodeSystem/observation-category' EHR_OBS_TYPE_CODE_SYSTEM
    , CAST('laboratory' AS VARCHAR2(20))                           EHR_OBS_TYPE
    ,'http://epic.com/YOUR_INSTITUTION/clarity/order_results/component_id' EHR_OBS_CODE_SYSTEM
    , comp.component_id EHR_OBS_CODE
    , comp.name         EHR_OBS_LABEL
    , to_char(ORD_RES.RESULT_DATE,'YYYY-MM-DD') EHR_OBS_DATE_TIME
    , ORD_RES.ORD_NUM_VALUE EHR_OBS_VALUE
    , ord_res.REFERENCE_UNIT EHR_OBS_VALUE_UNITS
    , floor(months_between(ORD_RES.RESULT_DATE, PAT.BIRTH_DATE)/12) AS AGE_WHEN_RESULTED
    FROM CLARITY.ORDER_PROC OP
    inner join fh_pat_to_eval pat_denom on pat_denom.epic_pat_id = OP.PAT_ID
    INNER JOIN CLARITY.PATIENT PAT ON pat_denom.epic_pat_id = PAT.PAT_ID
    INNER JOIN CLARITY.ORDER_RESULTS ORD_RES ON ORD_RES.ORDER_PROC_ID = OP.ORDER_PROC_ID
    inner join CLARITY.clarity_component comp on comp.component_id = ORD_RES.COMPONENT_ID
    WHERE ORD_RES.COMPONENT_ID IN (1234181741,1234151781,2392,4219,11422,121,3205,11227)
      AND ORD_RES.ORD_NUM_VALUE <> 9999999
      AND floor(months_between(ORD_RES.RESULT_DATE, PAT.BIRTH_DATE)/12) >= 18
    order by OP.pat_id, ORD_RES.RESULT_DATE
;

CREATE INDEX fh_pat_obs_20241016_i1 ON fh_pat_obs_20241016(EHR_PAT_ID);
CREATE INDEX fh_pat_obs_20241016_i2 ON fh_pat_obs_20241016(DE_PAT_ID);

```

### Common extraction errors

* Birth date is not formatted correctly. WARNING -- if you are using Excel to manage your CSV data, it may change and reformat the
  date to its format (dd/mm/yyyy or similar), even if it has been extracted correctly (yyyy-mm-dd).
* Column names are not uppercase. Fix - column names are currently case sensitive. Change them to uppercase. There is a ticket to make them all case insensitive.
* Misspellings in column names. Fix - be picky, the column names need to match exactly.
* Extra columns are added. Fix - delete extras.
* Columns are out of order. Fix - make sure the order is correct.
* Null values have the word "NULL" for a value. Fix - delete the "NULL" strings and leave the column empty.
* Cardinality of the join with YOUR_POPULATION table creates duplicate family history records. Fix - ensure YOUR_POPULATION table or query contains distinct pat_id.
* CSV file character set is UTF-8 BOM and has invisible characters that interferes with Java String comparisons during validation. Fix - save the file as UTF-8 (no BOM)

GARDE will help diagnose many of these issues, but it's best to get these issues resolved while creating the extracts such
that additional editing is not required after the extract has been performed.

### Mapping your site's terminology to GARDE

Once you have extracted the data from the EHR, you'll need to map the codes from your version of Epic to GARDE's codes.

See Authoring Mappings for Your Site in [README-Terminology.md](docs/README-Terminology.md)

## Run GARDE to Identify Potential FH Patients

After creating the 4 files above (demographics, family history, LDL labs, terminology mappings),  as follows: 

1. Perform the steps to [install and start GARDE here](https://bitbucket.org/RickSlc/garde-docker/README.md)
2. Follow the [instructions to run the demo here](../README.md), except

    a. choose the FH recommender from the picklist at the top

    b. load your terminology mappings file

    c. select your patient data files in the run dialog instead of the demo files

    d. Run
3. After the process completes, review the results in GARDE and/or download the results for analysis by clicking on the results button.


## GARDE Terminology

GARDE is designed to support interoperability using established terminology methods. Out-of-the-box, GARDE is packaged
with 400 standard codes that are mapped onto GARDE's evaluator rules. Additional standards are easy to load if desired
(instructions below). Just as standard codes can be added and mapped onto GARDE codes, proprietary/EHR codes can be mapped and
loaded as well. Definitions and instructions are provided below to get help you get started.

### Terminology Component Definitions 

| Entity            | Definition                                                                                                                                                                                                                                                                                                                                                                                                                                          |
|-------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Codes             | A code is comprised of a code system (URI that defines a code system), a code (A string value that identifies a concept as defined by the code system), and a label (A description of the concept as defined by the code system). Code system combined with a code establishes its composite key (makes it unique and comparable - label is not part of the equivalence evaluation). [FHIR reference](https://www.hl7.org/fhir/terminologies.html). |
| Code Associations | A code, a relationship, and another code. Associations are used to define value sets and code mappings.                                                                                                                                                                                                                                                                                                                                             |
| Value Sets        | Code sets that can be used in a specific context                                                                                                                                                                                                                                                                                                                                                                                                    |
| GARDE Code        | Special codes that GARDE uses internally -- all caps and underscores intended to make them human readable. Other codes, standards, EHR, etc., are mapped to GARDE's binding codes                                                                                                                                                                                                                                                                   |

#### Terminology components are used to
1. Define GARDE's internal codes and value sets via terminology components.
2. Map Standard codes (ICD 10, SNOMED CT, etc) to GARDE codes
3. Map EHR codes (from Epic, Cerner, etc.) to GARDE codes

### EHR Code Mappings
The files used for the [demo](../README.md) are a good place to start to review GARDE terminology 
mappings. First, review the file

[garde/terminology/garde-ehr-code-bindings-ut.csv](../garde/terminology/garde-ehr-code-bindings-ut.csv).

Below is a snapshot of the first rows that contains all of Utah's EHR code mappings to GARDE's HBOC (hereditary breast
and ovarian cancer) codes:

| GARDE_CODE      |CODE_SYSTEM| CODE       | CODE_LABEL                   |
|-----------------|-----------|------------|------------------------------|
| CANCER_BREAST   |http://epic.com/uhealth/clarity/zc_medical_hx| 1204       | Breast cancer                |
| CANCER_BREAST   |http://epic.com/uhealth/clarity/zc_medical_hx| 5          | "Cancer breast"              |
| CANCER_BREAST   |http://epic.com/uhealth/clarity/zc_medical_hx| 534        | Left breast cancer           |
| CANCER_BREAST   |http://epic.com/uhealth/clarity/zc_medical_hx| 628        | Postmenopausal breast cancer |
| CANCER_BREAST   |http://epic.com/uhealth/clarity/zc_medical_hx| 656        | Right breast cancer          |
| CANCER_PANCREAS |http://epic.com/uhealth/clarity/zc_medical_hx| 2100000011 | "Cancer pancreas"            |
| CANCER_PANCREAS |http://epic.com/uhealth/clarity/zc_medical_hx| 595        | Pancreatic cancer            |
| CANCER_PROSTATE |http://epic.com/uhealth/clarity/zc_medical_hx| 2100000005 | Prostate cancer              |
| CANCER_PROSTATE |http://epic.com/uhealth/clarity/zc_medical_hx| 8          | "Cancer prostate"            |
| HALF_SIBLING    |http://epic.com/uhealth/clarity/zc_relation| 2104300002 | "Half-sibling"               |
| HALF_BROTHER    |http://epic.com/uhealth/clarity/zc_relation| 2103200007 | "Half-brother"               |
| HALF_SISTER     |http://epic.com/uhealth/clarity/zc_relation| 2104300008 | "Half-sister"                |

Each of these rows translates to a mapping: GARDE_CODE -> EHR_CODE_IS -> (CODE_SYSTEM, CODE, CODE_LABEL). 

Using the first row as an example, it is translated as:

| CANCER_BREAST --> EHR_CODE_IS --> http://epic.com/uhealth/clarity/zc_medical_hx:120:Cancer-breast |
|--------------|

**Note -- GARDE_CODEs can have multiple mappings (as shown) to support as many mappings as needed.**

**Note -- GARDE terminology tooling/app does not support adding new/custom GARDE_CODEs. New GARE_CODEs may be requested
for special cases.**

**Note -- Only in the case of diagnosis/problem codes are more specific codes mapped to GARDE codes (CANCER_BREAST
EH_CODE_IS Left breast cancer). This is not necessary for relative relation codes, i.e., half-brother and half-sister
codes are not mapped to HALF_SIBLING**. The file

[GARDE-NCCN-binding-codes-2023.txt](https://bitbucket.org/RickSlc/garde-docker/src/main/garde/terminology/)

contains the complete list of GARDE codes for HBOC (codes GARDE rules are aware of) used in the demo.

### Standard Code Mappings

GARDE is packaged with 400 standard codes to support interoperability. The following file contains the starter set:

[garde-standard-code-bindings.csv](https://bitbucket.org/RickSlc/garde-docker/src/main/garde/terminology)

The file format is the same as described above for EHR Code Mappings.

GARDE's curated mappings to standard codes has the potential to minimize the mapping effort for those who 
use these standards.

### Authoring Mappings for Your Site

Mapping your EHR data onto GARDE requires a review of the codes your EHR uses for each of the GARDE codes. Independent 
of whether your EHR's codes are standards-based codes or proprietary codes, GARDE simply looks for the codes that are
added/loaded into GARDE. See the instructions to load a new mapping file below. A starter mapping file for HBOC has been
provided, a CSV file that contains all of the HBOC codes that GARDE looks for:

[GARDE-ehr-code-bindings-START_HERE.csv](https://bitbucket.org/RickSlc/garde-docker/src/main/garde/terminology/)

To start your mapping project, make a copy and rename it to something like "garde-ehr-code-bindings-YOUR_INSTITUTION.csv". Then, following 
the instructions/examples above, map each of the GARDE_CODEs in the file to a code by filling in the information required
for each code, the CODE_SYSTEM, CODE, and CODE_LABEL.

Hash marks "#" at the beginning of a row will tell the mapping processor to ignore that row. *Empty mappings will
produce errors*. We recommended putting a "#" on all incomplete or otherwise unmapped codes/rows.

**Important - GARDE does not require every GARDE code to be mapped. In fact, it is unlikely that all of the
GARDE codes will exist in your EHR. For example, many of the genetic mutations are new and do not exist yet in EHR lookup
libraries. Try to be as complete as possible to get the most complete and comprehensive results, but don't be 
surprised or concerned about codes that were not found/did not exist. Using GARDE's Evaluator Rules view, review which
codes have been mapped to each rule. An example if this view is shown below.**

#### Mapping to Epic Family Hx Codes (Requires query access to Clarity)

The Clarity family_hx table uses codes from table Clarity.ZC_MEDICAL_DX (verify this is true in your case).
To review all of the cancer codes used in system you could start with a query like this

```sql
select title, medical_hx_c
from clarity.zc_medical_hx
where title like '%CANCER%'
```
In the query results search for codes that map properly to the cancer type codes found in the file. In Utah's case, for 
"Breast cancer" the title is "CANCER, BREAST" and the medical_hx_c value is 5. Add the following row

|CODE_BINDING| CODE_SYSTEM                                            | CODE | CODE_LABEL            |
|-----------|--------------------------------------------------------|------|-----------------------|
|CANCER_BREAST| http://epic.com/YOUR_INSTITUTION/clarity/zc_medical_hx | 5 | CANCER, BREAST |

Find and complete as many as you can by editing the query.

To review familial relationships the Epic query is

```sql
select title, relation_c
from clarity.zc_relation
```

And similarly, "FATHER" will look something like this.

| CODE_BINDING | CODE_SYSTEM                                          | CODE | CODE_LABEL |
|--------------|------------------------------------------------------|------|------------|
| FATHER       | http://epic.com/YOUR_INSTITUTION/clarity/zc_relation | 2    | FATHER     |


:exclamation: It's important that each of the values (code system and code) are consistent with patient data that were
+extracted from the EHR. GARDE will use the codes you load against the codes that are extracted to run the evaluation
check for equivalence during GARDE evaluations. Code systems and (alphanumeric) codes are case-sensitive.

After you have exhausted your efforts filling out as many of the rows as you can find, add the "#" at the beginning of
the rows that you didn't find codes for.

### Load the terminology mappings file
In the GARDE app, click Terminology > Importer on the left menu to review the summary:

   <img src="images/GARDE-Terminology-summary.png" alt=""/>

No EHR codes had been loaded yet in the picture above ("GARDE codes -> EHR codes" is 0). To upload terminology mappings, 
click on "Import Terminology" up arrow, navigate to your file and load it.

   <img src="images/GARDE-Terminology-load-dialog.png" alt=""/>

The dialog will close when the import finishes (unless there is an error). Now you should see counts for the EHR codes. 

   <img src="images/GARDE-Terminology-summary-with-EHR.png" alt=""/>

### QA Loaded Terminology
GARDE Evaluators are mapped to Rules that are mapped to terminology-managed Value Sets and Codes providing the means to see exactly
which standards and/or EHR terminology entities GARDE rules map to. This assists the QA process by revealing rule-to-code
coverage for each rule.

Click on Evaluator > Rules 2023

   <img src="images/GARDE-Rule-terminology-qa.png" alt=""/>

Following each rule is a color-coded list of terminology dependencies. Click on any of them to see more detail.

| Color | Meaning                                                           |
|-------|-------------------------------------------------------------------|
| Green | Mapped to an EHR code (and possibly standard codes)               |
| Blue  | Is mapped to standard code/s, but no EHR codes                    |
| Red   | Not mapped                                                        |
| Yellow | These are code sets - click on them to see which codes are mapped |

Some of the codes in GARDE are not common, especially the genetic mutations and detailed familial relationships. Many of 
GARDE's familial relationships are more specific than codes from the EHR to support family side/blood relative 
computations ("Mother's father's father" versus "Maternal great grandfather"). And, recently discovered genetic
mutations are not often added to the family history selection lists. 

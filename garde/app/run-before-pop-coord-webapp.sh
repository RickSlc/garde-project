#!/bin/bash

# Add commands to run before garde-pop-coord-webapp here.
# Example for getting a secret key for cloud services, the command below will copy the echo command to the root's
# bash env setup file, .bashrc, that reads the key from a file dynamically without copying the key itself.

# echo "export CLOUD_KEY=\$(cat /shared/garde/security/secretKey)" > /root/.bashrc

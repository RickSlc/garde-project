#!/bin/bash

cd /shared/garde/app
./run-before-pop-coord-webapp.sh

# cat /root/.bashrc > /shared/garde/app/cat-root-bashrc.txt
source /root/.bashrc
# echo $AZURE_CLIENT_ID > /shared/garde/env.txt

java -Xmx6g -jar garde-popcoord-webapp-spring-boot.jar --spring.config.location=file:///shared/garde/config/ | tee /shared/garde/appLogs/garde-popcoord-webapp-$(date +'%Y%m%d_%H%M%S').log
